<?php

namespace Tests\Unit\Helpers;

use Tests\TestCase;

class TextClearTest extends TestCase
{
    /**
     * @param $text
     * @param $expected
     *
     * @dataProvider itClearTextSuccessDataProvider
     */
    public function test_it_clear_text_success($text, $expected)
    {
        $this->assertEquals($expected, text_clear($text));
    }

    public function itClearTextSuccessDataProvider(): array
    {
        return [
            ['Яблоки  на снегу', 'яблоки на снегу'],
            ['Яблоки   на снегу', 'яблоки на снегу'],
            ['Яблоки   на  снегу', 'яблоки на снегу'],
        ];
    }
}