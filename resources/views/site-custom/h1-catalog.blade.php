@php
    /** @var string $h1 */
    /** @var boolean $textCenter */
@endphp

<div class="section">
    <div class="container container--small container--white _md-pb-def">
        <div class="grid _items-center _sm-flex-nowrap">
            <div class="gcell _flex-grow">
                <h1 class="title _def-show title--size-h1 title--size-h1-border {{ $textCenter ? '_text-center' : '' }}">{!! $h1 !!} </h1>
                <a href="@foreach (Seo::breadcrumbs()->elements() as $breadcrumb)@if($loop -> remaining == 1){{ $breadcrumb -> getUrl() }}@endif @endforeach"
                   class="title title--link title--back _def-hide title--size-h1 title--size-h1-border {{ $textCenter ? '_text-center' : '' }}"> {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-left-thin', ['class' => 'title__icon']) !!} <span>{!! $h1 !!}</span></a>
            </div>
        </div>
    </div>
</div>
