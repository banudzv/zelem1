@php
/** @var string $h1 */
/** @var boolean $textCenter */
@endphp

<div class="section">
    <div class="container container--small container--white _md-pb-def">
        <div class="grid _items-center _sm-flex-nowrap">
            <div class="gcell _flex-grow">
                <h1 class="title title--size-h1 title--brand {{ $textCenter ? '_text-center' : '' }}">@lang('brands::site.brand') <span>{!! $h1 !!}</span> </h1>
            </div>
        </div>
    </div>
</div>
