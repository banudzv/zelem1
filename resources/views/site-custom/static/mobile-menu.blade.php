@if($main)
    <div class="_ptb-md">
        <div class="_mb-xs">
            @include('site-custom.static.phone-number.phone-number', [
                'phone' => $main,
                'link_mod_classes' => 'phone-number__link--size-lg',
                'show_description' => true,
            ])
        </div>
        <div class="_color-dark-blue">Бесплатно</div> <!-- todo добавить перевод -->
    </div>
@endif
