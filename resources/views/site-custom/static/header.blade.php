@if(isset($additions) && count($additions))
    <div class="grid _items-center _flex-nowrap _pl-md _xl-pl-def _xl-pr-xxl">
        <div class="header-line-item__link header-line-item__link--icon-small">
            <a href="tel:{{ $additions[0]->href }}">{{ $additions[0]->text_content }}</a>
            <div class="icon js-init" data-mfp="inline" data-mfp-src="#contact-popup">
                {!! SiteHelpers\SvgSpritemap::get('icon-arrow-bottom') !!}
            </div>
        </div>
        {{-- @if($main)
            <div class="gcell _pl-sm _xl-pl-def _def-show">
                @include('site-custom.static.phone-number.phone-number', [
                    'phone' => $main,
                    'link_mod_classes' => 'phone-number__link--size-xl',
                    'show_description' => true
                ])
            </div>
        @endif
        @if($additions && count($additions) > 0)
            <div class="gcell _pl-sm _xl-pl-def{{config('db.basic.mobile_show_phone_hide_slogan', false) ? null : ' _def-show'}}">
                @foreach($additions as $phone)
                    @if ($phone->text_content != null)
                        @include('site-custom.static.phone-number.phone-number', [
                            'phone' => $phone,
                        ])
                    @endif
                @endforeach
            </div>
        @endif
        <div class="gcell _pl-sm _xl-pl-def _def-show">
            {!! Widget::show('callback-button') !!}
        </div>
        @if ($schedule)
            <div class="gcell _pl-sm _xl-pl-def _xl-show">
                @include('site-custom.static.schedule-work.schedule-work')
            </div>
        @endif --}}
    </div>
@endif
