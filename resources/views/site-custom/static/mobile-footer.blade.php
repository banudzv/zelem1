<div class="gcell _ptb-md">
    <div class="_mt-md">
        @if($main)
            <div class="_mb-xs">
                @include('site-custom.static.phone-number.phone-number', [
                    'phone' => $main,
                    'link_mod_classes' => 'phone-number__link--size-lg',
                    'show_description' => true,
                ])
            </div>
        @endif
    </div>
</div>
