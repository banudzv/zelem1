@if(config('db.basic._'.\Lang::getLocale()))
    <div class="copyright">
        <p>{{ config('db.basic._'.\Lang::getLocale()) }}</p>
    </div>
@endif
