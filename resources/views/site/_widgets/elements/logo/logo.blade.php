@php
// $image = Html::image($logo, null, [
//     'class' => ['logo__image'],
// ]);
$url = in_array(Route::currentRouteName(), config('app.logo.notLinkRoutes', [])) ? null : route('site.home');
$_theme = isset($theme) ?? false ? $theme : 'dark';
$alt = 'alt="' . trans('global.logo-alt') . '"';
$title = 'title="' . trans('global.logo-title') . '"';
@endphp

@if($url)
    {!! Html::link(
        Route::currentRouteName() === 'site.home' ? null : route('site.home'),
        '<img class="logo__image" src="/resources/assets/site/svg/logo-zalem-' . $_theme . '.svg"' . $alt . $title . '>',
        ['class' => ['logo', $mod_classes ?? '']],
        null,
        false
    ) !!}
@else
    {!! Html::tag(
        'span',
        (string)
        '<img class="logo__image" src="/resources/assets/site/svg/logo-zalem-' . $_theme . '.svg"' . $alt . $title . '>',
        ['class' => ['logo', $mod_classes ?? '']]
    ) !!}
@endif
