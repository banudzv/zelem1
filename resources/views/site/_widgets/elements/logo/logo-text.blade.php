@php
$url = in_array(Route::currentRouteName(), config('app.logo.notLinkRoutes', [])) ? null : route('site.home');
@endphp

@if($url)
    {!! Html::link(
        Route::currentRouteName() === 'site.home' ||  Route::currentRouteName() == 'site.thank-you' ? null : route('site.home'),
        '<div class="logo__text">'.$text.'</div>',
        ['class' => ['logo', $mod_classes ?? '']],
        null,
        false
    ) !!}
@else
    {!! Html::tag(
        'span',
        '<div class="logo__text">'.$text.'</div>',
        ['class' => ['logo', $mod_classes ?? '']]
    ) !!}
@endif
