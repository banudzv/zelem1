<div class="vendor-code">
    <span class="vendor-code__label">@lang('products::site.attributes.vendor_code'): </span>
    <span class="vendor-code__code">{{ $code }}</span>
</div>
