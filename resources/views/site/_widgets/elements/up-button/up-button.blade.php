<div role="button" tabindex="0" class="up-button js-init" data-scroll-window='{"target": "up"}'>
	{!! SiteHelpers\SvgSpritemap::get('icon-arrow-top-thin', [
		'class' => 'up-button__symbol'
	]) !!}
    <div class="up-button__text">
        <!-- TODO: прогер нужно добавить в переводы-->
        Вверх
    </div>
</div>
