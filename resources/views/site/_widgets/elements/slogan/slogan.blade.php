@php
    $url = Route::currentRouteName() === 'site.home' ? null : route('site.home');
@endphp

<a {{$url !== null ? 'href=' .$url : null }} class="slogan {{$mod ?? ''}}">
    {{ $content }}
</a>
