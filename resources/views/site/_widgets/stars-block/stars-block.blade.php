<div class="stars-block stars-block--{{ $mod ?? 'normal' }}">
	<a {!! isset($link) ? sprintf("href=\"%s\"", route($link)) : null !!} class="stars-block__inner" title="{{ $title ?? null }}">
		<svg data-count="{{ $count }}" class="stars-block__svg" viewBox="0 0 75 15">
			<g>
				<use xlink:href="{{ site_media('assets/svg/spritemap.svg#icon-star') }}" x="0"></use>
			</g>
			<g>
				<use xlink:href="{{ site_media('assets/svg/spritemap.svg#icon-star') }}" x="25%"></use>
			</g>
			<g>
				<use xlink:href="{{ site_media('assets/svg/spritemap.svg#icon-star') }}" x="50%"></use>
			</g>
			<g>
				<use xlink:href="{{ site_media('assets/svg/spritemap.svg#icon-star') }}" x="75%"></use>
			</g>
			<g>
				<use xlink:href="{{ site_media('assets/svg/spritemap.svg#icon-star') }}" x="100%"></use>
			</g>
		</svg>
	</a>
</div>
