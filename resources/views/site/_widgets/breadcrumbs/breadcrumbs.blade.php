<script type="application/ld+json">
    {!! Seo::breadcrumbs()->ldJson() !!}
</script>
<div class="_md-pb-xs _pt-sm">
    <ol class="breadcrumbs">
        @foreach(Seo::breadcrumbs()->elements() as $breadcrumb)
            @if ($loop->last || !$breadcrumb->getUrl())
                <li class="breadcrumbs__item breadcrumbs__item--last">
                    <a class="breadcrumbs__link breadcrumbs__link--last">
                        <span>{{ __($breadcrumb->getTitle()) }}</span>
                    </a>
                </li>
                @else
                <li class="breadcrumbs__item">
                    <a class="breadcrumbs__link breadcrumbs__link--link" href="{{ $breadcrumb->getUrl() }}">
                        <span itemprop="name">{{ __($breadcrumb->getTitle()) }}</span>
                    </a>
                </li>
            @endif
        @endforeach
    </ol>
</div>
