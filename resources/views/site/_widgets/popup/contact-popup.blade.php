<div id="contact-popup" class="popup popup--contact">
    <div class="title title--size-h2 _color-dark-blue _mb-md">
        Контактные телефоны
    </div>
    <div class="grid grid--1">
        <div class="gcell _pb-sm">
            <a href="tel:{{ config('db.basic.phone_number_1') }}" class="link text--size-18">
                {{ config('db.basic.phone_number_1') }}
            </a>
        </div>
        <div class="gcell _pb-sm">
            <a href="tel:{{ config('db.basic.phone_number_2') }}" class="link text--size-18">
                {{ config('db.basic.phone_number_2') }}
            </a>
        </div>
        <div class="gcell ">
            <a href="tel:{{ config('db.basic.phone_number_3') }}" class="link text--size-18">
                {{ config('db.basic.phone_number_3') }}
            </a>
        </div>
    </div>
    <div class="title title--size-h2 _color-dark-blue _mb-md">
        График работы
    </div>
    <div class="text _color-blue-smoke text--size-18 _pb-def">
        {!! config('db.basic.schedule_' . \Lang::getLocale()) !!}
    </div>
    <a href="mailto:info@zalem.com.ua" class="link link--underline link--dark text--size-18">
        {{ config('db.basic.admin_email') }}
    </a>
    <div class="mfp-close">
        {!! SiteHelpers\SvgSpritemap::get('icon-close') !!}
    </div>
</div>
