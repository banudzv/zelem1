<div class="section">
    <div class="container _pb-xl">
        <article class="wysiwyg">
            @if(isset($h1))
                <h1>{{ $h1 }}</h1>
            @endif
            <div class="scroll-text scroll-text--article _color-gray2">
                <div class="scroll-text__content wysiwyg js-init _posr"
                        data-wrap-media
                        data-prismjs
                        data-draggable-table
                        data-perfect-scrollbar>
                    <div>
                        {{ $slot }}
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
