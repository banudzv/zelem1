@php
/** @var bool $isOpen */
$isOpen = $isOpen ?? false;
@endphp

<div class="conditions-item">
    <div class="conditions-item__head {{ $isOpen ? 'is-open' : null }}" data-wstabs-ns="conditions" data-wstabs-button="{{ $tabs }}">
        <div class="grid _flex-nowrap _items-center _flex-noshrink _mr-md">
            @if($icon)
            <div class="gcell _flex-noshrink">
                <div class="conditions-item__icon">
                    {!! SiteHelpers\SvgSpritemap::get($icon) !!}
                </div>
            </div>
            @endif
            <div class="gcell _pl-sm">
                <div class="conditions-item__title">{{ $title }}</div>
            </div>
        </div>
        @if ($paymentIcon ?? false)
        @endif
    </div>
    <div {!! $isOpen ? 'class="is-open"' : 'style="display: none;"' !!} data-wstabs-ns="conditions" data-wstabs-block="{{ $tabs }}">
        @if($paymentIcon ?? false)
           <div class="conditions-item__img _justify-start _pt-md">
               @foreach ($paymentIcon as $item)
                   <img src="/static/images/payment-methods/{{$item}}.png" alt="{{$item}}">
               @endforeach
           </div>
        @endif
        <div class="conditions-item__descr">
            {!! $descr !!}
        </div>
        <span class="conditions-item__link link link--underline link--dark js-init" data-mfp="ajax" data-mfp-src="{{ $url }}">
            @lang('buttons.detail')
        </span>
    </div>
</div>
