@php

    $languages = config('languages', []);

    $phone = ($phone = config('db.basic.phone_number_1')) ? '+' . preg_replace("/[^,.0-9]/", '', $phone) : null;

    $compareCounter = \CompareProducts::count();
    $wishlistCounter = \Wishlist::count();

    /** @var string[] $mmenuIconTop */
    $mmenuIconTop = [
        '<a' . ((Route::currentRouteName() === 'site.home') ? '' : ' href="/"') . ' class="mm-icon-link">' .
            SiteHelpers\SvgSpritemap::get('icon-home') .
        '</a>',
        '<a href="#" class="mm-icon-link js-init" data-mfp="inline" data-mfp-src="#popup-callback">' .
            SiteHelpers\SvgSpritemap::get('icon-phone') .
        '</a>',
        '<a href="#" class="mm-icon-link js-init" data-mfp="inline" data-mfp-src="#popup-consultation">' .
            SiteHelpers\SvgSpritemap::get('icon-ask-doctor') .
        '</a>'
    ];

    /** @var string[] $mmenuIconBottom */
    $mmenuIconBottom = [];
    $mmenuIconItems = Widget::show('social_buttons::icons-mobile') ?? [];
    if ($mmenuIconItems) {
        foreach ($mmenuIconItems['icons'] as $key => $link) {
            if (!empty($link)) {
                $str = '<a target="_blank" class="mm-icon-link mm-icon-link--' . $key . '" href="' . $link . '">' .
                            SiteHelpers\SvgSpritemap::get('icon-network-'.$key) .
                        '</a>';
                array_push($mmenuIconBottom, $str);
            }
        }
    }

    $options = [
        'type' => 'MenuNav',
        'user-type-options' => [
            'options' => [
                'navbar' => [
                    'title' => '',
                ],
                'navbars' => [
                    [
                        'position' => 'top',
                        // 'type' => 'tabs',
                        'content' => [
                            '<a href="'.route('site.home').'" class="logo logo--mobile _ml-def _p-none _text-left" >' . SiteHelpers\SvgSpritemap::get('logo-zalem-light') . '</a>',
                            '<div class="header-over__right _flex _justify-end _pr-sm">
                                <div class="header-line__list header-line__list--languages">'
                                    . Widget::show('languages::trigger') .
                                '</div>
                            </div>
                            <div class="mm-navbar-close">'
                                . SiteHelpers\SvgSpritemap::get('icon-close') .
                            '</div>'
                        ]
                        // 'content' => [
                        //     '<a href="#mobile-menu__menu">' . __('site_menu::site.mobile-menu-block') . '</a>',
                        //     (count($languages) > 1) ? '<a href="#mobile-menu__lang">' . __('site_menu::site.mobile-lang-block') . '</a>' : '',
                        // ]
                    ]
                ],
                // 'iconbar' => [
                //     'add' => true,
                //     'top' => $mmenuIconTop,
                //     'bottom' => $mmenuIconBottom,
                // ],
            ],
            'configuration' => (object)[],
        ]
    ];

    $isCurrent = preg_match('/^site.wishlist$/', Route::currentRouteName());
@endphp
<nav id="mobile-menu" class="js-init" data-product data-mmenu-module='{!! json_encode($options) !!}'>
    <ul id="mobile-menu__menu">
        <li>
            {!! Widget::show('categories::mobile-menu') !!}
        </li>
        <li class="mm--bordered">
            {!! Widget::show('users::mobile-auth-link') !!}
            <a class="mm-custom-link _flex _items-center" href="javascript:void(0)" role="button" tabindex="0"
               class="menu-button" data-cart-trigger="open" data-cart-splash="">
               <span class="_posr">
                    {!! SiteHelpers\SvgSpritemap::get('icon-shopping-mobile-menu', ['class' => 'mm-menu-icon']) !!}
                    <span class="menu-button__availability-indicator" data-cart-counter="total-quantity"></span>
               </span>
                <span>@lang('orders::site.cart')</span>
            </a>

            <a href="{{ route('site.wishlist') }}"
               role="link" tabindex="0"
               class="mm-custom-link _flex _items-center"
               data-wishlist-link>
                <span class="_posr">
                    {!! SiteHelpers\SvgSpritemap::get('icon-wishlist-mobile-menu', ['class' => 'mm-menu-icon']) !!}
                    <span class="menu-button__availability-indicator" data-wishlist-counter>
                        {{ $wishlistCounter }}
                    </span>
                </span>
                <span>@lang('wishlist::site.account-menu-link')</span>
            </a>
            <a href="{{ route('site.compare') }}"
               role="link" tabindex="0"
               class="mm-custom-link _flex _items-center js-init"
               data-comparelist-link>
                <span class="_posr">
                    {!! SiteHelpers\SvgSpritemap::get('icon-compare-mobile-menu', ['class' => 'mm-menu-icon']) !!}
                    <span class="menu-button__availability-indicator" data-comparelist-counter>
                        {{ $compareCounter }}
                    </span>
                </span>
                <span>@lang('compare::general.comparison')</span>
            </a>
        </li>
        <li>
            {!! Widget::show('site-menu::mobile') !!}
            <span class="_pl-def">{!! Widget::show('contacts', 'mobile-menu') !!}</span>
        </li>
    </ul>
    @if(count($languages) > 1)
        <ul id="mobile-menu__lang">
            @foreach($languages as $language)
                <li>
                    {!! Html::link($language->current_url_with_new_language, ucfirst($language->name), [
                        'class' => [$language->is_current ? 'is-active' : ''],
                    ]) !!}
                </li>
            @endforeach
        </ul>
    @endif
</nav>
