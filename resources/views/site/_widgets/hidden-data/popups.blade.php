@php
$currentRouteName = Route::currentRouteName();
@endphp
@if(\Illuminate\Support\Str::startsWith($currentRouteName, 'site.checkout'))
    {!! Widget::show('checkout-auth-popup') !!}
@else
    {!! Widget::show('auth-popup') !!}
@endif
{!! Widget::show('callback-popup') !!}
{!! Widget::show('consultation-popup') !!}
{!! Widget::show('contact-popup') !!}
@include('site._widgets.popup.contact-popup')
@include('site._widgets.popup.send-resume')
@include('site._widgets.popup.wishlist-confirm-mass-delete')
