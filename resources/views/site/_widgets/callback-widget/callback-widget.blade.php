@php
    /** @var array $items */
@endphp
<div class="callback-widget js-init" data-tooltip='{"trigger": "click", "offset": [-50, 30]}'>
    <div class="callback-widget__icon">
        {!! SiteHelpers\SvgSpritemap::get('icon-message') !!}
    </div>
    <div class="callback-widget__title">
        @lang('social_buttons::site.call-to-zalem')
    </div>
    <div class="template">
        <div data-tooltip-template>
            <div class="tooltip">
                @foreach ($items as $item)
                    <div class="tooltip__item">
                        <div class="tooltip__icon">
                            {!! SiteHelpers\SvgSpritemap::get($item['icon']) !!}
                        </div>
                        @if ($item['underline'] ?? false)
                            <a {{ isset($item['link']) ? 'href=' . $item['link'] : null }} {{ isset($item['mfp-type']) && isset($item['mfp']) ? 'data-mfp=' . $item['mfp-type'] . ' data-mfp-src=' . $item['mfp'] : null }} class="link link--dark {{ isset($item['mfp-type']) && isset($item['mfp']) ? 'js-init' : null }} link--underline tooltip__link" {{ isset($item['blank']) && $item['blank'] ? 'target=_blank' : null }}>
                                {{$item['name']}}
                            </a>
                        @else
                            <a {{ isset($item['link']) ? 'href=' . $item['link'] : null }} {{ isset($item['mfp-type']) && isset($item['mfp']) ? 'data-mfp=' . $item['mfp-type'] . ' data-mfp-src=' . $item['mfp'] : null }} class="link tooltip__link {{ isset($item['mfp-type']) && isset($item['mfp']) ? 'js-init' : null }}" {{ isset($item['blank']) && $item['blank'] ? 'target=_blank' : null }}>
                                {{$item['name']}}
                            </a>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
