<div class="">
    <div class="_flex _flex-column _xs-flex-row _items-center _justify-center _xs-justify-between">
        <div class="grid grid--1 grid--def-auto _items-center _ms-flex-nowrap _mb-def _xs-mb-none _def-mb-none">
            <div class="gcell _flex _justify-center _xs-justify-start _xs-pr-sm _sm-pr-def _text-center _ms-text-right _def-text-center">
                {!! Widget::show('logo') !!}
            </div>

            @if (!config('db.basic.mobile_show_phone_hide_slogan', false) || \WezomAgency\Browserizr::detect()->isDesktop())
                <div class="gcell _text-center _ms-text-left">
                    @component('site._widgets.elements.slogan.slogan')
                        @slot('content'){!! config('db.basic.slogan_'.Lang::getLocale()) !!} @endslot
                    @endcomponent
                </div>
            @endif
        </div>
        <div class="_no-print">
            {!! Widget::show('contacts', 'header') !!}
        </div>
    </div>
</div>
