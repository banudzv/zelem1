<div class="_ptb-sm _def-ptb-def">
    <div class="_flex _items-center _justify-center _def-justify-between">
        <div class="grid grid--1 grid--def-auto _items-center _flex-nowrap">
            <div class="gcell _pr-sm _sm-pr-def _def-show _hide-mobile">
                {!! Widget::show('logo') !!}
            </div>
            @if (!config('db.basic.mobile_show_phone_hide_slogan', false) || \WezomAgency\Browserizr::detect()->isDesktop())
                <div class="gcell">
                    @component('site._widgets.elements.slogan.slogan')
                        @slot('content'){!! config('db.basic.slogan_'.Lang::getLocale()) !!} site._widgets.elements.slogan.slogan@endslot
                    @endcomponent
                </div>
            @endif
        </div>
        {!! Widget::show('contacts', 'header') !!}
    </div>
</div>
