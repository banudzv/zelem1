<div class="grid grid--auto _items-center">
    <div class="gcell _xs-mr-xs _pr-sm">
        <div role="button" tabindex="0" class="menu-button" title="Корзина"
             data-mmenu-opener="mobile-menu">
            <div class="menu-button__icon">
                @include('site._widgets.elements.hamburger.hamburger')
            </div>
        </div>
    </div>
    <div class="gcell _ms-show">
        {!! Widget::show('logo-mobile') !!}
    </div>
    {{-- {!! Widget::show('mobile-top-compare-button') !!} --}}
    {{-- {!! Widget::show('mobile-top-wishlist-button') !!} --}}
    {!! Widget::show('products::search-bar-mobile') !!}
    {{--    {!! Widget::show('mobile-top-search-button') !!}--}}
    {!! Widget::show('mobile-top-cart-button') !!}
    {{-- {!! Widget::show('mobile-top-profile-button') !!} --}}
</div>
