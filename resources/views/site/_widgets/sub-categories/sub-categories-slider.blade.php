<div data-slick-slider="{{ json_encode([ 'type' => 'SlickSubCategories', 'user-type-options' => [] ]) }}"
     class="subcategories js-init">
    <div class="subcategories__inner" data-slick-slider>
        @php($i=0)
        @while($i < 10)
            @include('site._widgets.sub-categories.sub-category-item')
            @php($i++)
        @endwhile
    </div>
</div>
