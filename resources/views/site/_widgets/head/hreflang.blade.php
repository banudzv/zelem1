@foreach(config('languages', []) as $lang)
    @if($lang->is_current)
        <link rel="alternate" href="{!! request()->fullUrl() !!}" hreflang="{!! $lang->google_slug !!}">
    @else
        <link rel="alternate" href="{!! $lang->current_url_with_new_language !!}" hreflang="{!! $lang->google_slug !!}">
    @endif
@endforeach
