<meta charset="UTF-8">
<meta name="nosnippet">
@if(isProd() === false)
    <meta name="robots" content="noindex, nofollow"/>
@endif
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>{!! Seo::site()->getTitle() !!}</title>
@if(Seo::site()->getHideDescriptionKeywords() === false)
    <meta name="description" content='{!! Seo::site()->getDescription() !!}'>
    <meta name="keywords" content='{!! Seo::site()->getKeywords() !!}'>
@endif
@if(Seo::site()->getCanonical())
    <link rel="canonical" href="{{ Seo::site()->getCanonical() }}"/>
@endif
@include('site._widgets.head.hreflang')
@stack('meta-next-prev')
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="address=no">
<meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, viewport-fit=cover">
<meta property="og:title" content="{!! Seo::site()->getTitle() !!}">
<meta property="og:type" content="website">
<meta property="og:site_name" content="{{ env('APP_NAME') }}">
<meta property="og:image" content="{{ url('storage/logo.png') }}">
<meta property="og:image:width" content="151" />
<meta property="og:image:height" content="151" />
<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:description" content="{!! Seo::site()->getDescription() !!}">
@include('site._widgets.head.head-favicons.head-favicons')
@include('site._widgets.head.head-styles.head-styles')
