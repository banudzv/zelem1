@php
/** @var string $main */
/** @var string $mainLighten */
/** @var string $mainDarken */
/** @var string $secondary */
/** @var string $secondaryLighten */
/** @var string $secondaryDarken */

/** @var string $mainHeaderFooter */
/** @var string $secondaryHeaderFooter */
/** @var string $brandsHover */
/** @var string $vendorCode */
/** @var string $itemHover */
/** @var string $categoriesHover */
@endphp

<style id="color-panel">
	{{--  @todo выводить дополнительные кастомные цвета	--}}
	:root {
		--color-main: {{ $main }};
		--color-main-lighten: {{ $mainLighten }};
		--color-main-darken: {{ $mainDarken }};
		--color-secondary: {{ $secondary }};
		--color-secondary-lighten: {{ $secondaryLighten }};
		--color-secondary-darken: {{ $secondaryDarken }};
		--color-main-header-footer: {{ $mainHeaderFooter }};
		--color-secondary-header-footer: {{ $secondaryHeaderFooter }};
		--color-vendor-code: {{ $vendorCode }};
		--color-brands-hover: {{ $brandsHover }};
		--color-item-hover: {{ $itemHover }};
		--color-categories-hover: {{ $categoriesHover }};
	}
</style>
