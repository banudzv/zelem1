{{--@if($category)--}}
    <div class="tree__level tree__level--{{ $i }}">
        <div class="tree__item tree__item--{{ $i }}">
            {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-left-thin', [
                'class' => 'tree__icon'
            ]) !!}
            <div class="_ellipsis _flex-grow">
                <a {!! Html::attributes([
                    'class' => 'tree__link',
                    'title' => '123',
                    'href' => 'dssadsada',
                ]) !!}>text text</a>
            </div>
        </div>
{{--        @if($hasNext)--}}
{{--            @include('categories::site.widgets.catalog-tree.items', [--}}
{{--                'categories' => $categories,--}}
{{--                'i' => $i + 1,--}}
{{--            ])--}}
{{--        @endif--}}
    </div>
{{--@endif--}}
