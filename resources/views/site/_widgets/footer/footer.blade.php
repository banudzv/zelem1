@php
    $logo_image = false;

    $instagram = config('db.social.instagram');
    $facebook = config('db.social.facebook');
    $twitter = config('db.social.twitter');
    $youtube = config('db.social.youtube');
@endphp

@if(browserizr()->isDesktop())
    <div class="section section--footer-top _def-show">
        <div class="container">
            <div class="grid grid--1 _nml-xl  _justify-between">
                <div class="gcell gcell--md-4 _pl-xl">
                    <div class="_flex _items-center">
                        {{-- {!! Widget::show('logo', ) !!} --}}
                        @include('site._widgets.elements.logo.logo', ['theme' => 'dark', 'mod_classes' => 'logo--footer'])
                        <div class="_lg-show">
                            @component('site._widgets.elements.slogan.slogan', ['mod' => '_color-dark-blue'])
                                @slot('content'){!! config('db.basic.slogan_'.Lang::getLocale()) !!}@endslot
                            @endcomponent
                        </div>
                    </div>
                    @if( $instagram || $facebook || $twitter || $youtube)
                        <div class="grid _nml-def _mt-md _mb-xl">
                            @if($instagram)
                                <div class="gcell _pl-def">
                                    <a href="{{ $instagram }}" class="social-icon">
                                        {!! SiteHelpers\SvgSpritemap::get('icon-instagram') !!}
                                    </a>
                                </div>
                            @endif
                            @if($facebook)
                                <div class="gcell _pl-def">
                                    <a href="{{ $facebook }}" class="social-icon">
                                        {!! SiteHelpers\SvgSpritemap::get('icon-facebook') !!}
                                    </a>
                                </div>
                            @endif
                            @if($twitter)
                                <div class="gcell _pl-def">
                                    <a href="{{ $twitter }}" class="social-icon">
                                        {!! SiteHelpers\SvgSpritemap::get('icon-twitter') !!}
                                    </a>
                                </div>
                            @endif
                            @if($youtube)
                                <div class="gcell _pl-def">
                                    <a href="{{ $youtube }}" class="social-icon">
                                        {!! SiteHelpers\SvgSpritemap::get('icon-youtube') !!}
                                    </a>
                                </div>
                            @endif
                        </div>
                    @endif
                    {{-- @if(config('db.orders.show-delivery', true) || config('db.orders.show-payment', true)) --}}
                    <div class="_mb-def _pt-lg">
                        <div class="grid _items-center _mb-md">
                            <div class="gcell _pr-xl">
                                    <img class="js-lazyload"
                                         src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='112' height='36'%3E%3C/svg%3E"
                                         data-zzload-source-img="{{ site_media('/static/images/payment-methods/nova-poshta-dark.png') }}"
                                         width="100"
                                         height="36"
                                         alt="Nova Poshta">

                            </div>
                            <div class="gcell _pr-xs">
                                <div class="payment-methods">
                                    <img class="js-lazyload"
                                         src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='120' height='25'%3E%3C/svg%3E"
                                         data-zzload-source-img="{{ site_media('/static/images/payment-methods/visa.png') }}"
                                         width="51" height="17" alt="Visa">
                                </div>
                            </div>
                            <div class="gcell _pr-xs">
                                <div class="payment-methods">
                                    <img class="js-lazyload"
                                         src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='120' height='36'%3E%3C/svg%3E"
                                         data-zzload-source-img="{{ site_media('/static/images/payment-methods/mastercard.png') }}"
                                         width="41" height="32" alt="MasterCard">
                                </div>
                            </div>
                            <div class="gcell _pr-xs">
                                <div class="payment-methods">
                                    <img class="js-lazyload"
                                         src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='120' height='36'%3E%3C/svg%3E"
                                         data-zzload-source-img="{{ site_media('/static/images/payment-methods/liqpay2.png') }}"
                                         width="41" height="32" alt="MasterCard">
                                </div>
                            </div>
                        </div>
                        <div class="grid _items-center">
                            {{-- @if(config('db.orders.show-payment', true) && (config('db.liqpay.public-key') && config('db.liqpay.public-key')))
                                <div class="gcell _p-sm">
                                    <img class="js-lazyload" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='120' height='25'%3E%3C/svg%3E" data-zzload-source-img="{{ site_media('/static/images/payment-methods/liqpay.png') }}" width="90" height="19" alt="Liqpay">
                                </div>
                            @endif
                            @if(config('db.orders.show-delivery', true))
                                <div class="gcell _p-sm">
                                    <img class="js-lazyload" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='112' height='36'%3E%3C/svg%3E" data-zzload-source-img="{{ site_media('/static/images/payment-methods/nova-poshta.png') }}" width="112" height="36" alt="Nova Poshta">
                                </div>
                            @endif --}}
                        </div>
                        {{-- @if(config('db.orders.show-payment', true) && (config('db.liqpay.public-key') && config('db.liqpay.public-key')))
                            <div class="grid _nm-sm _items-center">
                                <div class="gcell _p-sm">
                                    <img class="js-lazyload" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='120' height='25'%3E%3C/svg%3E" data-zzload-source-img="{{ site_media('/static/images/payment-methods/visa.png') }}" width="51" height="17" alt="Visa">
                                </div>
                                <div class="gcell _p-sm">
                                    <img class="js-lazyload" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='112' height='36'%3E%3C/svg%3E" data-zzload-source-img="{{ site_media('/static/images/payment-methods/mastercard.png') }}" width="41" height="32" alt="MasterCard">
                                </div>
                            </div>
                        @endif --}}
                    </div>
                    {{-- @endif --}}
                    {{-- @if(Route::has('site.sitemap'))
                        <div class="_mb-def">
                            <a href="{{ route('site.sitemap') }}" class="link link--custom text text--size-13">@lang('global.sitemap')</a>
                        </div>
                    @endif --}}
                </div>
                <div class="gcell gcell--md-7 _pl-def">
                    <div class="grid grid--1 grid--def-3 _nml-def _lg-nml-lg _pt-def">
                        {!! Widget::show('footer-menu', 'footer', 'categories-footer') !!}
                        {!! Widget::show('footer-menu') !!}
                        {!! Widget::show('contacts', 'footer') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(config('db.basic.copyright_'.\Lang::getLocale()))
        <div class="section _bgcolor-background _pt-def _def-show">
            <div class="container">
                <hr class="_bgcolor-dark-blue">
                <div class="grid _justify-between _items-center _pt-md _pb-lg">
                    <div class="gcell _def-pl-none _pl-xl">
                        <div class="text text--size-13 text--semi-bold _color-dark-blue">
                            <p>
                                @if(config('db.basic.copyright_'.\Lang::getLocale()))
                                    &copy; {{ config('db.basic.copyright_'.\Lang::getLocale()) }}
                                @else

                                @endif
                            </p>
                        </div>
                    </div>

                    @if(env('DEVELOP_LINK', true))
                    <div class="gcell">
                        <a href="//locotrade.com.ua" target="_blank" class="link link--invert _color-dark-blue text text--size-13">
                            @lang('locotrade.locotrade_created_on')
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
@else
    <div class="section section--footer-mobile _def-hide _ptb-md">
       <div class="container">
    {{--       <div class="section _flex _items-center _flex-column _text-center">--}}
    {{--           {!! Widget::show('contacts', 'mobile-footer') !!}--}}
    {{--       </div>--}}
           <div class="footer-accordion js-init" data-accordion="{{ json_encode(['type' => 'multiple', 'slideTime' => 300]) }}">
               @foreach(config('site_menu.mobile-footer', []) as $menu)
                   @php
                       $menuWidget = new \App\Modules\SiteMenu\Widgets\FooterMenu('mobile-footer', $menu);
                   @endphp
                   @if($menuWidget->getMenu()->hasKids())
                       <div class="footer-accordion__body">
                           <div class="footer-accordion__title"
                                data-wstabs-button="{{ $loop->iteration }}"
                                data-wstabs-ns="MobileFooter"
                           >
                               @lang('site_menu::site.menu-title.' . $menu)
                               <div class="footer-accordion__icon">
                                   {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-bottom') !!}
                               </div>
                           </div>
                           <div class="footer-accordion__content" style="display: none;"
                                data-wstabs-block="{{ $loop->iteration }}"
                                data-wstabs-ns="MobileFooter"
                           >
                               {!! $menuWidget->render() !!}
                           </div>
                       </div>
                   @endif
               @endforeach
           </div>
           {!! Widget::show('contacts', 'mobile-footer') !!}
           @if( $instagram || $facebook || $twitter || $youtube)
               <div class="grid _nml-xs _mb-xl">
                   @if($instagram)
                       <div class="gcell _pl-xs">
                           <a href="{{ $instagram }}" class="social-icon">
                               {!! SiteHelpers\SvgSpritemap::get('icon-instagram') !!}
                           </a>
                       </div>
                   @endif
                   @if($facebook)
                       <div class="gcell _pl-xs">
                           <a href="{{ $facebook }}" class="social-icon">
                               {!! SiteHelpers\SvgSpritemap::get('icon-facebook') !!}
                           </a>
                       </div>
                   @endif
                   @if($twitter)
                       <div class="gcell _pl-xs">
                           <a href="{{ $twitter }}" class="social-icon">
                               {!! SiteHelpers\SvgSpritemap::get('icon-twitter') !!}
                           </a>
                       </div>
                   @endif
                   @if($youtube)
                       <div class="gcell _pl-xs">
                           <a href="{{ $youtube }}" class="social-icon">
                               {!! SiteHelpers\SvgSpritemap::get('icon-youtube') !!}
                           </a>
                       </div>
                   @endif
               </div>
           @endif

       @if(config('db.basic.copyright_'.\Lang::getLocale()))
               <div class="grid _flex-row _justify-start _items-center _text-center">
                   <div class="gcell">
                       <div class="text text--size-13 text--semi-bold _color-blue-smoke">
                               @if(config('db.basic.copyright_'.\Lang::getLocale()))
                               &copy; {{ config('db.basic.copyright_'.\Lang::getLocale()) }}
                               @else
                               @endif
                       </div>
                   </div>
                   @if(env('DEVELOP_LINK', true))
                       <div class="gcell _ml-md">
                           <a href="//locotrade.com.ua" target="_blank" class="text text--size-13 text--semi-bold _color-blue-smoke">
                               @lang('locotrade.locotrade_created_on')
                           </a>
                       </div>
                   @endif
               </div>
           @endif
       </div>
    </div>
@endif

{!! Widget::show('seo-metrics', 'counter') !!}
