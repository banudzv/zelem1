@if(config('db.basic.copyright_'.\Lang::getLocale()))
    <div class="section _bgcolor-background _ptb-md _no-print">
        <div class="container">
            <div class="grid _justify-between _items-center">
                <div class="gcell">
                    <div class="text text--size-13 text--semi-bold _color-dark-blue">
                        <p>
                            @if(config('db.basic.copyright_'.\Lang::getLocale()))
                            &copy; {{ config('db.basic.copyright_'.\Lang::getLocale()) }}
                            @else
                                &nbsp;
                            @endif
                        </p>
                    </div>
                </div>
                <div class="gcell">
                    <a href="//locotrade.com.ua" target="_blank" class="link link--invert _color-dark-blue text text--size-13">@lang('locotrade.locotrade_created_on')</a>
                </div>
            </div>
        </div>
    </div>
@endif
{!! Widget::show('seo-metrics', 'counter') !!}
