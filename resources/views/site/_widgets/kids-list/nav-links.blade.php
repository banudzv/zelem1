@component('products::site.widgets.accordion.accordion', [
    'options' => [
        'type' => 'multiple',
    ],
])
    <hr class="separator _color-gray2 _mtb-def _hide-last _hide-mobile _def-show">
    @component('products::site.widgets.filter-accordion.accordion-block', [
        'ns' => 'filter',
        'id' => 0,
        'header' => __('categories::site.categories-list'),
        'open' => false,
    ])
        @php($i = 0)
        <ul class="nav-links">
            @while($i < 10)
                <li class="nav-links__item">
                    <a class="nav-links__link" href="#!">Ссылка</a>
                </li>
                @php($i++)
            @endwhile
        </ul>
    @endcomponent
@endcomponent
