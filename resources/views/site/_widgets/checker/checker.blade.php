<label class="checker checkbox--simple {{ $disabled ?? null }} {{ $class ?? null }}">
    <input class="checker__input" {!! Html::attributes($attributes ?? []) !!}>
    @if(isset($slot) && trim(strip_tags($slot)))
        <span class="checker__text">
            {{ $slot }}
        </span>
    @endif
</label>
