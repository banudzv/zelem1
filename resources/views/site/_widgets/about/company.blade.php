<div class="section">
    <div class="container _plr-xl _mt-def _md-mt-xl _def-pl-xxl">
        <div class="about-company">
            <div class="grid grid--1 grid--md-2 _def-pl-xl _justify-between">
                <div class="gcell">
                    <div class="title title--size-h3 title--normal _color-dark-blue">
                        О Компании
                    </div>
                    <div class="text _color-dark-blue _mt-def _mb-sm">
                        Миссия zalem
                    </div>
                    <div class="about-company__text _color-blue-smoke">
                        Сделать процесс восстановления турбины максимально быстрым и доступным для автовладельцев. Обеспечить безупречное качество и долгосрочность результата ремонта.
                    </div>
                    <div class="about-company__list">
                        <div class="about-company__item">
                            {!! SiteHelpers\SvgSpritemap::get('icon-available') !!}
                            Открываем новые филиалы в регионах чтобы сделать ремонт турбины быстрее и доступнее
                        </div>
                        <div class="about-company__item">
                            {!! SiteHelpers\SvgSpritemap::get('icon-available') !!}
                            Используем инновационные подходы в диагностике и регенерации турбин
                        </div>
                        <div class="about-company__item">
                            {!! SiteHelpers\SvgSpritemap::get('icon-available') !!}
                            Осуществляем ремонт любой сложности всех типов турбин, актуаторов, ТНВД и форсунок
                        </div>
                        <div class="about-company__item">
                            {!! SiteHelpers\SvgSpritemap::get('icon-available') !!}
                            Чиним все виды транспортных средств где есть турбина
                        </div>
                    </div>
                    <div class="grid grid--1 grid--xs-2 grid--xl-auto _pt-lg _nmt-sm _nml-sm">
                        <div class="gcell _pl-sm _pt-sm">
                            <div class="about-company__info">
                                <div class="about-company__counter">
                                    536
                                </div>
                                <div class="text _text-center _color-blue-smoke">
                                    536 турбин ремонтирующихся ежемесячно
                                </div>
                            </div>
                        </div>
                        <div class="gcell _pl-sm _pt-sm">
                            <div class="about-company__info">
                                <div class="about-company__counter">
                                    6 487
                                </div>
                                <div class="text _text-center _color-blue-smoke">
                                    6 487 специалистов по всей Украине
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gcell _plr-lg _md-pr-none _text-center _mt-xl _def-mt-none _def-text-right _def_nmr-md">
                    <img src="static/images/about-company/about-company.png" alt="about-company">
                </div>
            </div>
        </div>
    </div>
</div>
