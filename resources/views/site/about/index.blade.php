@php
    $info = config('mock.info')
@endphp

@extends('site._layouts.main')

@section('layout-body')
    <div class="section about-page">
        <div class="container container--white container--small">
            <div class="_mb-xl wysiwyg">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati officiis, rerum. At, corporis debitis distinctio dolore incidunt labore molestias nulla praesentium quibusdam, ratione reiciendis repellat sed temporibus, ut veritatis vero?
            </div>
            <div class="grid grid--1 grid--def-2 _ptb-xl _flex-column-reverse _def-flex-row">
                <div class="gcell _pt-lg _def-pr-lg">
                    <div class="wysiwyg">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at delectus
                        distinctio, impedit
                        iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus
                        vitae. Officia,
                        recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at
                        delectus distinctio, impedit
                        iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus
                        vitae. Officia,
                        recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at
                        delectus distinctio, impedit
                        iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus
                        vitae. Officia,
                        recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at
                        delectus distinctio, impedit
                        iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus
                        vitae. Officia,
                        recusandae!
                    </div>
                    <div class="_flex _justify-center _def-justify-start _items-center _pt-lg _ms-nmlr-md _flex-column _items-stretch _ms-flex-row">
                        @if(isset($info) && $info)
                            @foreach($info as $infoItem)
                                @if($loop -> index <= 1)
                                    <div
                                        class="about-company__info about-company__info--about-page _mlr-none _mtb-md _def-mtb-none _ms-mlr-md">
                                        @if(isset($infoItem -> counter) && $infoItem -> counter)
                                            <div class="about-company__counter">
                                                {{ $infoItem -> counter }}
                                            </div>
                                        @endif
                                        @if(isset($infoItem -> icon) && $infoItem -> icon)
                                            <div class="about-company__icon">
                                                {!! \SiteHelpers\SvgSpritemap::get( $infoItem->icon , [
                                                        'fill' => '#47B39C',
                                                        'class' => ''
                                                ]) !!}
                                            </div>
                                        @endif
                                        <div class="text _text-center _color-blue-smoke">
                                            {{ $infoItem -> text ?? null }}
                                        </div>
                                    </div>
                                @endif

                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="gcell _pb-lg">
                    <img src="/public/site/static/images/image-about.png"
                         class="about-page__image _mlr-auto" alt="">
                </div>
            </div>
            <div class="grid grid--1 grid--def-2 _ptb-xxl _flex-column _def-flex-row">
                <div class="gcell _pb-lg">
                    <img src="/public/site/static/images/image-about-2.png"
                         class="about-page__image _ml-auto _def-ml-none _mr-auto"
                         alt="">
                </div>
                <div class="gcell _pt-lg _def-pl-lg">
                    <div class="wysiwyg">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at delectus
                        distinctio,
                        impedit
                        iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus
                        vitae.
                        Officia,
                        recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at
                        delectus
                        distinctio, impedit
                        iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus
                        vitae.
                        Officia,
                        recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at
                        delectus
                        distinctio, impedit
                        iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus
                        vitae.
                        Officia,
                        recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at
                        delectus
                        distinctio, impedit
                        iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus
                        vitae.
                        Officia,
                        recusandae!
                    </div>

                    <div class="_flex _pt-xxl _justify-center _items-center _flex-column _ms-nmlr-md _items-stretch _ms-flex-row">
                        @if(isset($info) && $info)
                            @foreach($info as $infoItem)
                                @if($loop -> index > 1)
                                    <div
                                        class="about-company__info about-company__info--about-page _mlr-none _mtb-md _def-mtb-none _ms-mlr-md">
                                        @if(isset($infoItem -> counter) && $infoItem -> counter)
                                            <div class="about-company__counter">
                                                {{ $infoItem -> counter }}
                                            </div>
                                        @endif
                                        @if(isset($infoItem -> icon) && $infoItem -> icon)
                                            <div class="about-company__icon">
                                                {!! \SiteHelpers\SvgSpritemap::get( $infoItem->icon , [
                                                        'fill' => '#47B39C',
                                                        'class' => ''
                                                ]) !!}
                                            </div>
                                        @endif
                                        <div class="text _text-center _color-blue-smoke">
                                            {{ $infoItem -> text ?? null }}
                                        </div>
                                    </div>
                                @endif

                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <div class="_pt-xl _pb-xxl wysiwyg">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at
                delectus distinctio,
                impedit
                iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus vitae.
                Officia,
                recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at delectus
                distinctio, impedit
                iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus vitae.
                Officia,
                recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at delectus
                distinctio, impedit
                iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus vitae.
                Officia,
                recusandae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci at delectus
                distinctio, impedit
                iste labore, laborum minima molestias nam neque nihil nobis quam quod recusandae, temporibus vitae.
                Officia,
                recusandae!
            </div>
            {!! Widget::show('advantage::show') !!}
        </div>
    </div>
@endsection
