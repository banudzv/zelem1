@extends('site._layouts._base')

@section('body')
    <div id="layout" class="layout layout--site {{ isset($wide) && $wide ? 'layout--wide' : '' }}">
        <div class="layout__body">
            @include('site._widgets.header.header')
            {!! Widget::show('breadcrumbs') !!}
            @if(!isset($hideH1) || $hideH1 === false)
                {!! Widget::show('h1-brands', $centerH1 ?? false) !!}
            @endif
            @yield('layout-body')
        </div>

        <div class="layout__footer">
            {{-- {!! Widget::show('subscribe::form') !!} --}}
            @if (Route::currentRouteName() !== 'site.home')
                {!! Widget::show('seo-block') !!}
            @endif
            {!! Widget::show('social_buttons::callback') !!}
            @include('site._widgets.footer.footer')
        </div>

        @include('site._widgets.fixed-menu.fixed-menu')
    </div>

    @include('site._widgets.scripts.scripts')
    @include('site._widgets.scripts.noscript')
    @include('site._widgets.hidden-data.index')
@endsection
