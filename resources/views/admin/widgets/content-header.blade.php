@php
$h1 = Seo::meta()->getH1();
if (Lang::has($h1)) {
	$h1 = __($h1);
}
$countOfEntity = Seo::meta()->getCountOfEntity();
if (Lang::has($countOfEntity)) {
	$countOfEntity = __($countOfEntity);
}
@endphp

<section class="content-header">
	<h1>@yield('h1', $h1)</h1>
	<h4>@yield('h4',$countOfEntity)</h4>
    @if(Seo::breadcrumbs()->needsToBeDisplayed())
		@include('admin.widgets.breadcrumbs')
	@endif
	<div class="text-right clearfix">{!! Widget::position('header-buttons')!!}</div>
</section>
