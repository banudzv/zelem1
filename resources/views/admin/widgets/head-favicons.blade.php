<link rel="apple-touch-icon" sizes="180x180" href="{{ site_media('static/favicons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ site_media('static/favicons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ site_media('static/favicons/favicon-16x16.png') }}">
<link rel="mask-icon" href="{{ site_media('static/favicons/safari-pinned-tab.svg') }}" color="#60bc4f">
<link rel="manifest" href="{{ site_media('static/favicons/site.webmanifest') }}">
<meta name="apple-mobile-web-app-title" content="{{ env('APP_NAME') }}">
<meta name="application-name" content="{{ env('APP_NAME') }}">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
