@php
    $_value = $value ?? 0;
    $colorClass = isset($color) ? ' progress-bar-' . $color : '';
@endphp

<div class="progress">
    <div class="progress-bar{{$colorClass}}"
         role="progressbar" aria-valuenow="{{ $_value }}"
         aria-valuemin="0" aria-valuemax="100"
         style="width: {{ $_value }}%">
        {{ $_value }}
    </div>
</div>