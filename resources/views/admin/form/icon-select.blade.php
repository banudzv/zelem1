<?php
/** @var Illuminate\Support\ViewErrorBag $errors */
/** @var CustomForm\IconSelect $element */

$error = $errors->first($element->getErrorSessionKey());
if ($error) {
    $element->addClassesToDiv('has-error');
}
?>
<div {!! Html::attributes($element->getBlockOptions()) !!}>
    @include('admin.form.components.label', ['element' => $element])
    @include('admin.form.components.icon' , ['icon' => $element->getValue() ? $element->getValue() : $element->getDefaultIcon(), 'file' => $element->getIconFile()])
    {!! Form::select($element->getName(), $element->getList(), $element->getValue(), $element->getOptions()) !!}
    @include('admin.form.components.helper', ['element' => $element])
</div>
