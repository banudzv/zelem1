<?php
/** @var Illuminate\Support\ViewErrorBag $errors */
/** @var CustomForm\Text $element */

$error = $errors->first($element->getErrorSessionKey());
if ($error) {
    $element->addClassesToDiv('has-error');
}
?>
@include('admin.form.components.label', ['element' => $element])
@if(count($element->getOptions()) and $element->getOptions()['class'])
    <div class="">
        @endif
        <?php $data = $element->getOptions(); ?>
        <input type="hidden" value="<?php echo isset($data['lat'])?$data['lat']:'';?>" name="{{ $data['input'] }}[lat]">
        <input type="hidden" value="<?php echo isset($data['lng'])?$data['lng']:'';?>" name="{{ $data['input'] }}[lng]">
        <input type="hidden" value="<?php echo isset($data['zoom'])?$data['zoom']:''; ?>" name="{{ $data['input'] }}[zoom]">

        <input type="text" id="address" class="form-control ui-autocomplete-input" placeholder="<?php echo __('Начните писать адрес...'); ?>" autocomplete="off">
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
        <div style="width: 100%; height: 600px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" id="gmap"></div>
        <?php $__env->startPush('scripts'); ?>
        <script src="https://maps.googleapis.com/maps/api/js?key={{config('db.contact.googlemaps')}}&sensor=false"
                type="text/javascript"></script>
        <script type="text/javascript">
					$('#address').val('');
					var map = false;

					var geocoder = false;

              <?php if(!isset($data['lng']) || !isset($data['lat'])): ?>
					var myLatlng = new google.maps.LatLng(48.93982088660896, 31.46484375);
					var zoom = 7;
					var marker = false;
              <?php else: ?>
					var myLatlng = new google.maps.LatLng(parseFloat('<?php echo $data['lat']; ?>'), parseFloat('<?php echo $data['lng']; ?>'));
					var zoom = parseInt($('input[name="{{ $data['input'] }}[zoom]"]').val());
					var marker = true;
          <?php endif; ?>

					function initialize() {
						var mapOptions = {
							center: myLatlng,
							zoom: zoom,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
						map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
						geocoder = new google.maps.Geocoder();
						google.maps.event.addListener(map, 'zoom_changed', function(){
							$('input[name="{{ $data['input'] }}[zoom]"]').val( map.zoom );
						});
					}

					function bind_location(location) {
						$('input[name="{{ $data['input'] }}[lat]"]').val( location.lat() );
						$('input[name="{{ $data['input'] }}[lng]"]').val( location.lng() );
						$('input[name="{{ $data['input'] }}[zoom]"]').val( map.zoom );
					}

					function placeMarker(location) {
						if (marker) {
							marker.setMap(null);
						}
						bind_location(location);
						pointt = new google.maps.Point(18, 54);
						marker = new google.maps.Marker({
							position: location,
							map: map,
							draggable: true
						});
						google.maps.event.addListener(marker, 'dragend', function(cc){
							bind_location(cc.latLng);
						});
					}

					$(document).ready(function(){
						initialize();
						google.maps.event.addListener(map, 'click', function(cc){
							placeMarker(cc.latLng);
						});

						if(marker) {
							pointt = new google.maps.Point(18, 54);
							marker = new google.maps.Marker({
								position: myLatlng,
								map: map,
								draggable: true
							});
							google.maps.event.addListener(marker, 'dragend', function(cc){
								bind_location(cc.latLng);
							});
						}

						$("#address").autocomplete({
							//Определяем значение для адреса при геокодировании
							source: function(request, response) {
								geocoder.geocode( {'address': request.term}, function(results, status) {
									response($.map(results, function(item) {
										return {
											label:  item.formatted_address,
											value: item.formatted_address,
											lat: item.geometry.location.lat(),
											lng: item.geometry.location.lng()
										}
									}));
								})
							},
							select: function(event, ui) {
								$('input[name="{{ $data['input'] }}[lat]"]').val( ui.item.lat );
								$('input[name="{{ $data['input'] }}[lng]"]').val( ui.item.lng );
								$('input[name="{{ $data['input'] }}[zoom]"]').val( map.zoom );
								var location = new google.maps.LatLng(ui.item.lat, ui.item.lng);
								map.setCenter(location);
								map.setZoom(14);
								placeMarker(location);
							}
						});
					});
        </script>
        <?php $__env->stopPush(); ?>
        @if(count($element->getOptions()) and $element->getOptions()['class'])
    </div>
@endif
