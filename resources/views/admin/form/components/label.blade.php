<?php
/** @var CustomForm\Input $element */
$text = $element->getLabel();
$params = $element->getAdditionalLabel();
if (Lang::has($text) || count($params) > 0) {
    $text = __($text,$params);
}
?>
@if($element->getLabel())
    {!!
         Form::label(
             $element->getName(),
             $text . ($element->isRequired() ? ' <span class="text-red">*</span>' : null),
             ['class' => 'control-label'],
             false
         )
    !!}
@endif
