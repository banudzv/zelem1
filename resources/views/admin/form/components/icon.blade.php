@php
    /** @var $icon string*/
@endphp
    <div class="svgIcon">
        <svg>
            <use xlink:href="{{siteMediaForAdmin('/assets/svg/' . $file, true) }}#{{$icon}}"></use>
        </svg>
    </div>

