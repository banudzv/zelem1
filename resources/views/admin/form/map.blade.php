<?php
/** @var Illuminate\Support\ViewErrorBag $errors */
/** @var CustomForm\Macro\Map $element */
?>

<div class="mapWrap" style="height: 450px;">
    <input type="text" id="address" class="form-control ui-autocomplete-input" placeholder="@lang('Начните писать адрес...')" autocomplete="off">
    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
    <div style="width: 100%; height: calc(100% - 30px); position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" id="gmap"></div>
</div>

@push('scripts')
<script src="http://maps.googleapis.com/maps/api/js?key={{ config('db.contact.googlemaps') }}&callback=initMap" type="text/javascript" async defer></script>
<script type="text/javascript">
    function initMap() {
        $('#address').val('');
        var map = false, geocoder = false, myLatlng, zoom, marker;

        @if($element->hasNoCoordinates())
            myLatlng = new google.maps.LatLng(48.93982088660896, 31.46484375);
            zoom = 6;
            marker = false;
        @else
            myLatlng = new google.maps.LatLng(parseFloat('{{ $element->getLatitude() }}'), parseFloat('{{ $element->getLongitude() }}'));
            zoom = parseInt($('input[name="zoom"]').val());
            marker = true;
        @endif

        function initialize() {
            var mapOptions = {
                center: myLatlng,
                zoom: zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
            geocoder = new google.maps.Geocoder();
            google.maps.event.addListener(map, 'zoom_changed', function () {
                $('input[name="zoom"]').val(map.zoom);
            });
        }

        function bind_location(location) {
            $('input[name="latitude"]').val(location.lat());
            $('input[name="longitude"]').val(location.lng());
            $('input[name="zoom"]').val(map.zoom);
        }

        function placeMarker(location) {
            if (marker) {
                marker.setMap(null);
            }
            bind_location(location);
            pointt = new google.maps.Point(18, 54);
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function (cc) {
                bind_location(cc.latLng);
            });
        }

        $(document).ready(function () {
            initialize();
            google.maps.event.addListener(map, 'click', function (cc) {
                placeMarker(cc.latLng);
            });

            if (marker) {
                pointt = new google.maps.Point(18, 54);
                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    draggable: true
                });
                google.maps.event.addListener(marker, 'dragend', function (cc) {
                    bind_location(cc.latLng);
                });
            }

            $("#address").autocomplete({
                //Определяем значение для адреса при геокодировании
                source: (request, response) => {
                    let result = geocoder.geocode({'address': request.term}, function (results, status) {
                        response($.map(results, function (item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                latitude: item.geometry.location.lat(),
                                longitude: item.geometry.location.lng()
                            }
                        }));
                    });
                },
                select: function (event, ui) {
                    $('input[name="latitude"]').val(ui.item.latitude);
                    $('input[name="longitude"]').val(ui.item.longitude);
                    $('input[name="zoom"]').val(map.zoom);
                    var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                    map.setCenter(location);
                    map.setZoom(14);
                    placeMarker(location);
                },
                error: error => {
                    console.log(error);
                }
            });
        });

        // if you want to respond to a specific error, you may hack the
        // console to intercept messages.
        // check if a message is a Google Map's error message and respond
        // accordingly
        (function takeOverConsole() { // taken from http://tobyho.com/2012/07/27/taking-over-console-log/
            var console = window.console
            if (!console) return

            function intercept(method) {
                var original = console[method]
                console[method] = function() {
                    // check message
                    if(arguments[0] && arguments[0].indexOf('InvalidKeyMapError') !== -1) {
                        message('Invalid Google Maps API key', 'error');
                    }

                    if (original.apply) {
                        // Do this for normal browsers
                        original.apply(console, arguments)
                    } else {
                        // Do this for IE
                        var text = Array.prototype.slice.apply(arguments).join(' ')
                        original(text)
                    }
                }
            }
            var methods = ['error']; // only interested in the console.error method
            for (var i = 0; i < methods.length; i++)
                intercept(methods[i])
        }())
    }
</script>
@endpush
