import { sectionAccordion } from 'assetsSite#/js/modules/section-accordion';

function loaderInit ($elements, moduleLoader) {
	$elements.data('moduleLoader', moduleLoader);
	sectionAccordion($elements);
}

export { loaderInit };
