import { init } from 'assetsSite#/js/modules/youtube-video';

function loaderInit ($elements, moduleLoader) {
	init($elements, moduleLoader);
}

export { loaderInit };
