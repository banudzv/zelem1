import { deliveryPrice } from 'assetsSite#/js/modules/delivery-price';

function loaderInit ($elements, moduleLoader) {
	$elements.data('moduleLoader', moduleLoader);
	deliveryPrice($elements);
}

export { loaderInit };
