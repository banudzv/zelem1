import { init } from 'assetsSite#/js/modules/categories-popup';

/**
 * @param {jQuery} $elements
 */
function loaderInit ($elements) {
	init($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

export { loaderInit };
