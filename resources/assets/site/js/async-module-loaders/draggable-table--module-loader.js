'use strict';

/**
 * Прослойка для асинхронной загрузки модуля `draggable-table`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import draggableTable from 'assetsSite#/js/modules/draggable-table';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {jQuery} $elements
 * @param {ModuleLoader} moduleLoader
 * @sourceCode
 */
function loaderInit ($elements, moduleLoader) {
	$elements.data('moduleLoader', moduleLoader);
	draggableTable($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

export { loaderInit };
