import { comparelistTable } from 'assetsSite#/js/modules/comparelist-table';

function loaderInit ($elements, moduleLoader) {
	$elements.data('moduleLoader', moduleLoader);
	comparelistTable($elements);
}

export { loaderInit };
