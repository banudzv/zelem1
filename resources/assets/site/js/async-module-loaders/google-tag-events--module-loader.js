'use strict';

/**
 * Прослойка для загрузки модуля `s2s`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import { googleEvents } from 'assetsSite#/js/modules/google-tag-events';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 * @sourceCode
 */
function loaderInit ($elements, moduleLoader) {
	googleEvents($elements, moduleLoader);
}

// ----------------------------------------
// Exports
// ----------------------------------------

export { loaderInit };
