import { optionsCheck } from 'assetsSite#/js/modules/options-check';

/**
 * @param {jQuery} $elements
 */
function loaderInit ($elements) {
	optionsCheck($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

export { loaderInit };
