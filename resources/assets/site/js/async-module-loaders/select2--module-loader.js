'use strict';

/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import { select2Init } from 'assetsSite#/js/modules/select2/select2';

/**
 * @param {jQuery} $elements
 */

function loaderInit ($elements, moduleLoader) {
	$elements.data('moduleLoader', moduleLoader);
	select2Init($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

export { loaderInit };
