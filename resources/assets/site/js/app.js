'use strict';

/**
 * @module
 * @author Oleg Dutchenko <dutchenko.o.dev@gmail.com>
 * @version 1.0.0
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import 'assetsSite#/js/polyfills/mdn';
import 'assetsSite#/js/polyfills/promise-polyfill';
import 'assetsSite#/js/modules/modernizr';
import 'assetsSite#/js/ajax-global-config';
import wsTabs from 'wezom-standard-tabs';
import moduleLoader from 'assetsSite#/js/async-module-loader';
import { lazyload } from 'assetsSite#/js/modules/lazyload';

// ----------------------------------------
// Private
// ----------------------------------------

const $window = window.jQuery(window);
const $document = window.jQuery(document);
const $html = window.jQuery(document.documentElement);

const _statusClasses = {
	ready: '_document-is-ready',
	load: '_window-is-load'
};

// ----------------------------------------
// Ready
// ----------------------------------------

window.jQuery(function () {
	wsTabs.init();
	require('assetsSite#/js/_site-modules/all.js');
	moduleLoader.init($document);
	$html.addClass(_statusClasses.ready);

	lazyload().then(observer => observer.observe());

	$document.on('click', '[data-print]', (e) => {
		if (e.currentTarget.tagName.toLocaleLowerCase() === 'a' && e.currentTarget.hasAttribute('href')) {
			return true;
		}

		if ($(e.currentTarget).data('print') === 'container') {
			$html.addClass('is-print-container');
		}

		window.print();
	});

	$('.scroll-text__content').each(function () {
		toggleShadow($(this));
		$(this).on('scroll resize', function () {
			toggleShadow($(this));
		});
	});

	function toggleShadow (el) {
		if (el.scrollTop() + el.innerHeight() >= el[0].scrollHeight) {
			el.closest('.scroll-text').addClass('hide-shadow');
		} else {
			el.closest('.scroll-text').removeClass('hide-shadow');
		}
	}

	$document.on('input', 'textarea', (e) => {
		let textarea = e.currentTarget;
		let offset = textarea.offsetHeight - textarea.clientHeight;

		textarea.style.height = 'auto';
		textarea.style.height = `${textarea.scrollHeight + offset}px`;
	});

	$('.js-filter-show').on('click', () => {
		$html.addClass('is-filter-show');
	});

	$('.js-filter-close').on('click', () => {
		$html.removeClass('is-filter-show');
	});
});

$('body').on('change', '.js-filter-item', function () {
	let _this = $(this);
	let id = _this.data('id');
	let form = _this[0].form;
	let data = new FormData(form); // eslint-disable-line no-undef
	let info = {};
	let engines = {};

	$('.uniq-class').find('input').not('[data-ion-prices]').prop('disabled', true);

	data.delete('_token');
	data = [...data.entries()];

	data.forEach(function (el) {
		if (info.hasOwnProperty(el[0])) {
			info[el[0]].push(el[1]);
		} else if (el[1].length) {
			info[el[0]] = [el[1]];
		}
	});

	let query = decodeURIComponent(new URLSearchParams(info).toString());

	engines[id] = _this.find('option:selected').val();

	$('.show-filter').toggleClass('show', isFilterChanged(query)).attr('href', window.location.origin + window.location.pathname + '?' + query);

	if ($(this).hasClass('js-select-filter')) {
		let newUrl = $(this).data('url');
		$.ajax({
			type: 'POST',
			url: newUrl,
			dataType: 'json',
			data: engines,
			timeout: 600000,
			success: function (data) {
				let select = ['<option value="" name="">Все</option>'];

				if (data.options) {
					Object.entries(data.options).forEach(([key, option]) => {
						select.push('' + '<option value="' + option.id + '" name="' + option.name + '">' + option.name + '</option>');
					});
				}
				$('.js-engine-filter').each(function () {
					if ($(this).data('find') === data.name) {
						$(this).html(select);
						$(this).closest('.select-wrap').addClass('visible');
					} else if (data.name === 'models') {
						if ($(this).data('find') !== 'models' && $(this).data('find') !== 'mark') {
							$(this).closest('.select-wrap').removeClass('visible');
							$(this).html('<option value="" name="">Все</option>');
						} else {
							$(this).closest('.select-wrap').addClass('visible');
						}
					}
				});
			}
		});
	}
});

$('.js-filter-item').first().trigger('change');

function isFilterChanged (query) {
	let location = window.location.href.split('?');
	return !(location.length > 1 && location[location.length - 1] === query.toString());
}

// ----------------------------------------
// Load
// ----------------------------------------

$window.on('load', function () {
	$html.addClass(_statusClasses.load);
});

window.onafterprint = function () {
	$('html').removeClass('is-print-container');
};
