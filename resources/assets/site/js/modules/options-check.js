export function optionsCheck ($elements) {
	var services = [];
	$elements.on('click', function (event) {
		if (event.target.hasAttribute('data-checker') || event.target.closest('[data-checker]')) {
			$(event.target).parents('[data-option]').attr('data-checked', $(event.target).prop('checked'));
			checkActiveServices();
		}
		event.stopPropagation();
	});
	$elements.each(function () {
		const $selects = $(this).find('[data-select]');
		$selects.each(function () {
			const $select = $(this);
			$select.on('change', function () {
				$(this).parents('[data-option]').attr('data-option-id', $(this).prop('value'));
				$(this).parents('[data-option]').find('[data-option-price]').text($(this).find('option:selected').data('price'));
				checkActiveServices();
			});
		});
	});
	function checkActiveServices () {
		services = [];
		$.each($elements.find('[data-option]'), function () {
			if ($(this).attr('data-checked') === 'true') {
				services.push(parseInt($(this).attr('data-option-id'), 10));
			}
		});
		$elements.data('product-options', services);
	}
}
