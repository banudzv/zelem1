export function init ($elements) {
	$elements.each(function () {
		let toggler = false;
		$(this).on('click', function (event) {
			var disableScroll = toggleScroll('disable');
			var enableScroll = toggleScroll('enable');
			if (event.target.hasAttribute('data-categories-popup-trigger') || event.target.closest('div[data-categories-popup-trigger]')) {
				if (!toggler) {
					$(this).find('[data-categories-popup]').addClass('is-active');
					disableScroll();
				} else {
					$(this).find('[data-categories-popup]').removeClass('is-active');
					enableScroll();
				}
				toggler = !toggler;
			}
		});
	});

	function toggleScroll (param) {
		return function () {
			switch (param) {
				case 'disable' :
					$('html').addClass('no-scroll');
					break;
				case 'enable' :
					$('html').removeClass('no-scroll');
					break;
			}
		};
	}
}
