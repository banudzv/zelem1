export function comparelistTable ($elements) {
	const $window = $(window);
	$elements.each(function () {
		const $element = $(this);
		let rowCount = $element.find('[data-row-count]').data('row-count');
		$.when(alignTable($element, rowCount)).then(() => $element.removeClass('is-loading'));
		$window.on('compareTableUpdate resize', function () {
			alignTable($element, rowCount);
		});
	});
}

export function alignTable ($element, rowCount) {
	$('[data-row]').css('height', 'unset');
	let i = 1;
	while (i <= rowCount) {
		let rowHeight = 0;
		$element.find(`[data-row=${i}]`).each(function () {
			if (rowHeight < $(this).outerHeight()) {
				rowHeight = $(this).outerHeight();
			}
		}).outerHeight(rowHeight);
		i++;
	}
}
