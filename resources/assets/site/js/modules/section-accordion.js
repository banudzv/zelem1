export function sectionAccordion ($elements) {
	$elements.each(function () {
		var hidden = true;
		$(this).find('[data-toggle-accordion]').on('click', function () {
			var $parent = $(this).parent();
			if (hidden) {
				$parent.find('[data-section-hidden]').show();
				$parent.find('[data-when-closed]').hide();
				$parent.find('[data-when-opened]').show();
				hidden = false;
			} else {
				$parent.find('[data-section-hidden]').hide();
				$parent.find('[data-when-closed]').show();
				$parent.find('[data-when-opened]').hide();
				hidden = true;
			}
		}
		);
	});
}
