import $ from 'jquery';

export function init ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('youtube-video-initialized')) {
			return true;
		}

		_init($el);
	});
}

function _init ($el) {
	const id = $el.data('video');
	const $thumbnail = $el.find('[data-thumbnail]');
	const $replaceButton = $el.find('[data-replace-with-video]');

	$replaceButton.on('click', () => {
		const $frame = `<div class="youtube-card__frame">
				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/${id}?&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>`;
		$thumbnail.html($frame);
	});
}
