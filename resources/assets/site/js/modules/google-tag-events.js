/* eslint-disable no-undef */
export function googleEvents ($elements) {
	$elements.on('click', '[data-trigger-event]', function (event) {
		const $this = $(this);
		const eventType = $this.data('trigger-event');
		const productObj = $this.parents('[data-product]').data('product') || null;
		const $cartCheckout = $('[data-cart-checkout]');
		const $wishlistProducts = $('[data-wishlist-products]');
		switch (eventType) {
			case 'productClick' :
				dataLayer.push({
					'event': 'productClick',
					'ecommerce': {
						'currencyCode': 'UAH',
						'click': {
							'products': [{
								'name': productObj.name,
								'id': productObj.id,
								'price': productObj.price,
								'brand': productObj.brand,
								'category': productObj.category
							}]
						}
					},
					'eventCallback': function () {
						event.preventDefault();
						event.stopPropagation();
						document.location = productObj.link;
					}
				});
				break;
			case 'addToCart':
				dataLayer.push({
					'event': 'addToCart',
					'ecommerce': {
						'currencyCode': 'UAH',
						'add': {
							'products': [{
								'name': productObj.name,
								'id': productObj.id,
								'price': productObj.price,
								'brand': productObj.brand,
								'category': productObj.category,
								'quantity': 1
							}]
						}
					}
				});
				break;
			case 'wishlistToCart':
				dataLayer.push({
					'event': 'addToCart',
					'ecommerce': {
						'currencyCode': 'UAH',
						'add': {
							'products': formProducts($wishlistProducts)
						}
					}
				});
				break;
			case 'removeFromCart':
				dataLayer.push({
					'event': 'removeFromCart',
					'ecommerce': {
						'currencyCode': 'UAH',
						'remove': {
							'products': [{
								'name': productObj.name,
								'id': productObj.id,
								'price': productObj.price,
								'brand': productObj.brand,
								'category': productObj.category,
								'quantity': 1
							}]
						}
					}
				});
				break;
			case 'removeFromCartAll':
				dataLayer.push({
					'event': 'removeFromCart',
					'ecommerce': {
						'currencyCode': 'UAH',
						'remove': {
							'products': [{
								'name': productObj.name,
								'id': productObj.id,
								'price': productObj.price,
								'brand': productObj.brand,
								'category': productObj.category,
								'quantity': parseInt($this.parents('[data-product]').find('[data-product-quantity]').val(), 10)
							}]
						}
					}
				});
				break;
			case 'oneClickBuy':
				dataLayer.push({
					'event': 'gaTriggerEvent',
					'gaEventCategory': '1_click_purchace',
					'gaEventAction': 'сlick_button'
				});
				break;
			case 'cabinetClick':
				dataLayer.push({
					'event': 'gaTriggerEvent',
					'gaEventCategory': 'Cabinet',
					'gaEventAction': 'сlick_button'
				});
				break;
			case 'deliveryCheck':
				dataLayer.push({
					'event': 'checkout',
					'ecommerce': {
						'checkout': {
							'actionField': {
								'step': 3
							},
							'products': formProducts($cartCheckout)
						}
					}
				});
				dataLayer.push({
					'event': 'checkoutOption',
					'ecommerce': {
						'checkout_option': {
							'actionField': {
								'step': 3,
								'option': $this.parent().text().trim()
							}
						}
					}
				});
				break;
			case 'paymentCheck':
				dataLayer.push({
					'event': 'checkout',
					'ecommerce': {
						'checkout': {
							'actionField': {
								'step': 4
							},
							'products': formProducts($cartCheckout)
						}
					}
				});
				dataLayer.push({
					'event': 'checkoutOption',
					'ecommerce': {
						'checkout_option': {
							'actionField': {
								'step': 4,
								'option': $this.parent().text().trim()
							}
						}
					}
				});
				break;
			case 'recieverCheck':
				dataLayer.push({
					'event': 'checkout',
					'ecommerce': {
						'checkout': {
							'actionField': {
								'step': 5
							},
							'products': formProducts($cartCheckout)
						}
					}
				});
				dataLayer.push({
					'event': 'checkoutOption',
					'ecommerce': {
						'checkout_option': {
							'actionField': {
								'step': 5,
								'option': $this.parent().text().trim()
							}
						}
					}
				});
				break;
		}
	});

	function formProducts ($parent) {
		const products = [];
		$parent.find('[data-product]').each((i, el) => {
			const productObj = $(el).data('product');
			delete productObj.link;
			products.push(productObj);
		});
		return products;
	}
}
