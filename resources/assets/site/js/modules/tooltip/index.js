import tippy, { roundArrow } from 'tippy.js';
import 'tippy.js/dist/svg-arrow.css';
import 'tippy.js/animations/scale.css';
import 'tippy.js/dist/tippy.css';

const tooltip = ($elements) => {
	$($elements).each((i, item) => {
		const template = $(item).find('[data-tooltip-template]')[0];
		const customOptions = $(item).data('tooltip') || {};
		const defaultOptions = {
			appendTo: 'parent',
			interactive: true,
			arrow: roundArrow,
			content: template,
			animation: 'scale'
		};

		const options = Object.assign({}, defaultOptions, customOptions);

		tippy(item, options);
	});
};

export default tooltip;
