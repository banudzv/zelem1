export function deliveryPrice ($elements) {
	let orderTotal = $('#order-total').text();
	let deliveryDefault = $('#delivery-price').text();
	let delivery = parseInt(deliveryDefault.slice(0, -1).trim());
	$elements.each((i, el) => {
		const $element = $(el);
		const orderBreakpoint = parseInt($element.data('delivery-price-breakpoint'), 10);
		$element.removeAttr('data-delivery-price-breakpoint');
		let price,
			priceThousand,
			total;
		$element.on('click', '[data-delivery-price-checker]', function () {
			total = parseInt(orderTotal.slice(0, -1).trim(), 10) - parseInt(deliveryDefault.slice(0, -1).trim(), 10);
			price = $(this).data('price');
			priceThousand = $(this).data('price-thousand');
			if (total <= orderBreakpoint) {
				$('#delivery-price').text(price + ' ₴');
				total += price;
				delivery = price;
			} else {
				$('#delivery-price').text(priceThousand + ' ₴');
				total += priceThousand;
				delivery = priceThousand;
			}
			$('#order-total').text(total + ' ₴');
		});
	});
	$(document).on('cartUpdate', function () {
		orderTotal = $('#order-total').text();
		deliveryDefault = $('#delivery-price').text();
		$('#delivery-price').text(delivery + ' ₴');
		$('#order-total').text(parseInt(orderTotal.slice(0, -1).trim()) + delivery + ' ₴');
	});
}
