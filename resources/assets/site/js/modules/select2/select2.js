import $ from 'jquery';
import 'select2';

export function select2Init ($elements) {
	const lang = window.LOCO_DATA.language === 'ua' ? 'uk' : window.LOCO_DATA.language;
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('select2-initialized')) {
			return true;
		}

		_init($el, lang);
	});
}
function _init ($element, lang) {
	$.fn.select2.amd.define('select2/i18n/' + lang, [], require('select2/src/js/select2/i18n/' + lang));
	$element.select2({
		multiple: true,
		minimumInputLength: 0,
		maximumSelectionLength: 1,
		language: lang,
		minimumResultsForSearch: 5,
		placeholder: $element.data('placeholder')
	});
	$element.removeAttr('multiple');
	$element.val(null).trigger('change');
	$element.on('select2:open', function () {
		$element.val(null).trigger('change');
	});
}
