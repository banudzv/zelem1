import $ from 'jquery';

function actionBar ($elements, moduleLoader) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('action-bar-initialized')) {
			return true;
		}

		const $window = $(window);
		const $section = $el.closest('[data-action-bar-section]');
		const elementOffset = $section.prev('.section').offset().top + $section.prev('.section').outerHeight();
		let scrollTop = $window.scrollTop();

		if (scrollTop > elementOffset) {
			$el.addClass('scrolled-out');
			$section.addClass('scrolled-out');
		} else {
			$el.removeClass('scrolled-out');
			$section.removeClass('scrolled-out');
		}

		$window.on('scroll', () => {
			scrollTop = $window.scrollTop();

			if (scrollTop > elementOffset + 100) {
				$el.addClass('scrolled-out');
				$section.addClass('scrolled-out');
			} else {
				$el.removeClass('scrolled-out');
				$section.removeClass('scrolled-out');
			}
		});
	});
}

export default actionBar;
