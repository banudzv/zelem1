'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import 'slick-carousel';
import 'slick-carousel/slick/slick.scss';
import 'custom-jquery-methods/fn/has-inited-key';
import * as types from './slick-types';
import { alignTable } from 'assetsSite#/js/modules/comparelist-table';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {jQuery} $containers
 */
function each ($containers) {
	$containers.each((i, el) => {
		const $container = $(el);
		if ($container.hasInitedKey('slick-slider-initialized')) {
			return true;
		}

		const type = $container.data(types.SlickType.dataAttrKey).type;
		/** @type {SlickType} */
		const TypeClass = types[type] || types.SlickDefault; // eslint-disable-line import/namespace
		const slickSlider = new TypeClass($container);
		slickSlider.initialize();
		$container.on('sliderUpdate', () => {
			slickSlider.$slider.on('destroy', function () {
				slickSlider.initialize();
			});
			slickSlider.destroy();

			const $tables = $('[data-comparelist-table]'); // кусок для таблицы сравнения
			$tables.each((i, el) => {
				const $table = $(el);
				alignTable($table, $table.find('[data-row-count]').data('row-count'));
			});
			$('[data-compare-count]').text(slickSlider.$rootElement.find('.slick-slide:not(.slick-cloned)').length);
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default each;
