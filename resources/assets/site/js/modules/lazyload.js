'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import Modernizr from 'modernizr';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * Селектор выборки элементов
 * @type {string}
 */
export const lazyloadSelector = '.js-lazyload';

/**
 * @param {string|NodeList|Element} [elements='.js-lazyload']
 * @param {zzLoadOptions} [userOptions={}]
 * @return {Promise<zzLoadObserver>}
 * @example
 * import { lazyload } from 'assets#/js/modules/lazyload'
 * lazyload().then(observer => observer.observe);
 */
export function lazyload (elements = lazyloadSelector, userOptions = {}) {
	const options = Object.assign({
		clearSourceAttrs: true
	}, userOptions);

	return new Promise((resolve, reject) => {
		const _initialize = ({ default: zzLoad }) => {
			resolve(zzLoad(elements, options));
		};

		if (Modernizr.intersectionobserver) {
			import('zz-load').then(_initialize);
		} else {
			import('intersection-observer').then(() => {
				import('zz-load').then(_initialize);
			});
		}
	});
}

/**
 * @typedef zzLoadObserver
 * @property {function} observe
 * @property {function} triggerLoad
 */
