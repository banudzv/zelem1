<?php

return [

    /*
     |--------------------------------------------------------------------------
     | Validation Language Lines
     |--------------------------------------------------------------------------
     |
     | The following language lines contain the default error messages used by
     | the validator class. Some of these rules have multiple versions such
     | as the size rules. Feel free to tweak each of these messages here.
     |
     */

    'accepted' => 'Поле ":attribute" має бути зазначено.',
    'active_url' => 'Значення поля ":attribute" не є коректною URL адресою.',
    'after' => 'Значення поля ":attribute" має бути датою більше ніж: date.',
    'after_or_equal' => 'Значення поля ":attribute" має бути датою, яка більше або дорівнює :date.',
    'alpha' => 'Значення поля ":attribute" може містити в собі тільки букви.',
    'alpha_dash' => 'Значення поля ":attribute" може містити в собі тільки букви, цифри і нижні підкреслення.',
    'alpha_num' => 'Значення поля ":attribute" може містити в собі тільки букви і цифри.',
    'array' => 'Значення поля ":attribute" має бути масивом.',
    'before' => 'Значення поля ":attribute" має бути датою менше :date.',
    'before_or_equal' => 'Значення поля ":attribute" має бути датою менше або дорівнює: date.',
    'between' => [
        'numeric' => 'Значення поля має бути в межах :min і :max.',
        'file' => 'Розмір файлу повинен бути в межах :min і :max кілобайт.',
        'string' => 'Рядок повинен бути не коротше :min і не довше :max символів.',
        'array' => 'Кількість елементів масиву повинно бути в межах між :min і :max.',
    ],
    'gt' => [
        'numeric' => 'Значення поля має бути більше ніж у полі :field.',
        'file' => 'Розмір файлу повинен бути більше ніж у полі :field',
        'string' => 'Значення має бути більше ніж у полі :field',
        'array' => 'Значення має бути більше ніж у полі :field',
    ],
    'gte' => [
        'numeric' => 'Значення має бути більше або дорівнювати значенню з поля :field.',
        'file' => 'Розмір файлу повинен бути більше або дорівнювати значенню з поля :field',
        'string' => 'Значення має бути більше або дорівнювати значенню з поля :field',
        'array' => 'Значення має бути більше або дорівнювати значенню з поля :field',
    ],
    'lte' => [
        'numeric' => 'Значення має бути менше або дорівнювати значенню з поля: field.',
        'file' => 'Розмір файлу повинен бути менше або дорівнювати значенню з поля: field',
        'string' => 'Значення має бути менше або дорівнювати значенню з поля: field',
        'array' => 'Значення має бути менше або дорівнювати значенню з поля: field',
    ],
    'boolean' => 'Значення поля ":attribute" має бути булевим.',
    'confirmed' => 'Значення поля ":attribute" не підтверджено.',
    'date' => 'Значення поля ":attribute" не є датою.',
    'date_format' => 'Значення поля ":attribute" не збігається з форматом :format.',
    'different' => 'Значення поля ":attribute" і :other повинні відрізнятися.',
    'digits' => 'Значення поля ":attribute" має містити в собі :digits цифр.',
    'digits_between' => 'Значення поля ":attribute" має містити від :min до :max символів.',
    'dimensions' => 'Файл не є зображенням.',
    'distinct' => 'Значення поля ":attribute" повторюється.',
    'email' => 'Значення поля ":attribute" має бути email адресою.',
    'exists' => 'Вибране значення некоректне.',
    'file' => 'Виберіть файл.',
    'filled' => 'Поле ":attribute" не заповнене.',
    'image' => 'Виберіть коректне зображення.',
    'in' => 'Обраний поле ":attribute" некоректно.',
    'in_array' => 'Значення не існує в :other.',
    'integer' => 'Значення поля ":attribute" має бути числом.',
    'ip' => 'Значення поля ":attribute" має бути коректним IP адресою.',
    'ipv4' => 'Значення поля ":attribute" має бути коректною IPv4 адресою.',
    'ipv6' => 'Значення поля ":attribute" має бути коректною IPv6 адресою.',
    'json' => 'Значення поля ":attribute" має бути коректним JSON рядком.',
    'max' => [
        'numeric' => 'Значення поля ":attribute" не може бути більше :max.',
        'file' => 'Файл не може важити більше :max кілобайт.',
        'string' => 'Значення поля ":attribute" не може бути довшим :max символів.',
        'array' => 'Масив не може містити більше :max елементів.',
    ],
    'mimes' => 'Значення поля ":attribute" має бути файлом типу: :values.',
    'mimetypes' => 'Значення поля ":attribute" має бути файлом типу: :values.',
    'min' => [
        'numeric' => 'Значення поля ":attribute" має бути не менше :min.',
        'file' => 'Розмір файлу повинен бути не менше :min кілобайт.',
        'string' => 'Значення поля ":attribute" має бути не коротше :min символів.',
        'array' => 'Масив має містити не менше :min елементів.',
    ],
    'not_in' => 'Вибране значення поля ":attribute" некоректно.',
    'numeric' => 'Значення поля ":attribute" має бути числом.',
    'present' => ':attribute: Значення має бути заповнене.',
    'regex' => ':attribute: Формат значення некоректний.',
    'required' => 'Поле ":attribute" обов\'язково для заповнення.',
    'required_if' => 'Поле обов\'язково для заповнення, коли :other = :value.',
    'required_unless' => 'Поле обов\'язково для заповнення, якщо: other є одним зі значень: :values.',
    'required_with' => 'Поле обов\'язково для заповнення, якщо хоча б одне з полів [:values] заповнене.',
    'required_with_all' => 'Поле обов\'язково для заповнення, якщо все поля [:values] заповнені.',
    'required_without' => 'Поле обов\'язково для заповнення, якщо поля [:values] незаповнені.',
    'required_without_all' => 'Поле обов\'язково для заповнення, якщо ніодно з полів [:values] не заповнено.',
    'same' => 'Значення цього поля і :other повинні збігатися.',
    'size' => [
        'numeric' => 'Число повинно бути довгою в :size цифр.',
        'file' => 'Розмір файлу повинен бути рівний :size кіллобайт.',
        'string' => 'Рядок повинен бути довгою :size символів.',
        'array' => 'Масив повинен містити :size елементів.',
    ],
    'string' => 'Значення поля ":attribute" має бути рядком.',
    'timezone' => 'Значення поля ":attribute" має бути коректним часового поясу.',
    'unique' => ':attribute: Введене значення не унікальна.',
    'uploaded' => 'Немає запису був завантажений.',
    'url' => 'Формат некоректний.',

    /**
     * Custom rules
     */
    'current_password' => 'Будь ласка вкажіть коректний діючий пароль.',
    'slug' => 'Введене значення для поля ":attribute" не унікальна.',
    'domain' => 'Доменне ім\'я некоректно.',

    /*
     |--------------------------------------------------------------------------
     | Custom Validation Language Lines
     |--------------------------------------------------------------------------
     |
     | Here you may specify custom validation messages for attributes using the
     | convention "attribute.rule" to name the lines. This makes it quick to
     | specify a specific custom language line for a given attribute rule.
     |
     */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
     |--------------------------------------------------------------------------
     | Custom Validation Attributes
     |--------------------------------------------------------------------------
     |
     | The following language lines are used to swap attribute place-holders
     | with something more reader friendly such as E-Mail Address instead
     | of "email". This simply helps us make messages a little cleaner.
     |
     */

    'attributes' => [
        'password' => 'Пароль',
        'name' => 'Назва',
        'user' => 'Користувач',
        'slug' => 'Аліас',
        'default' => 'За замовчуванням',
        'fio' => 'ПІБ',
        'first_name' => 'Ім\'я',
        'last_name' => 'Прізвище',
        'middle_name' => 'По батькові',
        'email' => 'Електронна пошта',
        'extension' => 'Формат',
        'quote' => 'Обрамлення csv',
        'coma' => 'Роздільники csv',
        'user_id' => 'Користувач',
        'created_at' => 'Дата створення',
        'updated_at' => 'Остання зміна',
        'published_at' => 'Дата публікації',
        'active' => 'Активність',
        'phone' => 'Номер телефону',
        'current_password' => 'Поточний пароль',
        'new_password' => 'Новий пароль',
        'new_password_confirmation' => 'Підтвердіть пароль',
        'language' => 'Мова',
        'image' => 'Зображення',
        'parent_id' => 'Батько',
        'date_from' => 'Дата від',
        'date_to' => 'Дата по',
        'content' => 'Опис',
        'short_content' => 'Короткий опис',
        'ip' => 'IP',
        'blocked_to' => 'Заблокований до',
        'blocked_forever' => 'Заблокований назавжди',
        'comment' => 'Коментар',
        'commentable_id' => 'Пов\'язана запис',
        'subject' => 'Тема',
        'text' => 'Контент',
        'publish_date' => 'Дата публікації',
        'identifier' => 'Ідентифікатор',
        'seo_text' => 'Seo текст',
        'album_id' => 'Альбом',
        'price' => 'Ціна',
        'category' => 'Категорія',
        'url' => 'Посилання',
        'type' => 'Тип',
        'file' => 'Файл',
        'brand_id' => 'Виробник',
        'available' => 'Наявність',
        'old_price' => 'Стара ціна',
        'color' => 'Колір',
        'birthday' => 'Дата народження',
        'sex' => 'Підлога',
        'city' => 'Місто',
        'vendor_code' => 'Артикул',
        'personal-data-processing' => 'Згоден на обробку даних',
        'mark' => 'Оцінка',
        'answer' => 'Відповідь',
        'answered_at' => 'Дата відповіді',
        'microdata' => 'Значення для мікророзмітки',
        'delivery' => 'Спосіб доставки',
        'payment_method' => 'Спосіб оплати',
        
        'h1' => 'Meta h1',
        'title' => 'Meta title',
        'keywords' => 'Meta keywords',
        'description' => 'Meta description',
        'per-page' => 'Кіл-ть на сторінці',
        'roles-per-page' => 'Кіл-ть ролей на сторінці',
        'per-page-client-side' => 'Кіл-ть на сторінці сайту',
        'auth' => '"Запаролити сайт"',
        'history-per-page' => 'Кіл-ть на сторінці',
        'autoplay' => 'Автозапуск',
        'per-widget' => 'Кіл-ть елементів у віджеті',
        'count-in-widget' => 'Кіл-ть елементів у віджеті',
        'per-page-for-user' => 'Кіл-ть елементів на сторінці в ЛК',
        'address_for_self_delivery' => 'Адреса для самовивозу',
        'facebook-api-secret' => 'App Id Facebook',
        'facebook-api-key' => 'App Secret Facebook',
        'twitter-api-key' => 'App Secret Twitter',
        'twitter-api-secret' => 'App key Twitter',
        'instagram-api-secret' => 'Client Secret Instagram',
        'instagram-api-key' => 'App ID Instagram',
        'hide-delivery-self' => 'pole "Ukryj opcję dostawy Odbiór" jest zaznaczone',
    ],
];
