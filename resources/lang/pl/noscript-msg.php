<?php

return [
    'close_notification' => 'Закрити повідомлення',
    'disabled_javascript' => 'У вашому браузері вимкнений JavaScript!',
    'required_javascript' => 'Для коректної роботи з сайтом необхідна підтримка Javascript.',
    'enable_javascript' => 'Ми рекомендуємо вам включити підтримку JavaScript у налаштунках вашого сайту.'
];
