<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Введені дані не підходять',
    'succeed' => 'Ви буди успішно авторизовані на сайті',
    'throttle' => 'Забагато спроб. Будь ласка, спробуйте через :seconds секунд.',
    'sign-in-please' => 'Авторизуйтесь для входу до адмін панелі',
    'logout' => 'Вихід',
    'remember-me' => 'Запам\'ятати мене',
    'sign-in' => 'Увійти',
];
