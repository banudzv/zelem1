<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль має бути не коротше шости символів и співпадати з підтвержденням',
    'reset' => 'Ваш пароль був скинутий!',
    'sent' => 'Ми відправили вам посилання для підтвердження парол на вашу email адресу!',
    'token' => 'Цей токен для збросу пароля не коректний.',
    'user' => 'Ми не можемо знайти користувача за вказанною email адресою.',
];
