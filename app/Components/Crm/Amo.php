<?php

namespace App\Components\Crm;

use AmoCRM\Client;
use AmoCRM\Models\Note;

/**
 * Class Amo
 * @package App\Components\Crm
 */
class Amo
{
    /**
     * @var Client
     */
    protected $client;
    protected $status_id;
    protected $responsible_user_id;
    protected $pipeline_id;

    /**
     * Amo constructor.
     */
    public function __construct()
    {
        $this->client = new Client(config('db.amo_crm.domain'), config('db.amo_crm.login'), config('db.amo_crm.hash'));
        $this->status_id = config('db.amo_crm.status_id');
        $this->pipeline_id = config('db.amo_crm.pipeline_id');
        $this->responsible_user_id = config('db.amo_crm.responsible_user_id');
    }

    /**
     * @param $data
     */
    public function newLead($data)
    {
        $lead = $this->client->lead;
        $lead['name'] = array_get($data, 'formName') . ' ' . $_SERVER['HTTP_HOST'];
        $lead['price'] = array_get($data, 'amount');
        $lead['date_create'] = time();
        $lead['pipeline_id'] = $this->pipeline_id;
        $lead['status_id'] = $this->status_id;
        $lead['responsible_user_id'] = $this->responsible_user_id;
        $lead['tags'] = [$_SERVER['HTTP_HOST'], array_get($data, 'formName')];
        // Добавление кастомного поля
        $lead->addCustomField(config('db.amo_crm.roistat_visit'), array_key_exists('roistat_visit', $_COOKIE) ? $_COOKIE['roistat_visit'] : 'неизвестно');
        // $lead->addCustomField(config('db.amo_crm.amount_id'), array_get($data, 'amount'));
        $leadId = $lead->apiAdd();
        if ($leadId) {
            //newContact
            $this->newContact($leadId, $data);
            //newTask
            $this->newTask($leadId);
            //newNote
            $text = array_get($data, 'text');
            if ($text) {
                $this->newNote($leadId, $text);
            }
        }
    }

    /**
     * @param $leadId
     * @param $data
     */
    public function newContact($leadId, $data)
    {
        $contact = $this->client->contact;
        $contact['name'] = array_get($data, 'name');
        $contact['date_create'] = time();
        $contact['responsible_user_id'] = $this->responsible_user_id;
        $contact['linked_leads_id'] = $leadId;
        // Добавление кастомного поля
        $contact->addCustomField(config('db.amo_crm.phone_id'), [
            [array_get($data, 'phone'), 'WORK'],
        ]);
        $contact->addCustomField(config('db.amo_crm.email_id'), [
            [array_get($data, 'email'), 'WORK'],
        ]);
        $contact->apiAdd();
    }

    /**
     * @param $leadId
     * @param $data
     */
    public function newTask($leadId)
    {
        $task = $this->client->task;
        $task['element_id'] = $leadId;
        $task['element_type'] = 2;
        $task['date_create'] = time();
        $task['status'] = 0;
        $task['task_type'] = $this->status_id;
        $task['text'] = 'Cвязаться по заявке с сайта ' . $_SERVER['HTTP_HOST'];
        $task['responsible_user_id'] = $this->responsible_user_id;
        $task['complete_till'] = strtotime(date('d.m.Y') . ' 20:59');
        $task->apiAdd();
    }

    /**
     * @param $leadId
     * @param $text
     */
    public function newNote($leadId, $text)
    {
        $note = $this->client->note;
        $note['element_id'] = $leadId;
        $note['element_type'] = Note::TYPE_LEAD;
        $note['note_type'] = Note::COMMON;
        $note['text'] = $text;
        $note->apiAdd();
    }
}