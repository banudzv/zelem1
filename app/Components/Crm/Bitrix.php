<?php

namespace App\Components\Crm;
use GuzzleHttp\Client;


/**
 * Class Bitrix
 * @package App\Components\Crm
 */
class Bitrix
{
    protected $client;
    protected $general_url = 'https://app.auspex.com.ua/user8066/turbosto/index.php?sq=1fy834yh6gf5rf1f3gh';

    /**
     * Bitrix constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $body
     * @return bool
     */
    public function send($body){
        $response = $this->client->post( $this->general_url,
            ['headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($body)
        ]);

        return $response->getStatusCode() == 200;
    }



}
