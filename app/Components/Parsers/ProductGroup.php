<?php

namespace App\Components\Parsers;

use Illuminate\Support\Collection;

/**
 * Class ProductGroup
 *
 * @package App\Components\Parsers
 */
class ProductGroup
{
    /**
     * @var Collection|AbstractParsedProduct[]
     */
    public $modifications;
    
    /**
     * @var AbstractParsedProduct|null
     */
    public $mainProduct;
    
    /**
     * ProductGroup constructor.
     */
    public function __construct()
    {
        $this->modifications = new Collection();
    }
    
    /**
     * @return array
     */
    public function getAllFeatures(): array
    {
        $features = $this->mergeFeatures($this->mainProduct);
        $this->modifications->each(function (AbstractParsedProduct $parsedProduct) use (&$features) {
            $features = $this->mergeFeatures($parsedProduct, $features);
        });
        return $features;
    }
    
    /**
     * @param AbstractParsedProduct $parsedProduct
     * @param array $features
     * @return array
     */
    private function mergeFeatures(AbstractParsedProduct $parsedProduct, array $features = []): array
    {
        foreach ($parsedProduct->features as $index => $feature) {
            if (!$feature) {
                continue;
            }
            $features[$feature] = array_get($features, $feature, []);
            $values = [];
            foreach ((array)$parsedProduct->featuresValues[$index] as $value) {
                if (!$value) {
                    continue;
                }
                $measure = $parsedProduct->featuresMeasures[$index] ?? null;
                $values[] = $value . ($measure === null ? '' : ' ' . $measure);
            }
            $features[$feature] = array_merge($features[$feature], $values);
            $features[$feature] = array_unique($features[$feature]);
        }
        return $features;
    }
    
    /**
     * @param bool $sameIdAsInPackage
     * @param AbstractParsedProduct $parsedProduct
     */
    public function addModification(AbstractParsedProduct $parsedProduct, bool $sameIdAsInPackage = false)
    {
        if(
            (
                $sameIdAsInPackage === false
                && $this->hasMainProduct() === false
            )
            || (
                !$parsedProduct->remotePackId
                && $this->hasMainProduct() === false
            )
            || (
                $sameIdAsInPackage === true
                && $parsedProduct->remotePackId == $parsedProduct->remoteUniqueIdentifier
            )
        ) {
            $this->mainProduct = $parsedProduct;
        } else {
            $this->modifications->push($parsedProduct);
        }
    }
    
    /**
     * @return bool
     */
    public function hasMainProduct(): bool
    {
        return $this->mainProduct !== null;
    }
}
