<?php


namespace App\Components\Parsers\PromUaCSV;


use App\Components\Parsers\AbstractParsedCategory;

class ParsedCategory extends AbstractParsedCategory
{
    /**
     * @var array
     */
    private $synonyms = [
        'Номер_группы' => 'remoteCategoryId',
        'Название_группы' => 'categoryName',
        'Идентификатор_группы' => 'categoryId',
        'Номер_родителя' => 'remoteParentId',
        'Идентификатор_родителя' => 'parentId',
    ];

    public static $requiredColumns = [
        'Номер_группы', 'Название_группы',
    ];

    /**
     * @var array
     */
    private $data;

    /**
     * Parsing...
     */
    public function parse(): void
    {
        foreach ($this->data as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param string $promUaPropertyName
     * @param mixed $value
     */
    public function setAttribute(string $promUaPropertyName, $value): void
    {
        if (isset($this->synonyms[$promUaPropertyName])) {
            $this->{$this->synonyms[$promUaPropertyName]} = $value;
        }
    }

    /**
     * @param int|null $categoryId
     */
    public function setRemoteCategoryIdAttribute($categoryId)
    {
        $this->attributes['remoteCategoryId'] = (int)$categoryId ?: null;
    }

    /**
     * @param int|null $categoryId
     */
    public function setCategoryIdAttribute($categoryId)
    {
        $this->attributes['categoryId'] = (int)$categoryId ?: null;
    }

    /**
     * @param int|null $categoryId
     */
    public function setRemoteParentIdAttribute($categoryId)
    {
        $this->attributes['remoteParentId'] = (int)$categoryId ?: null;
    }

    /**
     * @param int|null $categoryId
     */
    public function setParentIdAttribute($categoryId)
    {
        $this->attributes['parentId'] = (int)$categoryId ?: null;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return !$this->isNotValid();
    }

    /**
     * @return bool
     */
    public function isNotValid(): bool
    {
        return !$this->remoteCategoryId || !$this->categoryName;
    }


}