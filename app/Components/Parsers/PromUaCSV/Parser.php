<?php


namespace App\Components\Parsers\PromUaCSV;


use App\Components\Parsers\AbstractParsedCategory;
use App\Components\Parsers\AbstractParser;
use App\Components\Parsers\ProductGroup;
use App\Exceptions\WrongParametersException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Parser extends AbstractParser
{
    /**
     * @var string
     */
    private $pathToCategories;
    /**
     * @var string
     */
    private $pathToProducts;
    /**
     * @var string
     */
    private $pathToFile;

    public function __construct(string $pathToFile)
    {
        $this->categories = new Collection();
        $this->products = new Collection();
        $this->pathToFile = $pathToFile = storage_path('app/public/' . $pathToFile);
    }

    /**
     * @throws WrongParametersException
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function start(): void
    {
        $this->convertXlsToCsv();
        $this->parseCategories();
        $this->parseProducts();
        @unlink($this->pathToFile);
        @unlink($this->pathToCategories);
        @unlink($this->pathToProducts);
    }

    /**
     * @return AbstractParsedCategory
     */
    public function createEmptyParsedCategoryObject(): AbstractParsedCategory
    {
        return new ParsedCategory();
    }

    /**
     * @throws WrongParametersException
     */
    private function convertXlsToCsv(): void
    {
        try {
            $filePathParts = explode('/', $this->pathToFile);
            $fileName = end($filePathParts);
            $file = new UploadedFile($this->pathToFile, $fileName);
            $extension = $file->getClientOriginalExtension();
            if ($extension === 'xlsx') {
                $reader = new Xlsx();
            } elseif ($extension === 'xls') {
                $reader = new Xls();
            } else {
                throw new WrongParametersException('Wrong File');
            }
            $spreadsheet = $reader->load($this->pathToFile);
            $loadedSheetNames = $spreadsheet->getSheetNames();
            $writer = new Csv($spreadsheet);
            $filesPath = [];
            foreach ($loadedSheetNames as $sheetIndex => $loadedSheetName) {
                $writer->setSheetIndex($sheetIndex);
                $fileName = storage_path('app/public/') . str_replace(' ', '_', $loadedSheetName) . '.csv';
                $writer->save($fileName);
                $filesPath[] = $fileName;
            }

            if (sizeof($filesPath) !== 2) {
                throw new WrongParametersException('Wrong File');
            }
            $this->setPathToCategories($filesPath[1]); // categories
            $this->setPathToProducts($filesPath[0]); // products
        } catch (\Exception $exception) {
            throw new WrongParametersException($exception->getMessage());
        }


    }

    /**
     * @throws WrongParametersException
     */
    private function parseCategories()
    {
        $data = $this->getDataFromCsv($this->pathToCategories);
        foreach ($data as $value) {
            $parsedCategory = new ParsedCategory;
            $parsedCategory->setData($value)->parse();
            if ($parsedCategory->isValid()) {
                $this->categories->push($parsedCategory);
            }
        }
    }

    /**
     * @throws WrongParametersException
     */
    private function parseProducts()
    {
        $data = $this->getDataFromCsv($this->pathToProducts);
        foreach ($data as $value) {
            $parsedProduct = new ParsedProduct;
            $parsedProduct->setData($value)->parse();
            if (!$parsedProduct->price) {
                # This is not a good product
                # It has no price
                continue;
            }
            if (!$parsedProduct->currency) {
                $parsedProduct->currency = 'UAH';
            }
            if (!$parsedProduct->productUrl) {
                $parsedProduct->setAttribute(
                    'Продукт_на_сайте',
                    Str::slug($parsedProduct->name)
                );
            }
            /** @var ProductGroup $group */
            $group = $this->products->get($parsedProduct->getGroupId(), new ProductGroup());
            $group->addModification($parsedProduct, true);
            $this->products->put($parsedProduct->getGroupId(), $group);
        }
    }

    /**
     * @param $pathToFile
     */
    private function setPathToCategories($pathToFile): void
    {
        $this->pathToCategories = $pathToFile;
    }

    /**
     * @param $pathToFile
     */
    private function setPathToProducts($pathToFile): void
    {
        $this->pathToProducts = $pathToFile;
    }

    /**
     * @param $path
     * @return array
     * @throws WrongParametersException
     */
    private function getDataFromCsv($path): array
    {
        if (!file_exists($path) || !is_readable($path)) {
            throw new WrongParametersException("File $path not exist");
        }
        $header = null;
        $data = [];
        try {
            if (($handle = fopen($path, 'r')) !== false) {
                while (($row = fgetcsv($handle, 100000, ',')) !== false) {
                    if (!$header) {
                        $header = $row;
                    } else {
                        $data[] = array_combine($header, $row);
                    }
                }
                fclose($handle);
            }
            return $data;
        } catch (\Exception $exception) {
            throw new WrongParametersException($exception->getMessage());
        }

    }
}