<?php

namespace App\Components\Parsers;

use Illuminate\Support\Str;

/**
 * Class AbstractParsedProduct
 *
 * @package App\Components\Parsers
 * @property-read string|null $vendorCode
 * @property-read string|null $name
 * @property-read string|null $keywords
 * @property-read string|null $content
 * @property-read string $productType
 * @property-read string|null $unit
 * @property-read int|null $minUnits
 * @property-read int|null $wholesalePrice
 * @property-read int|null $minUnitsForWholeSale
 * @property-read string|null $categoryName
 * @property-read string|null $categoryUrl
 * @property-read string|null $deliveryPossibility
 * @property-read string|null $deliveryTime
 * @property-read int|null $packing
 * @property-read string $remoteUniqueIdentifier
 * @property-read int|null $productId
 * @property-read int|null $remoteCategoryUniqueId
 * @property-read string|null $brand
 * @property-read string|int|null $discount
 * @property-read string|null $remotePackId
 * @property-read string|null $labels
 * @property string|null $productUrl
 */
abstract class AbstractParsedProduct extends AbstractItem
{
    /**
     * @var array
     */
    protected $currencies = [
        'UAH' => 'UAH',
        'USD' => 'USD',
        'EUR' => 'EUR',
        'CHF' => 'CHF',
        'RUB' => 'RUB',
        'GBP' => 'GBP',
        'JPY' => 'JPY',
        'PLZ' => 'PLZ',
        'BYN' => 'BYN',
        'KZT' => 'KZT',
        'MDL' => 'MDL',
        'р' => 'RUB',
        'руб' => 'RUB',
        'дол' => 'USD',
        '$' => 'USD',
        'грн' => 'UAH',
    ];
    
    /**
     * Images links list
     *
     * @var array
     */
    public $images = [];
    
    /**
     * @var array
     */
    public $features = [];
    
    /**
     * @var array
     */
    public $featuresMeasures = [];
    
    /**
     * @var array
     */
    public $featuresValues = [];
    
    /**
     * @var float
     */
    public $price;
    
    /**
     * @var float
     */
    public $oldPrice;
    
    /**
     * @var int
     */
    public $availability;
    
    /**
     * @var string
     */
    public $vendorCode;
    
    /**
     * @var string
     */
    public $remoteCategoryId;
    
    /**
     * @var int|null
     */
    public $categoryId;
    
    /**
     * @var string
     */
    public $currency;
    
    /**
     * @var string
     */
    public $slug;
    
    /**
     * @var string
     */
    public $name;
    
    /**
     * Parsing...
     */
    abstract public function parse(): void;
    
    /**
     * @return string|null
     */
    public function getGroupId(): string
    {
        return $this->remotePackId ?: Str::uuid();
    }
    
    /**
     * @param string|null $featureName
     * @return string|null
     */
    public function getFeatureValueByFeatureName(?string $featureName): ?string
    {
        if (!$featureName) {
            return null;
        }
        foreach ($this->features as $index => $feature) {
            if ($feature == $featureName) {
                $value = array_get($this->featuresValues, $index);
                if (is_array($value)) {
                    return array_first($value);
                }
                return $value;
            }
        }
        return null;
    }
}
