<?php

namespace App\Components\Parsers;

use Exception;
use App\Components\Parsers\PromUa\Parser as PromUaParser;
use App\Components\Parsers\YandexMarket\Parser as YandexMarketParser;
use App\Components\Parsers\Locotrade\Parser as LocotradeParser;
use App\Components\Parsers\PromUaCSV\Parser as PromUaCSVParser;

/**
 * Class Entry
 *
 * @package App\Components\Parsers
 */
class Entry
{
    const TYPE_PROM_UA = 'prom-ua';
    const TYPE_YANDEX_MARKET = 'yandex-market';
    const TYPE_ONE_S = 'one-s';
    const TYPE_LOCOTRADE = 'locotrade.';
    const TYPE_PROM_UA_CSV = 'prom-ua-csv';

    /**
     * @param string|null $type
     * @param string $path
     * @return AbstractParser
     * @throws Exception
     */
    public static function getParser(?string $type, string $path): AbstractParser
    {
        switch ($type) {
            case Entry::TYPE_ONE_S:
            case Entry::TYPE_YANDEX_MARKET:
                return new YandexMarketParser($path);

            case Entry::TYPE_PROM_UA:
                return new PromUaParser($path);

            case Entry::TYPE_LOCOTRADE:
                return new LocotradeParser($path);

            case Entry::TYPE_PROM_UA_CSV:
                return new PromUaCSVParser($path);

            default:
                throw new Exception('Wrong parser type!');
        }
    }

}
