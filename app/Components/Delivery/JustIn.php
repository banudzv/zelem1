<?php

namespace App\Components\Delivery;

use App\Components\Delivery\Libs\JustIn as JustInLib;

/**
 * Class JustIn
 * @package App\Components\Delivery
 */
class JustIn
{
    /**
     * @var JustInLib Lib that works with the Justin API.
     */
    protected $api;
    /**
     * @var string API key for JustIn.
     */
    protected $apiKey;

    /**
     * JustIn constructor.
     */
    public function __construct()
    {
        $this->apiKey = config('db.justin.key');
        $this->api = new JustInLib(
            config('db.justin.login'), config('db.justin.password'),
            $this->apiKey, \Lang::getLocale(), true
        );
    }

    /**
     * Returns an array with statuses for the exact order.
     *
     * @param string $value
     * @param string $filterName clientNumber, orderNumber, TTN
     * @return array|array[]
     */
    public function getStatusesHistory(string $value, string $filterName = 'clientNumber'): array
    {
        $response = $this->api->getOrderStatusesHistory([
            [
                'name' => 'senderId',
                'comparison' => 'equal',
                'leftValue' => $this->apiKey,
            ],
            [
                'name' => $filterName,
                'comparison' => 'equal',
                'leftValue' => $value,
            ],
        ], 1);
        if (!isset($response['response']) || !$response['response']['status'] || !isset($response['data'])) {
            return [];
        }

        return array_map(function (array $status) {
            return [
                'date' => $status['fields']['statusDate'],
                'orderDescription' => $status['fields']['order']['descr'],
                'name' => $status['fields']['statusOrder']['descr'],
            ];
        }, $response['data']);
    }

    /**
     * Returns array of the possible statuses inside the system.
     *
     * @return array|array[]
     */
    public function getStatuses()
    {
        $response = $this->api->getStatusesList();
        if (!$response['response']['status'] || !isset($response['data'])) {
            return [];
        }

        return array_map(function (array $status) {
            return [
                'name' => $status['fields']['descr'],
                'final' => $status['fields']['final'],
                'code' => $status['fields']['code'],
            ];
        }, $response['data']);
    }

    /**
     * Returns the data about cities by name.
     *
     * @param string $city
     * @return mixed
     */
    public function getCityByName(string $city)
    {
        return $this->api->getCities([[
            'name' => 'descr',
            'comparison' => 'like',
            'leftValue' => $city,
        ]]);
    }

    /**
     * Returns the departments list.
     * Searches by the city UUID.
     *
     * @param string $uuid
     * @return array
     */
    public function getDepartmentsByCityUUID(string $uuid)
    {
        $justinResponse = $this->api->getDepartments([
            [
                'name' => 'city',
                'comparison' => 'equal',
                'leftValue' => $uuid,
            ]
        ]);
        if ($justinResponse['response']['status'] === false || !isset($justinResponse['data']) || $justinResponse['totalCountRecords'] === 0) {
            return [];
        }

        return $this->formatDepartments($justinResponse['data']);
    }

    /**
     * Returns formatted departments list for the chosen city.
     *
     * @param string $city
     * @return array
     */
    public function getDepartmentsByCityName(string $city): array
    {
        $justinResponse = $this->getCityByName($city);
        if ($justinResponse['response']['status'] === false || !isset($justinResponse['data']) || $justinResponse['totalCountRecords'] === 0) {
            return [];
        }

        return $this->getDepartmentsByCityUUID($justinResponse['data'][0]['fields']['uuid']);
    }

    /**
     * Just formats the departments list for the pretty result.
     *
     * @param array $data
     * @return array
     */
    protected function formatDepartments(array $data): array
    {
        usort($data, function (array $prev, array $next) {
            return (int)$prev['fields']['departNumber'] <=> (int)$next['fields']['departNumber'];
        });

        return array_map(function (array $department) {
            $datum = array_only($department['fields'], ['code', 'descr', 'weight_limit', 'address', 'departNumber']);
            $datum['id'] = implode('|', [$department['fields']['city']['uuid'], $datum['code'], $datum['departNumber']]);

            return $datum;
        }, $data);
    }
}
