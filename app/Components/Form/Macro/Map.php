<?php

namespace CustomForm\Macro;

use CustomForm\Element;

/**
 * Class Toggle
 *
 * @package CustomForm\Macro
 */
class Map extends Element
{
    protected $template = 'admin.form.map';
    
    /**
     * @var int
     */
    protected $zoom = 12;
    
    /**
     * @var string
     */
    protected $latitude;
    
    /**
     * @var string
     */
    protected $longitude;
    
    public function __construct(string $name, $object = null)
    {
        parent::__construct($name, $object);
        
        $this->setZoom(config('db.contact.zoom', $this->zoom));
        $this->setLongitude(config('db.contact.longitude', $this->longitude));
        $this->setLatitude(config('db.contact.latitude', $this->latitude));
    }
    
    /**
     * @param int|null $zoom
     * @return Map|Element
     */
    public function setZoom(?int $zoom): self
    {
        $this->zoom = $zoom ?? $this->zoom;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getZoom(): int
    {
        return $this->zoom;
    }
    
    /**
     * @param string|null $longitude
     * @return Map|Element
     */
    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getLongitude(): ?string
    {
        return $this->longitude;
    }
    
    /**
     * @param string|null $latitude
     * @return Map|Element
     */
    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getLatitude(): ?string
    {
        return $this->latitude;
    }
    
    /**
     * @return bool
     */
    public function hasNoCoordinates(): bool
    {
        return !$this->getLongitude() || !$this->getLatitude() || !$this->getZoom();
    }
}
