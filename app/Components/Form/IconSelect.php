<?php

namespace CustomForm;

/**
 * Select
 *
 * @package CustomForm
 */
class IconSelect extends SimpleSelect
{

    public $defaultIcon = '-';

    public $iconFile = '';

    /**
     * Select constructor
     *
     * @param  string $name
     * @param  null $object
     * @throws \App\Exceptions\WrongParametersException
     */
    public function __construct(string $name, $object = null)
    {
        parent::__construct($name, $object);
        $this->classes[] = 'select2';
        $this->classes[] = 'iconChange';
        $this->template = 'admin.form.icon-select';

    }

    public function addSvgSprite(string $file): self
    {
        $this->iconFile = $file . '.svg';

        $iconList = $this->getSvgListFromFile();
        // Add elements
        foreach ($iconList as $value => $title) {
            $this->choices->push([
                'value' => $value,
                'title' => $title,
            ]);
        }

        /*if (count($iconList)){
            $this->defaultIcon = reset($iconList);
        }*/
        // Return current select exemplar
        return $this;
    }

    public function getSvgListFromFile(){
        $path = public_path('site/assets/svg/' . $this->getIconFile()) ;
        $icons = [];
        if (is_file($path)){
            $dom = new \DOMDocument();
            $dom->loadXML(file_get_contents($path));
            $symbols = $dom->getElementsByTagName('symbol');
            foreach ($symbols as $symbol) {
                foreach ($symbol->attributes as $attribute){
                    if ($attribute->name == 'id'){
                        $icons[$attribute->value] = $attribute->value;
                        break;
                    }
                }
            }
            asort($icons,SORT_NATURAL );
        }
        return $icons;
    }

    public function getDefaultIcon()
    {
        return $this->defaultIcon;
    }

    public function getIconFile()
    {
        return $this->iconFile;
    }
}
