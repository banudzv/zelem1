<?php

namespace App\Components\Widget;

use Illuminate\Http\Request;

abstract class BasicWidget implements AbstractWidget
{
    abstract public function render();

    protected function getRequest(): Request
    {
        return request();
    }
}