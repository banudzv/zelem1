<?php

namespace App\Components\Mailer\Drivers;

use App\Components\Mailer\MailerAbstract;
use App\Components\Mailer\MailerFactory;
use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;

/**
 * Class SendPulse
 *
 * @package App\Components\Mailer\Drivers
 */
class SendPulse extends MailerAbstract implements MailerFactory
{
    /**
     * @var ApiClient
     */
    private $client;
    
    /**
     * @var string
     */
    private $addressBookName;
    
    /**
     * SendPulse constructor.
     *
     * @param bool $isMassMailing
     * @throws \Exception
     */
    public function __construct(bool $isMassMailing = false)
    {
        $mailConfig = $isMassMailing ? 'mass-mailing' : 'mail';
        $this->client = new ApiClient(
            config('db.' . $mailConfig . '.sendpulse_user_id', config('services.sendpulse.user_id')),
            config('db.' . $mailConfig . '.sendpulse_secret', config('services.sendpulse.secret')),
            new FileStorage(storage_path('logs'))
        );
        $this->addressBookName = config('db.' . $mailConfig . '.sendpulse_book', 'Locotrade');
    }
    
    /**
     * @param array $parameters
     * @throws \Throwable
     */
    public function sendMass(array $parameters = [])
    {
        // check address book
        $addressBooks = $this->client->listAddressBooks();
        $currentAddressBook = null;
        foreach ($addressBooks as $addressBook) {
            if ($addressBook->name === $this->addressBookName) {
                $currentAddressBook = $addressBook;
            }
        }
        // create address book if they not exist
        if (!$currentAddressBook) {
            $bookId = $this->client->createAddressBook($this->addressBookName);
            $currentAddressBook = $this->client->getBookInfo($bookId->id)[0];
        }
        // check emails in address book and database
        $emails = (array)$this->client->getEmailsFromBook($currentAddressBook->id);
        $notExistingEmails = [];
        foreach ($emails as $email) {
            if (!in_array($email->email, $this->recipients)) {
                $notExistingEmails[] = $email->email;
            } else {
                $pos = array_search($email->email, $this->recipients);
                unset($this->recipients[$pos]);
            }
        }
        // remove not existing emails from address book
        $this->client->removeEmails($currentAddressBook->id, $notExistingEmails);
        // adding new emails to address book
        $this->client->addEmails($currentAddressBook->id, $this->recipients);
        sleep(2);
        $fromName = config('db.mass-mailing.sendpulse_from_name');
        $fromEmail = config('db.mass-mailing.sendpulse_from_email');
        $subject = $this->subject;
        $body = $this->body;
        $html = view($this->markdown, $parameters + [
            'content' => $body,
            'subject' => $subject,
        ])->render();
        $response = $this->client->createCampaign($fromName, $fromEmail, $subject, $html, $currentAddressBook->id);
        if ($response && isset($response->is_error) && $response->is_error === true) {
            throw new \Exception($response->message ?? 'Unknown error');
        }
    }
    
    /**
     * @param array $parameters
     * @throws \Throwable
     */
    public function sendOne(array $parameters = [])
    {
        $subject = $this->subject;
        $body = $this->body;
        foreach ($this->recipients as $recipient) {
            $email = [
                'html' => view($this->markdown, $parameters + [
                    'content' => $body,
                    'subject' => $subject,
                ])->render(),
                'text' => strip_tags($body),
                'subject' => $subject,
                'from' => [
                    'name' => config('db.mail.sendpulse_from_name'),
                    'email' => config('db.mail.sendpulse_from_email'),
                ],
                'to' => [['email' => $recipient]],
            ];
            $response = $this->client->smtpSendMail($email);
            if ($response && $response->is_error === true) {
                throw new \Exception($response->message ?? 'Unknown error');
            }
        }
    }
    
    /**
     * @param array $parameters
     * @throws \Throwable
     */
    public function send(array $parameters = [])
    {
        $mailConfig = !empty($parameters['isMassMailing']) && $parameters['isMassMailing'] === true ? 'mass-mailing' : 'mail';
        switch ($mailConfig) {
            case 'mass-mailing':
                return $this->sendMass($parameters);
            default:
                return $this->sendOne($parameters);
        }
    }
    
}
