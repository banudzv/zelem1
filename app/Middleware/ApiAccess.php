<?php

namespace App\Middleware;

use Closure;

/**
 * Class ActiveUser
 *
 * @package App\Middleware
 */
class ApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isLocal()) {
            abort_unless($request->header('Accept') === 'application/json', 404);
            abort_unless(env('API_KEY'), 403);
            $authorization = $request->header('X-Authorization');
            abort_if($authorization !== env('API_KEY'), 403);
        }
        return $next($request);
    }
}
