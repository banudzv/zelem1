<?php

namespace App\Middleware;

use App\Core\AjaxTrait;
use App\Modules\Blacklist\Models\EmailBans;
use App\Modules\Blacklist\Models\IpBans;
use Closure;

class Blacklist
{
    use AjaxTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('_token')){
            $email = $request->get('email');
            if ($email){
                $isEmailBlacklisted = EmailBans::getBanned($email);
                if ($isEmailBlacklisted){
                    return $this->errorJsonAnswer([
                        'notyMessage' => __('blacklist::site.general.email-is-in-blacklist'),
                    ]);
                }
            }
            $ip = $request->ip();
            if ($ip){
                $isEmailBlacklisted = IpBans::getBanned($ip);
                if ($isEmailBlacklisted){
                    return $this->errorJsonAnswer([
                        'notyMessage' => __('blacklist::site.general.ip-is-in-blacklist'),
                    ]);
                }
            }
        }
        return $next($request);
    }
}
