<?php

namespace App\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class ProviderLoader
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach (config('app.providers') as $providerClass) {
            $provider = app()->resolveProvider($providerClass);

            $this->loadForSite($provider);
            $this->loadForAdmin($provider);
        }

        return $next($request);
    }

    private function loadForSite(ServiceProvider $provider)
    {
        if (method_exists($provider, 'loadForSite')) {
            $provider->loadForSite();
        }
    }

    private function loadForAdmin(ServiceProvider $provider)
    {
        if (method_exists($provider, 'loadForAdmin')) {
            $provider->loadForAdmin();
        }
    }
}