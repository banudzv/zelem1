<?php namespace App\Helpers;

use Illuminate\Support\Arr;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SheetsXls
{
    const EXTENTION = 'xlsx';

    private $spreadsheet;

    /**
     * SheetsXls constructor.
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function __construct()
    {
        $this->spreadsheet = new Spreadsheet();
        $this->spreadsheet->removeSheetByIndex(0);
    }

    /**
     * @param $items
     * @param $fields
     * @param $title
     * @param $tab_index
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function addSheet($items, $fields, $title, $tab_index)
    {
        $sheet = new Worksheet($this->spreadsheet, $title);
        $columns = $sheet->getHighestDataColumn();

        $row_counter = 1;
        foreach ($fields as $key => $value) {
            $range[] = $columns;
            $sheet->setCellValue($columns++ . $row_counter, $value);
        }

        foreach ($items as $item) {
            $row_counter++;
            $columns = $sheet->getHighestDataColumn($row_counter);
            foreach ($item as $value) {
                if (is_array($value)) {
                    if (count($value)) {
                        foreach ($value as $v) {
                            $sheet->setCellValueExplicit($columns++ . $row_counter, array_shift($v), DataType::TYPE_STRING);
                        }
                    }
                } else {
                    $sheet->setCellValueExplicit($columns++ . $row_counter, $value, DataType::TYPE_STRING);
                }
            }
        }

        $this->spreadsheet->addSheet($sheet, $tab_index);

        /** Set auto width */
        foreach ($range as $col) {
            $this->spreadsheet->getSheet($tab_index)
                ->getColumnDimension($col)
                ->setAutoSize(true);
            $this->spreadsheet->getSheet($tab_index)->getStyle($col)->getAlignment()->setWrapText(true);
        }
    }

    /**
     * @param string $filename
     * @param string $path
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function getFile($filename = 'file', $path = 'php://output')
    {
        $this->spreadsheet->setActiveSheetIndex(0);
        /** HTTP-headers */
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-Disposition: attachment; filename=" . $filename . "." . static::EXTENTION);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        $writer = new Xlsx($this->spreadsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($path . $filename . "." . static::EXTENTION);

        return $filename . "." . static::EXTENTION;
    }
}
