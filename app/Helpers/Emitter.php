<?php

namespace App\Helpers;

use GuzzleHttp\Client as cURL;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Emitter
 *
 * @package App\Helpers
 */
class Emitter
{
    
    /**
     * Send request by method GET
     *
     * @param string $endpoint
     * @param array $headers
     * @param string $httpAuth
     * @return mixed
     */
    public static function get(string $endpoint, array $headers = [], ?string $httpAuth = null)
    {
        $curl = new cURL();
        $settings = ['headers' => $headers];
        if ($httpAuth !== null) {
            $settings['curl'] = [CURLOPT_USERPWD => $httpAuth];
        }
        $response = $curl->get($endpoint, $settings);
        return static::generateAnswer($response);
    }
    
    /**
     * Send request by method POST
     *
     * @param string $endpoint
     * @param array $data
     * @param array $headers
     * @param string|null $httpAuth
     * @return mixed
     */
    public static function post(string $endpoint, array $data = [], array $headers = [], ?string $httpAuth = null)
    {
        $curl = new cURL();
        $settings = ['body' => $data, 'headers' => $headers];
        if ($httpAuth !== null) {
            $settings['curl'] = [CURLOPT_USERPWD => $httpAuth];
        }
        $response = $curl->post($endpoint, $settings);
        return static::generateAnswer($response);
    }
    
    /**
     * Generate answer to use from string in body
     *
     * @param ResponseInterface $response
     * @return mixed
     */
    private static function generateAnswer(ResponseInterface $response)
    {
        $body = (string)$response->getBody();
        return json_decode($body, 1) ?? $body;
    }
    
}
