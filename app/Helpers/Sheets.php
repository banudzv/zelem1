<?php namespace App\Helpers;

use Illuminate\Support\Arr;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Sheets
{
    /**
     * @param $items
     * @param $filename
     * @param $fields
     * @param string $extension
     * @param string $enclosure
     * @param string $delimiter
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public static function generateExcel($items, $filename, $fields, $extension = 'xls', $enclosure = '"', $delimiter = ',')
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($filename);

        $columns = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'
        ];

        $row_counter = 1;
        $column_counter = 0;
        foreach ($fields as $key => $value){
            $index = $key;
            if(is_numeric($key)){
                $index = $value;
            }
            $sheet->setCellValue($columns[$column_counter++].$row_counter, $index);
        }
        $row_counter++;

        foreach ($items as $item) {
            $column_counter = 0;
            foreach ($fields as $key => $value){
                $index = $key;
                if(is_numeric($key)){
                    $index = $value;
                }
                $sheet->setCellValueExplicit($columns[$column_counter++].$row_counter, Arr::get($item, $index), DataType::TYPE_STRING);
            }
            $row_counter++;
        }

        /** Set auto width */
        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getStyle($col)->getAlignment()->setWrapText(true);
        }

        /** HTTP-headers */
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-Disposition: attachment; filename=" . $filename . "." . $extension);

        if($extension === 'xlsx') {
            header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            $writer = new Xlsx($spreadsheet);
            $writer->setPreCalculateFormulas(false);
            $writer->save('php://output');
        } elseif($extension === 'csv'){
            header("Content-type: application/CSV");
            $writer = new Csv($spreadsheet);
            //$writer->setInputEncoding('CP1252');
            $writer->setDelimiter($delimiter);
            $writer->setEnclosure($enclosure);
            $writer->save('php://output');
        } else {
            header("Content-type: application/vnd.ms-excel");
            $writer = new Xls($spreadsheet);
            $writer->setPreCalculateFormulas(false);
            $writer->save('php://output');
        }
        die();
    }
}
