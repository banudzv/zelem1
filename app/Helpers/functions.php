<?php

use App\Helpers\ClearStringHelper;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Model;
use WezomAgency\Browserizr;
use WezomAgency\R2D2;

if (!function_exists('delivery_price')) {
    /**
     * @param float $amount
     * @param null|string $delivery
     * @param bool $Kiev
     * @return float
     */
    function delivery_price(float $amount, ?string $delivery = null, bool $Kiev = false)
    {
        if ($Kiev && $delivery === 'zalem-address') {
            return config('db.orders.zalem-kiev', 50);
        } elseif ($delivery === 'nova-poshta') {
            return config('db.orders.np-courier', 100);
        } elseif ($amount > 1000) {
            return config('db.orders.ot-1000', 0);
        } else {
            return config('db.orders.do-1000', 0);
        }
    }
}

if (!function_exists('is_kiev')) {
    /**
     * @param string $ref
     * @return bool
     */
    function is_kiev(string $ref)
    {
        return $ref === '8d5a980d-391c-11dd-90d9-001a92567626';
    }
}

if (!function_exists('calc')) {
    /**
     * @param float $amount
     * @return float
     */
    function calc(float $amount)
    {
        return Catalog::currency()->calculate($amount);
    }
}

if (!function_exists('format_for_site')) {
    /**
     * @param float $amount
     * @return float
     */
    function format_for_site(float $amount)
    {
        return Catalog::currency()->format($amount);
    }
}

if (!function_exists('format_only')) {
    /**
     * @param float $amount
     * @return float
     */
    function format_only(float $amount)
    {
        return Catalog::currency()->formatWithoutCalculation($amount);
    }
}

if (!function_exists('format_for_admin')) {
    /**
     * @param float $amount
     * @return float
     */
    function format_for_admin(float $amount)
    {
        return Catalog::currency()->formatForAdmin($amount);
    }
}

if (!function_exists('format')) {
    /**
     * @param float $amount
     * @return float
     */
    function format(float $amount)
    {
        if (Catalog::currenciesLoaded()) {
            return config('app.place') === 'site'
                ? format_for_site($amount)
                : format_for_admin($amount);
        }

        return $amount;
    }
}

if (!function_exists('admin_asset')) {
    /**
     * Generate an asset path for the application.
     *
     * @param string $path
     * @param bool $secure
     * @return string
     */
    function admin_asset($path, $secure = null)
    {
        return asset(config('app.admin_panel_prefix') . '/' . ltrim($path, '/'), $secure);
    }
}

if (!function_exists('admin_url')) {
    /**
     * Generate a url for the application.
     *
     * @param string $path
     * @param mixed $parameters
     * @param bool $secure
     * @return UrlGenerator|string
     */
    function admin_url($path = null, $parameters = [], $secure = null)
    {
        return url(config('app.admin_panel_prefix') . '/' . ltrim($path, '/'), $parameters, $secure);
    }
}

if (!function_exists('site_media')) {
    /**
     * @param string $path
     * @param bool $version
     * @param bool $secure
     * @param bool $absolute
     * @return string
     */
    function site_media(string $path, bool $version = false, bool $secure = false, bool $absolute = false)
    {
        $browserPath = '/' . trim($path, '/');
        if ($version) {
            $fsPath = site_fs_path($path);
            if (is_file($fsPath)) {
                $browserPath = $browserPath . '?v=' . filemtime($fsPath);
            }
        }
        if ($secure || $absolute) {
            $browserPath = asset($browserPath, $secure);
        }
        return $browserPath;
    }
}

if (!function_exists('site_fs_path')) {
    /**
     * @param string $path
     * @return string
     */
    function site_fs_path(string $path)
    {
        $clean_path = explode('?', $path)[0];
        $clean_path = explode('#', $clean_path)[0];
        $clean_path = 'site/' . trim($clean_path, '/');
        return (public_path($clean_path));
    }
}

if (!function_exists('site_get_contents')) {
    /**
     * @param string $path
     * @return string
     */
    function site_get_contents(string $path)
    {
        $path = site_fs_path($path);
        if (is_file($path)) {
            return file_get_contents($path);
        }
        return '';
    }
}

if (!function_exists('site_plural')) {
    /**
     * @param int $number
     * @param array $words
     * @return string
     */
    function site_plural(int $number, array $words = ['товар', 'товара', 'товаров'])
    {
        $ar = [2, 0, 1, 1, 1, 2];
        return $words[($number % 100 > 4 && $number % 100 < 20) ? 2 : $ar[min($number % 10, 5)]];
    }
}

if (!function_exists('isEnv')) {
    /**
     * Checks application environment
     *
     * @param string $environment
     * @return string
     */
    function isEnv(string $environment)
    {
        return env('APP_ENV') === $environment;
    }
}

if (!function_exists('isDemo')) {
    /**
     * Checks if application environment is `demo`
     *
     * @return string
     */
    function isDemo()
    {
        return isEnv('demo');
    }
}

if (!function_exists('isLocal')) {
    /**
     * Checks if application environment is `local`
     *
     * @return string
     */
    function isLocal()
    {
        return isEnv('local');
    }
}

if (!function_exists('isProd')) {
    /**
     * Checks if application environment is `production`
     *
     * @return string
     */
    function isProd()
    {
        return isEnv('production');
    }
}

if (!function_exists('browserizr')) {
    /**
     * Get Browserizr instance
     * @return Browserizr
     */
    function browserizr()
    {
        return Browserizr::detect();
    }
}

if (!function_exists('r2d2')) {
    /**
     * Get R2D2 instance
     * @return R2D2
     */
    function r2d2()
    {
        return R2D2::eject();
    }
}


if (!function_exists('isSecure')) {
    /**
     * Checks if application environment using https
     *
     * @return string
     */
    function isSecure()
    {
        return env('PROTOCOL') == 'https' ? true : false;
    }
}


if (!function_exists('isTest')) {
    /**
     * Checks if application environment is `test`
     *
     * @return string
     */
    function isTest()
    {
        return isEnv('test');
    }
}
if (!function_exists('strReplaceFirst')) {
    /**
     *  Replace first strpos in string for duplicate domains url with locale
     * @param $search
     * @param $replace
     * @param $text
     * @return mixed
     */
    function strReplaceFirst($search, $replace, $text)
    {
        $pos = strpos($text, $search);
        return $pos !== false ? substr_replace($text, $replace, $pos, strlen($search)) : $text;
    }
}

if (!function_exists('get_current_locale_slug')) {
    function get_current_locale_slug(): string
    {
        if (config('app.place') === 'site') {
            return Lang::getLocale();
        }

        return config('app.default-language', app()->getLocale());
    }
}

if (!function_exists('get_current_running_time')) {
    function get_current_running_time()
    {
        return (microtime(true) - LARAVEL_START);
    }
}

if (!function_exists('dump_current_running_time')) {
    function dump_current_running_time($message = null)
    {
        if ($message) {
            return dump($message . ': ' . get_current_running_time());
        }
        return dump(get_current_running_time());
    }
}

if (!function_exists('dd_current_running_time')) {
    function dd_current_running_time($message = null)
    {
        if ($message) {
            dd($message . ': ' . get_current_running_time());
        }
        dd(get_current_running_time());
    }
}

if (!function_exists('array_to_json')) {
    /**
     * @param array $array
     *
     * @return string
     */
    function array_to_json(array $array)
    {
        return json_encode($array);
    }
}

if (!function_exists('json_to_array')) {
    /**
     * @param string $data
     *
     * @return mixed
     */
    function json_to_array(string $data)
    {
        return json_decode($data, true);
    }
}

if (!function_exists('sort_models_by_ids_order')) {
    function sort_models_by_ids_order(array $ids): Closure
    {
        return function (Model $model) use ($ids) {
            return array_search($model->getKey(), array_values($ids));
        };
    }
}

if (!function_exists('str_clear')) {
    function str_clear(string $string)
    {
        return resolve(ClearStringHelper::class)->clear($string);
    }
}

if (!function_exists('str_clear_all')) {
    function str_clear_all(array $strings)
    {
        return resolve(ClearStringHelper::class)->clearAll($strings);
    }
}

if (!function_exists('siteMediaForAdmin')) {
    /**
     * @param string $path
     * @param bool $version
     * @param bool $secure
     * @param bool $absolute
     * @return string
     */
    function siteMediaForAdmin(?string $path, bool $version = false, bool $secure = false, bool $absolute = false)
    {
        if (!$path) {
            return '';
        }
        $browserPath = '/' . trim($path, '/');
        if ($version) {
            $fsPath = _fs_path('site' . $path);
            if (is_file($fsPath)) {
                $browserPath = $browserPath . '?v=' . filemtime($fsPath);
            }
        }
        if ($secure || $absolute) {
            $browserPath = asset($browserPath, $secure);
        }
        return $browserPath;
    }
}

if (!function_exists('_fs_path')) {
    /**
     * @param string $path
     * @param string $appFolder
     * @return string
     */
    function _fs_path(string $path, string $appFolder = '')
    {
        $clean_path = explode('?', $path)[0];
        $clean_path = explode('#', $clean_path)[0];
        $clean_path = $appFolder . '/' . trim($clean_path, '/');
        return (public_path($clean_path));
    }
}

if (!function_exists('text_clear')) {
    function text_clear(string $text): string
    {
        $text = preg_replace('/\s+/', ' ', $text);
        $text = str_replace(['-', '.', '_', ','], '', $text);

        return \Illuminate\Support\Str::lower($text);
    }
}