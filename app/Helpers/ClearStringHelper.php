<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class ClearStringHelper
{
    protected $search = [' ', '-', '_', '.', ',', '/'];

    public function clearAll(iterable $strings): array
    {
        $result = [];

        foreach ($strings as $string) {
            $result[] = $this->clear($string);
        }

        return array_unique($result);
    }

    public function clear(string $name)
    {
        return Str::lower(
            str_replace($this->search, '', Str::slug($name))
        );
    }
}