<?php

namespace App\Widgets\Site;

use App\Components\Widget\AbstractWidget;

/**
 * Class ColorsSchema
 *
 * @package App\Widgets\Site
 */
class ColorsSchema implements AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        return view('site._widgets.variables', [
            'main' => config('db.colors.main') ?: '#60bc4f',
            'mainLighten' => config('db.colors.main-lighten') ?: '#68ca56',
            'mainDarken' => config('db.colors.main-darken') ?: '#59ad49',
            'secondary' => config('db.colors.secondary') ?: '#f7931d',
            'secondaryLighten' => config('db.colors.secondary-lighten') ?: '#f7b21d',
            'secondaryDarken' => config('db.colors.secondary-darken') ?: '#e84d1a',
            /** 2019-08-09 **/
            'mainHeaderFooter' => config('db.colors.main-header-footer') ?: '#702f70',
            'secondaryHeaderFooter' => config('db.colors.secondary-header-footer') ?: '#1db377',
            'brandsHover' => config('db.colors.brands-hover') ?: '#332d33',
            'vendorCode' => config('db.colors.vendor-code') ?: '#1db377',
            'itemHover' => config('db.colors.item-hover') ?: '#faff91',
            'categoriesHover' => config('db.colors.categories-hover') ?: '#702f70',
        ]);
    }
    
}
