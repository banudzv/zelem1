<?php

namespace App\Widgets\Site;

use App\Components\Widget\AbstractWidget;
use Auth;

/**
 * Class ColorPanel
 *
 * @package App\Widgets\Site
 */
class ColorsPanel implements AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        if (Auth::guard('admin')->guest()) {
            return null;
        }
        return view('site.administrator.color-panel', [
            'colors' => [
                [
                    'label' => 'main',
                    'name' => trans('settings::general.colors.main'),
                    'value' => config('db.colors.main') ?: '#60bc4f',
                ],
                [
                    'label' => 'main-lighten',
                    'name' => trans('settings::general.colors.main-lighten'),
                    'value' => config('db.colors.main-lighten') ?: '#68ca56',
                ],
                [
                    'label' => 'main-darken',
                    'name' => trans('settings::general.colors.main-darken'),
                    'value' => config('db.colors.main-darken') ?: '#59ad49',
                ],
                [
                    'label' => 'secondary',
                    'name' => trans('settings::general.colors.secondary'),
                    'value' => config('db.colors.secondary') ?: '#f7931d',
                ],
                [
                    'label' => 'secondary-lighten',
                    'name' => trans('settings::general.colors.secondary-lighten'),
                    'value' => config('db.colors.secondary-lighten') ?: '#f7b21d',
                ],
                [
                    'label' => 'secondary-darken',
                    'name' => trans('settings::general.colors.secondary-darken'),
                    'value' => config('db.colors.secondary-darken') ?: '#e84d1a',
                ],
                [
                    'label' => 'main-header-footer',
                    'name' => trans('settings::general.colors.main-header-footer'),
                    'value' => config('db.colors.main-header-footer') ?: '#776677',
                ],
                [
                    'label' => 'secondary-header-footer',
                    'name' => trans('settings::general.colors.secondary-header-footer'),
                    'value' => config('db.colors.secondary-header-footer') ?: '#194563',
                ],
                [
                    'label' => 'vendor-code',
                    'name' => trans('settings::general.colors.vendor-code'),
                    'value' => config('db.colors.vendor-code') ?: '#775698',
                ],
                [
                    'label' => 'brands-hover',
                    'name' => trans('settings::general.colors.brands-hover'),
                    'value' => config('db.colors.brands-hover') ?: '#555332',
                ],
                [
                    'label' => 'item-hover',
                    'name' => trans('settings::general.colors.item-hover'),
                    'value' => config('db.colors.item-hover') ?: '#49aacc',
                ],
                [
                    'label' => 'categories-hover',
                    'name' => trans('settings::general.colors.categories-hover'),
                    'value' => config('db.colors.categories-hover') ?: '#555864',
                ]
            ],
        ]);
    }
    
}
