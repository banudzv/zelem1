@php
/** @var \App\Core\Modules\Languages\Models\Language[]|\Illuminate\Database\Eloquent\Collection $languages */
@endphp
@foreach($languages as $language)
<div class="header-line-item">
    {!! Html::link($language->current_url_with_new_language, $language->slug, [
        'class' => ['header-line-item__link', $language->is_current ? 'header-line-item__link--current' : ''],
    ]) !!}
</div>
@endforeach
