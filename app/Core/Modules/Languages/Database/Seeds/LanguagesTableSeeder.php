<?php

namespace App\Core\Modules\Languages\Database\Seeds;

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [];
        foreach (explode(';', env('LANGUAGES', 'Русский,ru,1,ru')) as $lang) {
            $languages[] = explode(',', $lang);
        }
        foreach ($languages AS $lang) {
            $model = new \App\Core\Modules\Languages\Models\Language();
            $model->name = array_get($lang, 0, 'Unknown');
            $model->slug = array_get($lang, 1, 'uu');
            $model->default = (bool)array_get($lang, 2, false);
            $model->google_slug = array_get($lang, 3, $model->slug);
            $model->save();
        }
    }
}
