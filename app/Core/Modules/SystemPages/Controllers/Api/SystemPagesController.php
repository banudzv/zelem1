<?php

namespace App\Core\Modules\SystemPages\Controllers\Api;

use App\Core\ApiController;
use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\Modules\SystemPages\Resources\SystemPageFullResource;

/**
 * Class SystemPagesController
 *
 * @package App\Core\Modules\SystemPages\Controllers\Api
 */
class SystemPagesController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/system-pages/all",
     *     tags={"SystemPages"},
     *     summary="Returns all system pages with all data",
     *     operationId="getSystemPgesFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/SystemPageFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return SystemPageFullResource::collection(SystemPage::with(['data'])->get());
    }
}
