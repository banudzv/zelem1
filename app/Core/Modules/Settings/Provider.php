<?php

namespace App\Core\Modules\Settings;

use App\Components\Delivery\NovaPoshta;
use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use CustomForm\Input;
use CustomForm\Macro\ColorPicker;
use CustomForm\Macro\Toggle;
use CustomForm\Select;
use CustomForm\WysiHtml5;
use CustomMenu, CustomRoles, CustomSettings;

/**
 * Class Provider
 * Settings service provider
 *
 * @package App\Core\Modules\Settings
 */
class Provider extends BaseProvider
{

    protected function presets()
    {
    }

    /**
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register menu
        CustomMenu::get()->group()
            ->link('settings::general.menu', RouteObjectValue::make('admin.settings.index'), 'fa fa-cogs')
            ->setPosition(11)
            ->additionalRoutesForActiveDetect(RouteObjectValue::make('admin.settings.group'));
        // Register role scopes
        CustomRoles::add('settings', 'settings::general.menu')
            ->only(RoleRule::INDEX, RoleRule::UPDATE, RoleRule::VIEW)
            ->addCustomPolicy('group', RoleRule::UPDATE);

        $settings = CustomSettings::createAndGet('justin', 'settings::justin.settings-name');
        $settings->add(
            Input::create('login')->setLabel('settings::justin.login'),
            ['required', 'string']
        );
        $settings->add(
            Input::create('password')->setLabel('settings::justin.password'),
            ['required', 'string']
        );
        $settings->add(
            Input::create('key')->setLabel('settings::justin.key'),
            ['required', 'string']
        );
        $this->liqPaySettings();
        CustomSettings::createAndGet('nova-poshta', 'settings::nova-poshta.settings-name', -9999);
        $this->logoSettings();
        $this->watermarkSettings();
     //   $this->slogan();
        //$this->colorsSchema();
        $this->amoCRM();
    }

    /**
     * @throws \App\Exceptions\WrongParametersException
     */
    private function liqPaySettings(): void
    {
        $settings = CustomSettings::createAndGet('liqpay', 'settings::liqpay.settings-name');
        $settings->add(
            Input::create('public-key')->setLabel('settings::liqpay.public-key'),
            ['nullable', 'string']
        );
        $settings->add(
            Input::create('private-key')->setLabel('settings::liqpay.private-key'),
            ['nullable', 'string']
        );
        $settings->add(
            Toggle::create('test')->setLabel('settings::liqpay.test')->required(),
            ['required', 'boolean']
        );
    }

    /**
     * @throws \App\Exceptions\WrongParametersException
     */
    private function amoCRM(): void
    {
        $settings = CustomSettings::createAndGet('amo_crm', 'settings::amo-crm.settings-name', -9999);
        $settings->add(
            Toggle::create('use_amo')
                ->setLabel('settings::amo-crm.use_amo')
                ->setDefaultValue(config('db.amo_crm.use_amo', false))
                ->setValue(config('db.amo_crm.use_amo', false))
                ->required(),
            ['required', 'boolean']
        );
        $settings->add(
            Input::create('domain')->setLabel('settings::amo-crm.domain'),
            ['nullable', 'string']
        );
        $settings->add(
            Input::create('login')->setLabel('settings::amo-crm.login'),
            ['nullable', 'string']
        );
        $settings->add(
            Input::create('hash')->setLabel('settings::amo-crm.hash'),
            ['nullable', 'string']
        );
        $settings->add(
            Input::create('status_id')->setLabel('settings::amo-crm.status_id'),
            ['nullable', 'integer', 'min:1']
        );
        $settings->add(
            Input::create('pipeline_id')->setLabel('settings::amo-crm.pipeline_id'),
            ['nullable', 'integer', 'min:1']
        );
        $settings->add(
            Input::create('responsible_user_id')->setLabel('settings::amo-crm.responsible_user_id'),
            ['nullable', 'integer', 'min:1']
        );
        $settings->add(
            Input::create('roistat_visit')->setLabel('settings::amo-crm.roistat_visit'),
            ['nullable', 'integer', 'min:1']
        );
        $settings->add(
            Input::create('phone_id')->setLabel('settings::amo-crm.phone_id'),
            ['nullable', 'integer', 'min:1']
        );
        $settings->add(
            Input::create('email_id')->setLabel('settings::amo-crm.email_id'),
            ['nullable', 'integer', 'min:1']
        );
    }

    /**
     * @throws \App\Exceptions\WrongParametersException
     */
    private function logoSettings(): void
    {
        CustomSettings::createAndGet('logo', 'settings::logo.settings-name', -9999);
    }

    private function watermarkSettings(): void
    {
        CustomSettings::createAndGet('watermark', 'settings::watermark.settings-name', -9999);
    }

    /**
     * @throws \App\Exceptions\WrongParametersException
     */
    private function colorsSchema(): void
    {
        $settings = CustomSettings::createAndGet('colors', 'settings::general.colors.settings');

        $colorRule = 'regex:/^#[a-zA-Z0-9]{6}$/i';

        $settings->add(
            Input::create('main')
                ->setType('color')
                ->setDefaultValue('#60bc4f')
                ->setLabel('settings::general.colors.main')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('main-lighten')
                ->setType('color')
                ->setDefaultValue('#68ca56')
                ->setLabel('settings::general.colors.main-lighten')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('main-darken')
                ->setType('color')
                ->setDefaultValue('#59ad49')
                ->setLabel('settings::general.colors.main-darken')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('secondary')
                ->setType('color')
                ->setDefaultValue('#f7931d')
                ->setLabel('settings::general.colors.secondary')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('secondary-lighten')
                ->setType('color')
                ->setDefaultValue('#f7b21d')
                ->setLabel('settings::general.colors.secondary-lighten')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('main-header-footer')
                ->setType('color')
                ->setDefaultValue('#e84d1a')
                ->setLabel('settings::general.colors.main-header-footer')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('secondary-header-footer')
                ->setType('color')
                ->setDefaultValue('#777777')
                ->setLabel('settings::general.colors.secondary-header-footer')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('brands-hover')
                ->setType('color')
                ->setDefaultValue('#555332')
                ->setLabel('settings::general.colors.brands-hover')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('vendor-code')
                ->setType('color')
                ->setDefaultValue('#775698')
                ->setLabel('settings::general.colors.secondary-header-footer')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('item-hover')
                ->setType('color')
                ->setDefaultValue('#49aacc')
                ->setLabel('settings::general.colors.item-hover')
                ->required(),
            ['required', $colorRule]
        );
        $settings->add(
            Input::create('categories-hover')
                ->setType('color')
                ->setDefaultValue('#556488')
                ->setLabel('settings::general.colors.categories-hover')
                ->required(),
            ['required', $colorRule]
        );
    }

}
