<?php

use Illuminate\Support\Facades\Route;

Route::get('settings/all', 'SettingsController@all');
Route::get('files/all', 'FilesController@all');
Route::get('logo', 'FilesController@logo');
Route::get('favicons', 'FilesController@favicons');
