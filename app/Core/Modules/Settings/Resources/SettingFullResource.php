<?php

namespace App\Core\Modules\Settings\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SettingFullResource
 *
 * @package App\Core\Modules\Settings\Resources
 *
 * @OA\Schema(
 *   schema="SettingFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"group", "alias", "value"},
 *           @OA\Property(property="group", type="string", description="Group of settings"),
 *           @OA\Property(property="alias", type="string", description="Setting name alias"),
 *           @OA\Property(property="value", type="string", description="Setting value"),
 *       )
 *   }
 * )
 */
class SettingFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->toArray();
    }
}
