<?php

namespace App\Core\Modules\Settings\Controllers\Api;

use App\Core\ApiController;
use File;

/**
 * Class FilesController
 *
 * @package App\Core\Modules\Settings\Controllers\Api
 */
class FilesController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/files/all",
     *     tags={"Settings"},
     *     summary="Returns all links to existed uploaded files by filemanager",
     *     operationId="getFilesFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(
     *               type="string",
     *           )
     *         )
     *     ),
     * )
     */
    public function all()
    {
        $URIs = [];
        $rootFolder = storage_path('app/public/files/filemanager');
        foreach (File::allFiles($rootFolder) as $file) {
            $localPath = str_replace($rootFolder, '', $file->getRealPath());
            $URIs[] = url('storage/files/filemanager/' . ltrim($localPath, '/'));
        }
        return $URIs;
    }
    
    /**
     * @OA\Get(
     *     path="/api/logo",
     *     tags={"Settings"},
     *     summary="Returns desktop and mobile logos",
     *     operationId="getLogo",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="object",
     *              allOf={
     *                  @OA\Schema(
     *                       required={"desktop", "mobile"},
     *                       @OA\Property(property="desktop", type="string", description="Logo for desktop"),
     *                       @OA\Property(property="mobile", type="string", description="Logo for mobile"),
     *                  )
     *              }
     *         )
     *     ),
     * )
     */
    public function logo()
    {
        return [
            'desktop' => url('storage/logo.png'),
            'mobile' => url('storage/logo--mobile.png'),
        ];
    }
    
    /**
     * @OA\Get(
     *     path="/api/favicons",
     *     tags={"Settings"},
     *     summary="Returns all favicons",
     *     operationId="getFavicons",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(
     *               type="string",
     *           )
     *         )
     *     ),
     * )
     */
    public function favicons()
    {
        $URIs = [];
        $rootFolder = storage_path('app/public/favicons');
        if (is_dir($rootFolder) === false) {
            return [];
        }
        foreach (File::allFiles($rootFolder) as $file) {
            $localPath = str_replace($rootFolder, '', $file->getRealPath());
            $URIs[] = url('storage/favicons/' . ltrim($localPath, '/'));
        }
        return $URIs;
    }
}
