<?php

namespace App\Core\Modules\Settings\Controllers\Api;

use App\Core\ApiController;
use App\Core\Modules\Settings\Models\Setting;
use App\Core\Modules\Settings\Resources\SettingFullResource;

/**
 * Class SettingsController
 *
 * @package App\Core\Modules\Settings\Controllers\Api
 */
class SettingsController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/settings/all",
     *     tags={"Settings"},
     *     summary="Returns all existed edittable site and admin panel settings",
     *     operationId="getSettingsFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/SettingFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return SettingFullResource::collection(Setting::all());
    }
}
