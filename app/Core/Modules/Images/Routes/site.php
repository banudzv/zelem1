<?php

use Illuminate\Support\Facades\Route;

// Frontend routes
Route::get('uploads/images/{size}/{image}', ['uses' => 'IndexController@image', 'as' => 'site.image.cache']);
