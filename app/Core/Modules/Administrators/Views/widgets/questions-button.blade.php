@php
/** @var $link string*/
/** @var $name string */
@endphp

<li class="dropdown user user-menu">
    <a href="{{ $link }}" target="_blank" style="color:#dd4b39">{{$name}}</a>
</li>
