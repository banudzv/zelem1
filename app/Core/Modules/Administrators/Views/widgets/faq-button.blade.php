@php
/** @var $link string*/
/** @var $name string */
@endphp

<li class="dropdown user user-menu">
    <a href="{{ $link }}" target="_blank">{{$name}}</a>
</li>
