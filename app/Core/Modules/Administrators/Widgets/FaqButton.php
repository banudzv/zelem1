<?php

namespace App\Core\Modules\Administrators\Widgets;

use App\Components\Widget\AbstractWidget;
use Auth, CustomMenu;

class FaqButton implements AbstractWidget
{

    public function render()
    {
        return view(
            'admins::widgets.faq-button', [
                'link' => 'https://locotrade.com.ua/faq',
                'name' => __('admin.button.help-center'),
            ]
        );
    }

}
