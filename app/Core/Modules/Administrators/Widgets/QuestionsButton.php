<?php

namespace App\Core\Modules\Administrators\Widgets;

use App\Components\Widget\AbstractWidget;
use Auth, CustomMenu;

class QuestionsButton implements AbstractWidget
{

    public function render()
    {
        return view(
            'admins::widgets.questions-button', [
                'link' => 'https://ticket.wezom.agency/index.php',
                'name' => __('admin.button.have-questions'),
            ]
        );
    }

}
