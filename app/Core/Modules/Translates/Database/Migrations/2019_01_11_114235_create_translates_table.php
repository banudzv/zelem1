<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('module')->nullable();
            $table->string('place')->nullable();
            $table->text('text')->nullable();
            $table->text('help')->nullable();
            $table->string('language', 3);
            $table->timestamps();

            $table->foreign('language')->references('slug')->on('languages')
                ->index('translates_language_languages_slug')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (app()->environment() !== 'production') {
            Schema::table('translates', function (Blueprint $table) {
                $table->dropForeign('translates_language_languages_slug');
            });
            Schema::dropIfExists('translates');
        }
    }
}
