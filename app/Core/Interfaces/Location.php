<?php

namespace App\Core\Interfaces;

use Exception;

interface Location
{
    /**
     * get array with lat and lng for location
     * [
     *  'lat' => 89.123456789,
     *  'lng' => 66.123456789,
     *  'zoom' => 6,
     * ]
     *
     * @return array
     *
     * @throws Exception
     */
    public function locationData(): array;
}