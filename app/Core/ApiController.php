<?php

namespace App\Core;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="WEZOM: Locotrade API",
 *      description="Locotrade private API. Made by Wezom IT company",
 *      @OA\Contact(
 *          email="demianenko.v@wezom.com.ua"
 *      )
 * )
 * @OA\Parameter(
 *     parameter="Auth",
 *     name="X-Authorization",
 *     in="header",
 *     required=true,
 *     @OA\Schema(
 *        type="string"
 *     )
 * )
 * @OA\Schema(
 *   schema="Error",
 *   type="object",
 *   description="No access",
 *   allOf={
 *      @OA\Schema(
 *          required={"error", "success"},
 *          @OA\Property(property="error", type="string", description="Error description"),
 *          @OA\Property(property="success", type="boolean", description="Error status"),
 *      )
 *   }
 * )
 * @OA\Schema(
 *   schema="PaginationLinks",
 *   type="object",
 *   description="Includes useful links for pagination",
 *   allOf={
 *      @OA\Schema(
 *          required={"first", "last"},
 *          @OA\Property(property="first", type="integer", description="Link to the first page"),
 *          @OA\Property(property="last", type="integer", description="Link to the last page"),
 *          @OA\Property(property="prev", type="string", description="Link to the previous page (if exists)"),
 *          @OA\Property(property="next", type="string", description="Link to the next page (if exists)"),
 *      )
 *   }
 * )
 * @OA\Schema(
 *   schema="PaginationMeta",
 *   type="object",
 *   description="Includes useful meta data for pagination",
 *   allOf={
 *      @OA\Schema(
 *          required={"current_page", "from", "last_page", "path", "per_page", "to", "total"},
 *          @OA\Property(property="current_page", type="integer", description="Current page number"),
 *          @OA\Property(property="from", type="integer", description="First element number on this page"),
 *          @OA\Property(property="to", type="integer", description="Last element number on this page"),
 *          @OA\Property(property="last_page", type="integer", description="Last page number"),
 *          @OA\Property(property="path", type="string", description="Canonical path to the page (first page)"),
 *          @OA\Property(property="per_page", type="integer", description="Limit of the elements on page"),
 *          @OA\Property(property="total", type="integer", description="The total count of existed elements by request"),
 *      )
 *   }
 * )
 */
class ApiController extends Controller
{

}
