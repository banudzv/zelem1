<?php

namespace App\Console\Commands;

use App\Core\Modules\Images\Models\Image;
use Illuminate\Console\Command;

/**
 * Class FixImageNames
 *
 * @package App\Console\Commands
 */
class FixImageNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:names';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change existing images names by related model names';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Image::cursor() as $image) {
            $image->setStandardName();
        }

        $this->info("All images have been renamed!");
    }
}
