<?php

namespace App\Console\Commands;

use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductTranslates;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Str;

/**
 * Class FixSlugs
 *
 * @package App\Console\Commands
 */
class FixSlugs extends Command
{
    use ConfirmableTrait;

    protected $modulesPath = 'app/Modules';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locotrade:fix-slugs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix products slugs';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $this->updateProductModifications();
    }

    private $slugs = [];
    private function updateProductModifications()
    {
        Product::with('data')->get()->each(function (Product $product) {
            $parts = [];
            $parts[] = $product->group->name;
            if ($product->value_id) {
                $parts[] = $product->value->current->name;
            }
            $name = implode(' ', $parts);
            $slug = $this->generateProductSlug($name);
            foreach (config('languages', []) as $languageAlias => $language) {
                $model = $product->dataFor($languageAlias);
                if ($model !== null) {
                    $model->update([
                        'slug' => $slug,
                    ]);
                } else {
                    ProductTranslates::updateOrCreate([
                        'row_id' => $product->id,
                        'language' => $languageAlias,
                    ], [
                        'name' => '',
                        'slug' => $slug,
                    ]);
                }
            }
        });
    }
    
    /**
     * @param $value
     * @param int $try
     * @return string
     */
    private function generateProductSlug($value, $try = 0)
    {
        $slug = Str::substr(Str::slug($value) . ($try ? '-' . $try : ''), 0, 170);
        if (in_array($slug, $this->slugs)) {
            return $this->generateProductSlug($value, ++$try);
        }
        $this->slugs[] = $slug;
        return $slug;
    }
    
}
