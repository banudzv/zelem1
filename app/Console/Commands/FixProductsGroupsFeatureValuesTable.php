<?php

namespace App\Console\Commands;

use App\Modules\Products\Models\ProductGroupFeatureValue;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;

/**
 * Class FixSlugs
 *
 * @package App\Console\Commands
 */
class FixProductsGroupsFeatureValuesTable extends Command
{
    use ConfirmableTrait;

    protected $modulesPath = 'app/Modules';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locotrade:fix-relations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix products_groups_features_values table';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $this->info(ProductGroupFeatureValue::whereDoesntHave('products')->delete() . ' rows were deleted!');
    }
    
}
