<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Artisan, File;

/**
 * Class CreateForm
 *
 * @package App\Console\Commands
 */
class Youtube extends Command
{
    protected $modulesPath = 'app/Modules';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'youtube:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simple form of Locotrade system installation';

    private $key;
    private $channelId;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

       $channel = config('db.basic.youtube_chanel');
       $channelArr = explode('/', $channel);
       $this->channelId = end($channelArr);
       $this->key = config('db.basic.youtube_api');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {

        $youtube = new \App\Modules\About\Models\Youtube();
        $result = $this->sendRequest('https://www.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails%2Cstatistics&id=' . $this->channelId);

        $youtube->title = $result->items[0]->snippet->title;
        $youtube->subscribers = $result->items[0]->statistics->subscriberCount;

        $result = $this->sendRequest('https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=' . $this->channelId . '&maxResults=4&order=date&type=video');
        $items = [];
        Carbon::setLocale('ru');
        foreach ($result->items as $item) {
            $itemInfo = $this->sendRequest('https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=' . $item->id->videoId);

            $duration = $itemInfo->items[0]->contentDetails->duration;
            preg_match_all('/\d+/', $duration, $times);
            $items[] = [
                'title' => $item->snippet->title,
                'video_id' => $item->id->videoId,
                'duration' => implode(':', $times[0]),
                'date' => strtotime($item->snippet->publishedAt),
            ];
        }
        $youtube->videos = json_encode($items);
        $youtube->save();
    }

    /**
     * @param $url
     * @return mixed|string
     */
    private function sendRequest($url)
    {
        $sendUrl = $url . '&key=' . $this->key;

        $content = file_get_contents($sendUrl);
        return json_decode($content);
    }
}
