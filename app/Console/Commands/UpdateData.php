<?php

namespace App\Console\Commands;

use App\Core\Modules\Images\Models\Image;
use App\Core\Modules\Languages\Models\Language;
use App\Core\Modules\Settings\Models\Setting;
use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\Modules\SystemPages\Models\SystemPageTranslates;
use App\Helpers\Emitter;
use App\Modules\Articles\Models\Article;
use App\Modules\Articles\Models\ArticleTranslates;
use App\Modules\Brands\Models\Brand;
use App\Modules\Brands\Models\BrandTranslates;
use App\Modules\Categories\Models\Category;
use App\Modules\Categories\Models\CategoryTranslates;
use App\Modules\Comments\Models\Comment;
use App\Modules\FastOrders\Models\FastOrder;
use App\Modules\Features\Models\Feature;
use App\Modules\Features\Models\FeatureTranslates;
use App\Modules\Features\Models\FeatureValue;
use App\Modules\Features\Models\FeatureValueTranslates;
use App\Modules\News\Models\News;
use App\Modules\News\Models\NewsTranslates;
use App\Modules\Orders\Models\Order;
use App\Modules\Pages\Models\Page;
use App\Modules\Pages\Models\PageTranslates;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupCategory;
use App\Modules\Products\Models\ProductGroupFeatureValue;
use App\Modules\Products\Models\ProductGroupLabel;
use App\Modules\Products\Models\ProductGroupTranslates;
use App\Modules\Products\Models\ProductTranslates;
use App\Modules\ProductsLabels\Models\Label;
use App\Modules\ProductsLabels\Models\LabelTranslates;
use App\Modules\SiteMenu\Models\SiteMenu;
use App\Modules\SiteMenu\Models\SiteMenuTranslates;
use App\Modules\SlideshowSimple\Models\SlideshowSimple;
use App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates;
use App\Modules\Subscribe\Models\Subscriber;
use App\Modules\Subscribe\Models\SubscriberMails;
use App\Modules\Users\Models\User;
use App\Modules\Wishlist\Models\Wishlist;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use File;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Storage;

/**
 * Class UpdateData
 *
 * @package App\Console\Commands
 */
class UpdateData extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locotrade:update {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate data from other domain';

    /**
     * @var Language[]|Collection
     */
    private $languages;

    /**
     * @var string
     */
    private $host = 'https://showroom.locotrade.com.ua';

    private $apiKey = 'wEAZYH9BMA69JDuqh65pbTNu';

    private $brands = [];
    private $categories = [];
    private $features = [];
    private $featureValues = [];
    private $labels = [];

    /**
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->option('force')) {
            $this->host = $this->host . '/api/';
        } else {
            $this->host = rtrim($this->ask('From what domain to pull the data?', $this->host), '/') . '/api/';
            $this->apiKey = $this->ask('Api key', $this->apiKey);
        }

        $this->languages = Language::all();

        $this->info('Removing data we do not need...');
        Comment::query()->delete();
        Image::query()->delete();
        Order::query()->delete();
        FastOrder::query()->delete();
        User::query()->delete();
        Subscriber::query()->delete();
        SubscriberMails::query()->delete();
        Wishlist::query()->delete();
        $this->removeFiles();

        $this->info('Uploading logo...');
        $this->loadLogo();
        $this->info('Uploading favicons...');
        $this->loadFavicons();
        $this->info('Uploading files for filemanager...');
        $this->loadFiles();

        $this->info('Updating settings...');
        Setting::query()->delete();
        $this->loadSettings();

        $this->info('Updating slider...');
        SlideshowSimple::query()->delete();
        $this->loadSlider();

        $this->info('Updating system pages...');
        SystemPage::query()->delete();
        $this->loadSystemPages();

        $this->info('Updating text pages...');
        Page::query()->delete();
        $this->loadPages();

        $this->info('Updating news...');
        News::query()->delete();
        $this->loadNews();

        $this->info('Updating articles...');
        Article::query()->delete();
        $this->loadArticles();

        $this->info('Updating site menu...');
        SiteMenu::query()->delete();
        $this->loadSiteMenu();

        $this->info('Updating brands...');
        Brand::query()->delete();
        $this->loadBrands();

        $this->info('Updating categories...');
        Category::query()->delete();
        $this->loadCategories();

        $this->info('Updating product labels...');
        Label::query()->delete();
        $this->loadProductLabels();

        $this->info('Updating features and values...');
        Feature::query()->delete();
        $this->loadFeaturesWithValues();

        $this->info('Updating products and all relative data...');
        ProductGroup::query()->delete();
        ProductGroupFeatureValue::query()->delete();
        $this->loadProductsWithAndAllData();
    }

    /**
     * @throws \Exception
     */
    private function loadProductsWithAndAllData()
    {
        $products = $this->curl('products/all');
        if (!$products) {
            return;
        }

        foreach ($products as $data) {
            \DB::beginTransaction();

            // Groups
            $group = new ProductGroup();
            $group->fill($data);
            $group->category_id = array_get($data, 'category_id') ? array_get($this->categories, array_get($data, 'category_id')) : null;
            $group->brand_id = array_get($data, 'brand_id') ? array_get($this->brands, array_get($data, 'brand_id')) : null;
            $group->feature_id = array_get($data, 'feature_id') ? array_get($this->features, array_get($data, 'feature_id')) : null;
            $group->price_min = array_get($data, 'price_min', 0);
            $group->price_max = array_get($data, 'price_max', 0);
            $group->save();

            $this->languages->each(function (Language $language) use ($group, $data) {
                $dataForTranslate = null;
                foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                    if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                        $dataForTranslate = $pageDataForTranslate;
                        break;
                    }
                }
                if (!isset($dataForTranslate)) {
                    return;
                }
                $translate = new ProductGroupTranslates();
                $translate->fill($dataForTranslate);
                $translate->row_id = $group->id;
                $translate->language = $language->slug;
                $translate->save();
            });

            // Modifications
            foreach (array_get($data, 'products') as $productData) {
                $product = new Product();
                $product->fill($productData);
                $product->group_id = $group->id;
                $product->brand_id = array_get($productData, 'brand_id') ? array_get($this->brands, array_get($productData, 'brand_id')) : null;
                $product->value_id = array_get($productData, 'value_id') ? array_get($this->featureValues, array_get($productData, 'value_id')) : null;
                $product->save();

                $this->languages->each(function (Language $language) use ($product, $productData) {
                    $dataForTranslate = null;
                    foreach (array_get($productData, 'data', []) as $pageDataForTranslate) {
                        if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                            $dataForTranslate = $pageDataForTranslate;
                            break;
                        }
                    }
                    if (!isset($dataForTranslate)) {
                        return;
                    }
                    $translate = new ProductTranslates();
                    $translate->fill($dataForTranslate);
                    $translate->row_id = $product->id;
                    $translate->language = $language->slug;
                    $translate->save();
                });

                // Product associated features
                foreach (array_get($data, 'feature_values', []) as $element) {
                    ProductGroupFeatureValue::create([
                        'group_id' => $group->id,
                        'product_id' => $product->id,
                        'feature_id' => array_get($element, 'feature_id') ? array_get($this->features, array_get($element, 'feature_id')) : null,
                        'value_id' => array_get($element, 'value_id') ? array_get($this->featureValues, array_get($element, 'value_id')) : null,
                    ]);
                }

                // Modification own images
                $images = array_get($productData, 'images', []);
                $images = array_reverse($images);
                foreach ($images as $productImageLink) {
                    $this->linkImage($productImageLink, $product);
                }
            }

            // Group images
            $images = array_get($data, 'images', []);
            $images = array_reverse($images);
            foreach ($images as $imageLink) {
                $this->linkImage($imageLink, $group);
            }

            // Labels
            foreach (array_get($data, 'labels', []) as $labelId) {
                if (!isset($this->labels[$labelId])) {
                    continue;
                }
                ProductGroupLabel::create([
                    'group_id' => $group->id,
                    'label_id' => $this->labels[$labelId],
                ]);
            }

            // Categories
            foreach (array_get($data, 'categories', []) as $categoryId) {
                if (!isset($this->categories[$categoryId])) {
                    continue;
                }
                ProductGroupCategory::create([
                    'group_id' => $group->id,
                    'category_id' => $this->categories[$categoryId],
                ]);
            }

            \DB::commit();
        }
    }

    private function loadFeaturesWithValues()
    {
        $features = $this->curl('features/all');
        if (!$features) {
            return;
        }

        foreach ($features as $featurePosition => $data) {
            \DB::beginTransaction();

            $feature = new Feature();
            $feature->fill($data);
            $feature->position = $featurePosition;
            $feature->save();

            $this->features[array_get($data, 'id')] = $feature->id;

            $this->languages->each(function (Language $language) use ($feature, $data) {
                $dataForTranslate = null;
                foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                    if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                        $dataForTranslate = $pageDataForTranslate;
                        break;
                    }
                }
                if (!isset($dataForTranslate)) {
                    return;
                }
                $translate = new FeatureTranslates();
                $translate->fill($dataForTranslate);
                $translate->row_id = $feature->id;
                $translate->language = $language->slug;
                $translate->save();
            });

            foreach (array_get($data, 'values', []) as $valuePosition => $valueData) {
                $value = new FeatureValue();
                $value->fill($valueData);
                $value->position = $featurePosition;
                $value->feature_id = $feature->id;
                $value->save();

                $this->featureValues[array_get($valueData, 'id')] = $value->id;

                $this->languages->each(function (Language $language) use ($value, $valueData) {
                    $dataForTranslate = null;
                    foreach (array_get($valueData, 'data', []) as $pageDataForTranslate) {
                        if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                            $dataForTranslate = $pageDataForTranslate;
                            break;
                        }
                    }
                    if (!isset($dataForTranslate)) {
                        return;
                    }
                    $translate = new FeatureValueTranslates();
                    $translate->fill($dataForTranslate);
                    $translate->row_id = $value->id;
                    $translate->language = $language->slug;
                    $translate->save();
                });
            }

            \DB::commit();
        }
    }

    /**
     * @throws \Exception
     */
    private function loadProductLabels()
    {
        $labels = $this->curl('product-labels/all');
        if (!$labels) {
            return;
        }

        foreach ($labels as $position => $data) {
            \DB::beginTransaction();
            $label = new Label();
            $label->fill($data);
            $label->position = $position;
            $label->save();

            $this->labels[array_get($data, 'id')] = $label->id;

            $this->languages->each(function (Language $language) use ($label, $data) {
                $dataForTranslate = null;
                foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                    if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                        $dataForTranslate = $pageDataForTranslate;
                        break;
                    }
                }
                if (!isset($dataForTranslate)) {
                    return;
                }
                $translate = new LabelTranslates();
                $translate->fill($dataForTranslate);
                $translate->row_id = $label->id;
                $translate->language = $language->slug;
                $translate->save();
            });
            \DB::commit();
        }
    }

    /**
     * @throws \Exception
     */
    private function loadCategories()
    {
        $categories = $this->curl('categories/all');
        if (!$categories) {
            return;
        }

        foreach ($categories as $data) {
            $this->loadCategory($data);
        }
    }

    private function loadCategory(array $data, ?int $parentId = null)
    {
        \DB::beginTransaction();
        $category = new Category();
        $category->fill($data);
        $category->parent_id = $parentId;
        $category->save();

        $this->categories[array_get($data, 'id')] = $category->id;

        $this->languages->each(function (Language $language) use ($category, $data) {
            $dataForTranslate = null;
            foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                    $dataForTranslate = $pageDataForTranslate;
                    break;
                }
            }
            if (!isset($dataForTranslate)) {
                return;
            }
            $translate = new CategoryTranslates();
            $translate->fill($dataForTranslate);
            $translate->row_id = $category->id;
            $translate->language = $language->slug;
            $translate->save();
        });
        \DB::commit();

        $this->linkImage(array_get($data, 'image'), $category);

        if (isset($data['children']) && is_array($data['children']) && count($data['children']) > 0) {
            foreach ($data['children'] as $kid) {
                $this->loadCategory($kid, $category->id);
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function loadBrands()
    {
        $brands = $this->curl('brands/all');
        if (!$brands) {
            return;
        }

        foreach ($brands as $data) {
            \DB::beginTransaction();
            $brand = new Brand();
            $brand->fill($data);
            $brand->save();

            $this->brands[array_get($data, 'id')] = $brand->id;

            $this->languages->each(function (Language $language) use ($brand, $data) {
                $dataForTranslate = null;
                foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                    if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                        $dataForTranslate = $pageDataForTranslate;
                        break;
                    }
                }
                if (!isset($dataForTranslate)) {
                    return;
                }
                $translate = new BrandTranslates();
                $translate->fill($dataForTranslate);
                $translate->row_id = $brand->id;
                $translate->language = $language->slug;
                $translate->save();
            });
            \DB::commit();

            $this->linkImage(trim(array_get($data, 'image')), $brand);
        }
    }

    /**
     * @throws \Exception
     */
    private function loadSiteMenu()
    {
        $menu = $this->curl('site-menu/all');
        if (!$menu) {
            return;
        }

        foreach ($menu as $data) {
            try {
                \DB::beginTransaction();
                $element = new SiteMenu();
                $element->fill($data);
                $element->save();

                $this->languages->each(function (Language $language) use ($element, $data) {
                    $dataForTranslate = null;
                    foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                        if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                            $dataForTranslate = $pageDataForTranslate;
                            break;
                        }
                    }
                    if (!isset($dataForTranslate)) {
                        return;
                    }
                    $translate = new SiteMenuTranslates();
                    $translate->fill($dataForTranslate);
                    $translate->row_id = $element->id;
                    $translate->language = $language->slug;
                    $translate->save();
                });
                \DB::commit();
            } catch (\Exception $exception) {
                $this->warn($exception->getMessage());
                \DB::rollBack();
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function loadArticles()
    {
        $articles = $this->curl('articles/all');
        if (!$articles) {
            return;
        }

        foreach ($articles as $data) {
            \DB::beginTransaction();
            $article = new Article();
            $article->fill($data);
            $article->views = (int)array_get($data, 'views', 0);
            $article->save();

            $this->languages->each(function (Language $language) use ($article, $data) {
                if (array_get($data, 'slug')) {
                    return;
                }
                $dataForTranslate = null;
                foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                    if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                        $dataForTranslate = $pageDataForTranslate;
                        break;
                    }
                }
                if (!isset($dataForTranslate)) {
                    return;
                }
                $translate = new ArticleTranslates();
                $translate->fill($dataForTranslate);
                $translate->row_id = $article->id;
                $translate->language = $language->slug;
                $translate->save();
            });
            \DB::commit();

            $this->linkImage(trim(array_get($data, 'image')), $article);
        }
    }

    /**
     * @throws \Exception
     */
    private function loadNews()
    {
        $news = $this->curl('news/all');
        if (!$news) {
            return;
        }

        foreach ($news as $data) {
            \DB::beginTransaction();
            $article = new News();
            $article->fill($data);
            $article->views = (int)array_get($data, 'views', 0);
            $article->save();

            $this->languages->each(function (Language $language) use ($article, $data) {
                if (array_get($data, 'slug')) {
                    return;
                }
                $dataForTranslate = null;
                foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                    if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                        $dataForTranslate = $pageDataForTranslate;
                        break;
                    }
                }
                if (!isset($dataForTranslate)) {
                    return;
                }
                $translate = new NewsTranslates();
                $translate->fill($dataForTranslate);
                $translate->row_id = $article->id;
                $translate->language = $language->slug;
                $translate->save();
            });
            \DB::commit();

            $this->linkImage(trim(array_get($data, 'image')), $article);
        }
    }

    /**
     * @throws \Exception
     */
    private function loadPages()
    {
        $pages = $this->curl('pages/all');
        if (!$pages) {
            return;
        }

        foreach ($pages as $data) {
            try {
                \DB::beginTransaction();
                $page = new Page();
                $page->fill($data);
                $page->save();

                $this->languages->each(function (Language $language) use ($page, $data) {
                    if (array_get($data, 'slug')) {
                        return;
                    }
                    $dataForTranslate = null;
                    foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                        if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                            $dataForTranslate = $pageDataForTranslate;
                            break;
                        }
                    }
                    if (!isset($dataForTranslate)) {
                        return;
                    }
                    $translate = new PageTranslates();
                    $translate->fill($dataForTranslate);
                    $translate->row_id = $page->id;
                    $translate->language = $language->slug;
                    $translate->save();
                });
                \DB::commit();
            } catch (\Exception $exception) {
                $this->warn($exception->getMessage());
                \DB::rollBack();
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function loadSystemPages()
    {
        $systemPages = $this->curl('system-pages/all');
        if (!$systemPages) {
            return;
        }

        foreach ($systemPages as $data) {
            try {
                \DB::beginTransaction();
                $systemPage = new SystemPage();
                $systemPage->save();

                $this->languages->each(function (Language $language) use ($systemPage, $data) {
                    if (array_get($data, 'slug')) {
                        return;
                    }
                    $dataForTranslate = null;
                    foreach (array_get($data, 'data', []) as $pageDataForTranslate) {
                        if (array_get($pageDataForTranslate, 'language') === $language->slug) {
                            $dataForTranslate = $pageDataForTranslate;
                            break;
                        }
                    }
                    if (!isset($dataForTranslate)) {
                        return;
                    }
                    $translate = new SystemPageTranslates();
                    $translate->fill([
                        'name' => array_get($dataForTranslate, 'name'),
                        'content' => array_get($dataForTranslate, 'content'),
                        'h1' => array_get($dataForTranslate, 'h1'),
                        'title' => array_get($dataForTranslate, 'title'),
                        'keywords' => array_get($dataForTranslate, 'keywords'),
                        'description' => array_get($dataForTranslate, 'description'),
                    ]);
                    $translate->slug = array_get($dataForTranslate, 'slug');
                    $translate->row_id = $systemPage->id;
                    $translate->language = $language->slug;
                    $translate->save();
                });
                \DB::commit();
            } catch (\Exception $exception) {
                $this->warn($exception->getMessage());
                \DB::rollBack();
            }
        }
    }

    private function loadFiles()
    {
        $files = $this->curl('files/all');
        if (!$files) {
            return;
        }
        foreach ($files as $file) {
            try {
                $fileData = $this->makeRequest($file, []);
                $parts = explode('storage', $file, 2);
                $path = end($parts);
                Storage::put(
                    $path,
                    is_array($fileData) ? json_encode($fileData) : $fileData
                );
            } catch (\Exception $exception) {
                $this->warn($exception->getMessage());
            }
        }
    }

    private function loadFavicons()
    {
        $favicons = $this->curl('favicons');
        if (!$favicons) {
            return;
        }
        foreach ($favicons as $favicon) {
            $fileData = $this->makeRequest($favicon, []);
            $parts = explode('storage', $favicon, 2);
            $path = end($parts);
            Storage::put(
                $path,
                is_array($fileData) ? json_encode($fileData) : $fileData
            );
        }
    }

    private function loadSettings()
    {
        $settings = $this->curl('settings/all');
        if (!$settings) {
            return;
        }
        foreach ($settings as $setting) {
            Setting::create($setting);
            \Config::set('db.' . array_get($setting, 'group') . '.' . array_get($setting, 'alias'), array_get($setting, 'value'));
        }
    }

    private function loadLogo()
    {
        $logos = $this->curl('logo');
        if (!$logos) {
            return;
        }
        // Desktop
        $desktop = array_get($logos, 'desktop');
        if ($desktop && $imageData = $this->makeRequest($desktop, [])) {
            $parts = explode('/', $desktop);
            $originalName = end($parts);
            Storage::put(
                $originalName,
                $imageData
            );
        }
        // Mobile
        $mobile = array_get($logos, 'mobile');
        if ($mobile && $imageData = $this->makeRequest($mobile, [])) {
            $parts = explode('/', $mobile);
            $originalName = end($parts);
            Storage::put(
                $originalName,
                $imageData
            );
        }
    }

    /**
     * @throws \Exception
     */
    private function loadSlider()
    {
        $slides = $this->curl('slides/all');
        if (!$slides) {
            return;
        }
        foreach ($slides as $data) {
            if (!isset($data['image']) || !array_get($data, 'image')) {
                continue;
            }
            \DB::beginTransaction();
            $slide = new SlideshowSimple();
            $slide->active = array_get($data, 'active', true);
            $slide->url = array_get($data, 'url');
            $slide->position = array_get($data, 'position', 0);
            $slide->save();

            $this->languages->each(function (Language $language) use ($slide, $data) {
                $dataForTranslate = null;
                foreach (array_get($data, 'data', []) as $slideDataForTranslate) {
                    if (array_get($slideDataForTranslate, 'language') === $language->slug) {
                        $dataForTranslate = $slideDataForTranslate;
                        break;
                    }
                }
                $translate = new SlideshowSimpleTranslates();
                if (!isset($dataForTranslate)) {
                    $translate->fill([
                        'name' => 'Unknown',
                    ]);
                } else {
                    $translate->fill([
                        'name' => array_get($dataForTranslate, 'name'),
                    ]);
                }
                $translate->row_id = $slide->id;
                $translate->language = $language->slug;
                $translate->save();
            });
            \DB::commit();

            $this->linkImage(trim(array_get($data, 'image')), $slide);
        }
    }

    /**
     * Remove all files we do not need
     */
    private function removeFiles(): void
    {
        $path = storage_path('app/public');
        foreach (scandir($path) as $item) {
            if ($item === '.' || $item === '..' || $item === '.gitignore') {
                continue;
            }
            $element = $path . '/' . $item;
            if (is_dir($element)) {
                File::deleteDirectory($element);
            } else if(is_file($element)) {
                File::delete($element);
            }
        }
    }

    /**
     * @param string $method
     * @return array|null
     */
    private function curl(string $method): ?array
    {
        return $this->makeRequest($this->host . ltrim($method, '/'));
    }

    /**
     * @param string|null $link
     * @param Model $model
     */
    private function linkImage(?string $link, Model $model)
    {
        if (!$link) {
            return;
        }
        try {
            $imageData = $this->makeRequest($link, []);
            if ($imageData === null) {
                return;
            }
            $configurations = $model->configurations();
            $parts = explode('/', $link);
            $originalName = end($parts);
            Storage::put(
                $configurations->folder . '/' . $originalName,
                $imageData
            );
            (new Image)->store(
                $model,
                $model->getDefaultType(),
                new UploadedFile(storage_path('app/public/' . $configurations->folder . '/' . $originalName), $originalName),
                $originalName
            );
        } catch (\Exception $exception) {
            $this->warn($exception->getMessage());
        }
    }

    private function makeRequest(string $link, array $headers = [])
    {
        $httpAuth = null;
        if (config('db.basic-auth.auth')) {
            $httpAuth = config('db.basic-auth.username') . ':' . config('db.basic-auth.password');
        }
        $headers['Accept'] = 'application/json';
        $headers['X-Authorization'] = $this->apiKey;
        return Emitter::get($link, $headers, $httpAuth);
    }
}
