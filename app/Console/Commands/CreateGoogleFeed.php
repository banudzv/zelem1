<?php

namespace App\Console\Commands;

use App\Core\Modules\Administrators\Models\Admin;
use App\Core\Modules\Administrators\Models\AdminRole;
use App\Core\Modules\Administrators\Models\Role;
use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Modules\Export\Jobs\GenerateGoogleMerchantFeedJob;
use App\Modules\Export\Models\GoogleMerchant;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Validator;
use File;
use View;
use Config;



use Illuminate\Validation\Rule;

/**
 * Class CreateSuperAdmin
 *
 * @package App\Console\Commands
 */
class CreateGoogleFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locotrade:create-google-feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create google feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    const FEED_NAME = 'google_merchant_{lang}.xml';

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        dispatch(new GenerateGoogleMerchantFeedJob());
    }
}
