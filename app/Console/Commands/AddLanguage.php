<?php

namespace App\Console\Commands;

use App\Core\Modules\Languages\Models\Language;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Str;

/**
 * Class AddLanguage
 *
 * @package App\Console\Commands
 */
class AddLanguage extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locotrade:language';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add language to site';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $defaultLanguage = Language::whereDefault(true)->first()->slug;
        
        $languageName = Str::substr($this->endlessQuestion('Название языка'), 0, 190);
        $languageSlug = Str::substr(Str::slug($this->endlessQuestion('Алиас для УРЛ адреса')), 0, 3);
        $languageGoogleSlug = Str::substr(Str::slug($this->endlessQuestion('Алиас для разметки')), 0, 3);
        $this->info("Данные для добавления: $languageName, $languageSlug, $languageGoogleSlug");
        if (!$this->confirm('Все верно?', true)) {
            die;
        }
        
        if (Language::whereSlug($languageSlug)->exists()) {
            $this->error('Язык с таким алиасом уже существует!');
            die;
        }
        Language::create([
            'slug' => $languageSlug,
            'name' => $languageName,
            'google_slug' => $languageGoogleSlug,
            'default' => false,
        ]);
    
        $tables = \DB::connection()->getDoctrineSchemaManager()->listTableNames();
        foreach ($tables as $table) {
            if (Str::endsWith($table, '_translates') || $table === 'translates') {
                $this->info("Обновляю таблицу $table...");
                $rows = \DB::table($table)->where('language', $defaultLanguage)->get();
                foreach ($rows as $row) {
                    $data = (array)$row;
                    unset($data['id'], $data['created_at'], $data['updated_at']);
                    $data['language'] = $languageSlug;
                    \DB::table($table)->insert($data);
                }
            }
        }
    }
    
    /**
     * @param string $question
     * @return mixed
     */
    private function endlessQuestion(string $question)
    {
        if ($answer = $this->ask($question)) {
            return $answer;
        }
        return $this->endlessQuestion($question);
    }
}
