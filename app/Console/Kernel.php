<?php

namespace App\Console;

use App\Modules\Export\Jobs\GenerateFacebookXmlJob;
use App\Modules\Export\Jobs\GenerateGoogleAdsFeedsJob;
use App\Modules\Export\Jobs\GenerateGoogleMerchantFeedJob;
use App\Modules\Export\Jobs\GenerateRemarketingFeedJob;
use App\Modules\Products\Models\Product;
use App\Modules\Search\Commands\ElasticIndexUpdateCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected $commands = [
        //
    ];

    protected function schedule(Schedule $schedule): void
    {

        $schedule->command(
            ElasticIndexUpdateCommand::class,
            [
                Product::class
            ]
        )->everyThirtyMinutes();

        $schedule->command('youtube:sync')->dailyAt('08:00');

        $schedule->job(new GenerateRemarketingFeedJob())->dailyAt('02:00');
        $schedule->job(new GenerateGoogleAdsFeedsJob())->dailyAt('02:10');
        $schedule->job(new GenerateGoogleMerchantFeedJob())->dailyAt('02:20');
        $schedule->job(new GenerateFacebookXmlJob())->dailyAt('02:30');
    }

    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        include base_path('routes/console.php');
    }
}
