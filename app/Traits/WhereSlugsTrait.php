<?php

namespace App\Traits;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Lang;

trait WhereSlugsTrait
{

    protected function whereSlugs(array $slugs): Closure
    {
        return function (Builder $builder) use ($slugs) {
            $builder->whereIn('slug', $slugs)
                ->where('language', Lang::getLocale());
        };
    }

}