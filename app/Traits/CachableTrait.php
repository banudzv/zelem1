<?php

namespace App\Traits;

use Exception;
use Illuminate\Cache\CacheManager;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait CachableTrait
 *
 * Not for class extended from Eloquent Models
 *
 * @package App\Traits
 */
trait CachableTrait
{
    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        $realMethodName = $this->getMethodName($name);

        if (!$this->checkMethodExists($realMethodName)) {
            throw new Exception(sprintf('Method %s not found in class', $realMethodName));
        }

        return $this->getCacheManager()
            ->rememberForever(
                $this->generateCacheKey($realMethodName, $arguments),
                function () use ($realMethodName, $arguments) {
                    return $this->$realMethodName(...$arguments);
                }
            );
    }

    protected function getMethodName(string $name)
    {
        $postfix = $this->methodPostfix();
        return preg_replace("/${postfix}$/", '', $name);
    }

    protected function methodPostfix(): string
    {
        return 'Cached';
    }

    protected function checkMethodExists(string $name)
    {
        return method_exists($this, $name);
    }

    protected function getCacheManager(): CacheManager
    {
        $cacheManager = app('cache');

        if ($this->isSupportTags()) {
            $cacheManager->tags([$this->cachableClassName()]);
        }

        return $cacheManager;
    }

    protected function isSupportTags(): bool
    {
        $driver = config('cache.default');

        if ($driver === 'redis') {
            return true;
        }

        if ($driver === 'memcached') {
            return true;
        }

        return false;
    }

    protected function cachableClassName()
    {
        return static::class;
    }

    protected function generateCacheKey(string $methodName, $arguments = [])
    {
        $additionalKey = $this->argumentsToKey($arguments);

        return sprintf(
            '%s::%s::%s::%s',
            $this->cachableClassName(),
            $methodName,
            $additionalKey,
            get_current_locale_slug()
        );
    }

    protected function argumentsToKey($arguments): string
    {
        if (!$first = array_shift($arguments)) {
            return '';
        }

        if ($first instanceof Model) {
            return $first->getKey();
        }

        return (string)$first;
    }

    public function cacheFlush()
    {
        $this->getCacheManager()->flush();
    }
}