<?php


namespace App\Traits;


trait FormattedDateAdmin
{
    /**
     * @return null|string
     */
    public function getFormattedDateAttribute(): ?string
    {
        if (isset($this->created_at)) {
            return $this->created_at->format('d') . ' ' .
                __(config('months.full.' . $this->created_at->format('n'), $this->created_at->format('m'))) . ' ' .
                $this->created_at->format('Y');
        }
        return null;
    }

    /**
     * @return null|string
     */
    public function getPublishedDateAttribute(): ?string
    {
        if (isset($this->published_at)) {
            return $this->published_at->format('d') . ' ' .
                __(config('months.full.' . $this->published_at->format('n'), $this->published_at->format('m'))) . ' ' .
                $this->published_at->format('Y');
        }
        return null;
    }
}