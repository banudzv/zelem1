<?php
namespace App\Traits;

use DB;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Lang;

/**
 * Trait ModelMain
 *
 * Методы для работы с основной таблицей, имеющей таблицу в БД с переводами
 *
 * @package  App\Traits
 * @property Collection $data
 * @property $this $current
 * @property object|mixed|null $dataFor
 */
trait ModelMain
{

    public static function getRelatedTableName()
    {
        $relatedModelName = static::relatedModelName();
        $relatedModel = new $relatedModelName;
        return $relatedModel->getTable();
    }

    public static function relatedModelName()
    {
        $currentClass = static::class;
        $translatesClass = $currentClass . 'Translates';
        return $translatesClass;
    }

    /**
     * Get row by filed in `current` relation
     *
     * @param string $field
     * @param null $value
     * @return Builder|Model
     */
    public static function getByCurrent(string $field, $value)
    {
        return static::with('current')
            ->whereHas(
                'current',
                function (Builder $query) use ($field, $value) {
                    $query->where($field, $value);
                }
            )
            ->first();
    }

    public function data()
    {
        return $this->hasMany($this->relatedModelName(), 'row_id', 'id');
    }

    public function current()
    {
        if (config('app.place') === 'site') {
            $currntLanguage = Lang::getLocale();
        } else {
            $currntLanguage = config('app.default-language', app()->getLocale());
        }
        return $this
            ->hasOne($this->relatedModelName(), 'row_id', 'id')
            ->where('language', '=', $currntLanguage)
            ->withDefault();
    }

    public function dataForCurrentLanguage($default = null)
    {
        $data = $this->data;
        foreach ($data as $element) {
            if ($element->language === config('app.locale')) {
                return $element;
            }
        }
        return $default;
    }

    /**
     * @param Request|array $request
     * @param string|null $imageGroupType
     * @return string|null
     * @throws Exception
     */
    public function createRow($request, ?string $imageGroupType = null): ?string
    {
        $data = $request instanceof Request ? ($request->input() ?: []) : (array)$request;
        try {
            DB::beginTransaction();
            $this->fill($data);
            if ($this->save() !== true) {
                return 'Can not execute SQL request!';
            }
            $modelName = $this->relatedModelName();
            foreach (config('languages', []) as $language) {
                $translate = new $modelName();
                $translate->fill(Arr::get($data, $language['slug'], []));
                $translate->language = $language['slug'];
                $translate->row_id = $this->id;
                $translate->save();
                if (method_exists($translate, 'uploadImage')) {
                    $translate->uploadImage($imageGroupType);
                }
            }
            if (method_exists($this, 'uploadImage')) {
                $this->uploadImage($imageGroupType);
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollback();
            return $exception->getMessage();
        }
        return null;
    }

    /**
     * @param Request|array $request
     * @param string|null $imageGroupType
     * @return string|null
     * @throws Exception
     */
    public function updateRow($request, ?string $imageGroupType = null): ?string
    {
        $data = $request instanceof Request ? ($request->input() ?: []) : (array)$request;
        try {
            DB::beginTransaction();
            $this->fill($data);
            $changes = [];
            if ($this->isDirty()) {
                $changes['main'] = array_keys($this->getDirty());
            }
            if ($this->isDirty()) {
                if ($this->save() !== true) {
                    return 'Can not execute SQL request!';
                }
            } else {
                $this->touch();
            }
            if (method_exists($this, 'uploadImage')) {
                $this->uploadImage($imageGroupType);
            }
            $modelName = $this->relatedModelName();
            foreach (config('languages', []) as $language) {
                $translate = $this->dataFor($language['slug']);
                if (!$translate) {
                    $translate = new $modelName();
                }
                $translate->fill(Arr::get($data, $language['slug'], []));
                $translate->row_id = $this->id;
                $translate->language = $language['slug'];
                if ($translate->isDirty()) {
                    $changes[$translate->language] = array_keys($translate->getDirty());
                }
                $translate->save();
                if (method_exists($translate, 'uploadImage')) {
                    $translate->uploadImage($imageGroupType);
                }
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollback();
            return $exception->getMessage();
        }
        return null;
    }

    public function dataFor($lang, $default = null)
    {
        $data = $this->data;
        foreach ($data as $element) {
            if ($element->language === $lang) {
                return $element;
            }
        }
        return $default;
    }

    /**
     * Delete row from the database
     *
     * @return bool
     */
    public function deleteRow()
    {
        try {
            if (method_exists($this, 'deleteImagesIfExist')) {
                $this->deleteImagesIfExist();
            }
            $this->delete();
        } catch (Exception $exception) {
            return false;
        }
        return true;
    }

    protected function getCurrentLanguageSlug()
    {
        return config('app.place') === 'site'
            ? Lang::getLocale()
            : config('app.default-language', app()->getLocale());
    }
}
