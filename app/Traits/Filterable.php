<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * @method static self|Builder filter(array $attributes = [])
 */
trait Filterable
{
    use \EloquentFilter\Filterable;
}