<?php
namespace App\Traits;

use App\Core\Modules\Languages\Models\Language;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Lang;

/**
 * Trait ModelTranslates
 *
 * Методы для работы с таблицей с переводами
 *
 * @package App\Traits
 *
 * @property-read object|mixed|null $row
 * @property-read Language $lang
 *
 * @see ModelTranslates::scopeWhereLangIsCurrent()
 * @method static self|Builder whereLangIsCurrent()
 */
trait ModelTranslates
{
    public function row()
    {
        $modelName = $this->relatedModelName();
        return $this->belongsTo($modelName, 'row_id', 'id')->withDefault();
    }

    public function relatedModelName()
    {
        $currentClass = static::class;
        $mainClass = Str::replaceLast('Translates', '', $currentClass);
        return $mainClass;
    }

    public function lang()
    {
        return $this->belongsTo(Language::class, 'slug', 'language')->withDefault();
    }

    public function scopeWhereLangIsCurrent(Builder $builder)
    {
        $builder->where('language', Lang::getLocale());
    }
}
