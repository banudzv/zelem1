<?php

namespace App\Modules\SocialButtons\Widgets;

use App\Components\Widget\AbstractWidget;

class Callback implements AbstractWidget
{

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $messengers = config('social_buttons.messengers', []);
        $items = [];

        foreach ($messengers as $messenger) {
            if (config('db.social.messenger-' . $messenger, false)) {
                switch ($messenger) {
                    case 'viber':
                        $link = browserizr()->isDesktop()
                            ? 'viber://chat?number=%2B' . config('db.social.messenger-' . $messenger)
                            : 'viber://add?number=' . config('db.social.messenger-' . $messenger);
                        break;
                    case 'telegram':
                        $link = 'https://t.me/' . config('db.social.messenger-' . $messenger);
                        break;
                    case 'messenger':
                        $link = 'https://m.me/' . config('db.social.messenger-' . $messenger);
                        break;
                    default:
                        $link = config('db.social.messenger-' . $messenger);
                        break;
                }

                $items[] = [
                    'icon' => $messenger,
                    'name' => ucfirst($messenger),
                    'blank' => true,
                    'link' => $link
                ];
            }
        }

        if (config('db.basic.phone_number_1', false)) {
            $items[] = [
                'icon' => 'phone-call',
                'name' => trans('social_buttons::site.call-me'),
                'mfp' => '#popup-callback',
                'mfp-type' => 'inline'
            ];
        }

        return view('site._widgets.callback-widget.callback-widget', [
            'items' => $items
        ]);
    }

}
