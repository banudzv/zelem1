<?php

return [
    'socials' => ['instagram', 'facebook', 'twitter', 'youtube'],
    'messengers' => ['messenger', 'viber', 'telegram']
];
