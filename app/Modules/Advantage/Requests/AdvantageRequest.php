<?php

namespace App\Modules\Advantage\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;
use Config;

/**
 * Class AdvantageRequest
 * @package App\Modules\Advantage\Requests
 */
class AdvantageRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;
    
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * @return array
     * @throws \App\Exceptions\WrongParametersException
     */
    public function rules(): array
    {
        return $this->generateRules([
            'active' => ['required', 'boolean'],
        ], [
            'name' => ['required', 'max:191'],
        ]);
    }

    /**
     * @return array
     */
    public function attributes()
    {
        $attributes = [];
        foreach (Config::get('languages') as $key => $lang) {
            $attributes = [
                $key.'.slug' => trans('validation.attributes.slug')
            ];
        }
        return $attributes;
    }
    
}
