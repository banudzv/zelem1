<?php

namespace App\Modules\Advantage\Models;

use App\Traits\ActiveScopeTrait;
use App\Traits\CheckRelation;
use App\Traits\ModelMain;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use SiteHelpers\SvgSpritemap;

/**
 * App\Modules\Advantage\Models\Advantage
 *
 * @property int $id
 * @property bool $active
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Modules\Advantage\Models\AdvantageTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Advantage\Models\AdvantageTranslates[] $data
 * @property-read string $link_in_admin_panel
 * @property-read \App\Modules\Advantage\Models\Advantage $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage active($active = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage wherePosition($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\Advantage whereLike($column, $value, $boolean = 'and')
 * @property string $svg_image
 */
class Advantage extends Model
{
    use ModelMain, EloquentTentacle, ActiveScopeTrait, CheckRelation;

    const SEO_TEMPLATE_ALIAS = 'advantages';

    protected $casts = ['active' => 'boolean'];

    protected $fillable = ['active', 'position', 'svg_image'];


    /**
     * @return array
     */
    public static function getDictionaryForSelects(): array
    {
        $advantages = new EloquentCollection();
        Advantage::with('current')
            ->oldest('position')
            ->latest('id')
            ->get()
            ->each(function (Advantage $advantage) use ($advantages) {
                $currentCollection = $advantages->get((int)$advantage->parent_id, new EloquentCollection());
                $currentCollection->push($advantage);
                $advantages->put((int)$advantage->parent_id, $currentCollection);
            });
        return static::makeDictionaryFrom($advantages, 0);
    }

    /**
     * @param EloquentCollection $advantages
     * @param int $parentId
     * @param array $dictionary
     * @param int $level
     * @return array
     */
    protected static function makeDictionaryFrom(EloquentCollection $advantages, int $parentId, array &$dictionary = [], int $level = 0): array
    {
        $advantages->get($parentId, new EloquentCollection)->each(function (Advantage $advantage) use (&$dictionary, $level, $advantages) {
            $spaces = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level);
            $dictionary[$advantage->id] = $spaces . $advantage->current->name;
            static::makeDictionaryFrom($advantages, $advantage->id, $dictionary, ++$level);
        });
        return $dictionary;
    }

    /**
     * Get advantages list related to this group
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Advantage::class, 'parent_id', 'id')
            ->with(['current', 'children','data'])
            ->oldest('position')
            ->latest('id');
    }

    /**
     * Get advantages list related to this group
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeChildren()
    {
        return $this->hasMany(Advantage::class, 'parent_id', 'id')
            ->where('active', true)
            ->with(['current', 'activeChildren'])
            ->oldest('position')
            ->latest('id');
    }

    /**
     * Parent advantage
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne(Advantage::class, 'id', 'parent_id');
    }

    /**
     * Checks if parent can be linked as parent for chosen advantage
     *
     * @param int $advantageId
     * @param int|null $parentId
     * @return bool
     */
    public static function isAvailableToChoose(int $advantageId, ?int $parentId = null): bool
    {
        if ($parentId === null) {
            return true;
        }
        if ($advantageId === $parentId) {
            return false;
        }
        return in_array($parentId, Advantage::getAllChildrenIds($advantageId)) === false;
    }

    /**
     * Builds advantages full tree
     *
     * @return Collection|Advantage[][]
     */
    public static function tree(): Collection
    {
        $advantages = new Collection();
        Advantage::with('current')
            ->oldest('position')
            ->latest('id')
            ->get()
            ->each(function (Advantage $advantage) use ($advantages) {
                $elements = $advantages->get((int)$advantage->parent_id, []);
                $elements[] = $advantage;
                $advantages->put((int)$advantage->parent_id, $elements);
            });
        return $advantages;
    }

    /**
     * @return mixed
     */
    public static function getList()
    {
        return self::with('current')
            ->oldest('position')
            ->get();
    }

    /**
     * Get children ids list for advantage by id
     *
     * @param int|null $advantageId
     * @return array
     */
    public static function getAllChildrenIds(?int $advantageId = null): array
    {
        return Advantage::getChildrenIdsFromCollection(Advantage::tree(), $advantageId);
    }

    /**
     * Get children ids list for advantage from existing list by advantage id
     *
     * @param Collection|Advantage[][] $advantages
     * @param int|null $advantageId
     * @return array
     */
    public static function getChildrenIdsFromCollection(Collection $advantages, ?int $advantageId = null): array
    {
        $childrenIds = [];
        foreach ($advantages->get((int)$advantageId, []) as $advantage) {
            $childrenIds[] = $advantage->id;
            $childrenIds = array_merge($childrenIds, Advantage::getChildrenIdsFromCollection($advantages, $advantage->id));
        }
        return $childrenIds;
    }

    /**
     * Advantage link in admin panel
     *
     * @return string
     */
    public function getLinkInAdminPanelAttribute(): string
    {
        return route('admin.advantages.edit', ['advantage' => $this->id]);
    }

    /**
     * Top level of advantages
     *
     * @return Advantage[]|Builder[]|EloquentCollection
     */
    public static function topLevel()
    {
        return Advantage::with('current')
            ->active(true)
            ->where(function(Builder $query) {
                $query
                    ->where('parent_id', 0)
                    ->orWhereNull('parent_id');
            })
            ->oldest('position')
            ->latest('id')
            ->get();
    }

    /**
     * @return bool
     */
    public function getHasChildrenAttribute(): bool
    {
        return $this->children && $this->children->isNotEmpty();
    }

    public function moveKidsToTheParentAdvantage(): void
    {
        Advantage::whereParentId($this->id)->update([
            'parent_id' => $this->parent_id,
        ]);
    }

    public function uploadSvgImage($file)
    {
        if(isset($file)) {
            $svgContent = file_get_contents($file);
            $svg = SvgSpritemap::cleanSvg($svgContent);
            $this->svg_image = $svg;
            $this->save();
        }
    }
}
