<?php

namespace App\Modules\Advantage\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\Advantage\Models\AdvantageTranslates
 *
 * @property int $id
 * @property string $name
 * @property int $row_id
 * @property string $language
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\Advantage\Models\Advantage $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\AdvantageTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\AdvantageTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\AdvantageTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\AdvantageTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\AdvantageTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\AdvantageTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Advantage\Models\AdvantageTranslates whereRowId($value)
 * @mixin \Eloquent
 */
class AdvantageTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'advantages_translates';
    
    public $timestamps = false;
    
    protected $fillable = ['name', 'row_id', 'language'];
}
