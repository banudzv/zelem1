<?php

use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:advantage'])->group(function () {
    Route::put('advantages/{advantage}/active', [
        'uses' => 'AdvantageController@active',
        'as' => 'admin.advantages.active',
    ]);
    Route::put('advantages/sortable', [
        'uses' => 'AdvantageController@sortable',
        'as' => 'admin.advantages.sortable',
    ]);
    Route::get('advantages/{advantage}/destroy', [
        'uses' => 'AdvantageController@destroy',
        'as' => 'admin.advantages.destroy',
    ]);
    Route::get('advantages/{advantage}/delete-svg', [
        'uses' => 'AdvantageController@deleteImageSvg',
        'as' => 'admin.advantages.delete-image-svg',
    ]);
    Route::resource('advantages', 'AdvantageController')
        ->except('show', 'destroy')
        ->names('admin.advantages');
});

// Routes for unauthenticated administrators


