<?php

namespace App\Modules\Advantage;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Advantage\Widgets\AdvantagesView;
use Widget, CustomMenu, CustomRoles;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Advantage
 */
class Provider extends BaseProvider
{

    /**
     * Set custom presets
     */
    protected function presets()
    {
    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register left menu block
        CustomMenu::get()->group()
            ->block('content', 'advantage::general.menu-block', 'fa fa-files-o')
            ->link('advantage::general.menu', RouteObjectValue::make('admin.advantages.index'))
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.advantages.edit'),
                RouteObjectValue::make('admin.advantages.create')
            );
        // Register role scopes
        CustomRoles::add('advantage', 'advantage::general.permission-name')
            ->except(RoleRule::VIEW);
    }


    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
        Widget::register(AdvantagesView::class, 'advantage::show');
    }


}
