@php
/** @var \App\Modules\Advantage\Models\Advantage[]|\Illuminate\Support\Collection $advantages */
@endphp
<div class="section">
    <div class="container _pb-xxl _mb-def _pt-lg">
        <div class="grid grid--2 grid--md-3 grid--def-6 _nml-sm _def-nml-def _nmt-def">
            @foreach ($advantages as $advantage)
                <div class="gcell _md-pl-def _pl-sm _pt-def">
                    <div class="feature">
                        <div class="feature__icon">
                            {!! $advantage->svg_image !!}
                        </div>
                        <div class="feature__name">{{ $advantage->current->name }}</div>
                    </div>
                </div>
        @endforeach
        </div>
    </div>
</div>