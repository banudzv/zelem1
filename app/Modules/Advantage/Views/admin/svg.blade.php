@php
    /** @var App\Modules\Advantage\Models\Advantage $advantage */
@endphp

<div class="box box-solid">
    <div class="box-body">
        <div class="col-md-12 image3dbox">
            <div class="group-images-3d">
                @if(isset($advantage->svg_image))
                    <div class="file-attach recently-uploaded">
                        <a
                            href="{{ route('admin.advantages.delete-image-svg', $advantage->id) }}"
                            class="delete-remote-group-image delete-remote-group-3d-image ajax-request"
                            data-confirm="Изображение загружено на сервер. Удалить?"
                        >
                            <i class="fa fa-close"></i>
                        </a>
                        <div
                            class="label"
                            style="cursor: default; padding: .2em .6em .3em;"
                        > {!! $advantage->svg_image !!}</div>
                    </div>
                @endif
            </div>
            <div class="clearfix"></div>
            <button class="btn btn-flat add-group-3d-image {{ isset($advantage->svg_image) ? '_hide' : null }}"
                    type="button">@lang('advantage::admin.add-image')</button>
        </div>
    </div>
</div>

@push('styles')
    <style>
        .group-images-3d svg {
            width: 100%;
            height: 100%;
        }
        ._hide {
            display: none;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $('.image3dbox').on('click', '.add-group-3d-image', function () {
            var $button = $(this),
                $block = $button.closest('.image3dbox'),
                $image3d = $block.find('.group-images-3d'),
                index = $block.data('index'),
                template = '<div class="file-attach">' +
                    '<a href="#" class="delete-group-image">' +
                    '<i class="fa fa-close"></i>' +
                    '</a>' +
                    '<label class="label">' +
                    '<i class="glyphicon glyphicon-paperclip"></i>' +
                    '<input type="file" name="svg-image" accept=".svg">' +
                    '</label>' +
                    '</div>';
            $button.hide();
            $image3d.append(template);
        });
        $('.image3dbox').on('click', '.delete-remote-group-3d-image', function (e) {
            e.preventDefault();
            var $it = $(this), text = $it.data('confirm') || null;
            var data = $it.data('body') || {}, method = 'GET';
            if (Object.keys(data).length > 0) {
                data['_method'] = $it.data('method') || undefined;
                method = $it.data('method') || 'GET';
            }
            if (text) {
                confirmation(text, function () {
                    $.ajax({
                        type: method,
                        url: $it.attr('href'),
                        data: data,
                    }).done((data) => {
                        if (data.success) {
                            $('.group-images-3d').find('.file-attach').remove();
                            $('.add-group-3d-image').show();
                        }
                    });
                });
            } else {
                $.ajax({
                    type: method,
                    url: $it.attr('href'),
                    data: data,
                }).done((data) => {
                    if (data.success) {
                        $('.group-images-3d').find('.file-attach').remove();
                        $('.add-group-3d-image').show();
                    }
                });
            }
        });
        $('.image3dbox').on('click', '.delete-group-image', function () {
            let box = $(this).closest('.image3dbox');
            let button = box.find('.add-group-3d-image');
            $(this).closest('.file-attach').fadeOut(300, function () {
                $(this).remove();
                button.show();
            });
            return false;
        });
    </script>
@endpush
