@php
    /** @var \App\Modules\Advantage\Models\Advantage[] $advantages */
$className = \App\Modules\Advantage\Models\Advantage::class;
$getParameters = request()->query();
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        <div class="dd pageList" id="myNest" data-depth="1">
            <ol class="dd-list">
                @include('advantage::admin.items', ['advantages' => $advantages])
            </ol>
        </div>
        <span id="parameters"
              data-url="{{ route('admin.advantages.sortable', ['class' => $className, 'query' => $getParameters]) }}"></span>
        <input type="hidden" id="myNestJson">
    </div>
@stop
