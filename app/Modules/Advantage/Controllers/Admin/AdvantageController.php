<?php

namespace App\Modules\Advantage\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Advantage\Forms\AdvantageForm;
use App\Modules\Advantage\Models\Advantage;
use App\Modules\Advantage\Requests\AdvantageRequest;
use Seo;

/**
 * Class AdvantageController
 *
 * @package App\Modules\Advantage\Controllers\Admin
 */
class AdvantageController extends AdminController
{
    /**
     * AdvantageController constructor.
     * Global for controller parts
     */
    public function __construct()
    {
        Seo::breadcrumbs()->add(
            'advantage::seo.index',
            RouteObjectValue::make('admin.advantages.index')
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $this->addCreateButton('admin.advantages.create');
        Seo::meta()->setH1('advantage::seo.index');
        $advantages = Advantage::getList();
        if ($advantages->isNotEmpty()) {
            return view('advantage::admin.index', ['advantages' => $advantages]);
        }
        return view('advantage::admin.no-pages');
    }

    /**
     * Create advantage page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Throwable
     */
    public function create()
    {
        Seo::breadcrumbs()->add('advantage::seo.create');
        Seo::meta()->setH1('advantage::seo.create');
        $this->initValidation((new AdvantageRequest())->rules());
        return view('advantage::admin.create', [
            'form' => AdvantageForm::make(),
        ]);
    }

    /**
     * Store new advantage
     *
     * @param AdvantageRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(AdvantageRequest $request)
    {
        $advantage = (new Advantage());
        if ($message = $advantage->createRow($request)) {
            return $this->afterFail($message);
        }
        $advantage->uploadSvgImage($request->file('svg-image'));
        return $this->afterStore(['id' => $advantage->id]);
    }

    /**
     * Update advantage page
     *
     * @param Advantage $advantage
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Throwable
     */
    public function edit(Advantage $advantage)
    {
        Seo::breadcrumbs()->add($advantage->current->name ?? 'advantage::seo.edit');
        Seo::meta()->setH1('advantage::seo.edit');
        $this->initValidation((new AdvantageRequest())->rules());
        return view('advantage::admin.update', [
            'object' => $advantage,
            'form' => AdvantageForm::make($advantage),
        ]);
    }

    /**
     * Update information for current advantage
     *
     * @param AdvantageRequest $request
     * @param Advantage $advantage
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(AdvantageRequest $request, Advantage $advantage)
    {
        if ($message = $advantage->updateRow($request)) {
            return $this->afterFail($message);
        }
        $advantage->uploadSvgImage($request->file('svg-image'));
        return $this->afterUpdate();
    }

    public function deleteImageSvg(Advantage $advantage)
    {
        $advantage->svg_image = null;
        $advantage->save();
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Delete all data for advantage including images
     *
     * @param Advantage $advantage
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(Advantage $advantage)
    {
        $advantage->deleteRow();
        return $this->afterDestroy();
    }
}
