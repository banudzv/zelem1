<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvatagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advantages', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(1);
            $table->integer('position')->default(0);
            $table->longText('svg_image')->nullable();
            $table->timestamps();
        });

        Schema::create('advantages_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);

            $table->foreign('language')->references('slug')->on('languages')
                ->index('advantages_translates_language_languages_slug')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('advantages')
                ->index('advantages_translates_row_id_advantages_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advantages_translates', function (Blueprint $table) {
            $table->dropForeign('advantages_translates_language_languages_slug');
            $table->dropForeign('advantages_translates_row_id_advantages_id');
        });
        Schema::dropIfExists('advantages_translates');
        Schema::dropIfExists('advantages');
    }
}
