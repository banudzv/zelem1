<?php

namespace App\Modules\Advantage\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'advantage';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'Преимущества',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Список преимуществ',
                ],
                [
                    'name' => 'seo.create',
                    'ru' => 'Создание преимущества',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование преимущества',
                ],
                [
                    'name' => 'admin.add-image',
                    'ru' => 'Добавить SVG иконку',
                ],
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
