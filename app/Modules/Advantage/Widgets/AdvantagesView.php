<?php

namespace App\Modules\Advantage\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Advantage\Models\Advantage;

class AdvantagesView implements AbstractWidget
{

    public function render()
    {
        $advantages = Advantage::active()
            ->with('current')
            ->get();

        if (!$advantages || $advantages->isEmpty()) {
            return null;
        }

        return view('advantage::widgets.index', ['advantages' => $advantages]);
    }

}
