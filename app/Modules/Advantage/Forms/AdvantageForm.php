<?php

namespace App\Modules\Advantage\Forms;

use App\Core\Interfaces\FormInterface;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use App\Modules\Advantage\Models\Advantage;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\Text;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdvantageForm
 *
 * @package App\Modules\Advantage\Forms
 */
class AdvantageForm implements FormInterface
{

    /**
     * Make form
     *
     * @param Model|null $advantage
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Throwable
     */
    public static function make(?Model $advantage = null): Form
    {
        $advantage = $advantage ?? new Advantage();
        $form = Form::create();
        $form->fieldSet(6, FieldSet::COLOR_SUCCESS)->add(
            Toggle::create('active', $advantage)->required(),
            Text::create()->setDefaultValue(view('advantage::admin.svg', ['advantage' => $advantage]))
        );
        $form->fieldSetForLang(6)->add(
            Input::create('name', $advantage)->required()
        );
        return $form;
    }

}
