<?php

namespace App\Modules\ProductsCom\Widgets;

use Widget, Cookie;
use Illuminate\Support\Facades\DB;
use App\Components\Parsers\ProductGroup;
use App\Modules\Products\Models\Product;
use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\ProductGroupLabel;
use App\Modules\Comments\Models\Comment;


/**
 * Class ComProducts
 *
 * @package App\Modules\ProductsCom\Widgets
 */
class ComProducts implements AbstractWidget
{
    const LIMIT_IN_WIDGET = 15;
    
    protected $ignoredProductId;
    
    public function __construct(?int $ignoredProductId = null)
    {
        $this->ignoredProductId = $ignoredProductId;
    }

    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
    
      $productsIds = Comment::where('active', 1)
                        ->where('commentable_id', '>=', Product::min('id'))
                        ->selectRaw('COUNT(`commentable_id`) AS count, `commentable_id`')
                        ->groupBy('commentable_id')
                        ->orderBy('count', 'DESC')
                        ->limit(5)
                        ->get()
                        ->transform(function ($item) {
                            return $item->commentable_id;
                        })
                        ->toArray();

        $products = Product::whereIn('id', $productsIds)
                     ->where('active', true)
                     ->get();
        
        return Widget::show(
            'products::slider',
            $products,
            trans('com::general.widget-name'),
            route('site.com-products')
        );
    }
    
}
