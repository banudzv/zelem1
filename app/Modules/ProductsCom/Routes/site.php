<?php

use Illuminate\Support\Facades\Route;

Route::paginate('com-products', [
    'as' => 'site.com-products',
    'uses' => 'ComController@index',
]);
