<?php

namespace App\Modules\ProductsCom\Listeners;

use App\Core\Interfaces\ListenerInterface;
use Cookie;

/**
 * Class ViewProductListener
 *
 * @package App\Modules\ProductsCom\Listeners
 */
class ViewProductListener implements ListenerInterface
{
    
    public static function listens(): string
    {
        return 'products::view';
    }
    
    /**
     * Handle the event.
     *
     * @param int $productId
     * @return void
     */
    public function handle(int $productId)
    {
        $comProductsIds = request()->cookie('com_products', '[]');
        $comProductsIds = json_decode($comProductsIds, true) ?: [];
        if (!in_array($productId, $comProductsIds)) {
            array_unshift($comProductsIds, $productId);
        }
        $comProductsIds = array_slice($comProductsIds, 0, 20);
        Cookie::queue(Cookie::forever('com_products', json_encode(array_unique($comProductsIds))));
    }
}
