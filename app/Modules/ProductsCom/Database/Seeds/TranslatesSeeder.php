<?php

namespace App\Modules\ProductsViewed\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'com';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'Самые обсуждаемые',
                ],
                [
                    'name' => 'general.settings-name',
                    'ru' => 'Самые обсуждаемые',
                ],
                [
                    'name' => 'general.widget-name',
                    'ru' => 'Самые обсуждаемые',
                ],
                [
                    'name' => 'general.see-all',
                    'ru' => 'Смотреть все',
                ],
                [
                    'name' => 'settings.group-name',
                    'ru' => 'Самые обсуждаемые',
                ],
                [
                    'name' => 'settings.attributes.per-page',
                    'ru' => 'Количество товаров на странице',
                ],
                [
                    'name' => 'settings.attributes.per-widget',
                    'ru' => 'Количество товаров в виджете',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'general.widget-name',
                    'ru' => 'Самые обсуждаемые',
                ],
            ],
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
