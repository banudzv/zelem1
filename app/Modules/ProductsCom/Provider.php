<?php

namespace App\Modules\ProductsCom;

use App\Core\BaseProvider;
use App\Modules\ProductsCom\Widgets\ComProducts;
use CustomForm\Input;
use CustomSettings, Widget;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\ProductsCom
 */
class Provider extends BaseProvider
{

    /**
    * Set custom presets
    */
    protected function presets()
    {
        $this->setModuleName('com');
        $this->setTranslationsNamespace('com');
        $this->setViewNamespace('com');
        $this->setConfigNamespace('com');
    }
    
    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register module configurable settings
        $settings = CustomSettings::createAndGet('com', 'com::general.settings-name');
        $settings->add(
            Input::create('per-page')
                ->setType('number')
                ->setLabel('com::settings.attributes.per-page')
                ->required(),
            ['required', 'integer', 'min:1']
        );
        $settings->add(
            Input::create('per-widget')
                ->setType('number')
                ->setLabel('com::settings.attributes.per-widget')
                ->required(),
            ['required', 'integer', 'min:1']
        );
    }
    
    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
        Widget::register(ComProducts::class, 'com::products');
    }
    
}
