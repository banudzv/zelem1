<?php

namespace App\Modules\ProductsCom\Controllers\Site;

use Widget;
use App\Core\SiteController;
use Illuminate\Support\Facades\DB;
use App\Components\Parsers\ProductGroup;
use App\Modules\Products\Models\ProductGroupLabel;
use App\Core\Modules\SystemPages\Models\SystemPage;

/**
 * Class ComController
 *
 * @package App\Modules\ProductsCom\Controllers\Site
 */
class ComController extends SiteController
{
    
    /**
     * Products list after search
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /** @var SystemPage $page */
        $page = SystemPage::getByCurrent('slug', 'com-products');
    
        $this->meta($page->current, $page->current->content);
        $this->breadcrumb($page->current->name, 'site.com-products');
        $this->setOtherLanguagesLinks($page);
        $this->pageNumber();
        $this->canonical(route('site.com-products'));
        $productsIds = request()->cookie('com_products', '[]');
        $productsIds = json_decode($productsIds, true);
        $productsIds = is_array($productsIds) ? $productsIds : [];
        
        $productsList = Widget::show('products::com-page', $productsIds ?: [], config('db.com.per-page', 10));
        return $productsList ?? view('com::site.no-com-products', [
            'page' => $page,
        ]);
    }
    
}
