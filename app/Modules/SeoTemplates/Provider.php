<?php

namespace App\Modules\SeoTemplates;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\SeoTemplates\Models\SeoTemplate;
use Cache;
use CustomMenu;
use CustomRoles;
use Illuminate\Database\Eloquent\Collection;
use Schema;
use Seo;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Index
 */
class Provider extends BaseProvider
{

    /**
     * Set custom presets
     */
    protected function presets()
    {
    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register left menu block
        CustomMenu::get()->group()->block('seo', 'seo_templates::general.main-menu-block', 'fa fa-rocket')
            ->setPosition(7)
            ->link('seo_templates::general.menu', RouteObjectValue::make('admin.seo_templates.index'))
            ->additionalRoutesForActiveDetect(RouteObjectValue::make('admin.seo_templates.edit'));
        // Register role scopes
        CustomRoles::add('seo_templates', 'seo_templates::general.permission-name')
            ->except(RoleRule::VIEW, RoleRule::STORE, RoleRule::DELETE);
    }

    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
        if (Schema::hasTable((new SeoTemplate())->getTable())) {
            $this->app->booted(
                function () {
                    foreach ($this->getSeoTemplates() as $template) {
                        Seo::site()->addAvailableTemplate($template->alias, $template->current);
                    }
                }
            );
        }
    }

    /**
     * @return SeoTemplate[]|Collection
     */
    protected function getSeoTemplates()
    {
        return Cache::remember(
            'seo_templates_all',
            3600,
            function () {
                return SeoTemplate::query()
                    ->with('current')
                    ->get();
            }
        );
    }

}
