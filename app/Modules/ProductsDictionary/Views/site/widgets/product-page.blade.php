@php
    /** @var \CustomForm\Select $dictionarySelect */
@endphp

<div class="grid _items-center _justify-center _def-justify-start _mb-def _nml-sm">
    <div class="gcell _pl-sm">
        <form onsubmit="event.preventDefault()">
            {!! $dictionarySelect->render() !!}
        </form>
    </div>
</div>
