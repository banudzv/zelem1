@php
    /** @var \CustomForm\Select $dictionarySelect */
@endphp

<div class="additional_specification">
    <div class="gcell gcell--12 _pb-def">
        {!! $dictionarySelect->render() !!}
    </div>
</div>
