<?php

namespace App\Modules\ProductsDictionary\Widgets\Admin;

use App\Components\Widget\AbstractWidget;
use App\Modules\Orders\Models\OrderItem;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\ProductsDictionary\Models\Dictionary;
use App\Modules\ProductsDictionary\Models\DictionaryRelation;
use CustomForm\SimpleSelect;

/**
 * Class DictionaryFormOnOrderPage
 *
 * @package App\Modules\ProductsDictionary\Widgets
 */
class DictionaryFormOnOrderViewPage implements AbstractWidget
{
    /**
     * @var OrderItem
     */
    protected $item;

    /**
     * DictionaryFormOnOrderViewPage constructor.
     * @param OrderItem $item
     */
    public function __construct(OrderItem $item)
    {
        $this->item = $item;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function render()
    {
        $dictionaryValues = [];
        $status = config('db.products_dictionary.select_status', 0);
        if (!$status) {
            $product = Product::whereId($this->item->product_id)->first();
            $relations = DictionaryRelation::whereGroupId($product->group_id)->get();
            foreach ($relations as $rel) {
                $dictionaryValues[$rel->dictionary()->withTrashed()->first()->id] = $rel->dictionary()->withTrashed()->first()->current->name;
            }
        } else {
            $dictionaries = Dictionary::with(['current'])->withTrashed()->get();
            foreach ($dictionaries as $dictionary) {
                $dictionaryValues[$dictionary->id] = $dictionary->current->name;
            }
        }
        $keys = array_keys($dictionaryValues);

        $dictionarySelect = SimpleSelect::create('dictionary')
            ->add($dictionaryValues)
            ->setValue(in_array($this->item->dictionary_id, $keys) ? $this->item->dictionary_id : array_shift($keys))
            ->setLabel('')
            ->addClassesToDiv('col-md-10')
            ->setOptions(['id' => 'dictionary-select']);


        return $dictionarySelect->render();
    }

}
