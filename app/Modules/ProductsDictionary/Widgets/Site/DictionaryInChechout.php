<?php

namespace App\Modules\ProductsDictionary\Widgets\Site;


use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\ProductsDictionary\Models\Dictionary;
use App\Modules\ProductsDictionary\Models\DictionaryRelation;
use CustomForm\Select;

/**
 * Class DictionaryInChechout
 *
 * @package App\Modules\ProductsDictionary\Widgets
 */
class DictionaryInChechout implements AbstractWidget
{
    /**
     * @var int|mixed|null
     */
    protected $group_id;
    /**
     * @var array
     */
    protected $selected;


    /**
     * DictionaryInChechout constructor.
     * @param int|null $productId
     * @param array|null $selected
     */
    public function __construct(?int $productId, $selected = null)
    {
        $product = Product::whereId($productId)->firstOrFail();
        $this->group_id = $product->group_id;
        if (isset($selected)) {
            $this->selected = $selected;
        } else {
            $this->selected = [];
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function render()
    {
        $selected = [];
        $status = config('db.products_dictionary.select_status');

        if (isset($status) && $status) {
            $dictionaries = Dictionary::with('current')->get();
            foreach ($dictionaries as $obj) {
                $selected[$obj->id] = $obj->current->name;
            }
        } else {
            $relations = DictionaryRelation::whereGroupId($this->group_id)->get();
            foreach ($relations as $rel) {
                $result = $rel->dictionary()->first();
                $selected[$result->id] = $result->current->name;
            }
        }

        $dictionarySelect = Select::create('dictionary_id')
            ->add($selected)
            ->setValue($this->selected)
            ->setLabel(config('db.products_dictionary.' . \Lang::getLocale() . '_title'))
            ->setOptions(['id' => 'dictionary-select']);

        return view('products_dictionary::site.widgets.checkout-item', [
            'dictionarySelect' => $dictionarySelect,
            'values' => $selected
        ]);
    }

}
