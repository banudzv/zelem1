<?php

namespace App\Modules\ProductsDictionary\Widgets\Site;

use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\Product;
use App\Modules\ProductsDictionary\Models\Dictionary;
use CustomForm\Select;

/**
 * Class DictionaryFormOnProductPage
 *
 * @package App\Modules\ProductsDictionary\Widgets
 */
class DictionarySelectOnProductPage implements AbstractWidget
{
    /**
     * @var Product|null
     */
    protected $product;
    /**
     * @var array
     */
    protected $selected;

    /**
     * DictionarySelectOnProductPage constructor.
     * @param Product $product
     * @param array|null $selected
     */
    public function __construct(Product $product, $selected = null)
    {
        $this->product = $product;
        if (isset($selected)) {
            $this->selected = $selected;
        } else {
            $this->selected = [];
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function render()
    {
        if (!config('db.products_dictionary.site_status')) {
            return null;
        }
        
        if (config('db.products_dictionary.select_status')) {
            $dictionary = Dictionary::with('current')->whereActive(true)->get()->mapWithKeys(function (Dictionary $dictionary) {
                return [$dictionary->id => $dictionary->current->name];
            })->toArray();
        } else {
            $dictionary = $this->product->group->dictionaries->where('active', true)->mapWithKeys(function (Dictionary $dictionary) {
                return [$dictionary->id => $dictionary->current->name];
            })->toArray();
        }
        
        if (empty($dictionary)) {
            return null;
        }

        $dictionarySelect = Select::create('dictionary_id')
            ->add($dictionary)
            ->setValue($this->selected)
            ->setLabel(false)
            ->setOptions(['id' => 'dictionary-select', 'class' => 'select']);

        return view('products_dictionary::site.widgets.product-page', [
            'dictionarySelect' => $dictionarySelect,
        ]);
    }

}
