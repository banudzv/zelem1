<?php

namespace App\Modules\ProductsDictionary\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\ProductsDictionary\Models\Dictionary;

/**
 * Class DictionaryMail
 *
 * @package App\Modules\Products\Widgets
 */
class DictionaryMail implements AbstractWidget
{

    /**
     * @var mixed
     */
    protected $dictionaryId;

    /**
     * DictionaryMail constructor.
     * @param ProductGroup $product
     */
    public function __construct(ProductGroup $product)
    {
        $this->dictionaryId = $product->dictionary_id;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        $dictionary = Dictionary::find($this->dictionaryId);
        return view('products_dictionary::mail.order-item', [
            'dictionary' => $dictionary,
        ]);
    }

}
