<?php

namespace App\Modules\ProductsDictionary\Models;

use App\Traits\ActiveScopeTrait;
use App\Traits\CheckRelation;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\ProductsDictionary\Models\Dictionary
 *
 * @property int $id
 * @property int $dictionary_id
 * @property int $group_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Modules\ProductsDictionary\Models\Dictionary $dictionary
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation active($active = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation whereDictionaryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\ProductsDictionary\Models\DictionaryRelation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DictionaryRelation extends Model
{
    use ActiveScopeTrait, EloquentTentacle, CheckRelation;
    
    protected $table = 'products_dictionary_related';

    protected $fillable = ['group_id', 'dictionary_id'];

    public function dictionary()
    {
        return $this->belongsTo(Dictionary::class, 'dictionary_id', 'id')
            ->withDefault();
    }
}
