@php
/** @var \App\Modules\Engine\Models\EngineNumber[] $numbers */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter ?? '' !!}
        <div class="box">
            <div class="box-body table-responsive no-padding mailbox-messages">
                <table class="table table-hover">
                    <tr>
                        <th>@lang('validation.attributes.name')</th>
                        <th>@lang('engine::field.vendor_id')</th>
                        <th>@lang('engine::field.model_id')</th>
                        <th>@lang('engine::field.volume_id')</th>
                        <th>@lang('engine::field.power_id')</th>
                        <th></th>
                    </tr>
                    @foreach($numbers AS $number)
                        <tr>
                            <td>{{ $number->engine_number }}</td>
                            <td>{!! $number->vendor ? $number->vendor->name : '&mdash;' !!}</td>
                            <td>{!! $number->model ? $number->model->name : '&mdash;' !!}</td>
                            <td>{!! $number->volume ? $number->volume->name : '&mdash;' !!}</td>
                            <td>{!! ($number->power && $number->power->current) ? $number->power->current->name : '&mdash;' !!}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.numbers.edit', $number->id) !!}
                                {!! \App\Components\Buttons::delete('admin.numbers.destroy', $number->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $numbers->appends(request()->except('page'))->links() }}</div>
    </div>
@stop

@include('engine::scripts', ['postfix' => ''])
