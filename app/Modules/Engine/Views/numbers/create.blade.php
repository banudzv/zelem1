@php
/** @var \CustomForm\Builder\Form $form */
$form->buttons->showCloseButton(route('admin.numbers.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.numbers.store']) !!}
        {!! $form->render() !!}
    {!! Form::close() !!}
@stop

@include('engine::scripts')
