@php
/** @var \CustomForm\Builder\Form $form */
$form->buttons->showCloseButton(route('admin.models.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.models.store']) !!}
        {!! $form->render() !!}
    {!! Form::close() !!}
@stop
