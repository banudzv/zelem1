@php
/** @var \App\Modules\Engine\Models\Model[] $models */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter ?? '' !!}
        <div class="box">
            <div class="box-body table-responsive no-padding mailbox-messages">
                <table class="table table-hover">
                    <tr>
                        <th>@lang('validation.attributes.name')</th>
                        <th>@lang('engine::field.vendor_id')</th>
                        <th></th>
                    </tr>
                    @foreach($models AS $model)
                        <tr>
                            <td>{{ $model->name }}</td>
                            <td>{!! $model->vendor ? $model->vendor->name : '&mdash;' !!}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.models.edit', $model->id) !!}
                                {!! \App\Components\Buttons::delete('admin.models.destroy', $model->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $models->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
