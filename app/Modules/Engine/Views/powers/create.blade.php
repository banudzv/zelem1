@php
/** @var \CustomForm\Builder\Form $form */
$form->buttons->showCloseButton(route('admin.powers.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.powers.store']) !!}
        {!! $form->render() !!}
    {!! Form::close() !!}
@stop

@include('engine::scripts')
