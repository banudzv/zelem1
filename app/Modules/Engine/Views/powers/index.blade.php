@php
/** @var \App\Modules\Engine\Models\Power[] $powers */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter ?? '' !!}
        <div class="box">
            <div class="box-body table-responsive no-padding mailbox-messages">
                <table class="table table-hover">
                    <tr>
                        <th>@lang('validation.attributes.name')</th>
                        <th>@lang('engine::field.vendor_id')</th>
                        <th>@lang('engine::field.model_id')</th>
                        <th>@lang('engine::field.volume_id')</th>
                        <th></th>
                    </tr>
                    @foreach($powers AS $power)
                        <tr>
                            <td>{{ $power->current->name }}</td>
                            <td>{!! $power->vendor ? $power->vendor->name : '&mdash;' !!}</td>
                            <td>{!! $power->model ? $power->model->name : '&mdash;' !!}</td>
                            <td>{!! $power->volume ? $power->volume->name : '&mdash;' !!}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.powers.edit', $power->id) !!}
                                {!! \App\Components\Buttons::delete('admin.powers.destroy', $power->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $powers->appends(request()->except('page'))->links() }}</div>
    </div>
@stop

@include('engine::scripts', ['postfix' => ''])
