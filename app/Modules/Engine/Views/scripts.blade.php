@php
/** @var string $postfix */
$postfix = $postfix ?? '_id';
@endphp

@push('scripts')
    <script>
        const $vendorsSelect = $("select[name='vendor<?= $postfix ?>']");
        const $modelsSelect = $("select[name='model<?= $postfix ?>']");
        const $volumesSelect = $("select[name='volume<?= $postfix ?>']");
        const $powersSelect = $("select[name='power<?= $postfix ?>']");

        if($modelsSelect.length) {
            $vendorsSelect.on('change', function () {
                setOptions($modelsSelect);
                if ($volumesSelect.length) {
                    setOptions($volumesSelect);
                }
                if ($powersSelect.length) {
                    setOptions($powersSelect);
                }
                sendRequest("{{ route('api.ajax.models') }}", {
                    vendorId: $vendorsSelect.val(),
                }).done(function (data) {
                    setOptions($modelsSelect, data.options);
                });
            });
        }

        if($volumesSelect.length) {
            $modelsSelect.on('change', function () {
                setOptions($volumesSelect);
                if ($powersSelect.length) {
                    setOptions($powersSelect);
                }
                sendRequest("{{ route('api.ajax.volumes') }}", {
                    modelId: $modelsSelect.val(),
                }).done(function (data) {
                    setOptions($volumesSelect, data.options);
                });
            });
        }

        if($powersSelect.length) {
            $volumesSelect.on('change', function () {
                setOptions($powersSelect);
                sendRequest("{{ route('api.ajax.powers') }}", {
                    volumeId: $volumesSelect.val(),
                }).done(function (data) {
                    setOptions($powersSelect, data.options);
                });
            });
        }
    </script>
@endpush
