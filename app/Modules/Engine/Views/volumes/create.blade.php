@php
/** @var \CustomForm\Builder\Form $form */
$form->buttons->showCloseButton(route('admin.volumes.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.volumes.store']) !!}
        {!! $form->render() !!}
    {!! Form::close() !!}
@stop

@include('engine::scripts')
