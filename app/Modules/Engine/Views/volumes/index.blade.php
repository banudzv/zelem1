@php
/** @var \App\Modules\Engine\Models\Volume[] $volumes */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter ?? '' !!}
        <div class="box">
            <div class="box-body table-responsive no-padding mailbox-messages">
                <table class="table table-hover">
                    <tr>
                        <th>@lang('validation.attributes.name')</th>
                        <th>@lang('engine::field.vendor_id')</th>
                        <th>@lang('engine::field.model_id')</th>
                        <th></th>
                    </tr>
                    @foreach($volumes AS $volume)
                        <tr>
                            <td>{{ $volume->name }}</td>
                            <td>{!! $volume->vendor ? $volume->vendor->name : '&mdash;' !!}</td>
                            <td>{!! $volume->model ? $volume->model->name : '&mdash;' !!}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.volumes.edit', $volume->id) !!}
                                {!! \App\Components\Buttons::delete('admin.volumes.destroy', $volume->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $volumes->appends(request()->except('page'))->links() }}</div>
    </div>
@stop

@include('engine::scripts', ['postfix' => ''])
