@php
/** @var \App\Modules\Engine\Models\Vendor[] $vendors */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter ?? '' !!}
        <div class="box">
            <div class="box-body table-responsive no-padding mailbox-messages">
                <table class="table table-hover">
                    <tr>
                        <th>{{ __('validation.attributes.name') }}</th>
                        <th></th>
                    </tr>
                    @foreach($vendors AS $vendor)
                        <tr>
                            <td>{{ $vendor->name }}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.vendors.edit', $vendor->id) !!}
                                {!! \App\Components\Buttons::delete('admin.vendors.destroy', $vendor->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $vendors->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
