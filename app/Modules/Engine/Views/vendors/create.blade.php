@php
/** @var \CustomForm\Builder\Form $form */
$form->buttons->showCloseButton(route('admin.vendors.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.vendors.store']) !!}
        {!! $form->render() !!}
    {!! Form::close() !!}
@stop
