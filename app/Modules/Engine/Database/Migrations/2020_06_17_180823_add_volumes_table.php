<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVolumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_volumes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->index();
            $table->integer('vendor_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->foreign('vendor_id')->references('id')->on('car_vendors')
                ->index('volumes_vendor_id_vendors_id')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('model_id')->references('id')->on('car_models')
                ->index('volumes_model_id_models_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_volumes', function (Blueprint $table) {
            $table->dropForeign('volumes_vendor_id_vendors_id');
            $table->dropForeign('volumes_model_id_models_id');
        });
        Schema::drop('car_volumes');
    }
}
