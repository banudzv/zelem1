<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_models', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->index();
            $table->integer('vendor_id')->unsigned();
            $table->foreign('vendor_id')->references('id')->on('car_vendors')
                ->index('car_models_vendor_id_car_vendors_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_models', function (Blueprint $table) {
            $table->dropForeign('car_models_vendor_id_car_vendors_id');
        });
        Schema::drop('car_models');
    }
}
