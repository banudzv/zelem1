<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEngineNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_engine_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->integer('volume_id')->unsigned();
            $table->integer('power_id')->unsigned();
            $table->string('engine_number')->index();
            $table->foreign('vendor_id')->references('id')->on('car_vendors')
                ->index('cen_vendor_id_vendors_id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('model_id')->references('id')->on('car_models')
                ->index('cen_model_id_models_id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('volume_id')->references('id')->on('car_volumes')
                ->index('cen_volume_id_volumes_id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('power_id')->references('id')->on('car_powers')
                ->index('cen_power_id_powers_id')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::create('product_engine_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->string('engine_number')->index();
            $table->foreign('group_id')->references('id')->on('products_groups')
                ->index('pen_group_id_pg_id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_engine_numbers', function (Blueprint $table) {
            $table->dropForeign('pen_product_id_p_id');
            $table->dropForeign('pen_group_id_pg_id');
        });
        Schema::drop('product_engine_numbers');
        Schema::table('car_engine_numbers', function (Blueprint $table) {
            $table->dropForeign('cen_vendor_id_vendors_id');
            $table->dropForeign('cen_model_id_models_id');
            $table->dropForeign('cen_volume_id_volumes_id');
            $table->dropForeign('cen_power_id_powers_id');
        });
        Schema::drop('car_engine_numbers');
    }
}
