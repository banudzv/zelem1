<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_years', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('year')->index();
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('products_groups')
                ->index('years_group_id_products_groups_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_years', function (Blueprint $table) {
            $table->dropForeign('years_group_id_products_groups_id');
        });
        Schema::drop('car_years');
    }
}
