<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_powers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('vendor_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->integer('volume_id')->unsigned();
            $table->foreign('vendor_id')->references('id')->on('car_vendors')
                ->index('powers_vendor_id_vendors_id')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('model_id')->references('id')->on('car_models')
                ->index('powers_model_id_models_id')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('volume_id')->references('id')->on('car_volumes')
                ->index('powers_volume_id_volumes_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::create('car_powers_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->index();
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);
            $table->foreign('language')->references('slug')->on('languages')
                ->index('powers_translates_language_languages_slug')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('car_powers')
                ->index('powers_translates_row_id_powers_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_powers_translates', function (Blueprint $table) {
            $table->dropForeign('powers_translates_language_languages_slug');
            $table->dropForeign('powers_translates_row_id_powers_id');
        });
        Schema::drop('car_powers_translates');
        Schema::table('car_volumes', function (Blueprint $table) {
            $table->dropForeign('volumes_vendor_id_vendors_id');
            $table->dropForeign('volumes_model_id_models_id');
            $table->dropForeign('powers_volume_id_volumes_id');
        });
        Schema::drop('car_volumes');
    }
}
