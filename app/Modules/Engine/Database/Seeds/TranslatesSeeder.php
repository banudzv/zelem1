<?php

namespace App\Modules\Engine\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'engine';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'menu.block',
                    'ru' => 'Двигатель',
                ],
                [
                    'name' => 'menu.vendors',
                    'ru' => 'Марки авто',
                ],
                [
                    'name' => 'seo.vendors.index',
                    'ru' => 'Марки авто',
                ],
                [
                    'name' => 'seo.vendors.create',
                    'ru' => 'Создать марку авто',
                ],
                [
                    'name' => 'seo.vendors.edit',
                    'ru' => 'Редактировать марку авто',
                ],
                [
                    'name' => 'menu.models',
                    'ru' => 'Модели авто',
                ],
                [
                    'name' => 'seo.models.index',
                    'ru' => 'Модели авто',
                ],
                [
                    'name' => 'seo.models.create',
                    'ru' => 'Создать модель авто',
                ],
                [
                    'name' => 'seo.models.edit',
                    'ru' => 'Редактировать модель авто',
                ],
                [
                    'name' => 'menu.volumes',
                    'ru' => 'Типы двигателей',
                ],
                [
                    'name' => 'seo.volumes.index',
                    'ru' => 'Типы двигателей',
                ],
                [
                    'name' => 'seo.volumes.create',
                    'ru' => 'Создать тип двигателя',
                ],
                [
                    'name' => 'seo.volumes.edit',
                    'ru' => 'Редактировать тип двигателя',
                ],
                [
                    'name' => 'menu.powers',
                    'ru' => 'Мощности двигателей',
                ],
                [
                    'name' => 'seo.powers.index',
                    'ru' => 'Мощности двигателей',
                ],
                [
                    'name' => 'seo.powers.create',
                    'ru' => 'Добавить мощность двигателя',
                ],
                [
                    'name' => 'seo.powers.edit',
                    'ru' => 'Редактировать мощность двигателя',
                ],
                [
                    'name' => 'menu.numbers',
                    'ru' => 'Номера двигателей',
                ],
                [
                    'name' => 'seo.numbers.index',
                    'ru' => 'Номера двигателей',
                ],
                [
                    'name' => 'seo.numbers.create',
                    'ru' => 'Добавить номер двигателя',
                ],
                [
                    'name' => 'seo.numbers.edit',
                    'ru' => 'Редактировать номер двигателя',
                ],
                [
                    'name' => 'field.vendor_id',
                    'ru' => 'Марка',
                ],
                [
                    'name' => 'field.model_id',
                    'ru' => 'Модель',
                ],
                [
                    'name' => 'field.volume_id',
                    'ru' => 'Тип',
                ],
                [
                    'name' => 'field.power_id',
                    'ru' => 'Мощность',
                ],
                [
                    'name' => 'field.engine_number',
                    'ru' => 'Номер',
                ],
                [
                    'name' => 'status.in_queue',
                    'ru' => 'В очереди',
                ],
                [
                    'name' => 'status.in_progress',
                    'ru' => 'В процессе',
                ],
                [
                    'name' => 'status.finished',
                    'ru' => 'Завершен',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.filter.all',
                    'ru' => 'Все',
                ],
                [
                    'name' => 'site.filter.vendor',
                    'ru' => 'Марка авто',
                ],
                [
                    'name' => 'site.filter.model',
                    'ru' => 'Модель авто',
                ],
                [
                    'name' => 'site.filter.volume',
                    'ru' => 'Объем двигателя',
                ],
                [
                    'name' => 'site.filter.power',
                    'ru' => 'Мощность двигателя',
                ],
                [
                    'name' => 'site.filter.engine-number',
                    'ru' => 'Номер двигателя',
                ],
            ],
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
