<?php

namespace App\Modules\Engine;

use App\Core\BaseProvider;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;

/**
 * Module configuration class
 *
 * @package App\Modules\Engine
 */
class Provider extends BaseProvider
{
    /**
     * {@inheritDoc}
     */
    protected function presets()
    {
    }

    /**
     * {@inheritDoc}
     * @throws WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        $block = \CustomMenu::get()->group()->block('engine', 'engine::menu.block');
        $block
            ->link('engine::menu.vendors', RouteObjectValue::make('admin.vendors.index'))
            ->setPosition(1)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.vendors.create'),
                RouteObjectValue::make('admin.vendors.edit')
            );
        $block
            ->link('engine::menu.models', RouteObjectValue::make('admin.models.index'))
            ->setPosition(2)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.models.create'),
                RouteObjectValue::make('admin.models.edit')
            );
        $block
            ->link('engine::menu.volumes', RouteObjectValue::make('admin.volumes.index'))
            ->setPosition(3)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.volumes.create'),
                RouteObjectValue::make('admin.volumes.edit')
            );
        $block
            ->link('engine::menu.powers', RouteObjectValue::make('admin.powers.index'))
            ->setPosition(4)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.powers.create'),
                RouteObjectValue::make('admin.powers.edit')
            );
        $block
            ->link('engine::menu.numbers', RouteObjectValue::make('admin.numbers.index'))
            ->setPosition(5)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.numbers.create'),
                RouteObjectValue::make('admin.numbers.edit')
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function afterBoot()
    {
    }
}
