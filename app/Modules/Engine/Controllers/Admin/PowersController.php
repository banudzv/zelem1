<?php

namespace App\Modules\Engine\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Filters\PowersFilter;
use App\Modules\Engine\Forms\PowerForm;
use App\Modules\Engine\Models\Power;
use App\Modules\Engine\Requests\PowerRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class PowersController
 * @package App\Modules\Engine\Controllers\Admin
 */
class PowersController extends AdminController
{
    /**
     * @var int Powers per page
     */
    protected $limit;

    /**
     * PowersController constructor.
     */
    public function __construct()
    {
        $this->limit = 20;
        \Seo::breadcrumbs()->add('engine::seo.powers.index', RouteObjectValue::make('admin.powers.index'));
    }

    /**
     * Page with paginated powers list.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $this->addCreateButton('admin.powers.create');
        \Seo::meta()->setH1('engine::seo.powers.index');
        $powers = Power::filter($request->only('name', 'vendor', 'model', 'volume'))
            ->with(['vendor', 'model', 'current', 'volume'])
            ->latest('id')->paginate($this->limit);
        $filter = PowersFilter::showFilter($request);

        return view('engine::powers.index', compact('powers', 'filter'));
    }

    /**
     * Renders a page with for for creating power.
     *
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function create()
    {
        \Seo::breadcrumbs()->add('engine::seo.powers.create');
        \Seo::meta()->setH1('engine::seo.powers.create');
        $this->jsValidation(new PowerRequest());

        return view('engine::powers.create', ['form' => PowerForm::make()]);
    }

    /**
     * Creates power in the database.
     *
     * @param PowerRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function store(PowerRequest $request)
    {
        try {
            $power = new Power();
            if ($error = $power->createRow($request->validated())) {
                return $this->afterFail($error);
            }

            return $this->afterStore(['id' => $power->id]);
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Editing power page with the form.
     *
     * @param Power $power
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function edit(Power $power)
    {
        \Seo::breadcrumbs()->add($power->current->name);
        \Seo::meta()->setH1('engine::seo.powers.edit');
        $this->jsValidation(new PowerRequest());

        return view('engine::powers.update', ['form' => PowerForm::make($power)]);
    }

    /**
     * Updates power in the database.
     *
     * @param PowerRequest $request
     * @param Power $power
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function update(PowerRequest $request, Power $power)
    {
        try {
            if ($error = $power->updateRow($request->validated())) {
                return $this->afterFail($error);
            }

            return $this->afterUpdate();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Deletes power from the database.
     *
     * @param Power $power
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(Power $power)
    {
        try {
            $power->delete();

            return $this->afterDestroy();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }
}
