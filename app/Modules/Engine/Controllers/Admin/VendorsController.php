<?php

namespace App\Modules\Engine\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Filters\VendorsFilter;
use App\Modules\Engine\Forms\VendorForm;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Requests\VendorRequest;
use Cache;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Seo;

/**
 * Class VendorsController
 * @package App\Modules\Engine\Controllers\Admin
 */
class VendorsController extends AdminController
{
    /**
     * @var int Vendors per page
     */
    protected $limit;

    /**
     * VendorsController constructor.
     */
    public function __construct()
    {
        $this->limit = 20;
        Seo::breadcrumbs()->add('engine::seo.vendors.index', RouteObjectValue::make('admin.vendors.index'));
    }

    /**
     * Page with paginated vendors list.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $this->addCreateButton('admin.vendors.create');
        Seo::meta()->setH1('engine::seo.vendors.index');
        $vendors = Vendor::query()
            ->filter($request->only('name'))
            ->oldest('name')
            ->paginate($this->limit);
        $filter = VendorsFilter::showFilter($request);

        return view('engine::vendors.index', compact('vendors', 'filter'));
    }

    /**
     * Renders a page with for for creating vendor.
     *
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function create()
    {
        Seo::breadcrumbs()->add('engine::seo.vendors.create');
        Seo::meta()->setH1('engine::seo.vendors.create');
        $this->jsValidation(new VendorRequest());

        return view('engine::vendors.create', ['form' => VendorForm::make()]);
    }

    /**
     * Creates vendor in the database.
     *
     * @param VendorRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function store(VendorRequest $request)
    {
        try {
            $vendor = Vendor::create($request->validated());

            $this->cacheClear();

            return $this->afterStore(['id' => $vendor->id]);
        } catch (Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Editing vendor page with the form.
     *
     * @param Vendor $vendor
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function edit(Vendor $vendor)
    {
        Seo::breadcrumbs()->add($vendor->name);
        Seo::meta()->setH1('engine::seo.vendors.edit');
        $this->jsValidation(new VendorRequest());

        return view('engine::vendors.update', ['form' => VendorForm::make($vendor)]);
    }

    /**
     * Updates vendor in the database.
     *
     * @param VendorRequest $request
     * @param Vendor $vendor
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function update(VendorRequest $request, Vendor $vendor)
    {
        try {
            $vendor->update($request->validated());

            $this->cacheClear();

            return $this->afterUpdate();
        } catch (Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Deletes vendor from the database.
     *
     * @param Vendor $vendor
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(Vendor $vendor)
    {
        try {
            $vendor->delete();

            $this->cacheClear();

            return $this->afterDestroy();
        } catch (Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    protected function cacheClear(): void
    {
        Cache::clear();
    }
}
