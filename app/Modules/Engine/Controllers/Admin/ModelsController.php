<?php

namespace App\Modules\Engine\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Filters\ModelsFilter;
use App\Modules\Engine\Forms\ModelForm;
use App\Modules\Engine\Models\Model;
use App\Modules\Engine\Requests\ModelRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class ModelsController
 * @package App\Modules\Engine\Controllers\Admin
 */
class ModelsController extends AdminController
{
    /**
     * @var int Models per page
     */
    protected $limit;

    /**
     * ModelsController constructor.
     */
    public function __construct()
    {
        $this->limit = 20;
        \Seo::breadcrumbs()->add('engine::seo.models.index', RouteObjectValue::make('admin.models.index'));
    }

    /**
     * Page with paginated models list.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $this->addCreateButton('admin.models.create');
        \Seo::meta()->setH1('engine::seo.models.index');
        $models = Model::filter($request->only('name', 'vendor'))
            ->with('vendor')->oldest('name')->paginate($this->limit);
        $filter = ModelsFilter::showFilter($request);

        return view('engine::models.index', compact('models', 'filter'));
    }

    /**
     * Renders a page with for for creating model.
     *
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function create()
    {
        \Seo::breadcrumbs()->add('engine::seo.models.create');
        \Seo::meta()->setH1('engine::seo.models.create');
        $this->jsValidation(new ModelRequest());

        return view('engine::models.create', ['form' => ModelForm::make()]);
    }

    /**
     * Creates models in the database.
     *
     * @param ModelRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function store(ModelRequest $request)
    {
        try {
            $model = Model::create($request->validated());

            return $this->afterStore(['id' => $model->id]);
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Editing model page with the form.
     *
     * @param Model $model
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function edit(Model $model)
    {
        \Seo::breadcrumbs()->add($model->name);
        \Seo::meta()->setH1('engine::seo.models.edit');
        $this->jsValidation(new ModelRequest());

        return view('engine::models.update', ['form' => ModelForm::make($model)]);
    }

    /**
     * Updates model in the database.
     *
     * @param ModelRequest $request
     * @param Model $model
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function update(ModelRequest $request, Model $model)
    {
        try {
            $model->update($request->validated());

            return $this->afterUpdate();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Deletes model from the database.
     *
     * @param Model $model
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(Model $model)
    {
        try {
            $model->delete();

            return $this->afterDestroy();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }
}
