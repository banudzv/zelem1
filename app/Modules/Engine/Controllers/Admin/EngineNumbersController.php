<?php

namespace App\Modules\Engine\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Filters\EngineNumbersFilter;
use App\Modules\Engine\Forms\EngineNumberForm;
use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Engine\Requests\EngineNumberRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class EngineNumbersController
 * @package App\Modules\Engine\Controllers\Admin
 */
class EngineNumbersController extends AdminController
{
    /**
     * @var int Engine numbers per page
     */
    protected $limit;

    /**
     * PowersController constructor.
     */
    public function __construct()
    {
        $this->limit = 20;
        \Seo::breadcrumbs()->add('engine::seo.numbers.index', RouteObjectValue::make('admin.numbers.index'));
    }

    /**
     * Page with paginated powers list.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $this->addCreateButton('admin.numbers.create');
        \Seo::meta()->setH1('engine::seo.numbers.index');
        $numbers = EngineNumber::filter($request->only('number', 'vendor', 'model', 'volume', 'power.current'))
            ->with(['vendor', 'model', 'volume', 'power'])
            ->latest('id')->paginate($this->limit);
        $filter = EngineNumbersFilter::showFilter($request);

        return view('engine::numbers.index', compact('numbers', 'filter'));
    }

    /**
     * Renders a page with for for creating engine number.
     *
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function create()
    {
        \Seo::breadcrumbs()->add('engine::seo.numbers.create');
        \Seo::meta()->setH1('engine::seo.numbers.create');
        $this->jsValidation(new EngineNumberRequest());

        return view('engine::numbers.create', ['form' => EngineNumberForm::make()]);
    }

    /**
     * Creates engine number in the database.
     *
     * @param EngineNumberRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function store(EngineNumberRequest $request)
    {
        try {
            $number = EngineNumber::create($request->validated());

            return $this->afterStore(['id' => $number->id]);
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Editing power page with the form.
     *
     * @param EngineNumber $number
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function edit(EngineNumber $number)
    {
        \Seo::breadcrumbs()->add($number->engine_number);
        \Seo::meta()->setH1('engine::seo.numbers.edit');
        $this->jsValidation(new EngineNumberRequest());

        return view('engine::numbers.update', ['form' => EngineNumberForm::make($number)]);
    }

    /**
     * Updates engine number in the database.
     *
     * @param EngineNumberRequest $request
     * @param EngineNumber $number
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function update(EngineNumberRequest $request, EngineNumber $number)
    {
        try {
            $number->update($request->validated());

            return $this->afterUpdate();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Deletes engine number from the database.
     *
     * @param EngineNumber $number
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(EngineNumber $number)
    {
        try {
            $number->delete();

            return $this->afterDestroy();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }
}
