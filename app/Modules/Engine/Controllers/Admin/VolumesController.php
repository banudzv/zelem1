<?php

namespace App\Modules\Engine\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Filters\VolumesFilter;
use App\Modules\Engine\Forms\VolumeForm;
use App\Modules\Engine\Models\Volume;
use App\Modules\Engine\Requests\VolumeRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class VolumesController
 * @package App\Modules\Engine\Controllers\Admin
 */
class VolumesController extends AdminController
{
    /**
     * @var int Volumes per page
     */
    protected $limit;

    /**
     * VolumesController constructor.
     */
    public function __construct()
    {
        $this->limit = 20;
        \Seo::breadcrumbs()->add('engine::seo.volumes.index', RouteObjectValue::make('admin.volumes.index'));
    }

    /**
     * Page with paginated volumes list.
     *
     * @param Request $request
     * @return Application|Factory|View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $this->addCreateButton('admin.volumes.create');
        \Seo::meta()->setH1('engine::seo.volumes.index');
        $volumes = Volume::filter($request->only('name', 'vendor', 'model'))
            ->with(['vendor', 'model'])->oldest('name')->paginate($this->limit);
        $filter = VolumesFilter::showFilter($request);

        return view('engine::volumes.index', compact('volumes', 'filter'));
    }

    /**
     * Renders a page with for for creating volume.
     *
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function create()
    {
        \Seo::breadcrumbs()->add('engine::seo.volumes.create');
        \Seo::meta()->setH1('engine::seo.volumes.create');
        $this->jsValidation(new VolumeRequest());

        return view('engine::volumes.create', ['form' => VolumeForm::make()]);
    }

    /**
     * Creates volume in the database.
     *
     * @param VolumeRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function store(VolumeRequest $request)
    {
        try {
            $volume = Volume::create($request->validated());

            return $this->afterStore(['id' => $volume->id]);
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Editing volume page with the form.
     *
     * @param Volume $volume
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function edit(Volume $volume)
    {
        \Seo::breadcrumbs()->add($volume->name);
        \Seo::meta()->setH1('engine::seo.volumes.edit');
        $this->jsValidation(new VolumeRequest());

        return view('engine::volumes.update', ['form' => VolumeForm::make($volume)]);
    }

    /**
     * Updates volume in the database.
     *
     * @param VolumeRequest $request
     * @param Volume $volume
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function update(VolumeRequest $request, Volume $volume)
    {
        try {
            $volume->update($request->validated());

            return $this->afterUpdate();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Deletes volume from the database.
     *
     * @param Volume $volume
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(Volume $volume)
    {
        try {
            $volume->delete();

            return $this->afterDestroy();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }
}
