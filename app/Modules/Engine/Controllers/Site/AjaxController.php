<?php

namespace App\Modules\Engine\Controllers\Site;

use App\Core\AjaxTrait;
use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Engine\Models\Model;
use App\Modules\Engine\Models\PowerTranslates;
use App\Modules\Engine\Models\Volume;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class AjaxController
 * @package App\Modules\Engine\Controllers\Site
 */
class AjaxController
{
    use AjaxTrait;

    /**
     * Returns models list.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function models(Request $request)
    {
        $modelsQuery = Model::query();
        if (is_array($request->input('vendorId'))) {
            $modelsQuery->whereIn('vendor_id', $request->input('vendorId'));
        } else {
            $modelsQuery->where('vendor_id', (int)$request->input('vendorId'));
        }

        return $this->successJsonAnswer([
            'options' => $modelsQuery->oldest('name')->get(['name', 'id', 'vendor_id'])->toArray(),
            'name' => 'models',
        ]);
    }

    /**
     * Returns volumes list.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function volumes(Request $request)
    {
        return $this->successJsonAnswer([
            'options' => Volume::whereModelId((int)$request->input('modelId'))
                ->oldest('name')->get(['name', 'id'])->toArray(),
            'name' => 'volumes',
        ]);
    }

    /**
     * Returns powers list.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function powers(Request $request)
    {
        return $this->successJsonAnswer([
            'options' => PowerTranslates::whereHas('row', function (Builder $builder) use ($request) {
                    $builder->where('volume_id', (int)$request->input('volumeId'));
                })->whereLanguage('ru')->oldest('name')->get(['name', 'row_id'])->map(function (PowerTranslates $power) {
                    return ['id' => $power->row_id, 'name' => $power->name];
                })->toArray(),
            'name' => 'powers',
        ]);
    }

    /**
     * Returns powers list.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function engineNumbers(Request $request)
    {
        return $this->successJsonAnswer([
            'options' => EngineNumber::wherePowerId((int)$request->input('powerId'))
                ->oldest('engine_number')->get(['engine_number'])->map(function (EngineNumber $number) {
                    return ['id' => $number->engine_number, 'name' => $number->engine_number];
                })
                ->unique()
                ->toArray(),
            'name' => 'engineNumbers',
        ]);
    }

    /**
     * Searches through the engines numbers list.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function engineNumbersByName(Request $request)
    {
        return $this->successJsonAnswer([
            'items' => EngineNumber::whereDoesntHave('groups', function (Builder $builder) use ($request) {
                    $builder->where('group_id', $request->query('groupId'));
                })
                ->where('engine_number', 'like', $request->input('query') . '%')
                ->distinct()
                ->get('engine_number')
                ->map(function (EngineNumber $number) {
                    return [
                        'id' => $number->engine_number,
                        'markup' => $number->engine_number,
                        'selection' => $number->engine_number,
                    ];
                })
                ->toArray(),
        ]);
    }
}
