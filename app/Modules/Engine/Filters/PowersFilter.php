<?php

namespace App\Modules\Engine\Filters;

use App\Modules\Engine\Models\Model;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use EloquentFilter\ModelFilter;
use App\Exceptions\WrongParametersException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class PowersFilter
 * @package App\Modules\Engine\Filters
 */
class PowersFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @param Request $request
     * @return string
     * @throws WrongParametersException
     */
    static function showFilter(Request $request)
    {
        $form = new Form();
        $form->fieldSetForFilter()->add(
            Input::create('name')->setValue($request->input('name'))->addClassesToDiv('col-md-2'),
            Select::create('vendor')
                ->add(Vendor::oldest('name')->pluck('name', 'id')->toArray())
                ->setValue($request->input('vendor'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.vendor_id'))
                ->setPlaceholder(''),
            Select::create('model')
                ->add(
                    (int)$request->input('vendor')
                        ? Model::whereVendorId($request->input('vendor'))->oldest('name')
                            ->pluck('name', 'id')->toArray()
                        : []
                )
                ->setValue($request->input('model'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.model_id'))
                ->setPlaceholder(''),
            Select::create('volume')
                ->add(
                    ((int)$request->input('vendor') && (int)$request->input('model'))
                        ? Volume::whereVendorId($request->input('vendor'))
                            ->whereModelId($request->input('model'))->oldest('name')
                            ->pluck('name', 'id')->toArray()
                        : []
                )
                ->setValue($request->input('volume'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.volume_id'))
                ->setPlaceholder('')
        );

        return $form->renderAsFilter();
    }

    /**
     * Filter by $name
     *
     * @param string $name
     * @return PowersFilter
     */
    public function name(string $name)
    {
        return $this->whereHas('data', function (Builder $builder) use ($name) {
            $builder->where('name', 'LIKE', "%$name%");
        });
    }

    /**
     * Filter by vendor id.
     *
     * @param int $vendor
     * @return PowersFilter
     */
    public function vendor(int $vendor)
    {
        return $this->where('vendor_id', $vendor);
    }

    /**
     * Filter by model id.
     *
     * @param int $model
     * @return PowersFilter
     */
    public function model(int $model)
    {
        return $this->where('model_id', $model);
    }

    /**
     * Filter by volume id.
     *
     * @param int $volume
     * @return PowersFilter
     */
    public function volume(int $volume)
    {
        return $this->where('volume_id', $volume);
    }
}
