<?php

namespace App\Modules\Engine\Filters;

use CustomForm\Builder\Form;
use CustomForm\Input;
use EloquentFilter\ModelFilter;
use App\Exceptions\WrongParametersException;
use Illuminate\Http\Request;

/**
 * Class VendorsFilter
 * @package App\Modules\Engine\Filters
 */
class VendorsFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @param Request $request
     * @return string
     * @throws WrongParametersException
     */
    static function showFilter(Request $request)
    {
        $form = new Form();
        $form->fieldSetForFilter()->add(
            Input::create('name')->setValue($request->input('name'))->addClassesToDiv('col-md-2')
        );

        return $form->renderAsFilter();
    }

    /**
     * Filter by $name
     *
     * @param string $name
     * @return VendorsFilter
     */
    public function name(string $name)
    {
        return $this->where('name', 'LIKE', "%$name%");
    }
}
