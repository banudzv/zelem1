<?php

namespace App\Modules\Engine\Filters;

use App\Modules\Engine\Models\Model;
use App\Modules\Engine\Models\Vendor;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use EloquentFilter\ModelFilter;
use App\Exceptions\WrongParametersException;
use Illuminate\Http\Request;

/**
 * Class VolumesFilter
 * @package App\Modules\Engine\Filters
 */
class VolumesFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @param Request $request
     * @return string
     * @throws WrongParametersException
     */
    static function showFilter(Request $request)
    {
        $form = new Form();
        $form->fieldSetForFilter()->add(
            Input::create('name')->setValue($request->input('name'))->addClassesToDiv('col-md-2'),
            Select::create('vendor')
                ->add(Vendor::oldest('name')->pluck('name', 'id')->toArray())
                ->setValue($request->input('vendor'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.vendor_id'))
                ->setPlaceholder(''),
            Select::create('model')
                ->add(
                    (int)$request->input('vendor')
                        ? Model::whereVendorId($request->input('vendor'))->oldest('name')
                            ->pluck('name', 'id')->toArray()
                        : []
                )
                ->setValue($request->input('model'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.model_id'))
                ->setPlaceholder('')
        );

        return $form->renderAsFilter();
    }

    /**
     * Filter by $name
     *
     * @param string $name
     * @return VolumesFilter
     */
    public function name(string $name)
    {
        return $this->where('name', 'LIKE', "%$name%");
    }

    /**
     * Filter by $vendorId
     *
     * @param int $vendor
     * @return VolumesFilter
     */
    public function vendor(int $vendor)
    {
        return $this->where('vendor_id', $vendor);
    }

    /**
     * Filter by $vendorId
     *
     * @param int $model
     * @return VolumesFilter
     */
    public function model(int $model)
    {
        return $this->where('model_id', $model);
    }
}
