<?php

namespace App\Modules\Engine\Filters;

use App\Modules\Engine\Models\Model;
use App\Modules\Engine\Models\Power;
use App\Modules\Engine\Models\PowerTranslates;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use EloquentFilter\ModelFilter;
use App\Exceptions\WrongParametersException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class EngineNumbersFilter
 * @package App\Modules\Engine\Filters
 */
class EngineNumbersFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @param Request $request
     * @return string
     * @throws WrongParametersException
     */
    static function showFilter(Request $request)
    {
        $form = new Form();
        $form->fieldSetForFilter()->add(
            Input::create('number')->setValue($request->input('number'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.engine_number')),
            Select::create('vendor')
                ->add(Vendor::oldest('name')->pluck('name', 'id')->toArray())
                ->setValue($request->input('vendor'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.vendor_id'))
                ->setPlaceholder(''),
            Select::create('model')
                ->add(
                    (int)$request->input('vendor')
                        ? Model::whereVendorId($request->input('vendor'))->oldest('name')
                            ->pluck('name', 'id')->toArray()
                        : []
                )
                ->setValue($request->input('model'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.model_id'))
                ->setPlaceholder(''),
            Select::create('volume')
                ->add(
                    ((int)$request->input('vendor') && (int)$request->input('model'))
                        ? Volume::whereVendorId($request->input('vendor'))
                        ->whereModelId($request->input('model'))->oldest('name')
                        ->pluck('name', 'id')->toArray()
                        : []
                )
                ->setValue($request->input('volume'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.volume_id'))
                ->setPlaceholder(''),
            Select::create('power')
                ->add(
                    ((int)$request->input('vendor') && (int)$request->input('model') && (int)$request->input('volume'))
                        ? PowerTranslates::whereHas('row', function (Builder $builder) use ($request) {
                                $builder
                                    ->where('vendor_id', $request->input('vendor'))
                                    ->where('model_id', $request->input('model'))
                                    ->where('volume_id', $request->input('volume'));
                            })->oldest('name')->pluck('name', 'row_id')->toArray()
                        : []
                )
                ->setValue($request->input('power'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.power_id'))
                ->setPlaceholder('')
        );

        return $form->renderAsFilter();
    }

    /**
     * Filter by number
     *
     * @param string $number
     * @return EngineNumbersFilter
     */
    public function number(string $number)
    {
        return $this->where('engine_number', 'LIKE', "%$number%");
    }

    /**
     * Filter by vendor id.
     *
     * @param int $vendor
     * @return EngineNumbersFilter
     */
    public function vendor(int $vendor)
    {
        return $this->where('vendor_id', $vendor);
    }

    /**
     * Filter by model id.
     *
     * @param int $model
     * @return EngineNumbersFilter
     */
    public function model(int $model)
    {
        return $this->where('model_id', $model);
    }

    /**
     * Filter by volume id.
     *
     * @param int $volume
     * @return EngineNumbersFilter
     */
    public function volume(int $volume)
    {
        return $this->where('volume_id', $volume);
    }

    /**
     * Filter by power id.
     *
     * @param int $power
     * @return EngineNumbersFilter
     */
    public function power(int $power)
    {
        return $this->where('power_id', $power);
    }
}
