<?php

namespace App\Modules\Engine\Filters;

use App\Modules\Engine\Models\Vendor;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use EloquentFilter\ModelFilter;
use App\Exceptions\WrongParametersException;
use Illuminate\Http\Request;

/**
 * Class ModelsFilter
 * @package App\Modules\Engine\Filters
 */
class ModelsFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @param Request $request
     * @return string
     * @throws WrongParametersException
     */
    static function showFilter(Request $request)
    {
        $form = new Form();
        $form->fieldSetForFilter()->add(
            Input::create('name')->setValue($request->input('name'))->addClassesToDiv('col-md-2'),
            Select::create('vendor')
                ->add(Vendor::oldest('name')->pluck('name', 'id')->toArray())
                ->setValue($request->input('vendor'))->addClassesToDiv('col-md-2')
                ->setLabel(trans('engine::field.vendor_id'))
                ->setPlaceholder('')
        );

        return $form->renderAsFilter();
    }

    /**
     * Filter by $name
     *
     * @param string $name
     * @return ModelsFilter
     */
    public function name(string $name)
    {
        return $this->where('name', 'LIKE', "%$name%");
    }

    /**
     * Filter by $vendorId
     *
     * @param int $vendor
     * @return ModelsFilter
     */
    public function vendor(int $vendor)
    {
        return $this->where('vendor_id', $vendor);
    }
}
