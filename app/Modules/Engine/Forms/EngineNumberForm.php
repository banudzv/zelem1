<?php

namespace App\Modules\Engine\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Engine\Models\PowerTranslates;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Engine\Models\Model as EngineModel;

/**
 * Class EngineNumberForm
 * @package App\Modules\Engine\Forms
 */
class EngineNumberForm implements FormInterface
{
    /**
     * @param Model|EngineNumber|null $number
     * @return Form
     * @throws WrongParametersException|\Exception
     */
    public static function make(?Model $number = null): Form
    {
        $number = $number ?? new EngineNumber;
        $form = new Form();
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Input::create('engine_number', $number)
                ->setLabel(trans('engine::field.engine_number'))->required(),
            Select::create('vendor_id', $number)
                ->setLabel(trans('engine::field.vendor_id'))
                ->add(Vendor::oldest('name')->pluck('name', 'id')->toArray())
                ->setPlaceholder('')
                ->required(),
            Select::create('model_id', $number)
                ->add(
                    ($number->exists && $number->vendor_id)
                        ? EngineModel::whereVendorId($number->vendor_id)
                            ->oldest('name')->pluck('name', 'id')->toArray()
                        : []
                )
                ->setLabel(trans('engine::field.model_id'))
                ->required(),
            Select::create('volume_id', $number)
                ->add(
                    ($number->exists && $number->vendor_id && $number->model_id)
                        ? Volume::whereVendorId($number->vendor_id)
                            ->whereModelId($number->model_id)
                            ->oldest('name')->pluck('name', 'id')->toArray()
                        : []
                )
                ->setLabel(trans('engine::field.volume_id'))
                ->required(),
            Select::create('power_id', $number)
                ->add(
                    ($number->exists && $number->vendor_id && $number->model_id && $number->volume_id)
                        ? PowerTranslates::whereHas('row', function (Builder $builder) use ($number) {
                            $builder
                                ->where('vendor_id', $number->vendor_id)
                                ->where('model_id', $number->model_id)
                                ->where('volume_id', $number->volume_id);
                          })->oldest('name')->pluck('name', 'row_id')->toArray()
                        : []
                )
                ->setLabel(trans('engine::field.power_id'))
                ->required()
        );

        return $form;
    }
}
