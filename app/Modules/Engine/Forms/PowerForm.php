<?php

namespace App\Modules\Engine\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Models\Power;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Engine\Models\Model as EngineModel;

/**
 * Class PowerForm
 * @package App\Modules\Engine\Forms
 */
class PowerForm implements FormInterface
{
    /**
     * @param Model|Power|null $power
     * @return Form
     * @throws WrongParametersException|\Exception
     */
    public static function make(?Model $power = null): Form
    {
        $power = $power ?? new Power;
        $form = new Form();
        $form->fieldSetForLang(12)->add(
            Input::create('name', $power)->required()
        );
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Select::create('vendor_id', $power)
                ->add(Vendor::oldest('name')->pluck('name', 'id')->toArray())
                ->setLabel(trans('engine::field.vendor_id'))
                ->setPlaceholder('')
                ->required(),
            Select::create('model_id', $power)
                ->add(
                    ($power->exists && $power->vendor_id)
                        ? EngineModel::whereVendorId($power->vendor_id)
                            ->oldest('name')->pluck('name', 'id')->toArray()
                        : []
                )
                ->setLabel(trans('engine::field.model_id'))
                ->required(),
            Select::create('volume_id', $power)
                ->add(
                    ($power->exists && $power->vendor_id && $power->model_id)
                        ? Volume::whereVendorId($power->vendor_id)
                            ->whereModelId($power->model_id)
                            ->oldest('name')->pluck('name', 'id')->toArray()
                        : []
                )
                ->setLabel(trans('engine::field.volume_id'))
                ->required()
        );

        return $form;
    }
}
