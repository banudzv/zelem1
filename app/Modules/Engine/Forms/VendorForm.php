<?php

namespace App\Modules\Engine\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Models\Vendor;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Input;
use Illuminate\Database\Eloquent\Model;

/**
 * Class VendorForm
 * @package App\Modules\Engine\Forms
 */
class VendorForm implements FormInterface
{
    /**
     * @param Model|Vendor|null $vendor
     * @return Form
     * @throws WrongParametersException|\Exception
     */
    public static function make(?Model $vendor = null): Form
    {
        $vendor = $vendor ?? new Vendor;
        $form = new Form();
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Input::create('name', $vendor)->required()
        );

        return $form;
    }
}
