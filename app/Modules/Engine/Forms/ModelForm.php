<?php

namespace App\Modules\Engine\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Models\Vendor;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Engine\Models\Model as EngineModel;

/**
 * Class ModelForm
 * @package App\Modules\Engine\Forms
 */
class ModelForm implements FormInterface
{
    /**
     * @param Model|EngineModel|null $model
     * @return Form
     * @throws WrongParametersException|\Exception
     */
    public static function make(?Model $model = null): Form
    {
        $model = $model ?? new EngineModel;
        $form = new Form();
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Input::create('name', $model)->required(),
            Select::create('vendor_id', $model)
                ->add(Vendor::oldest('name')->pluck('name', 'id')->toArray())
                ->setPlaceholder('')
                ->setLabel(trans('engine::field.vendor_id'))
                ->required()
        );

        return $form;
    }
}
