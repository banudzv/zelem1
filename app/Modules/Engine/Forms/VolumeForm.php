<?php

namespace App\Modules\Engine\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Engine\Models\Model as EngineModel;

/**
 * Class VolumeForm
 * @package App\Modules\Engine\Forms
 */
class VolumeForm implements FormInterface
{
    /**
     * @param Model|Volume|null $volume
     * @return Form
     * @throws WrongParametersException|\Exception
     */
    public static function make(?Model $volume = null): Form
    {
        $volume = $volume ?? new Volume;
        $form = new Form();
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Input::create('name', $volume)->required(),
            Select::create('vendor_id', $volume)
                ->add(Vendor::oldest('name')->pluck('name', 'id')->toArray())
                ->setPlaceholder('')->setLabel(trans('engine::field.vendor_id'))
                ->setPlaceholder('')
                ->required(),
            Select::create('model_id', $volume)
                ->add(
                    ($volume->exists && $volume->vendor_id)
                        ? EngineModel::whereVendorId($volume->vendor_id)
                            ->oldest('name')->pluck('name', 'id')->toArray()
                        : []
                )
                ->setLabel(trans('engine::field.model_id'))
                ->required()
        );

        return $form;
    }
}
