<?php

Route::post('api/car/models', ['uses' => 'AjaxController@models', 'as' => 'api.ajax.models']);
Route::post('api/car/volumes', ['uses' => 'AjaxController@volumes', 'as' => 'api.ajax.volumes']);
Route::post('api/car/powers', ['uses' => 'AjaxController@powers', 'as' => 'api.ajax.powers']);
Route::post('api/car/engine-numbers-by-name', ['uses' => 'AjaxController@engineNumbersByName', 'as' => 'api.ajax.engine-numbers-by-name']);
Route::post('api/car/engine-numbers', ['uses' => 'AjaxController@engineNumbers', 'as' => 'api.ajax.engine-numbers']);
