<?php

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:engine'])->group(function () {
    Route::get('car/vendors/{vendor}/destroy', [
        'uses' => 'VendorsController@destroy', 'as' => 'admin.vendors.destroy'
    ]);
    Route::resource('car/vendors', 'VendorsController')
        ->except('show', 'destroy')->names('admin.vendors');

    Route::get('car/models/{model}/destroy', [
        'uses' => 'ModelsController@destroy', 'as' => 'admin.models.destroy'
    ]);
    Route::resource('car/models', 'ModelsController')
        ->except('show', 'destroy')->names('admin.models');

    Route::get('car/volumes/{volume}/destroy', [
        'uses' => 'VolumesController@destroy', 'as' => 'admin.volumes.destroy'
    ]);
    Route::resource('car/volumes', 'VolumesController')
        ->except('show', 'destroy')->names('admin.volumes');

    Route::get('car/powers/{power}/destroy', [
        'uses' => 'PowersController@destroy', 'as' => 'admin.powers.destroy'
    ]);
    Route::resource('car/powers', 'PowersController')
        ->except('show', 'destroy')->names('admin.powers');

    Route::get('car/numbers/{number}/destroy', [
        'uses' => 'EngineNumbersController@destroy', 'as' => 'admin.numbers.destroy'
    ]);
    Route::resource('car/numbers', 'EngineNumbersController')
        ->except('show', 'destroy')->names('admin.numbers');
});

// Routes for unauthenticated administrators


