<?php

namespace App\Modules\Engine\Models;

use App\Modules\Engine\Filters\EngineNumbersFilter;
use App\Modules\Products\Models\ProductEngineNumber;
use App\Modules\Products\Models\ProductGroup;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use App\Modules\Engine\Models\Model as EngineModel;

/**
 * Class EngineNumber
 *
 * @package App\Modules\Engine\Models
 * @property int $id
 * @property int $vendor_id
 * @property int $model_id
 * @property int $volume_id
 * @property int $power_id
 * @property string $engine_number
 * @property-read Vendor $vendor
 * @property-read EngineModel $model
 * @property-read Volume $volume
 * @property-read Power $power
 * @property-read Collection|ProductGroup[] $groups
 * @method static Builder|EngineNumber newModelQuery()
 * @method static Builder|EngineNumber newQuery()
 * @method static Builder|EngineNumber query()
 * @method static Builder|EngineNumber whereId($value)
 * @method static Builder|EngineNumber whereVendorId($value)
 * @method static Builder|EngineNumber whereModelId($value)
 * @method static Builder|EngineNumber whereVolumeId($value)
 * @method static Builder|EngineNumber wherePowerId($value)
 * @method static Builder|EngineNumber whereEngineNumber($value)
 * @mixin \Eloquent
 */
class EngineNumber extends Model
{
    use Filterable;

    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'car_engine_numbers';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['vendor_id', 'model_id', 'volume_id', 'power_id', 'engine_number'];

    /**
     * Provides filtration.
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(EngineNumbersFilter::class);
    }

    /**
     * Relation to the vendor of the car.
     *
     * @return BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'id');
    }

    /**
     * Relation to the model of the car.
     *
     * @return BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(EngineModel::class, 'model_id', 'id');
    }

    /**
     * Relation to the volume of the car engine.
     *
     * @return BelongsTo
     */
    public function volume()
    {
        return $this->belongsTo(Volume::class, 'volume_id', 'id');
    }

    /**
     * Relation to the power of the car engine.
     *
     * @return BelongsTo
     */
    public function power()
    {
        return $this->belongsTo(Power::class, 'power_id', 'id');
    }

    /**
     * Relation to the product groups.
     * TODO Check this relation correctness
     *
     * @return HasManyThrough
     */
    public function groups()
    {
        return $this->hasManyThrough(
            ProductGroup::class, ProductEngineNumber::class,
            'engine_number', 'id', 'engine_number', 'group_id'
        );
    }
}
