<?php

namespace App\Modules\Engine\Models;

use App\Modules\Engine\Filters\VolumesFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Builder;
use App\Modules\Engine\Models\Model as EngineModel;

/**
 * Class Volume
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $vendor_id
 * @property int $model_id
 * @property string $name
 * @property-read Vendor $vendor
 * @property-read EngineModel $model
 * @method static Builder|Volume newModelQuery()
 * @method static Builder|Volume newQuery()
 * @method static Builder|Volume query()
 * @method static Builder|Volume whereCreatedAt($value)
 * @method static Builder|Volume whereId($value)
 * @method static Builder|Volume whereUpdatedAt($value)
 * @method static Builder|Volume whereVendorId($value)
 * @method static Builder|Volume whereModelId($value)
 * @method static Builder|Volume whereName($value)
 * @mixin \Eloquent
 */
class Volume extends Model
{
    use Filterable;

    /**
     * {@inheritDoc}
     */
    protected $table = 'car_volumes';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['vendor_id', 'model_id', 'name'];

    /**
     * Provides filtering.
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(VolumesFilter::class);
    }

    /**
     * Relation to the vendor of the car.
     *
     * @return BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'id');
    }

    /**
     * Relation to the model of the car.
     *
     * @return BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(EngineModel::class, 'model_id', 'id');
    }
}
