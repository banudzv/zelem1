<?php

namespace App\Modules\Engine\Models;

use App\Modules\Products\Models\ProductGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Year
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $year
 * @property int $group_id
 * @property-read ProductGroup $group
 * @method static Builder|Year newModelQuery()
 * @method static Builder|Year newQuery()
 * @method static Builder|Year query()
 * @method static Builder|Year whereCreatedAt($value)
 * @method static Builder|Year whereId($value)
 * @method static Builder|Year whereGroupId($value)
 * @method static Builder|Year whereUpdatedAt($value)
 * @method static Builder|Year whereYear($value)
 * @mixin \Eloquent
 */
class Year extends Model
{
    /**
     * {@inheritDoc}
     */
    protected $table = 'car_years';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['year', 'group_id'];

    /**
     * Relation to the product.
     *
     * @return BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(ProductGroup::class, 'group_id', 'id');
    }
}
