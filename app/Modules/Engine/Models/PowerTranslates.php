<?php

namespace App\Modules\Engine\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use App\Core\Modules\Languages\Models\Language;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class PowerTranslates
 *
 * @package App\Modules\Engine\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $name
 * @property int $row_id
 * @property string $language
 * @property-read Language $lang
 * @property-read Power $row
 * @method static Builder|PowerTranslates newModelQuery()
 * @method static Builder|PowerTranslates newQuery()
 * @method static Builder|PowerTranslates query()
 * @method static Builder|PowerTranslates whereCreatedAt($value)
 * @method static Builder|PowerTranslates whereId($value)
 * @method static Builder|PowerTranslates whereLanguage($value)
 * @method static Builder|PowerTranslates whereName($value)
 * @method static Builder|PowerTranslates whereRowId($value)
 * @method static Builder|PowerTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PowerTranslates extends Model
{
    use ModelTranslates;

    /**
     * {@inheritDoc}
     */
    protected $table = 'car_powers_translates';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['row_id', 'language', 'name'];
}
