<?php

namespace App\Modules\Engine\Models;

use App\Modules\Engine\Filters\VendorsFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Vendor
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $name
 * @property-read Collection|Model[] $models
 * @property-read Collection|Volume[] $volumes
 * @property-read int|null $models_count
 * @property-read int|null $volumes_count
 * @method static Builder|Vendor newModelQuery()
 * @method static Builder|Vendor newQuery()
 * @method static Builder|Vendor query()
 * @method static Builder|Vendor filter(array $attributes = [])
 * @method static Builder|Vendor whereCreatedAt($value)
 * @method static Builder|Vendor whereId($value)
 * @method static Builder|Vendor whereName($value)
 * @method static Builder|Vendor whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Vendor extends Model
{
    use Filterable;

    /**
     * {@inheritDoc}
     */
    protected $table = 'car_vendors';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['name'];

    /**
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(VendorsFilter::class);
    }

    /**
     * Relation to the car models linked to the vendor.
     *
     * @return HasMany
     */
    public function models()
    {
        return $this->hasMany(Model::class, 'vendor_id', 'id');
    }

    /**
     * Relation to the car volumes linked to the vendor and model.
     *
     * @return HasMany
     */
    public function volumes()
    {
        return $this->hasMany(Volume::class, 'vendor_id', 'id');
    }
}
