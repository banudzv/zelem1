<?php

namespace App\Modules\Engine\Models;

use App\Modules\Engine\Filters\PowersFilter;
use App\Traits\ModelMain;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use App\Modules\Engine\Models\Model as EngineModel;

/**
 * Class Power
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $vendor_id
 * @property int $model_id
 * @property int $volume_id
 * @property-read PowerTranslates $current
 * @property-read Collection|PowerTranslates[] $data
 * @property-read int|null $data_count
 * @property-read Vendor $vendor
 * @property-read EngineModel $model
 * @property-read Volume $volume
 * @method static Builder|Power newModelQuery()
 * @method static Builder|Power newQuery()
 * @method static Builder|Power query()
 * @method static Builder|Power whereCreatedAt($value)
 * @method static Builder|Power whereId($value)
 * @method static Builder|Power whereUpdatedAt($value)
 * @method static Builder|Power whereVendorId($value)
 * @method static Builder|Power whereModelId($value)
 * @method static Builder|Power whereVolumeId($value)
 * @mixin \Eloquent
 */
class Power extends Model
{
    use ModelMain, Filterable;

    /**
     * {@inheritDoc}
     */
    protected $table = 'car_powers';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['vendor_id', 'model_id', 'volume_id'];

    /**
     * Provides filtering.
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(PowersFilter::class);
    }

    /**
     * Relation to the vendor of the car.
     *
     * @return BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'id');
    }

    /**
     * Relation to the model of the car.
     *
     * @return BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(EngineModel::class, 'model_id', 'id');
    }

    /**
     * Relation to the volume of the car engine.
     *
     * @return BelongsTo
     */
    public function volume()
    {
        return $this->belongsTo(Volume::class, 'volume_id', 'id');
    }
}
