<?php

namespace App\Modules\Engine\Models;

use App\Modules\Engine\Filters\ModelsFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Model
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $vendor_id
 * @property string $name
 * @property-read Vendor $vendor
 * @property-read Collection|Volume[] $volumes
 * @method static Builder|Model newModelQuery()
 * @method static Builder|Model newQuery()
 * @method static Builder|Model query()
 * @method static Builder|Model whereCreatedAt($value)
 * @method static Builder|Model whereId($value)
 * @method static Builder|Model whereUpdatedAt($value)
 * @method static Builder|Model whereVendorId($value)
 * @method static Builder|Model whereName($value)
 * @mixin \Eloquent
 */
class Model extends EloquentModel
{
    use Filterable;

    /**
     * {@inheritDoc}
     */
    protected $table = 'car_models';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['vendor_id', 'name'];

    /**
     * Provides filtering by model.
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(ModelsFilter::class);
    }

    /**
     * Relation to the vendor of the car.
     *
     * @return BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'id');
    }

    /**
     * Relation to the car volumes linked to the vendor and model.
     *
     * @return HasMany
     */
    public function volumes()
    {
        return $this->hasMany(Volume::class, 'model_id', 'id');
    }

    public function getFullName(): string
    {
        return "{$this->vendor->name} {$this->name}";
    }
}
