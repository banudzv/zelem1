<?php

namespace App\Modules\Engine\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\Engine\Models\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ModelRequest
 * @package App\Modules\Engine\Requests
 */
class ModelRequest extends FormRequest implements RequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        /** @var Model|null $model */
        $model = $this->route('model');

        return [
            'name' => ['required', 'max:191', Rule::unique('car_models', 'name')->ignore($model)],
            'vendor_id' => ['required', Rule::exists('car_vendors', 'id')],
        ];
    }
}
