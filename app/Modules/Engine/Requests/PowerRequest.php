<?php

namespace App\Modules\Engine\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PowerRequest
 * @package App\Modules\Engine\Requests
 */
class PowerRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return $this->generateRules([
            'vendor_id' => ['required', Rule::exists('car_vendors', 'id')],
            'model_id' => ['required', Rule::exists('car_models', 'id')],
            'volume_id' => ['required', Rule::exists('car_volumes', 'id')],
        ], [
            'name' => ['required', 'max:191'],
        ]);
    }
}
