<?php

namespace App\Modules\Engine\Requests;

use App\Core\Interfaces\RequestInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class EngineNumberRequest
 * @package App\Modules\Engine\Requests
 */
class EngineNumberRequest extends FormRequest implements RequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'engine_number' => ['required', 'max:191'],
            'vendor_id' => ['required', Rule::exists('car_vendors', 'id')],
            'model_id' => ['required', Rule::exists('car_models', 'id')],
            'volume_id' => ['required', Rule::exists('car_volumes', 'id')],
            'power_id' => ['required', Rule::exists('car_powers', 'id')],
        ];
    }
}
