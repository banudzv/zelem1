<?php

namespace App\Modules\Engine\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\Engine\Models\Volume;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class VolumeRequest
 * @package App\Modules\Engine\Requests
 */
class VolumeRequest extends FormRequest implements RequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        /** @var Volume|null $volume */
        $volume = $this->route('volume');

        return [
            'name' => ['required', 'max:191', Rule::unique('car_volumes', 'name')->ignore($volume)],
            'vendor_id' => ['required', Rule::exists('car_vendors', 'id')],
            'model_id' => ['required', Rule::exists('car_models', 'id')],
        ];
    }
}
