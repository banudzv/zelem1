<?php

namespace App\Modules\System\Tests\Unit;

use App\Modules\System\Models\Process;
use Cache;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Psr\SimpleCache\InvalidArgumentException;
use Tests\TestCase;

class ProcessTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @throws InvalidArgumentException
     */
    public function test_it_has_progress_in_cache()
    {
        $process = new Process();
        $process->id = 1;

        $this->assertEquals(null, Cache::get($process->generateCacheKey()));

        $process->setProgressInCache(50);

        $this->assertEquals(50, Cache::get($process->generateCacheKey()));

        $process->setProgressInCache(100);

        $this->assertEquals(100, Cache::get($process->generateCacheKey()));

    }
}