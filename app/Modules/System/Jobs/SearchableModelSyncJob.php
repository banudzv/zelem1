<?php

namespace App\Modules\System\Jobs;

use App\Modules\Search\Commands\ElasticIndexUpdateCommand;

class SearchableModelSyncJob extends AbstractProcessJob
{

    public static function type(): string
    {
        return 'searchable_model_sync';
    }

    protected function command(): string
    {
        return ElasticIndexUpdateCommand::class;
    }
}