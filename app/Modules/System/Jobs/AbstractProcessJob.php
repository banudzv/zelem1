<?php

namespace App\Modules\System\Jobs;

use App\Modules\System\Models\Process;
use Artisan;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

abstract class AbstractProcessJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Process
     */
    protected $process;

    public function __construct(array $parameters = [])
    {
        $this->process = Process::query()->create(
            [
                'parameters' => $parameters,
                'type' => static::type(),
            ]
        );

        $this->process->setStatusCreated();
    }

    abstract public static function type(): string;

    public function handle()
    {
        $output = Artisan::call($this->command(), $this->process->parameters());

        $this->process->refresh();

        $this->process->addLog('Output code: ' . $output);
    }

    abstract protected function command(): string;
}
