@php
    use App\Modules\System\Models\Process;
    /** @var Process[] $processes */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! \App\Modules\System\Filters\ProcessFilter::showFilter() !!}
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>{{ __('system::process.attributes.type') }}</th>
                        <th>{{ __('system::process.attributes.progress') }}</th>
                        <th>{{ __('system::process.attributes.status') }}</th>
                        <th>{{ __('system::process.attributes.start') }}</th>
                        <th>{{ __('system::process.attributes.finish') }}</th>
                        <th></th>
                    </tr>
                    @foreach($processes AS $process)
                        <tr>
                            <td title="{{ $process->parametersToString() }}">
                                @lang('system::process.' . $process->type)
                            </td>
                            <td class="col-md-3">
                                @if($process->getProgress())
                                    @include('admin.components.progress-bar', [
                                        'value' => $process->getProgress(),
                                        'color' => $process->isFailed() ? 'danger' : 'success'
                                    ])
                                @endif
                            </td>
                            <td class="col-md-1">
                                @if($process->status)
                                    <span class="label label-{{ Process::getLabel($process->status) }}"
                                          title="{{ $process->body }}">
                                        @lang('system::process.status.' . $process->status)
                                    </span>
                                @endif
                            </td>
                            <td class="col-md-1">
                                {{ $process->created_at }}
                            </td>
                            <td class="col-md-1">
                                @if($process->isFinished())
                                    {{ $process->updated_at }}
                                @endif
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $processes->appends(request()->except('page'))->links() }}</div>
    </div>
@stop