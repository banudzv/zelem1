<?php

namespace App\Modules\System\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Macro\MultiSelect;
use Illuminate\Database\Eloquent\Model;

class CreateProcessForm implements FormInterface
{
    /**
     * @param Model|null $baseModel
     * @return Form
     * @throws WrongParametersException
     */
    public static function make(?Model $baseModel = null): Form
    {
        $form = Form::create();

        $types = [];
        foreach(config('system.process_types', []) as $type => $class) {
            $types[$type] = trans('system::process.' . $type);
        }

        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)
            ->add(
                MultiSelect::create('process[]')
                    ->addClassesToDiv('col-md-12')
                    ->add($types)
                    ->setLabel('system::general.attributes.processes')
            );

        return $form->ajax();
    }
}