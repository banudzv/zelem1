<?php

namespace App\Modules\System;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use CustomForm\Input;
use CustomMenu;
use CustomRoles;
use CustomSettings;

class Provider extends BaseProvider
{
    protected function presets()
    {
    }

    /**
     * @throws WrongParametersException
     */
    public function loadForAdmin()
    {
        CustomMenu::get()->group()
            ->block('system', 'system::general.menu-block', 'fa fa-wrench')
            ->setPosition(11)
            ->link(
                'system::process.menu',
                RouteObjectValue::make('admin.process.index')
            );

        // Register module configurable settings
        $settings = CustomSettings::createAndGet('system', 'system::general.settings-name');
        $settings->add(
            Input::create('process-per-page')
                ->setLabel('system::settings.attributes.process-per-page'),
            ['required', 'integer', 'min:1']
        );

        // Register role scopes
        CustomRoles::add('system', 'system::general.permission-name')
            ->enableAll()->except(RoleRule::UPDATE);
    }
}
