<?php


namespace App\Modules\System\Controllers\Admin;


use App\Core\AdminController;
use App\Core\AjaxTrait;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\System\Forms\CreateProcessForm;
use App\Modules\System\Models\Process;
use App\Modules\System\Requests\CreateProcessRequest;
use App\Modules\System\Services\ProcessService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Seo;
use Throwable;

class SystemController extends AdminController
{
    use AjaxTrait;

    /**
     * @var ProcessService
     */
    private $service;

    public function __construct(ProcessService $service)
    {
        $this->service = $service;
    }

    /**
     * @return Factory|View
     * @throws Exception
     */
    public function index()
    {
        $this->addBaseBreadcrumbs();
        Seo::meta()->setH1('system::seo.process.index');
        $this->registerButtons();

        $processes = Process::filter(request()->only('type', 'status', 'created_at'))
            ->latest()
            ->paginate(config('db.system.process-per-page', 50));

        return view(
            'system::admin.process.index',
            [
                'processes' => $processes
            ]
        );
    }

    private function addBaseBreadcrumbs()
    {
        Seo::breadcrumbs()->add(
            'system::seo.process.index',
            RouteObjectValue::make('admin.process.index')
        );
    }

    /**
     * @throws Exception
     */
    private function registerButtons()
    {
        $this->addCustomButton('admin.process.create', [], __('system::process.add'), 'bg-olive ajax-request');
    }

    /**
     * @return JsonResponse
     * @throws Throwable
     */
    public function create()
    {
        return $this->successMfpMessage(
            view(
                'system::admin.process.popup',
                [
                    'title' => __('system::form-create.title'),
                    'form' => CreateProcessForm::make(),
                    'formId' => uniqid('create-process-form'),
                    'method' => 'POST',
                    'url' => route('admin.process.store'),
                ]
            )->render()
        );
    }

    /**
     * @param CreateProcessRequest $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function store(CreateProcessRequest $request)
    {
        $processes = $request->input('process');

        foreach ($processes as $type) {
            $this->service->run($type);
        }

        return redirect()->back();
    }
}