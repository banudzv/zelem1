<?php

namespace App\Modules\System\Services;

use App\Modules\System\Jobs\AbstractProcessJob;
use Exception;

class ProcessService
{
    /**
     * @param string $type
     * @param array $parameters
     * @throws Exception
     */
    public function run(string $type, array $parameters = [])
    {
        /** @var AbstractProcessJob $jobClass */
        $jobClass = $this->getJobByType($type);
        $jobClass::dispatch($parameters);
    }

    /**
     * @param string $type
     * @return mixed
     * @throws Exception
     */
    protected function getJobByType(string $type)
    {
        $types = config('system.process_types', []);

        if (isset($types[$type])) {
            return $types[$type];
        }

        throw new Exception('Type not found');
    }
}