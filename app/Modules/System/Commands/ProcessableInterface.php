<?php

namespace App\Modules\System\Commands;

interface ProcessableInterface
{
    public function getProcessType(): string;
}