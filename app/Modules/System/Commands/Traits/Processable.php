<?php

namespace App\Modules\System\Commands\Traits;

use App\Modules\System\Commands\ProcessableInterface;
use App\Modules\System\Models\Process;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\Console\Helper\ProgressBar;

trait Processable
{
    /**
     * @var Process
     */
    protected $process;

    /**
     * @var ProgressBar
     */
    protected $progressBar;

    protected function setBody(string $body)
    {
        $this->process->setBody($body);
    }

    protected function startProgress(int $count)
    {
        $this->setProcess();
        $this->progressProcess($count);

        $this->progressBar = $this->output->createProgressBar($count);
        $this->progressBar->start();
    }

    protected function setProcess()
    {
        if (!$this->checkProcessable()) {
            return;
        }

        $processId = $this->argument('process');

        if ($processId) {
            $process = Process::query()->find($processId);
            $this->process = $process;
        } else {
            $this->process = Process::query()->create(
                [
                    'type' => $this->getProcessType(),
                    'parameters' => $this->options() + $this->arguments()
                ]
            );
            $this->process->setStatusCreated();
        }
    }

    protected function checkProcessable()
    {
        return $this instanceof ProcessableInterface &&
            property_exists($this, 'signature') &&
            strpos($this->signature, '{process?}');
    }

    protected function progressProcess(int $count)
    {
        $this->process->setStatusInProgress();
    }

    /**
     * @param int $count
     * @throws InvalidArgumentException
     */
    protected function addProgress(int $count)
    {
        $this->progressBar->advance($count);

        $this->setProgress(
            $this->progressBar->getProgressPercent()
        );
    }

    /**
     * @param float $progress
     * @throws InvalidArgumentException
     */
    protected function setProgress(float $progress)
    {
        $this->process->setProgress($progress);
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function finishProgress()
    {
        $this->progressBar->finish();

        $this->finishProcess();
    }

    /**
     * @param string|null $message
     * @throws InvalidArgumentException
     */
    protected function finishProcess(?string $message = null)
    {
        $this->setProgress(100);
        $this->process->setStatusFinished();

        if ($message) {
            $this->process->setBody($message);
        }
    }

    protected function failProgress(string $errorMessage)
    {
        $this->failProcess($errorMessage);

        $this->progressBar->clear();
    }

    protected function failProcess(?string $error = null)
    {
        $this->process->setStatusFailed();

        if ($error) {
            $this->process->setBody($error);
        }
    }

}