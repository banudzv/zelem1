<?php

Route::middleware(['auth:admin'])
    ->name('admin.process.')
    ->group(function() {

        Route::get('processes', [
            'as' => 'index',
            'uses' => 'SystemController@index'
        ]);

        Route::get('processes/create', [
            'as' => 'create',
            'uses' => 'SystemController@create'
        ]);

        Route::post('processes', [
            'as' => 'store',
            'uses' => 'SystemController@store'
        ]);
});