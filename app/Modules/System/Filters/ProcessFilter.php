<?php

namespace App\Modules\System\Filters;

use App\Modules\Articles\Models\ArticleCategory;
use App\Modules\System\Models\Process;
use Carbon\Carbon;
use CustomForm\Builder\Form;
use CustomForm\Macro\DateRangePicker;
use CustomForm\Select;
use CustomForm\Input;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Class ProcessFilter
 *
 * @package App\Core\Modules\System\Filters
 */
class ProcessFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @return string
     * @throws \App\Exceptions\WrongParametersException
     */
    static function showFilter()
    {
        $form = Form::create();
        $types = [];
        foreach(config('system.process_types', []) as $type => $class) {
            $types[$type] = trans('system::process.' . $type);
        }
        $statuses = [];
        foreach (Process::statuses() as $status) {
            $statuses[$status] = trans('system::process.status.' . $status);
        }
        $form->fieldSetForFilter()->add(
            Select::create('type')
                ->add($types)
                ->setValue(request('type'))
                ->addClassesToDiv('col-md-3')
                ->setPlaceholder(__('global.all'))
                ->setLabel('system::process.attributes.type'),
            Select::create('status')
                ->add($statuses)
                ->setValue(request('status'))
                ->addClassesToDiv('col-md-3')
                ->setPlaceholder(__('global.all'))
                ->setLabel('system::process.attributes.status'),
            DateRangePicker::create('created_at')
                ->setValue(request('created_at'))
                ->addClassesToDiv('col-md-2')
        );

        return $form->renderAsFilter();
    }
    
    /**
     * Filter by status
     *
     * @param  string $status
     * @return ProcessFilter
     */
    public function status(string $status)
    {
        return $this->where('status', $status);
    }

    /**
     * Filter by type
     *
     * @param  string $type
     * @return ProcessFilter
     */
    public function type(string $type)
    {
        return $this->where('type', $type);
    }

    /**
     * @param string $createdAt
     * @return $this
     */
    public function createdAt(string $createdAt)
    {
        $range = explode(' - ', $createdAt);
        $startDate = Carbon::parse($range[0])->startOfDay();
        $endDate = Carbon::parse($range[1])->endOfDay();
        return $this->where(
            function (Builder $query) use ($startDate, $endDate) {
                return $query->where('created_at', '>=', $startDate)
                    ->where('created_at', '<=', $endDate);
            }
        );
    }
}
