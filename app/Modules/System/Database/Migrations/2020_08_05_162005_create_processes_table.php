<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessesTable extends Migration
{

    public function up()
    {
        Schema::create(
            'processes',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('type');
                $table->string('status')->nullable();
                $table->float('progress')->unsigned()->nullable();
                $table->text('body')->nullable();
                $table->json('parameters')->nullable();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('processes');
    }
}
