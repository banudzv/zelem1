<?php

namespace App\Modules\System\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'system';

    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu-block',
                    'ru' => 'Система',
                ],
                [
                    'name' => 'process.menu',
                    'ru' => 'Задачи',
                ],
                [
                    'name' => 'general.permission-name',
                    'ru' => 'Система',
                ],
                [
                    'name' => 'general.settings-name',
                    'ru' => 'Система',
                ],
                [
                    'name' => 'settings.attributes.process-per-page',
                    'ru' => 'Процессов на странице',
                ],
                [
                    'name' => 'seo.process.index',
                    'ru' => 'Управление задачами',
                ],
                [
                    'name' => 'process.add',
                    'ru' => 'Добавить задачи',
                ],
                [
                    'name' => 'form-create.title',
                    'ru' => 'Выбор задач для запуска',
                ],
                [
                    'name' => 'general.attributes.processes',
                    'ru' => 'Выберите задачи',
                ],
                [
                    'name' => 'process.searchable_model_sync',
                    'ru' => 'Обновление индекса',
                ],
                [
                    'name' => 'process.attributes.type',
                    'ru' => 'Тип процесса',
                ],
                [
                    'name' => 'process.attributes.status',
                    'ru' => 'Состояние',
                ],
                [
                    'name' => 'process.attributes.progress',
                    'ru' => 'Прогресс',
                ],
                [
                    'name' => 'process.attributes.start',
                    'ru' => 'Стартовал',
                ],
                [
                    'name' => 'process.attributes.finish',
                    'ru' => 'Окончен',
                ],
                [
                    'name' => 'process.status.created',
                    'ru' => 'Создан',
                ],
                [
                    'name' => 'process.status.finished',
                    'ru' => 'Окончен',
                ],
                [
                    'name' => 'process.status.failed',
                    'ru' => 'Прерван (ошибка)',
                ],
                [
                    'name' => 'process.status.in_progress',
                    'ru' => 'В процессе',
                ],
            ],
            Translate::PLACE_SITE => []
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
