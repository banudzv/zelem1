<?php

namespace App\Modules\System\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;

class CreateProcessRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return $this->generateRules(
            [
                'process' => ['required', 'array'],
                'process.*' => ['string', $this->processesValidation()]
            ]
        );
    }

    /**
     * @return In
     */
    protected function processesValidation(): In
    {
        return Rule::in(array_keys(config('system.process_types', [])));
    }
}
