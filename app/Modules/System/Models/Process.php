<?php

namespace App\Modules\System\Models;

use App\Modules\System\Filters\ProcessFilter;
use App\Traits\Filterable;
use Cache;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * @property int id
 * @property string status
 * @property string type
 * @property string body
 * @property int progress
 * @property array parameters
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Process extends Model
{

    use Filterable;

    const STATUS_CREATED = 'created';
    const STATUS_FINISHED = 'finished';
    const STATUS_FAILED = 'failed';
    const STATUS_IN_PROGRESS = 'in_progress';

    protected $fillable = [
        'type',
        'body',
        'parameters',
    ];

    protected $casts = [
        'parameters' => 'array',
    ];

    /**
     * @param $state
     * @return string
     * @throws Exception
     */
    public static function getLabel($state)
    {
        if (!in_array($state, self::statuses())) {
            throw new Exception("Not found status {$state}");
        }

        switch ($state) {
            case self::STATUS_FINISHED:
                return 'success';
                break;
            case self::STATUS_FAILED:
                return 'danger';
                break;
            case self::STATUS_IN_PROGRESS:
                return 'info';
                break;
            default:
                return 'primary';
                break;
        }
    }

    public static function statuses(): array
    {
        return [
            self::STATUS_CREATED,
            self::STATUS_FINISHED,
            self::STATUS_FAILED,
            self::STATUS_IN_PROGRESS
        ];
    }

    public function setStatusCreated()
    {
        $this->status = self::STATUS_CREATED;
        $this->save();
    }

    public function setStatusInProgress()
    {
        $this->status = self::STATUS_IN_PROGRESS;
        $this->save();
    }

    public function setStatusFinished()
    {
        $this->status = self::STATUS_FINISHED;
        $this->save();
    }

    public function setStatusFailed()
    {
        $this->status = self::STATUS_FAILED;
        $this->save();
    }

    /**
     * @param float $progress
     * @throws InvalidArgumentException
     */
    public function setProgress(float $progress)
    {
        $progress = max($progress, 0);
        $progress = min($progress, 100);

        $this->progress = $progress;
        $this->save();

        $this->setProgressInCache($progress);
    }

    /**
     * @param $progress
     * @throws InvalidArgumentException
     */
    public function setProgressInCache($progress)
    {
        Cache::set($this->generateCacheKey(), $progress, 3600);
    }

    public function generateCacheKey(): string
    {
        return sprintf('process.with_id.' . $this->getKey());
    }

    public function getProgress(): int
    {
        if ($this->status == self::STATUS_IN_PROGRESS) {
            return (int)$this->getProgressFromCache();
        }

        return (int)$this->progress;
    }

    public function getProgressFromCache()
    {
        return Cache::get($this->generateCacheKey());
    }

    public function setBody(string $body)
    {
        $this->body = $body;
        $this->save();
    }

    public function isFailed()
    {
        return $this->status === self::STATUS_FAILED;
    }

    public function isFinished()
    {
        return $this->status === self::STATUS_FINISHED;
    }

    public function modelFilter(): string
    {
        return $this->provideFilter(ProcessFilter::class);
    }

    public function parameters(): array
    {
        return $this->parameters + ['process' => $this->id];
    }

    public function parametersToString(): string
    {
        return array_to_json($this->parameters ?? []);
    }

    public function addLog(string $log): void
    {
        $this->body .= PHP_EOL;
        $this->body .= Carbon::now() . ' ' . $log;
        $this->save();
    }
}