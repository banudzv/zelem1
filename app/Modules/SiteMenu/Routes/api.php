<?php

use Illuminate\Support\Facades\Route;

Route::get('site-menu/types', 'SiteMenuController@types');
Route::get('site-menu/all', 'SiteMenuController@all');
Route::get('site-menu/{type}', ['uses' => 'SiteMenuController@show', 'as' => 'api.site_menu.show']);
