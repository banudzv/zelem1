@php
/** @var \App\Components\SiteMenu\Group $menu */
@endphp
<ul class="nav-links{{ (isset($list_mod_classes) ? ' ' . $list_mod_classes : '') }}">
    @foreach($menu->getKids() as $link)
       @php
       @endphp
        <li class="nav-links__item">
            <a class="nav-links__link nav-links__link--footer"
               @if($link->isInternal() === false)
                       target="_blank"
               @endif
               href="{{ $link->getUrl() }}">{{ $link->name }}</a>
        </li>
    @endforeach
</ul>
