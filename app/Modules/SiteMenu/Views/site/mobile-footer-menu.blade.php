@php
/** @var \App\Components\SiteMenu\Group $menu */
@endphp
@include('site_menu::site.nav-links.nav-links', [
    'menu' => $menu,
])
