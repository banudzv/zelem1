@php
/** @var \App\Components\SiteMenu\Group $menu */
/** @var string $menuSlug */
@endphp
<div class="gcell _pl-def _lg-pl-lg">
    <div class="title title--size-normal _color-dark-blue">
        @lang('site_menu::site.menu-title.' . $menuSlug)
    </div>
    <div class="_mtb-def">
        @include('site_menu::site.nav-links.nav-links', [
            'menu' => $menu,
        ])
    </div>
</div>
