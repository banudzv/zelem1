@php
    /** @var \App\Components\SiteMenu\Group $menu */
@endphp
<div class="header-over js-init" data-product>
    <div class="header-over__left">
        {!! Widget::show('logo') !!}
        <div class="_lg-show">
            @component('site._widgets.elements.slogan.slogan')
                @slot('content'){!! config('db.basic.slogan_'.Lang::getLocale()) !!}@endslot
            @endcomponent
        </div>
    </div>
    <div class="header-over__center">
        <div class="header-line">
            <div class="header-line__list">
                @foreach($menu->getKids() as $element)
                    @if ($loop->first)
                        @include('site_menu::site.header-menu-element', ['element' => $element, 'isActive' => true])
                    @else
                        @include('site_menu::site.header-menu-element', ['element' => $element])
                    @endif
                @endforeach
            </div>
        </div>
        <div class="header-line-item header-line-item--phone">
            <div class="header-line-item__link header-line-item__link--icon-small">
                <a href="tel:{{ config('db.basic.phone_number_1') }}">{{ config('db.basic.phone_number_1') }}</a>
                <div class="icon js-init" data-mfp="inline" data-mfp-src="#contact-popup">
                    {!! SiteHelpers\SvgSpritemap::get('icon-arrow-bottom') !!}
                </div>
            </div>
        </div>
    </div>
    <div class="header-over__right">
        <div class="header-line__list">
            {!! Widget::show('languages::trigger') !!}
        </div>
        {!! Widget::show('header-auth-link') !!}
    </div>
</div>
