<?php

use App\Modules\SiteMenu\Models\SiteMenu;

return [
    'places' => [
        SiteMenu::PLACE_FOOTER,
        SiteMenu::PLACE_HEADER,
        SiteMenu::PLACE_MOBILE,
        SiteMenu::PLACE_CUSTOMERS,
        SiteMenu::PLACE_PARTNERS,
        SiteMenu::PLACE_ZALEM,
        SiteMenu::PLACE_CATEGORIES_FOOTER,
        SiteMenu::PLACE_CATEGORIES_MOBILE
    ],
    'mobile-footer' => [
        SiteMenu::PLACE_CATEGORIES_MOBILE,
        SiteMenu::PLACE_ZALEM,
        SiteMenu::PLACE_CUSTOMERS,
        SiteMenu::PLACE_PARTNERS
    ]
];
