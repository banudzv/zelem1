<?php

namespace App\Modules\SiteMenu\Controllers\Api;

use App\Core\ApiController;
use App\Modules\SiteMenu\Models\SiteMenu;
use App\Modules\SiteMenu\Resources\SiteMenuElementFullResource;
use App\Modules\SiteMenu\Resources\SiteMenuElementResource;

/**
 * Class SiteMenuController
 *
 * @package App\Modules\SiteMenu\Controllers\Api
 */
class SiteMenuController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/site-menu/types",
     *     tags={"SiteMenu"},
     *     summary="Returns all menu types available on the site",
     *     operationId="getSiteMenuTypes",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(type="string")
     *         )
     *     ),
     * )
     */
    public function types()
    {
        return config('site_menu.places', []);
    }
    
    /**
     * @OA\Get(
     *     path="/api/site-menu/all",
     *     tags={"SiteMenu"},
     *     summary="Returns all the menus",
     *     operationId="getSiteMenuFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/SiteMenuFullInformation")
     *     ),
     * )
     */
    public function all()
    {
        return SiteMenuElementFullResource::collection(SiteMenu::with(['data'])->get());
    }
    
    /**
     * @OA\Get(
     *     path="/api/site-menu/{type}",
     *     tags={"SiteMenu"},
     *     summary="Returns list of elements for menu",
     *     operationId="getMenu",
     *     deprecated=false,
     *     @OA\Parameter(
     *         name="type",
     *         in="path",
     *         description="Menu type",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="menuType"
     *         )
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/SiteMenuElement")
     *         )
     *     ),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     *
     * @param string $type
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show(string $type)
    {
        abort_unless(in_array($type, config('site_menu.places', [])), 404);
        return SiteMenuElementResource::collection(SiteMenu::wherePlace($type)->active(true)->oldest('position')->get());
    }
}
