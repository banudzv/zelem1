<?php

namespace App\Modules\SiteMenu\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Exceptions\WrongParametersException;
use CustomSiteMenu;

/**
 * Class FooterMenu
 * @package App\Modules\SiteMenu\Widgets
 */
class FooterMenu implements AbstractWidget
{
    protected $place;
    /**
     * @var string
     */
    private $menu;

    private $menuData;

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */

    /**
     * FooterMenu constructor.
     *
     * @param string $place
     * @throws WrongParametersException
     */

    public function __construct(string $place = 'footer', string $menu = 'footer')
    {
        if ($place !== 'footer' && $place !== 'mobile-footer') {
            throw new WrongParametersException('$place could be only `footer` of `mobile-footer`');
        }
        $this->place = $place;
        $this->menu = $menu;
    }

    public function render()
    {
        if ($this->getMenu()->hasKids() === false) {
            return null;
        }

        return view('site_menu::site.'.$this->place.'-menu', [
            'menu' => $this->getMenu(),
            'menuSlug' => $this->menu
        ]);
    }

    public function getMenu()
    {
        if (!$this->menuData) {
            $this->menuData = CustomSiteMenu::get($this->menu);
        }

        return $this->menuData;
    }
}
