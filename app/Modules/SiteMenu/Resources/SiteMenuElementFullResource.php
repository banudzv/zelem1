<?php

namespace App\Modules\SiteMenu\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SiteMenuElementFullResource
 *
 * @package App\Modules\SiteMenu\Resources
 *
 * @OA\Schema(
 *   schema="SiteMenuFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"active", "noindex", "nofollow", "parent_id", "position", "place", "data"},
 *           @OA\Property(property="active", type="boolean", description="Menu element activity"),
 *           @OA\Property(property="noindex", type="boolean", description="Do we need to place url into noindex tag?"),
 *           @OA\Property(property="nofollow", type="boolean", description="Do we need to add rel='nofollow' to the link?"),
 *           @OA\Property(property="parent_id", type="integer", description="Element parent id"),
 *           @OA\Property(property="position", type="integer", description="Element position"),
 *           @OA\Property(property="place", type="string", description="Menu type"),
 *           @OA\Property(
 *              property="data",
 *              type="array",
 *              description="Multilanguage data",
 *              @OA\Items(
 *                  type="object",
 *                  allOf={
 *                      @OA\Schema(
 *                          required={"language", "name", "slug", "slug", "slug_type"},
 *                          @OA\Property(property="language", type="string", description="Language to store"),
 *                          @OA\Property(property="name", type="string", description="Menu element name"),
 *                          @OA\Property(property="slug", type="string", description="Menu element url"),
 *                          @OA\Property(property="slug_type", type="string", description="Menu element url type"),
 *                      )
 *                  }
 *              )
 *           ),
 *       )
 *   }
 * )
 */
class SiteMenuElementFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->toArray();
    }
}
