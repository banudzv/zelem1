<?php

namespace App\Modules\SiteMenu\Resources;

use App\Modules\SiteMenu\Models\SiteMenu;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SiteMenuElementResource
 *
 * @package App\Modules\SiteMenu\Resources
 *
 * @OA\Schema(
 *   schema="SiteMenuElement",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"noindex", "nofollow", "name", "url"},
 *           @OA\Property(property="noindex", type="boolean", description="Do we need to place url into noindex tag?"),
 *           @OA\Property(property="nofollow", type="boolean", description="Do we need to add rel='nofollow' to the link?"),
 *           @OA\Property(property="name", type="string", description="Text to show for clients"),
 *           @OA\Property(property="url", type="string", description="URL to link user"),
 *       )
 *   }
 * )
 */
class SiteMenuElementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var SiteMenu $element */
        $element = $this->resource;
        
        return [
            'noindex' => $element->noindex,
            'nofollow' => $element->nofollow,
            'name' => $element->current->name,
            'url' => url($element->current->slug),
        ];
    }
}
