<?php

namespace App\Modules\SiteMenu\Database\Seeds;

use App\Core\Modules\Languages\Models\Language;
use App\Modules\SiteMenu\Models\SiteMenu;
use App\Modules\SiteMenu\Models\SiteMenuTranslates;
use Exception;
use Illuminate\Database\Seeder;
use DB;
use Log;
use Illuminate\Support\Arr;

class SiteMenuSeeder extends Seeder
{
    public const MODULE = 'site_menu';

    /**
     * @throws Exception
     */
    public function run() {
        foreach ($this->data() as $place => $menu) {
            $itemsCount = SiteMenu::where('place', $place)->count();
            if ($itemsCount) {
                continue;
            }
            foreach ($menu as $menuItem) {
                DB::beginTransaction();

                try {
                    $menuItem['active'] = true;
                    $menuItem['place'] = $place;
                    $menuItem['parent_id'] = 0;

                    $newMenuItem = SiteMenu::Create(
                        Arr::except($menuItem, ['languages'])
                    );

                    Language::all()->each(
                        function (Language $language) use ($menuItem, $newMenuItem) {
                            $siteMenuTranslates = isset($menuItem['languages'][$language->slug]) ?
                                $menuItem['languages'][$language->slug] : $menuItem['languages']['ru'];
                            $siteMenuTranslates['row_id'] = $newMenuItem->id;
                            $siteMenuTranslates['language'] = $language->slug;
                            $siteMenuTranslates['slug_type'] = SiteMenu::INTERNAL_LINK;

                            SiteMenuTranslates::firstOrCreate(
                                [
                                    'row_id' => $siteMenuTranslates['row_id'],
                                    'language' => $siteMenuTranslates['language']
                                ],
                                $siteMenuTranslates
                            );
                        }
                    );

                    DB::commit();
                } catch (Exception $exception) {
                    DB::rollBack();
                    Log::error($exception->getMessage());
                }
            }
        }
    }

    protected function data()
    {
        return [
            SiteMenu::PLACE_CUSTOMERS => [
                [
                    'languages' => [
                        'ru' => [
                            'name' => 'Контакты',
                            'slug' => '/contact',
                        ],
                        'ua' => [
                            'name' => 'Контакты',
                            'slug' => '/contact',
                        ]
                    ]
                ],
                [
                    'languages' => [
                        'ru' => [
                            'name' => 'Доставка и оплата',
                            'slug' => '/dostavka-i-oplata',
                        ],
                        'ua' => [
                            'name' => 'Доставка та оплата',
                            'slug' => '/dostavka-i-oplata',
                        ]
                    ]
                ],
                [
                    'languages' => [
                        'ru' => [
                            'name' => 'Гарантия и возврат',
                            'slug' => '/warranty',
                        ],
                        'ua' => [
                            'name' => 'Гарантія та повернення',
                            'slug' => '/warranty',
                        ]
                    ]
                ],
                [
                    'languages' => [
                        'ru' => [
                            'name' => 'Оферта',
                            'slug' => '/oferta',
                        ],
                        'ua' => [
                            'name' => 'Оферта',
                            'slug' => '/oferta',
                        ]
                    ]
                ]
            ],
            SiteMenu::PLACE_PARTNERS => [
                [
                    'languages' => [
                        'ru' => [
                            'name' => 'Контакты',
                            'slug' => '/contact',
                        ],
                        'ua' => [
                            'name' => 'Контакты',
                            'slug' => '/contact',
                        ]
                    ]
                ],
                [
                    'languages' => [
                        'ru' => [
                            'name' => 'Доставка и оплата',
                            'slug' => '/dostavka-i-oplata',
                        ],
                        'ua' => [
                            'name' => 'Доставка та оплата',
                            'slug' => '/dostavka-i-oplata',
                        ]
                    ]
                ],
                [
                    'languages' => [
                        'ru' => [
                            'name' => 'Гарантия и возврат',
                            'slug' => '/warranty',
                        ],
                        'ua' => [
                            'name' => 'Гарантія та повернення',
                            'slug' => '/warranty',
                        ]
                    ]
                ],
                [
                    'languages' => [
                        'ru' => [
                            'name' => 'Оферта',
                            'slug' => '/oferta',
                        ],
                        'ua' => [
                            'name' => 'Оферта',
                            'slug' => '/oferta',
                        ]
                    ]
                ]
            ],
        ];
    }
}
