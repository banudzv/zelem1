<?php

namespace App\Modules\Export\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\Categories\Models\Category;
use App\Modules\CustomGoods\Models\CustomGood;
use App\Modules\Export\Models\ExportPromUa;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Macro\MultiSelect;
use CustomForm\Text;
use Illuminate\Database\Eloquent\Model;
use Html;

/**
 * Class ArticleForm
 *
 * @package App\Core\Modules\Administrators\Forms
 */
class ExportForm implements FormInterface
{

    /**
     * @param  Model|ExportPromUa|null $model
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $model = null): Form
    {
        $form = Form::create()->doNotShowTopButtons();
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Text::create()->setValue('Для экспорта всех категорий оставьте поле пустым'),
            MultiSelect::create('categories[]')
                ->add(Category::getDictionaryForSelects())
                ->setLabel('categories::general.attributes.categories')
        );
        $form->buttons->doNotShowSaveAndAddButton()->doNotShowSaveAndCloseButton()->doNotShowSaveButton();
        $form->buttons->addCustomButton('admin.buttons.export', ['href' => route('admin.export.store'), 'type' => 'submit', 'class' => 'btn btn-flat btn-m btn-success']);

        return $form;
    }

}
