<?php

namespace App\Modules\Export\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'export';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.permission-name',
                    'ru' => 'Экспорт',
                ],
                [
                    'name' => 'general.status',
                    'ru' => 'Статус выполнения',
                ],
                [
                    'name' => 'general.created_at',
                    'ru' => 'Экспорт запущен',
                ],
                [
                    'name' => 'general.updated_at',
                    'ru' => 'Экспорт выполнен',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.facebook-feed.description',
                    'ru' => 'Zalem products feed for facebook'
                ]
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
