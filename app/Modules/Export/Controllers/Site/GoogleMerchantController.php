<?php

namespace App\Modules\Export\Controllers\Site;

use App\Core\SiteController;
use App\Modules\Export\Models\GoogleMerchant;

/**
 * Class GoogleMerchantController
 *
 * @package App\Modules\Export\Controllers\Site
 */
class GoogleMerchantController extends SiteController
{
    
    /**
     * Google xml output
     *
     * @return \Illuminate\Http\Response
     */
    public function indexXml()
    {
        $model = new GoogleMerchant;
        $offers = $model->getGoogleOffersXml();

        return response()->view('export::site.google.xml', [
            'offers' => $offers,
        ])->header('Content-Type', 'text/xml');
    }

}
