<?php

namespace App\Modules\Export\Controllers\Site;

use App\Core\SiteController;
use App\Modules\Export\Models\Facebook;

/**
 * Class FacebookController
 *
 * @package App\Modules\Export\Controllers\Site
 */
class FacebookController extends SiteController
{
    
    /**
     * Google xml output
     *
     * @return \Illuminate\Http\Response
     */
    public function indexXml()
    {
        $model = new Facebook();
        $offers = $model->getFacebookOffersXml();

        return response()->view('export::site.facebook.xml', [
            'offers' => $offers,
        ])->header('Content-Type', 'text/xml');
    }

}
