<?php

namespace App\Modules\Export\Controllers\Site;

use App\Core\SiteController;
use App\Modules\Export\Models\YandexMarket;
use Carbon\Carbon;

/**
 * Class YandexMarketController
 *
 * @package App\Modules\Export\Controllers\Site
 */
class PromUaController extends SiteController
{
    
    /**
     * Yandex xml output
     *
     * @return \Illuminate\Http\Response
     */
    public function indexXml()
    {
        $model = new YandexMarket;
        $offers = $model->getYandexOffersXml();
        return response()->view('export::site.prom.xml', [
            'offers' => $offers,
            'date' => $offers->isNotEmpty() ? $offers->max('updated_at') : Carbon::now(),
            'categories' => $model->getYandexCategoriesXml(),
            'currencies' => $model->getCurrencies(),
        ])->header('Content-Type', 'text/xml');
    }

}
