<?php

namespace App\Modules\Export\Controllers\Site;

use App\Core\SiteController;
use App\Modules\Export\Models\Rozetka;

/**
 * Class RozetkaController
 *
 * @package App\Modules\Export\Controllers\Site
 */
class RozetkaController extends SiteController
{
    
    /**
     * Rozetka xml output
     *
     * @return \Illuminate\Http\Response
     */
    public function indexXml()
    {
        $model = new Rozetka;
        $offers = $model->getOffersXml();
        $categories = $model->getCategoriesXml();
        $currencies = $model->getCurrencies();

        return response()->view('export::site.rozetka.xml', [
            'offers' => $offers,
            'categories' => $categories,
            'currencies' => $currencies,
        ])->header('Content-Type', 'text/xml');
    }

}
