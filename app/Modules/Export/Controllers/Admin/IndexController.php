<?php

namespace App\Modules\Export\Controllers\Admin;

use App\Core\AjaxTrait;
use App\Core\AdminController;
use App\Modules\Export\Forms\ExportForm;
use App\Modules\Export\Jobs\Export;
use App\Modules\Export\Models\ExportPromUa;

/**
 * Class IndexController
 *
 * @package App\Modules\Import\Controllers\Admin
 */
class IndexController extends AdminController
{
    use AjaxTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        return view('export::admin.index', [
            'export' => null,
            'list' => ExportPromUa::getList(),
            'form' => ExportForm::make(),
        ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $exportPromUa = new ExportPromUa();
        $exportPromUa->status = ExportPromUa::STATUS_NEW;
        $exportPromUa->save();
        Export::dispatch($exportPromUa, request('categories'));

        return redirect()->back();

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function status()
    {
        $export = ExportPromUa::getLast();

        return $this->successJsonAnswer([
            'status' => $export ? $export->status : '',
        ]);
    }

    /**
     * @param ExportPromUa $exportPromUa
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(ExportPromUa $exportPromUa)
    {
        $exportPromUa->deleteFile();
        $exportPromUa->forceDelete();

        return $this->afterDestroy();
    }

    /**
     * @param ExportPromUa $exportPromUa
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function download(ExportPromUa $exportPromUa)
    {
        $file = storage_path() . '/app/public/export/' . $exportPromUa->file;

        if (is_file($file)) {
            return response()->download($file);
        }
        return $this->afterFail();
    }


}
