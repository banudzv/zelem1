<?php

use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:export'])->group(function () {

    Route::get('catalog/export', [
        'uses' => 'IndexController@index',
        'as' => 'admin.export.index',
    ]);
    Route::post('catalog/export/store', [
        'uses' => 'IndexController@store',
        'as' => 'admin.export.store',
    ]);
    Route::get('catalog/export/check-status', [
        'uses' => 'IndexController@status',
        'as' => 'admin.export.check-status',
    ]);
    Route::get('catalog/export/{exportPromUa}/destroy', [
        'uses' => 'IndexController@destroy',
        'as' => 'admin.export.destroy'
    ]);
    Route::get('catalog/export/{exportPromUa}/download', [
        'uses' => 'IndexController@download',
        'as' => 'admin.export.download'
    ]);


});

