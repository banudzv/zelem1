<?php

use Illuminate\Support\Facades\Route;

Route::get('yandex_market.xml', ['as' => 'site.yandex_market.xml', 'uses' => 'YandexMarketController@indexXml']);
//Route::get('google_merchant.xml', ['as' => 'site.google_merchant.xml', 'uses' => 'GoogleMerchantController@indexXml']);
//Route::get('facebook.xml', ['as' => 'site.facebook.xml', 'uses' => 'FacebookController@indexXml']);
Route::get('prom_merchant.xml', ['as' => 'site.prom_merchant.xml', 'uses' => 'PromUaController@indexXml']);
Route::get('rozetka.xml', ['as' => 'site.rozetka.xml', 'uses' => 'RozetkaController@indexXml']);
