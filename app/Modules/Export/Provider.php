<?php

namespace App\Modules\Export;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use CustomMenu;
use CustomRoles;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Export
 */
class Provider extends BaseProvider
{

    /**
    * Set custom presets
    */
    protected function presets()
    {
    }
    
    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        $group = CustomMenu::get()->group();
        $group
            ->block('catalog')
            ->link('Экспорт', RouteObjectValue::make('admin.export.index'))
            ->setPosition(12);
        // Register role scopes
        CustomRoles::add('export', 'export::general.permission-name')
            ->except(RoleRule::VIEW,RoleRule::UPDATE);

    }
    
    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {

    }

}
