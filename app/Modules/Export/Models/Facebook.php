<?php

namespace App\Modules\Export\Models;

use App\Modules\Categories\Models\Category;
use App\Modules\Currencies\Models\Currency;
use App\Modules\Products\Models\Product;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Facebook
 * @package App\Modules\Export\Models
 */
class Facebook
{

    protected $_tree = [];
    protected $_xmlMap = [];
    
    /**
     * @var Product[]|Collection
     */
    protected $_xmlProducts;
    protected $_imagesXmlMap = [];

    public function getXml()
    {
        if (empty($this->_tree)) {
            $this->buildXml();
        }
        return $this->_tree;
    }

    private function buildXml()
    {
        array_map(function($item) {
            if (!class_exists($item)) {
                return;
            }
            $provider = new $item([]);
            if (!method_exists($provider, 'initSitemap')) {
                return;
            }
            if ( ($parts = $provider->initSitemap()) && is_array($parts) ) {
                foreach ($parts as $part) {
                    $this->_tree[] = $part;
                }
            }
        }, config('app.providers'));
    }

    public function getCurrencies(){
        return Currency::where('default_on_site', 1)
            ->orWhere('default_in_admin_panel', 1)
            ->get();
    }

    public function getFacebookOffersXml()
    {
        if (empty($this->_xmlProducts)) {
            $this->buildFacebookOffersXml();
        }
        return $this->_xmlProducts;
    }

    public function getFacebookCategoriesXml()
    {
        if (empty($this->_xmlCategories)) {
            $this->buildFacebookCategoriesXml();
        }
        return $this->_xmlCategories;
    }

    private function buildFacebookOffersXml()
    {
        $this->_xmlProducts = Product::with(
            'brand.data',
            'data',
            'images',
            'group',
            'category.data',
            'labels.data'
        )
            ->where('active', 1)
            ->get();
    }

    private function buildFacebookCategoriesXml()
    {
        $this->_xmlCategories = Category::where('active', 1)->get();
    }
}
