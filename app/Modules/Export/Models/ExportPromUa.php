<?php


namespace App\Modules\Export\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\Export\Models\ExportPromUa
 *
 * @property int $id
 * @property string $status
 * @property string|null $file
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed|null $url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Export\Models\ExportPromUa whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExportPromUa extends Model
{
    const STATUS_NEW = 'new';
    const STATUS_PROCESSING = 'processing';
    const STATUS_DONE = 'done';
    const STATUS_FAILED = 'failed';

    protected $table = 'exports_prom_ua';

    protected $fillable = ['status', 'file', 'message', 'data'];


    /**
     * @return ExportPromUa|object
     */
    public static function getLast(): ?self
    {
        return ExportPromUa::latest('id')->where('status', '!=', static::STATUS_DONE)->first();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList()
    {
        return ExportPromUa::latest('id')->paginate(10);
    }

    /**
     * @return bool
     */
    public function isInProcess(): bool
    {
        return $this->status !== static::STATUS_DONE && $this->status !== static::STATUS_FAILED;
    }

    /**
     * @return bool
     */
    public function deleteFile(): bool
    {
        $file = storage_path() . '/app/public/export/' . $this->file;
        if (is_file($file)) {
            return unlink($file);
        }
        return false;

    }


}