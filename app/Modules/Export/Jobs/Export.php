<?php

namespace App\Modules\Export\Jobs;

use App\Helpers\SheetsXls;
use App\Modules\Categories\Models\Category;
use App\Modules\Currencies\Models\Currency as CurrencyModel;
use App\Modules\Export\Models\ExportPromUa;
use App\Modules\Features\Models\FeatureTranslates;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use File;

/**
 * Class Export
 * @package App\Modules\Export\Jobs
 */
class Export implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $exportPromUa;

    private $category_ids;

    public function __construct(ExportPromUa $exportPromUa, $category_ids = null)
    {
        $this->exportPromUa = $exportPromUa;
        $this->category_ids = $category_ids;
    }

    public function handle()
    {
        try {
            $this->exportPromUa->status = ExportPromUa::STATUS_PROCESSING;
            $this->exportPromUa->update();
            list($products, $productsKeys) = $this->getProducts();
            list($prices, $pricesKeys) = $this->getPrices();
            list($categories, $categoriesKeys) = $this->getCategories();
            $spreadsheet = new SheetsXls();
            $filename = date('d_m_Y_H_m_s');
            $spreadsheet->addSheet($products, $productsKeys, 'Products', 0);
            $spreadsheet->addSheet($prices, $pricesKeys, 'Prices', 1);
            $spreadsheet->addSheet($categories, $categoriesKeys, 'Categories', 2);
            $path = storage_path() . '/app/public/export/';
            if (!File::isDirectory($path)) {
                File::makeDirectory($path);
            }
            $file = $spreadsheet->getFile($filename, $path);
            $this->exportPromUa->file = $file;
            $this->exportPromUa->status = ExportPromUa::STATUS_DONE;
            $this->exportPromUa->update();
        } catch (\Exception $exception) {
            $this->exportPromUa->status = ExportPromUa::STATUS_FAILED;
            $this->exportPromUa->message = $exception->getTraceAsString();
            $this->exportPromUa->update();
            \Log::error($exception);
        }
    }

    /**
     * Returns data layer and columns names for groups.
     *
     * @return array
     */
    protected function getProducts(): array
    {
        $maxFeatures = 0;
        $products = ProductGroup::with([
            'brandCurrent', 'featureCurrent', 'data', 'engineNumbers',
            'years', 'featureValues', 'featuresCurrent', 'valuesCurrent',
        ])->get()->map(function (ProductGroup $group) use (&$maxFeatures) {
            $tr = $group->data->where('language', config('app.default-language', app()->getLocale()))->first();
            $datum = [];
            $datum[] = $group->id;
            $datum[] = null;
            $datum[] = $group->category_id;
            $datum[] = null;
            $datum[] = $group->brandCurrent ? $group->brandCurrent->name : null;
            $datum[] = $group->featureCurrent ? $group->featureCurrent->name : null;
            $datum[] = $tr->name;
            foreach (config('languages', []) as $lang => $_) {
                $translate = $group->data->where('language', $lang)->first();
                $datum[] = $translate ? $translate->name : '';
            }
            $datum[] = $tr->text;
            foreach (config('languages', []) as $lang => $_) {
                $translate = $group->data->where('language', $lang)->first();
                $datum[] = $translate ? $translate->text : '';
            }
            $datum[] = $group->engineNumbers->implode('engine_number', '|');
            $datum[] = $group->years->implode('year', '|');

            $featureCurrent = $group->featuresCurrent->where('row_id', '!=', $group->feature_id);
            $group->featuresCurrent->each(function (FeatureTranslates $ft) use (&$datum, $group) {
                $datum[] = $ft->name;
                $datum[] = $group->valuesCurrent->whereIn(
                    'row_id',
                    $group->featureValues->where('feature_id', $ft->row_id)->pluck('value_id')
                )->implode('name', '|');
            });
            if ($featureCurrent->count() > $maxFeatures) {
                $maxFeatures = $featureCurrent->count();
            }

            return $datum;
        })->toArray();
        $productsKeys = ['GroupId', 'GroupNumber', 'CategoryId', 'CategoryNumber',
            'Brand', 'MainFeature', 'Name'];
        foreach (config('languages', []) as $lang => $_) {
            $productsKeys[] = 'Name_' . $lang;
        }
        $productsKeys[] = 'Description';
        foreach (config('languages', []) as $lang => $_) {
            $productsKeys[] = 'Description_' . $lang;
        }
        $productsKeys[] = 'EngineNumbers';
        $productsKeys[] = 'Year';
        for ($i = 0; $i < $maxFeatures; $i++) {
            $productsKeys[] = 'Feature';
            $productsKeys[] = 'Value';
        }

        return [$products, $productsKeys];
    }

    /**
     * Returns data layer and columns names for prices.
     *
     * @return array
     */
    protected function getPrices(): array
    {
        /** @var CurrencyModel $currency */
        $currency = config('currency.admin');

        $prices = Product::with(['valueCurrent', 'data'])->get()->map(function (Product $product) use ($currency) {
            $tr = $product->data->where('language', config('app.default-language', app()->getLocale()))->first();
            $datum = [
                $product->id, $product->group_id, null, $currency->microdata, $product->price,
                $product->old_price ?: null, (int)$product->available,
                $product->valueCurrent ? $product->valueCurrent->id : null,
                $product->vendor_code, $tr->name
            ];
            foreach (config('languages', []) as $lang => $_) {
                $translate = $product->data->where('language', $lang)->first();
                $datum[] = $translate ? $translate->name : '';
            }
            $datum[] = $tr->short_name;
            foreach (config('languages', []) as $lang => $_) {
                $translate = $product->data->where('language', $lang)->first();
                $datum[] = $translate ? $translate->short_name : '';
            }
            $datum[] = $product->images->implode('original_photo_link', '|');

            return $datum;
        })->toArray();
        $pricesKeys = ['ProductId', 'GroupId', 'GroupNumber', 'Currency', 'Price', 'OldPrice',
            'Availability', 'MainFeatureValue', 'VendorCode', 'Name'];
        foreach (config('languages', []) as $lang => $_) {
            $pricesKeys[] = 'Name_' . $lang;
        }
        $pricesKeys[] = 'ShortName';
        foreach (config('languages', []) as $lang => $_) {
            $pricesKeys[] = 'ShortName_' . $lang;
        }
        $pricesKeys[] = 'Images';

        return [$prices, $pricesKeys];
    }

    /**
     * Returns data layer and columns names for categories.
     *
     * @return array
     */
    protected function getCategories(): array
    {
        $categories = Category::with(['data'])->get()->map(function (Category $category) {
            $tr = $category->data->where('language', config('app.default-language', app()->getLocale()))->first();
            $datum = [$category->id, null, $category->parent_id, null, $tr->name];
            foreach (config('languages', []) as $lang => $_) {
                $translate = $category->data->where('language', $lang)->first();
                $datum[] = $translate ? $translate->name : '';
            }

            return $datum;
        })->toArray();
        $categoriesKeys = ['CategoryId', 'CategoryNumber', 'CategoryParentId',
            'CategoryParentNumber', 'Name'];
        foreach (config('languages', []) as $lang => $_) {
            $categoriesKeys[] = 'Name_' . $lang;
        }

        return [$categories, $categoriesKeys];
    }
}
