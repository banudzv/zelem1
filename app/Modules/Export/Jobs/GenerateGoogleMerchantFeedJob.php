<?php

namespace App\Modules\Export\Jobs;

use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Modules\Export\Models\GoogleMerchant;
use Config;
use File;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use View;

class GenerateGoogleMerchantFeedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const FEED_NAME = 'google_merchant_{lang}.xml';

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = storage_path() . config('export.export-path');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }

        $model = new GoogleMerchant;
        $offers = $model->getGoogleOffersXml();
        $pages = $this->getSystemPages();

        foreach (config('languages') as $slug => $lang) {
            $filename = $path . str_replace('{lang}', $slug, static::FEED_NAME);
            $breadcrumbs = $pages->map(
                function (SystemPage $page) use ($slug) {
                    return $page->dataFor($slug)->name;
                }
            )->toArray();

            $content = View::make(
                'export::site.google.lang-xml',
                [
                    'offers' => $offers,
                    'lang' => $slug,
                    'default' => $slug == Config::get('app.default-language'),
                    'breadcrumbs' => $breadcrumbs
                ]
            )->render();

            File::put($filename, $content);
        }
    }

    protected function getSystemPages()
    {
        return SystemPage::with('data')
            ->whereHas(
                'data',
                function (Builder $query) {
                    $query->whereIn('slug', ['index', 'categories']);
                }
            )
            ->get();
    }
}
