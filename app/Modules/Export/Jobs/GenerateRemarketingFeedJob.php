<?php

namespace App\Modules\Export\Jobs;

use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Products\Models\Product;
use Config;
use File;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateRemarketingFeedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const FEED_NAME = 'google_ads_feed_{lang}.csv';

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $products = $this->getProducts();

        $path = storage_path() . config('export.export-path');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }

        $columns = array(
            'ID',
            'Item title',
            'Final URL',
            'Image URL',
            'Item description',
            'Item Category',
            'Price',
            'Sale Price'
        );

        foreach (config('languages') as $slug => $lang) {
            $fileName = $path . str_replace('{lang}', $slug, static::FEED_NAME);
            $file = fopen($fileName, 'w');
            fputcsv($file, $columns);

            $prefix = $slug == Config::get('app.default-language') ? '' : $slug;

            foreach ($products as $product) {
                if ($product->price_for_site > 0) {
                    $link = str_replace(
                        '/' . $slug . '/',
                        '/',
                        route(
                            'site.product',
                            [
                                'slug' => $product->dataFor($slug)->slug
                            ],
                            false
                        )
                    );

                    $car = $product->group->allEngineNumbers->map(
                        function (EngineNumber $engineNumber) {
                            return $engineNumber->vendor->name . ' ' . $engineNumber->model->name;
                        }
                    )->unique()->sort()->first();

                    $row = [
                        'ID' => $product->id,
                        'Item title' => $product->getNameForLanguage($slug),
                        'Final URL' => url($prefix . $link),
                        'Image URL' => $product->preview->link('big', false),
                        'Item description' => $car,
                        'Item Category' => $product->category ? $product->category->dataFor($slug)->name : '',
                        'Price' => ($product->old_price > $product->price)
                            ? $product->old_price_with_code
                            : $product->price_with_code,
                        'Sale Price' => ($product->old_price > $product->price)
                            ? $product->price_with_code
                            : ''
                    ];

                    fputcsv($file, $row);
                }
            }

            fclose($file);
        }
    }

    protected function getProducts()
    {
        return Product::with(
            'data',
            'images',
            'category.data',
            'group.allEngineNumbers.vendor',
            'group.allEngineNumbers.model'
        )
            ->where('active', 1)
            ->get();
    }
}
