<?php

namespace App\Modules\Export\Jobs;

use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\Product;
use App\Modules\ProductsLabels\Models\Label;
use Config;
use File;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateGoogleAdsFeedsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const PRODUCTS_FEED_NAME = 'google_ads_products_feed_{lang}.csv';
    const CATEGORIES_FEED_NAME = 'google_ads_categories_feed_{lang}.csv';

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = storage_path() . config('export.export-path');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }

        $columns = array(
            'Page URL',
            'Custom label'
        );

        foreach (config('languages') as $slug => $lang) {
            $productsFilename = $path . str_replace('{lang}', $slug, static::PRODUCTS_FEED_NAME);
            $categoriesFilename = $path . str_replace('{lang}', $slug, static::CATEGORIES_FEED_NAME);

            $this->writeToFile(
                $productsFilename,
                $columns,
                $this->getProducts($slug)
            );

            $this->writeToFile(
                $categoriesFilename,
                $columns,
                $this->getCategories($slug)
            );
        }
    }

    protected function writeToFile(string $fileName, array $columns, array $data)
    {
        $file = fopen($fileName, 'w');
        fputcsv($file, $columns);

        foreach ($data as $item) {
            $row = [];
            foreach ($columns as $column) {
                $row[$column] = $item[$column];
            }

            fputcsv($file, $row);
        }

        fclose($file);
    }

    protected function getProducts(string $lang)
    {
        $prefix = $lang == Config::get('app.default-language') ? '' : $lang;

        return Product::with('data', 'labels.data')
            ->where('active', 1)
            ->get()
            ->filter(
                function (Product $product) {
                    return $product->is_available && $product->price_for_site > 0;
                }
            )
            ->map(
                function (Product $product) use ($prefix, $lang) {
                    $labels = $product->labels->map(
                        function (Label $label) use ($lang) {
                            return strtoupper($label->dataFor($lang)->text);
                        }
                    )->toArray();

                    $link = str_replace(
                        '/' . $lang . '/',
                        '/',
                        route(
                            'site.product',
                            [
                                'slug' => $product->dataFor($lang)->slug
                            ],
                            false
                        )
                    );

                    return [
                        'Page URL' => url($prefix . $link),
                        'Custom label' => implode(';', $labels)
                    ];
                }
            )
            ->toArray();
    }

    protected function getCategories(string $lang)
    {
        $prefix = $lang == Config::get('app.default-language') ? '' : $lang;

        return Category::with('data')
            ->active()
            ->get()
            ->map(
                function (Category $category) use ($prefix, $lang) {
                    $link = str_replace(
                        '/' . $lang . '/',
                        '/',
                        route(
                            'site.category',
                            [
                                'slug' => $category->dataFor($lang)->slug
                            ],
                            false
                        )
                    );

                    return [
                        'Page URL' => url($prefix . $link),
                        'Custom label' => null
                    ];
                }
            )
            ->toArray();
    }
}
