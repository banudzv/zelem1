@php
    /** @var \App\Modules\Products\Models\Product[] $offers */
    /** @var string $lang */
    /** @var boolean $default */
    /** @var array $breadcrumbs */
echo '<?xml version="1.0" encoding="utf-8" ?>';
@endphp
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
    <channel>
        <title>{{config('db.basic.company_'.\Lang::getLocale())}}</title>
        <description>@lang('export::site.facebook-feed.description')</description>
        <link>{{ route('site.home')}}</link>
        @foreach($offers as $offer)
            @if($offer->price_for_site > 0)
            <item>
                <g:id>{{ $offer->id }}</g:id>
                <g:title>{{ $offer->getNameForLanguage($lang) }}</g:title>
                @if(trim(strip_tags($offer->group->dataFor($lang)->text)))
                    <g:description><![CDATA[{!! \Illuminate\Support\Str::limit(trim(strip_tags($offer->group->dataFor($lang)->text)), 4850, '...') !!}]]></g:description>
                @else
                    <g:description><![CDATA[{!! $offer->getNameForLanguage($lang) !!}]]></g:description>
                @endif
                @php
                    $prefix = $default ? '' : $lang;
                    $link = str_replace('/' . $lang . '/', '/', route('site.product', ['slug' => $offer->dataFor($lang)->slug], false));
                @endphp
                <g:link>{{ url($prefix . $link) }}</g:link>
                @if($imageLink = $offer->preview->link('big', false))
                    <g:image_link>{{ $imageLink }}</g:image_link>
                @else
                    <g:image_link>{{ site_media('static/images/placeholders/no-photo.png', false, true, true) }}</g:image_link>
                @endif
                <g:condition>new</g:condition>
                <g:availability>{{ $offer->is_available ? 'in stock' : 'out of stock' }}</g:availability>
                @if($offer->old_price)
                    <g:price>{{ $offer->old_price_with_code }}</g:price>
                    <g:sale_price>{{ $offer->price_with_code }}</g:sale_price>
                @else
                    <g:price>{{ $offer->price_with_code }}</g:price>
                @endif
                @php
                    $categories = $offer->categories()->map(function ($category) use ($lang) {
                        return $category->dataFor($lang)->name;
                    })->toArray();
                    $productType = array_merge($breadcrumbs, $categories);
                @endphp
                <g:product_type>{{ implode(' > ', $productType) }}</g:product_type>
                @if($offer->brand)
                    <g:brand>{{ $offer->brand->dataFor($lang)->name }}</g:brand>
                @endif
                @foreach($offer->labels as $label)
                    @if($loop->index < 5)
                        <g:custom_label_{{ $loop->index }}>{{ $label->dataFor($lang)->text }}</g:custom_label_{{ $loop->index }}>
                    @endif
                @endforeach
                <g:google_product_category>2820</g:google_product_category>
            </item>
            @endif
        @endforeach
    </channel>
</rss>
