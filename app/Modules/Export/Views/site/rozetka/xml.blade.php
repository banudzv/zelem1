@php
/** @var \App\Modules\Products\Models\Product[] $offers */
echo '<?xml version="1.0" encoding="utf-8" ?>';
$stack = [];
@endphp
<yml_catalog date="{{ date('Y-m-d H:i') }}">
    <shop>
        <name>{{config('db.basic.company_'.\Lang::getLocale())}}</name>
        <company>{{config('db.basic.company_'.\Lang::getLocale())}}</company>
        <url>{{ route('site.home')}}</url>
        <platform>Locotrade</platform>
        <currencies>
            @foreach($currencies as $currency)
                <currency id="{{ $currency->microdata }}" rate="{{ $currency->multiplier }}"/>
            @endforeach
        </currencies>
        <categories>
            @foreach($categories as $category)
                {!! Html::tag('category', $category->current->name, [
                    'id' => $category->id,
                    'parent_id' => $category->parent_id ?: false,
                ]) !!}
            @endforeach
        </categories>
        <offers>
            @foreach($offers as $offer)
                @if(in_array($offer->name, $stack) === false && $offer->brand_id)
                @php
                    $noImages = true;
                @endphp
                @foreach($offer->gallery() as $image)
                    @if($image->isImageExists())
                        @php
                            $noImages = false;
                        @endphp
                    @endif
                @endforeach
                @if($noImages === true)
                    @continue
                @endif
                @php
                    $stack[] = $offer->name;
                @endphp
                <offer id="{{ $offer->id }}" available="{{ $offer->is_available ? 'true' : 'false' }}">
                    <url>{{ $offer->site_link }}</url>
                    <price>{{ $offer->price_for_site }}</price>
                    <currencyId>{{ Catalog::currency()->microdataName() }}</currencyId>
                    <categoryId>{{ $offer->group->category_id }}</categoryId>
                    @php
                        $counter = 0;
                    @endphp
                    @foreach($offer->gallery() as $image)
                        @if($image->isImageExists() && $counter < 8)
                            @php
                                $counter++;
                            @endphp
                            <picture>{{ $image->link('600x500') }}</picture>
                        @endif
                    @endforeach
                    <vendor>{{ $offer->brand->current->name }}</vendor>
                    <stock_quantity>{{ $offer->is_available ? \App\Modules\Export\Models\Rozetka::$quantity : 0 }}</stock_quantity>
                    <name>{{ $offer->name }}</name>
                    <description>
                        <![CDATA[
                        {!! $offer->group->current->text !!}
                        ]]>
                    </description>
                    @foreach($offer->feature_values as $param)
                        <param name="{{ $param->feature->current->name }}">{{ $param->value->current->name }}</param>
                    @endforeach
                </offer>
                @endif
            @endforeach
        </offers>
    </shop>
</yml_catalog>
