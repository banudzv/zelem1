@php
    /** @var \App\Modules\Products\Models\Product[] $offers */
echo '<?xml version="1.0" encoding="utf-8" ?>';
@endphp
<shop>
    @foreach($currencies as $currency)
        <currency id="{{ $currency->microdata }}" rate="{{ $currency->multiplier }}"/>
    @endforeach
    <catalog>
        @foreach($categories as $category)
            <category id="{{ $category->id }}" {!! $category->parent_id ? 'parentId="' . $category->parent_id . '"' : '' !!}>{{ $category->current->name }}</category>
        @endforeach
    </catalog>
    <items>
        @foreach($offers as $offer)
            @if($offer->price_for_site > 0)
                <item id="{{ $offer->id }}" selling_type="{{ $offer->wholesale->count() ? 'u' : 'r' }}">
                    <name>{{ $offer->name }}</name>
                    <categoryId>{{ $offer->group->category_id }}</categoryId>
                    @if($offer->wholesale->count())
                        <prices>
                            @foreach($offer->wholesale as $wholesale)
                                <price>
                                    <value>{{$wholesale->price}}</value>
                                    <quantity>{{$wholesale->quantity}}</quantity>
                                </price>
                            @endforeach
                        </prices>
                    @endif
                    <price>{{ $offer->price_for_site }}</price>
                    @foreach($offer->images as $image)
                        @if($image->isImageExists())
                            <image>{{ $image->link('medium') }}</image>
                        @endif
                    @endforeach
                    @if($offer->brand_id)
                        <vendor>{{ $offer->brand->current->name }}</vendor>
                    @endif
                    <vendorCode>{{ $offer->vendor_code }}</vendorCode>
                    @if($offer->value_id)
                        <param name="{{ $offer->value->feature->current->name }}">{{ $offer->value->current->name }}</param>
                    @endif
                    @foreach($offer->feature_values as $param)
                        <param name="{{ $param->feature->current->name }}">{{ $param->value->current->name }}</param>
                    @endforeach
                    <description><![CDATA[{!! $offer->group->current->text !!}]]></description>
                    <available>{{ $offer->is_available ? 'true' : '' }}{{ $offer->is_custom_goods ? 'false' : '' }}</available>
                    <keywords>{!! $offer->current->keywords !!}</keywords>
                </item>
            @endif
        @endforeach
    </items>
</shop>
