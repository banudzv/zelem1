@php
    /** @var \App\Modules\Products\Models\Product[] $offers */
echo '<?xml version="1.0" encoding="utf-8" ?>';
@endphp
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
    <channel>
        <title>{{config('db.basic.company_'.\Lang::getLocale())}}</title>
        <link>{{ route('site.home')}}</link>
        @foreach($offers as $offer)
            @if($offer->price_for_site > 0)
            <item>
                <g:id>{{ $offer->id }}</g:id>
                <g:title>{{ $offer->name }}</g:title>
                @if(trim(strip_tags($offer->group->current->text)))
                    <g:description><![CDATA[{!! \Illuminate\Support\Str::limit(trim(strip_tags($offer->group->current->text)), 4850, '...') !!}]]></g:description>
                @else
                    <g:description><![CDATA[{!! $offer->name !!}]]></g:description>
                @endif
                <g:link>{{ $offer->site_link }}</g:link>
                @if($imageLink = $offer->preview->link('big', false))
                    <g:image_link>{{ $imageLink }}</g:image_link>
                @else
                    <g:image_link>{{ site_media('static/images/placeholders/no-photo.png', false, true, true) }}</g:image_link>
                @endif
                <g:condition>new</g:condition>
                <g:availability>{{ $offer->is_available ? 'in stock' : 'out of stock' }}</g:availability>
                @if($offer->old_price > $offer->price)
                    <g:price>{{ $offer->old_price_with_code }}</g:price>
                    <g:sale_price>{{ $offer->price_with_code }}</g:sale_price>
                @else
                    <g:price>{{ $offer->price_with_code }}</g:price>
                @endif
                <g:google_product_category>2820</g:google_product_category>
                <g:product_type>{{ $offer->categories()->implode('current.name', ' &gt; ') }}</g:product_type>
                @if($offer->brand)
                    <g:brand>{{ $offer->brand->current->name }}</g:brand>
                @endif
                @foreach($offer->labels as $label)
                    <g:custom_label_{{ $loop->index }}>{{ $label->current->text }}</g:custom_label_{{ $loop->index }}>
                @endforeach
            </item>
            @endif
        @endforeach
    </channel>
</rss>
