@php
    /** @var \App\Modules\Products\Models\Product[] $offers */
    /** @var string $lang */
    /** @var boolean $default */
    /** @var array $breadcrumbs */
echo '<?xml version="1.0" encoding="utf-8" ?>';
@endphp
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
    <channel>
        <title>{{config('db.basic.company_'.\Lang::getLocale())}}</title>
        <link>{{ route('site.home')}}</link>
        @foreach($offers as $offer)
            @if($offer->price_for_site > 0 and $offer->dataFor($lang))
            <item>
                <g:id>{{ $offer->id }}</g:id>
                <g:title>{{ $offer->getNameForLanguage($lang) }}</g:title>
                @if(trim(strip_tags($offer->group->dataFor($lang)->text)))
                    <g:description><![CDATA[{!! \Illuminate\Support\Str::limit(trim(strip_tags($offer->group->dataFor($lang)->text)), 4850, '...') !!}]]></g:description>
                @else
                    <g:description><![CDATA[{!! $offer->getNameForLanguage($lang) !!}]]></g:description>
                @endif
                @php
                    $prefix = $default ? '' : $lang;
                    $link = str_replace('/' . $lang . '/', '/', route('site.product', ['slug' => $offer->dataFor($lang)->slug], false));
                @endphp
                <g:link>{{ url($prefix . $link) }}</g:link>
                @if($imageLink = $offer->preview->link('big', false))
                    <g:image_link>{{ $imageLink }}</g:image_link>
                @else
                    <g:image_link>{{ site_media('static/images/placeholders/no-photo.png', false, true, true) }}</g:image_link>
                @endif
                <g:condition>new</g:condition>
                <g:availability>{{ $offer->is_available ? 'in stock' : 'out of stock' }}</g:availability>
                @if($offer->old_price > $offer->price)
                    <g:price>{{ $offer->old_price_with_code }}</g:price>
                    <g:sale_price>{{ $offer->price_with_code }}</g:sale_price>
                @else
                    <g:price>{{ $offer->price_with_code }}</g:price>
                @endif
                <g:google_product_category>2820</g:google_product_category>
                @php
                    $categories = $offer->categories()->map(function ($category) use ($lang) {
                        return $category->dataFor($lang)->name;
                    })->toArray();
                    $productType = array_merge($breadcrumbs, $categories);
                @endphp
                <g:product_type>{{ implode(' &gt; ', $productType) }}</g:product_type>
                @if($offer->brand)
                    <g:brand>{{ $offer->brand->dataFor($lang)->name }}</g:brand>
                @endif
                <g:mpn>{{ $offer->vendor_code}}</g:mpn>
                <g:custom_label_1>{{ $offer->vendor_code}}</g:custom_label_1>
                <g:custom_label_4>{{ $lang }}</g:custom_label_4>
            {{--@foreach($offer->labels as $label)
                    <g:custom_label_{{ $loop->index }}>{{ $label->dataFor($lang)->text }}</g:custom_label_{{ $loop->index }}>
                @endforeach--}}
            </item>
            @endif
        @endforeach
    </channel>
</rss>
