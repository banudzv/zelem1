@php
    /** @var \App\Modules\Export\Models\ExportPromUa[] list */
    /** @var \App\Modules\Export\Models\ExportPromUa|null $export */
    $messageClasses = [
        \App\Modules\Export\Models\ExportPromUa::STATUS_NEW => 'info',
        \App\Modules\Export\Models\ExportPromUa::STATUS_PROCESSING => 'info',
        \App\Modules\Export\Models\ExportPromUa::STATUS_DONE => 'success',
        \App\Modules\Export\Models\ExportPromUa::STATUS_FAILED => 'danger',
    ];
@endphp


@extends('admin.layouts.main')

@if($export && in_array($export->status, [\App\Modules\Export\Models\ExportPromUa::STATUS_NEW, \App\Modules\Export\Models\ExportPromUa::STATUS_PROCESSING]))
    @push('scripts')
        <script>
            let currentStatus = '{{ $export->status }}';
            setInterval(() => {
                $.ajax({
                    url: '{{ route('admin.export.check-status') }}',
                    method: 'GET',
                }).done((data) => {
                    if (data.status !== currentStatus) {
                        window.location.reload();
                    }
                });
            }, 5000);
        </script>
    @endpush
@endif

@section('content-no-row')
    {!! Form::open(['route' => 'admin.export.store', 'files' => false]) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop

@section('content')
    @if($export)
        <div class="callout callout-{{ $messageClasses[$export->status] ?? 'info' }}">
            <h4>Последний экспорт</h4>
            @if($export->status === \App\Modules\Export\Models\ExportPromUa::STATUS_NEW)
                Запущен. Ожидает очереди...
            @elseif($export->status === \App\Modules\Export\Models\ExportPromUa::STATUS_PROCESSING)
                Запущен. В процессе выполнения...
            @elseif($export->status === \App\Modules\Export\Models\ExportPromUa::STATUS_DONE)
                Успешно выполнен!
            @else
                Не выполнен или выполнен частично!
            @endif
            @if($export->message)
                <div>{{ $export->message }}</div>
            @endif
        </div>
    @endif
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>{{ __('validation.attributes.file') }}</th>
                        <th>{{ __('export::general.status') }}</th>
                        <th>{{ __('export::general.created_at') }}</th>
                        <th>{{ __('export::general.updated_at') }}</th>
                        <th></th>
                    </tr>
                    @foreach($list as $item)
                        <tr>
                            <td><a href="{{route('admin.export.download',[$item->id])}}">{{$item->file}}</a></td>
                            <td>{{ $item->status }}</td>
                            <td>{!! $item->created_at ?\Illuminate\Support\Carbon::parse($item->created_at): '&mdash;' !!}</td>
                            <td>{!! $item->updated_at ?\Illuminate\Support\Carbon::parse($item->updated_at): '&mdash;' !!}</td>
                            <td>
                                @if($item->status === \App\Modules\Export\Models\ExportPromUa::STATUS_DONE)
                                    {!! \App\Components\Buttons::delete('admin.export.destroy', $item->id) !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $list->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
