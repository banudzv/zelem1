@php
$title = 'Reset Password';
@endphp

@extends('site._layouts.account-no-menu')

@push('hidden_data')
    {!! $validation !!}
@endpush

@section('account-content')
    <div class="gcell gcell--12 gcell--md-9 gcell--def-6 _p-none _pt-lg _nmt-lg _mlr-auto">
        <div class="form form--password-change">
            {!! Form::open(['id' => $formId, 'route' => 'site.reset-password', 'method' => 'post', 'class' => ['js-init', 'ajax-form']]) !!}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form__body">
                <div class="grid">
                    <div class="gcell gcell--12 _pt-md">
                        <div class="grid">
                            @if(session('status'))
                                <div class="gcell gcell--12 _ptb-lg">
                                    @component('site._widgets.alert.alert', [
                                        'alert_type' => 'danger',
                                    ])
                                        <div>{{ session('status') }}</div>
                                    @endcomponent
                                </div>
                            @endif
                            <div class="gcell gcell--12 _ptb-md">
                                <div class="control control--input">
                                    <div class="control__inner">
                                        <input class="control__field" type="email" name="email" id="profile-email" value="{{ old('email') }}">
                                        <label class="control__label" for="profile-email">{{ __('users::site.seo.your-email') }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="gcell gcell--12 _ptb-md">
                                <div class="control control--input">
                                    <div class="control__inner">
                                        <input class="control__field" type="password" name="password" id="profile-password">
                                        <label class="control__label" for="profile-password">{{ __('users::site.new-password') }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="gcell gcell--12 _pt-md _pb-lg">
                                <div class="control control--input">
                                    <div class="control__inner">
                                        <input class="control__field" type="password" name="password_confirmation" id="profile-password_confirmation">
                                        <label class="control__label" for="profile-password_confirmation">{{ __('users::site.repeat-password') }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="gcell gcell--12">
                        <div class="grid _justify-center">
                            <div class="gcell gcell--9 gcell--ms-6 gcell--def-4 _pb-sm">
                                <div class="control control--submit">
                                    <button class="button button--theme-main button--size-normal button--width-full" type="submit">
                                        <span class="button__body">
                                            <span class="button__text">{{ __('users::site.seo.reset-password-button') }}</span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div class="gcell gcell--12 _pb-lg _pt-sm _text-center">
                                <a class="link" href="{{ route('site.register') }}">@lang('users::site.seo.registration')</a>
                                <a class="link" href="{{ route('site.login') }}">{{ __('users::site.seo.login') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
