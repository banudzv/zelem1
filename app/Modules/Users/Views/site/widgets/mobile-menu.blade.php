@php
/** @var \App\Components\SiteMenu\Group $menu */
@endphp

{{-- <ul id="user-account"> --}}
    @foreach($menu->getKids() as $link)
        <a class="mm-custom-link _flex _items-center" href="{{ $link->getUrl() }}">
            {!! SiteHelpers\SvgSpritemap::get($link->icon, [
                'class' => 'mm-menu-icon'
            ]) !!}
            <span>{{ __($link->name) }}</span>
        </a>
    @endforeach
        <a  class="mm-custom-link _flex _items-center" href="{{ route('site.logout') }}">
            {!! SiteHelpers\SvgSpritemap::get('icon-logout', [
                'class' => 'mm-menu-icon'
            ]) !!}
            <span>@lang('users::site.logout-from-the-site')</span>
        </a>
{{-- </ul> --}}
