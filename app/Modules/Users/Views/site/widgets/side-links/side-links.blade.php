@php
/** @var \App\Components\SiteMenu\Group $menu */
@endphp

<div class="side-links">
    @foreach($menu->getKids() as $link)
        <div>
            <a class="side-link {{ $link->isActive() ? 'is-active' : null }}" {{ $link-> isActive() ? null : 'href='.$link->getUrl() }}>@lang($link->name)</a>
{{--            {!! Html::link($link->isActive() ? null : $link->getUrl(), __($link->name), [--}}
{{--                'class' => ['side-link', $link->isActive() ? 'is-active' : null],--}}
{{--            ]) !!}--}}
        </div>
    @endforeach
</div>
