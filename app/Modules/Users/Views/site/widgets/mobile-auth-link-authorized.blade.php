<a class="mm-custom-link _flex _items-center mm-custom-link--separator-bottom" href="{{ route('site.account') }}">
    {!! \SiteHelpers\SvgSpritemap::get('icon-user-mobile-menu', ['class' => 'mm-menu-icon']) !!}<span>{{ \Auth::user()->first_name . ' ' . \Auth::user()->last_name }}</span>
</a>
{!! Widget::show('users::mobile-menu') !!}
