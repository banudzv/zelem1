<a class="mm-custom-link js-init _flex _items-center" data-mfp="inline" data-trigger-event="cabinetClick" data-mfp-src="#popup-regauth" {{--href="{{ route('site.login') }}"
   --}}>
    {!! \SiteHelpers\SvgSpritemap::get('icon-user-mobile-menu', ['class' => 'mm-menu-icon']) !!} <span> @lang('users::site.entrance-to-the-site')</span>
</a>
