@php
$title = trans('users::site.seo.registration');
@endphp

@extends('site._layouts.account-no-menu')

@section('account-content')
    <div class="section" style="width: 100%;">
        <div class="container">
            <div class="box box--gap">
                <div class="gcell gcell--12 gcell--ms-8 gcell--def-6 _mlr-auto _pt-md _p-none _md-p-def _def-p-lg">
                    {!! Widget::show('registration-form') !!}
                </div>
                <div class="gcell gcell--12 gcell--ms-8 gcell--def-6 _mlr-auto _pt-md _plr-none _md-plr-def _def-plr-lg">
                    <div class="grid _nmlr-sm _justify-center">
                        {!! Widget::show('social-networks') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
