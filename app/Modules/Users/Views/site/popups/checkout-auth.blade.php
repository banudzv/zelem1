@php($socials = Widget::show('social-networks'))
<div id="popup-login-only" class="popup popup--login-only _pb-xl">
    <div class="popup__container">
        <div class="wstabs-block is-active" data-wstabs-ns="regauth" data-wstabs-block="1">
            <div class="popup__head">
                <div class="grid _flex-nowrap">
                    <div class="gcell gcell--auto _flex-noshrink _pr-sm">
                        {!! SiteHelpers\SvgSpritemap::get('icon-user', [
                            'class' => 'svg-icon svg-icon--icon-user',
                        ]) !!}
                    </div>
                    <div class="gcell gcell--auto _flex-grow">
                        <div class="popup__title">@lang('users::site.sing-in')</div>
                        <div class="popup__desc">@lang('users::site.benefits-after-login')</div>
                    </div>
                </div>
            </div>
            <div class="popup__body">
                {!! Widget::show('login-form', true, true) !!}
            </div>
        </div>
        <div class="wstabs-block" data-wstabs-ns="regauth" data-wstabs-block="2">
            @include('users::site.popups.registration')
        </div>
        <div class="wstabs-block" data-wstabs-ns="regauth" data-wstabs-block="3">
            @include('users::site.popups.forgot-password')
        </div>
        @if($socials)
            <div class="popup__social-section">
                <div class="popup__container _mt-xl">
                    <div class="popup__desc _text-center _mb-def">@lang('users::site.login-by-social-networks')</div>
                    <div class="grid _justify-center _nmlr-sm">
                        {!! $socials !!}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
