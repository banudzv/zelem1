@php
    /** @var \CustomForm\Builder\Form $form */
    $form->buttons->showCloseButton(route('admin.users.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.users.exportLoad']) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop
