<?php

namespace App\Modules\Users\Models;

use App\Core\Modules\Settings\Models\Setting;
use App\Modules\Users\Events\ForgotPasswordEvent;
use App\Modules\Users\Filters\UsersFilter;
use Eloquent;
use EloquentFilter\Filterable;
use Event;
use Greabock\Tentacles\EloquentTentacle;
use Hash;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Throwable;

/**
 * App\Modules\Users\Models\User
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property bool $active
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $phone
 * @property-read string $cleared_phone
 * @property-read string $edit_page_link
 * @property-read string $name
 * @property-read Collection|UserNetwork[] $networks
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @method static Builder|User active()
 * @method static Builder|User filter($input = array(), $filter = null)
 * @method static bool|null forceDelete()
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|User query()
 * @method static bool|null restore()
 * @method static Builder|User simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|User where($column, $operator, $value = null)
 * @method static Builder|User whereActive($value)
 * @method static Builder|User whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLastName($value)
 * @method static Builder|User whereLike($column, $value, $boolean = 'and')
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhone($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User withTrashed()
 * @method static Builder|User withoutTrashed()
 * @method static Builder|User onlyTrashed()
 *
 * @mixin Eloquent
 * @property-read int|null $networks_count
 * @property-read int|null $notifications_count
 */
class User extends Authenticatable
{
    use EloquentTentacle;
    use Filterable;
    use Notifiable;
    use SoftDeletes;

    public const DEFAULT_USERS_LIMIT = 10;

    protected $casts = [
        'active' => 'boolean'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'password'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * Register new user
     *
     * @param array $attributes
     * @return User
     * @throws Throwable
     */
    public static function register(array $attributes = [])
    {
        $user = new User();
        $attributes['active'] = data_get($attributes, 'active', false);
        $user->updateInformation($attributes);
        return $user;
    }

    /**
     * Update user information
     *
     * @param array $attributes
     * @throws Throwable
     */
    public function updateInformation(array $attributes = [])
    {
        $this->first_name = data_get($attributes, 'first_name');
        $this->last_name = data_get($attributes, 'last_name');
        $this->email = data_get($attributes, 'email');
        $this->phone = data_get($attributes, 'phone');
        $this->active = data_get($attributes, 'active', $this->active);
        if (isset($attributes['password']) && $attributes['password']) {
            $this->password = $attributes['password'];
        }
        $this->saveOrFail();
    }

    public static function getArrayForExport(): array
    {
        $array = [];
        User::query()
            ->active()
            ->select(['first_name', 'last_name', 'email', 'phone'])
            ->where('id', '>', 0)
            ->latest('id')
            ->get()
            ->each(
                function (User $user) use (&$array) {
                    $array[] = [
                        'fio' => $user->first_name . ' ' . $user->last_name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                    ];
                }
            );
        return $array;
    }

    /**
     * Trashed users list
     *
     * @return LengthAwarePaginator
     */
    public static function trashedList()
    {
        return User::forList(true);
    }

    /**
     * Get users list to show
     *
     * @param bool $trashed
     * @return LengthAwarePaginator
     */
    public static function forList($trashed = false)
    {
        $users = User::filter(request()->all())->where('id', '>', 0);
        if ($trashed === true) {
            $users->onlyTrashed();
        }
        return $users->latest('id')->paginate(config('db.users.per-page', static::DEFAULT_USERS_LIMIT));
    }

    /**
     * List uf pairs user_id => user_name
     *
     * @return User[]|Collection
     */
    public static function dictionary()
    {
        return User::all()->mapWithKeys(
            function (User $user) {
                return [$user->id => '[' . $user->id . '] ' . $user->name];
            }
        )->toArray();
    }

    /**
     * Available users list to choose by admin
     *
     * @return User[]|Collection
     */
    public static function available()
    {
        return User::all();
    }

    /**
     * @return array
     */
    public static function getSettingsForSocialsLogin(): array
    {
        $socialSettings = Setting::whereGroup('socials-login')->get()->keyBy('alias')->map(
            function ($setting) {
                return $setting->value;
            }
        )->toArray();
        $checkSocials = [];
        foreach ((array)config('users.socials', []) as $socialName => $setting) {
            $apiIndex = $socialName . '-api-key';
            $secretIndex = $socialName . '-api-secret';
            if (Arr::get($socialSettings, $apiIndex) && Arr::get($socialSettings, $secretIndex)) {
                $checkSocials[$socialName] = [
                    $apiIndex => $socialSettings[$apiIndex],
                    $secretIndex => $socialSettings[$secretIndex],
                ];
            }
        }
        return $checkSocials;
    }

    public function sendPasswordResetNotification($token)
    {
        Event::dispatch(new ForgotPasswordEvent($this->email, $token));
    }

    public function networks()
    {
        return $this->hasMany(UserNetwork::class, 'user_id', 'id');
    }

    public function modelFilter()
    {
        return $this->provideFilter(UsersFilter::class);
    }

    /**
     * Update users password
     *
     * @param  $password
     * @return bool
     * @throws Throwable
     */
    public function updatePassword(string $password)
    {
        $this->password = $password;
        return $this->save();
    }

    public function updatePhone(string $phone)
    {
        $this->phone = $phone;
        return $this->save();
    }

    /**
     * Password mutator
     *
     * @param string $password
     */
    public function setPasswordAttribute(string $password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', true);
    }

    /**
     * Form name from last and first names
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return trim($this->first_name . ' ' . $this->last_name);
    }

    /**
     * Link to edit page in admin panel
     *
     * @return string
     */
    public function getEditPageLinkAttribute(): string
    {
        return route('admin.users.edit', $this->id);
    }

    /**
     * Phone number without anything except numbers
     *
     * @return string
     */
    public function getClearedPhoneAttribute(): string
    {
        return preg_replace('/[^0-9]*/', '', $this->phone);
    }

}
