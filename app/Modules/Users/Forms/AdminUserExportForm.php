<?php

namespace App\Modules\Users\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\Users\Models\User;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminUserExportForm
 *
 * @package App\Core\Modules\Administrators\Forms
 */
class AdminUserExportForm implements FormInterface
{

    /**
     * @param  Model|User|null $user
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $user = null): Form
    {
        $form = Form::create();
        // Field set without languages tabs
        $form->fieldSet()->add(
            Select::create('extension')
                ->add(
                    [
                        'xls' => 'xls',
                        'xlsx' => 'xlsx',
                        'csv' => 'csv',
                    ]
                )
                ->setValue(request('extension'))
                ->addClassesToDiv('col-md-4'),
            Input::create('quote')->setValue(request('extension') ?: '"')->addClassesToDiv('col-md-4'),
            Input::create('coma')->setValue(request('extension') ?: ',')->addClassesToDiv('col-md-4')
        );
        $form->buttons->doNotShowCloseButton();
        $form->buttons->doNotShowSaveAndAddButton();
        $form->buttons->doNotShowSaveAndCloseButton();
        $form->doNotShowBottomButtons();
        return $form;
    }

}
