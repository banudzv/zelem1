<?php

namespace App\Modules\Contact\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;
use Config;

/**
 * Class ContactBlockRequest
 * @package App\Modules\Contact\Requests
 */
class ContactBlockRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;
    
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * @return array
     * @throws \App\Exceptions\WrongParametersException
     */
    public function rules(): array
    {
        return $this->generateRules([
            'active' => ['required', 'boolean'],
        ], [
            'title' => ['required', 'max:191'],
            'text' => ['nullable', 'max:191']
        ]);
    }

    /**
     * @return array
     */
    public function attributes()
    {
        $attributes = [];
        foreach (Config::get('languages') as $key => $lang) {
            $attributes[$key.'.title'] = trans('contact::blocks.form-attributes.title');
        }

        return $attributes;
    }
    
}
