<?php

namespace App\Modules\Contact\Requests;

use App\Core\Interfaces\RequestInterface;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SiteCallbackRequest
 *
 * @package App\Core\Modules\Administrators\Requests
 */
class SiteContactRequest extends FormRequest implements RequestInterface
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $request = [
            'name' => ['required', 'string', 'min:3', 'max:191', 'regex:/^[а-яА-Яa-zA-zёЁіІїЇ ]{3,191}$/u'],
            'email' => ['required', 'email'],
            'phone' => ['required', 'regex:/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/', 'string', 'max:191'],
            'message' => ['required', 'string', 'min:5', 'max:191'],
        ];
        if(\Config::get('db.basic.agreement_show')) {
            $request['personal-data-processing'] =  ['required', 'in:on'];
        }
        return $request;
    }

    public function attributes()
    {
        return [
            'name' => trans('contact::site.name'),
            'message' => trans('contact::site.message')
        ];
    }

    public function messages(): array
    {
        return [
            'name.regex' => trans('contact::site.attributes.name.format')
        ];
    }

}
