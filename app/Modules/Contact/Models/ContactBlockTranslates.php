<?php

namespace App\Modules\Contact\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Modules\Contact\Models\ContactBlockTranslates
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string|null $content
 * @property int $row_id
 * @property string $language
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\Contact\Models\ContactBlock $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlockTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlockTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlockTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlockTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlockTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlockTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlockTranslates whereRowId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlockTranslates whereTitle($value)
 * @mixin \Eloquent
 */
class ContactBlockTranslates extends Model
{
    use ModelTranslates;
    
    protected $table = 'contact_blocks_translates';
    
    public $timestamps = false;
    
    protected $fillable = ['title', 'text', 'content'];
    
    protected $hidden = ['id', 'row_id'];
}
