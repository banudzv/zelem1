<?php

namespace App\Modules\Contact\Models;

use App\Modules\Contact\Filters\ContactFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\Contact\Models\Contact
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $phone
 * @property string $message
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\Contact whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Contact extends Model
{
    use Filterable;

    protected $table = 'contacts';

    protected $casts = ['active' => 'boolean'];
    
    protected $fillable = ['name', 'email', 'phone', 'active', 'message'];

    /**
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(ContactFilter::class);
    }
    
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList()
    {
        return Contact::filter(request()->all())
            ->latest()
            ->paginate(config('db.contact.per-page', 10));
    }
}
