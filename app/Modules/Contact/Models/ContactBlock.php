<?php

namespace App\Modules\Contact\Models;

use App\Traits\ActiveScopeTrait;
use App\Traits\ModelMain;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Model;
use Widget;

/**
 * App\Modules\Contact\Models\ContactBlock
 *
 * @property int $id
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Modules\Contact\Models\ContactBlockTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Contact\Models\ContactBlockTranslates[] $data
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Contact\Models\ContactBlock active($active = true)
 */
class ContactBlock extends Model
{
    use ModelMain, EloquentTentacle, ActiveScopeTrait;
    
    protected $casts = ['active' => 'boolean'];
    
    protected $fillable = ['active', 'icon', 'position'];
    
    protected $hidden = ['created_at', 'updated_at'];
    
    /**
     * Get list of news
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList()
    {
        return ContactBlock::with(['current'])
            ->oldest('position')
            ->oldest('id')
            ->get();
    }
}
