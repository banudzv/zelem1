<?php

namespace App\Modules\Contact\Controllers\Site;

use App\Core\AjaxTrait;
use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\SiteController;
use App\Modules\Contact\Models\Contact;
use App\Modules\Contact\Models\ContactBlock;
use App\Modules\Contact\Requests\SiteContactRequest;
use App\Modules\Dictionaries\Branches\Models\Branch;
use Catalog;

/**
 * Class ContactController
 *
 * @package App\Modules\Contact\Controllers\Site
 */
class ContactController extends SiteController
{
    use AjaxTrait;

    public function index()
    {
        /** @var SystemPage $page */
        $page = SystemPage::getByCurrent('slug', 'contact');
        $this->meta($page->current);
        $this->breadcrumb($page->current->name, 'site.contact', [$page->current->slug]);
        $this->canonical(route('site.contact'));
        $blocks = ContactBlock::with('current')
            ->active()
            ->oldest('position')
            ->get();

        return view('contact::site.index', [
            'page' => $page,
            'blocks' => $blocks,
            'branches' => Branch::getListForClientSide()
        ]);
    }

    /**
     * @param SiteContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function send(SiteContactRequest $request)
    {
        $contact = new Contact();
        $contact->fill($request->all());
        if ($contact->save()) {
            event('contact::created', $contact);
            return $this->successMfpMessage(trans('contact::general.message-success'), [
                'reset' => true,
                'dataLayer' => Catalog::ecommerce()->getFormDataLayer('contact')
            ]);
        }
        return $this->errorJsonAnswer([
            'notyMessage' => trans('contact::general.message-false'),
        ]);
    }
    
}
