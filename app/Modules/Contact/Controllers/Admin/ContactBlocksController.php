<?php

namespace App\Modules\Contact\Controllers\Admin;

use App\Core\ObjectValues\RouteObjectValue;
use App\Core\AdminController;
use App\Modules\Contact\Forms\ContactBlockForm;
use App\Modules\Contact\Models\ContactBlock;
use App\Modules\Contact\Requests\ContactBlockRequest;
use Seo;

/**
 * Class ContactBlocksController
 *
 * @package App\Modules\Contact\Controllers\Admin
 */
class ContactBlocksController extends AdminController
{
    
    public function __construct()
    {
        Seo::breadcrumbs()->add('contact::seo.index', RouteObjectValue::make('admin.contact-blocks.index'));
    }
    
    /**
     * Register widgets with buttons
     */
    private function registerButtons()
    {
        // Create new block button
        $this->addCreateButton('admin.contact-blocks.create');
    }
    
    /**
     * Blocks sortable list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // Set page buttons on the top of the page
        $this->registerButtons();
        // Set h1
        Seo::meta()->setH1('contact::seo.index');
        Seo::meta()->setCountOfEntity(trans('contact::seo.blocks.count',['count' => ContactBlock::count(['id'])]));
        // Get blocks
        $contactBlocks = ContactBlock::getList();
        // Return view list
        return view('contact::admin.blocks.index', ['blocks' => $contactBlocks]);
    }
    
    /**
     * Create new contact-block element
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function create()
    {
        // Breadcrumb
        Seo::breadcrumbs()->add('contact::seo.blocks.create');
        // Set h1
        Seo::meta()->setH1('contact::seo.blocks.create');
        // Javascript validation
        $this->initValidation(
            (new ContactBlockRequest())->rules(),
            null,
            [],
            (new ContactBlockRequest())->attributes()
        );
        // Return form view
        return view('contact::admin.blocks.create', [
            'form' => ContactBlockForm::make(),
        ]);
    }
    
    /**
     * Create contact block in database
     *
     * @param  ContactBlockRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(ContactBlockRequest $request)
    {
        $block = (new ContactBlock());
        // Create new block
        if ($message = $block->createRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterStore(['id' => $block->id]);
    }
    
    /**
     * Update element page
     *
     * @param  ContactBlock $block
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(ContactBlock $block)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($block->current->title ?? 'contact::seo.blocks.edit');
        // Set h1
        Seo::meta()->setH1('contact::seo.blocks.edit');
        // Javascript validation
        $this->initValidation(
            (new ContactBlockRequest())->rules(),
            null,
            [],
            (new ContactBlockRequest())->attributes()
        );
        // Return form view
        return view('contact::admin.blocks.update', [
            'form' => ContactBlockForm::make($block),
        ]);
    }
    
    /**
     * Update block in database
     *
     * @param  ContactBlockRequest $request
     * @param  ContactBlock $block
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(ContactBlockRequest $request, ContactBlock $block)
    {
        // Update existed block
        if ($message = $block->updateRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterUpdate();
    }
    
    /**
     * Totally delete block from database
     *
     * @param  ContactBlock $block
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(ContactBlock $block)
    {
        // Delete block
        $block->forceDelete();
        // Do something
        return $this->afterDestroy();
    }
    
}
