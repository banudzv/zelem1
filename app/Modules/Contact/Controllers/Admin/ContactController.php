<?php

namespace App\Modules\Contact\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Contact\Filters\ContactFilter;
use App\Modules\Contact\Forms\AdminContactForm;
use App\Modules\Contact\Models\Contact;
use App\Modules\Contact\Requests\AdminContactRequest;
use Seo;

/**
 * Class contactController
 *
 * @package App\Modules\contact\Controllers\Admin
 */
class ContactController extends AdminController
{
    
    public function __construct()
    {
        Seo::breadcrumbs()->add('contact::seo.index', RouteObjectValue::make('admin.contact.index'));
    }

    /**
     * Contact sortable list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function index()
    {
        // Set h1
        Seo::meta()->setH1('contact::seo.index');
        Seo::meta()->setCountOfEntity(trans('contact::seo.count',['count' => Contact::count(['id'])]));
        // Get contact
        $contact = Contact::getList();
        // Return view list
        return view('contact::admin.index', [
            'contact' => $contact,
            'filter' => ContactFilter::showFilter()
        ]);
    }
    
    /**
     * Update element page
     *
     * @param  Contact $contact
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(Contact $contact)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($contact->name ?? 'contact::seo.edit');
        // Set h1
        Seo::meta()->setH1('contact::seo.edit');
        // Javascript validation
        $this->initValidation((new AdminContactRequest())->rules());
        // Return form view
        return view('contact::admin.update', [
            'form' => AdminContactForm::make($contact),
        ]);
    }
    
    /**
     * Update page in database
     *
     * @param  AdminContactRequest $request
     * @param  Contact $contact
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(AdminContactRequest $request, Contact $contact)
    {
        // Fill new data
        $contact->fill($request->all());
        // Update existed page
        $contact->save();
        // Do something
        return $this->afterUpdate();
    }
    
    /**
     * Totally delete page from database
     *
     * @param  Contact $contact
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(Contact $contact)
    {
        // Delete contact
        $contact->forceDelete();
        // Do something
        return $this->afterDestroy();
    }
    
}
