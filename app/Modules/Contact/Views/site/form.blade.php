@push('scripts')
    {!! $validation !!}
@endpush

{!! Form::open(['route' => 'contact-send', 'class' => ['js-init', 'ajax-form'], 'id' => $formId]) !!}
<div class="form__body _md-plr-lg">
    <div class="grid _nmtb-sm">
        <div class="gcell gcell--12 _ptb-md">
            <div class="control control--input {{ $name ? 'has-value' : null }}">
                <div class="control__inner">
                    <input class="control__field" type="text" name="name" id="contacts-name" required value="{{ $name }}">
                    <label class="control__label" for="name">@lang('contact::site.name') *</label>
                </div>
            </div>
        </div>
        <div class="grid gcell--12 _spacer-lg">
            <div class="gcell gcell--12 gcell--ms-6 _pr-none _ms-pr-sm _ptb-md">
                <div class="control control--input {{ $email ? 'has-value' : null }}">
                    <div class="control__inner">
                        <input class="control__field js-init" type="email" name="email" id="contacts-email" data-emailmask required value="{{ $email }}">
                        <label class="control__label" for="email">@lang('contact::site.email') *</label>
                    </div>
                </div>
            </div>
            <div class="gcell gcell--12 gcell--ms-6 _pl-none _ms-pl-sm _ptb-md">
                <div class="control control--input {{ $phone ? 'has-value' : null }}">
                    <div class="control__inner">
                        <input class="control__field js-init" type="tel" name="phone" id="contacts-phone" data-phonemask value="{{ $phone }}">
                        <label class="control__label" for="phone">@lang('contact::site.phone') *</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="gcell gcell--12 _ptb-md">
            <div class="control control--textarea">
                <div class="control__inner">
                    <textarea class="control__field control__field" type="text" name="message" id="contact-message" required></textarea>
                    <label class="control__label" for="message">@lang('contact::site.message') *</label>
                </div>
            </div>
        </div>
        @if(\Config::get('db.basic.agreement_show'))
            <div class="gcell gcell--12 _pt-lg _pb-sm _hide">
                @component('site._widgets.checker.checker', [
                    'attributes' => [
                        'type' => 'checkbox',
                        'name' => 'personal-data-processing',
                        'required' => true,
                        'checked' => true
                    ]
                ])
                    * @lang('contact::site.agreement') →
                    <a href="{{ config('db.basic.agreement_link') }}" target="_blank">@lang('global.more')</a>
                    <label id="personal-data-processing-error" class="has-error" for="personal-data-processing" style="display: none;"></label>
                @endcomponent
            </div>
        @endif
    </div>
</div>
<div class="form__footer">
    <div class="_flex _justify-center _pt-def">
        <div class="control control--submit">
            <button class="button button--theme-main button--size-form" type="submit">
                <span class="button__body">
                    <span class="button__text">@lang('contact::site.order')</span>
                </span>
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
