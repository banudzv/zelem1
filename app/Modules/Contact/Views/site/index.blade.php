@php
    /** @var \App\Core\Modules\SystemPages\Models\SystemPage $page */
    /** @var App\Modules\Contact\Models\ContactBlock[]|Illuminate\Database\Eloquent\Collection $blocks */
    /** @var App\Modules\Dictionaries\Branches\Models\Branch[]|Illuminate\Database\Eloquent\Collection $branches */
@endphp

@extends('site._layouts.main')

@push('expandedCatalog', false)

@push('microdata')
    {!! Widget::show('contact::ld+json') !!}
@endpush

@section('layout-body')
<div class="section _mb-def">
    <div class="container container--small container--white">
        <div class="contact-cards grid grid--1 grid--sm-2 grid--def-4">
            @foreach($blocks as $block)
                <div class="gcell">
                    <div class="contact-cards__item">
                        <div class="contact-cards__item-icon">
                            @if($block->icon)
                                {!! r2d2()->svgSymbol(
                                    $block->icon,
                                    [],
                                    asset('assets/svg/spritemap.svg')) !!}
                            @endif
                        </div>
                        <div class="contact-cards__item-title">
                            {{ $block->current->title }}
                        </div>
                        @if($block->current->text)
                            <div class="contact-cards__item-text">
                                {!! $block->current->text !!}
                            </div>
                        @endif
                        @if($block->current->content)
                            {!! $block->current->content !!}
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
        <article class="wysiwyg  _ptb-xl">
            <div class="scroll-text">
                <div class="scroll-text__content wysiwyg js-init contacts__text-block"
                     data-wrap-media
                     data-prismjs
                     data-draggable-table
                     data-perfect-scrollbar>
                    {!! $page->current->content !!}
                </div>
            </div>
        </article>
        @if($branches)
            <div class="wstabs contacts__tabs js-init" data-product-tabs>
                <div class="contacts__tabs-nav">
                    @foreach ($branches as $branch)
                        <div class="contacts__tabs-nav-item {{ $loop->first ? 'is-active' : '' }}"
                             data-wstabs-ns="mapTabs"
                             data-wstabs-button="{{ $loop->index }}"
                        >
                            {{ $branch->current->name }}
                        </div>
                    @endforeach
                </div>
                <div class="contacts__tabs-content">
                    @foreach ($branches as $branch)
                        <div class="contacts__tabs-block {{ $loop->first ? 'is-active' : '' }}"
                             data-wstabs-ns="mapTabs"
                             data-wstabs-block="{{ $loop->index }}">
                            <iframe src="https://maps.google.com/maps?q={{ $branch->getCoordinatesString() }}&amp;output=embed" frameborder="0"></iframe>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="contacts__form">
            <div class="container container--md">
                <div class="title title--size-h3 _text-center _mb-xl">
                    @lang('contact::site.form-title')
                </div>
                {!! Widget::show('contact::contact-form') !!}
            </div>
        </div>
    </div>
</div>

@endsection
