<script type="application/ld+json">/*<![CDATA[*/
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "{{ $name }}",
  "image": "{{ $image }}",
  "@id": "{{ $id }}",
  "url": "{{ $url }}",
  "telephone": "{{ $phone }}",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "{{ $street }}",
    "addressLocality": "{{ $city }}",
    "addressCountry": "UA"
  },
  "openingHoursSpecification": [{
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday"
    ],
    "opens": "09:00",
    "closes": "18:00"
  },{
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": "Saturday",
    "opens": "10:00",
    "closes": "14:00"
  }]
}
/*]]>*/</script>