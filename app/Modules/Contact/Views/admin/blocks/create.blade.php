@php
    /** @var \CustomForm\Builder\Form $form */
    $form->buttons->showCloseButton(route('admin.contact-blocks.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.contact-blocks.store']) !!}
        {!! $form->render() !!}
    {!! Form::close() !!}
@stop
