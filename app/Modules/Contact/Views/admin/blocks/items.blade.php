@php
    /**
     * @var \App\Modules\Contact\Models\ContactBlock[]|\Illuminate\Support\Collection $blocks
     */
@endphp

@foreach ($blocks as $block)
    <li class="dd-item dd3-item" data-id="{{ $block->id }}">
        <div title="Drag" class="dd-handle dd3-handle">Drag</div>
        <div class="dd3-content">
            <table style="width: 100%;">
                <tr>
                    <td class="column-drag" width="1%"></td>
                    <td valign="top" class="pagename-column">
                        <div class="clearFix">
                            <div class="pull-left">
                                <div class="overflow-20">
                                    <a class="pageLinkEdit"
                                       href="{{ route('admin.contact-blocks.edit', ['block' => $block->id]) }}">
                                        {{ $block->current->title }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td width="30" valign="top" class="icon-column status-column">
                        {!! Widget::active($block, 'admin.contact-blocks.active') !!}
                    </td>
                    <td class="nav-column icon-column" valign="top" width="100" align="right">
                        {!! \App\Components\Buttons::edit('admin.contact-blocks.edit', $block->id) !!}
                        {!! \App\Components\Buttons::delete('admin.contact-blocks.destroy', $block->id) !!}
                    </td>
                </tr>
            </table>
        </div>
    </li>
@endforeach
