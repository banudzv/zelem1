@php
    /** @var \App\Modules\Contact\Models\ContactBlock[]|\Illuminate\Support\Collection $blocks */
    $className = \App\Modules\Contact\Models\ContactBlock::class;
    $getParameters = request()->query();
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        <div class="dd pageList" id="myNest" data-depth="1">
            <ol class="dd-list">
                @include('contact::admin.blocks.items', ['blocks' => $blocks])
            </ol>
        </div>
        <span id="parameters"
              data-url="{{ route('admin.contact-blocks.sortable', [
                    'class' => $className,
                    'query' => $getParameters
              ]) }}"></span>
        <input type="hidden" id="myNestJson">
    </div>
@stop
