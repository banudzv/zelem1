<?php

namespace App\Modules\Contact\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Contact\Requests\SiteContactRequest;
use Auth;
use JsValidator;

/**
 * Class ContactForm
 *
 * @package App\Modules\Contact\Widgets
 */
class ContactForm implements AbstractWidget
{
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $formId = uniqid('contact-form');
        $validation = JsValidator::make(
            (new SiteContactRequest())->rules(),
            (new SiteContactRequest())->messages(),
            (new SiteContactRequest())->attributes(),
            '#' . $formId
        );

        return view('contact::site.form', [
            'formId' => $formId,
            'validation' => $validation,
            'name' => Auth::check() ? Auth::user()->name : null,
            'email' => Auth::check() ? Auth::user()->email : null,
            'phone' => Auth::check() ? Auth::user()->phone : null,
        ]);
    }
    
}
