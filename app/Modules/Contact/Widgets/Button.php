<?php

namespace App\Modules\Contact\Widgets;

use App\Components\Widget\AbstractWidget;

/**
 * Class Button
 *
 * @package App\Modules\Contact\Widgets
 */
class Button implements AbstractWidget
{
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        return view('contact::site.button');
    }
    
}
