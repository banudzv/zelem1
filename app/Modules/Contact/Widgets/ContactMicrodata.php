<?php

namespace App\Modules\Contact\Widgets;

use App\Components\Widget\AbstractWidget;

/**
 * Class ContactMicrodata
 *
 * @package App\Modules\Contact\Widgets
 */
class ContactMicrodata implements AbstractWidget
{
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $_theme = 'dark';

        return view('contact::site.microdata', [
            'name' => config('db.basic.company_'.\Lang::getLocale(), config('app.name')),
            'image' => route('site.home') . '/resources/assets/site/svg/logo-zalem-' . $_theme . '.svg',
            'id' => route('site.home'),
            'url' => route('site.contact'),
            'phone' => preg_replace('/\D/', '', config('db.basic.phone_number_1')),
            'street' => config('db.basic.address_street_' . \Lang::getLocale()),
            'city' => config('db.basic.address_city_' . \Lang::getLocale())
        ]);
    }
    
}
