<?php

namespace App\Modules\Contact\Database\Seeds;

use App\Core\Modules\Languages\Models\Language;
use App\Core\Modules\Mail\Models\MailTemplate;
use App\Core\Modules\Mail\Models\MailTemplateTranslates;
use Illuminate\Database\Seeder;

class MailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $template = new MailTemplate();
        $template->name = 'contact::general.mail-templates.names.contact-admin';
        $template->alias = 'contact-admin';
        $template->variables = [
            'name' => 'contact::general.mail-templates.attributes.name',
            'email' => 'contact::general.mail-templates.attributes.email',
            'phone' => 'contact::general.mail-templates.attributes.phone',
            'message' => 'contact::general.mail-templates.attributes.message',
            'admin_href' => 'contact::general.mail-templates.attributes.admin_href',
        ];
        $template->save();
    
        Language::all()->each(function (Language $language) use ($template) {
            $translate = new MailTemplateTranslates();
            $translate->language = $language->slug;
            $translate->row_id = $template->id;
            $translate->subject = 'New contact';
            $translate->text = 'You have new contact request. User name {name} email {email}';
            $translate->save();
        });
    }
}
