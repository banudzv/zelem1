<?php

namespace App\Modules\Contact\Database\Seeds;

use App\Core\Modules\Languages\Models\Language;
use App\Modules\Contact\Models\ContactBlock;
use App\Modules\Contact\Models\ContactBlockTranslates;
use DB;
use Exception;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Log;

class ContactBlocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->blocks() as $block) {
            DB::beginTransaction();

            try {
                $block['active'] = true;
                $newBlock = ContactBlock::firstOrCreate(
                    ['icon' => $block['icon']],
                    Arr::except($block, ['languages'])
                );

                Language::all()->each(
                    function (Language $language) use ($block, $newBlock) {
                        $blockTranslates = $block['languages'][$language->slug]
                            ?? $block['languages']['ru'];
                        $blockTranslates['row_id'] = $newBlock->id;
                        $blockTranslates['language'] = $language->slug;

                        ContactBlockTranslates::firstOrCreate(
                            [
                                'row_id' => $blockTranslates['row_id'],
                                'language' => $blockTranslates['language']
                            ],
                            $blockTranslates
                        );
                    }
                );
                DB::commit();
            } catch (Exception $exception) {
                DB::rollBack();
                Log::error($exception->getMessage());
            }
        }
    }

    protected function blocks()
    {
        return [
            [
                'icon' => 'contact-2',
                'languages' => [
                    'ua' => [
                        'title' => 'Телефонуйте',
                        'text' => '+38 044 355 26 71<br>0800-000-00-00',
                        'content' => ''
                    ],
                    'ru' => [
                        'title' => 'Звоните',
                        'text' => '+38 044 355 26 71<br>0800-000-00-00',
                        'content' => ''
                    ]
                ]
            ],
            [
                'icon' => 'contact-4',
                'languages' => [
                    'ua' => [
                        'title' => 'Пишіть',
                        'text' => '<a href="mailto:postturbosto@gmail.com" class="contact-cards__item-text contact-cards__item-text--underline">postturbosto@gmail.com</a>',
                        'content' => ''
                    ],
                    'ru' => [
                        'title' => 'Пишите',
                        'text' => '<a href="mailto:postturbosto@gmail.com" class="contact-cards__item-text contact-cards__item-text--underline">postturbosto@gmail.com</a>',
                        'content' => ''
                    ]
                ]
            ],
            [
                'icon' => 'contact-1',
                'languages' => [
                    'ua' => [
                        'title' => 'Розклад роботи',
                        'text' => 'Пн-Пт: з 9:00 до 18:00<br>Сб: з 10:00 до 14:00',
                        'content' => ''
                    ],
                    'ru' => [
                        'title' => 'Расписание работы',
                        'text' => 'Пн-Пт: с 9:00 до 18:00<br>Сб: с 10:00 до 14:00',
                        'content' => ''
                    ]
                ]
            ],
            [
                'icon' => 'contact-3',
                'languages' => [
                    'ua' => [
                        'title' => 'СТО та Філії',
                        'text' => 'Ми працюємо по всій Україні',
                        'content' => '<a href="javascript:void(0)" class="contact-cards__item-text contact-cards__item-text--underline contact-cards__item-text--icon contact-cards__item-text--semibold">Наші СТО та філії<img src="/resources/assets/site/svg/arrow-right.svg" class="contact-cards__text-icon contact-cards__text-icon--right" alt=""></a>'
                    ],
                    'ru' => [
                        'title' => 'СТО и Филиалы',
                        'text' => 'Мы работаем по всей Украине',
                        'content' => '<a href="javascript:void(0)" class="contact-cards__item-text contact-cards__item-text--underline contact-cards__item-text--icon contact-cards__item-text--semibold">Наши СТО и филиалы<img src="/resources/assets/site/svg/arrow-right.svg" class="contact-cards__text-icon contact-cards__text-icon--right" alt=""></a>'
                    ]
                ]
            ]
        ];
    }
}
