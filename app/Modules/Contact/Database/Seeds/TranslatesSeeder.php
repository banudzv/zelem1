<?php

namespace App\Modules\Contact\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'contact';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'Контактная форма',
                ],
                [
                    'name' => 'general.blocks-menu',
                    'ru' => 'Блоки страницы контактов',
                ],
                [
                    'name' => 'general.status',
                    'ru' => 'Обработан',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Запросы из контактной формы',
                ],
                [
                    'name' => 'seo.blocks.index',
                    'ru' => 'Блоки страницы контактов',
                ],
                [
                    'name' => 'seo.blocks.create',
                    'ru' => 'Создание блока',
                ],
                [
                    'name' => 'seo.blocks.edit',
                    'ru' => 'Редактирование блока',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование запроса из контактной формы',
                ],
                [
                    'name' => 'general.message',
                    'ru' => 'Обращение',
                ],
                [
                    'name' => 'settings.group-name',
                    'ru' => 'Страница "Контакты" и ее форма',
                ],
                [
                    'name' => 'settings.attributes.per-page',
                    'ru' => 'Количество записей на странице в админ панели',
                ],
                [
                    'name' => 'settings.attributes.googlemaps',
                    'ru' => 'Ключ к GoogleMaps API',
                ],
                [
                    'name' => 'settings.attributes.googlemaps-help',
                    'ru' => 'При отсутствии ключа карта на странице "Контакты" не будет выведена',
                ],
                [
                    'name' => 'general.notification',
                    'ru' => 'Запрос из контактной формы',
                ],
                [
                    'name' => 'seo.count',
                    'ru' => 'Количество запросов из контактной формы - :count',
                ],
                [
                    'name' => 'seo.blocks.count',
                    'ru' => 'Количество блоков - :count',
                ],
                [
                    'name' => 'blocks.form-attributes.title',
                    'ru' => 'Заголовок',
                ],
                [
                    'name' => 'blocks.form-attributes.text',
                    'ru' => 'Текст',
                ],
                [
                    'name' => 'blocks.form-attributes.content',
                    'ru' => 'Содержимое блока',
                ],
                [
                    'name' => 'blocks.form-attributes.icon',
                    'ru' => 'Иконка',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.we-will-call-you-back',
                    'ru' => 'мы свяжемся с вами в ближайшее время',
                ],
                [
                    'name' => 'site.order',
                    'ru' => 'Отправить',
                ],
                [
                    'name' => 'site.name',
                    'ru' => 'Ваше ФИО',
                ],
                [
                    'name' => 'site.email',
                    'ru' => 'Email',
                ],
                [
                    'name' => 'site.phone',
                    'ru' => 'Ваш номер телефона',
                ],
                [
                    'name' => 'site.agreement',
                    'ru' => 'Я согласен на обработку моих данных.',
                ],
                [
                    'name' => 'site.required',
                    'ru' => 'Вы должны заполнить все обязательные поля',
                ],
                [
                    'name' => 'site.message',
                    'ru' => 'Ваше обращение',
                ],
                [
                    'name' => 'site.form-title',
                    'ru' => 'Форма обратной связи',
                ],
                [
                    'name' => 'general.message-success',
                    'ru' => 'Ваше сообщение успешно отправлено! Мы свяжемся с вами в ближайшее время',
                ],
                [
                    'name' => 'general.message-false',
                    'ru' => 'Не удалось отправить форму, перезагрузите страницу и повторите попытку',
                ],
                [
                    'name' => 'site.attributes.name.format',
                    'ru' => 'Поле может содержать только буквенные символы',
                ],
            ],
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
