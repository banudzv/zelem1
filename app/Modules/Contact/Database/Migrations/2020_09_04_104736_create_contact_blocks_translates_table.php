<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactBlocksTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_blocks_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('text')->nullable();
            $table->text('content')->nullable();
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);

            $table->foreign('language')->references('slug')->on('languages')
                ->index('contact_blocks_translates_language_languages_slug')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('contact_blocks')
                ->index('contact_blocks_translates_row_id_contact_blocks_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (app()->environment() !== 'production') {
            Schema::table('contact_blocks_translates', function (Blueprint $table) {
                $table->dropForeign('contact_blocks_translates_language_languages_slug');
                $table->dropForeign('contact_blocks_translates_row_id_contact_blocks_id');
            });
            Schema::dropIfExists('contact_blocks_translates');
        }
    }
}
