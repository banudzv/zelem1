<?php

use Illuminate\Support\Facades\Route;

// Frontend routes
Route::post('contact/send', ['as' => 'contact-send', 'uses' => 'ContactController@send']);
Route::get('contact', 'ContactController@index')->name('site.contact');
