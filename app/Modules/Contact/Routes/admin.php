<?php

use Illuminate\Support\Facades\Route;

// Only authenticated
Route::middleware(['auth:admin', 'permission:contact'])->group(
    function () {
        // Admins list in admin panel
        Route::put('contact/{contact}/active', ['uses' => 'ContactController@active', 'as' => 'admin.contact.active']);
        Route::get('contact/{contact}/destroy', ['uses' => 'ContactController@destroy', 'as' => 'admin.contact.destroy']);
        Route::resource('contact', 'ContactController')
            ->except('show', 'destroy', 'create', 'store')
            ->names('admin.contact');
    }
);

Route::middleware(['auth:admin', 'permission:contact-blocks'])->group(function () {
    // Admins list in admin panel
    Route::put('contact-blocks/{block}/active', [
        'uses' => 'ContactBlocksController@active',
        'as' => 'admin.contact-blocks.active'
    ]);
    Route::get('contact-blocks/{block}/destroy', [
        'uses' => 'ContactBlocksController@destroy',
        'as' => 'admin.contact-blocks.destroy'
    ]);
    Route::put('contact-blocks/sortable', [
        'uses' => 'ContactBlocksController@sortable',
        'as' => 'admin.contact-blocks.sortable',
    ]);
    Route::resource('contact-blocks', 'ContactBlocksController')
        ->except('show', 'destroy')
        ->names('admin.contact-blocks')
        ->parameters(['contact-blocks' => 'block']);
});