<?php

namespace App\Modules\Contact\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\Contact\Models\Contact;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\TextArea;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminContactForm
 *
 * @package App\Modules\Contact\Forms
 */
class AdminContactForm implements FormInterface
{
    
    /**
     * @param  Model|Contact|null $contact
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $contact = null): Form
    {
        $contact = $contact ?? new Contact;
        $form = Form::create();
        $form->buttons->doNotShowSaveAndAddButton();
        // Field set without languages tabs
        $form->fieldSet()->add(
            Toggle::create('active', $contact)->setLabel(__('contact::general.status'))->required(),
            Input::create('name', $contact)->setLabel(__('validation.attributes.first_name')),
            Input::create('email', $contact)->required(),
            Input::create('phone', $contact),
            TextArea::create('message', $contact)->setLabel(__('contact::general.message'))
        );
        return $form;
    }
    
}
