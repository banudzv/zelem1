<?php

namespace App\Modules\Contact\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\Contact\Models\ContactBlock;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\IconSelect;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\TextArea;
use CustomForm\TinyMce;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ContactBlockForm
 *
 * @package App\Modules\Contact\Forms
 */
class ContactBlockForm implements FormInterface
{

    /**
     * Make form
     *
     * @param Model|null $block
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Throwable
     */
    public static function make(?Model $block = null): Form
    {
        $block = $block ?? new ContactBlock();
        $form = Form::create();
        $form->fieldSet(4, FieldSet::COLOR_SUCCESS)->add(
            Toggle::create('active', $block)->required(),
            IconSelect::create('icon', $block)
                ->add(['&mdash;'])
                ->addSvgSprite('spritemap')
                ->setLabel('contact::blocks.form-attributes.icon')
        );
        $form->fieldSetForLang(8)->add(
            Input::create('title', $block)
                ->setLabel('contact::blocks.form-attributes.title')
                ->required(),
            TextArea::create('text', $block)
                ->setLabel('contact::blocks.form-attributes.text'),
            TinyMce::create('content', $block)
                ->setLabel('contact::blocks.form-attributes.content')
        );

        return $form;
    }

}
