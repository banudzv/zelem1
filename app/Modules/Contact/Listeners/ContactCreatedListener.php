<?php

namespace App\Modules\Contact\Listeners;

use App\Components\Mailer\MailSender;
use App\Core\Interfaces\ListenerInterface;
use App\Core\Modules\Mail\Models\MailTemplate;
use App\Modules\Contact\Models\Contact;

/**
 * Class ContactCreatedListener
 *
 * @package App\Modules\Contact\Listeners
 */
class ContactCreatedListener implements ListenerInterface
{
    public static function listens(): string
    {
        return 'contact::created';
    }
    
    /**
     * Handle the event.
     *
     * @param Contact $contact
     * @return void
     */
    public function handle(Contact $contact)
    {
        $this->sendMail($contact);
    }
    
    private function sendMail(Contact $contact): void
    {
        if (config('db.basic.admin_email')) {
            $template = MailTemplate::getTemplateByAlias('contact-admin');
            if ($template) {
                $from = [
                    '{name}',
                    '{email}',
                    '{phone}',
                    '{message}',
                    '{admin_href}',
                ];
                $to = [
                    $contact->name,
                    $contact->email,
                    $contact->phone,
                    $contact->message,
                    route('admin.contact.edit', ['id' => $contact->id])
                ];
                $subject = str_replace($from, $to, $template->current->subject);
                $body = str_replace($from, $to, $template->current->text);
                MailSender::send(config('db.basic.admin_email'), $subject, $body);
            }
            return;
        }
    }

}
