<?php

namespace App\Modules\Contact\Listeners;

use App\Components\Crm\Amo;
use App\Components\Crm\Bitrix;
use App\Core\Interfaces\ListenerInterface;
use App\Core\Modules\Notifications\Models\Notification;
use App\Modules\Contact\Models\Contact;

/**
 * Class NewContactOrderNotification
 *
 * @package App\Modules\Contact\Listeners
 */
class NewContactOrderNotification implements ListenerInterface
{

    const NOTIFICATION_TYPE = 'contact';

    const NOTIFICATION_ICON = 'fa fa-phone';

    public static function listens(): string
    {
        return 'contact::created';
    }

    /**
     * Handle the event.
     *
     * @param Contact $contact
     * @return void
     */
    public function handle(Contact $contact)
    {
        if (config('db.amo_crm.use_amo')) {
            $this->addInAmoCrm($contact);
        }
        $this->sendBitrix($contact);
        Notification::send(
            static::NOTIFICATION_TYPE,
            'contact::general.notification',
            'admin.contact.edit',
            ['id' => $contact->id]
        );
    }

    /**
     * @param Contact $contact
     */
    public function addInAmoCrm(Contact $contact)
    {
        $data = [];
        $data['formName'] = __('contact::general.notification');
        $data['name'] = $contact->name ?: $contact->phone;
        $data['email'] = $contact->email;
        $data['phone'] = $contact->phone;
        $data['text'] = $contact->message;
        $amo = new Amo();
        $amo->newLead($data);
    }

    /**
     * @param Contact $contact
     */
    public function sendBitrix(Contact $contact)
    {
        $data = [];
        $data['formName'] = __('contact::general.notification');
        $data['requestType'] = 'contact-form';
        $data['name'] = $contact->name;
        $data['email'] = $contact->email;
        $data['phone'] = $contact->phone;
        $data['text'] = $contact->message;

        $bitrix = new Bitrix();
        $bitrix->send($data);
    }
}
