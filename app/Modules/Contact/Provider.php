<?php

namespace App\Modules\Contact;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Contact\Models\Contact;
use App\Modules\Contact\Widgets\Button;
use App\Modules\Contact\Widgets\ContactForm;
use App\Modules\Contact\Widgets\ContactMicrodata;
use CustomForm\Hidden;
use CustomForm\Input;
use CustomForm\Macro\Map;
use CustomSettings, CustomRoles, CustomMenu, Widget;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Contact
 */
class Provider extends BaseProvider
{
    /**
    * Set custom presets
    */
    protected function presets()
    {
    }
    
    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register module configurable settings
        $settings = CustomSettings::createAndGet('contact', 'contact::settings.group-name');
        $settings->add(
            Input::create('per-page')
                ->setLabel('contact::settings.attributes.per-page')
                ->setDefaultValue(10)
                ->required(),
            ['required', 'integer', 'min:1', 'max:100']
        );
        $settings->add(
            Input::create('googlemaps')
                ->setHelp('contact::settings.attributes.googlemaps-help')
                ->setLabel('contact::settings.attributes.googlemaps'),
            ['nullable', 'string']
        );
        $settings->add(Map::create('map'));
        if (config('db.contact.googlemaps')) {
            $settings->add(Hidden::create('zoom')->setLabel(false)->setDefaultValue(config('db.contact.zoom')));
            $settings->add(Hidden::create('longitude')->setLabel(false)->setDefaultValue(config('db.contact.longitude')));
            $settings->add(Hidden::create('latitude')->setLabel(false)->setDefaultValue(config('db.contact.latitude')));
        }
        
        // Register left menu block
        CustomMenu::get()->group()->block('forms', 'contact::general.main-menu-block', 'fa fa-edit')
            ->link('contact::general.menu', RouteObjectValue::make('admin.contact.index'))
            ->addCounter(Contact::whereActive(false)->count(), 'bg-green')
            ->additionalRoutesForActiveDetect(RouteObjectValue::make('admin.contact.edit'));

        CustomMenu::get()->group()->block('content', 'articles::general.menu-block', 'fa fa-files-o')
            ->link('contact::general.blocks-menu', RouteObjectValue::make('admin.contact-blocks.index'))
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.contact-blocks.edit'),
                RouteObjectValue::make('admin.contact-blocks.create')
            );
        // Register role scopes
        CustomRoles::add('contact', 'contact::general.menu')->except(RoleRule::VIEW, RoleRule::STORE);
        CustomRoles::add('contact-blocks', 'contact::general.menu')->except(RoleRule::VIEW);
    }

    
    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot() {
        Widget::register(Button::class, 'contact-button');
        Widget::register(ContactForm::class, 'contact::contact-form');
        Widget::register(ContactMicrodata::class, 'contact::ld+json');
    }
    
}
