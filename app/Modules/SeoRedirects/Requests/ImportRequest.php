<?php

namespace App\Modules\SeoRedirects\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\Import\Rules\Extension;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ImportRequest
 *
 * @package App\Modules\Import\Requests
 */
class ImportRequest extends FormRequest implements RequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'import' => ['required', 'file', new Extension],
            'old_redirects' => ['required', Rule::in(['none', 'disable', 'delete'])],
        ];
    }
    
}
