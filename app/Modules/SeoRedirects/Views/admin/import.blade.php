@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['files' => true]) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop
