<?php

namespace App\Modules\SeoRedirects\Middleware;

use App\Modules\SeoRedirects\Models\SeoRedirect;
use Closure, Request;
use Illuminate\Support\Str;

class Seo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $redirect = $this->redirects();
        if ($redirect) {
            return redirect($redirect->link_to, $redirect->type);
        }
        $url = request()->fullUrl();
        $urlParts = explode('?', $url);
        if (preg_match('/\/page\/1$/', $urlParts[0])) {
            $url = Str::replaceLast('/page/1', '', $urlParts[0]);
            if (count($urlParts) > 1) {
                $url .= '?' . $urlParts[1];
            }
            return redirect($url, 302);
        }
        return $next($request);
    }

    /**
     * @return SeoRedirect|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    private function redirects()
    {
        $result = SeoRedirect::getWhereUrl(Request::getRequestUri());
        if ($result && $result->exists) {
            return $result;
        }
        return null;
    }
}
