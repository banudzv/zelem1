<?php

namespace App\Modules\SeoRedirects\Forms;

use App\Components\Parsers\Entry;
use App\Core\Interfaces\FormInterface;
use CustomForm\Builder\Form;
use CustomForm\Group\Group;
use CustomForm\Group\Radio;
use CustomForm\Input;
use CustomForm\Select;
use CustomForm\Text;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ImportForm
 *
 * @package App\Core\Modules\Import\Forms
 */
class ImportForm implements FormInterface
{
    
    /**
     * @param  Model|null $model
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $model = null): Form
    {
        $form = Form::create();
        // Field set with languages tabs
        $form->fieldSet()->add(
            Input::create('import')
                ->setLabel('Файл импорта .xlsx, .xls')
                ->setType('file')
                ->setOptions(['accept' => '.xlsx, .xls'])
                ->setHelp('Структура файла: 1. перенаправление с*; 2. Перенаправление на*; 3. Код перенаправления (301, 302...)')
                ->required(),
            Select::create('old_redirects')
                ->add([
                    'none' => 'Ничего не делать',
                    'disable' => 'Снять с публикации',
                    'delete' => 'Удалить',
                ])
                ->setLabel('Уже загруженные перенаправления')
                ->required()
        );
        $form->buttons->doNotShowSaveAndAddButton();
        $form->buttons->doNotShowSaveAndCloseButton();
        $form->doNotShowTopButtons();
        return $form;
    }
    
}
