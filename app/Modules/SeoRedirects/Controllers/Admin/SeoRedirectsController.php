<?php

namespace App\Modules\SeoRedirects\Controllers\Admin;

use App\Components\Parsers\PromUa\ParsedCategory;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\SeoRedirects\Filters\SeoRedirectsFilter;
use App\Modules\SeoRedirects\Forms\AdminSeoRedirectsForm;
use App\Modules\SeoRedirects\Forms\ImportForm;
use App\Modules\SeoRedirects\Models\SeoRedirect;
use App\Modules\SeoRedirects\Requests\AdminSeoRedirectsRequest;
use App\Modules\SeoRedirects\Requests\ImportRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Seo;
use App\Core\AdminController;

/**
 * Class SeoRedirectsController
 *
 * @package App\Modules\SeoRedirects\Controllers\Admin
 */
class SeoRedirectsController extends AdminController
{

    public function __construct()
    {
        Seo::breadcrumbs()->add('seo_redirects::seo.index', RouteObjectValue::make('admin.seo_redirects.index'));
    }
    
    /**
     * Register widgets with buttons
     *
     * @throws \Exception
     */
    private function registerButtons()
    {
        // Create new seoRedirects button
        $this->addCustomButton('admin.seo_redirects.import', [], __('seo_redirects::button.import'));
        $this->addCreateButton('admin.seo_redirects.create');
    }

    /**
     * SeoRedirects sortable list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function index()
    {
        // Set page buttons on the top of the page
        $this->registerButtons();
        // Set h1
        Seo::meta()->setH1('seo_redirects::seo.index');
        // Get seoRedirects
        $seoRedirects = SeoRedirect::getList();
        // Return view list
        return view('seo_redirects::admin.index', [
            'seoRedirects' => $seoRedirects,
            'filter' => SeoRedirectsFilter::showFilter(),
        ]);
    }

    /**
     * Create new element page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function create()
    {
        // Breadcrumb
        Seo::breadcrumbs()->add('seo_redirects::seo.create');
        // Set h1
        Seo::meta()->setH1('seo_redirects::seo.create');
        // Javascript validation
        $this->initValidation((new AdminSeoRedirectsRequest())->rules());
        // Return form view
        return view('seo_redirects::admin.create', [
            'form' => AdminSeoRedirectsForm::make(),
        ]);
    }

    /**
     * Create page in database
     *
     * @param  AdminSeoRedirectsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(AdminSeoRedirectsRequest $request)
    {
        $seoRedirect = new SeoRedirect();
        $seoRedirect->fill($request->all());
        $seoRedirect->save();
        return $this->afterStore(['id' => $seoRedirect->id]);
    }

    /**
     * Update element page
     *
     * @param  SeoRedirect $seoRedirect
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(SeoRedirect $seoRedirect)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($seoRedirect->name ?? 'seo_redirects::seo.edit');
        // Set h1
        Seo::meta()->setH1('seo_redirects::seo.edit');
        // Javascript validation
        $this->initValidation((new AdminSeoRedirectsRequest)->rules());
        // Return form view
        return view('seo_redirects::admin.update', [
            'form' => AdminSeoRedirectsForm::make($seoRedirect),
        ]);
    }

    /**
     * Update page in database
     *
     * @param AdminSeoRedirectsRequest $request
     * @param SeoRedirect $seoRedirect
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(AdminSeoRedirectsRequest $request, SeoRedirect $seoRedirect)
    {
        // Fill new data
        $seoRedirect->fill($request->all());
        // Create new page
        $seoRedirect->save();
        // Do something
        return $this->afterUpdate(['id' => $seoRedirect->id]);
    }

    /**
     * Totally delete page from database
     *
     * @param  SeoRedirect $seoRedirect
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(SeoRedirect $seoRedirect)
    {
        // Delete seoRedirects
        $seoRedirect->forceDelete();
        // Do something
        return $this->afterDestroy();
    }
    
    public function import()
    {
        Seo::breadcrumbs()->add('seo_redirects::seo.import');
        Seo::meta()->setH1('seo_redirects::seo.import');
        $this->initValidation((new ImportRequest)->rules());
        return view('seo_redirects::admin.import', [
            'form' => ImportForm::make(),
        ]);
    }
    
    private $columns = ['A' => 'from', 'B' => 'to', 'C' => 'code'];
    
    /**
     * @param ImportRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function processImport(ImportRequest $request)
    {
        $file = $request->file('import');
        // Parse file for import
        $reader = IOFactory::createReader(ucfirst($file->getClientOriginalExtension()));
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($request->file('import')->getRealPath());
        
        if ($request->input('old_redirects') === 'disable') {
            SeoRedirect::query()->update([
                'active' => false,
            ]);
        } elseif ($request->input('old_redirects') === 'delete') {
            SeoRedirect::query()->delete();
        }
    
        foreach ($spreadsheet->getSheet(0)->getRowIterator() as $row) {
            $redirect = [];
            foreach ($row->getCellIterator('A', 'C') as $cell) {
                $redirect[$this->columns[$cell->getColumn()]] = $cell->getValue();
            }
            $validation = \Validator::make($redirect, ['from' => 'required|url', 'to' => 'required|url', 'code' => 'nullable|' . Rule::in(array_keys(config('seo_redirects.types', [])))]);
            if (!$validation->fails()) {
                $parsedFromUrl = parse_url($redirect['from']);
                if (!$parsedFromUrl || !array_get($parsedFromUrl, 'path')) {
                    continue;
                }
                $parsedToUrl = parse_url($redirect['to']);
                if (!$parsedToUrl || !array_get($parsedToUrl, 'path')) {
                    continue;
                }
                $redirect['from'] = $parsedFromUrl['path'];
                $redirect['to'] = $parsedToUrl['path'];
                $redirect['code'] = (int)$redirect['code'] ?: 302;
                
                SeoRedirect::updateOrCreate([
                    'link_from' => $redirect['from'],
                ], [
                    'link_to' => $redirect['to'],
                    'type' => $redirect['code'],
                    'active' => true,
                ]);
            }
        }
        
        return redirect()->back();
    }
}
