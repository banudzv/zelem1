@php
/** @var int $productId */
/** @var bool $isInComparison */
@endphp

<div class="item-card-controls__control {{ $isInComparison ? 'is-active' : null }}" data-comparelist-toggle="{{ $productId }}">
    {!! SiteHelpers\SvgSpritemap::get('icon-compare', [
        // 'class' => 'button__icon button__icon--before'
    ]) !!}
</div>
