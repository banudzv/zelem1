@php
/** @var string $products */
$hideH1 = true;
@endphp

@extends('site._layouts.compare')

@section('layout-body')
    <div class="section _mb-lg">
        <div class="container container--inner _mb-xl _def-mb-xxl">
            {!! $products !!}
            <hr class="separator _color-gray3 _mtb-xl">
        </div>
    </div>
@endsection
