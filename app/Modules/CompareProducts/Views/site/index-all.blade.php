@extends('site._layouts.compare')
@section('layout-body')
    <div class="section _mb-lg">
        <div class="container container--inner _mb-xl _def-mb-xxl">
            {!! $products !!}
        </div>
    </div>
@endsection
