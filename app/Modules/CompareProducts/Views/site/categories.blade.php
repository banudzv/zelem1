@php
    /** @var array $categories */
@endphp

<div data-compare-categories>
    @foreach($categories as $category)
        <div class="grid _flex-nowrap compare-popover _items-center">
            <div class="gcell gcell--auto _flex-noshrink _pr-def compare-popover__text _mr-xm">
                <a href="{{ $category['link'] }}">
                    {{ $category['name'] }} <span data-category="">({{ $category['total'] }})</span>
                </a>
            </div>
            <div class="gcell gcell--auto _flex-grow _ml-auto _flex _justify-end">
                <a class="compare-popover__icon"
                   data-products-ids="{{ $category['products'] }}"
                   data-delete-url="{{ route('site.compare.remove') }}"
                >
                    {!! SiteHelpers\SvgSpritemap::get('icon-close') !!}
                </a>
            </div>
        </div>
    @endforeach
</div>
