<?php

namespace App\Modules\CompareProducts;

use App\Core\BaseProvider;
use App\Core\ObjectValues\LinkObjectValue;
use App\Modules\CompareProducts\Facades\CompareFacade;
use App\Modules\CompareProducts\Widgets\CompareActionBarButton;
use App\Modules\CompareProducts\Widgets\CompareCategoriesDropdown;
use App\Modules\CompareProducts\Widgets\CompareProductButton;
use App\Modules\CompareProducts\Widgets\CompareMobileButton;
use CustomForm\Macro\Toggle;
use CustomSettings;
use Illuminate\Foundation\AliasLoader;
use Widget, Catalog, CustomSiteMenu;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\CompareProducts
 */
class Provider extends BaseProvider
{

    /**
    * Set custom presets
    */
    protected function presets()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('CompareProducts', CompareFacade::class);
        
        $this->setModuleName('compare');
        $this->setTranslationsNamespace('compare');
        $this->setViewNamespace('compare');
        $this->setConfigNamespace('compare');
    }
    
    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        $settings = CustomSettings::createAndGet('products');
        $settings->add(
            Toggle::create('hide-compare')
                ->setLabel('compare::settings.attributes.hide-compare')
                ->setDefaultValue(false)
                ->required(),
            ['required', 'boolean']
        );
    }
    
    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
        if ((bool)config('db.products.hide-compare', false) === false) {
            Widget::register(CompareActionBarButton::class, 'compare::action-bar');
            Widget::register(CompareProductButton::class, 'compare::button');
            Widget::register(CompareCategoriesDropdown::class, 'compare::categories');
            Widget::register(CompareMobileButton::class, 'mobile-top-compare-button');
        }
        
        // Register mobile menu block
        /*$menu = CustomSiteMenu::get('account-mobile');
        $menu->link(
            'compare::site.compare',
            LinkObjectValue::make(route('site.compare')),
            'icon-compare'
        );*/
    }

}
