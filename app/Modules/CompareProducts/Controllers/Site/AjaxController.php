<?php

namespace App\Modules\CompareProducts\Controllers\Site;

use App\Core\AjaxTrait;
use App\Core\SiteController;
use CompareProducts;
use Illuminate\Http\Request;
use Widget;

/**
 * Class AjaxController
 *
 * @package App\Modules\Compare\Controllers\Site
 */
class AjaxController extends SiteController
{
    use AjaxTrait;
    
    /**
     * Toggles product in comparison (adds and removes it)
     *
     * @param int $productId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function toggle(int $productId)
    {
        abort_if((bool)config('db.products.hide-compare', false), 404);
        CompareProducts::toggle($productId);
        return $this->successJsonAnswer([
            'total' => CompareProducts::count(),
            'categories' => Widget::show('compare::categories')->render()
        ]);
    }

    public function remove(Request $request)
    {
        $productIds = $request->input('product_ids');

        if ($productIds) {
            if (!is_array($productIds)) {
                $productIds = [$productIds];
            }

            CompareProducts::massRemove($productIds);
        }

        return $this->successJsonAnswer([
            'total' => CompareProducts::count(),
            'categories' => Widget::show('compare::categories')->render()
        ]);
    }
}
