<?php

namespace App\Modules\CompareProducts\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Categories\Models\Category;
use CompareProducts;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CompareCategoriesDropdown
 *
 * @package App\Modules\CompareProducts\Widgets
 */
class CompareCategoriesDropdown implements AbstractWidget
{
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $productsIds = CompareProducts::getProducts()->toArray();
        $categories = Category::with([
                'current',
                'products' => function($query) use ($productsIds) {
                    $query->whereIn('products.id', $productsIds);
                }
            ])
            ->active()
            ->whereHas('products', function (Builder $builder) use ($productsIds) {
                $builder->whereIn('products.id', $productsIds);
            })
            ->get()
            ->mapWithKeys(function ($category) {
                return [$category->id => [
                    'name' => $category->current->name,
                    'total' => $category->products->count(),
                    'products' => implode(',', $category->products->pluck('id')->toArray()),
                    'link' => route('site.compare.category', ['slug' => $category->current->slug])
                ]];
            })
            ->toArray();

        return view('compare::site.categories', [
            'categories' => $categories,
        ]);
    }
    
}
