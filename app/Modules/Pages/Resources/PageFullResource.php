<?php

namespace App\Modules\Pages\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PagesResource
 *
 * @package App\Modules\Pages\Resources
 *
 * @OA\Schema(
 *   schema="PageFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "active", "position", "parent_id", "data"},
 *           @OA\Property(property="id", type="integer", description="Article id"),
 *           @OA\Property(property="active", type="boolean", description="Article activity"),
 *           @OA\Property(property="position", type="integer", description="Position in the list"),
 *           @OA\Property(property="parent_id", type="integer", description="Parent page id"),
 *           @OA\Property(
 *              property="data",
 *              type="array",
 *              description="Multilanguage data",
 *              @OA\Items(
 *                  type="object",
 *                  allOf={
 *                      @OA\Schema(
 *                          required={"language", "name", "slug"},
 *                          @OA\Property(property="language", type="string", description="Language to store"),
 *                          @OA\Property(property="name", type="string", description="Article name"),
 *                          @OA\Property(property="slug", type="string", description="Page slug"),
 *                          @OA\Property(property="content", type="string", description="Article full content"),
 *                          @OA\Property(property="h1", type="string", description="Article meta h1"),
 *                          @OA\Property(property="title", type="string", description="Article meta title"),
 *                          @OA\Property(property="description", type="string", description="Article meta description"),
 *                          @OA\Property(property="keywords", type="string", description="Article meta keywords"),
 *                      )
 *                  }
 *              )
 *           ),
 *       )
 *   }
 * )
 */
class PageFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->toArray();
    }
}
