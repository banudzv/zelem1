<?php

namespace App\Modules\Pages\Controllers\Api;

use App\Core\ApiController;
use App\Modules\Pages\Models\Page;
use App\Modules\Pages\Resources\PageFullResource;

/**
 * Class PagesController
 *
 * @package App\Modules\Pages\Controllers\Api
 */
class PagesController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/pages/all",
     *     tags={"TextPages"},
     *     summary="Returns all text pages with all data",
     *     operationId="getArticlesFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/PageFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return PageFullResource::collection(Page::with(['data'])->oldest('position')->get());
    }
}
