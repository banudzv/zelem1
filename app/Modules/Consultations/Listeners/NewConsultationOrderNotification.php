<?php

namespace App\Modules\Consultations\Listeners;

use App\Components\Crm\Amo;
use App\Core\Interfaces\ListenerInterface;
use App\Core\Modules\Mail\Models\MailTemplate;
use App\Core\Modules\Notifications\Models\Notification;
use App\Modules\Consultations\Models\Consultation;
use App\Modules\Subscribe\Events\NewSubscriberEvent;
use App\Components\Mailer\MailSender;

/**
 * Class NewConsultationOrderNotification
 *
 * @package App\Modules\Consultations\Listeners
 */
class NewConsultationOrderNotification implements ListenerInterface
{

    const NOTIFICATION_TYPE = 'consultations';

    const NOTIFICATION_ICON = 'fa fa-phone';

    public static function listens(): string
    {
        return 'consultation';
    }

    /**
     * Handle the event.
     *
     * @param Consultation $consultation
     * @return void
     */
    public function handle(Consultation $consultation)
    {
        if (config('db.amo_crm.use_amo')) {
            $this->addInAmoCrm($consultation);
        }
        Notification::send(
            static::NOTIFICATION_TYPE,
            'consultations::general.notification',
            'admin.consultations.edit',
            ['id' => $consultation->id]
        );
    }

    /**
     * @param Consultation $consultation
     */
    public function addInAmoCrm(Consultation $consultation)
    {
        $data = [];
        $data['formName'] = __('consultations::general.notification');
        $data['name'] = $consultation->name ?: $consultation->phone;
        $data['phone'] = $consultation->phone;
        $data['text'] = $consultation->question;
        $amo = new Amo();
        $amo->newLead($data);
    }
}
