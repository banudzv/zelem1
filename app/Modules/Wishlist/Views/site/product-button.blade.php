@php
/** @var int $productId */
/** @var bool $isInWishlist */
@endphp

<div class="item-card-controls__control {{ $isInWishlist ? 'is-active' : null }}" data-wishlist-toggle="{{ $productId }}">
    {!! SiteHelpers\SvgSpritemap::get('icon-wishlist', [
        // 'class' => 'button__icon button__icon--before'
    ]) !!}
</div>
