@php
@endphp

@if(isset($slot) && trim(strip_tags($slot)))
    @if(Auth::guest())
        <div class="section _mb-lg">
            <div class="container container--small container--white">
                {{ $slot }}
            </div>
        </div>
    @else
        <div class="account account--wishlist">
            {{ $slot }}
        </div>
    @endif
@endif
