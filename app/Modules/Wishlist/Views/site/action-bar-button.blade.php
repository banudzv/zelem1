@php
/** @var int $total */
$isCurrent = preg_match('/^site.wishlist$/', Route::currentRouteName());
@endphp
<div class="action-bar__control _def-show">
        @if ($isCurrent || $total < 1)
            <a role="link" tabindex="0" data-href="{{ route('site.wishlist') }}"
                class="action-bar-control action-bar-control--wishlist  {{ $isCurrent ? 'is-current' : null }}"
            data-wishlist-link>
        @else
            <a href="{{ route('site.wishlist') }}"
                role="link" tabindex="0"
                class="action-bar-control action-bar-control--wishlist"
            data-wishlist-link>
        @endif
        {!! SiteHelpers\SvgSpritemap::get('icon-wishlist', ['class' => 'action-bar-control__icon']) !!}
        {{-- <div class="action-bar-control__title _ellipsis">@lang('wishlist::site.action-bar-button')</div> --}}
        <div class="action-bar-control__count" data-wishlist-counter>{{ $total ?: null }}</div>
    </a>
    <div class="popover {{ $total > 0 ? '_hide' : null }}" data-wishlist-popover>
        <div class="grid _flex-nowrap">
            <div class="gcell gcell--auto _flex-noshrink _pr-def">
                {!! SiteHelpers\SvgSpritemap::get('icon-wishlist', [
					'class' => 'svg-icon svg-icon--icon-wishlist',
				]) !!}
            </div>
            <div class="gcell gcell--auto _flex-grow">
                <div class="title title--size-h3  _color-dark-blue">@lang('wishlist::site.wishlist-is-empty')</div>
                @if(Auth::check())
                    <div>@lang('wishlist::site.add-products-to-your-wishlist')</div>
                @else
                    <div>@lang('wishlist::site.add-products-to-your-wishlist-2') <span role="button" class="js-init" data-mfp="inline" data-mfp-src="#popup-regauth">@lang('wishlist::site.login')</span></div>
                @endif
            </div>
        </div>
    </div>
</div>
