<?php

use Illuminate\Support\Facades\Route;

Route::get('news', 'NewsController@index');
Route::get('news/all', 'NewsController@all');
Route::get('news/{news}', ['uses' => 'NewsController@show', 'as' => 'api.news.show']);
