<?php

namespace App\Modules\News\Controllers\Api;

use App\Core\ApiController;
use App\Modules\News\Models\News;
use App\Modules\News\Resources\NewsCollection;
use App\Modules\News\Resources\NewsFullResource;
use App\Modules\News\Resources\NewsResource;

/**
 * Class NewsController
 *
 * @package App\Modules\News\Controllers\Api
 */
class NewsController extends ApiController
{
    const LIMIT = 10;
    
    /**
     * @OA\Get(
     *     path="/api/news/all",
     *     tags={"News"},
     *     summary="Returns all news with all data",
     *     operationId="getNewsFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/NewsFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return NewsFullResource::collection(News::with(['data', 'image'])->get());
    }
    
    /**
     * @OA\Get(
     *     path="/api/news",
     *     tags={"News"},
     *     summary="Returns list of news with block for pagination",
     *     description="`page` attribute could be used in query",
     *     operationId="getNews",
     *     deprecated=false,
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page needed for pagination",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="imageSize",
     *         in="query",
     *         description="Image size to return (original by default)",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/NewsList")
     *     ),
     * )
     */
    public function index()
    {
        return new NewsCollection(News::with(['current', 'image'])->published()->whereActive(true)->latest('id')->paginate(static::LIMIT));
    }
    
    /**
     * @OA\Get(
     *     path="/api/news/{id}",
     *     tags={"News"},
     *     summary="Returns one article by id",
     *     description="Returns one article by id",
     *     operationId="getOneOfNews",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="News id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="newsId"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="imageSize",
     *         in="query",
     *         description="Image size to return (original by default)",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/News"),
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     *
     * @param News $news
     * @return NewsResource
     */
    public function show(News $news)
    {
        abort_if($news->is_in_feature, 404);
        abort_unless($news->active, 404);
        return new NewsResource($news);
    }
}
