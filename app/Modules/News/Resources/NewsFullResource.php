<?php

namespace App\Modules\News\Resources;

use App\Modules\News\Models\News;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ArticleResource
 *
 * @package App\Modules\Articles\Resources
 *
 * @OA\Schema(
 *   schema="NewsFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "date", "active", "views", "show_short_content", "show_image", "data"},
 *           @OA\Property(property="id", type="integer", description="Article id"),
 *           @OA\Property(property="date", type="string", description="Article publishing date"),
 *           @OA\Property(property="active", type="boolean", description="Article activity"),
 *           @OA\Property(property="views", type="integer", description="Article views counter"),
 *           @OA\Property(property="show_short_content", type="boolean", description="Show short content?"),
 *           @OA\Property(property="show_image", type="boolean", description="Show image inside the page?"),
 *           @OA\Property(property="image", type="string", description="Link to original image"),
 *           @OA\Property(
 *              property="data",
 *              type="array",
 *              description="Multilanguage data",
 *              @OA\Items(
 *                  type="object",
 *                  allOf={
 *                      @OA\Schema(
 *                          required={"language", "name", "slug"},
 *                          @OA\Property(property="language", type="string", description="Language to store"),
 *                          @OA\Property(property="name", type="string", description="Article name"),
 *                          @OA\Property(property="slug", type="string", description="Article slug"),
 *                          @OA\Property(property="short_content", type="string", description="Short article content (preview)"),
 *                          @OA\Property(property="content", type="string", description="Article full content"),
 *                          @OA\Property(property="h1", type="string", description="Article meta h1"),
 *                          @OA\Property(property="title", type="string", description="Article meta title"),
 *                          @OA\Property(property="description", type="string", description="Article meta description"),
 *                          @OA\Property(property="keywords", type="string", description="Article meta keywords"),
 *                      )
 *                  }
 *              )
 *           ),
 *       )
 *   }
 * )
 */
class NewsFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var News $article */
        $article = $this->resource;
        $response = $article->toArray();
        
        unset($response['image']);
        $response['image'] = $article->image->link('original', false);
        
        return $response;
    }
}
