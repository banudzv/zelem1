<?php

namespace App\Modules\News\Resources;

use App\Modules\News\Models\News;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class NewsSimpleResource
 *
 * @package App\Modules\News\Resources
 *
 * @OA\Schema(
 *   schema="NewsSimple",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "views", "date", "name", "link"},
 *           @OA\Property(property="id", type="integer", description="Article id"),
 *           @OA\Property(property="views", type="integer", description="Article views counter"),
 *           @OA\Property(property="name", type="string", description="Article name"),
 *           @OA\Property(property="link", type="string", description="Article link (for API request)"),
 *           @OA\Property(property="short_content", type="string", description="Short article content (preview)"),
 *           @OA\Property(property="image", type="string", description="Image in chosen size (original by default)"),
 *       )
 *   }
 * )
 */
class NewsSimpleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var News $article */
        $article = $this->resource;
        
        return [
            'id' => $article->id,
            'views' => $article->views,
            'date' => $article->formatted_published_at,
            'name' => $article->current->name,
            'link' => route('api.news.show', $article->id),
            'short_content' => $article->current->short_content,
            'image' => $article->image->link(request()->query('imageSize', 'original')),
        ];
    }
}
