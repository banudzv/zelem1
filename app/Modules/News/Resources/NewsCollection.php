<?php

namespace App\Modules\News\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class NewsCollection
 *
 * @package App\Modules\Articles\Resources
 */
class NewsCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     *
     * @OA\Schema(
     *   schema="NewsList",
     *   @OA\Property(
     *      property="data",
     *      description="News list",
     *      type="array",
     *      @OA\Items(ref="#/components/schemas/NewsSimple")
     *   ),
     *   @OA\Property(
     *      property="links",
     *      ref="#/components/schemas/PaginationLinks",
     *   ),
     *   @OA\Property(
     *      property="meta",
     *      ref="#/components/schemas/PaginationMeta",
     *   ),
     * )
     */
    public function toArray($request)
    {
        return NewsSimpleResource::collection($this->collection);
    }
}
