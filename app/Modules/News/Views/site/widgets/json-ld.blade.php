@php
/** @var \App\Modules\News\Models\News $news */
/** @var string|null $logo */
@endphp

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "NewsArticle",
    "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "{{ $news->link }}"
    },
    "headline": "{{ $news->current->name }}",
    @if($news->image)
    "image": [
        @if($news->image->isImageExists())
        "{{ $news->image->link() }}"
        @else
        "{{ route('site.home') }}{{ $news->image->link() }}"
        @endif
    ],
    @endif
    "datePublished": "{{ $news->published_at }}",
    "dateModified": "{{ $news->updated_at ?: $news->published_at }}",
    "author": {
        "@type": "Organization",
        "name": "{{ env('APP_NAME') }}"
    },
    "publisher": {
        "@type": "Organization",
        @if(isset($logo) && $logo)
        "logo": {
            "@type": "ImageObject",
            "url": "{{ $logo }}"
        },
        @endif
        "name": "{{ env('APP_NAME') }}"
    },
    "description": "{{ $news->teaser }}"
}
</script>
