@php
/** @var App\Modules\News\Models\News $news */
@endphp

<div class="news-card">
	<a href="{{ $news->link }}" class="news-card__image lazyload-blur">
		{!! $news->imageTag('345x255', ['class' => 'lazyload-blur__image', ]) !!}
	</a>
	<time class="news-card__datetime" datetime="{{ $news->published_at->format('Y-m-d') }}">
		{{ $news->formatted_published_at }}
	</time>
	<div class="news-card__title">
		<a href="{{ $news->link }}">{{ $news->current->name }}</a>
	</div>
	<div class="news-card__desc">
		{{ $news->teaser }}
	</div>
	{!! Widget::show('news::json-ld', $news) !!}
</div>
