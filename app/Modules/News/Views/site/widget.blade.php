@php
    /** @var \App\Modules\News\Models\News[] $news */
@endphp
<div class="section _def-show">
    <div class="container _mtb-lg _def-mtb-xxl">
        <div class="grid grid--auto _justify-between _items-center _pb-def">
            <div class="gcell _mb-def _mr-def">
                <div class="title title--size-h2 _color-dark-blue title--normal">@lang('news::site.news')</div>
            </div>
            <div class="gcell _mb-def _self-end">
                <a href="{{route('site.news')}}" class="link link--underline link--icon-small text--semi-bold link--dark">
                    {{trans('news::site.all-news')}}
                    <div class="icon">
                        {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right') !!}
                    </div>
                </a>
            </div>
        </div>

        @include('news::site.widgets.news-list', [
			'news' => $news,
			'grid_mod_classes' => 'grid--2 grid--def-4',
		])
    </div>
</div>
