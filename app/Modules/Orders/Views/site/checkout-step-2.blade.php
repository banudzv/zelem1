@php
    /** @var \App\Modules\Orders\Types\StepOneType $info */
    /** @var array $paymentMethods */
    /** @var array $deliveries */
    /** @var array $zalemWarehouses */
    /** @var bool $canMakeOrder */
    /** @var bool $showZalemAddressDelivery */
    /** @var integer $minimumOrderAmount */
    /** @var array $justinWarehouses */
    /** @var bool $isKiev */
    $canMakeOrder = true;
@endphp

@extends('site._layouts.checkout')

@push('head-styles')
    <style>
        .checkout-delivery-trigger.is-active {
            pointer-events: none;
        }
    </style>
@endpush

@push('cart-route')
    <script>
        var cartRoute = 2;
    </script>
@endpush

@push('hidden_data')
    {!! JsValidator::make($rules, $messages, $attributes, '#' . $formId) !!}
    <script>
        window.LOCO_DATA.checkout = "/";
    </script>
@endpush

@section('layout-body')
    <div class="section section--gradient js-init" data-product>
        <div class="container container--white">
            <div class="grid _justify-center _def-justify-between _def-flex-row-reverse">
                <div class="gcell gcell--12  gcell--def-6 _plr-sm _pt-md _def-pt-sm _def-pb-xl _md-plr-md _md-pt-md _def-p-xl"
                     data-cart-container="checkout">
                    {!! Widget::show('orders::cart::checkout', $totalAmount, $totalAmountOld, $services) !!}
                </div>
                <div class="gcell gcell--12  gcell--def-6 _plr-md _ptb-def _def-plr-xl _def-ptb-lg"
                     style="background-color: #fff;">
                    <div
                        class="title title--size-h1 title--normal _color-dark-blue">@lang('orders::site.checkout')</div>
                    <div class="checkout-step">
                        <div class="checkout-step__number">
                            @lang('orders::site.step-1')
                        </div>
                        <div
                            class="title title--size-h4 title--normal _color-dark-blue">@lang('orders::site.contact-data')</div>
                    </div>
                    <div class="_pt-sm _mb-sm">
                        <span class="text text--size-14 _color-blue-smoke">{{ $info->name }}, <span
                                class="text _color-dark-blue text--semi-bold">{{ $info->phone }}</span></span>
                        <a href="{{ route('site.checkout') }}"
                           class="link link--underline link--dark text--size-14 _ml-xs">
                            @lang('orders::site.edit')
                        </a>
                    </div>

                    <div class="checkout-step _mt-xl">
                        <div class="checkout-step__number">
                            @lang('orders::site.step-2')
                        </div>
                        @if(config('db.orders.show-delivery', true) && config('db.orders.show-payment', true))
                            <div
                                class="title title--size-h4 title--normal _color-dark-blue">@lang('orders::site.payment-and-delivery')</div>
                        @elseif(config('db.orders.show-delivery', true))
                            <div
                                class="title title--size-h4 title--normal _color-dark-blue">@lang('orders::site.delivery-title')</div>
                        @elseif(config('db.orders.show-payment', true))
                            <div
                                class="title title--size-h4 title--normal _color-dark-blue">@lang('orders::site.payment')</div>
                        @endif
                    </div>
                    @if(config('db.orders.show-delivery', true) === true)
                        <div class="_pt-sm _mb-sm">
                            <span
                                class="text text--size-14 _color-blue-smoke">@if($info->city)@lang('orders::site.city')@endif, <span
                                    class="text _color-dark-blue text--semi-bold">{{ $info->city }}</span></span>
                        </div>
                    @endif

                    <div class="form form--checkout-step-2">
                        {!! Form::open(['route' => 'site.checkout.step-2', 'data-form-checkout' => '', 'class' => ['js-init', 'ajax-form'], 'id' => $formId]) !!}
                        <div class="form__body js-init" data-delivery-price data-delivery-price-breakpoint="1000" data-related="{{ json_encode(['payment-method']) }}">
                            <div class="grid _nmtb-sm">
                                {{-- @if(config('db.orders.show-delivery', true) === true)
                                    <div class="gcell gcell--12 _ptb-sm">
                                        <div class="hint">
                                            @if($info->city)@lang('orders::site.city') <strong>{{ $info->city }}</strong>@endif
                                        </div>
                                    </div>
                                @endif --}}
                                @if(config('db.orders.show-delivery', true) || config('db.orders.show-payment', true))
                                    <div class="gcell gcell--12 _ptb-sm">
                                        @if(config('db.orders.show-delivery', true))
                                            <div class="_mb-def">
                                                {{-- <div class="hint hint--lg _pl-md"><span>@lang('orders::general.delivery-type')</span></div> --}}
                                                <div class="grid _mtb-md js-init _posr"
                                                     data-accordion="{{ json_encode(['type' => 'single-checkers', 'slideTime' => 200]) }}">
                                                    @foreach($deliveries as $delivery => $deliveryName)
                                                        @if (($delivery === 'self' && !config('db.orders.address_for_self_delivery_'.\Lang::getLocale())) or config('db.orders.hide-delivery-' . $delivery))
                                                            @continue
                                                        @endif
                                                        @if($delivery === 'zalem-warehouse' && empty($zalemWarehouses))
                                                            @continue
                                                        @endif
                                                        @if($delivery === 'zalem-address' && !$showZalemAddressDelivery)
                                                            @continue
                                                        @endif
                                                        @if($delivery === 'justin-warehouse' && !$justinWarehouses)
                                                            @continue
                                                        @endif
                                                        <div class="gcell gcell--12 _ptb-xs">
                                                            @php
                                                                $ns = 'delivery-method';
                                                                $id = $loop->iteration;
                                                                $isOpened = '';
                                                                $related_to = ['bank_transaction', 'liqpay'];
                                                                if (preg_match
                                                                ('/^nova-poshta$|^self$|^address$|^zalem/', $delivery)) {
                                                                    $related_to[] = 'cash';
                                                                }
                                                                if (preg_match('/^nova-poshta-self$|^address$|^zalem|^justin/', $delivery)) {
                                                                    $related_to[] = 'cash-on-delivery';
                                                                }
                                                                if (preg_match('/^other$/', $delivery)) {
                                                                    $related_to[] = 'cash-on-delivery';
                                                                }
                                                            @endphp
                                                            <div class="checkout-delivery-trigger {{ $isOpened }}"
                                                                 style="display: inline-block;" {!! sprintf("data-wstabs-ns=\"%s\" data-wstabs-button=\"%s\"", $ns, $id) !!}>
                                                                @component('site._widgets.radio.radio', [
                                                                    'classes' => '_color-gray4',
                                                                    'attributes' => [
                                                                        'type' => 'radio',
                                                                        'name' => 'delivery',
                                                                        'value' => $delivery,
                                                                        'data-related-to' => 'payment-method',
                                                                        'data-relations' => json_encode($related_to),
                                                                        'data-price' => delivery_price(0, $delivery, $isKiev),
                                                                        'data-price-thousand' => delivery_price(1001, $delivery, $isKiev),
                                                                        'data-delivery-price-checker' => '',
                                                                        'data-trigger-event' => 'deliveryCheck'
                                                                    ]
                                                                ])
                                                                    @lang($deliveryName)
                                                                @endcomponent
                                                            </div>
                                                            <div class="checkout-delivery-content {{ $isOpened }}"
                                                                 {!! $isOpened ? null : 'style="display: none;"' !!} data-wstabs-ns="{{ $ns }}"
                                                                 data-wstabs-block="{{ $id }}">
                                                                <div class="_pt-sm _pb-md">
                                                                    @if($delivery === 'nova-poshta-self')
                                                                        <div class="control control--select">
                                                                            <div class="control__inner custom-select2">
                                                                                <select lang="{{ app()->getLocale() }}"
                                                                                        data-initialize-select2
                                                                                        data-placeholder="@lang('orders::general.choose-warehouse')" class="control__field js-init"
                                                                                        name="{{ $delivery }}"
                                                                                        id="{{ $delivery }}"
                                                                                        onchange="this.blur()">
                                                                                    @foreach( $warehouses as $warehouse)
                                                                                        <option
                                                                                            value="{{ $warehouse->Ref }}">
                                                                                            {{ $warehouse->Description }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    @elseif($delivery === 'justin-warehouse')
                                                                        <div class="control control--select">
                                                                            <div class="control__inner">
                                                                                <select class="control__field js-init"
                                                                                        name="{{ $delivery }}"
                                                                                        id="{{ $delivery }}"
                                                                                        data-initialize-select2
                                                                                        data-placeholder="@lang('orders::general.choose-warehouse')"
                                                                                        onchange="this.blur()">
                                                                                    @foreach($justinWarehouses as $justinWarehouse)
                                                                                        <option
                                                                                            value="{{ $justinWarehouse['code'] }}" {{ old($delivery) === $justinWarehouse['code']  ? 'selected' : null }}>
                                                                                            {{ $justinWarehouse['descr'] }}
                                                                                            : {{ $justinWarehouse['address'] }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    @elseif($delivery === 'zalem-warehouse')
                                                                        <div class="control control--select">
                                                                            <div class="control__inner">
                                                                                <select class="control__field js-init"
                                                                                        data-initialize-select2
                                                                                        data-placeholder="@lang('orders::general.choose-warehouse')"
                                                                                        name="{{ $delivery }}"
                                                                                        id="{{ $delivery }}"
                                                                                        onchange="this.blur()">
                                                                                    @foreach($zalemWarehouses as $warehouseId => $warehouseName)
                                                                                        <option
                                                                                            value="{{ $warehouseId }}" {{ old($delivery) === $warehouseId  ? 'selected' : null }}>
                                                                                            {{ $warehouseName }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    @elseif($delivery === 'nova-poshta' || $delivery === 'address' || $delivery === 'zalem-address')
                                                                        <div class="grid">
                                                                            <div
                                                                                class="gcell gcell--2 gcell--ms-1 _flex _items-center">
                                                                                <div
                                                                                    class="text text--size-18 _color-dark-blue">
                                                                                    @lang('orders::site.checkout-form.address')
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="gcell gcell--10 gcell--ms-5 _pl-md">
                                                                                <div class="control control--input">
                                                                                    <div class="control__inner">
                                                                                        <input class="control__field"
                                                                                               type="text"
                                                                                               name="{{ $delivery }}[street]"
                                                                                               id="{{ $delivery }}-1"
                                                                                               required>
                                                                                        <label class="control__label"
                                                                                               for="{{ $delivery }}-1">@lang('orders::site.checkout-form.street')</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="gcell gcell--6 gcell--ms-3 _pt-md _ms-pt-none _ms-pl-md">
                                                                                <div class="control control--input">
                                                                                    <div class="control__inner">
                                                                                        <input class="control__field"
                                                                                               type="text"
                                                                                               name="{{ $delivery }}[house]"
                                                                                               id="{{ $delivery }}-2"
                                                                                               required>
                                                                                        <label class="control__label"
                                                                                               for="{{ $delivery }}-2">@lang('orders::site.checkout-form.house')</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="gcell gcell--6 gcell--ms-3 _pt-md _ms-pt-none _pl-md">
                                                                                <div class="control control--input">
                                                                                    <div class="control__inner">
                                                                                        <input class="control__field"
                                                                                               type="text"
                                                                                               name="{{ $delivery }}[flat]"
                                                                                               id="{{ $delivery }}-3">
                                                                                        <label class="control__label"
                                                                                               for="{{ $delivery }}-3">@lang('orders::site.checkout-form.flat')</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @elseif($delivery === 'self')
                                                                        <div
                                                                            class="wysiwyg">{!! config('db.orders.address_for_self_delivery_'.\Lang::getLocale()) !!}</div>
                                                                        {{-- @elseif($delivery === 'address')
                                                                            <div class="control control--input">
                                                                                <div class="control__inner">
                                                                                    <input class="control__field" type="text" name="{{ $delivery }}"  id="{{ $delivery }}" required>
                                                                                    <label class="control__label" for="{{ $delivery }}">Введите адрес доставки</label>
                                                                                </div>
                                                                            </div> --}}
                                                                    @elseif($delivery === 'other')
                                                                        <div class="grid _mtb-xs _pl-lg js-init">
                                                                            @foreach($restDeliveries ?? [] as $restDelivery => $restDeliveryName)
                                                                                @php
                                                                                    $id = $loop -> iteration;
                                                                                    $ns = 'other-deliveries';
                                                                                @endphp
                                                                                <div class="gcell gcell--12 _ptb-xs">
                                                                                    <div class="checkout-delivery-trigger {{ $loop->first ? 'is-active is-open' : null }}"
                                                                                         {!! sprintf("data-wstabs-ns=\"%s\" data-wstabs-button=\"%s\"", $ns, $id) !!}>
                                                                                    @component('site._widgets.radio.radio', [
                                                                                        'classes' => '_color-gray4',
                                                                                        'attributes' => [
                                                                                            'type' => 'radio',
                                                                                            'name' => 'other',
                                                                                            'value' => $restDelivery,
                                                                                            'checked' => $loop->first ? true : false,
                                                                                            'data-trigger-event' => 'deliveryCheck'
                                                                                        ]
                                                                                    ])
                                                                                        @lang($restDeliveryName)
                                                                                    @endcomponent
                                                                                    </div>
                                                                                    <div class="checkout-delivery-content {{ $loop->first ? 'is-active is-open' : null }}" {{ !$loop->first ? 'style=display:none' : null }} {!! sprintf("data-wstabs-ns=\"%s\" data-wstabs-block=\"%s\"", $ns, $id) !!}>
                                                                                        @if($restDelivery === 'nova-poshta-self')
                                                                                            <div class="control control--select _pt-md">
                                                                                                <div class="control__inner custom-select2">
                                                                                                    <select lang="{{ app()->getLocale() }}"
                                                                                                            data-initialize-select2 class="control__field js-init"
                                                                                                            data-placeholder="@lang('orders::general.choose-warehouse')"
                                                                                                            name="{{ $restDelivery }}"
                                                                                                            id="{{ $restDelivery }}"
                                                                                                            onchange="this.blur()">
                                                                                                        @foreach( $warehouses as $warehouse)
                                                                                                            <option
                                                                                                                value="{{ $warehouse->Ref }}">
                                                                                                                {{ $warehouse->Description }}
                                                                                                            </option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        @elseif($restDelivery === 'justin-warehouse')
                                                                                            <div class="control control--select _pt-md">
                                                                                                <div class="control__inner">
                                                                                                    <select class="control__field js-init"
                                                                                                            name="{{ $restDelivery }}"
                                                                                                            id="{{ $restDelivery }}"
                                                                                                            data-initialize-select2
                                                                                                            data-placeholder="@lang('orders::general.choose-warehouse')"
                                                                                                            onchange="this.blur()">
                                                                                                        @foreach($justinWarehouses as $justinWarehouse)
                                                                                                            <option
                                                                                                                value="{{ $justinWarehouse['code'] }}" {{ old($delivery) === $justinWarehouse['code']  ? 'selected' : null }}>
                                                                                                                {{ $justinWarehouse['descr'] }}
                                                                                                                : {{ $justinWarehouse['address'] }}
                                                                                                            </option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        @elseif($restDelivery === 'zalem-warehouse')
                                                                                            <div class="control control--select _pt-md">
                                                                                                <div class="control__inner">
                                                                                                    <select class="control__field js-init"
                                                                                                            data-initialize-select2
                                                                                                            data-placeholder="@lang('orders::general.choose-warehouse')"
                                                                                                            name="{{ $restDelivery }}"
                                                                                                            id="{{ $restDelivery }}"
                                                                                                            onchange="this.blur()">
                                                                                                        @foreach($zalemWarehouses as $warehouseId => $warehouseName)
                                                                                                            <option
                                                                                                                value="{{ $warehouseId }}" {{ old($delivery) === $warehouseId  ? 'selected' : null }}>
                                                                                                                {{ $warehouseName }}
                                                                                                            </option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        @elseif($restDelivery === 'nova-poshta' || $restDelivery === 'address' || $restDelivery === 'zalem-address')
                                                                                            <div class="grid _pt-md">
                                                                                                <div
                                                                                                    class="gcell gcell--2 gcell--ms-1 _flex _items-center">
                                                                                                    <div
                                                                                                        class="text text--size-18 _color-dark-blue">
                                                                                                        @lang('orders::site.checkout-form.address')
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="gcell gcell--10 gcell--ms-5 _pl-md">
                                                                                                    <div class="control control--input">
                                                                                                        <div class="control__inner">
                                                                                                            <input class="control__field"
                                                                                                                   type="text"
                                                                                                                   name="{{ $restDelivery }}[street]"
                                                                                                                   id="{{ $restDelivery }}-1"
                                                                                                                   required>
                                                                                                            <label class="control__label"
                                                                                                                   for="{{ $restDelivery }}-1">@lang('orders::site.checkout-form.street')</label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="gcell gcell--6 gcell--ms-3 _pt-md _ms-pt-none _ms-pl-md">
                                                                                                    <div class="control control--input">
                                                                                                        <div class="control__inner">
                                                                                                            <input class="control__field"
                                                                                                                   type="text"
                                                                                                                   name="{{ $restDelivery }}[house]"
                                                                                                                   id="{{ $restDelivery }}-2"
                                                                                                                   required>
                                                                                                            <label class="control__label"
                                                                                                                   for="{{ $restDelivery }}-2">@lang('orders::site.checkout-form.house')</label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="gcell gcell--6 gcell--ms-3 _pt-md _ms-pt-none _pl-md">
                                                                                                    <div class="control control--input">
                                                                                                        <div class="control__inner">
                                                                                                            <input class="control__field"
                                                                                                                   type="text"
                                                                                                                   name="{{ $restDelivery }}[flat]"
                                                                                                                   id="{{ $restDelivery }}-3">
                                                                                                            <label class="control__label"
                                                                                                                   for="{{ $restDelivery }}-3">@lang('orders::site.checkout-form.flat')</label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        @elseif($restDelivery === 'self')
                                                                                            <div
                                                                                                class="wysiwyg _pt-md">{!! config('db.orders.address_for_self_delivery_'.\Lang::getLocale()) !!}</div>
                                                                                            {{-- @elseif($delivery === 'address')
                                                                                                <div class="control control--input">
                                                                                                    <div class="control__inner">
                                                                                                        <input class="control__field" type="text" name="{{ $delivery }}"  id="{{ $delivery }}" required>
                                                                                                        <label class="control__label" for="{{ $delivery }}">Введите адрес доставки</label>
                                                                                                    </div>
                                                                                                </div> --}}
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <label id="delivery-error" class="has-error _ml-md" for="delivery"
                                                           style="display: none;"></label>
                                                </div>
                                            </div>
                                        @endif
                                        @if(config('db.orders.show-payment', true))
                                            <div>
                                                <div class="hint hint--lg hint--decore _text-left">
                                                    <span
                                                        class="text text--size-18 _color-dark-blue">@lang('orders::general.payment-method')</span>
                                                </div>
                                                <div class="grid _mtb-md _posr js-init"
                                                     data-accordion="{{ json_encode(['type' => 'single-checkers', 'slideTime' => 200]) }}">
                                                    @foreach($paymentMethods as $paymentMethod => $paymentMethodName)
                                                        @if($paymentMethod !== \App\Modules\Orders\Models\Order::PAYMENT_LIQPAY || $showLiqpay)
                                                            <div class="gcell gcell--12 _ptb-xs">
                                                                <div class="checkout-delivery-trigger"
                                                                     style="display: inline-block;" {!! sprintf("data-wstabs-ns=\"%s\" data-wstabs-button=\"%s\"", 'payment-method', $loop->iteration) !!}>
                                                                    @component('site._widgets.radio.radio', [
                                                                        'rootAttrs' => 'data-related-for="payment-method"',
                                                                        'classes' => '_color-gray4',
                                                                        'attributes' => [
                                                                            'type' => 'radio',
                                                                            'name' => 'payment_method',
                                                                            'value' => $paymentMethod,
                                                                            'data-trigger-event' => 'paymentCheck'
                                                                        ],
                                                                    ])
                                                                        @lang($paymentMethodName)
                                                                    @endcomponent
                                                                </div>
                                                                {{-- <div class="checkout-delivery-content" style="display: none;" data-wstabs-ns="payment-method" data-wstabs-block="{{ $loop->iteration }}">
                                                                    <div class="_pt-sm _pb-md">
                                                                        <div class="control control--input">
                                                                            <div class="control__inner">
                                                                                <input class="control__field" type="text" name="{{ $delivery }}"  id="{{ $delivery }}" required>
                                                                                <label class="control__label" for="{{ $delivery }}">Введите адрес доставки</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> --}}
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                    <div class="gcell gcell--12 _ptb-xs">
                                                        <div class="checkout-delivery-trigger"
                                                             style="display: inline-block;" {!! sprintf("data-wstabs-ns=\"%s\" data-wstabs-button=\"%s\"", 'payment-method', '6') !!}>
                                                            @component('site._widgets.radio.radio', [
                                                                // 'rootAttrs' => 'data-related-for="payment-method"',
                                                                'classes' => '_color-gray4',
                                                                'attributes' => [
                                                                    'type' => 'radio',
                                                                    'name' => 'payment_method',
                                                                    'value' => 'business',
                                                                    'data-trigger-event' => 'paymentCheck'

                                                                ],
                                                            ])
                                                                @lang('orders::general.payment-methods.business')
                                                            @endcomponent
                                                        </div>
                                                        <div class="checkout-delivery-content" style="display: none;"
                                                             data-wstabs-ns="payment-method" data-wstabs-block="6">
                                                            <div class="_pt-sm _ms-pl-xl">
                                                                <div class="grid grid--1 grid--ms-2">
                                                                    <div class="gcell">
                                                                        <div class="control control--input">
                                                                            <div class="control__inner">
                                                                                <input class="control__field"
                                                                                       type="text"
                                                                                       name="business[company]"
                                                                                       id="business-1" required>
                                                                                <label class="control__label"
                                                                                       for="business-1">@lang('orders::site.checkout-form.business.company')</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="gcell _pt-xs _sm-pt-none _sm-pl-xs">
                                                                        <div class="control control--input">
                                                                            <div class="control__inner">
                                                                                <input class="control__field"
                                                                                       type="text"
                                                                                       name="business[egrpu]"
                                                                                       id="business-2" required>
                                                                                <label class="control__label"
                                                                                       for="business-2">@lang('orders::site.checkout-form.business.egrpu')</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label id="payment_method-error" class="has-error _ml-md"
                                                           for="payment_method" style="display: none;"></label>
                                                </div>
                                            </div>
                                        @endif
                                        <div>
                                            <div class="hint hint--lg hint--decore _text-left">
                                                <span
                                                    class="text text--size-18 _color-dark-blue">@lang('orders::site.checkout-form.who-is-receiver')</span>
                                            </div>
                                            <div class="grid _mtb-md _posr js-init"
                                                 data-accordion="{{ json_encode(['type' => 'single-checkers', 'slideTime' => 200]) }}">
                                                <div class="gcell gcell--12  _ptb-xs">
                                                    <div class="checkout-delivery-trigger"
                                                         style="display: inline-block;" {!! sprintf("data-wstabs-ns=\"%s\" data-wstabs-button=\"%s\"", 'receiver', '1') !!}>
                                                        @component('site._widgets.radio.radio', [
                                                            'classes' => '_color-gray4',
                                                            'attributes' => [
                                                                'type' => 'radio',
                                                                'name' => 'receiver',
                                                                'value' => 0,
                                                                'data-trigger-event' => 'recieverCheck'
                                                            ],
                                                        ])
                                                            @lang('orders::site.checkout-form.receiver-is-me')
                                                        @endcomponent
                                                    </div>
                                                </div>
                                                <div class="gcell gcell--12  _ptb-xs">
                                                    <div class="checkout-delivery-trigger"
                                                         style="display: inline-block;" {!! sprintf("data-wstabs-ns=\"%s\" data-wstabs-button=\"%s\"", 'receiver', '2') !!}>
                                                        @component('site._widgets.radio.radio', [
                                                            'classes' => '_color-gray4',
                                                            'attributes' => [
                                                                'type' => 'radio',
                                                                'name' => 'receiver',
                                                                'value' => 1,
                                                                'data-trigger-event' => 'recieverCheck'
                                                            ],
                                                        ])
                                                            @lang('orders::site.checkout-form.receiver-is-another-person')
                                                        @endcomponent
                                                    </div>
                                                    <div class="checkout-delivery-content _pt-def _sm-pl-xl"
                                                         style="display: none;" data-wstabs-ns="receiver"
                                                         data-wstabs-block="2">
                                                        <div class="_pb-lg ">
                                                            <div class="control control--input">
                                                                <div class="control__inner">
                                                                    <input class="control__field js-init" data-phonemask type="text"
                                                                           name="phone" id="phone" required>
                                                                    <label class="control__label"
                                                                           for="phone">@lang('orders::site.checkout-form.receiver.phone')
                                                                        *</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="_pb-lg ">
                                                            <div class="control control--input">
                                                                <div class="control__inner">
                                                                    <input class="control__field" type="text"
                                                                           name="first_name" id="name-1" required>
                                                                    <label class="control__label"
                                                                           for="name-1">@lang('orders::site.checkout-form.receiver.first-name')
                                                                        *</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="_pb-lg ">
                                                            <div class="control control--input">
                                                                <div class="control__inner">
                                                                    <input class="control__field" type="text"
                                                                           name="last_name" id="name-2" required>
                                                                    <label class="control__label"
                                                                           for="name-2">@lang('orders::site.checkout-form.receiver.last-name')
                                                                        *</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="">
                                                            <div class="control control--input">
                                                                <div class="control__inner">
                                                                    <input class="control__field" type="text"
                                                                           name="middle_name" id="name-3" required>
                                                                    <label class="control__label"
                                                                           for="name-3">@lang('orders::site.checkout-form.receiver.middle-name')</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @component('site._widgets.checker.checker', [
                                    'attributes' => [
                                            'type' => 'checkbox',
                                            'required' => false,
                                            'checked' => false
                                        ],
                                   'name' => 'no-recall',
                                   'class' => '_ptb-md _mlr-auto',
                               ])
                                    @lang('orders::site.checkout-form.no-recall')
                                @endcomponent
                                <div class="gcell gcell--12 _ptb-sm _text-center js-init"
                                     data-accordion='{"type":"single-checkers","slideTime":200"}'>
                                    <span class="link link--underline link--dark text--size-14" data-wstabs-ns="comment"
                                          data-wstabs-button="1">
                                        @lang('orders::site.checkout-form.add-comment')
                                    </span>
                                    <div class="control control--textarea _mt-lg" style="display: none;"
                                         data-wstabs-ns="comment" data-wstabs-block="1">
                                        <div class="control__inner">
                                            <textarea rows="5" class="control__field" type="text" name="comment"
                                                      id="profile-comment"></textarea>
                                            <label class="control__label"
                                                   for="profile-comment">@lang('orders::site.add-comment')</label>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="gcell gcell--12 _ptb-sm">
                                    <div class="grid _justify-center _items-center _nm-md">
                                        @if((bool)config('db.orders.do-not-call-field-exists', true))
                                            <div class="gcell gcell--auto _flex-noshrink _p-md">
                                                @component('site._widgets.checker.checker', [
                                                    'classes' => '_color-gray4',
                                                    'attributes' => [
                                                        'type' => 'checkbox',
                                                        'name' => 'do_not_call_me',
                                                        'checked' => false,
                                                        'value' => 1,
                                                    ]
                                                ])
                                                    @lang('orders::general.do-not-call-me')
                                                @endcomponent
                                            </div>
                                        @endif
                                        <div class="gcell gcell--auto _flex-grow _p-md _text-center">
                                            <div style="font-size: 12px;">{!! trans('orders::site.delivery-price') !!}</div>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="form__footer">
                            <div class="grid _justify-center _pt-def">
                                <div class="gcell _text-center">
                                    <button class="button button--theme-main button--size-small" type="submit">
                                        <span class="button__body">
                                            <span class="button__text text--size-16 _ptb-xxs">@lang('orders::site.approve')</span>
                                        </span>
                                    </button>
                                    <div class="_mt-lg">
                                        <span class="text text--blue-smoke text--size-14">
                                            @lang('orders::site.accept')
                                        </span>
                                        <a target="_blank" href="{{ config('db.basic.agreement_link_' . \Lang::getLocale()) }}"
                                           class="text--size-14 link link--dark link--underline">
                                            @lang('orders::site.agreement')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
