@php
/** @var \App\Modules\Orders\Types\StepOneType $info */
/** @var \App\Modules\Orders\Components\Cart\Cart $cart */
@endphp

@extends('site._layouts.checkout')

@push('hidden_data')
    {!! JsValidator::make($rules, $messages, $attributes, '#' . $formId) !!}
@endpush

@push('cart-route')
    <script>
        var cartRoute = 1;
    </script>
@endpush

@push('hidden_data')
    <script>
        window.LOCO_DATA.checkout = "/";
    </script>
@endpush

@push('microdata')
    <script>
        fbq('track', 'InitiateCheckout', {!! $cart->getFBPixelData() !!});
    </script>
@endpush

@section('layout-body')
    <div class="section section--gradient">
        <div class="container container--white">
            <div class="grid _justify-center _def-justify-between _def-flex-row-reverse">
                <div class="gcell gcell--12 gcell--md-8 gcell--def-6 _plr-sm _pt-sm _pb-xl _md-plr-md _md-pt-md _def-ptb-xl _def-show" data-cart-container="checkout" style="min-height: 150px">
                    {!! Widget::show('orders::cart::checkout', $totalAmount, $totalAmountOld, $services) !!}
                </div>
                <div class="gcell gcell--12 gcell--md-8 gcell--def-6 _plr-md _ptb-def _def-pl-xl _def-ptb-lg" style="background-color: #fff;">
                    <div class="title title--size-h1 title--normal _color-dark-blue _text-center _ms-text-left">@lang('orders::site.checkout')</div>
                    <div class="checkout-step">
                        <div class="checkout-step__number">
                            @lang('orders::site.step-1')
                        </div>
                        <div class="title title--size-h4 title--normal _color-dark-blue">@lang('orders::site.contact-data')</div>
                    </div>
                    {{-- <div class="hint hint--decore _text-left"><span>@lang('orders::site.step-1')</span></div> --}}
                    {{-- <div class="title title--size-h2 _mb-none">@lang('orders::site.contact-data')</div> --}}

                    @guest
                    <div class="_pt-sm _mb-sm">
                        <span class="text text--size-14 _color-blue-smoke">@lang('users::site.are-you-client')</span>
                        <a class="link link--underline link--dark text--size-14 js-init" data-mfp="inline" data-mfp-src="#popup-login-only">
                            @lang('users::site.enter')
                        </a>
                    </div>
                    @endguest
                    {{-- {!! Widget::show('are-you-client-button') !!} --}}
                    <div class="form form--checkout-step-1 _pt-xl _mt-md">
                        {!! Form::open(['route' => 'site.checkout', 'class' => ['js-init', 'ajax-form'], 'id' => $formId, 'autocomplete' => 'off']) !!}
                        <div class="form__body">
                            <div class="grid _nmtb-sm">
                                <div class="gcell gcell--12 _pb-lg">
                                    <div class="control control--input {{ $info->name ? 'has-value' : '' }}">
                                        <div class="control__inner">
                                            <input class="control__field" type="text" name="name" id="profile-name" value="{{ $info->name }}" {{ (bool)config('db.orders.name-is-required', true) ? 'required' : '' }}>
                                            <label class="control__label" for="profile-name">@lang('orders::site.name-and-last-name') {{ (bool)config('db.orders.name-is-required', true) ? '*' : '' }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="gcell gcell--12 _pb-lg">
                                    <div class="control control--input {{ $info->phone ? 'has-value' : '' }}">
                                        <div class="control__inner">
                                            <input class="control__field js-init" type="tel" name="phone" data-phonemask id="profile-phone" value="{{ $info->phone }}" {{ (bool)config('db.orders.phone-is-required', true) ? 'required' : '' }}>
                                            <label class="control__label" for="profile-phone">@lang('orders::site.validation.attributes.phone') {{ (bool)config('db.orders.phone-is-required', true) ? '*' : '' }}</label>
                                        </div>
                                    </div>
                                </div>
                                @if(config('db.orders.show-delivery', true))
                                    <div class="gcell gcell--12 _pb-lg">
                                        <div class="control control--input {{ $info->city ?? null ? 'has-value' : '' }} js-init" data-location-suggest="{{ route('site.location.suggest') }}">
                                            <div class="control__inner">
                                                <input type="text" class="_visuallyhidden" name="locationId" id="profile-location-id" value="{{ $info->locationId }}" data-suggest-location-id>
                                                <input class="control__field find-city" type="text" name="location" id="profile-location" value="{{ $info->city }}" required data-suggest-input>
                                                <label id="profile-location-id-error" class="has-error" for="profile-location-id" style="display: none;"></label>
                                                <label class="control__label" for="profile-location">@lang('orders::site.city') *</label>
                                                <div class="location-suggest-wrap" data-suggest-list></div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="gcell gcell--12 _pb-xl">
                                    <div class="control control--input {{ $info->email ? 'has-value' : '' }}">
                                        <div class="control__inner">
                                            <input class="control__field" type="email" name="email" id="profile-email" value="{{ $info->email }}" {{ (bool)config('db.orders.email-is-required', true) ? 'required' : '' }}>
                                            <label class="control__label" for="profile-email">@lang('orders::site.validation.attributes.email') {{ (bool)config('db.orders.email-is-required', true) ? '*' : '' }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form__footer">
                            <div class="grid _justify-end">
                                <div class="gcell">
                                    <div class="control control--submit">
                                        <button class="button button--theme-default-invert button--size-normal" type="submit">
                                            <span class="button__body">
                                                <span class="button__text">@lang('orders::site.next-step')</span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if(config('db.orders.show-delivery', true) && config('db.orders.show-payment', true))
                    <div class="_mt-xl is-disabled">
                        <div class="checkout-step">
                            <div class="checkout-step__number">
                                @lang('orders::site.step-2')
                            </div>
                            <div class="title title--size-h4 title--normal _color-dark-blue">@lang('orders::site.payment-and-delivery')</div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
