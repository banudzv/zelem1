@php
    /** @var \App\Modules\Orders\Models\Order $order */
    $receiver = $order->receiver ?: $order->client ?: $order->user;
@endphp

@extends('site._layouts.checkout')

@push('microdata')
    <script>
        fbq('track', 'Purchase', {!! $order->FBPixelData !!});
    </script>
@endpush

@section('layout-body')
    <div class="section section--gradient">
        <div class="container container--white _md-pb-xxl">
            <div class="grid _def-pl-xl _def-ml-xs _pt-none _ms-pt-lg">
                <div class="gcell gcell--12 gcell--def-7 _def-pr-lg">
                    <div
                        class="title title--bold title--size-h1 _text-center _ms-text-left _color-dark-blue mb-sm _ms-mb-lg">
                        {{ __('orders::site.thank-you') }}
                    </div>

                    <div class="_flex _justify-center _ms-justify-start">
                        <button class="button button--air _mb-lg" data-print="container">
                        <span class="button__body">
                            {!! SiteHelpers\SvgSpritemap::get('icon-print', [
                            'class' => 'button__icon button__icon--after'
                            ]) !!}
                            <span
                                class="button__text link link--underline link--dark">@lang('orders::site.print-order')</span>
                        </span>
                        </button>
                    </div>
                    <div class="print-container">
                        <div
                            class="title title--size-h4 title--bold _text-center _ms-text-left _color-dark-blue _mtb-sm _ms-mtb-none">
                            @lang('orders::site.order-id') №{{ $order->id }}
                        </div>
                        <div class="hint hint--decore _mt-xs _mb-lg"></div>
                        <div class="grid grid--1 _pt-xl _pb-lg _nml-sm _nmb-sm order-product--checkout">
                            @foreach($order->items as $item)
                                @php
                                    $image = $item->product ? $item->product->preview : null;
                                    $title = $item->product ? $item->product->name : null;
                                @endphp
                                <div class="gcell _pl-sm _pb-sm _mb-def">
                                    <div class="_flex _items-center">
                                        <a class="order-product__preview">
                                            {!! Widget::show('image', $image, 'small', ['class' => 'order-product__image']) !!}
                                        </a>
                                        <div class="_pl-sm _flex _flex-grow _flex-column">
                                            <div class="_mb-md _flex">
                                                <span class="title _color-dark-blue _mr-auto">{{ $title }}</span>
                                                <div class="order-product__price _ml-sm">
                                                    <span>{{ $item->quantity }} x <span
                                                            class="title _color-dark-blue">{{ $item->formatted_price }}</span></span> {{--кол-во и цена--}}
                                                </div>
                                            </div>
                                            <div class="_mt-auto">
                                                @foreach($item->services as $service)
                                                    <div class="_mb-xs _flex _justify-between">
                                                        {{ $service["service"] }}:
                                                        <span
                                                            class="title _color-dark-blue">{{ format_only($service["price"]) }}</span>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @if($order->delivery)
                                <div class="grid _justify-between _flex-nowrap _pl-sm _pb-sm">
                                    <div class="gcell order-product__spacer"></div>
                                    <div class="gcell _color-dark-blue _flex-noshrink text--semi-bold _pl-sm">
                                        @lang('orders::site.delivery-title'):
                                    </div>
                                    <div class="dots"></div>
                                    <div
                                        class="gcell _flex-noshrink  _text-right _color-blue-smoke _pl-xs text--nowrap">
                                        {{ format_only($order->delivery_price) }}
                                    </div>
                                </div>
                            @endif
                            <div class="grid _pl-sm _justify-between _flex-nowrap _items-end _pb-md">
                                <div class="gcell _text-left _color-dark-blue _flex-noshrink title title--size-h4">
                                    <span>@lang('orders::site.total-to-pay'):</span>
                                </div>
                                <div class="dots"></div>
                                <div class="gcell title title--size-h4 _color-dark-blue text--nowrap _flex-noshrink">
                                    {{ format_only($order->total_amount_as_number + $order->delivery_price) }}
                                </div>
                            </div>
                        </div>
                        <div class="grid _justify-between _flex-nowrap">
                            <div class="gcell _color-dark-blue _flex-noshrink text--semi-bold _pr-xs">
                                @lang('orders::site.created_at'):
                            </div>
                            <div class="dots"></div>
                            <div class="gcell _flex-noshrink _text-right _color-blue-smoke _pl-xs">
                                {{ $order->created_at->format('d.m.Y') }}
                            </div>
                        </div>
                        @if(config('db.orders.show-delivery', true))
                            <div class="grid _justify-between _flex-nowrap _pt-sm">
                                <div class="gcell _color-dark-blue _flex-noshrink text--semi-bold _pr-xs">
                                    @lang('orders::site.delivery-address'):
                                </div>
                                <div class="dots"></div>
                                <div class="gcell _text-right _color-blue-smoke _pl-xs">
                                    <span> {{ $order->city }}, {{ $order->delivery_address }}</span>
                                </div>
                            </div>
                        @endif
                        @if($order->payment_method)
                            <div class="grid _justify-between _flex-nowrap _pt-sm">
                                <div class="gcell _color-dark-blue _flex-noshrink text--semi-bold _pr-xs">
                                    @lang('orders::site.payment-method'):
                                </div>
                                <div class="dots"></div>
                                <div class="gcell _flex-noshrink _text-right _color-blue-smoke _pl-xs">
                                    @lang("orders::general.payment-methods.{$order->payment_method}")
                                </div>
                            </div>
                        @endif
                        @if($order->delivery)
                            <div class="grid _justify-between _flex-nowrap _pt-sm">
                                <div class="gcell _color-dark-blue _flex-noshrink text--semi-bold _pr-xs">
                                    @lang('orders::site.delivery-type'):
                                </div>
                                <div class="dots"></div>
                                <div class="gcell _xs-flex-noshrink  _text-right _color-blue-smoke _pl-xs">
                                    @lang("orders::general.deliveries.{$order->delivery}")
                                </div>
                            </div>
                        @endif
                        <div class="grid _justify-between _flex-nowrap _pt-sm">
                            <div class="gcell _color-dark-blue _flex-noshrink text--semi-bold _pr-xs">
                                @lang('orders::site.receiver'):
                            </div>
                            <div class="dots"></div>
                            <div class="gcell _flex-noshrink _text-right _color-blue-smoke _pl-xs">
                                {{ $receiver->name }}, {{ $receiver->phone }}
                            </div>
                        </div>
                    </div>
                    <div class="grid _pt-lg _pb-xxl">
                        {{--                        <div class="gcell gcell--auto">--}}
                        {{--                            <a href="{{ route('site.home') }}" class="link text--size-18 link--icon-left link--underline">--}}
                        {{--                                <div class="icon">--}}
                        {{--                                    {!! SiteHelpers\SvgSpritemap::get('icon-arrow-left') !!}--}}
                        {{--                                </div>--}}
                        {{--                                @lang('orders::general.back-main')--}}
                        {{--                            </a>--}}
                        {{--                        </div>--}}
                        @if(Auth::user())
                            <div class="gcell gcell--auto _pr-xl">
                                <a href="{{ route('site.account.orders') }}"
                                   class="link text--size-18 link--icon-left link--underline">
                                    <div class="icon">
                                        {!! SiteHelpers\SvgSpritemap::get('icon-arrow-left') !!}
                                    </div>
                                    @lang('orders::general.back-to-orders')
                                </a>
                            </div>
                        @endif
                        <div
                            class=" {{ Auth::user() ? 'gcell--12 gcell--xs-auto _xs-pl-xl _mt-lg _xs-mt-none' : 'gcell' }}">
                            <a href="categories" class="link text--size-18 link--icon-left link--underline">
                                <div class="icon">
                                    {!! SiteHelpers\SvgSpritemap::get('icon-arrow-left') !!}
                                </div>
                                @lang('orders::site.back-catalog')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
