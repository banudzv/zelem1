<!-- TODO переверстать -->
<div id="popup-one-click-buy" class="popup popup--one-click-buy" style="max-width:50em;">
    <div class="popup__container">
        <div class="popup-css delivery-status-popup">
            <div name="content">
                <div>
                    <h2>@lang('orders::site.delivery.status')</h2>
                </div>
                <div>
                    Error! Can not get status of the order.
                </div>
            </div>
        </div>
    </div>
</div>