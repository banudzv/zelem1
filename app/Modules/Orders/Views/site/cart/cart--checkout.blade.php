@php
/** @var \App\Modules\Orders\Components\Cart\Cart $cart */
/** @var float $amount */
/** @var float $amountOld */
/** @var float $defaultDeliveryPrice */
/** @var $services array|\Illuminate\Support\Collection[]|\App\Modules\Products\Models\ServicePrice[][] */
@endphp

<div class="_def-pl-lg _nmt-sm" style="min-height: 150px;">
    <div class="title title--size-h1 title--normal _color-dark-blue _def-pl-xxl">
        @lang('orders::site.your-order')
    </div>
    <div class="cart-checkout" data-cart-checkout>
        {{-- <div class="cart-checkout__head">
            <div class="cart-checkout__title">@lang('orders::site.your-order')</div>
        </div> --}}
        <div class="cart-checkout__body">
            @foreach($cart->getItems() as $cartItem)
                {!!
                    Widget::show(
                        'products::checkout',
                        $cartItem->getProductId(),
                        $cartItem->getQuantity(),
                        $cartItem->getDictionaryId(),
                        array_get($services, $cartItem->getProductId())
                    )
                !!}
            @endforeach
        </div>
        <div class="cart-checkout__footer">
            <div class="cart-checkout__total-amount">
                <div class="grid _flex-column-reverse _items-center _xxs-flex-row _justify-end _xxs-justify-between _xxs-items-end">
                    <div class="gcell gcell--auto _md-pr-sm _mb-xs"></div>
                    <div class="gcell gcell--auto _pl-sm _flex _flex-column _items-end">
                        <div><strong>Доставка:</strong> <span id="delivery-price">{{ format_only($defaultDeliveryPrice) }}</span></div>
                    </div>
                </div>
            </div>
            <div class="hint hint--decore _text-right _color-blue-smoke"><span>@lang('orders::site.total')</span></div>
            <div class="cart-checkout__total-amount">
                <div class="grid _flex-column-reverse _items-center _xxs-flex-row _justify-end _xxs-justify-between _xxs-items-end">
                    <div class="gcell gcell--auto _md-pr-sm _mb-xs">
                        <span class="link link--underline link--dark text--size-18 js-init" data-cart-trigger="edit" data-mfp>
                            @lang('orders::site.edit-order')
                        </span>
                    </div>
                    <div class="gcell gcell--auto _pl-sm _flex _flex-column _items-end">
                        @if($amount != $amountOld && $amountOld > 0)
                            <div class="cart-detailed__old-total-price">{{ format_only($amountOld + $defaultDeliveryPrice) }}</div>
                        @endif
                        <div><strong id="order-total">{{ format_only($amount + $defaultDeliveryPrice) }}</strong></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
