@php
    use App\Modules\Products\Models\Product;
    /** @var int $productId */
    /** @var Product $product*/
    $data_dictionary = [
        'ru' => [
            'buy' => trans('orders::site.buy'),
            'in-cart' => trans('orders::site.in-cart'),
        ],
        'ua' => [
            'buy' => trans('orders::site.buy'),
            'in-cart' => trans('orders::site.in-cart'),
        ],
    ];
@endphp
{{-- <div class="item-card-controls__control _def-flex-grow"> --}}

<button type="button"
        class="button button--theme-main button--buy button--radius-bottom-left {{ $withText ? 'button--size-large' : 'button--size-collapse-normal' }} button--width-full"
        data-cart-action="add"
        data-trigger-event="addToCart"
        @if(isset($product) && $product)
        onclick="fbq('trackCustom', 'AddToCart', {{ $product->FBPixelData }})"
        @endif
        data-product-id="{{ $productId }}"
        data-dictionary="{{ json_encode($data_dictionary) }}"
>
    <span class="button__body">
        {!! SiteHelpers\SvgSpritemap::get('icon-shopping', [
            'class' => 'button__icon button__icon--after'
        ]) !!}
        <span class="button__text">@lang('products::site.buy')</span>
    </span>
</button>
{{-- </div> --}}
