@php
/** @var \App\Modules\Orders\Components\Cart\Cart $cart */
/** @var array $addedProducts */
/** @var string $totalAmount */
/** @var bool $showButton */
/** @var int|null $dictionaryId */
/** @var \Illuminate\Support\Collection[]|array $services */
/** @var string|null $route */

$addedProducts = $addedProducts ?? [];
@endphp

<div class="cart-detailed {{ $cart->getTotalQuantity() > 0 ? null : 'is-empty' }}">
    <div class="cart-detailed__body">
        @if (count($addedProducts) > 0)
            <div class="cart-detailed__section">
                <div class="cart-detailed__title">@lang('orders::site.item-added-to-cart')</div>
                <div class="cart-detailed__items-list">
                    @foreach($addedProducts as $addedProduct)
                    @php($cartItem = $cart->getItem($addedProduct, $dictionaryId))
                        @if($cartItem)
                            {!!
                                Widget::show(
                                    'products::cart-item',
                                    $cartItem->getProductId(),
                                    $cartItem->getQuantity(),
                                    $cartItem->getDictionaryId(),
                                    array_get($services, $cartItem->getProductId())
                                )
                            !!}
                        @endif
                    @endforeach
                </div>
            </div>
        @endif
        @if($cart->getItems()->count() > count($addedProducts))
            <div class="cart-detailed__section">
                <div class="cart-detailed__title">{{ $addedProducts ? __('orders::site.other-items') : __('orders::site.cart') }}</div>
                <div class="cart-detailed__items-list">
                    @foreach($cart->getItems() as $cartItem)
                        @if(in_array($cartItem->getProductId(), $addedProducts) && $cartItem->getDictionaryId() === $dictionaryId)
                            @continue
                        @endif
                        {!!
                            Widget::show(
                                'products::cart-item',
                                $cartItem->getProductId(),
                                $cartItem->getQuantity(),
                                $cartItem->getDictionaryId(),
                                array_get($services, $cartItem->getProductId())
                            )
                        !!}
                    @endforeach
                </div>
            </div>
        @endif
        <div class="cart-detailed__summary">
            <div class="hint hint--decore _text-right _color-blue-smoke"><span>@lang('orders::site.total')</span></div>
            @if($totalAmountOld != $totalAmount && (float)$totalAmountOld > 0)
                <div class="cart-detailed__old-total-price">{{ $totalAmountOld }}</div>
            @endif
            <div class="cart-detailed__total-price"><strong>{{ $totalAmount }}</strong></div>
        </div>
    </div>
    <div class="cart-detailed__footer">
        <div class="grid _items-center _flex-column _md-flex-row _justify-center _justify-between">
            <div class="gcell  gcell--md-auto _flex-noshrink _mb-sm _md-mb-none">
                @if (!$route)
                    <button class="button button--theme-default-invert button--size-large" data-cart-continue data-cart-trigger="close">
                        <span class="button__body">
                            <span class="button__text">@lang('categories::general.buy-something')</span>
                        </span>
                    </button>
                @endif
            </div>
            <div class="gcell _md-ml-auto gcell--md-auto _md-pl-md _flex-noshrink">
                {!! Widget::show('orders::checkout-button', $route) !!}
            </div>
        </div>
    </div>
</div>
