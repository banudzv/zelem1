@php
/** @var \CustomForm\Builder\Form $form */
$url = route('admin.zalem.cities');
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['method' => 'POST', 'url' => $url]) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop
