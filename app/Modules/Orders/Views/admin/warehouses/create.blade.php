@php
/** @var \CustomForm\Builder\Form $form */
$form->buttons->showCloseButton(route('admin.warehouses.index'));
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => ['admin.warehouses.store']]) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop
