@php
/** @var $warehouses \App\Modules\Orders\Models\WarehouseTranslates[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>@lang('orders::column.city')</th>
                        <th>@lang('orders::column.address')</th>
                        <th></th>
                    </tr>
                    @foreach($warehouses AS $warehouse)
                        <tr>
                            <td>{{ $warehouse->city }}</td>
                            <td>{{ $warehouse->address }}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.warehouses.edit', [$warehouse->row_id]) !!}
                                {!! \App\Components\Buttons::delete('admin.warehouses.destroy', $warehouse->row_id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $warehouses->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
