@php
/** @var \App\Modules\Orders\Models\Order $order */
$customer = $order->client ?: $order->user;
@endphp
<address>
    <strong>{{ $customer->name }}</strong><br>
    @if($customer->phone)
        @lang('orders::general.phone'): {{$customer->phone}}<br>
    @endif
    @if($customer->email)
        @lang('orders::general.email'): {{ $customer->email }}
    @endif
</address>