@php
/** @var \App\Modules\Orders\Models\Order $order */
$showAddress = $showAddress ?? true;
$receiver = $order->receiver ?: $order->client ?: $order->user;
@endphp
<address>
    <strong>{{ $receiver->name }}</strong><br>
    {{ $order->city }}<br>
    {{ $order->delivery_address }}<br>
    @if($receiver->phone)
        @lang('orders::general.phone'): {{ $receiver->phone }}<br>
    @endif
    @if(isset($receiver->email) && $receiver->email)
        @lang('orders::general.email'): {{ $receiver->email }}
    @endif
</address>
