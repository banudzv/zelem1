@php
/** @var \App\Modules\Orders\Models\Order $order */
$showAddress = $showAddress ?? true;
$receiver = $order->receiver ?: $order->client ?: $order->user;
@endphp
<address>
    <strong>{{ $receiver->name }}</strong><br>
    {{ $order->city }}<br>
    {{ $order->delivery_address }}<br>
    @if($receiver->phone)
        @lang('orders::general.phone'): {!! Html::link("tel:{$receiver->cleared_phone}", $receiver->phone) !!}<br>
    @endif
    @if(isset($receiver->email) && $receiver->email)
        @lang('orders::general.email'): {!! Html::mailto($receiver->email) !!}
    @endif
</address>
