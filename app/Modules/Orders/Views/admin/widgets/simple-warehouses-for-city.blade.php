@if(!empty($warehouses))
    @foreach($warehouses as $id => $name)
        <option value="{{ $id }}">{{ $name }}</option>
    @endforeach
@else
    <option>Ничего не найдено</option>
@endif

