<?php

namespace App\Modules\Orders\Forms;

use App\Components\Delivery\JustIn;
use App\Components\Delivery\NovaPoshta;
use App\Core\Interfaces\FormInterface;
use App\Modules\Orders\Models\Order;
use App\Modules\Orders\Models\WarehouseTranslates;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\Select;
use CustomForm\Text;
use CustomForm\TextArea;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Widget, Html;

/**
 * Class OrderForm
 *
 * @package App\Modules\Orders\Forms
 */
class OrderForm implements FormInterface
{

    /**
     * @param Model|null $order
     * @return Form
     * @throws \App\Exceptions\WrongParametersException|\Exception
     */
    public static function make(?Model $order = null): Form
    {
        $order = $order ?? new Order;
        $client = ($order && $order->exists) ? $order->client : null;
        $form = Form::create();
        $tabs = $form->tabs();

        if ($order && $order->exists) {
            if ($order->user_id) {
                $user = Text::create('user')
                    ->setDefaultValue(Widget::show('short-user-information', $order->user));
            } else {
                $user = Text::create('user')
                    ->setDefaultValue(
                        Html::tag(
                            'div',
                            trans('orders::general.no-user-selected'),
                            [
                                'class' => ['form-group', 'text-bold', 'text-red']
                            ]
                        )
                    );
            }
        } else {
            $user = Text::create('user')
                ->setDefaultValue(Widget::show('live-search-user'));
        }
    
        $novaPoshta = new NovaPoshta();
        $cities = [];
        foreach ($novaPoshta->getCities()->data as $city) {
            $cities[$city->Ref] = $city->DescriptionRu;
        }
        
        $tabs->createTab('Шаг 1. Контактная информация')->fieldSet()->add(
            $user,
            Input::create('name', $client)
                ->setLabel('validation.attributes.first_name')
                ->setOptions([(bool)config('db.orders.name-is-required', true) ? 'required' : 'nullable']),
            Input::create('email', $client)
                ->setType('email')
                ->setOptions([(bool)config('db.orders.email-is-required', true) ? 'required' : 'nullable']),
            Input::create('phone', $client)
                ->setOptions([(bool)config('db.orders.phone-is-required', true) ? 'required' : 'nullable']),
            Select::create('locationId')
                ->setLabel(trans('orders::general.sender-city'))
                ->setPlaceholder('&mdash;')
                ->addClasses('get-all-warehouses')
                ->setOptions([
                    'href' => route('admin.all-warehouses.city'),
                    'data-np' => '#whNp',
                    'data-justin' => '#whJustin',
                    'data-zalem' => '#whZalem',
                ])
                ->add($cities)
                ->setValue($order->city_ref)
        );
        $deliveries = array_keys(config('orders.deliveries', []));
        foreach ($deliveries as $key => $delivery) {
            unset($deliveries[$key]);
            $deliveries[$delivery] = "orders::general.deliveries.$delivery";
        }
        $paymentMethods = array_keys(config('orders.payment-methods', []));
        foreach ($paymentMethods as $key => $paymentMethod) {
            unset($paymentMethods[$key]);
            $paymentMethods[$paymentMethod] = "orders::general.payment-methods.$paymentMethod";
        }
        $other = [];
        foreach ((array)config('orders.rest-deliveries', []) as $deliveryType) {
            $other[$deliveryType] = $deliveryType;
        }
        $senderWarehouses = [];
        if ($order->city_ref || old('locationId')) {
            foreach ((array)$novaPoshta->getWarehousesCityRef($order->city_ref ?: old('locationId'))->data as $warehouse) {
                $senderWarehouses[$warehouse->Ref] = $warehouse->DescriptionRu;
            }
        }
        $zalemWarehouses = WarehouseTranslates::getWarehousesListByCityReference($order->city_ref ?: old('locationId'));
        $justinWarehouses = self::getJustinWarehousesForSelect($cities, $order->city_ref ?: old('locationId'));
        $delivery = old('delivery', $order->delivery);
        $tabs->createTab('Шаг 2. Доставка и оплата')->fieldSet()->add(
            Select::create('delivery', $order)
                ->addClasses('hidden-block-trigger')
                ->setLabel('orders::general.delivery-type')
                ->setPlaceholder('&mdash;')
                ->add($deliveries),
            Input::create('zalem-address')
                ->addClassesToDiv('hidden-block')
                ->addClassesToDiv(($delivery === 'zalem-address') ? 'show-hidden-block' : '')
                ->setValue($order->delivery_address)
                ->setLabel('orders::general.delivery-address'),
            Input::create('nova-poshta')
                ->addClassesToDiv('hidden-block')
                ->addClassesToDiv(($delivery === 'nova-poshta') ? 'show-hidden-block' : '')
                ->setValue($order->delivery_address)
                ->setLabel('orders::general.delivery-address'),
            Select::create('zalem-warehouse')
                ->addClassesToDiv('hidden-block')
                ->addClassesToDiv(($delivery === 'zalem-warehouse') ? 'show-hidden-block' : '')
                ->setLabel(trans('orders::general.zalem-warehouse'))
                ->setOptions(['id' => 'whZalem'])
                ->add($zalemWarehouses)
                ->setValue($order->warehouse_ref)
                ->setPlaceholder('&mdash;'),
            Select::create('justin-warehouse')
                ->addClassesToDiv('hidden-block')
                ->addClassesToDiv(($delivery === 'justin-warehouse') ? 'show-hidden-block' : '')
                ->setLabel(trans('orders::general.justin-warehouse'))
                ->add($justinWarehouses)
                ->setOptions(['id' => 'whJustin'])
                ->setValue($order->warehouse_ref)
                ->setPlaceholder('&mdash;'),
            Input::create('address')
                ->addClassesToDiv('hidden-block')
                ->addClassesToDiv(($delivery === 'address') ? 'show-hidden-block' : '')
                ->setValue($order->delivery_address)
                ->setLabel('orders::general.delivery-address'),
            Select::create('nova-poshta-self')
                ->addClassesToDiv('hidden-block')
                ->addClassesToDiv(($delivery === 'nova-poshta-self') ? 'show-hidden-block' : '')
                ->setLabel(trans('orders::general.sender-warehouse'))
                ->setOptions(['id' => 'whNp'])
                ->add($senderWarehouses)
                ->setValue($order->warehouse_ref)
                ->setPlaceholder('&mdash;'),
            Input::create('other')
                ->addClassesToDiv('hidden-block')
                ->addClassesToDiv(($delivery === 'other') ? 'show-hidden-block' : '')
                ->setValue($order->delivery_address)
                ->setLabel('orders::general.delivery-address'),
            Select::create('payment_method', $order)
                ->setLabel('orders::general.payment-method')
                ->add($paymentMethods)
                ->setPlaceholder('&mdash;'),
            ($order->exists && $order->paid_auto) ?
                Text::create()
                    ->setDefaultValue(
                        \Html::tag(
                            'div',
                            trans('orders::general.paid-auto'),
                            ['class' => ['form-group', 'text-red', 'text-bold']]
                        )
                    ) :
                Toggle::create('paid', $order)
                    ->setLabel('orders::general.paid')
                    ->setDefaultValue(false),
            TextArea::create('comment', $order)->setOptions(['rows' => 3]),
            (bool)config('db.orders.do-not-call-field-exists', true)
                ? Toggle::create('do_not_call_me', $order)
                    ->setLabel('orders::general.do-not-call-me')
                    ->setDefaultValue(true)
                : null,
            Input::create('e_invoice')
                ->setValue($order->e_invoice)
                ->setLabel('orders::general.e-invoice'),
            Input::create('ttn', $order)
                ->setLabel('orders::general.ttn-number'),
            Toggle::create('use_receiver')->setDefaultValue(isset($order->receiver))->setLabel('Указать получателя?'),
            Input::create('receiver[phone]')->setValue($order->receiver->phone ?? null),
            Input::create('receiver[first_name]')->setValue($order->receiver->first_name ?? null),
            Input::create('receiver[last_name]')->setValue($order->receiver->last_name ?? null),
            Input::create('receiver[middle_name]')->setValue($order->receiver->middle_name ?? null)
        );

        $tabs->createTab('Шаг 3. Товары')
            ->fieldSetForView(
                'orders::admin.orders.items.list',
                ['order' => $order]
            );
        return $form;
    }

    protected static function getJustinWarehousesForSelect(array $cities, ?string $cityRef): array
    {
        if (!$cityRef || !($city = array_get($cities, $cityRef))) {
            return [];
        }
        $justin = new JustIn();
        $data = $justin->getDepartmentsByCityName($city);
        $warehouses = [];
        foreach ($data as $datum) {
            $warehouses[$datum['id']] = $datum['descr'] . ': ' . $datum['address'];
        }

        return $warehouses;
    }
}
