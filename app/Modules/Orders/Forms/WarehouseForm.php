<?php

namespace App\Modules\Orders\Forms;

use App\Components\Delivery\NovaPoshta;
use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Orders\Models\Warehouse;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WarehouseForm
 * @package App\Modules\Orders\Forms
 */
class WarehouseForm implements FormInterface
{
    /**
     * @param Model|Warehouse|null $warehouse
     * @return Form
     * @throws WrongParametersException
     * @throws \Exception
     */
    public static function make(?Model $warehouse = null): Form
    {
        $warehouse = $warehouse ?? new Warehouse;
        $form = new Form();
        $form->fieldSet()->add(
            Select::create('city_ref')->add(self::getCities())
                ->setLabel(trans('orders::column.city'))
                ->setValue($warehouse->city_ref)->required()
        );
        $form->fieldSetForLang()->add(
            Input::create('address', $warehouse)->setLabel(trans('orders::column.address'))
                ->required()
        );

        return $form;
    }

    /**
     * Returns cities list.
     *
     * @return array
     */
    protected static function getCities(): array
    {
        $novaPoshta = new NovaPoshta();
        $cities = [];
        foreach ((array)$novaPoshta->getCities()->data as $wh) {
            $cities[$wh->Ref] = $wh->DescriptionRu;
        }

        return $cities;
    }
}
