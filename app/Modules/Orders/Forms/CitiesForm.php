<?php

namespace App\Modules\Orders\Forms;

use App\Components\Delivery\NovaPoshta;
use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use CustomForm\Builder\Form;
use CustomForm\Macro\MultiSelect;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CitiesForm
 * @package App\Modules\Orders\Forms
 */
class CitiesForm implements FormInterface
{
    /**
     * Fake method
     */
    public static function make(?Model $warehouse = null): Form
    {
        return new Form();
    }

    /**
     * Actually a form.
     *
     * @param array $chosenCities
     * @return Form
     * @throws WrongParametersException
     */
    public static function html(array $chosenCities): Form
    {
        $form = new Form();
        $form->fieldSet()->add(
            MultiSelect::create('city_ref[]')->add(self::getCities())
                ->setLabel(trans('orders::column.city'))
                ->setValue($chosenCities)->required()
        );
        $form->buttons->doNotShowSaveAndCloseButton();
        $form->buttons->doNotShowSaveAndAddButton();

        return $form;
    }

    /**
     * Returns cities list.
     *
     * @return array
     */
    protected static function getCities(): array
    {
        $novaPoshta = new NovaPoshta();
        $cities = [];
        foreach ((array)$novaPoshta->getCities()->data as $wh) {
            $cities[$wh->Ref] = $wh->DescriptionRu;
        }

        return $cities;
    }
}
