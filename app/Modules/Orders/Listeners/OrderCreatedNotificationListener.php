<?php

namespace App\Modules\Orders\Listeners;

use App\Components\Crm\Amo;
use App\Components\Crm\Bitrix;
use App\Core\Interfaces\ListenerInterface;
use App\Core\Modules\Notifications\Models\Notification;
use App\Modules\Orders\Types\OrderType;
use App\Modules\Products\Models\Product;
use Catalog;

/**
 * Class OrderCreatedNotificationListener
 *
 * @package App\Modules\Orders\Listeners
 */
class OrderCreatedNotificationListener implements ListenerInterface
{
    const NOTIFICATION_TYPE = 'order';

    const NOTIFICATION_ICON = 'fa fa-cart-plus';

    public static function listens(): string
    {
        return 'orders::created';
    }

    public function handle(OrderType $order)
    {
        if (config('db.amo_crm.use_amo')) {
            $this->addInAmoCrm($order);
        }
        $this->sendBitrix($order);
        Notification::send(
            static::NOTIFICATION_TYPE,
            'orders::general.notification',
            'admin.orders.show',
            ['order' => $order->id]
        );
    }

    /**
     * @param OrderType $order
     */
    public function addInAmoCrm(OrderType $order)
    {
        $data = [];
        $data['formName'] = __('orders::general.notification').' Id-'. $order->items->implode('productId',', Id- ');
        $data['name'] = $order->clientName ?: $order->clientPhone;
        $data['phone'] = $order->clientPhone;
        $data['email'] = $order->clientEmail;
        $data['amount'] = $order->total;
        $data['text'] = $order->comment;
        $amo = new Amo();
        $amo->newLead($data);
    }

    /**
     * @param OrderType $order
     */
    public function sendBitrix(OrderType $order)
    {
        $data = [];
        $data['formName'] = __('orders::general.notification').' Id-'. $order->items->implode('productId',', Id- ');
        $data['requestType'] = 'order';
        $data['name'] = $order->clientName ?: $order->clientPhone;
        $data['phone'] = $order->clientPhone;
        $data['email'] = $order->clientEmail;
        $data['amount'] = Catalog::currency()->format($order->total);
        $data['city'] = $order->city;
        $data['delivery_address'] = $order->deliveryAddress;
        $data['payment_method'] = __(config('orders.payment-methods', [])[$order->paymentMethod]);
        $data['delivery_type'] = __(config('orders.deliveries', [])[$order->deliveryType]);
        $data['text'] = $order->comment;
        foreach($order->items as $item){
            $product = Product::whereId($item->productId)->first();

            $data['items'][$item->productId] = [
                'link' => $product->getSiteLinkAttribute(),
                'name' => $product->name,
                'price' => Catalog::currency()->format($item->price),
                'artikul' => $product->vendor_code,
                'quantity' => $item->quantity,
                ];
        }

        $bitrix = new Bitrix();
        $bitrix->send($data);
    }
}
