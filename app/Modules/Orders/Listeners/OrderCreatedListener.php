<?php

namespace App\Modules\Orders\Listeners;

use App\Core\Interfaces\ListenerInterface;
use App\Modules\Orders\Jobs\OrderMailsJob;
use App\Modules\Orders\Types\OrderType;
use Cart;

/**
 * Class OrderCreatedListener
 *
 * @package App\Modules\Orders\Listeners
 */
class OrderCreatedListener implements ListenerInterface
{
    public static function listens(): string
    {
        return 'orders::created';
    }

    /**
     * Handle the event.
     *
     * @param OrderType $orderType
     * @return void
     */
    public function handle(OrderType $orderType)
    {
        Cart::delete();
        OrderMailsJob::dispatch($orderType)->onQueue('emails');
    }

}
