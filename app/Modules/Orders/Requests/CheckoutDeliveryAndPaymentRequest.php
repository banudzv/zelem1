<?php

namespace App\Modules\Orders\Requests;

use App\Core\Interfaces\RequestInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CheckoutContactInformationRequest
 *
 * @package App\Modules\Orders\Requests
 */
class CheckoutDeliveryAndPaymentRequest extends FormRequest implements RequestInterface
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'comment' => ['nullable', 'string', 'max:191'],
            'do_not_call_me' => ['nullable', 'boolean'],
        ];
        if (config('db.orders.show-delivery', true)) {
            $rules = array_merge($rules, [
                'delivery' => ['required', Rule::in(array_keys(config('orders.deliveries', [])))],
                'zalem-warehouse' => ['required_if:delivery,zalem-warehouse', 'nullable', Rule::exists('zalem_warehouses', 'id')],
                'zalem-address.street' => ['required_if:delivery,zalem-address', 'nullable', 'string', 'max:191'],
                'zalem-address.house' => ['required_if:delivery,zalem-address', 'nullable', 'string', 'max:20'],
                'zalem-address.flat' => ['nullable', 'integer', 'min:0', 'nullable', 'max:1000'],
                'nova-poshta-self' => ['nullable', 'required_if:delivery,nova-poshta-self', 'nullable', 'string', 'max:191'],
                'nova-poshta.street' => ['required_if:delivery,nova-poshta', 'nullable', 'string', 'max:191'],
                'nova-poshta.house' => ['required_if:delivery,nova-poshta', 'nullable', 'string', 'max:20'],
                'nova-poshta.flat' => ['nullable', 'integer', 'min:0', 'nullable', 'max:1000'],
                'address.street' => ['required_if:delivery,other', 'nullable', 'string', 'max:191'],
                'address.house' => ['required_if:delivery,other', 'nullable', 'string', 'max:20'],
                'address.flat' => ['nullable', 'integer', 'min:0', 'nullable', 'max:1000'],
                'other' => ['nullable', 'required_if:delivery,other', 'nullable', Rule::in(array_keys(config('orders.rest-deliveries', [])))],
            ]);
        }
        if (config('db.orders.show-payment', true)) {
            $rules['payment_method'] = ['required', Rule::in(array_merge(array_keys(config('orders.payment-methods', [])), ['business']))];
        }
        if (request()->expectsJson()) {
            $regex = 'regex:/^[a-zA-Zа-яА-ЯіІїЇєёЁЄґҐĄąĆćĘęŁłŃńÓóŚśŹźŻż\.\'\`\-]*$/u';
        } else {
            $regex = 'regex:/^[a-zA-Zа-яА-ЯіІїЇєёЁЄґҐĄąĆćĘęŁłŃńÓóŚśŹźŻż\.\'\`\-]*$/';
        }

        return array_merge($rules, [
            'phone' => ['required_if:receiver,1', 'nullable', 'regex:/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/', 'string', 'max:191'],
            'first_name' => ['required_if:receiver,1', 'nullable', $regex, 'string', 'max:191'],
            'last_name' => ['required_if:receiver,1', 'nullable', $regex, 'string', 'max:191'],
            'middle_name' => ['nullable', $regex, 'string', 'max:191'],
            'business.company' => ['required_if:payment_method,business', 'nullable', 'string', 'max:191'],
            'business.egrpu' => ['required_if:payment_method,business', 'nullable', 'integer'],
        ]);
    }

    public function messages()
    {
        return [
            'required_if' => 'Поле обязательно для заполнения',
        ];
    }
}
