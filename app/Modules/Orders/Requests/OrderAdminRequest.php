<?php

namespace App\Modules\Orders\Requests;

use App\Core\Interfaces\RequestInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ArticleRequest
 *
 * @package App\Modules\Articles\Requests
 */
class OrderAdminRequest extends FormRequest implements RequestInterface
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $className = config('auth.providers.users.model');
        $table = $className ? (new $className)->getTable() : null;
        if (request()->expectsJson()) {
            $regex = 'regex:/^[a-zA-Zа-яА-ЯіІїЇєёЁЄґҐĄąĆćĘęŁłŃńÓóŚśŹźŻż\.\'\`\-]*$/u';
        } else {
            $regex = 'regex:/^[a-zA-Zа-яА-ЯіІїЇєёЁЄґҐĄąĆćĘęŁłŃńÓóŚśŹźŻż\.\'\`\-]*$/';
        }

        return [
            'user_id' => ['nullable', 'integer', $table ? Rule::exists($table, 'id') : 'min:1'],

            'name' => [(bool)config('db.orders.name-is-required', true) ? 'required' : 'nullable', 'string', 'max:191'],
            'email' => [(bool)config('db.orders.email-is-required', true) ? 'required' : 'nullable', 'string', 'email', 'max:191'],
            'phone' => [(bool)config('db.orders.phone-is-required', true) ? 'required' : 'nullable', 'regex:/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/', 'string', 'max:191'],
            'locationId' => ['nullable', 'string', 'max:191'],

            'delivery' => ['nullable', Rule::in(array_keys(config('orders.deliveries', [])))],
            'justin-warehouse' => ['nullable', 'required_if:delivery,justin-warehouse', 'nullable', 'string', 'max:191'],
            'zalem-warehouse' => ['nullable', 'required_if:delivery,zalem-warehouse', 'nullable', Rule::exists('zalem_warehouses', 'id')],
            'nova-poshta-self' => ['nullable', 'required_if:delivery,nova-poshta-self', 'nullable', 'string', 'max:191'],
            'nova-poshta' => ['nullable', 'required_if:delivery,nova-poshta', 'nullable', 'string', 'max:191'],
            'address' => ['nullable', 'required_if:delivery,address', 'nullable', 'string', 'max:191'],
            'zalem-address' => ['nullable', 'required_if:delivery,zalem-address', 'nullable', 'string', 'max:191'],
            'other' => ['nullable', 'required_if:delivery,other', 'string', 'max:191'],
            'payment_method' => ['nullable', Rule::in(array_keys(config('orders.payment-methods', [])))],
            'comment' => ['nullable', 'string', 'max:191'],
            'do_not_call_me' => ['nullable', 'boolean'],
            'paid' => ['nullable', 'boolean'],
            'ttn' => ['nullable', 'string'],

            'receiver.phone' => ['required_if:use_receiver,1', 'nullable', 'regex:/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/', 'string', 'max:191'],
            'receiver.first_name' => ['required_if:use_receiver,1', 'nullable', $regex, 'string', 'max:191'],
            'receiver.last_name' => ['required_if:use_receiver,1', 'nullable', $regex, 'string', 'max:191'],
            'receiver.middle_name' => ['nullable', $regex, 'string', 'max:191'],
        ];
    }

    public function attributes(): array
    {
        return [
            'locationId' => trans('validation.attributes.city'),
        ];
    }

    public function messages()
    {
        return [
            'required_if' => 'Поле обязательно для заполнения',
        ];
    }
}
