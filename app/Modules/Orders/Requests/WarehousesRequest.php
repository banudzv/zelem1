<?php

namespace App\Modules\Orders\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class WarehousesRequest
 * @package App\Modules\Orders\Requests
 */
class WarehousesRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;

    /**
     * {@inheritDoc}
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function rules(): array
    {
        $npLocationsRegex = '/^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/';

        return $this->generateRules([
            'city_ref' => ['required', 'string', 'regex:' . $npLocationsRegex],
        ], [
            'address' => ['required', 'string', 'max:191'],
        ]);
    }
}
