<?php

namespace App\Modules\Orders\Requests;

use App\Core\Interfaces\RequestInterface;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ZalemCitiesRequest
 * @package App\Modules\Orders\Requests
 */
class ZalemCitiesRequest extends FormRequest implements RequestInterface
{
    /**
     * {@inheritDoc}
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function rules(): array
    {
        $npLocationsRegex = '/^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/';

        return [
            'city_ref' => ['array'],
            'city_ref.*' => ['string', 'regex:' . $npLocationsRegex],
        ];
    }
}
