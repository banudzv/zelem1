<?php

namespace App\Modules\Orders\Jobs;

use App\Components\Mailer\MailSender;
use App\Core\Modules\Mail\Models\MailTemplate;
use App\Modules\Orders\Models\Order;
use App\Modules\Orders\Types\OrderType;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class OrderMailsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var OrderType
     */
    protected $orderType;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrderType $orderType)
    {
        $this->orderType = $orderType;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->orderType->clientEmail) {
            $template = MailTemplate::getTemplateByAlias(Order::MAIL_TEMPLATE_ORDER_CREATED);
            if ($template) {
                MailSender::send(
                    $this->orderType->clientEmail,
                    $template->current->subject,
                    $template->current->text,
                    'orders::mail.orders',
                    [
                        'orderType' => $this->orderType,
                    ]
                );
            }
        }
        if (config('db.basic.admin_email')) {
            $templateAdmin = MailTemplate::getTemplateByAlias(Order::MAIL_TEMPLATE_ORDER_CREATED_ADMIN);
            if ($templateAdmin) {
                $from = [
                    '{admin_href}',
                ];
                $to = [
                    route('admin.orders.edit', ['id' => $this->orderType->id])
                ];
                $subject = str_replace($from, $to, $templateAdmin->current->subject);
                $body = str_replace($from, $to, $templateAdmin->current->text);
                MailSender::send(
                    config('db.basic.admin_email'),
                    $subject,
                    $body,
                    'orders::mail.orders',
                    [
                        'orderType' => $this->orderType,
                    ]
                );
            }
        }
    }
}
