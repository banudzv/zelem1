<?php

namespace App\Modules\Orders\Controllers\Admin;

use App\Components\Delivery\JustIn;
use App\Components\Delivery\NovaPoshta;
use App\Core\AjaxTrait;
use App\Core\AdminController;
use App\Modules\Orders\Models\Order;
use App\Modules\Orders\Models\WarehouseTranslates;
use Illuminate\Http\Request;

/**
 * Class AjaxController
 *
 * @package App\Modules\Orders\Controllers\Admin
 */
class AjaxController extends AdminController
{
    use AjaxTrait;
    
    public function togglePaid(Request $request, Order $order)
    {
        $order->paid = !$order->paid;
        $order->save();
        
        return $this->successJsonAnswer([
            'paid' => $order->paid,
        ]);
    }

    public function getWarehousesForCity(Request $request)
    {
        $cityRef = $request->input('cityRef');
        if(!$cityRef){
            return [];
        }
        $novaPoshta = new NovaPoshta();
        $warehouses = $novaPoshta->getWarehousesCityRef($cityRef)->data;

        return $this->successJsonAnswer([
            'html' => view('orders::admin.widgets.warehouses-for-city', ['warehouses' => $warehouses])->render(),
        ]);
    }

    public function getAllWarehousesForCity(Request $request)
    {
        $cityRef = $request->input('cityRef');
        if(!$cityRef){
            return [];
        }
        $novaPoshta = new NovaPoshta();
        $warehouses = $novaPoshta->getWarehousesCityRef($cityRef)->data;
        $npCity = (new NovaPoshta)->getCityByReference($cityRef);
        if ($npCity && $npCity->success && count($npCity->data)) {
            $response = (new JustIn())->getDepartmentsByCityName($npCity->data[0]->DescriptionRu);
            $justinWarehouses = [];
            foreach ($response ?? [] as $datum) {
                $justinWarehouses[$datum['id']] = $datum['descr'] . ': ' . $datum['address'];
            }
        }
        $zalemWarehouses = WarehouseTranslates::getWarehousesListByCityReference($cityRef);

        return $this->successJsonAnswer([
            'np' => view('orders::admin.widgets.warehouses-for-city', ['warehouses' => $warehouses])->render(),
            'justin' => view('orders::admin.widgets.simple-warehouses-for-city', ['warehouses' => $justinWarehouses ?? []])->render(),
            'zalem' => view('orders::admin.widgets.simple-warehouses-for-city', ['warehouses' => $zalemWarehouses])->render(),
        ]);
    }
}
