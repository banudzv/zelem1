<?php

namespace App\Modules\Orders\Controllers\Admin;

use App\Components\Delivery\NovaPoshta;
use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Orders\Forms\WarehouseForm;
use App\Modules\Orders\Models\Warehouse;
use App\Modules\Orders\Models\WarehouseTranslates;
use App\Modules\Orders\Requests\WarehousesRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class WarehousesController
 * @package App\Modules\Orders\Controllers\Admin
 */
class WarehousesController extends AdminController
{
    /**
     * @var int limit of the warehouses on the page.
     */
    protected $limit = 20;

    /**
     * WarehousesController constructor.
     */
    public function __construct()
    {
        \Seo::breadcrumbs()->add(
            'orders::seo.warehouses.index',
            RouteObjectValue::make('admin.warehouses.index')
        );
    }

    /**
     * The page with the list of warehouses.
     *
     * @return Application|Factory|View
     * @throws \Exception
     */
    public function index()
    {
        $this->addCreateButton('admin.warehouses.create');
        \Seo::meta()->setH1('orders::seo.warehouses.index');
        $warehouses = WarehouseTranslates::whereLanguage(\Lang::getLocale())
            ->oldest('city')->paginate($this->limit);

        return view('orders::admin.warehouses.index', compact('warehouses'));
    }

    /**
     * Warehouse creating page.
     *
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function create()
    {
        \Seo::breadcrumbs()->add('orders::seo.warehouses.create');
        \Seo::meta()->setH1('orders::seo.warehouses.create');
        $form = WarehouseForm::make();

        return view('orders::admin.warehouses.create', compact('form'));
    }

    /**
     * Creates the warehouse.
     *
     * @param WarehousesRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     * @throws \Exception
     */
    public function store(WarehousesRequest $request)
    {
        try {
            $this->mergeRequestWithTheCity($request);
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
        $wh = new Warehouse();
        if ($message = $wh->createRow($request)) {
            return $this->afterFail($message);
        }

        return $this->afterStore(['id' => $wh->id]);
    }

    /**
     * Editing warehouse page.
     *
     * @param Warehouse $warehouse
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function edit(Warehouse $warehouse)
    {
        \Seo::breadcrumbs()->add('orders::seo.warehouses.edit');
        \Seo::meta()->setH1('orders::seo.warehouses.edit');
        $form = WarehouseForm::make($warehouse);

        return view('orders::admin.warehouses.update', compact('form'));
    }

    /**
     * Updates warehouse in the database.
     *
     * @param WarehousesRequest $request
     * @param Warehouse $warehouse
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function update(WarehousesRequest $request, Warehouse $warehouse)
    {
        try {
            $this->mergeRequestWithTheCity($request);
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
        if ($message = $warehouse->updateRow($request)) {
            return $this->afterFail($message);
        }

        return $this->afterUpdate();
    }

    /**
     * Deletes warehouse from the database.
     *
     * @param Warehouse $warehouse
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(Warehouse $warehouse)
    {
        try {
            $warehouse->delete();

            return $this->afterDestroy();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    /**
     * Adds city name to the request from NovaPoshta API
     *
     * @param Request $request
     * @throws \ErrorException
     */
    protected function mergeRequestWithTheCity(Request $request): void
    {
        $data = (new NovaPoshta())->getCityByReference($request->input('city_ref'))->data;
        if (!$data) {
            throw new \ErrorException('Wrong city! NovaPoshta returns empty result.');
        }
        $city = $data[0];
        foreach (config('languages', []) as $lang => $_) {
            if ($lang === 'ru' && $request->input('ru')) {
                $ru = $request->input('ru') + ['city' => $city->DescriptionRu];
                $request->merge(['ru' => $ru]);
            } else {
                $ua = $request->input($lang) + ['city' => $city->Description];
                $request->merge([$lang => $ua]);
            }
        }
    }
}
