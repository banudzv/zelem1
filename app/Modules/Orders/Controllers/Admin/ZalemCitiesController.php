<?php

namespace App\Modules\Orders\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Orders\Forms\CitiesForm;
use App\Modules\Orders\Models\City;
use App\Modules\Orders\Requests\ZalemCitiesRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class WarehousesController
 * @package App\Modules\Orders\Controllers\Admin
 */
class ZalemCitiesController extends AdminController
{
    /**
     * The page with the list of warehouses.
     *
     * @return Application|Factory|View
     * @throws \Exception
     */
    public function index()
    {
        \Seo::breadcrumbs()->add('orders::seo.cities.index');
        \Seo::meta()->setH1('orders::seo.cities.index');
        $form = CitiesForm::html(City::pluck('city_ref')->toArray());

        return view('orders::admin.cities.index', compact('form'));
    }

    /**
     * Updates warehouse in the database.
     *
     * @param ZalemCitiesRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     */
    public function update(ZalemCitiesRequest $request)
    {
        $stored = City::pluck('city_ref')->toArray();
        $new = $request->input('city_ref', []);
        City::whereIn('city_ref', array_diff($stored, $new))->delete();
        foreach (array_diff($new, $stored) as $ref) {
            City::create(['city_ref' => $ref]);
        }

        return $this->customRedirect('admin.zalem.cities', [], 'admin.messages.data-updated');
    }
}
