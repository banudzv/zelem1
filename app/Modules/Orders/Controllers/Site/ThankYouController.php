<?php

namespace App\Modules\Orders\Controllers\Site;

use App\Core\SiteController;
use App\Modules\Orders\Models\Order;
use Catalog;
use Illuminate\Http\Request;

/**
 * Class ThankYouController
 *
 * @package App\Modules\Orders\Controllers\Site
 */
class ThankYouController extends SiteController
{
    public function show(Request $request)
    {
        $orderId = $request->session()->pull('thank-you');
        if (!$orderId) {
            return redirect()->route('site.home');
        }
        $order = Order::find($orderId);
        if (!$order) {
            return redirect()->route('site.home');
        }
        Catalog::ecommerce()->setPage('purchase', 'transaction');
        Catalog::ecommerce()->setOrder($order);

        return view('orders::site.thank-you-page.thank-you', [
            'order' => $order,
        ]);
    }
}

