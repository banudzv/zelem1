<?php

namespace App\Modules\Orders\Controllers\Site;

use App\Components\Delivery\JustIn;
use App\Core\AjaxTrait;
use App\Core\SiteController;
use App\Modules\Orders\Components\Cart\CartItem;
use App\Modules\Orders\Models\City;
use App\Modules\Orders\Models\Order;
use App\Modules\Orders\Models\OrderClient;
use App\Modules\Orders\Models\OrderStatus;
use App\Modules\Orders\Models\OrderStatusesHistory;
use App\Modules\Orders\Models\WarehouseTranslates;
use App\Modules\Orders\Requests\CheckoutContactInformationRequest;
use App\Modules\Orders\Requests\CheckoutDeliveryAndPaymentRequest;
use App\Modules\Orders\Types\OrderType;
use App\Modules\Orders\Types\StepOneType;
use App\Modules\Products\Models\ServicePrice;
use Auth, Cart, Exception;
use Catalog;
use Illuminate\Http\Request;
use App\Components\Delivery\NovaPoshta;
use Illuminate\Support\Arr;

/**
 * Class CheckoutController
 *
 * @package App\Modules\Orders\Controllers\Site
 */
class CheckoutController extends SiteController
{
    use AjaxTrait;

    /**
     * @var JustIn
     */
    protected $justin;

    /**
     * CheckoutController constructor.
     * @param JustIn $justIn
     */
    public function __construct(JustIn $justIn)
    {
        $this->justin = $justIn;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contactInformation()
    {
        $info = $this->getUnfinishedOrderInformation();
        $this->sameMeta('orders::site.step-1');
        $this->breadcrumb('orders::site.step-1');
        $formId = 'checkout-information-form';
        $cart = Cart::me();
        $services = $cart->getServices();
        list($totalAmount, $totalAmountOld) = $cart->getTotalAmountOfTheCart($services);
        Catalog::ecommerce()->setPage('cart', 'checkout', ['step' => 1]);
        Catalog::ecommerce()->setCart($cart);

        return view('orders::site.checkout-step-1', [
            'info' => new StepOneType($info),
            'formId' => $formId,
            'rules' => (new CheckoutContactInformationRequest())->rules(),
            'messages' => (new CheckoutContactInformationRequest())->messages(),
            'attributes' => (new CheckoutContactInformationRequest())->attributes(),
            'totalAmount' => format_only($totalAmount),
            'totalAmountOld' => format_only($totalAmountOld),
            'services' => $services,
            'cart' => $cart
        ]);
    }

    /**
     * @param CheckoutContactInformationRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function storeContactInformation(CheckoutContactInformationRequest $request)
    {
        $this->validation();
        $info = $request->only('name', 'phone', 'email', 'locationId');
        $info['city'] = $request->input('location');
        Cart::linkUnfinishedOrder($info);

        return $this->successJsonAnswer([
            'redirect' => route('site.checkout.step-2'),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws Exception
     */
    public function deliveryAndPayment()
    {
        $this->validation();
        $info = $this->getUnfinishedOrderInformation();
        if (!$info) {
            return response()->redirectToRoute('site.checkout');
        }
        $warehouses = (object)[];
        if ($info['city']) {
            $novaPoshta = new NovaPoshta($info['city']);
            $warehouses = $novaPoshta->getWarehousesCityRef($info['locationId']);
            $warehouses = $warehouses->data;

            $npCity = $novaPoshta->getCityByReference($info['locationId']);
            if ($npCity && $npCity->success && count($npCity->data)) {
                $justinWarehouses = $this->justin->getDepartmentsByCityName($npCity->data[0]->DescriptionRu);
            }
        }
        $zalemWarehouses = WarehouseTranslates::getWarehousesListByCityReference($info['locationId']);
        $cart = Cart::me();
        $services = $cart->getServices();
        list($totalAmount, $totalAmountOld) = $cart->getTotalAmountOfTheCart($services);
        $this->sameMeta('orders::site.step-2');
        $this->breadcrumb('orders::site.step-2');
        Catalog::ecommerce()->setPage('cart', 'checkout', ['step' => 2]);
        Catalog::ecommerce()->setCart($cart);

        return view('orders::site.checkout-step-2', [
            'info' => new StepOneType($info),
            'deliveries' => config('orders.deliveries', []),
            'restDeliveries' => config('orders.rest-deliveries', []),
            'paymentMethods' => config('orders.payment-methods', []),
            'warehouses' => $warehouses,
            'formId' => 'checkout-delivery-form',
            'rules' => (new CheckoutDeliveryAndPaymentRequest())->rules(),
            'messages' => (new CheckoutDeliveryAndPaymentRequest())->messages(),
            'attributes' => (new CheckoutDeliveryAndPaymentRequest())->attributes(),
            'showLiqpay' => config('db.liqpay.public-key') && config('db.liqpay.private-key'),
            'totalAmount' => $totalAmount,
            'totalAmountOld' => $totalAmountOld,
            'services' => $services,
            'justinWarehouses' => $justinWarehouses ?? [],
            'zalemWarehouses' => $zalemWarehouses,
            'showZalemAddressDelivery' => (isset($info['locationId']) && $info['locationId'])
                ? City::whereCityRef($info['locationId'])->exists()
                : false,
            'isKiev' => is_kiev($info['locationId']),
        ]);
    }

    /**
     * @param CheckoutDeliveryAndPaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function storeDeliveryAndPaymentInformation(CheckoutDeliveryAndPaymentRequest $request)
    {
        $this->validation();
        $data = $this->getUnfinishedOrderInformation();
        if (!$data) {
            return $this->errorJsonAnswer([
                'redirect' => route('site.checkout'),
            ]);
        }
        $services = Cart::getServices();
        $orderStatus = OrderStatus::newOrder();
        $data += $request->only('delivery', 'payment_method', 'comment');
        $data['do_not_call_me'] = (bool)$request->input('do_not_call_me');
        $data['status_id'] = $orderStatus->id;
        if (config('db.orders.show-delivery', true)) {
            list($totalAmount, $totalAmountOld) = Cart::getTotalAmountOfTheCart($services);
            $delivery = $request->input('delivery');
            $data['city_ref'] = array_get($data, 'locationId');
            $data['delivery_price'] = delivery_price($totalAmount, $delivery, is_kiev($data['city_ref']));
            if ($delivery === 'nova-poshta-self') {
                $warehouseRef = $request->input($delivery);
                $data['warehouse_ref'] = $warehouseRef;
                $data['delivery_address'] = (new NovaPoshta)->getWarehouseInfo($warehouseRef);
            } elseif ($delivery === 'justin-warehouse') {
                $warehouseCode = $request->input($delivery);
                $npCity = (new NovaPoshta)->getCityByReference(array_get($data, 'locationId'));
                if ($npCity && $npCity->success && count($npCity->data)) {
                    $justinWarehouses = $this->justin->getDepartmentsByCityName($npCity->data[0]->DescriptionRu);
                }
                $department = array_first($justinWarehouses ?? [], function (array $department) use ($warehouseCode) {
                    return $department['code'] == $warehouseCode;
                });
                if (!$department) {
                    throw new \ErrorException('Wrong warehouse.');
                }
                $data['warehouse_ref'] = $department['id'];
                $data['delivery_address'] = $department['descr'] . ': ' . $department['address'];
            } elseif ($delivery === 'zalem-warehouse') {
                $warehouse = WarehouseTranslates::whereLanguage(\Lang::getLocale())
                    ->whereRowId($request->input($delivery))->first();
                $data['warehouse_ref'] = $warehouse->row_id;
                $data['delivery_address'] = $warehouse->address;
            } elseif ($delivery === 'other') {
                $addressField = $request->input('other');
                $address = $request->input($addressField);
                $others = config('orders.rest-deliveries', []);
                $data['delivery_address'] = strip_tags(trans(data_get($others, $addressField, $addressField)));
                if (is_array($address)) {
                    $parts = [];
                    if (isset($address['street'])) {
                        $parts[] = $address['street'];
                    }
                    if (isset($address['house'])) {
                        $parts[] = 'дом ' . $address['house'];
                    }
                    if (isset($address['flat'])) {
                        $parts[] = 'кв. ' . $address['flat'];
                    }
                    $data['delivery_address'] .= ': ' . implode(', ', $parts);
                } else {
                    $data['delivery_address'] .= ': ' . $address;
                }
            } else {
                $address = $request->input($delivery);
                if (is_array($address)) {
                    $parts = [];
                    if (isset($address['street'])) {
                        $parts[] = $address['street'];
                    }
                    if (isset($address['house'])) {
                        $parts[] = 'дом ' . $address['house'];
                    }
                    if (isset($address['flat'])) {
                        $parts[] = 'кв. ' . $address['flat'];
                    }
                    $data['delivery_address'] = implode(', ', $parts);
                } else {
                    $data['delivery_address'] = $address;
                }
                $data['delivery_address'] = is_array($address) ? implode(', ', array_filter($address)) : $address;
            }
        }
        if ($request->input('payment_method') === 'business') {
            $data += $request->input('business', []);
        }
        // Save order
        $order = Order::store($data, Auth::id());
        // Create client in the database and link to the order
        $client = OrderClient::store(
            $order->id,
            array_get($data, 'name'),
            array_get($data, 'phone'),
            array_get($data, 'email')
        );
        if ((bool)$request->input('receiver')) {
            $receiver = $order->receiver()->create(
                $request->only('phone', 'first_name', 'last_name', 'middle_name')
            );
        }

        $orderId = $order->id;
        $items = [];
        Cart::getItems()->each(function (CartItem $item) use (&$items, $orderId, $services) {
            if ($serv = Arr::get($services, $item->getProductId(), [])) {
                $serv = $serv->map(function (ServicePrice $option) {
                    return [
                        'id' => $option->id,
                        'option' => $option->current->name,
                        'service' => $option->service->current->name,
                        'price' => $option->price,
                    ];
                })->toArray();
            }
            if($item->getDictionaryId()) {
                $items[$item->getProductId()][] = [
                    'product_id' => $item->getProductId(),
                    'order_id' => $orderId,
                    'quantity' => $item->getQuantity(),
                    'price' => null,
                    'dictionary_id' => $item->getDictionaryId(),
                    'services' => $serv,
                ];
            } else{
                $items[$item->getProductId()][] = [
                    'product_id' => $item->getProductId(),
                    'order_id' => $orderId,
                    'quantity' => $item->getQuantity(),
                    'price' => null,
                    'services' => $serv,
                ];
            }
        });
        event('orders::data-for-checkout-ready-without-prices', [$items]);
        OrderStatusesHistory::write($order->id, $order->status_id);
        event('orders::created', new OrderType($order, $client, $order->fresh('items')->items));

        if ($order->payment_method === Order::PAYMENT_LIQPAY) {
            return $this->successJsonAnswer([
                'orderId' => $order->id,
                'redirect' => route('site.orders.payment-liqpay', $order->id),
            ]);
        }
        session(['thank-you' => $order->id]);

        return $this->successJsonAnswer([
            'orderId' => $order->id,
            'redirect' => route('site.thank-you'),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function locationSuggest(Request $request)
    {
        $query = $request->input('query');
        $data = (new NovaPoshta)->searchSettlements($query);
        if (!$data || !isset($data['data']) || empty($data['data']) || !isset($data['data'][0]['TotalCount']) || !$data['data'][0]['TotalCount']) {
            return $this->successJsonAnswer([
                'html' => view('orders::site.widgets.location-suggest.location-suggest', ['locations' => []])->render(),
            ]);
        }
        $addresses = $data['data'][0]['Addresses'];

        return $this->successJsonAnswer([
            'html' => view('orders::site.widgets.location-suggest.location-suggest', ['locations' => $addresses])->render(),
        ]);
    }

    /**
     * @return array
     */
    private function getUnfinishedOrderInformation(): array
    {
        if (Cart::hasUnfinishedOrder()) {
            return Cart::getUnfinishedOrder()->getInformation();
        }
        return Auth::guest() ? [] : [
            'email' => Auth::user()->email,
            'phone' => Auth::user()->phone,
            'name' => Auth::user()->name,
        ];
    }

    /**
     * @throws Exception
     */
    private function validation()
    {
        abort_unless(Cart::getTotalQuantity(), 404, 'You can not order nothing');
    }
}
