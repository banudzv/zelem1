<?php

namespace App\Modules\Orders\Controllers\Site\Cart;

use App\Core\SiteController;
use Cart;

/**
 * Class IndexController
 *
 * @package App\Modules\Orders\Controllers\Site
 */
class IndexController extends SiteController
{
    
    /**
     * Cart items list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->breadcrumb('orders::site.cart', 'site.cart.index');
        $this->sameMeta('orders::site.cart');
        $cart = Cart::me();
        $services = $cart->getServices();
        list($totalAmount, $totalAmountOld) = $cart->getTotalAmountOfTheCart($services);

        return view('orders::site.cart.index', compact('cart', 'services',
            'totalAmountOld', 'totalAmount'));
    }
    
}