<?php

namespace App\Modules\Orders\Controllers\Site\Cart;

use App\Core\AjaxTrait;
use App\Core\SiteController;
use App\Modules\Orders\Requests\CartRequest;
use App\Modules\Orders\Requests\UpdateCartRequest;
use App\Modules\Products\Models\ServicePrice;
use Illuminate\Http\JsonResponse;
use Widget, Cart;

/**
 * Class IndexController
 *
 * @package App\Modules\Orders\Controllers\Site
 */
class AjaxController extends SiteController
{
    use AjaxTrait;

    /**
     * @return JsonResponse
     * @throws \Throwable
     */
    public function index()
    {
        return $this->cartStructureInJson();
    }

    /**
     * Add product to the cart
     *
     * @param CartRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function add(CartRequest $request)
    {
        $productIds = $request->input('product_id', []);
        if (is_array($productIds) === false) {
            $productIds = [$productIds];
        }
        if (!config('db.products_dictionary.site_status')) {
            $dictionaryId = null;
        } else {
            $dictionaryId = $request->input('product_data.dictionary_id');
        }
        if ($servicesIds = $request->input('product_data.services', [])) {
            $services = ServicePrice::select(['id'])->whereIn('id', $servicesIds)
                ->pluck('id')->toArray();
        }
        $addedProductsIds = [];
        foreach ($productIds as $productId) {
            if ($cartItem = Cart::addItem($productId, 1, $dictionaryId, $services ?? [])) {
                $addedProductsIds[] = $cartItem->getProductId();
                $dictionaryId = $cartItem->getDictionaryId();
            }
        }

        return $this->cartStructureInJson($addedProductsIds, $dictionaryId);
    }

    /**
     * Delete item from the cart
     *
     * @param CartRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function delete(CartRequest $request)
    {
        $productIds = $request->input('product_id', []);
        $dictionaryId = $request->input('product_data.dictionary_id', null);
        if (is_array($productIds) === false) {
            $productIds = [$productIds];
        }
        foreach ($productIds as $productId) {
            Cart::removeItem($productId, $dictionaryId);
        }

        return $this->cartStructureInJson();
    }

    /**
     * Update item quantity
     *
     * @param UpdateCartRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function updateQuantity(UpdateCartRequest $request)
    {
        Cart::setQuantity(
            $request->input('product_id'),
            $request->input('product_data.quantity'),
            $request->input('product_data.dictionary_id')
        );

        return $this->cartStructureInJson();
    }

    /**
     * Update item dictionary
     *
     * @param UpdateCartRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function updateDictionary(UpdateCartRequest $request)
    {
        Cart::setDictionary(
            $request->input('product_id'),
            $request->input('product_data.old_dictionary_id'),
            $request->input('product_data.dictionary_id')
        );

        return $this->cartStructureInJson();
    }

    /**
     * @param array $addedProducts
     * @param int|null $dictionaryId
     * @return JsonResponse
     * @throws \Throwable
     */
    protected function cartStructureInJson(array $addedProducts = [], ?int $dictionaryId = null)
    {
        if (config('db.products_dictionary.site_status')) {
            Cart::checkDictionary();
        }
        $cart = Cart::me();
        $services = $cart->getServices();
        list($totalAmount, $totalAmountOld) = $cart->getTotalAmountOfTheCart($services);
        $totalAmount = format_only($totalAmount);
        $totalAmountOld = format_only($totalAmountOld);
        $totalQuantity = Cart::getTotalQuantity();
        $briefly = view('orders::site.cart.cart--briefly', [
            'cart' => $cart,
            'totalAmount' => $totalAmount,
        ])->render();
        if ($totalQuantity > 0) {
            $detailed = view('orders::site.cart.cart--detailed', [
                'cart' => $cart,
                'addedProducts' => $addedProducts,
                'dictionaryId' => $dictionaryId,
                'totalAmount' => $totalAmount,
                'totalAmountOld' => $totalAmountOld,
                'services' => $services,
                'route' => request()->has('cartRoute') ? request()->get('cartRoute') : null
            ])->render();
        } else {
            $detailed = view('orders::site.cart.cart--detailed-empty', [
                'cart' => $cart,
            ])->render();
        }

        return $this->successJsonAnswer([
            'html' => [
                'briefly' => $briefly,
                'detailed' => $detailed,
                'checkout' => (string)Widget::show('orders::cart::checkout', $totalAmount, $totalAmountOld, $services),
            ],
            'total_quantity' => $totalQuantity,
            'total_amount' => $totalAmount,
        ]);
    }
}
