<?php

namespace App\Modules\Orders\Controllers\Site;

use App\Components\Delivery\JustIn;
use App\Components\Delivery\NovaPoshta;
use App\Core\AjaxTrait;
use App\Core\SiteController;
use App\Modules\Orders\Models\Order;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class AccountController
 * @package App\Modules\Orders\Controllers\Site
 */
class AccountController extends SiteController
{
    use AjaxTrait;

    /**
     * @return
     */
    public function index()
    {
        $this->sameMeta('orders::site.my-orders');
        $this->breadcrumb('orders::site.my-orders');
        $orders = Order::paginateForUser(\Auth::id());

        return view('orders::site.account', compact('orders'));
    }

    /**
     * Returns the delivery status history for the order.
     *
     * @param Order $order
     * @return Application|Factory|JsonResponse|View
     * @throws \Exception
     */
    public function getDeliveryStatus(Order $order)
    {
        if ($order->delivery === 'justin-warehouse') {
            list ($column, $value) = $this->getFilterForJustIn($order);
            $statuses = (new JustIn())->getStatusesHistory($value, $column);
            if (count($statuses) > 0) {
                $status = array_last($statuses);
                $date = new Carbon($status['date']);
                $data = [
                    'delivery' => [
                        'ttn' => $order->ttn,
                        'e_invoice' => $order->e_invoice,
                        'date' => $this->formatDate($date),
                        'time' => $date->format('H:i:s'),
                        'citySender' => null,
                        'cityRecipient' => $order->city,
                        'status' => $status['name'],
                        'warehouseRecipient' => $order->delivery_address,
                    ],
                    'link' => route('site.orders.get-delivery-status', ['order' => $order->id]),
                ];

                return view('orders::site.widgets.delivery-status.delivery-status', $data);
            }
        } elseif ($order->delivery === 'nova-poshta-self' || $order->delivery === 'nova-poshta') {
            $novaposhta = new NovaPoshta();
            $response = $novaposhta->getDeliveryInformationByTTN($order->ttn);
            $deliveryInformation = array_get($response->data, 0);
            if ($deliveryInformation && $deliveryInformation->RefCitySender) {
                $date = Carbon::parse($deliveryInformation->DateCreated);
                $data = [
                    'delivery' => [
                        'ttn' => $deliveryInformation->Number,
                        'date' => $this->formatDate($date),
                        'time' => $date->format('H:i:s'),
                        'citySender' => $deliveryInformation->CitySender,
                        'cityRecipient' => $deliveryInformation->CityRecipient,
                        'status' => $deliveryInformation->Status,
                        'warehouseRecipient' => $deliveryInformation->WarehouseRecipient,
                    ],
                    'link' => route('site.orders.get-delivery-status', ['order' => $order->id]),
                ];

                return view('orders::site.widgets.delivery-status.delivery-status', $data);
            }
        }

        return view('orders::site.widgets.delivery-status.delivery-status-error');
    }

    /**
     * Returns filter's value and column for JustIn API.
     *
     * @param Order $order
     * @return array
     */
    protected function getFilterForJustIn(Order $order): array
    {
        if ($order->e_invoice) {
            return ['orderNumber', $order->e_invoice];
        }
        if ($order->ttn) {
            return ['TTN', $order->ttn];
        }

        return ['clientNumber', $order->id];
    }

    /**
     * @param Carbon $date
     * @return string
     */
    protected function formatDate(Carbon $date)
    {
        return $date->format('d') . ' ' .
            __(config('months.full.' . $date->format('n'), $date->format('m'))) . ' ' .
            $date->format('Y');
    }
}
