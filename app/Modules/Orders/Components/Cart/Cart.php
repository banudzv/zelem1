<?php

namespace App\Modules\Orders\Components\Cart;

use App\Modules\Orders\Models\Cart as CartModel;
use App\Modules\Orders\Models\CartItem as CartItemModel;
use App\Modules\Orders\Models\CartUnfinishedOrder as UnfinishedOrderModel;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ServicePrice;
use App\Modules\ProductsDictionary\Models\Dictionary;
use Catalog;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Exception, Cookie, Hash;

use function DI\string;

/**
 * Class Cart
 *
 * @package App\Modules\Orders\Facades
 */
class Cart
{
    const CART_ALIAS = 'cart';

    /**
     * @var Model
     */
    protected $model;

    /**
     * Items inside the cart
     *
     * @var Collection|CartItem[]
     */
    protected $items;

    /**
     * Items inside the cart
     *
     * @var CartUnfinishedOrder|null
     */
    protected $unfinishedOrder;

    /**
     * CartAbstraction constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->items = new Collection();
        $this->create();
    }

    /**
     * @return Cart
     */
    public function me(): self
    {
        return $this;
    }

    /**
     * Returns model of the cart from the database
     *
     * @return CartModel|Model|null
     */
    public function getModel(): ?Model
    {
        return $this->model;
    }

    /**
     * Services lst by product ID.
     *
     * @return array
     */
    public function getServices(): array
    {
        $servicesIds = [];
        foreach ($this->getItems() as $item) {
            $cartItem = $item->getModel();
            $servicesIds = array_merge($cartItem->services ?? [], $servicesIds);
        }
        if (empty($servicesIds)) {
            return [];
        }
        $servInst = ServicePrice::with(['current', 'service.current'])->whereIn('id', $servicesIds)->get();
        $services = [];
        foreach ($this->getItems() as $item) {
            $cartItem = $item->getModel();
            $services[$cartItem->product_id] = $servInst->filter(
                function (ServicePrice $option) use ($cartItem) {
                    return in_array($option->id, $cartItem->services ?? []);
                }
            );
        }

        return $services;
    }

    /**
     * @param array|Collection[]|ServicePrice[][] $services
     * @return array|float[]
     */
    public function getTotalAmountOfTheCart(array $services = []): array
    {
        $totalAmount = $oldTotalAmount = 0;
        foreach ($this->getItems() as $cartItem) {
            if ($product = $cartItem->getProduct()) {
                $totalAmount += calc($product->price * $cartItem->getQuantity());
                if ($product->old_price) {
                    $oldTotalAmount += calc($product->old_price * $cartItem->getQuantity());
                }
                /** @var ServicePrice $service */
                foreach (Arr::get($services, $product->id, []) as $service) {
                    $totalAmount += $service->price * $cartItem->getQuantity();
                    if ($product->old_price) {
                        $oldTotalAmount += $service->price * $cartItem->getQuantity();
                    }
                }
            }
        }

        return [$totalAmount, $oldTotalAmount];
    }

    /**
     * Returns all items inside the cart
     *
     * @return CartItem[]|Collection
     */
    public function getItems(): Collection
    {
        return $this->items->filter(
            function (CartItem $cartItem) {
                return $cartItem->isAvailable();
            }
        );
    }

    /**
     * Returns new cart hash
     * Stores unique hash key of the cart to cookies
     *
     * @return string
     * @throws Exception
     */
    public function create(): string
    {
        $cartHash = array_get($_COOKIE, self::CART_ALIAS);
        if ($cartHash) {
            $this->createFromHash($cartHash);
            return $cartHash;
        }
        return $this->createNew();
    }

    /**
     * Will create cart and cart items instances from the existed data
     *
     * @param string $hash
     * @return string
     * @throws Exception
     */
    private function createFromHash(string $hash): string
    {
        $cart = CartModel::whereHash($hash)->first();
        if (!$cart) {
            return $this->createNew();
        }
        $this->model = $cart;

        $this->fillItemsList();
        $this->restoreUnfinishedOrder();

        return $hash;
    }

    /**
     * Creates new cart instance
     *
     * @return string
     * @throws Exception
     */
    private function createNew(): string
    {
        /** @var CartModel $cart */
        $cart = new CartModel();
        $cart->hash = Hash::make(microtime() . random_int(1, 999999));
        $cart->save();

        $this->model = $cart;

        setcookie(self::CART_ALIAS, $cart->hash, time() + 2628000, '/');

        return $cart->hash;
    }

    /**
     * Adds product to the cart
     * Add $quantity to the existed cart item or creates new
     *
     * @param int $productId
     * @param int $quantity
     * @param int|null $dictionaryId
     * @param array $services
     * @return CartItem|null
     * @throws Exception
     */
    public function addItem(
        int $productId,
        int $quantity = 1,
        ?int $dictionaryId = null,
        array $services = []
    ): ?CartItem {
        // Check product availability
        $product = Product::find($productId);
        if (!$product || !$product->active || !$product->is_available) {
            return null;
        }
        if (!$dictionaryId && config('db.products_dictionary.site_status')) {
            $status = config('db.products_dictionary.select_status');
            if ($status) {
                $dictionary = Dictionary::orderBy('position')->first();
            } else {
                $dictionary = Dictionary::whereHas(
                    'relations',
                    function (Builder $builder) use ($product) {
                        $builder->where('group_id', $product->id);
                    }
                )->orderBy('position')->first();
            }
            if ($dictionary) {
                $dictionaryId = $dictionary->id;
            }
        }
        // Add product to the cart
        if ($this->hasItem($productId, $dictionaryId)) {
            $item = $this->addToExistedItem($productId, $quantity, $dictionaryId, $services);
        } else {
            $item = $this->createItem($productId, $quantity, $dictionaryId, $services);
        }
        return $item;
    }

    /**
     * Checks if cart has product inside it
     *
     * @param int $productId
     * @param int|null $dictionaryId
     * @return bool
     */
    public function hasItem(int $productId, ?int $dictionaryId = null): bool
    {
        foreach ($this->items as $item) {
            if ($item->getProductId() === $productId && $item->getDictionaryId() === $dictionaryId) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns cart item object by product id
     *
     * @param int $productId
     * @param null $dictionaryId
     * @return CartItem|null
     */
    public function getItem(int $productId, ?int $dictionaryId = null): ?CartItem
    {
        foreach ($this->items as $item) {
            if ($item->getProductId() === $productId && $item->getDictionaryId() === $dictionaryId) {
                return $item;
            }
        }
        return null;
    }

    /**
     * Removes item by product id
     *
     * @param int $productId
     * @param int|null $dictionaryId
     * @throws Exception
     */
    public function removeItem(int $productId, ?int $dictionaryId = null): void
    {
        foreach ($this->items as $index => $item) {
            if ($item->getProductId() !== $productId) {
                continue;
            }
            if ($item->getDictionaryId() === $dictionaryId) {
                $this->items->forget($index);
                $item->delete();
                return;
            }
        }
    }

    /**
     * Sets new quantity value to the item
     *
     * @param int $productId
     * @param int $quantity
     * @param int|null $dictionaryId
     */
    public function setQuantity(int $productId, int $quantity = 1, ?int $dictionaryId = null): void
    {
        foreach ($this->items as $index => $item) {
            if ($item->getProductId() !== $productId) {
                continue;
            }
            if ($item->getDictionaryId() === $dictionaryId) {
                $item->setQuantity($quantity);
                break;
            }
        }
    }

    /**
     * Sets new quantity value to the item
     *
     * @param int $productId
     * @param int|null $dictionaryIdOld
     * @param int|null $dictionaryId
     * @throws Exception
     */
    public function setDictionary(int $productId, ?int $dictionaryIdOld, ?int $dictionaryId = null): void
    {
        foreach ($this->items as $index => $item) {
            if ($item->getProductId() !== $productId) {
                continue;
            }
            if ($item->getDictionaryId() === $dictionaryIdOld) {
                if ($this->hasItem($productId, $dictionaryId)) {
                    $otherItem = $this->getItem($productId, $dictionaryId);
                    $otherItem->increment($item->getQuantity());
                    \Cart::removeItem($productId, $dictionaryIdOld);
                } else {
                    $item->setDictionary($dictionaryId);
                }
                break;
            }
        }
    }

    /**
     * Removes cart from the database & cookies
     *
     * @throws Exception
     */
    public function delete(): void
    {
        $this->items = new Collection();
        if ($this->model && $this->model->exists) {
            $this->model->delete();
            $this->model = null;
        }
        Cookie::forget(static::CART_ALIAS);
    }

    /**
     * Returns total quantity of the cart items
     *
     * @return int
     */
    public function getTotalQuantity(): int
    {
        $totalCount = 0;
        $this->getItems()->each(
            function (CartItem $cartItem) use (&$totalCount) {
                $totalCount += $cartItem->getQuantity();
            }
        );

        return $totalCount;
    }

    /**
     * @return array
     */
    public function getQuantities(): array
    {
        $quantities = [];
        $this->getItems()->each(
            function (CartItem $cartItem) use (&$quantities) {
                $quantities[$cartItem->getProductId()] = (isset(
                    $quantities[$cartItem->getProductId()]
                )) ? $quantities[$cartItem->getProductId()] + $cartItem->getQuantity() : $cartItem->getQuantity();
            }
        );

        return $quantities;
    }

    /**
     * @param CartUnfinishedOrder|null $unfinishedOrder
     */
    public function setUnfinishedOrder(?CartUnfinishedOrder $unfinishedOrder): void
    {
        $this->unfinishedOrder = $unfinishedOrder;
    }

    /**
     * @return CartUnfinishedOrder|null
     */
    public function getUnfinishedOrder(): ?CartUnfinishedOrder
    {
        return $this->unfinishedOrder;
    }

    /**
     * @return bool
     */
    public function hasUnfinishedOrder(): bool
    {
        return $this->unfinishedOrder !== null;
    }

    /**
     * @param int $productId
     * @param int $quantity
     * @param int|null $dictionaryId
     * @param array $services
     * @return CartItem
     * @throws Exception
     */
    protected function addToExistedItem(
        int $productId,
        int $quantity = 1,
        $dictionaryId = null,
        array $services = []
    ): CartItem {
        $cartItem = $this->getItem($productId, $dictionaryId);
        $cartItem->increment($quantity);
        $cartItem->setServices($services);

        return $cartItem;
    }

    /**
     * @param int $productId
     * @param int $quantity
     * @param int|null $dictionaryId
     * @param array $services
     * @return CartItem
     */
    protected function createItem(
        int $productId,
        int $quantity = 1,
        ?int $dictionaryId = null,
        array $services = []
    ): CartItem {
        $cartItem = new CartItem;
        $cartItem->create($this->model->id, $productId, $quantity, $dictionaryId, $services);
        $this->items->push($cartItem);

        return $cartItem;
    }

    protected function fillItemsList(): void
    {
        $cartInstance = $this;
        /** @var CartModel $cart */
        $cart = $this->model;
        $cart->items->each(
            function (CartItemModel $item) use (&$cartInstance) {
                $cartItem = new CartItem();
                $cartItem->setModel($item);
                $cartInstance->items->push($cartItem);
            }
        );
    }

    protected function restoreUnfinishedOrder(): void
    {
        /** @var CartModel $cart */
        $cart = $this->model;
        if ($cart->unfinishedOrder) {
            $unfinishedOrder = new CartUnfinishedOrder();
            $unfinishedOrder->setModel($cart->unfinishedOrder);
            $this->setUnfinishedOrder($unfinishedOrder);
        }
    }

    /**
     * @param array $information
     */
    public function linkUnfinishedOrder(array $information): void
    {
        /** @var CartModel $cart */
        $cart = $this->model;
        if ($cart->unfinishedOrder) {
            $model = $cart->unfinishedOrder;
        } else {
            $model = new UnfinishedOrderModel();
        }
        $model->information = $information;
        $model->cart_id = $this->model->id;
        $model->save();

        $unfinishedOrder = new CartUnfinishedOrder();
        $unfinishedOrder->setModel($model);
        $this->setUnfinishedOrder($unfinishedOrder);
    }

    /**
     * @throws Exception
     */
    public function checkDictionary()
    {
        $this->items->each(
            function (CartItem $item) {
                if (!$item->getDictionaryId()) {
                    return;
                }
                $dictionary = Dictionary::whereId($item->getDictionaryId())->withTrashed()->first();
                if ($dictionary && !$dictionary->trashed()) {
                    return;
                }
                $analog = $this->getAnalogWithoutAdditionalFeature($item->getProductId());
                if ($analog) {
                    $analog->increment($item->getQuantity());
                }
                \Cart::removeItem($item->getProductId(), $item->getDictionaryId());
            }
        );
    }

    /**
     * @param int|null $productId
     * @return CartItem|null
     */
    private function getAnalogWithoutAdditionalFeature(?int $productId): ?CartItem
    {
        if (!$productId) {
            return null;
        }
        foreach ($this->items as $item) {
            if ($item->getProductId() !== $productId) {
                continue;
            }
            if ($item->getDictionaryId() !== null) {
                continue;
            }
            return $item;
        }
    }

    public function getFBPixelData()
    {
        $count = 0;
        $products = $this->getModel()->items->map(
            function (CartItemModel $item) use (&$count) {
                $count += $item->quantity;

                return [
                    'id' => $item->product->id,
                    'quantity' => $item->quantity
                ];
            }
        )->toArray();

        list($totalAmount) = $this->getTotalAmountOfTheCart($this->getServices());

        $data = [
            'content_type' => 'product',
            'contents' => $products,
            'value' => (int)number_format($totalAmount, config('db.currencies.number-symbols', 2), '.', ''),
            'currency' => Catalog::currency()->microdataName() ?: 'UAH',
            'num_items' => $count
        ];

        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
}
