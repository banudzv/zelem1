<?php

namespace App\Modules\Orders\Widgets\Cart;

use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\ServicePrice;
use Illuminate\Support\Collection;

/**
 * Class CartInCheckout
 *
 * @package App\Modules\Orders\Widgets
 */
class CartInCheckout implements AbstractWidget
{
    /**
     * @var array|Collection[]|ServicePrice[][]
     */
    protected $services;
    /**
     * @var string
     */
    protected $totalAmount;
    /**
     * @var string
     */
    protected $totalAmountOld;

    /**
     * CartInCheckout constructor.
     *
     * @param array|Collection[]|ServicePrice[][] $services
     * @param string $totalAmount
     * @param string $totalAmountOld
     */
    public function __construct(string $totalAmount, string $totalAmountOld, array $services = [])
    {
        $this->totalAmount = $totalAmount;
        $this->totalAmountOld = $totalAmountOld;
        $this->services = $services;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        return view('orders::site.cart.cart--checkout', [
            'cart' => \Cart::me(),
            'services' => $this->services ?? [],
            'amount' => (float)$this->totalAmount,
            'amountOld' => (float)$this->totalAmountOld,
            'defaultDeliveryPrice' => delivery_price((float)$this->totalAmount),
        ]);
    }
}
