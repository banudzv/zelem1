<?php

namespace App\Modules\Orders\Widgets;

use App\Components\Widget\AbstractWidget;
use Cart;

/**
 * Class CheckoutButton
 *
 * @package App\Modules\Orders\Widgets
 */
class CheckoutButton implements AbstractWidget
{
    /**
     * @var int
     */
    private $route;

    public function __construct(?string $route = null)
    {
        $this->route = (int) $route;
    }
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        return view('orders::site.cart.checkout-button', [
            'cart' => Cart::me(),
            'canMakeOrder' => true,
            'link' => ($this->route == 2) ? route('site.checkout.step-2') : route('site.checkout')
        ]);
    }
    
}
