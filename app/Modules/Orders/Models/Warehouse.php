<?php

namespace App\Modules\Orders\Models;

use App\Traits\ModelMain;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Builder;

/**
 * Class Warehouse
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $city_ref
 * @property-read WarehouseTranslates $current
 * @property-read WarehouseTranslates[]|Collection $data
 * @method static Builder|Warehouse whereId($value)
 * @method static Builder|Warehouse whereCreatedAt($value)
 * @method static Builder|Warehouse whereUpdatedAt($value)
 * @method static Builder|Warehouse whereCityRef($value)
 * @package App\Modules\Orders\Models
 * @mixin \Eloquent
 */
class Warehouse extends Model
{
    use ModelMain;

    /**
     * {@inheritDoc}
     */
    protected $table = 'zalem_warehouses';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['city_ref'];
}
