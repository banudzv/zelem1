<?php

namespace App\Modules\Orders\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OrderReceiver
 * @package App\Modules\Orders\Models
 * @property int $id
 * @property int $order_id
 * @property string $phone
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property-read string $name
 * @property-read string $cleared_phone
 * @property-read Order $order
 * @method static Builder|OrderReceiver newModelQuery()
 * @method static Builder|OrderReceiver newQuery()
 * @method static Builder|OrderReceiver query()
 * @method static Builder|OrderReceiver whereId($value)
 * @method static Builder|OrderReceiver whereOrderId($value)
 * @method static Builder|OrderReceiver whereFirstName($value)
 * @method static Builder|OrderReceiver whereLastName($value)
 * @method static Builder|OrderReceiver whereMiddleName($value)
 * @method static Builder|OrderReceiver wherePhone($value)
 * @mixin \Eloquent
 */
class OrderReceiver extends Model
{
    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'orders_receivers';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['order_id', 'phone', 'first_name', 'last_name', 'middle_name'];

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    /**
     * Returns full name of the receiver.
     *
     * @return string
     */
    public function getNameAttribute(): string
    {
        if ($this->middle_name) {
            return $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
        }

        return $this->last_name . ' ' . $this->first_name;
    }

    /**
     * Phone number with numbers only.
     *
     * @return string
     */
    public function getClearedPhoneAttribute()
    {
        return preg_replace('/[^0-9]*/', '', $this->phone);
    }
}
