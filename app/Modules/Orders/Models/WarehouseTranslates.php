<?php

namespace App\Modules\Orders\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Warehouse
 * @property int $id
 * @property int $row_id
 * @property string $language
 * @property string $city
 * @property string $address
 * @property-read Warehouse $row
 * @method static Builder|WarehouseTranslates whereId($value)
 * @method static Builder|WarehouseTranslates whereRowId($value)
 * @method static Builder|WarehouseTranslates whereLanguage($value)
 * @method static Builder|WarehouseTranslates whereCity($value)
 * @method static Builder|WarehouseTranslates whereAddress($value)
 * @package App\Modules\Orders\Models
 * @mixin \Eloquent
 */
class WarehouseTranslates extends Model
{
    use ModelTranslates;

    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'zalem_warehouse_translates';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['row_id', 'language', 'city', 'address'];

    /**
     * Returns all warehouses that belongs to Zalem for chosen by user city.
     *
     * @param string|null $cityReference
     * @return array
     */
    public static function getWarehousesListByCityReference(?string $cityReference): array
    {
        return $cityReference
            ? WarehouseTranslates::whereLanguage(\Lang::getLocale())
                ->whereHas('row', function (Builder $builder) use ($cityReference) {
                    $builder->where('city_ref', $cityReference);
                })->pluck('address', 'row_id')->toArray()
            : [];
    }
}
