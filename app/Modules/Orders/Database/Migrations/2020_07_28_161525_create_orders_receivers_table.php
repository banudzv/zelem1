<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersReceiversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_receivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('phone');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name')->nullable();

            $table->foreign('order_id')->on('orders')->references('id')
                ->index('orders_receivers_order_id_orders_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_receivers', function (Blueprint $table) {
            $table->dropForeign('orders_receivers_order_id_orders_id');
        });
        Schema::drop('orders_receivers');
    }
}
