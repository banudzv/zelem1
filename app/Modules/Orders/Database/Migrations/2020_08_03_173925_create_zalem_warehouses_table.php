<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZalemWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zalem_warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('city_ref')->comment('Reference to NovaPoshta city.');
        });
        Schema::create('zalem_warehouse_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city');
            $table->string('address');
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);
            $table->foreign('language')->references('slug')->on('languages')
                ->index('zwt_language_languages_slug')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('zalem_warehouses')
                ->index('zwt_row_id_zw_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zalem_warehouse_translates', function (Blueprint $table) {
            $table->dropForeign('zwt_language_languages_slug');
            $table->dropForeign('zwt_row_id_zw_id');
        });
        Schema::drop('zalem_warehouse_translates');
        Schema::drop('zalem_warehouses');
    }
}
