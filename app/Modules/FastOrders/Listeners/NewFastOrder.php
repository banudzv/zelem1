<?php

namespace App\Modules\FastOrders\Listeners;

use AmoCRM\Client;
use App\Components\Crm\Amo;
use App\Components\Crm\Bitrix;
use App\Components\Mailer\MailSender;
use App\Core\Interfaces\ListenerInterface;
use App\Core\Modules\Mail\Models\MailTemplate;
use App\Core\Modules\Notifications\Models\Notification;
use App\Modules\FastOrders\Events\FastOrderEvent;
use App\Modules\Products\Models\Product;

/**
 * Class NewFastOrder
 *
 * @package App\Modules\FastOrders\Listeners
 */
class NewFastOrder implements ListenerInterface
{

    const NOTIFICATION_TYPE = 'fast-order';

    const NOTIFICATION_ICON = 'fa fa-cart-plus';

    public static function listens(): string
    {
        return FastOrderEvent::class;
    }

    /**
     * Handle the event.
     *
     * @param FastOrderEvent $event
     * @return void
     */
    public function handle(FastOrderEvent $event)
    {
        if (config('db.amo_crm.use_amo')) {
            $this->addInAmoCrm($event);
        }
        $this->sendBitrix($event);
        $this->sendMail($event);
        Notification::send(
            static::NOTIFICATION_TYPE,
            'fast_orders::general.notification',
            'admin.fast_orders.edit',
            ['fastOrder' => $event->orderId]
        );
    }

    /**
     * @param FastOrderEvent $event
     */
    public function sendMail(FastOrderEvent $event)
    {
        if (!config('db.basic.admin_email')) {
            return;
        }
        $template = MailTemplate::getTemplateByAlias('fast-orders');
        if (!$template) {
            return;
        }
        $from = [
            '{phone}',
            '{admin_href}',
        ];
        $to = [
            $event->phone,
            route('admin.fast_orders.edit', ['id' => $event->orderId])
        ];
        $subject = str_replace($from, $to, $template->current->subject);
        $body = str_replace($from, $to, $template->current->text);
        MailSender::send(config('db.basic.admin_email'), $subject, $body);
    }

    /**
     * @param FastOrderEvent $event
     */
    public function addInAmoCrm(FastOrderEvent $event)
    {
        $product = Product::with('current')->whereId($event->productId)->first();
        $data = [];
        $data['formName'] = __('fast_orders::general.notification') . 'Id ' . $event->productId;
        $data['name'] = $event->phone;
        $data['phone'] = $event->phone;
        $data['text'] = route('site.product', $product->current->slug);
        $amo = new Amo();
        $amo->newLead($data);
    }

    /**
     * @param FastOrderEvent $event
     */
    public function sendBitrix(FastOrderEvent $event){
        $product = Product::with('current')->whereId($event->productId)->first();
        $data = [];
        $data['requestType'] = 'fast-order';
        $data['formName'] = __('fast_orders::general.notification') . 'Id ' . $event->productId;
        $data['name'] = $event->phone;
        $data['phone'] = $event->phone;
        $data['text'] = route('site.product', $product->current->slug);

        $bitrix = new Bitrix();
        $bitrix->send($data);
    }

}
