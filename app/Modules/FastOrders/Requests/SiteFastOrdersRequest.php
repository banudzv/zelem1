<?php

namespace App\Modules\FastOrders\Requests;

use App\Core\Interfaces\RequestInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class SiteFastOrdersRequest
 * @package App\Modules\FastOrders\Requests
 */
class SiteFastOrdersRequest extends FormRequest implements RequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'phone' => ['required', 'string', 'min:10', 'max:191', 'regex:/^(\s*)?(\+)?([- ()]?\d[- ()]?){12}(\s*)?$/'],
            'user_id' => ['sometimes', Rule::exists('users', 'id')],
            'product_id' => ['required', Rule::exists('products', 'id')],
        ];
        if(\Config::get('db.basic.agreement_show')) {
            $rules['personal-data-processing'] =  ['required', 'in:on'];
        }
        switch (config('db.fast_orders.show_name', 1)) {
            case 2:
                $rules['name'] = ['nullable', 'string', 'min:2', 'max:191'];
                break;
            case 3:
                $rules['name'] = ['required', 'string', 'min:2', 'max:191'];
                break;
            default: break;
        }
        return $rules;
    }
    
    public function attributes()
    {
        return [
            'name' => trans('fast_orders::site.name'),
        ];
    }
    
}
