<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'front'], function () {
    // Frontend routes
    Route::get('/', ['as' => 'front.home', 'uses' => 'IndexController@home']);
});
