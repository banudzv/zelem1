<?php

namespace App\Modules\Frontend\Controllers\Site;

use App\Core\SiteController;

/**
 * Class IndexController
 *
 * @package App\Modules\Frontend\Controllers\Site
 */
class IndexController extends SiteController
{
    /**
     * Home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('site._layouts.text');
    }
}
