<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUniqueIndexIntoFeaturesValuesTranslatesTable extends Migration
{

    public function up()
    {
        Schema::table(
            'features_values_translates',
            function (Blueprint $table) {
                $table->unique(['slug', 'language']);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'features_values_translates',
            function (Blueprint $table) {
                $table->dropUnique(['slug', 'language']);
            }
        );
    }

}
