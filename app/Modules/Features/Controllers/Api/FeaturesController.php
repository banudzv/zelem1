<?php

namespace App\Modules\Features\Controllers\Api;

use App\Core\ApiController;
use App\Modules\Features\Models\Feature;
use App\Modules\Features\Resources\FeatureFullResource;

/**
 * Class BrandsController
 *
 * @package App\Modules\Brands\Controllers\Api
 */
class FeaturesController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/features/all",
     *     tags={"Features"},
     *     summary="Returns all features with all data and with values",
     *     operationId="getFeaturesFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/FeatureFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return FeatureFullResource::collection(Feature::with(['data', 'fullValues'])->oldest('position')->get());
    }
}
