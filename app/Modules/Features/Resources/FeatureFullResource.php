<?php

namespace App\Modules\Features\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class FeatureFullResource
 *
 * @package App\Modules\Features\Resources
 *
 * @OA\Schema(
 *   schema="FeatureFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "active", "in_filter", "main", "type", "data"},
 *           @OA\Property(property="id", type="integer", description="ID"),
 *           @OA\Property(property="active", type="boolean", description="Feature activity"),
 *           @OA\Property(property="in_filter", type="boolean", description="Should be shown in filter?"),
 *           @OA\Property(property="main", type="boolean", description="Is this a main feature?"),
 *           @OA\Property(property="type", type="string", description="Feature type (single, multiple etc.)"),
 *           @OA\Property(
 *              property="data",
 *              type="array",
 *              description="Multilanguage data",
 *              @OA\Items(
 *                  type="object",
 *                  allOf={
 *                      @OA\Schema(
 *                          required={"language", "name", "slug"},
 *                          @OA\Property(property="language", type="string", description="Language to store"),
 *                          @OA\Property(property="name", type="string", description="Feature name"),
 *                          @OA\Property(property="slug", type="string", description="Feature slug"),
 *                      )
 *                  }
 *              )
 *           ),
 *           @OA\Property(
 *              property="values",
 *              type="object",
 *              description="Values list",
 *              allOf={
 *                  @OA\Schema(
 *                      required={"id", "active", "data"},
 *                      @OA\Property(property="id", type="integer", description="ID"),
 *                      @OA\Property(property="active", type="boolean", description="Feature activity"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="array",
 *                          description="Multilanguage data",
 *                          @OA\Items(
 *                              type="object",
 *                              allOf={
 *                                  @OA\Schema(
 *                                      required={"language", "name", "slug"},
 *                                      @OA\Property(property="language", type="string", description="Language to store"),
 *                                      @OA\Property(property="name", type="string", description="Feature value name"),
 *                                      @OA\Property(property="slug", type="string", description="Feature value slug"),
 *                                  )
 *                              }
 *                          )
 *                       ),
 *                  )
 *              }
 *           ),
 *       )
 *   }
 * )
 */
class FeatureFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $features = $this->resource->toArray();
        $features['values'] = $features['full_values'];
        unset($features['full_values']);
        
        return $features;
    }
}
