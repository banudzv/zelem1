<?php

namespace App\Modules\Features\Models;

use App\Traits\ActiveScopeTrait;
use App\Traits\CheckRelation;
use App\Traits\ModelMain;
use Eloquent;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Modules\Features\Models\FeatureValue
 *
 * @property int $id
 * @property int $position
 * @property bool $active
 * @property int|null $feature_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read FeatureValueTranslates $current
 * @property-read Collection|FeatureValueTranslates[] $data
 * @property-read Feature|null $feature
 * @method static Builder|FeatureValue active($active = true)
 * @method static Builder|FeatureValue newModelQuery()
 * @method static Builder|FeatureValue newQuery()
 * @method static Builder|FeatureValue query()
 * @method static Builder|FeatureValue whereActive($value)
 * @method static Builder|FeatureValue whereCreatedAt($value)
 * @method static Builder|FeatureValue whereFeatureId($value)
 * @method static Builder|FeatureValue whereId($value)
 * @method static Builder|FeatureValue wherePosition($value)
 * @method static Builder|FeatureValue whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read int|null $data_count
 *
 * @see \App\Modules\Products\Services\ProductSearchServiceInterface::getFeatureValueStatsByRequest()
 * @property int|null products_count
 */
class FeatureValue extends Model
{
    use ModelMain, ActiveScopeTrait, EloquentTentacle, CheckRelation;

    protected $table = 'features_values';

    protected $casts = ['active' => 'boolean', 'position' => 'integer'];

    protected $fillable = ['position', 'feature_id', 'active'];

    public function feature()
    {
        return $this->belongsTo(Feature::class, 'feature_id', 'id');
    }

    public function getName(): ?string
    {
        return $this->name ?? $this->current->name;
    }

    public function getSlug(): ?string
    {
        return $this->slug ?? $this->current->slug;
    }
}
