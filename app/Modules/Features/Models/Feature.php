<?php

namespace App\Modules\Features\Models;

use App\Modules\Features\Filters\FeatureFilter;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupFeatureValue;
use App\Traits\ActiveScopeTrait;
use App\Traits\CheckRelation;
use App\Traits\ModelMain;
use Eloquent;
use EloquentFilter\Filterable;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class Feature
 *
 * @property int $id
 * @property bool $active
 * @property bool $in_filter
 * @property string $type
 * @property int $position
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property bool $main
 * @property-read FeatureTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|FeatureTranslates[] $data
 * @property-read \Illuminate\Database\Eloquent\Collection|FeatureValue[] $fullValues
 * @property-read string $link_in_admin_panel
 * @property-read mixed $values_dictionary
 * @property-read \Illuminate\Database\Eloquent\Collection|ProductGroup[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|FeatureValue[] $values
 * @method static Builder|Feature active($active = true)
 * @method static Builder|Feature newModelQuery()
 * @method static Builder|Feature newQuery()
 * @method static Builder|Feature query()
 * @method static Builder|Feature whereActive($value)
 * @method static Builder|Feature whereCreatedAt($value)
 * @method static Builder|Feature whereId($value)
 * @method static Builder|Feature whereInFilter($value)
 * @method static Builder|Feature whereMain($value)
 * @method static Builder|Feature wherePosition($value)
 * @method static Builder|Feature whereType($value)
 * @method static Builder|Feature whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read int|null $data_count
 * @property-read int|null $full_values_count
 * @property-read int|null $groups_count
 * @property-read int|null $values_count
 * @method static Builder|Feature filter($input = array(), $filter = null)
 * @method static Builder|Feature paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Feature simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Feature whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|Feature whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|Feature whereLike($column, $value, $boolean = 'and')
 */
class Feature extends Model
{
    use ModelMain, EloquentTentacle, ActiveScopeTrait, CheckRelation, Filterable;

    const TYPE_SINGLE = 'single';

    const TYPE_MULTIPLE = 'multiple';

    protected $casts = ['active' => 'boolean', 'main' => 'boolean', 'in_filter' => 'boolean'];

    protected $fillable = ['active', 'in_filter', 'type', 'main'];

    public static function allActive(?int $categoryId = null)
    {
        $query = Feature::with('current')->active(true);
        if ($categoryId) {
            $query->whereHas(
                'groups',
                function (Builder $builder) use ($categoryId) {
                    $builder->where('category_id', $categoryId);
                }
            );
        }
        return $query->oldest('position')->get();
    }

    public static function getList()
    {
        return Feature::with(['current'])->filter(request()->only('name', 'active'))->oldest('position')->get();
    }

    public static function getAllActiveByIds(array $ids): Collection
    {
        return Feature::with('current', 'values', 'values.current')
            ->whereIn('id', $ids)
            ->active(true)
            ->oldest('position')
            ->get();
    }

    /**
     * @return mixed
     */
    public function modelFilter()
    {
        return $this->provideFilter(FeatureFilter::class);
    }

    public function getLinkInAdminPanelAttribute(): string
    {
        return route('admin.features.edit', $this->id);
    }

    public function groups()
    {
        return $this->hasManyThrough(
            ProductGroup::class,
            ProductGroupFeatureValue::class,
            'feature_id',
            'id',
            'id',
            'group_id'
        );
    }

    /**
     * Values list
     *
     * @return HasMany
     */
    public function values()
    {
        return $this->hasMany(FeatureValue::class, 'feature_id', 'id')
            ->with('current')
            ->oldest('position');
    }

    public function fullValues()
    {
        return $this->hasMany(FeatureValue::class, 'feature_id', 'id')
            ->with('data')
            ->oldest('position');
    }

    public function getValuesDictionaryAttribute(): array
    {
        $dictionary = [];
        $this->values->loadMissing('current')->each(
            function (FeatureValue $value) use (&$dictionary) {
                $dictionary[$value->id] = $value->current->name;
            }
        );
        return $dictionary;
    }

    public function isMultiple(): bool
    {
        return $this->type === Feature::TYPE_MULTIPLE;
    }

    public function getName(): ?string
    {
        return $this->name ?? $this->current->name;
    }

    public function getSlug(): ?string
    {
        return $this->slug ?? $this->current->slug;
    }
}
