<?php

namespace App\Modules\Features\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroupFeatureValue;

/**
 * Class FeaturesForCompare
 *
 * @package App\Modules\Features\Widgets
 */
class FeaturesForCompare implements AbstractWidget
{
    /**
     * @var Product
     */
    private $product;

    /**
     * FeaturesOnProductPage constructor.
     *
     * @param array $featuresAndValues
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $values = $this->product->feature_values;

        if (empty($values)) {
            return [];
        }
        $features = [];

        $values->each(function (ProductGroupFeatureValue $featureValue) use (&$features) {
            if ($featureValue->value->active && $featureValue->feature->active) {
                $features[$featureValue->feature->current->name] =
                    $features[$featureValue->feature->current->name] ?? [];
                $features[$featureValue->feature->current->name][] = $featureValue->value->current->name;
            }
        });

        $engineNumbers = $this->product->group->allEngineNumbers;

        $features[trans('products::site.engine.car')] = $engineNumbers->map(function (EngineNumber $engineNumber) {
            return $engineNumber->vendor->name . ' ' . $engineNumber->model->name;
        })->unique()->sort()->toArray();

        $features[trans('products::site.engine.volume')] = $engineNumbers
            ->unique('volume_id')
            ->map(function (EngineNumber $engineNumber) {
                return $engineNumber->volume->name;
            })
            ->unique()
            ->toArray();

        $features[trans('products::site.engine.power')] = $engineNumbers
            ->unique('power_id')
            ->map(function (EngineNumber $engineNumber) {
                return $engineNumber->power->current->name;
            })
            ->unique()
            ->toArray();

        $features[trans('products::site.engine.number')] = $engineNumbers
            ->unique('engine_number')
            ->pluck('engine_number')
            ->toArray();

        $features[trans('products::site.engine.year')] = $this->product->group->years
            ->pluck('year')
            ->toArray();

        if (empty($features)) {
            return [];
        }

        return $features;
    }
    
}
