@php
/** @var \App\Modules\ProductsServices\Models\ProductService[] $services */
/** @var $conditions array */
@endphp

<div class="js-init" data-accordion='{"type":"multiple"}'>
    @if($conditions && count($conditions) > 0)
        <div class="conditions-item conditions-item--options _pl-none">
            <div class="conditions-item__head is-open">
                <div class="grid _flex-nowrap _items-center">
                    <div class="gcell _flex-noshrink">
                        <div class="conditions-item__icon">
                            {!! SiteHelpers\SvgSpritemap::get('icon-service') !!}
                        </div>
                    </div>
                    <div class="gcell _pl-sm">
                        <div class="conditions-item__title">
                            @lang('products_services::site.additional-services')
                        </div>
                    </div>
                </div>
            </div>
            <div class="is-open js-init" data-product-options>
                @foreach ($conditions as $key => $item)
                    <div class="conditions-item__item" data-option data-checked="false" data-option-id="{{ $item['value'] }}">
                        @component('site._widgets.checker.checker', [
                            'attributes' => [
                                'type' => 'checkbox',
                                'name' => $item['name'],
                                'checked' => $item['checked'],
                                'value' => $item['value'],
                                'data-checker',
                            ],
                            'class' => '_mr-sm',
                            'name' => $item['name'],
                            'disabled' => false,
                            'checked' => $item['checked']
                        ])
                            {{$item['label']}}
                        @endcomponent
                        @if (isset($item['select']))
                            <select id="sort-control" name="order" data-select class="select select--size-normal js-sort-control">
                                @foreach ($item['select'] as $option)
                                    <option data-price="{{ $option['formattedPrice'] }}" value="{{$option['value']}}">{{$option['label']}}</option>
                                @endforeach
                            </select>
                        @endif
                        <div class="conditions-item__item-price">
                            <span data-option-price>{{$item['formattedPrice']}}</span>
                            <div class="conditions-item__item-icon js-init" data-mfp="inline" data-mfp-src="#conditions-item-{{$key}}">
                                {!! SiteHelpers\SvgSpritemap::get('icon-info') !!}
                                <div class="template">
                                    <div id="conditions-item-{{$key}}" class="popup popup--condition-item">
                                        <div class="title title--normal title--size-h3 _color-dark-blue">
                                            {{$item['popup']['title']}}
                                        </div>
                                        <div class="wysiwyg _pb-def">
                                            {!!$item['popup']['text']!!}
                                        </div>
                                        @if($item['popup']['link'])
                                            <a href="{{$item['popup']['link']}}" class="link link--underline link--icon-small tooltip__link">
                                                @lang('products_services::site.details')
                                                <div class="icon">
                                                    {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right') !!}
                                                </div>
                                            </a>
                                        @endif
                                        <div class="mfp-close">
                                            {!! SiteHelpers\SvgSpritemap::get('icon-close') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@foreach($services as $position => $service)
        @if ($loop->iteration === 2)
            @include('site._widgets.conditions-item.conditions-item', [
                'icon' => ($service->show_icon && $service->icon) ? $service->icon : null,
                'title' => $service->current->name,
                'url' => route('site.products-services.info-popup', $service->id),
                'descr' => $service->current->description,
                'tabs' => $service->id,
                'isOpen' => !$position,
                'paymentIcon' => [
                     'card2',
                    'mastercard',
                    'visa',
                    'liqpay2'
                ]
            ])
        @elseif ($loop->iteration === 3)

        @else
            @include('site._widgets.conditions-item.conditions-item', [
                    'icon' => ($service->show_icon && $service->icon) ? $service->icon : null,
                    'title' => $service->current->name,
                    'url' => route('site.products-services.info-popup', $service->id),
                    'descr' => $service->current->description,
                    'tabs' => $service->id,
                     'paymentIcon' => [
                        'novaposhta',
                        'ukrposhta',
                        'delivery-man',
                        'delivery-hand'
                    ]
                ])
        @endif
    @endforeach
</div>
