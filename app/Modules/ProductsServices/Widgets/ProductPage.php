<?php

namespace App\Modules\ProductsServices\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\Service;
use App\Modules\Products\Models\ServicePrice;
use App\Modules\ProductsServices\Models\ProductService;
use Illuminate\Support\Collection;

/**
 * Class ProductPage
 *
 * @package App\Modules\ProductsServices\Widgets
 */
class ProductPage implements AbstractWidget
{
    /**
     * @var Service[]|Collection|null
     */
    protected $conditions;

    /**
     * ProductPage constructor.
     *
     * @param Collection|null|Service[] $conditions
     */
    public function __construct(?Collection $conditions)
    {
        $this->conditions = $conditions;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $services = ProductService::getList()->where('active', true);
        if ($services->isEmpty()) {
            return null;
        }

        return view('products_services::site.widget', [
            'services' => $services,
            'conditions' => $this->conditions->map(function (Service $service) {
                $firstOption = $service->options->first();
                $condition = [
                    'name' => $service->current->name,
                    'checked' => false,
                    'label' => $service->current->name,
                    'value' => $firstOption->id,
                    'price' => $firstOption->price,
                    'formattedPrice' => format_only($firstOption->price),
                    'popup' => [
                        'title' => $service->current->name,
                        'text' => $service->current->teaser,
                        'link' => $service->current->url,
                    ],
                ];
                if ($service->options->count() > 1) {
                    $condition['select'] = [];
                    $service->options->each(function (ServicePrice $option, int $index) use (&$condition) {
                        $condition['select'][] = [
                            'selected' => $index === 0,
                            'value' => $option->id,
                            'label' => $option->current->name,
                            'price' => $option->price,
                            'formattedPrice' => format_only($option->price),
                        ];
                    });
                }

                return $condition;
            })->toArray(),
        ]);
    }
    
}
