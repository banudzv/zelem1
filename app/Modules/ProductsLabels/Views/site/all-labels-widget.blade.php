@php
/** @var \App\Modules\ProductsLabels\Models\Label[]|\Illuminate\Database\Eloquent\Collection $labels */
@endphp

@foreach($labels as $label)
    @if($label && $label->current && $label->limitedGroups->count() >= config('db.labels.minimum-in-widget', \App\Modules\ProductsLabels\Models\Label::DEFAULT_MINIMUM_PRODUCTS_IN_WIDGET))
        {!! Widget::show(
            'products::groups-slider',
            $label->limitedGroups,
            $label->current->name,
            route('site.products-by-labels', $label->current->slug)
        ) !!}
    @endif
@endforeach
