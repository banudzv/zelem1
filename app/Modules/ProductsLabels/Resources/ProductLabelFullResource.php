<?php

namespace App\Modules\ProductsLabels\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProductLabelFullResource
 *
 * @package App\Modules\ProductsLabels\Resources
 *
 * @OA\Schema(
 *   schema="ProductLabelFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "active", "color", "show", "data"},
 *           @OA\Property(property="id", type="integer", description="ID"),
 *           @OA\Property(property="active", type="boolean", description="Label activity"),
 *           @OA\Property(property="color", type="string", description="Label color"),
 *           @OA\Property(property="show", type="boolean", description="Do we need to show this on the main page?"),
 *           @OA\Property(
 *              property="data",
 *              type="array",
 *              description="Multilanguage data",
 *              @OA\Items(
 *                  type="object",
 *                  allOf={
 *                      @OA\Schema(
 *                          required={"language", "name", "slug"},
 *                          @OA\Property(property="language", type="string", description="Language to store"),
 *                          @OA\Property(property="name", type="string", description="Label name"),
 *                          @OA\Property(property="text", type="string", description="Text on the label"),
 *                          @OA\Property(property="slug", type="string", description="Label slug"),
 *                          @OA\Property(property="content", type="string", description="Label full content"),
 *                          @OA\Property(property="h1", type="string", description="Label meta h1"),
 *                          @OA\Property(property="title", type="string", description="Label meta title"),
 *                          @OA\Property(property="description", type="string", description="Label meta description"),
 *                          @OA\Property(property="keywords", type="string", description="Label meta keywords"),
 *                      )
 *                  }
 *              )
 *           ),
 *       )
 *   }
 * )
 */
class ProductLabelFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->toArray();
    }
}
