<?php

namespace App\Modules\ProductsLabels\Widgets;

use Illuminate\Support\Facades\DB;
use App\Components\Widget\AbstractWidget;
use App\Modules\ProductsLabels\Models\Label;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupLabel;

/**
 * Class AllLabels
 *
 * @package App\Modules\ProductsLabels\Widgets
 */
class AllLabels implements AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $labels = Label::getListForMainPage();
        if ($labels->isEmpty()) {
            return null;
        }
        return view('labels::site.all-labels-widget', [
            'labels' => $labels,
        ]);
    }
    
}
