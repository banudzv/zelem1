<?php

namespace App\Modules\ProductsLabels\Controllers\Api;

use App\Core\ApiController;
use App\Modules\ProductsLabels\Models\Label;
use App\Modules\ProductsLabels\Resources\ProductLabelFullResource;

/**
 * Class LabelsController
 *
 * @package App\Modules\ProductsLabels\Controllers\Api
 */
class LabelsController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/product-labels/all",
     *     tags={"ProductLabels"},
     *     summary="Returns all possible product labels with all data",
     *     operationId="getProductLabelsFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/ProductLabelFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return ProductLabelFullResource::collection(Label::with(['data'])->oldest('position')->get());
    }
}
