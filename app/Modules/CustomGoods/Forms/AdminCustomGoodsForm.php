<?php

namespace App\Modules\CustomGoods\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\CustomGoods\Models\CustomGood;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\Text;
use Illuminate\Database\Eloquent\Model;
use Html;

/**
 * Class ArticleForm
 *
 * @package App\Core\Modules\Administrators\Forms
 */
class AdminCustomGoodsForm implements FormInterface
{

    /**
     * @param  Model|CustomGood|null $model
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $model = null): Form
    {
        $model = $model ?? new CustomGood();
        $form = Form::create();
        $form->buttons->doNotShowSaveAndAddButton();
        $user = Text::create('user_id')->setDefaultValue('-----');
        if ($model->user_id) {
            $user = Text::create('user_id')->setDefaultValue(Html::link(route('admin.users.edit',
                $model->user_id), $model->user->name, ['target' => '_blank']));
        }
        // Field set without languages tabs
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Toggle::create('active', $model)->setLabel(__('fast_orders::general.status'))->required(),
            Input::create('created_at', $model)->setOptions(['readonly']),
            Input::create('ip', $model)->setOptions(['readonly']),
            Text::create('first_name')->setDefaultValue(Html::tag(
                'div',
                __('validation.attributes.first_name').': ' . ($model->name ?: '&mdash;'),
                ['class' => ['form-group', 'text-bold']]
            )),
            Text::create('phone')->setDefaultValue(Html::tag(
                'div',
                __('validation.attributes.phone').': '. '<a href="tel:'.$model->phone.'">'.$model->phone.'</a>',
                ['class' => ['form-group', 'text-bold']]
            )),
            $model->product_id ? Text::create('product')->setDefaultValue(Html::tag(
                'div',
                __('fast_orders::general.product').': '. '<a href="'.route('admin.groups.edit', $model->product->group_id).'" target="_blank">'.$model->product->name.'</a>',
                ['class' => ['form-group', 'text-bold']]
            )) : null,
            $user
        );
        return $form;
    }

}
