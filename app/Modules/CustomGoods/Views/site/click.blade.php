<button class="button button--theme-main button--size-collapse-normal button--width-full js-init" data-mfp="ajax" data-mfp-src="{{ $url }}">
    <span class="button__body">
        {!! SiteHelpers\SvgSpritemap::get('icon-shopping', [
                'class' => 'button__icon button__icon--before'
            ]) !!}
        <span class="button__text">{{ __('custom_goods::site.button') }}</span>
    </span>
</button>
