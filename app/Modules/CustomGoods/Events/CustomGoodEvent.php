<?php

namespace App\Modules\CustomGoods\Events;

/**
 * Class FastOrderEventEvent
 *
 * @package App\Modules\CustomGoods\Events
 */
class CustomGoodEvent
{
    /**
     * @var string
    */
    public $phone;


    public $name = null;

    /**
     * @var int
    */
    public $productId;
    
    /**
     * @var int
     */
    public $orderId;

    /**
     * CustomGoodEvent constructor.
     * @param string $phone
     * @param string|null $name
     * @param int $productId
     * @param int $orderId
     */
    public function __construct(string $phone, string $name = null, int $productId, int $orderId)
    {
        $this->phone = $phone;
        $this->name = $name;
        $this->productId = $productId;
        $this->orderId = $orderId;
    }
}