<?php

namespace App\Modules\CustomGoods\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'custom_goods';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.permission-name',
                    'ru' => 'Товары под заказ',
                ],
                [
                    'name' => 'general.settings-name',
                    'ru' => 'Товары под заказ',
                ],
                [
                    'name' => 'general.menu',
                    'ru' => 'Товары под заказ',
                ],
                [
                    'name' => 'general.status',
                    'ru' => 'Обработан',
                ],
                [
                    'name' => 'general.product',
                    'ru' => 'Товар',
                ],
                [
                    'name' => 'general.message-success',
                    'ru' => 'Ваша форма успешно отправлена! Менеджер с Вами свяжется в кратчайший срок',
                ],
                [
                    'name' => 'general.message-false',
                    'ru' => 'Не удалось отправить форму, перезагрузите страницу и повторите попытку',
                ],
                [
                    'name' => 'general.form-phone',
                    'ru' => 'Номер телефона',
                ],
                [
                    'name' => 'general.form-submit-button',
                    'ru' => 'Отправить',
                ],
                [
                    'name' => 'general.notification',
                    'ru' => 'Новая заявка товара на заказ',
                ],
                [
                    'name' => 'general.attributes.phone',
                    'ru' => 'Телефон заказчика',
                ],
                [
                    'name' => 'general.block',
                    'ru' => 'Заказы',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Товары под заказ',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование заказа',
                ],
                [
                    'name' => 'settings.group-name',
                    'ru' => 'Товары под заказ',
                ],
                [
                    'name' => 'settings.attributes.per-page',
                    'ru' => 'Количество элементов на странице в админ панели',
                ],
                [
                    'name' => 'settings.attributes.required-name',
                    'ru' => 'Обязательное имя при отправке формы',
                ],
                [
                    'name' => 'general.mail-templates.names.custom-goods',
                    'ru' => 'Товар под заказ (Администратору)',
                ],
                [
                    'name' => 'general.mail-templates.names.custom-goods-for-user',
                    'ru' => 'Товар под заказ (Пользователю)',
                ],
                [
                    'name' => 'general.mail-templates.attributes.name',
                    'ru' => 'ФИО клиента',
                ],
                [
                    'name' => 'general.mail-templates.attributes.phone',
                    'ru' => 'Телефон клиента',
                ],
                [
                    'name' => 'general.mail-templates.attributes.name-user',
                    'ru' => 'ФИО пользователя',
                ],
                [
                    'name' => 'general.mail-templates.attributes.phone-user',
                    'ru' => 'Телефон пользователя',
                ],
                [
                    'name' => 'general.mail-templates.attributes.admin_href',
                    'ru' => 'Ссылка на админ панель',
                ],
                [
                    'name' => 'seo.count',
                    'ru' => 'Количество заказов - :count',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.button',
                    'ru' => 'Заказать',
                ],
                [
                    'name' => 'site.popup.buy-by-one-click',
                    'ru' => 'Заказать',
                ],
                [
                    'name' => 'site.popup-name',
                    'ru' => 'Введите ФИО',
                ],
                [
                    'name' => 'site.popup-phone',
                    'ru' => 'Введите номер телефона',
                ],
                [
                    'name' => 'site.popup.enter-the-phone-number',
                    'ru' => 'Введите номер телефона и мы Вам перезвоним для оформления заказа',
                ],
                [
                    'name' => 'site.popup.enter-the-phone-number',
                    'ru' => 'Введите номер телефона и мы Вам перезвоним для оформления заказа',
                ],
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
