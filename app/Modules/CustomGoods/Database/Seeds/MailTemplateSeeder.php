<?php

namespace App\Modules\CustomGoods\Database\Seeds;

use App\Core\Modules\Languages\Models\Language;
use App\Core\Modules\Mail\Models\MailTemplate;
use App\Core\Modules\Mail\Models\MailTemplateTranslates;
use Illuminate\Database\Seeder;

class MailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $template = new MailTemplate();
        $template->name = 'custom_goods::general.mail-templates.names.custom-goods';
        $template->alias = 'custom-goods';
        $template->variables = [
            'phone' => 'custom_goods::general.mail-templates.attributes.phone',
            'name' => 'custom_goods::general.mail-templates.attributes.name'];
        $template->save();
    
        Language::all()->each(function (Language $language) use ($template) {
            $translate = new MailTemplateTranslates();
            $translate->language = $language->slug;
            $translate->row_id = $template->id;
            $translate->subject = 'New custom goods';
            $translate->text = 'You have new custom goods. User phone {phone}, name {name}';
            $translate->save();
        });

        $template = new MailTemplate();
        $template->name = 'custom_goods::general.mail-templates.names.custom-goods-for-user';
        $template->alias = 'custom-goods-for-user';
        $template->variables = [
            'phone' => 'custom_goods::general.mail-templates.attributes.phone-user',
            'name' => 'custom_goods::general.mail-templates.attributes.name-user'
        ];
        $template->save();

        Language::all()->each(function (Language $language) use ($template) {
            $translate = new MailTemplateTranslates();
            $translate->language = $language->slug;
            $translate->row_id = $template->id;
            $translate->subject = 'New custom goods';
            $translate->text = 'Than you for order. Your phone {phone}, name {name}';
            $translate->save();
        });
    }
}
