<?php

namespace App\Modules\CustomGoods;

use App\Core\Modules\Notifications\Types\NotificationType;
use App\Modules\CustomGoods\Listeners\NewCustomGoods;
use App\Modules\CustomGoods\Widgets\CustomGoodClick;
use App\Modules\CustomGoods\Widgets\CustomGoods;
use App\Modules\CustomGoods\Models\CustomGood as CustomGoodsModel;
use CustomForm\Macro\Toggle;
use Widget;
use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use CustomForm\Input;
use CustomSettings, CustomRoles, CustomMenu;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\CustomGoods
 */
class Provider extends BaseProvider
{

    /**
    * Set custom presets
    */
    protected function presets()
    {
        $this->registerNotificationType(
            NewCustomGoods::NOTIFICATION_TYPE,
            NewCustomGoods::NOTIFICATION_ICON,
            NotificationType::COLOR_GREEN
        );
    }
    
    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register module configurable settings
        $settings = CustomSettings::createAndGet('custom_goods', 'custom_goods::settings.group-name');
        $settings->add(
            Input::create('per-page')->setLabel('custom_goods::settings.attributes.per-page'),
            ['required', 'integer', 'min:1']
        );
        $settings->add(
            Toggle::create('required-name')
                ->setLabel('custom_goods::settings.attributes.required-name')
                ->setDefaultValue(false)
                ->required(),
            ['required', 'boolean']
        );
        // Register left menu block
        $group = CustomMenu::get()->group();
        $group
            ->block('orders')
            ->link('custom_goods::general.menu', RouteObjectValue::make('admin.custom_goods.index'))
            ->addCounter(CustomGoodsModel::whereActive(false)->count(), 'bg-blue')
            ->additionalRoutesForActiveDetect(RouteObjectValue::make('admin.custom_goods.edit'));
        // Register role scopes
        CustomRoles::add('custom_goods', 'custom_goods::general.menu')
            ->except(RoleRule::VIEW, RoleRule::STORE);
    }

    
    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
       Widget::register(CustomGoodClick::class, 'custom-goods::button');
       Widget::register(CustomGoods::class, 'custom-goods-one-click');
    }

}
