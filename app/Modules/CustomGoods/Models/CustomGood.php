<?php

namespace App\Modules\CustomGoods\Models;

use App\Modules\CustomGoods\Filters\CustomGoodsFilter;
use App\Modules\Products\Models\Product;
use App\Modules\Users\Models\User;
use App\Traits\FormattedDateAdmin;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\CustomGoods\Models\CustomGood
 *
 * @property int $id
 * @property string $phone
 * @property string|null $name
 * @property string $ip
 * @property int $product_id
 * @property int|null $user_id
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Modules\Products\Models\Product $product
 * @property-read \App\Modules\Users\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\CustomGoods\Models\CustomGood whereUserId($value)
 * @mixin \Eloquent
 * @property-read null|string $formatted_date
 * @property-read null|string $published_date
 */
class CustomGood extends Model
{
    use Filterable, FormattedDateAdmin;

    protected $table = 'custom_goods';

    protected $casts = ['active' => 'boolean'];

    protected $fillable = ['phone', 'name', 'product_id', 'user_id', 'ip', 'active'];

    /**
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(CustomGoodsFilter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList()
    {
        return CustomGood::filter(request()->all())
            ->latest()->paginate(config('db.fast_orders.per-page', 10));
    }
}
