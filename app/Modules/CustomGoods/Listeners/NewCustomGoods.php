<?php

namespace App\Modules\CustomGoods\Listeners;

use App\Components\Mailer\MailSender;
use App\Core\Interfaces\ListenerInterface;
use App\Core\Modules\Mail\Models\MailTemplate;
use App\Core\Modules\Notifications\Models\Notification;
use App\Modules\CustomGoods\Events\CustomGoodEvent;
use Illuminate\Support\Facades\Auth;

/**
 * Class NewCustomGoods
 *
 * @package App\Modules\CustomGoods\Listeners
 */
class NewCustomGoods implements ListenerInterface
{

    const NOTIFICATION_TYPE = 'custom-good';

    const NOTIFICATION_ICON = 'fa fa-cart-plus';

    public static function listens(): string
    {
        return CustomGoodEvent::class;
    }

    /**
     * Handle the event.
     *
     * @param NewCustomGoods $event
     * @return void
     */
    public function handle(CustomGoodEvent $event)
    {
        $this->sendMail($event);
        $this->sendMailUser($event);
        Notification::send(
            static::NOTIFICATION_TYPE,
            'custom_goods::general.notification',
            'admin.custom_goods.edit',
            ['customGood' => $event->orderId]
        );
    }

    /**
     * @param CustomGoodEvent $event
     */
    public function sendMail(CustomGoodEvent $event)
    {
        if (!config('db.basic.admin_email')) {
            return;
        }
        $template = MailTemplate::getTemplateByAlias('custom-goods');
        if (!$template) {
            return;
        }
        $from = [
            '{phone}',
            '{name}',
            '{admin_href}',
        ];
        $to = [
            $event->phone,
            $event->name,
            route('admin.custom_goods.edit', ['id' => $event->orderId])
        ];
        $subject = str_replace($from, $to, $template->current->subject);
        $body = str_replace($from, $to, $template->current->text);
        MailSender::send(config('db.basic.admin_email'), $subject, $body);
    }


    public function sendMailUser(CustomGoodEvent $event)
    {
        if (Auth::guest() or !Auth::user()->email) {
            return;
        }
        $template = MailTemplate::getTemplateByAlias('custom-goods-for-user');
        if (!$template) {
            return;
        }
        $from = [
            '{phone}',
            '{name}'
        ];
        $to = [
            $event->phone,
            $event->name
        ];
        $subject = str_replace($from, $to, $template->current->subject);
        $body = str_replace($from, $to, $template->current->text);
        MailSender::send(Auth::user()->email, $subject, $body);
    }
}
