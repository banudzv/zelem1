<?php

namespace App\Modules\CustomGoods\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\CustomGoods\Filters\CustomGoodsFilter;
use App\Modules\CustomGoods\Forms\AdminCustomGoodsForm;
use App\Modules\CustomGoods\Models\CustomGood;
use App\Modules\CustomGoods\Requests\AdminCustomGoodsRequest;
use Seo;

/**
 * Class CustomGoodsController
 *
 * @package App\Modules\CustomGoods\Controllers\Admin
 */
class CustomGoodsController extends AdminController
{
    public function __construct()
    {
        Seo::breadcrumbs()->add('custom_goods::seo.index', RouteObjectValue::make('admin.custom_goods.index'));
    }

    /**
     * CustomGoods sortable list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function index()
    {
        // Set h1
        Seo::meta()->setH1('custom_goods::seo.index');
        Seo::meta()->setCountOfEntity(trans('custom_goods::seo.count',['count' => CustomGood::count(['id'])]));
        // Get CustomGoods
        $customGoods = CustomGood::getList();
        // Return view list
        return view('custom_goods::admin.index', [
            'customGoods' => $customGoods,
            'filter' => CustomGoodsFilter::showFilter()
        ]);
    }
    
    /**
     * Update element page
     *
     * @param  CustomGood $customGood
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(CustomGood $customGood)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($customGood->name ?? 'custom_goods::seo.edit');
        // Set h1
        Seo::meta()->setH1('custom_goods::seo.edit');
        // Javascript validation
        $this->initValidation((new AdminCustomGoodsRequest())->rules());
        // Return form view
        return view(
            'custom_goods::admin.update', [
                'form' => AdminCustomGoodsForm::make($customGood),
            ]
        );
    }
    
    /**
     * Update page in database
     *
     * @param  AdminCustomGoodsRequest $request
     * @param  CustomGood $customGood
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(AdminCustomGoodsRequest $request, CustomGood $customGood)
    {
        // Fill new data
        $customGood->fill($request->all());
        // Update existed page
        $customGood->save();
        // Do something
        return $this->afterUpdate();
    }
    
    /**
     * Totally delete page from database
     *
     * @param  CustomGood $customGood
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(CustomGood $customGood)
    {
        // Delete callback
        $customGood->forceDelete();
        // Do something
        return $this->afterDestroy();
    }
}
