<?php

namespace App\Modules\CustomGoods\Controllers\Site;

use App\Core\SiteController;
use App\Modules\CustomGoods\Requests\SiteCustomGoodsRequest;
use Illuminate\Http\Request;

class AjaxController extends SiteController
{
    public function popup(Request $request, int $productId)
    {
        return \Widget::show('custom-goods-one-click', $productId) ?? '';
    }
}
