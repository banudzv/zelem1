<?php

namespace App\Modules\CustomGoods\Controllers\Site;

use App\Core\SiteController;
use App\Modules\CustomGoods\Events\CustomGoodEvent;
use App\Modules\CustomGoods\Models\CustomGood;
use App\Modules\CustomGoods\Requests\SiteCustomGoodsRequest;
use Auth, Event;
use App\Core\AjaxTrait;

/**
 * Class CustomGoodsController
 * @package App\Modules\CustomGoods\Controllers\Site
 */
class CustomGoodsController extends SiteController
{
    use AjaxTrait;
    
    /**
     * @param SiteCustomGoodsRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function send(SiteCustomGoodsRequest $request)
    {
        $customGood = new CustomGood();
        $customGood->fill($request->all());
        $customGood->ip = request()->ip();
        $customGood->user_id = Auth::id();
        if($customGood->save()){
            $customGood->save();
            Event::dispatch(new CustomGoodEvent($customGood->phone, $customGood->name, $customGood->product_id, $customGood->id));
            return $this->successMfpMessage(trans('custom_goods::general.message-success'));
        }
        return $this->errorJsonAnswer([
            'notyMessage' => trans('custom_goods::general.message-false'),
        ]);
        
    }

}
