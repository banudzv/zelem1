<?php

namespace App\Modules\CustomGoods\Widgets;

use App\Components\Widget\AbstractWidget;

class CustomGoodClick implements AbstractWidget
{
    
    private $productId;
    
    public function __construct(int $productId)
    {
        $this->productId = $productId;
    }
    
    public function render()
    {
        return view('custom_goods::site.click', [
            'url' => route('custom-goods-popup', $this->productId),
        ]);
    }
    
}
