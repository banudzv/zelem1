<?php

namespace App\Modules\CustomGoods\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\CustomGoods\Requests\SiteCustomGoodsRequest;
use Auth;

/**
 * Class Popup
 *
 * @package App\Modules\Callback\Widgets
 */
class CustomGoods implements AbstractWidget
{
    /**
     * @var int
     */
    protected $productId;

    /**
     * ProductCard constructor.
     *
     * @param int $product
     */
    public function __construct(int $productId)
    {
        $this->productId = $productId;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $formId = uniqid('orders-one-click-buy');
        $validation = \JsValidator::make(
            (new SiteCustomGoodsRequest())->rules(),
            [],
            [],
            "#$formId"
        );
        return view('custom_goods::site.popup.one-click-buy', [
            'productId'=> $this->productId,
            'formId' => $formId,
            'validation' => $validation,
            'phone' => Auth::check() ? Auth::user()->phone : null,
            'name' => Auth::check() ? Auth::user()->name : null,
        ]);
    }

}
