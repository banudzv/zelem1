<?php

use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:custom_goods'])->group(function () {
    // Admins list in admin panel
    Route::put('custom-goods/{customGood}/active', ['uses' => 'CustomGoodsController@active', 'as' => 'admin.custom_goods.active']);
    Route::get('custom-goods/{customGood}/destroy', ['uses' => 'CustomGoodsController@destroy', 'as' => 'admin.custom_goods.destroy']);
    Route::resource('custom-goods', 'CustomGoodsController')
        ->except('show', 'destroy', 'store', 'create')
        ->names('admin.custom_goods');
});
