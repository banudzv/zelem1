<?php

use Illuminate\Support\Facades\Route;

// Frontend routes
Route::get('custom-goods', ['as' => 'custom-goods', 'uses' => 'CustomGoodsController@index']);
Route::post('custom-goods/send', ['as' => 'custom-goods-send', 'uses' => 'CustomGoodsController@send']);
Route::post('custom-goods-popup/{productId}',['as' => 'custom-goods-popup', 'uses' => 'AjaxController@popup']);