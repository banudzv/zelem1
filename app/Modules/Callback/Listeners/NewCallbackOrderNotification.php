<?php

namespace App\Modules\Callback\Listeners;

use App\Components\Crm\Amo;
use App\Components\Crm\Bitrix;
use App\Core\Interfaces\ListenerInterface;
use App\Core\Modules\Mail\Models\MailTemplate;
use App\Core\Modules\Notifications\Models\Notification;
use App\Modules\Callback\Models\Callback;
use App\Modules\Subscribe\Events\NewSubscriberEvent;
use App\Components\Mailer\MailSender;

/**
 * Class NewCallbackOrderNotification
 *
 * @package App\Modules\Callback\Listeners
 */
class NewCallbackOrderNotification implements ListenerInterface
{

    const NOTIFICATION_TYPE = 'callback';

    const NOTIFICATION_ICON = 'fa fa-phone';

    public static function listens(): string
    {
        return 'callback';
    }

    /**
     * Handle the event.
     *
     * @param Callback $callback
     * @return void
     */
    public function handle(Callback $callback)
    {
        if (config('db.amo_crm.use_amo')) {
            $this->addInAmoCrm($callback);
        }
        $this->sendBitrix($callback);
        Notification::send(
            static::NOTIFICATION_TYPE,
            'callback::general.notification',
            'admin.callback.edit',
            ['id' => $callback->id]
        );
    }

    /**
     * @param Callback $callback
     */
    public function addInAmoCrm(Callback $callback)
    {
        $data = [];
        $data['formName'] = __('callback::general.notification');
        $data['name'] = $callback->name ?: $callback->phone;
        $data['phone'] = $callback->phone;
        $amo = new Amo();
        $amo->newLead($data);
    }

    /**
     * @param Callback $callback
     */
    public function sendBitrix(Callback $callback)
    {
        $data = [];
        $data['formName'] = __('callback::general.notification');
        $data['requestType'] = 'call-request';
        $data['name'] = $callback->name ?: $callback->phone;
        $data['phone'] = $callback->phone;

        $bitrix = new Bitrix();
        $bitrix->send($data);
    }
}
