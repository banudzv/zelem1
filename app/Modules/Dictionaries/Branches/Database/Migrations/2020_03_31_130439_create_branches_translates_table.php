<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);

            $table->foreign('language')->references('slug')->on('languages')
                ->index('branches_translates_language_languages_slug')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('branches')
                ->index('branches_translates_row_id_branches_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (app()->environment() !== 'production') {
            Schema::table('branches_translates', function (Blueprint $table) {
                $table->dropForeign('branches_translates_language_languages_slug');
                $table->dropForeign('branches_translates_row_id_branches_id');
            });
            Schema::dropIfExists('branches_translates');
        }
    }
}
