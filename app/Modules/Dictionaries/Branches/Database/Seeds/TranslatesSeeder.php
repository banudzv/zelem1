<?php

namespace App\Modules\Dictionaries\Branches\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'branches';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'dictionaries.general.menu-block',
                    'ru' => 'Справочники',
                ],
                [
                    'name' => 'general.menu',
                    'ru' => 'Филиалы',
                ],
                [
                    'name' => 'general.phones',
                    'ru' => 'Телефоны (если больше одного, то указывать через запятую)',
                ],
                [
                    'name' => 'general.show_on_map',
                    'ru' => 'Показывать на карте',
                ],
                [
                    'name' => 'general.location',
                    'ru' => 'Расположение на карте',
                ],
                [
                    'name' => 'general.address',
                    'ru' => 'Адрес',
                ],
                [
                    'name' => 'general.storage',
                    'ru' => 'Время работы (склад)',
                ],
                [
                    'name' => 'general.office',
                    'ru' => 'Время работы (офис)',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Филиалы',
                ],
                [
                    'name' => 'seo.create',
                    'ru' => 'Добавление филиала',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование филиала',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'general.storage',
                    'ru' => 'Склад:',
                ],
                [
                    'name' => 'general.office',
                    'ru' => 'Офис:',
                ],
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
