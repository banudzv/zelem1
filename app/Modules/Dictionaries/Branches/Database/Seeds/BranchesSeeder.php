<?php

namespace App\Modules\Dictionaries\Branches\Database\Seeds;

use App\Core\Modules\Languages\Models\Language;
use App\Core\Modules\Settings\Models\Setting;
use App\Modules\Dictionaries\Branches\Models\Branch;
use App\Modules\Dictionaries\Branches\Models\BranchTranslates;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Exception;
use Log;
use DB;

class BranchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data() as $key => $branch) {
            DB::beginTransaction();

            try {
                $branch['active'] = true;
                $branch['position'] = $key;

                Branch::firstOrCreate(
                    ['id' => $key + 1],
                    Arr::except($branch, ['languages'])
                );

                Language::all()->each(
                    function (Language $language) use ($branch, $key) {
                        $branchTranslates = $branch['languages'][$language->slug]
                            ?? $branch['languages']['ru'];
                        $branchTranslates['row_id'] = $key + 1;
                        $branchTranslates['language'] = $language->slug;

                        BranchTranslates::firstOrCreate(
                            [
                                'row_id' => $branchTranslates['row_id'],
                                'language' => $branchTranslates['language']
                            ],
                            $branchTranslates
                        );
                    }
                );

                DB::commit();
            } catch (Exception $exception) {
                DB::rollBack();
                Log::error($exception->getMessage());
            }
        }
    }

    protected function data()
    {
        return [
            [
                'location' => [
                    'lat' => 46.4735337,
                    'lng' => 30.7136537,
                    'zoom' => 16
                ],
                'languages' => [
                    'ru' => [
                        'name' => 'Одесса',
                    ],
                    'ua' => [
                        'name' => 'Одеса'
                    ]
                ]
            ],
            [
                'location' => [
                    'lat' => 50.0000572,
                    'lng' => 36.2164431,
                    'zoom' => 16
                ],
                'languages' => [
                    'ru' => [
                        'name' => 'Харьков',
                    ],
                    'ua' => [
                        'name' => 'Харків'
                    ]
                ]
            ],
            [
                'location' => [
                    'lat' => 50.4128615,
                    'lng' => 30.5139132,
                    'zoom' => 16
                ],
                'languages' => [
                    'ru' => [
                        'name' => 'Киев',
                    ],
                    'ua' => [
                        'name' => 'Київ'
                    ]
                ]
            ],
            [
                'location' => [
                    'lat' => 49.8550883,
                    'lng' => 24.0725209,
                    'zoom' => 16
                ],
                'languages' => [
                    'ru' => [
                        'name' => 'Львов',
                    ],
                    'ua' => [
                        'name' => 'Львів'
                    ]
                ]
            ]
        ];
    }
}
