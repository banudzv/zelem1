<?php

use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:branches'])->group(function() {
    Route::prefix('dictionaries')->group(function() {

        Route::prefix('branches')->group(function() {
            Route::put('{branch}/active', ['uses' => 'IndexController@active','as' => 'admin.branches.active']);
            Route::put('sortable',['uses' => 'IndexController@sortable', 'as' => 'admin.branches.sortable']);
            Route::get('{branch}/destroy', ['uses' => 'IndexController@destroy', 'as' => 'admin.branches.destroy']);
        });

        Route::resource('branches', 'IndexController')->except('show', 'destroy')->names('admin.branches');
    });
});
