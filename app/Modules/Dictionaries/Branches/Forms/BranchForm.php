<?php

namespace App\Modules\Dictionaries\Branches\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Dictionaries\Branches\Models\Branch;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\Map;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BranchForm
 *
 * @package App\Modules\Dictionaries\Branches\Forms
 */
class BranchForm implements FormInterface
{

    /**
     * @param Model|Branch|null $model
     *
     * @return Form
     * @throws WrongParametersException
     */
    public static function make(?Model $model = null): Form
    {
        $model = $model ?? new Branch();

        $form = Form::create();
        $form->fieldSet()->add(
            Toggle::create('active', $model)
                ->required()
                ->setDefaultValue(true)
        );

        $form->fieldSet()
            ->add(
                Map::create('location', $model)
                    ->setLabel('branches::general.location')
                    ->setOptions(['input' => 'location'] + ($model->location ? $model->location : []))
            );

        $form->fieldSetForLang()->add(
            Input::create('name', $model)->required()
        );

        return $form;
    }

}
