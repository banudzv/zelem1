<?php

namespace App\Modules\Dictionaries\Branches\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BranchRequest
 *
 * @package App\Modules\Dictionaries\Branches\Requests
 */
class BranchRequest extends FormRequest implements RequestInterface
{

    use ValidationRulesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return $this->generateRules([
            'active' => ['required', 'bool'],
            'location' => ['sometimes', 'array'],
        ], [
            'name' => ['required', 'max:191'],
        ]);
    }


    public function attributes()
    {
        $attributes = [
            'active' => trans('validation.attributes.available'),
        ];
        foreach (config('languages', []) as $slug => $language) {
            $attributes += [
                $slug . '.name' => trans('validation.attributes.name'),
            ];
        }

        return $attributes;
    }

}
