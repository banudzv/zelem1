<?php

namespace App\Modules\Dictionaries\Branches\Models;

use App\Core\Interfaces\Location;
use App\Traits\ActiveScopeTrait;
use App\Traits\ModelMain;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


/**
 * App\Modules\Dictionaries\Branches\Models\Branch
 *
 * @property int $id
 * @property bool $active
 * @property array $location
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read BranchTranslates $current
 * @property-read Collection|BranchTranslates[] $data
 * @method static Builder|Branch newModelQuery()
 * @method static Builder|Branch newQuery()
 * @method static Builder|Branch query()
 * @method static Builder|Branch whereActive($value)
 * @method static Builder|Branch whereCreatedAt($value)
 * @method static Builder|Branch whereId($value)
 * @method static Builder|Branch whereUpdatedAt($value)
 * @method static Builder|Branch published()
 * @mixin Eloquent
 */
class Branch extends Model implements Location
{
    use ModelMain;
    use ActiveScopeTrait;

    protected $table = 'branches';

    protected $casts = [
        'active' => 'boolean',
        'location' => 'array',
    ];

    protected $fillable = [
        'active',
        'location'
    ];

    public static function getList(): Collection
    {
        return self::oldest('position')->latest()->get();
    }

    public static function getListForClientSide()
    {
        return self::with(['current'])
            ->active()
            ->oldest('position')
            ->get();
    }

    public function locationData(): array
    {
        return [
            'lat' => $this->location['lat'],
            'lng' => $this->location['lng'],
            'zoom' => $this->location['zoom'],
        ];
    }

    public function getCoordinatesString()
    {
        return $this->location['lat'] . ',' . $this->location['lng'];
    }

    public function getMapSettingsAttribute()
    {
        $settings = [];
        if ($this->location) {
            $settings = [
                'center' => [
                    'lat' => (float) $this->location['lat'],
                    'lng' => (float) $this->location['lng'],
                ],
                'zoom' => (int) $this->location['zoom']
            ];
        }

        return json_encode($settings);
    }
}
