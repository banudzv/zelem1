<?php

namespace App\Modules\Dictionaries\Branches\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Modules\Dictionaries\Branches\Models\BranchTranslates
 *
 * @property int $id
 * @property string $name
 * @property int $row_id
 * @property string $language
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\Dictionaries\Branches\Models\Branch $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Dictionaries\Branches\Models\BranchTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Dictionaries\Branches\Models\BranchTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Dictionaries\Branches\Models\BranchTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Dictionaries\Branches\Models\BranchTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Dictionaries\Branches\Models\BranchTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Dictionaries\Branches\Models\BranchTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Dictionaries\Branches\Models\BranchTranslates whereRowId($value)
 * @mixin \Eloquent
 */
class BranchTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'branches_translates';

    public $timestamps = false;

    protected $fillable = ['name'];

}
