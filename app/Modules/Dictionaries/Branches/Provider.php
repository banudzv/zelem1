<?php

namespace App\Modules\Dictionaries\Branches;

use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use CustomMenu, Widget, CustomRoles;
use App\Core\BaseProvider;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Index
 */
class Provider extends BaseProvider
{

    /**
     * Set custom presets
     */
    protected function presets()
    {

    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register left menu block
        CustomMenu::get()->group()
            ->block(
                'dictionaries',
                'branches::dictionaries.general.menu-block',
                'fa fa-files-o')
            ->setPosition(25)
            ->link('branches::general.menu', RouteObjectValue::make('admin.branches.index'))
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.branches.edit'), RouteObjectValue::make('admin.branches.create')
            );
        // Register role scopes
        CustomRoles::add('branches', 'branches::general.menu')->except(RoleRule::VIEW);
    }

    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
    }

}
