<?php

namespace App\Modules\Dictionaries\Branches\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Dictionaries\Branches\Forms\BranchForm;
use App\Modules\Dictionaries\Branches\Models\Branch;
use App\Modules\Dictionaries\Branches\Requests\BranchRequest;
use Seo;

/**
 * Class IndexController
 *
 * @package App\Modules\Dictionaries\Branches\Controller
 */
class IndexController extends AdminController
{

    /**
     * AdminsController constructor.
     */
    public function __construct()
    {
        Seo::breadcrumbs()->add('branches::seo.index', RouteObjectValue::make('admin.branches.index'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->addCreateButton('admin.branches.create');
        // Set h1
        Seo::meta()->setH1('branches::seo.index');
        // Get article
        $branches = Branch::getList();
        if ($branches->count() > 0) {
            return view('branches::admin.index', [
                'branches' => $branches,
            ]);
        }
        return view('branches::admin.no-pages');
    }

    /**
     * @param Branch $branch
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(Branch $branch)
    {
        Seo::meta()->setH1('branches::seo.edit');
        Seo::breadcrumbs()->add('branches::seo.edit');
        $this->jsValidation(new BranchRequest());
        return view('branches::admin.update', ['form' => BranchForm::make($branch)]);
    }

    /**
     * @param BranchRequest $request
     * @param Branch $branch
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Throwable
     */
    public function update(BranchRequest $request, Branch $branch)
    {
        // Update existed branch
        if ($message = $branch->updateRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterUpdate();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function create()
    {
        Seo::meta()->setH1('branches::seo.create');
        Seo::breadcrumbs()->add('branches::seo.create');
        $this->jsValidation(new BranchRequest());
        return view('branches::admin.create', ['form' => BranchForm::make()]);
    }

    /**
     * @param BranchRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Throwable
     */
    public function store(BranchRequest $request)
    {
        $branch = new Branch();
        // Create new branch
        if ($message = $branch->createRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterStore(['branch' => $branch->id]);
    }

    /**
     * @param Branch $branch
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException|\Exception
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();
        return $this->afterDestroy();
    }

}
