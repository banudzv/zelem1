<?php
/**
 * @var \App\Modules\Dictionaries\Branches\Models\Branch[]|\Illuminate\Support\Collection $branches
 */
?>

@foreach ($branches as $branch)
    <li class="dd-item dd3-item" data-id="{{ $branch->id }}">
        <div title="Drag" class="dd-handle dd3-handle">Drag</div>
        <div class="dd3-content">
            <table style="width: 100%;">
                <tr>
                    <td class="column-drag" width="1%"></td>
                    <td valign="top" class="pagename-column">
                        <div class="clearFix">
                            <div class="pull-left">
                                <div class="overflow-20">
                                    <a class="pageLinkEdit" href="{{ route('admin.branches.edit', $branch->id) }}" style="color: {{ $branch->color }};">
                                        {{ $branch->current->name }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td width="30" valign="top" class="icon-column status-column">
                        {!! Widget::active($branch, 'admin.branches.active') !!}
                    </td>
                    <td class="nav-column icon-column" valign="top" width="100" align="right">
                        {!! \App\Components\Buttons::edit('admin.branches.edit', $branch->id) !!}
                        {!! \App\Components\Buttons::delete('admin.branches.destroy', $branch->id) !!}
                    </td>
                </tr>
            </table>
        </div>
    </li>
@endforeach
