@php
/** @var \App\Modules\Dictionaries\Branches\Models\Branch[] $branches */
$className = \App\Modules\Dictionaries\Branches\Models\Branch::class;
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        <div class="dd pageList" id="myNest" data-depth="1">
            <ol class="dd-list">
                @include('branches::admin.items', ['branches' => $branches])
            </ol>
        </div>
        <span id="parameters" data-url="{{ route('admin.branches.sortable', ['class' => $className]) }}"></span>
        <input type="hidden" id="myNestJson">
    </div>
@stop
