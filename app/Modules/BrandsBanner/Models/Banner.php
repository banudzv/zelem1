<?php

namespace App\Modules\BrandsBanner\Models;

use App\Modules\Brands\Models\Brand;
use App\Modules\BrandsBanner\Filters\BannerFilter;
use App\Modules\BrandsBanner\Filters\BrandsFilter;
use App\Modules\BrandsBanner\Images\BannerImage;
use App\Modules\BrandsBanner\Images\BrandImage;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Traits\ActiveScopeTrait;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use EloquentFilter\Filterable;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\BrandsBanner\Models\Banner
 *
 * @property int $id
 * @property bool $active
 * @property int $brand_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Modules\Images\Models\Image[] $allImages
 * @property-read \App\Modules\BrandsBanner\Models\BannerTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\BrandsBanner\Models\BannerTranslates[] $data
 * @property-read \App\Core\Modules\Images\Models\Image $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Modules\Images\Models\Image[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner active($active = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\Banner whereBrandId($value)
 * @mixin \Eloquent
 * @property-read \App\Modules\Brands\Models\Brand $brand
 */
class Banner extends Model
{
    use ModelMain, Imageable, EloquentTentacle, ActiveScopeTrait, Filterable;

    protected $casts = ['active' => 'boolean'];

    protected $fillable = ['active', 'brand_id'];

    protected $table = 'brands_banner';

    /**
     * Product image configurations class
     *
     * @return string
     */
    protected function imageClass()
    {
        return BannerImage::class;
    }

    /**
     * Register filter model
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(BannerFilter::class);
    }

    /**
     * Returns list of brands to show in admin panel
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList()
    {
        return Banner::with('current')
            ->filter(request()->only('name', 'active'))
            ->latest('id')
            ->paginate(config('db.brands.per-page', 10));
    }
    public function brand()
    {
        return $this->hasOne(Brand::class, 'id', 'brand_id');
    }

}
