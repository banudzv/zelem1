<?php

namespace App\Modules\BrandsBanner\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Lang;

/**
 * App\Modules\BrandsBanner\Models\BannerTranslates
 *
 * @property int $id
 * @property string $name
 * @property string|null $content
 * @property string|null $url
 * @property int $row_id
 * @property string $language
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\BrandsBanner\Models\Banner $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereRowId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\BrandsBanner\Models\BannerTranslates whereTitle($value)
 * @mixin \Eloquent
 */
class BannerTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'brands_banner_translates';

    public $timestamps = false;

    protected $fillable = ['name', 'content', 'url', 'row_id', 'language'];

}
