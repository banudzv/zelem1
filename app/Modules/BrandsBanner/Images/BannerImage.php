<?php

namespace App\Modules\BrandsBanner\Images;

use App\Components\Image\ImagesGroup;
use App\Core\Abstractions\ImageContainer;

/**
 * Class ArticlesImage
 *
 * @package App\Modules\BrandsBanner\Images
 */
class BannerImage extends ImageContainer
{

    /**
     * Field name in the form
     *
     * @return string
     */
    public static function getField(): string
    {
        return 'image';
    }

    /**
     * Folder name
     *
     * @return string
     */
    public static function getType(): string
    {
        return 'brands_banner';
    }

    /**
     * Configurations
     *
     * @return ImagesGroup
     * @throws \App\Exceptions\WrongParametersException
     */
    public function configurations(): ImagesGroup
    {
        $image = new ImagesGroup($this->getType());
        $image
            ->addTo('small')
            ->setWidth(150)
            ->setHeight(150)
            ->setCrop(false);
        $image
            ->addTo('big')
            ->setWidth(980)
            ->setCrop(false);
        return $image;
    }

}
