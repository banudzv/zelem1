@php
/** @var \CustomForm\Builder\Form $form */
$form->buttons->showCloseButton(route('admin.brands_banner.index'));
$url = route('admin.brands_banner.update', Route::current()->parameters);
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['method' => 'PUT', 'files' => true, 'url' => $url]) !!}
        {!! $form->render() !!}
    {!! Form::close() !!}
@stop
