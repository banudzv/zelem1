@php
/** @var \App\Modules\BrandsBanner\Models\Brand[] $banners */
/** @var string $filter*/
$className = \App\Modules\BrandsBanner\Models\Brand::class;

@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter !!}
        <div class="box">
            <div class="box-body table-responsive no-padding mailbox-messages">
                <table class="table table-hover">
                    <tr>
                        <th>
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle">
                                <i class="fa fa-square-o"></i>
                            </button>
                        </th>
                        <th>{{ __('validation.attributes.name') }}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($banners AS $banner)
                        <tr data-id="{{$banner->id}}">
                            <td width="30"><input type="checkbox"></td>
                            <td>{{ $banner->current->name }}</td>
                            <td>{!! Widget::active($banner, 'admin.brands_banner.active') !!}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.brands_banner.edit', $banner->id) !!}
                                {!! \App\Components\Buttons::delete('admin.brands_banner.destroy', $banner->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $banners->appends(request()->except('page'))->links() }}</div>
        <span id="parameters-name" data-name="{{ $className }}"></span>
    </div>
@stop
