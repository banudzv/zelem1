<?php

use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:brands_banner'])->group(function () {
    Route::put('brands_banner/{banner}/active', ['uses' => 'IndexController@active', 'as' => 'admin.brands_banner.active']);
    Route::get(
        'brands_banner/{banner}/destroy',
        ['uses' => 'IndexController@destroy', 'as' => 'admin.brands_banner.destroy']
    );
    Route::post(
        'brands_banner/delete',
        ['uses' => 'IndexController@delete', 'as' => 'admin.brands_banner.delete']
    );
    Route::post(
        'brands_banner/activeMulti',
        ['uses' => 'IndexController@activeMulti', 'as' => 'admin.brands_banner.active-multi']
    );
    Route::resource('brands_banner', 'IndexController')
        ->except('show', 'destroy')
        ->names('admin.brands_banner');
});

// Routes for unauthenticated administrators


