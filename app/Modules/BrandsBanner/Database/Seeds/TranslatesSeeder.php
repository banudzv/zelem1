<?php

namespace App\Modules\BrandsBanner\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'brands_banner';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'Банер брендов',
                ],
                [
                    'name' => 'general.permission-name',
                    'ru' => 'Банер брендов',
                ],
                [
                    'name' => 'general.settings-name',
                    'ru' => 'Банер брендов',
                ],
                [
                    'name' => 'general.validation',
                    'ru' => 'Выберите баннер из списка',
                ],
                [
                    'name' => 'general.brand_id',
                    'ru' => 'Производитель',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Список банеров',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование баннера',
                ],
                [
                    'name' => 'seo.create',
                    'ru' => 'Добавление нового баннера',
                ],
                [
                    'name' => 'settings.attributes.per-page',
                    'ru' => 'Количество баннеров на странице в админ панели',
                ],
                [
                    'name' => 'seo.count',
                    'ru' => 'Количество баннеров - :count',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.more',
                    'ru' => 'Подробнее'
                ]
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
