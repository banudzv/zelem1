<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsBannerTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands_banner', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(true);
            $table->integer('brand_id')->unsigned();
            $table->timestamps();


            $table->foreign('brand_id')->references('id')->on('brands')
                ->index('brands_banner_brand_id_brands_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::create('brands_banner_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('content')->nullable();
            $table->string('url')->nullable();
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);

            $table->foreign('language')->references('slug')->on('languages')
                ->index('brands_banner_translates_language_languages_slug')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('brands_banner')
                ->index('brands_banner_translates_row_id_brands_banner_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (app()->environment() !== 'production') {

            Schema::table('brands_banner_translates', function (Blueprint $table) {
                $table->dropForeign('brands_banner_translates_language_languages_slug');
                $table->dropForeign('brands_banner_translates_row_id_brands_banner_id');
            });
            Schema::dropIfExists('brands_banner_translates');

            Schema::table('brands_banner', function (Blueprint $table) {
                $table->dropForeign('brands_banner_brand_id_brands_id');
            });
            Schema::dropIfExists('brands_banner');

        }
    }
}
