<?php

namespace App\Modules\BrandsBanner\Controllers\Admin;

use App\Modules\BrandsBanner\Filters\BannerFilter;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\BrandsBanner\Forms\BannerForm;
use App\Modules\BrandsBanner\Models\Banner;
use App\Modules\BrandsBanner\Models\BannerTranslates;
use App\Modules\BrandsBanner\Requests\BannerRequest;
use Seo;
use App\Core\AdminController;

/**
 * Class IndexController
 *
 * @package App\Modules\BrandsBanner\Controllers\Admin
 */
class IndexController extends AdminController
{

    public function __construct()
    {
        Seo::breadcrumbs()->add('brands_banner::seo.index', RouteObjectValue::make('admin.brands_banner.index'));
    }

    /**
     * Brands list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Components\AdminRoleScopes\WrongParametersException
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Exception
     */
    public function index()
    {
        $this->addCreateButton('admin.brands_banner.create');
        $this->addActiveButton('admin.brands_banner.active-multi');
        $this->addDeleteButton('admin.brands_banner.delete');
        Seo::meta()->setH1('brands_banner::seo.index');
        Seo::meta()->setCountOfEntity(trans('brands_banner::seo.count',['count' => Banner::count(['id'])]));
        $banners = Banner::getList();
        return view('brands_banner::admin.index', [
            'banners' => $banners,
            'filter' => BannerFilter::showFilter(),
        ]);
    }

    /**
     * Create new element page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Exception
     */
    public function create()
    {
        Seo::breadcrumbs()->add('brands_banner::seo.create');
        Seo::meta()->setH1('brands_banner::seo.create');
        $this->initValidation((new BannerRequest())->rules());
        return view('brands_banner::admin.create', [
            'form' => BannerForm::make(),
        ]);
    }

    /**
     * Create page in database
     *
     * @param  BannerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(BannerRequest $request)
    {
        $banner = (new Banner);
        if ($message = $banner->createRow($request)) {
            return $this->afterFail($message);
        }
        return $this->afterStore(['id' => $banner->id]);
    }

    /**
     * Update element page
     *
     * @param  Banner $brandsBanner
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(Banner $brandsBanner)
    {
        Seo::breadcrumbs()->add($brandsBanner->current->name ?? 'brands_banner::seo.edit');
        Seo::meta()->setH1('brands_banner::seo.edit');
        $this->initValidation((new BannerRequest)->rules());
        return view('brands_banner::admin.update', [
            'form' => BannerForm::make($brandsBanner),
        ]);
    }

    /**
     * Update page in database
     *
     * @param  BannerRequest $request
     * @param  Banner $brandsBanner
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(BannerRequest $request, Banner $brandsBanner)
    {
        $banners = [];
        $brandsBanner->data->each(function (BannerTranslates $translate) use (&$banners) {
            $banners[$translate->language] = $translate->slug;
        });

        // Update existed article
        if ($message = $brandsBanner->updateRow($request)) {
            return $this->afterFail($message);
        }

        // Do something
        return $this->afterUpdate();
    }

    /**
     * Totally delete page from database
     *
     * @param  Banner $banner
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(Banner $banner)
    {
        $bannerId = $banner->id;
        $banners = [];
        $banner->data->each(function (BannerTranslates $translate) use (&$banners) {
            $banners[$translate->language] = $translate->slug;
        });
        $banner->deleteRow();
        return $this->afterDestroy();
    }

}
