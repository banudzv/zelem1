<?php

namespace App\Modules\BrandsBanner\Filters;

use CustomForm\Builder\Form;
use CustomForm\Select;
use CustomForm\Input;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Class BannerFilter
 *
 * @package App\Core\Modules\BrandsBanner\Filters
 */
class BannerFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @return string
     * @throws \App\Exceptions\WrongParametersException
     */
    static function showFilter()
    {
        $form = Form::create();
        $form->fieldSetForFilter()->add(
            Select::create('active')
                ->add([
                    '0' => __('global.unpublished'),
                    '1' => __('global.published'),
                ])
                ->setValue(request('active'))
                ->addClassesToDiv('col-md-2')
                ->setPlaceholder(__('global.all'))
        );
        return $form->renderAsFilter();
    }

    /**
     * Filter by name
     *
     * @param  string $name
     * @return BannerFilter
     */
    public function name(string $name)
    {
        $name = Str::lower($name);
        return $this->related('current', function (Builder $query) use ($name) {
            return $query->whereRaw('LOWER(name) LIKE ?', ["%$name%"]);
        });
    }

    /**
     * Filter by active
     *
     * @param  string $active
     * @return BannerFilter
     */
    public function active(string $active)
    {
        return $this->where(function (Builder $query) use ($active) {
            return $query->whereActive((bool)$active);
        });
    }
}
