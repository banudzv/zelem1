<?php

namespace App\Modules\BrandsBanner\Forms;

use App\Components\Form\ObjectValues\ModelForSelect;
use App\Core\Interfaces\FormInterface;
use App\Modules\Brands\Models\Brand;
use App\Modules\BrandsBanner\Images\BannerImage;
use App\Modules\BrandsBanner\Models\Banner;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Image;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\Select;
use CustomForm\TinyMce;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BannerForm
 *
 * @package App\Core\Modules\BrandsBanner\Forms
 */
class BannerForm implements FormInterface
{

    /**
     * @param Model|Banner|null $banner
     * @return Form
     * @throws \App\Exceptions\WrongParametersException|\Exception
     */
    public static function make(?Model $banner = null): Form
    {
        $banner = $banner ?? new Banner;
        $form = Form::create();
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Toggle::create('active', $banner)->required(),
            Select::create('brand_id', $banner)
                ->model(ModelForSelect::make(
                    Brand::with('current')->get()
                )->setValueFieldName('current.name')),
            Image::create(BannerImage::getField(), $banner->image)
        );
        $form->fieldSetForLang(12)->add(
            Input::create('name', $banner)->required(),
            Input::create('url', $banner),
            TinyMce::create('content', $banner)
        );
        return $form;
    }

}
