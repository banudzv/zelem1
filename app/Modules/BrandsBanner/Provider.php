<?php

namespace App\Modules\BrandsBanner;

use App\Core\BaseProvider;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\BrandsBanner\Widgets\BrandBanner;
use Widget, CustomRoles, CustomMenu;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\BrandsBanner
 */
class Provider extends BaseProvider
{

    /**
    * Set custom presets
    */
    protected function presets()
    {
    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {

        // Register left menu block
        CustomMenu::get()->group()
            ->block('content', 'brands_banner::general.menu-block', 'fa fa-files-o')
            ->link('brands_banner::general.menu', RouteObjectValue::make('admin.brands_banner.index'))
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.brands_banner.edit'),
                RouteObjectValue::make('admin.brands_banner.create')
            );
        // Register role scopes
        CustomRoles::add('brands_banner', 'brands_banner::general.permission-name');
    }


    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot() {
        Widget::register(BrandBanner::class, 'brands_banner::show-banner');
    }


}
