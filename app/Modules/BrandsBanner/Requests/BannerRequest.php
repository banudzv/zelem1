<?php

namespace App\Modules\BrandsBanner\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Exceptions\WrongParametersException;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BrandRequest
 *
 * @package App\Modules\BrandsBanner\Requests
 */
class BannerRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws WrongParametersException
     */
    public function rules(): array
    {
        return $this->generateRules([
            'active' => ['required', 'bool'],
            'brand_id' => ['required'],
        ], [
            'name' => ['required']
        ]);
    }

}
