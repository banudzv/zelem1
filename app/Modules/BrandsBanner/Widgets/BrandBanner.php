<?php

namespace App\Modules\BrandsBanner\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\BrandsBanner\Models\Banner;

class BrandBanner implements AbstractWidget
{

    private  $bannerId;

    public function __construct($bannerId)
    {
        $this->bannerId = $bannerId;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $banner = Banner::find($this->bannerId);
        return view('brands_banner::widgets.show', [
            'banner' => $banner,
        ]);
    }

}
