<?php

namespace App\Modules\Categories\Controllers\Site;

use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\SiteController;
use App\Modules\Categories\Models\Category;
use Catalog;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Widget;

class CategoryController extends SiteController
{
    /**
     * @var SystemPage
     */
    static $categoriesMainPage;

    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        static::$categoriesMainPage = SystemPage::getByCurrent('slug', 'categories');
        abort_unless(static::$categoriesMainPage && static::$categoriesMainPage->exists, 404);
    }

    /**
     * Categories list page
     *
     * @return Factory|View
     */
    public function index()
    {
        $this->meta(static::$categoriesMainPage->current, static::$categoriesMainPage->current->content);
        $this->setOtherLanguagesLinks(static::$categoriesMainPage);
        Catalog::category()->addMainCategoriesPageBreadcrumb(static::$categoriesMainPage->current->name);
        $categories = Category::topLevel();
        $categories->load('activeChildren', 'activeChildren.current');
        $this->pageNumber();
        $this->canonical(route('site.categories'));

        return view(
            'categories::site.categories',
            [
                'categories' => $categories,
                'page' => static::$categoriesMainPage,
            ]
        );
    }

    public function show(string $slug, Request $request)
    {
        /** @var Category $category */
        $category = Category::getOneBySlug($slug);
        abort_unless(
            $category &&
            $category->exists &&
            $category->active,
            404
        );

        $this->setMeta($category, $request);

        if (config('db.products.show-categories-if-has', false)) {
            $category->loadMissing('activeChildren');
            if ($category->activeChildren->isNotEmpty()) {
                $category->loadMissing('activeChildren.current');
                return view(
                    'categories::site.categories',
                    [
                        'categories' => $category->activeChildren,
                        'page' => $category,
                    ]
                );
            }
        }
        Catalog::ecommerce()->setPage('category', 'productImpressions');

        $productsList = Widget::show('products::category-page', $category);

        return $productsList
            ?? view(
                'categories::site.no-products',
                [
                    'page' => $category,
                ]
            );
    }

    protected function setMeta(Category $category, Request $request): void
    {
        Catalog::category()->addMainCategoriesPageBreadcrumb(static::$categoriesMainPage->current->name);
        Catalog::category()->fillParentsBreadcrumbs($category);

        $this->meta($category->current, $category->current->seo_text);

        $this->metaTemplate(
            Category::SEO_TEMPLATE_ALIAS,
            [
                'name' => $category->current->name,
                'content' => $category->current->seo_text,
            ]
        );

        $this->setOtherLanguagesLinks($category);
//        $this->pageNumber();
        if ($request->query()) {
            $this->hideDescriptionKeywords();
        }
        $this->canonical(route('site.category', [$category->current->slug]));
    }

}
