<?php

namespace App\Modules\Categories\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Categories\Forms\CategoryForm;
use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\CategoryService as CategoryServiceModel;
use App\Modules\Categories\Requests\CategoryRequest;
use App\Modules\Categories\Services\CategoryService;
use Cache;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Seo;
use Throwable;

class CategoryController extends AdminController
{
    /**
     * @var CategoryService
     */
    protected $service;

    public function __construct(CategoryService $service)
    {
        Seo::breadcrumbs()->add(
            'categories::seo.index',
            RouteObjectValue::make('admin.categories.index')
        );

        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $this->addCreateButton('admin.categories.create');

        Seo::meta()->setH1('categories::seo.index');
        Seo::meta()->setCountOfEntity(__('categories::seo.count', ['count' => Category::count(['id'])]));

        $stats = $this->service->getProductStats();

        if (
            $request->query('name') === null
            && $request->query('active') === null
        ) {
            $categories = $this->service->getAll();

            $this->service->fillProductCounters($categories, $stats);

            $tree = $this->service->generateTree($categories);
            if ($tree->isNotEmpty()) {
                return view('categories::admin.index', ['categories' => $tree]);
            }

            return view('categories::admin.no-pages');
        }

        $categories = Category::getList();
        $this->service->fillProductCounters($categories, $stats);

        return view('categories::admin.query-index', ['categories' => $categories]);
    }

    /**
     * Create category page
     *
     * @return Factory|View
     * @throws WrongParametersException
     * @throws Throwable
     */
    public function create()
    {
        Seo::breadcrumbs()->add('categories::seo.create');
        Seo::meta()->setH1('categories::seo.create');
        $this->initValidation((new CategoryRequest())->rules());
        return view(
            'categories::admin.create',
            [
                'form' => CategoryForm::make(),
            ]
        );
    }

    /**
     * Store new category
     *
     * @param CategoryRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     * @throws Exception
     */
    public function store(CategoryRequest $request)
    {
        $category = (new Category());
        if ($message = $category->createRow($request)) {
            return $this->afterFail($message);
        }
        $this->syncWithServices($request, $category->id);

        $this->cacheClear();

        return $this->afterStore(['id' => $category->id]);
    }

    /**
     * Syncs categories with services.
     *
     * @param CategoryRequest $request
     * @param int $categoryId
     * @throws Exception
     */
    protected function syncWithServices(CategoryRequest $request, int $categoryId): void
    {
        $serviceIds = $request->input('services', []);
        CategoryServiceModel::whereNotIn('service_id', $serviceIds)
            ->whereCategoryId($categoryId)->delete();
        $storedServiceIds = CategoryServiceModel::whereCategoryId($categoryId)->pluck('service_id')->toArray();
        foreach (array_diff($serviceIds, $storedServiceIds) as $serviceId) {
            CategoryServiceModel::create(['category_id' => $categoryId, 'service_id' => $serviceId]);
        }
    }

    /**
     * Update category page
     *
     * @param Category $category
     * @return Factory|View
     * @throws WrongParametersException
     * @throws Throwable
     */
    public function edit(Category $category)
    {
        Seo::breadcrumbs()->add($category->current->name ?? 'categories::seo.edit');
        Seo::meta()->setH1('categories::seo.edit');
        $this->initValidation((new CategoryRequest())->rules());
        return view(
            'categories::admin.update',
            [
                'object' => $category,
                'form' => CategoryForm::make($category),
            ]
        );
    }

    /**
     * Update information for current category
     *
     * @param CategoryRequest $request
     * @param Category $category
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     * @throws Exception
     */
    public function update(CategoryRequest $request, Category $category)
    {
        if ($message = $category->updateRow($request)) {
            return $this->afterFail($message);
        }
        $this->syncWithServices($request, $category->id);
        $category->uploadSvgImage($request->file('svg-image'));
        $category->updateRelations($request);

        $this->cacheClear();

        return $this->afterUpdate();
    }

    /**
     * Delete preview in category
     *
     * @param Category $category
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function deleteImage(Category $category)
    {
        $category->deleteImagesIfExist();
        return $this->afterDeletingImage();
    }

    public function deleteImageSvg(Category $category)
    {
        $category->svg_image = null;
        $category->save();
        return response()->json(
            [
                'success' => true
            ]
        );
    }

    /**
     * Delete all data for category including images
     *
     * @param Category $category
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(Category $category)
    {
        $category->moveKidsToTheParentCategory();
        $category->deleteRow();

        $this->cacheClear();

        return $this->afterDestroy();
    }

    protected function cacheClear(): void
    {
        Cache::clear();
    }
}
