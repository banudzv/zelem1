<?php

namespace App\Modules\Categories\Controllers\Api;

use App\Core\ApiController;
use App\Modules\Categories\Models\Category;
use App\Modules\Categories\Resources\CategoryFullResource;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CategoriesController
 *
 * @package App\Modules\Categories\Controllers\Api
 */
class CategoriesController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/categories/all",
     *     tags={"Categories"},
     *     summary="Returns categories tree with all data",
     *     operationId="getCategoryFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/CategoryFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return CategoryFullResource::collection(Category::with(['data', 'image', 'children', 'children.data'])
            ->where(function(Builder $query) {
                $query
                    ->where('parent_id', 0)
                    ->orWhereNull('parent_id');
            })
            ->oldest('position')
            ->latest('id')
            ->get());
    }
}
