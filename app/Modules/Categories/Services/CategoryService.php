<?php

namespace App\Modules\Categories\Services;

use App\Modules\Categories\Models\Category;
use App\Modules\Categories\Repositories\CategoryRepository;
use App\Modules\Products\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Log;
use Throwable;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getProductStats()
    {
        try {
            return Product::searchBuilder()
                ->select([false])
                ->aggregation('categories', 'category_ids')
                ->resolve()
                ->getAggregationByName('categories');
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());
        }

        return [];
    }

    /**
     * @param Collection|Category[] $categories
     * @param array $categoryStats
     * @return int
     */
    public function fillProductCounters(Collection $categories, array $categoryStats): int
    {
        $result = 0;

        foreach ($categories as $category) {
            $category->products_count = $categoryStats[$category->id] ?? 0;
            $result += $category->products_count;
        }

        return $result;
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function generateTree(Collection $categories)
    {
        $tree = Collection::make();

        foreach ($categories as $category) {
            $elements = $tree->get((int)$category->parent_id, []);
            $elements[] = $category;
            $tree->put((int)$category->parent_id, $elements);
        }

        return $tree;
    }
}