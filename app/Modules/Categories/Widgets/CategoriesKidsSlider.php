<?php

namespace App\Modules\Categories\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Categories\Models\Category;

/**
 * Class CategoriesKidsSlider
 *
 * @package App\Modules\Categories\Widgets
 */
class CategoriesKidsSlider implements AbstractWidget
{
    /**
     * @var int
     */
    protected $categoryId;

    /**
     * CategoriesKidsSlider constructor.
     *
     * @param int $categoryId
     */
    public function __construct(int $categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $categories = Category::getKidsFor($this->categoryId);
        if (!$categories || $categories->isEmpty()) {
            return null;
        }

        return view('categories::site.widgets.kids-slider.sub-categories-slider', [
            'categories' => $categories
        ]);
    }
}
