<?php

namespace App\Modules\Categories\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Exceptions\WrongParametersException;
use App\Modules\Categories\Models\Category;

/**
 * Class CategoriesFooter
 *
 * @package App\Modules\Categories\Widgets
 */
class CategoriesFooter implements AbstractWidget
{
    protected $place;
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function __construct(string $place = 'footer')
    {
        if ($place !== 'footer' && $place !== 'mobile-footer') {
            throw new WrongParametersException('$place could be only `footer` of `mobile-footer`');
        }
        $this->place = $place;
    }

    public function render()
    {
        $categories = Category::getListForFooter();
        return view('categories::site.widgets.'.$this->place.'-menu.index', [
            'categories' => $categories,
        ]);
    }

}
