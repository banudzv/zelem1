<?php

namespace App\Modules\Categories\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Categories\Models\Category;

/**
 * Class CategoriesFilterKids
 *
 * @package App\Modules\Categories\Widgets
 */
class CategoriesFilterKids implements AbstractWidget
{
    /**
     * @var int
     */
    protected $categoryId;

    /**
     * CategoriesFilterKids constructor.
     *
     * @param int $categoryId
     */
    public function __construct(int $categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $categories = Category::getKidsOrSameCategoriesIfEmptyById($this->categoryId);
        if (!$categories || $categories->isEmpty()) {
            return null;
        }
        if ($categories->count() === 1 && $categories->first()->id === $this->categoryId) {
            return null;
        }

        return view('categories::site.widgets.kids-list.nav-links', [
            'categories' => $categories,
            'currentCategoryId' => $this->categoryId,
            'currentCategorySlug' => request()->slug,
        ]);
    }
}
