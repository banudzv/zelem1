@php
    /** @var \App\Modules\Categories\Models\Category[] $categories */
    $className = \App\Modules\Categories\Models\Category::class;
    $getParameters = request()->query();
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! App\Modules\Categories\Filters\CategoryFilter::showFilter() !!}
        <div class="dd pageList" id="myNest" data-depth="1">
            <ol class="dd-list">
                @include('categories::admin.query-items', ['categories' => $categories])
            </ol>
        </div>
        <span id="parameters" data-url="{{ route('admin.categories.sortable', ['class' => $className,'query' => $getParameters]) }}"></span>
        <input type="hidden" id="myNestJson">
    </div>
@stop