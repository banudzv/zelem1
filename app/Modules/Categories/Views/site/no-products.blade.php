@php

@endphp

@extends('site._layouts.catalog')

@section('layout-body')
    <div class="container container--white container--small">
        <div class="_ptb-sm _def-pt-none _def-pb-def">
            {!! Widget::show('categories::kids-slider', $page->id) !!}
        </div>
    </div>

    <div class="section _mb-lg">
        <div class="container container--inner">
{{--            <div class="box">--}}
{{--                <h1 class="title title--size-h1">--}}
{{--                    {{ $page->current->name }}--}}
{{--                </h1>--}}
{{--            </div>--}}
            <div class="grid grid--1" id="filter">
                <div class="gcell gcell--def-3 gcell--lg-1-of-5">
                    <div class="box _def-show _mb-none">
                        <div>
                            {!! Widget::show('categories::in-filter') !!}
{{--                          {!! Widget::show('categories::kids', $page->id) !!}--}}
{{--                        {!! Widget::show('products::filter', $page-.action-bar-slide__holde>id) !!}--}}
                        </div>
                    </div>
                </div>
                <div class="gcell gcell--def-9 gcell--lg-4-of-5">
                    <div class="_def-ml-xxs box--full-height _mb-none">
                        <div class="box box--full-height _mb-none">
                            <div class="_text-center">
                                <div class="wysiwyg">
                                    <p>@lang('messages.no-content')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="separator _color-gray3 _mtb-xl">
        </div>
    </div>

    {!! Widget::show('products::new') !!}
@endsection
