@php
/** @var \App\Modules\Categories\Models\Category[] $categories */
@endphp

<div class="grid grid--2 grid--ms-3 grid--def-4 grid--lg-5 catalog__group-wrapper">
    @foreach($categories as $category)
        <div class="gcell">
            <div class="catalog-group js-init" data-categories-wrapper>
                <div class="catalog-group__image">
                    <a class="_def-show" href="{{ $category->site_link }}" title="{{ $category->current->name }}">
                        {!! $category->imageTag('small', ['width' => 260, 'height' => 260], false, site_media('static/images/placeholders/no-category.png')) !!}
                    </a>
                    <a class="_def-hide" href="{{ count($category -> activeChildren) > 0 ? 'javascript:void(0)' : $category->site_link }}" data-categories-popup-trigger title="{{ $category->current->name }}">
                        {!! $category->imageTag('small', ['width' => 260, 'height' => 260], false, site_media('static/images/placeholders/no-category.png')) !!}
                    </a>
                </div>
                <div class="catalog-group__title">
                    <div class="title title--size-h3">
                        <a class="_def-show link" href="{{ $category->site_link }}">
                            {{ $category->current->name }}
                        </a>
                        <a class="_def-hide link" href="{{ count($category -> activeChildren) > 0 ? 'javascript:void(0)' : $category->site_link }}" data-categories-popup-trigger>
                            {{ $category->current->name }}
                        </a>
                    </div>
                </div>
                @if (count($category -> activeChildren) > 0)
                <div class="catalog-group__categories-popup js-init _flex _flex-column _justify-start" data-categories-popup>
                    <div class="title title--size-h3 title--normal _flex _items-center" data-categories-popup-trigger> {!! SiteHelpers\SvgSpritemap::get('icon-arrow-left-thin', ['width' => 20, 'height' => 20, 'class' => '_mr-md']) !!} <span>{{ $category->current->name }}</span></div>
                    <div class="catalog-group__categories-popup-wrap _flex-grow">
                        @foreach($category->activeChildren as $index => $childCategory)
                        <div class="catalog-group__category-item">
                            <a href="{{ $childCategory->site_link }}"
                               class="catalog-group__category-link"
                               title="{{ $childCategory->current->name }}">
                                {{ $childCategory->current->name }}
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
                <div class="catalog-group__inner _def-show js-init" data-section-accordion>
                    @foreach($category->activeChildren as $index => $childCategory)
                        <div class="catalog-group__item" {{  $index > 1 ? 'style=display:none; data-section-hidden' : '' }}>
                            <a href="{{ $childCategory->site_link }}"
                                    class="catalog-group__link"
                                    title="{{ $childCategory->current->name }}">
                                {{ $childCategory->current->name }}
                            </a>
                        </div>
                        @if($loop -> last)
                            <div class="catalog-group__item" data-toggle-accordion>
                                <a href="javascript:void(0)" class="catalog-group__link catalog-group__link--all" data-when-closed>
                                    @lang('categories::site.show-all')
                                </a>
                                <a href="javascript:void(0)" style="display: none;" class="catalog-group__link catalog-group__link--all" data-when-opened>
                                    @lang('categories::site.hide')
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>
