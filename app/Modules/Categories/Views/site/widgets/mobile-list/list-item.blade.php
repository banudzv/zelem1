@php
/** @var \App\Modules\Categories\Models\Category $category */
@endphp

    <a href="{{ $category->site_link }}">
        {{-- @if($category->symbol) --}}
            {!! \SiteHelpers\SvgSpritemap::get('icon-category-' . $loop->iteration, [
                'class' => 'mm-custom-icon'
            ]) !!}
        {{-- @endif --}}
        {{ $category->current->name }}
    </a>
{{-- @php
    $kids = $category->getKids();
    @endphp
@if($kids && $kids->isNotEmpty())
    @foreach($kids as $subCategory)
        @include('categories::site.widgets.mobile-list.list-item', ['category' => $subCategory])
    @endforeach
@endif --}}
