<a href="{{ route('site.categories') }}" class="mm-custom-link _flex _items-center">
    {!! SiteHelpers\SvgSpritemap::get('catalogue', ['class' => 'mm-menu-icon']) !!}  <span>@lang('categories::site.catalog')</span>
</a>
{{-- @foreach(\App\Modules\Categories\Models\Category::getKidsFor(0) as $category)
    @include('categories::site.widgets.mobile-list.list-item', ['category' => $category])
@endforeach --}}
