@php
    /** @var \App\Modules\Categories\Models\Category[]|\Illuminate\Database\Eloquent\Collection $categories */
@endphp

@if($categories->isNotEmpty())
    <div data-slick-slider="{{ json_encode([ 'type' => 'SlickSubCategories', 'user-type-options' => [] ]) }}"
         class="subcategories js-init">
        <div class="subcategories__inner" data-slick-slider>
            @foreach($categories as $category)
                @include('categories::site.widgets.kids-slider.sub-category-item', ['category' => $category])
            @endforeach
        </div>
    </div>
@endif
