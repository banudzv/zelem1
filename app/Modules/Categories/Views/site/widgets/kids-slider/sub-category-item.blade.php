<div class="subcategories__item-wrapper">
    <a href="{{ $category->site_link }}" class="subcategories__item">
        <span class="subcategories__item-image">
            {!! $category->imageTag('small', ['width' => 320, 'height' => 240], false, site_media('static/images/placeholders/no-image-320x240.jpg')) !!}
        </span>
        <span class="subcategories__item-title">
            {{ $category->current->name }}
        </span>
    </a>
</div>
