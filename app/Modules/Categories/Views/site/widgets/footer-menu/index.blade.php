<div class="gcell _pl-def _lg-pl-lg">
    <div class="title title--size-normal _color-dark-blue">@lang('categories::site.catalog')</div>
    <div class="_mtb-def">
        @include('categories::site.widgets.footer-menu.nav-links.nav-links', [
            'categories' => $categories,
            // 'list_mod_classes' => '_def-columns-md-2'
        ])
    </div>
</div>
