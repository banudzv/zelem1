@php
/** @var \App\Modules\Categories\Models\Category[] $categories */
/** @var bool $expanded */
@endphp
<div class="action-bar-catalog{{ $expanded ? ' action-bar-catalog--expanded' : '' }}"
    data-action-bar-catalog="{{ $expanded ? 'expanded' : '' }}">
    <div class="action-bar-catalog-head">
        <div class="action-bar-catalog-opener" data-action-bar-opener>
            <div class="action-bar-catalog-opener__icon">
                @include('site._widgets.elements.hamburger.hamburger')
            </div>
            <div class="action-bar-catalog-opener__text">@lang('categories::site.catalog')</div>
        </div>
        <a href="{{ route('site.categories') }}" class="action-bar-catalog-link">
            @lang('categories::site.all-categories')
        </a>
    </div>
    <div class="action-bar-catalog-body" data-action-bar-body>
        <div class="action-bar-catalog-body__list js-init" data-perfect-scrollbar>
            <div class="action-bar-catalog-list js-init" data-submenu>
                @foreach($categories as $category)
                {{-- {{dd($category->current->slug)}} --}}
                    <div class="action-bar-catalog-list__item{{ $category->has_active_children ? ' js-submenu-item' : null }}">
                        <a class="action-bar-catalog-list__link" href="{{ $category->site_link }}">
                            {{-- @if($category->symbol)
                                {!! SiteHelpers\SvgSpritemap::get($category->symbol) !!}
                            @endif --}}
                            @if($category->svg_image)
                            <div class="action-bar-catalog-list__icon">
                                {!! $category->svg_image !!}
                            </div>
                            @endif
                            <span class="action-bar-catalog-list__text">{{ $category->current->name }}</span>
                            <div class="action-bar-catalog-list__arrow">
                                {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right') !!}
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
