<div class="action-bar-inner-menu">
    @foreach(\App\Modules\Categories\Models\Category::getKidsFor(0) as $category)
        @php
            $kids = $category->getKids()
        @endphp
        @if(!$kids || $kids->isEmpty())
            @continue
        @endif
        @php
        /** @var \App\Core\Modules\Images\Models\Image $image */
        $image = $category->image;
        @endphp
        <div class="action-bar-inner-menu__item" id="inner-menu__item-{{ $loop->index }}">
            @if ($image && $image->exists && $image->isImageExists())
                <div class="action-bar-inner-menu__image _flex-order-1">
                    {!! $image->imageTag('original') !!}
                </div>
            @endif
            <div class="action-bar-inner-menu__column _ptb-md">
                <div class="action-bar-inner-menu__scroll js-init _posr" data-perfect-scrollbar>
                    <div class="action-bar-inner-menu__holder">
                        <div class="action-bar-submenu">
                            @foreach($kids as $child)
                                <div class="action-bar-submenu__item">
                                    <a class="action-bar-submenu__link" href="{{ $child->site_link }}">
                                        {{ $child->current->name }}
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
