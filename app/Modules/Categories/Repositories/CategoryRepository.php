<?php

namespace App\Modules\Categories\Repositories;

use App\Modules\Categories\Models\Category;
use App\Traits\CachableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * @see CategoryRepository::all()
 * @method Category[]|Collection allCached()
 */
class CategoryRepository
{
    use CachableTrait;

    public function getAll()
    {
        static $all;

        if (is_null($all)) {
            $all = $this->all();
        }

        return $all;
    }

    public function all(): Collection
    {
        return $this->query()
            ->oldest('position')
            ->with(['current'])
            ->get();
    }

    /**
     * @return Builder|Category
     */
    public function query(): Builder
    {
        return Category::query();
    }
}