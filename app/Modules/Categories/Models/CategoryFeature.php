<?php

namespace App\Modules\Categories\Models;

use App\Modules\Features\Models\Feature;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CategoryFeature
 *
 * @package App\Modules\Categories\Models
 * @property int $id
 * @property int $category_id
 * @property int $feature_id
 * @property-read Category $category
 * @property-read Feature $feature
 * @method static Builder|CategoryFeature newModelQuery()
 * @method static Builder|CategoryFeature newQuery()
 * @method static Builder|CategoryFeature query()
 * @method static Builder|CategoryFeature whereCreatedAt($value)
 * @method static Builder|CategoryFeature whereId($value)
 * @method static Builder|CategoryFeature whereUpdatedAt($value)
 * @method static Builder|CategoryFeature whereCategoryId($value)
 * @method static Builder|CategoryFeature whereFeatureId($value)
 * @mixin \Eloquent
 */
class CategoryFeature extends Model
{
    public $timestamps = false;

    protected $table = 'categories_features';

    protected $fillable = ['category_id', 'feature_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function feature()
    {
        return $this->hasOne(Feature::class, 'id', 'feature_id');
    }
}
