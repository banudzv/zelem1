<?php

namespace App\Modules\Categories\Models;

use App\Core\Modules\Languages\Models\Language;
use App\Traits\ModelTranslates;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\Categories\Models\CategoryTranslates
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $row_id
 * @property string $language
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $search_keywords
 * @property string|null $description
 * @property string|null $seo_text
 * @property-read Language $lang
 * @property-read Category $row
 * @method static Builder|CategoryTranslates newModelQuery()
 * @method static Builder|CategoryTranslates newQuery()
 * @method static Builder|CategoryTranslates query()
 * @method static Builder|CategoryTranslates whereDescription($value)
 * @method static Builder|CategoryTranslates whereH1($value)
 * @method static Builder|CategoryTranslates whereId($value)
 * @method static Builder|CategoryTranslates whereKeywords($value)
 * @method static Builder|CategoryTranslates whereLanguage($value)
 * @method static Builder|CategoryTranslates whereName($value)
 * @method static Builder|CategoryTranslates whereRowId($value)
 * @method static Builder|CategoryTranslates whereSeoText($value)
 * @method static Builder|CategoryTranslates whereSlug($value)
 * @method static Builder|CategoryTranslates whereTitle($value)
 * @mixin Eloquent
 */
class CategoryTranslates extends Model
{
    use ModelTranslates;

    public $timestamps = false;

    protected $table = 'categories_translates';

    protected $fillable = [
        'name',
        'slug',
        'h1',
        'title',
        'keywords',
        'description',
        'seo_text',
        'row_id',
        'language',
        'search_keywords',
    ];
}
