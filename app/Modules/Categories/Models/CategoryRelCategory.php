<?php

namespace App\Modules\Categories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CategoryRelCategory
 *
 * @package App\Modules\Categories\Models
 * @property int $id
 * @property int $category_id
 * @property int $other_id
 * @property-read Category $category
 * @property-read Category $other
 * @method static Builder|CategoryRelCategory newModelQuery()
 * @method static Builder|CategoryRelCategory newQuery()
 * @method static Builder|CategoryRelCategory query()
 * @method static Builder|CategoryRelCategory whereCreatedAt($value)
 * @method static Builder|CategoryRelCategory whereId($value)
 * @method static Builder|CategoryRelCategory whereUpdatedAt($value)
 * @method static Builder|CategoryRelCategory whereCategoryId($value)
 * @method static Builder|CategoryRelCategory whereFeatureId($value)
 * @mixin \Eloquent
 */
class CategoryRelCategory extends Model
{
    public $timestamps = false;

    protected $table = 'categories_relation_category';

    protected $fillable = ['category_id', 'other_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function other()
    {
        return $this->hasOne(Category::class, 'id', 'other_id');
    }
}
