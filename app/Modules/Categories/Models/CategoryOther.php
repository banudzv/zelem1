<?php

namespace App\Modules\Categories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CategoryOther
 *
 * @package App\Modules\Categories\Models
 * @property int $id
 * @property int $category_id
 * @property int $other_id
 * @property-read Category $category
 * @property-read Category $other
 * @method static Builder|CategoryOther newModelQuery()
 * @method static Builder|CategoryOther newQuery()
 * @method static Builder|CategoryOther query()
 * @method static Builder|CategoryOther whereCreatedAt($value)
 * @method static Builder|CategoryOther whereId($value)
 * @method static Builder|CategoryOther whereUpdatedAt($value)
 * @method static Builder|CategoryOther whereCategoryId($value)
 * @method static Builder|CategoryOther whereFeatureId($value)
 * @mixin \Eloquent
 */
class CategoryOther extends Model
{
    public $timestamps = false;

    protected $table = 'categories_other';

    protected $fillable = ['category_id', 'other_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function other()
    {
        return $this->hasOne(Category::class, 'id', 'other_id');
    }
}
