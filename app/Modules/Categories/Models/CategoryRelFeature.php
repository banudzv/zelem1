<?php

namespace App\Modules\Categories\Models;

use App\Modules\Features\Models\Feature;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CategoryRelFeature
 *
 * @package App\Modules\Categories\Models
 * @property int $id
 * @property int $category_id
 * @property int $feature_id
 * @property-read Category $category
 * @property-read Feature $feature
 * @method static Builder|CategoryRelFeature newModelQuery()
 * @method static Builder|CategoryRelFeature newQuery()
 * @method static Builder|CategoryRelFeature query()
 * @method static Builder|CategoryRelFeature whereCreatedAt($value)
 * @method static Builder|CategoryRelFeature whereId($value)
 * @method static Builder|CategoryRelFeature whereUpdatedAt($value)
 * @method static Builder|CategoryRelFeature whereCategoryId($value)
 * @method static Builder|CategoryRelFeature whereFeatureId($value)
 * @mixin \Eloquent
 */
class CategoryRelFeature extends Model
{
    public $timestamps = false;

    protected $table = 'categories_relation_features';

    protected $fillable = ['category_id', 'feature_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function feature()
    {
        return $this->hasOne(Feature::class, 'id', 'feature_id');
    }
}
