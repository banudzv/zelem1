<?php

namespace App\Modules\Categories\Models;

use App\Core\Modules\Images\Models\Image;
use App\Modules\Categories\Filters\CategoryFilter;
use App\Modules\Categories\Images\CategoryImage;
use App\Modules\Features\Models\Feature;
use App\Modules\Features\Models\FeatureTranslates;
use App\Modules\Products\Models\CategoryService;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupFeatureValue;
use App\Modules\Products\Models\Service;
use App\Traits\ActiveScopeTrait;
use App\Traits\CheckRelation;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use Cache;
use Eloquent;
use EloquentFilter\Filterable;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Lang;
use Schema;
use SiteHelpers\SvgSpritemap;

/**
 * App\Modules\Categories\Models\Category
 *
 * @property int $id
 * @property bool $active
 * @property int|null $parent_id
 * @property int $position
 * @property int $relevance
 * @property bool $show_product_features_in_filter
 * @property bool $show_engine_features_in_filter
 *
 * @see \App\Modules\Categories\Services\CategoryService::fillProductCounters()
 * @property int|null $products_count
 *
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read EloquentCollection|Category[] $activeChildren
 * @property-read EloquentCollection|Image[] $allImages
 * @property-read EloquentCollection|Category[] $children
 * @property-read CategoryTranslates $current
 * @property-read EloquentCollection|CategoryTranslates[] $data
 * @property-read bool $has_active_children
 * @property-read bool $has_children
 * @property-read string $link_in_admin_panel
 *
 * @see Category::getSiteLinkAttribute()
 * @property-read string $site_link
 * @property-read Image $image
 * @property-read EloquentCollection|Image[] $images
 * @property-read Category $parent
 * @property-read EloquentCollection|Product[] $products
 * @method static Builder|Category active($active = true)
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static Builder|Category query()
 * @method static Builder|Category whereActive($value)
 * @method static Builder|Category whereCreatedAt($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereParentId($value)
 * @method static Builder|Category wherePosition($value)
 * @method static Builder|Category whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int|null $position_in_footer
 * @method static Builder|Category wherePositionInFooter($value)
 * @property-read int|null $active_children_count
 * @property-read int|null $all_images_count
 * @property-read int|null $children_count
 * @property-read int|null $data_count
 * @property-read int|null $images_count
 * @property-read EloquentCollection|Service[] $services
 * @property-read array|int[] $services_ids
 * @method static Builder|Category filter($input = array(), $filter = null)
 * @method static Builder|Category paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Category simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Category whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|Category whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|Category whereLike($column, $value, $boolean = 'and')
 *
 * @see Category::scopeWhereTopLevel()
 * @method static Builder|self whereTopLevel()
 * @property string $svg_image
 * @property-read Feature[]|Collection $features
 * @property-read Category[]|Collection $otherCategories
 * @property-read Feature[]|Collection $relCategories
 * @property-read Category[]|Collection $relFeatures
 */
class Category extends Model
{
    use ModelMain, Imageable, EloquentTentacle, ActiveScopeTrait, CheckRelation, Filterable;

    const SEO_TEMPLATE_ALIAS = 'categories';

    protected $casts = [
        'active' => 'boolean',
        'show_product_features_in_filter' => 'boolean',
        'show_engine_features_in_filter' => 'boolean'
    ];

    protected $fillable = [
        'active',
        'parent_id',
        'relevance',
        'position',
        'position_in_footer',
        'svg_image',
        'show_engine_features_in_filter',
        'show_product_features_in_filter'
    ];

    /**
     * @var null|Category[][]|EloquentCollection
     */
    public static $dumpParent;

    /**
     * @var null|Category[]|EloquentCollection
     */
    public static $dump;

    public static function dump(): void
    {
        if (static::$dump !== null) {
            return;
        }

        static::$dump = new EloquentCollection();
        static::$dumpParent = new EloquentCollection();

        if (Schema::hasTable('categories') === false) {
            return;
        }

        [static::$dump, static::$dumpParent] = self::getAllAndParent();
    }

    protected static function getAllAndParent(): array
    {
        return Cache::remember(
            'category_all_and_parent_' . Lang::getLocale(),
            1800,
            function () {
                $all = new EloquentCollection();
                $dumpParent = new EloquentCollection();

                Category::with(
                    'current',
                    'image',
                    'image.current',
                    'activeChildren',
                    'data'
                )
                    ->active()
                    ->oldest('position')
                    ->latest('id')
                    ->get()
                    ->each(
                        function (Category $category) use ($all, $dumpParent) {
                            $currentCollection = $dumpParent->get((int)$category->parent_id, new EloquentCollection());
                            $currentCollection->push($category);
                            $dumpParent->put((int)$category->parent_id, $currentCollection);
                            $all->put($category->id, $category);
                        }
                    );

                return [$all, $dumpParent];
            }
        );
    }

    /**
     * Returns linked to the category services ids.
     *
     * @return array|int[]
     */
    public function getServicesIdsAttribute(): array
    {
        return $this->services->pluck('id')->toArray();
    }

    /**
     * Relation to the services.
     *
     * @return HasManyThrough
     */
    public function services()
    {
        return $this->hasManyThrough(
            Service::class,
            CategoryService::class,
            'category_id',
            'id',
            'id',
            'service_id'
        );
    }

    public function getName(): ?string
    {
        return $this->name ?? $this->current->name;
    }

    public function getSlug(): ?string
    {
        return $this->slug ?? $this->current->slug;
    }

    /**
     * @return mixed
     */
    public function modelFilter()
    {
        return $this->provideFilter(CategoryFilter::class);
    }

    /**
     * @param int $parentCategoryId
     * @return array|int[]
     */
    public static function getCategoriesIds(int $parentCategoryId): array
    {
        $categories = [$parentCategoryId];
        foreach (Category::$dumpParent->get($parentCategoryId, []) as $category) {
            $categories = array_merge($categories, self::getCategoriesIds($category->id));
        }

        return $categories;
    }

    /**
     * @return array
     */
    public static function getDictionaryForSelects(): array
    {
        $categories = new EloquentCollection();
        Category::with('current')
            ->oldest('position')
            ->latest('id')
            ->get()
            ->each(function (Category $category) use ($categories) {
                $currentCollection = $categories->get((int)$category->parent_id, new EloquentCollection());
                $currentCollection->push($category);
                $categories->put((int)$category->parent_id, $currentCollection);
            });
        return static::makeDictionaryFrom($categories, 0);
    }

    /**
     * @param EloquentCollection $categories
     * @param int $parentId
     * @param array $dictionary
     * @param int $level
     * @return array
     */
    protected static function makeDictionaryFrom(EloquentCollection $categories, int $parentId, array &$dictionary = [], int $level = 0): array
    {
        $categories->get($parentId, new EloquentCollection)->each(function (Category $category) use (&$dictionary, $level, $categories) {
            $spaces = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level);
            $dictionary[$category->id] = $spaces . $category->current->name;
            static::makeDictionaryFrom($categories, $category->id, $dictionary, ++$level);
        });
        return $dictionary;
    }

    /**
     * @param int $categoryId
     * @return Category|null
     */
    public static function getOne(?int $categoryId): ?Category
    {
        static::dump();
        return static::$dump->get($categoryId);
    }

    /**
     * @return null|Category[]|EloquentCollection
     */
    public static function getAll()
    {
        static::dump();
        return static::$dump;
    }

    public static final function getListForFooter()
    {
        $categories = new EloquentCollection();
        static::getAll()->each(function (Category $category) use ($categories) {
            if ($category->position_in_footer !== null) {
                $categories->push($category);
            }
        });
        return $categories->sortBy('position_in_footer');
    }

    /**
     * @param string $slug
     * @return Category|null
     */
    public static function getOneBySlug(string $slug): ?Category
    {
        static::dump();
        foreach (static::$dump as $category) {
            if ($category->current->slug === $slug) {
                return $category;
            }
        }
        return null;
    }

    /**
     * @param int $categoryId
     * @return EloquentCollection|Category[]
     */
    public static function getKidsFor(?int $categoryId): EloquentCollection
    {
        static::dump();
        return static::$dumpParent->get((int)$categoryId, new EloquentCollection);
    }

    /**
     * @return EloquentCollection|Category[]
     */
    public function getKids(): EloquentCollection
    {
        static::dump();
        return static::$dumpParent->get($this->id, new EloquentCollection);
    }

    /**
     * @return EloquentCollection|Category[]
     */
    public function getSame(): EloquentCollection
    {
        static::dump();
        return static::getKidsFor((int)$this->parent_id)->filter(function (Category $category) {
            return $category->id !== $this->id;
        });
    }

    /**
     * @return EloquentCollection|Category[]
     */
    public function getKidsOrSameIfEmpty(): EloquentCollection
    {
        static::dump();
        $kids = static::getKidsFor((int)$this->id);
        return $kids->isNotEmpty() ? $kids : static::getKidsFor((int)$this->parent_id);
    }

    /**
     * @return Category|null
     */
    public function getParent(): ?Category
    {
        static::dump();
        return $this->getOne((int)$this->parent_id);
    }

    public function isShowEngineFeaturesInFilter(): bool
    {
        return (bool)$this->show_engine_features_in_filter;
    }

    public function isShowProductFeaturesInFilter(): bool
    {
        return (bool)$this->show_product_features_in_filter;
    }

    /**
     * Image config
     *
     * @return string|array
     */
    protected function imageClass()
    {
        return CategoryImage::class;
    }

    /**
     * Get categories list related to this group
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')
            ->with(['current', 'children', 'data'])
            ->oldest('position')
            ->latest('id');
    }

    /**
     * Get categories list related to this group
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeChildren()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')
            ->where('active', true)
            ->with(['current', 'activeChildren'])
            ->oldest('position')
            ->latest('id');
    }

    /**
     * Parent category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function products()
    {
        return $this->hasManyThrough(
            Product::class,
            ProductGroup::class,
            'category_id',
            'group_id',
            'id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function features()
    {
        return $this->hasManyThrough(
            Feature::class,
            CategoryFeature::class,
            'category_id',
            'id',
            'id',
            'feature_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function otherCategories()
    {
        return $this->hasManyThrough(
            Category::class,
            CategoryOther::class,
            'category_id',
            'id',
            'id',
            'other_id'
        );
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function relFeatures()
    {
        return $this->hasManyThrough(
            Feature::class,
            CategoryRelFeature::class,
            'category_id',
            'id',
            'id',
            'feature_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function relCategories()
    {
        return $this->hasManyThrough(
            Category::class,
            CategoryRelCategory::class,
            'category_id',
            'id',
            'id',
            'other_id'
        );
    }

    /**
     * Checks if parent can be linked as parent for chosen category
     *
     * @param int $categoryId
     * @param int|null $parentId
     * @return bool
     */
    public static function isAvailableToChoose(int $categoryId, ?int $parentId = null): bool
    {
        if ($parentId === null) {
            return true;
        }
        if ($categoryId === $parentId) {
            return false;
        }
        return in_array($parentId, Category::getAllChildrenIds($categoryId)) === false;
    }

    /**
     * Builds categories full tree
     *
     * @return Collection|Category[][]
     */
    public static function tree(): Collection
    {
        $categories = new Collection();
        Category::with('current')
            ->oldest('position')
            ->latest('id')
            ->get()
            ->each(function (Category $category) use ($categories) {
                $elements = $categories->get((int)$category->parent_id, []);
                $elements[] = $category;
                $categories->put((int)$category->parent_id, $elements);
            });
        return $categories;
    }

    /**
     * @return mixed
     */
    public static function getList()
    {
        return self::with('current')
            ->filter(request()->only('name', 'active'))
            ->oldest('position')
            ->get();
    }

    /**
     * Get Parent ids for category by slug
     *
     * @param string $slug
     * @return EloquentCollection
     */
    public static function getTreeFilter(string $slug): EloquentCollection
    {
        $categories = new EloquentCollection();
        static::dump();
        $category = null;
        foreach (static::$dump as $categoryObject) {
            if ($categoryObject->current->slug === $slug) {
                $category = $categoryObject;
                break;
            }
        }
        if (!$category) {
            return $categories;
        }
        $categories->push($category);
        while ($category = $category->getParent()) {
            $categories->push($category);
        }
        return $categories->reverse();
    }

    /**
     * Get children ids list for category by slug
     *
     * @param string $slug
     * @return EloquentCollection|null|Category[]
     */
    public static function getSameCategories(string $slug): ?EloquentCollection
    {
        return Category::getOneBySlug($slug)->getSame();
    }

    /**
     * @param string $slug
     * @return EloquentCollection|null
     */
    public static function getKidsOrSameCategoriesIfEmpty(string $slug): ?EloquentCollection
    {
        return Category::getOneBySlug($slug)->getKidsOrSameIfEmpty();
    }

    public static function getKidsOrSameCategoriesIfEmptyById(int $id): ?EloquentCollection
    {
        return Category::getOne($id)->getKidsOrSameIfEmpty();
    }

    /**
     * Get children ids list for category by id
     *
     * @param int|null $categoryId
     * @return array
     */
    public static function getAllChildrenIds(?int $categoryId = null): array
    {
        return Category::getChildrenIdsFromCollection(Category::tree(), $categoryId);
    }

    /**
     * Get children ids list for category from existing list by category id
     *
     * @param Collection|Category[][] $categories
     * @param int|null $categoryId
     * @return array
     */
    public static function getChildrenIdsFromCollection(Collection $categories, ?int $categoryId = null): array
    {
        $childrenIds = [];
        foreach ($categories->get((int)$categoryId, []) as $category) {
            $childrenIds[] = $category->id;
            $childrenIds = array_merge($childrenIds, Category::getChildrenIdsFromCollection($categories, $category->id));
        }
        return $childrenIds;
    }

    /**
     * Category link in admin panel
     *
     * @return string
     */
    public function getLinkInAdminPanelAttribute(): string
    {
        return route('admin.categories.edit', ['category' => $this->id]);
    }

    /**
     * Top level of categories
     *
     * @return Category[]|Builder[]|EloquentCollection
     */
    public static function topLevel()
    {
        return Category::with('current')
            ->active(true)
            ->where(function (Builder $query) {
                $query
                    ->where('parent_id', 0)
                    ->orWhereNull('parent_id');
            })
            ->oldest('position')
            ->latest('id')
            ->get();
    }

    /**
     * @param Builder|self $builder
     */
    public function scopeWhereTopLevel(Builder $builder)
    {
        $builder->where('parent_id', 0)
            ->orWhereNull('parent_id');
    }

    /**
     * Link on category inner page
     *
     * @return string
     */
    public function getSiteLinkAttribute(): string
    {
        return route('site.category', ['slug' => $this->current->slug]);
    }

    public function link(array $parameters = []): string
    {
        $basic = [
            'slug' => $this->getSlug(),
            'page' => false,
        ];

        return route('site.category', array_merge($basic, $parameters));
    }

    /**
     * @return bool
     */
    public function getHasChildrenAttribute(): bool
    {
        return $this->children && $this->children->isNotEmpty();
    }

    /**
     * @return bool
     */
    public function getHasActiveChildrenAttribute(): bool
    {
        return $this->getKids()->isNotEmpty();
    }


    /**
     * @param int $id
     * @return array|null
     */
    public function getPagesLinksByIdForImage(int $id)
    {
        $links = [];
        $item = Category::active()->find($id);
        if ($item) {
            if ($item->parent) {
                $links[] = url(route('site.category', ['slug' => $item->parent->current->slug], false), [], isSecure());
            } else {
                $links[] = url(route('site.categories', [], false), [], isSecure());
            }

        }
        return $links;
    }

    public function moveKidsToTheParentCategory(): void
    {
        Category::whereParentId($this->id)->update([
            'parent_id' => $this->parent_id,
        ]);
    }

    public static function getArrayForExportProm($category_ids = null): array
    {
        $categories = Category::with('current')
            ->oldest('id');
        if (!empty($category_ids)) {
            $categories = $categories->whereIn('id', $category_ids);
        }
        $categories = $categories->get();
        $export = [];
        foreach ($categories as $category) {
            $array['Номер_группы'] = $category->id;
            $array['Название_группы'] = $category->current->name;
            $array['Идентификатор_группы'] = '';
            $array['Номер_родителя'] = $category->parent_id;
            $array['Идентификатор_родителя'] = '';
            $export[] = $array;
        }
        return $export;
    }

    public function uploadSvgImage($file)
    {
        if (isset($file)) {
            $svgContent = file_get_contents($file);
            $svg = SvgSpritemap::cleanSvg($svgContent);
            $this->svg_image = $svg;
            $this->save();
        }
    }

    /**
     * @param $categoryId
     * @return Feature[]|Builder[]|EloquentCollection|\Illuminate\Database\Query\Builder[]|Collection
     */
    public static function getFeatures($categoryId)
    {
        $featureIds = ProductGroupFeatureValue::whereHas('group', function (Builder $builder) use ($categoryId) {
            $builder->where('category_id', $categoryId);
        })->pluck('feature_id')->toArray();

        return FeatureTranslates::whereLanguage(Lang::getLocale())->whereIn('row_id', array_unique($featureIds))
            ->pluck('name', 'row_id')->toArray();
    }

    public function updateRelations($request)
    {
        /** @var \Request $request*/
        CategoryOther::whereCategoryId($this->id)->delete();
        $otherCategoriesIds = $request->input('other_categories', []);
        foreach ($otherCategoriesIds as $id) {
            $other = new CategoryOther();
            $other->other_id = $id;
            $other->category_id = $this->id;
            $other->save();
        }

        $featuresIds = $request->input('features', []);

        CategoryFeature::whereCategoryId($this->id)->delete();
        foreach ($featuresIds as $id){
            $feature = new CategoryFeature();
            $feature->category_id = $this->id;
            $feature->feature_id = $id;
            $feature->save();
        }

        CategoryRelCategory::whereCategoryId($this->id)->delete();
        $otherCategoriesIds = $request->input('rel_categories', []);
        foreach ($otherCategoriesIds as $id) {
            $other = new CategoryRelCategory();
            $other->other_id = $id;
            $other->category_id = $this->id;
            $other->save();
        }

        CategoryRelFeature::whereCategoryId($this->id)->delete();
        $featuresIds = $request->input('rel_features', []);
        foreach ($featuresIds as $id){
            $feature = new CategoryRelFeature();
            $feature->category_id = $this->id;
            $feature->feature_id = $id;
            $feature->save();
        }
    }

    public function getRelevance(): int
    {
        return $this->relevance ?? 0;
    }

}
