<?php

namespace App\Modules\Categories\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'categories';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'Категории',
                ],
                [
                    'name' => 'general.permission-name',
                    'ru' => 'Категории',
                ],
                [
                    'name' => 'general.validation',
                    'ru' => 'Выберите категорию из списка!',
                ],
                [
                    'name' => 'general.attributes.show_product_features_in_filter',
                    'ru' => 'Показать фильтр по характеристикам товара',
                ],
                [
                    'name' => 'general.attributes.show_engine_features_in_filter',
                    'ru' => 'Показать фильтр по характеристикам двигателя',
                ],
                [
                    'name' => 'general.attributes.categories',
                    'ru' => 'Категории',
                ],
                [
                    'name' => 'general.attributes.main-category',
                    'ru' => 'Основная категория',
                ],
                [
                    'name' => 'general.attributes.relevance',
                    'ru' => 'Релевантность',
                ],
                [
                    'name' => 'general.messages.no-icon',
                    'ru' => 'Нет иконки',
                ],
                [
                    'name' => 'general.category-does-not-exist',
                    'ru' => 'Категории не существует',
                ],
                [
                    'name' => 'menu.categories',
                    'ru' => 'Категории товаров',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Категории товаров',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование категории',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Создать новую категорию товаров',
                ],
                [
                    'name' => 'seo.template-variables.category-name',
                    'ru' => 'Название категории',
                ],
                [
                    'name' => 'seo.template-variables.category-content',
                    'ru' => 'Полностью все описание категории',
                ],
                [
                    'name' => 'seo.template-variables.category-content-n',
                    'ru' => 'Часть описания категории (N символов)',
                ],
                [
                    'name' => 'settings.group-name',
                    'ru' => 'Каталог',
                ],
                [
                    'name' => 'settings.attributes.categories-per-page',
                    'ru' => 'Количество категорий на странице в админ панели',
                ],
                [
                    'name' => 'settings.attributes.products-per-page',
                    'ru' => 'Количество товаров на странице в админ панели',
                ],
                [
                    'name' => 'general.attributes.other-categories',
                    'ru' => 'Дополнимтельные категории, в которых будет отображаться товар',
                ],
                [
                    'name' => 'general.attributes.position_in_footer',
                    'ru' => 'Позиция для вывода ссылки на категорию в меню в футере',
                ],
                [
                    'name' => 'general.attributes.is_show_car_filter',
                    'ru' => 'Показывать фильтр по авто',
                ],
                [
                    'name' => 'general.helpers.position_in_footer',
                    'ru' => 'Если категорию нужно скрыть - оставьте поле пустым',
                ],
                [
                    'name' => 'seo.count',
                    'ru' => 'Количество категорий товаров - :count',
                ],
                [
                    'name' => 'admin.add-image',
                    'ru' => 'Добавить SVG иконку',
                ],
                [
                    'name' => 'admin.tabs.filter-products',
                    'ru' => 'Значения дл подбора товаров',
                ],
                [
                    'name' => 'admin.attribute.other_categories',
                    'ru' => 'Доп. категории для подбора',
                ],
                [
                    'name' => 'admin.attribute.features',
                    'ru' => 'Характеристики для подбора',
                ],
                [
                    'name' => 'admin.attribute.related_categories',
                    'ru' => 'Доп. категории для подбора сопутствующие товары',
                ],
                [
                    'name' => 'admin.attribute.related_features',
                    'ru' => 'Характеристики для подбора сопутствующие товары',
                ],
                [
                    'name' => 'admin.attributes.search_keywords',
                    'ru' => 'Поисковые слова',
                ],
                [
                    'name' => 'general.buy-something',
                    'ru' => 'Продолжить покупки',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.all-categories',
                    'ru' => 'Все категории',
                ],
                [
                    'name' => 'general.buy-something',
                    'ru' => 'Продолжить покупки',
                ],
                [
                    'name' => 'site.categories-list',
                    'ru' => 'Список категорий',
                ],
                [
                    'name' => 'site.category',
                    'ru' => 'Категория',
                ],
                [
                    'name' => 'site.catalog',
                    'ru' => 'Каталог',
                ],
                [
                    'name' => 'site.show-all',
                    'ru' => 'Показать все',
                ],
                [
                    'name' => 'site.hide',
                    'ru' => 'Скрыть',
                ],
                [
                    'name' => 'site.sort',
                    'ru' => 'Сортировать:',
                ],
                [
                    'name' => 'site.items-per-page',
                    'ru' => 'Товаров на странице:',
                ],
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
