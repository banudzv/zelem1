<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRelevanceColumnIntoCategoriesTable extends Migration
{

    public function up()
    {
        Schema::table(
            'categories',
            function (Blueprint $table) {
                $table->integer('relevance')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'categories',
            function (Blueprint $table) {
                $table->dropColumn('relevance');
            }
        );
    }
}
