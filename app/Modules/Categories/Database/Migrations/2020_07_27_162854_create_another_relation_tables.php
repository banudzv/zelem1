<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnotherRelationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_relation_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('feature_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('categories')
                ->index('categories_relation_features_category_id_categories_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('feature_id')->references('id')->on('features')
                ->index('categories_relation_features_feature_id_features_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


        Schema::create('categories_relation_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('other_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('categories')
                ->index('categories_relation_category_category_id_categories_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('other_id')->references('id')->on('categories')
                ->index('categories_relation_category_other_id_category_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories_relation_features', function (Blueprint $table) {
            $table->dropForeign('categories_relation_features_category_id_categories_id');
            $table->dropForeign('categories_relation_features_feature_id_features_id');
        });
        Schema::table('categories_relation_category', function (Blueprint $table) {
            $table->dropForeign('categories_relation_category_category_id_categories_id');
            $table->dropForeign('categories_relation_category_other_id_category_id');
        });
        Schema::dropIfExists('categories_features');
        Schema::dropIfExists('categories_other');
    }
}
