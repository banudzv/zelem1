<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSearchKeywordsIntoCategoriesTranslatesTable extends Migration
{

    public function up()
    {
        Schema::table(
            'categories_translates',
            function (Blueprint $table) {
                $table->text('search_keywords')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'categories_translates',
            function (Blueprint $table) {
                $table->dropColumn('search_keywords');
            }
        );
    }
}
