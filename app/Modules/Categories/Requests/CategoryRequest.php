<?php

namespace App\Modules\Categories\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Categories\Images\CategoryImage;
use App\Modules\Categories\Models\Category;
use App\Modules\Categories\Models\CategoryTranslates;
use App\Modules\Categories\Rules\AvailableToChooseCategory;
use App\Rules\MultilangSlug;
use App\Traits\ValidationRulesTrait;
use Config;
use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;

    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * @throws WrongParametersException
     */
    public function rules(): array
    {
        /** @var Category $category */
        $category = $this->route('category');

        $parentIdRules = ['nullable'];
        if ($category) {
            $parentIdRules[] = new AvailableToChooseCategory($category);
        }

        return $this->generateRules(
            [
                'active' => ['required', 'boolean'],
                'relevance' => ['nullable', 'integer'],
                'position_in_footer' => ['nullable', 'integer'],
                CategoryImage::getField() => ['sometimes', 'image', 'max:' . config('image.max-size')],
                'parent_id' => ['nullable', $parentIdRules],
            ],
            [
                'name' => ['required', 'max:191'],
                'slug' => [
                    'required',
                    new MultilangSlug(
                        (new CategoryTranslates())->getTable(),
                        null,
                        $category->id ?? null
                    ),
                ],
                'search_keywords' => ['nullable', 'string', 'max:2048'],
            ]
        );
    }

    public function attributes(): array
    {
        $attributes = [];
        foreach (Config::get('languages') as $key => $lang) {
            $attributes = [
                $key . '.slug' => trans('validation.attributes.slug')
            ];
        }
        return $attributes;
    }

}
