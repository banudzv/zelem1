<?php

namespace App\Modules\Categories\Forms;

use App\Components\Form\ObjectValues\ModelForSelect;
use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Categories\Images\CategoryImage;
use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\Service;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Image;
use CustomForm\Input;
use CustomForm\Macro\InputForSlug;
use CustomForm\Macro\MultiSelect;
use CustomForm\Macro\Slug;
use CustomForm\Macro\Toggle;
use CustomForm\Select;
use CustomForm\Text;
use CustomForm\TextArea;
use CustomForm\TinyMce;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class CategoryForm implements FormInterface
{
    /**
     * Make form
     *
     * @param Model|null $category
     * @return Form
     * @throws WrongParametersException
     * @throws Throwable
     */
    public static function make(?Model $category = null): Form
    {
        $categories = Category::with('current')->get();
        $parametersForSelect = ModelForSelect::make($categories)
            ->setParentIdKeyFieldName('parent_id')
            ->setValueFieldName('current.name');
        $category = $category ?? new Category();
        $form = Form::create();
        $tabs = $form->tabs();
        $mainInformationTab = $tabs->createTab('admin.tabs.general-information');
        $mainInformationTab->fieldSet(6, FieldSet::COLOR_SUCCESS)->add(
            Toggle::create('active', $category)->required(),
            Toggle::create('show_engine_features_in_filter', $category)
                ->setLabel('categories::general.attributes.show_engine_features_in_filter')
                ->required(),
            Toggle::create('show_product_features_in_filter', $category)
                ->setLabel('categories::general.attributes.show_product_features_in_filter')
                ->required(),
            Input::create('relevance', $category)
                ->setType('number')
                ->setLabel('categories::general.attributes.relevance')
                ->required(),
            Input::create('position_in_footer', $category)
                ->setLabel('categories::general.attributes.position_in_footer')
                ->setHelp('categories::general.helpers.position_in_footer'),
            Select::create('parent_id', $category)
                ->model($parametersForSelect)
                ->setCanChooseGroupElement(true)
                ->setDoNotShowElement($category->id ?? null)
                ->setPlaceholder('&mdash;'),
            MultiSelect::create('services[]')->add(
                Service::with('current')->get(['id'])->mapWithKeys(
                    function (Service $service) {
                        return [$service->id => $service->current->name];
                    }
                )->toArray()
            )->setValue($category ? $category->services_ids : []),
            Text::create()->setDefaultValue(view('categories::admin.svg', ['category' => $category])),
            Image::create(CategoryImage::getField(), $category->image)
        );
        $mainInformationTab->fieldSetForLang(6)->add(
            InputForSlug::create('name', $category)->required(),
            Slug::create('slug', $category)->required(),
            TextArea::create('search_keywords', $category)
                ->setLabel('categories::admin.attributes.search_keywords')
        );
        $tabs->createTab('admin.tabs.seo')->fieldSetForLang()->add(
            Input::create('h1', $category),
            Input::create('title', $category),
            TextArea::create('keywords', $category),
            TextArea::create('description', $category),
            TinyMce::create('seo_text', $category)
        );
        if ($category->exists) {
            $categoriesForSelect = $categories->where('id', '!=', $category->id);
            $featuresForSelect = Category::getFeatures($category->id);

            $selectedOtherCategory = $category->otherCategories->pluck('id')->toArray();
            $selectedFeatures = $category->features->pluck('id')->toArray();

            $selectedRelCategory = $category->relCategories->pluck('id')->toArray();
            $selectedRelFeatures = $category->relFeatures->pluck('id')->toArray();
            $tabs->createTab('categories::admin.tabs.filter-products')->fieldSet()->add(
                MultiSelect::create('other_categories[]')
                    ->model(
                        ModelForSelect::make(
                            $categoriesForSelect
                        )->setValueFieldName('current.name')
                    )
                    ->setLabel('categories::admin.attribute.other_categories')
                    ->setValue($selectedOtherCategory),
                MultiSelect::create('features[]')
                    ->add($featuresForSelect)
                    ->setLabel('categories::admin.attribute.features')
                    ->setValue($selectedFeatures),

                MultiSelect::create('rel_categories[]')
                    ->model(
                        ModelForSelect::make(
                            $categoriesForSelect
                        )->setValueFieldName('current.name')
                    )
                    ->setLabel('categories::admin.attribute.related_categories')
                    ->setValue($selectedRelCategory),
                MultiSelect::create('rel_features[]')
                    ->add($featuresForSelect)
                    ->setLabel('categories::admin.attribute.related_features')
                    ->setValue($selectedRelFeatures)
            );
        }
        return $form;
    }

}
