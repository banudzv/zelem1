<?php

namespace App\Modules\Categories\Resources;

use App\Modules\Categories\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CategoryFullResource
 *
 * @package App\Modules\Categories\Resources
 *
 * @OA\Schema(
 *   schema="CategoryFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "active", "position", "data"},
 *           @OA\Property(property="id", type="integer", description="ID"),
 *           @OA\Property(property="active", type="boolean", description="Category activity"),
 *           @OA\Property(property="position", type="integer", description="Category position"),
 *           @OA\Property(property="image", type="string", description="Link to original image"),
 *           @OA\Property(
 *              property="data",
 *              type="array",
 *              description="Multilanguage data",
 *              @OA\Items(
 *                  type="object",
 *                  allOf={
 *                      @OA\Schema(
 *                          required={"language", "name", "slug"},
 *                          @OA\Property(property="language", type="string", description="Language to store"),
 *                          @OA\Property(property="name", type="string", description="Category name"),
 *                          @OA\Property(property="slug", type="string", description="Category slug"),
 *                          @OA\Property(property="seo_text", type="string", description="Seo text for category"),
 *                          @OA\Property(property="h1", type="string", description="Category meta h1"),
 *                          @OA\Property(property="title", type="string", description="Category meta title"),
 *                          @OA\Property(property="description", type="string", description="Category meta description"),
 *                          @OA\Property(property="keywords", type="string", description="Category meta keywords"),
 *                      )
 *                  }
 *              )
 *           ),
 *           @OA\Property(
 *              property="children",
 *              description="Sub categories list",
 *              type="array",
 *              @OA\Items(ref="#/components/schemas/CategoryFullInformation")
 *           )
 *       )
 *   }
 * )
 */
class CategoryFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Category $category */
        $category = $this->resource;
        $response = $category->toArray();
        
        unset($response['image']);
        $response['image'] = $category->image->link('original', false);
        
        return $response;
    }
}
