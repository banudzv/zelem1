<?php

namespace App\Modules\Search\Livewire\Admin;

class IndexList extends AbstractIndexComponent
{
    protected $listeners = [
        'index.added' => 'render',
        'index.deleted' => 'render',
        'index.change-main' => 'render',
        'index.change-state' => 'render',
    ];

    public function render()
    {
        return view('search::admin.indexes.index')
            ->with('indexes', $this->searchService()->getAll());
    }
}