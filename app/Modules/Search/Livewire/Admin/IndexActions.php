<?php

namespace App\Modules\Search\Livewire\Admin;

use App\Exceptions\WrongParametersException;
use App\Helpers\Alert;
use App\Modules\Search\Models\SearchIndex;
use Exception;
use Illuminate\Http\RedirectResponse;
use Throwable;

class IndexActions extends AbstractIndexComponent
{
    public $key;

    public $isMain;

    public $name;

    public $state;

    public $currentPage = '/admin/system/search';

    public function mount(SearchIndex $searchIndex)
    {
        $this->key = $searchIndex->getKey();
        $this->isMain = $searchIndex->isMain();
        $this->state = $searchIndex->getState();
        $this->name = $searchIndex->getName();
    }

    /**
     * @param int $id
     * @return bool|RedirectResponse
     * @throws WrongParametersException
     */
    public function delete(int $id)
    {
        try {
            $this->searchService()
                ->deleteIndexById($id);

            $this->emitUp('index.deleted');

            return true;
        } catch (Throwable $throwable) {
            Alert::danger($throwable->getMessage());

            return redirect()->to($this->currentPage);
        }
    }

    /**
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function setMain(int $id)
    {
        $this->searchService()
            ->setAsMainById($id);

        $this->searchService()->cacheFlush();

        $this->emitUp('index.change-main');

        return true;
    }

    /**
     * @param int $id
     * @return bool|RedirectResponse
     * @throws WrongParametersException
     */
    public function create(int $id)
    {
        try {
            $this->searchService()
                ->createIndex($id);

            $this->emitUp('index.change-state');

            return true;
        } catch (Throwable $throwable) {
            Alert::danger($throwable->getMessage());

            return redirect()->to($this->currentPage);
        }
    }

    /**
     * @param int $id
     * @return bool|RedirectResponse
     * @throws WrongParametersException
     */
    public function fillIndex(int $id)
    {
        try {
            $this->searchService()
                ->fill($id);

            $this->emitUp('index.change-state');

            return true;
        } catch (Throwable $throwable) {
            Alert::danger($throwable->getMessage());

            return redirect()->to($this->currentPage);
        }
    }

    /**
     * @param int $id
     * @return bool|RedirectResponse
     * @throws WrongParametersException
     */
    public function updateSchema(int $id)
    {
        try {
            $this->searchService()
                ->updateSchema($id);

            $this->emitUp('index.change-state');
            Alert::success('search::admin.actions.schema_updated');

            return redirect()->to($this->currentPage);
        } catch (Throwable $throwable) {
            Alert::danger($throwable->getMessage());

            return redirect()->to($this->currentPage);
        }
    }

    /**
     * @param int $id
     * @return bool|RedirectResponse
     * @throws WrongParametersException
     */
    public function refresh(int $id)
    {
        try {
            $this->searchService()
                ->fill($id);

            $this->emitUp('index.change-state');

            return true;
        } catch (Throwable $throwable) {
            Alert::danger($throwable->getMessage());

            return redirect()->to($this->currentPage);
        }
    }

    public function render()
    {
        return view('search::admin.indexes.actions');
    }
}