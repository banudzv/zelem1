<?php

namespace App\Modules\Search\Livewire\Admin;

use Illuminate\Validation\ValidationException;

class CreateIndex extends AbstractIndexComponent
{

    public $type;

    public $name;

    /** @see CreateIndex::updatedName() */
    public $slug;

    public $description;

    public function create()
    {
        $this->generateSlug($this->name);

        $this->validate($this->rules());

        $this->searchService()->createModel($this->getPublicPropertiesDefinedBySubClass());

        $this->emit('index.added');

        $this->clear();

        $this->dispatchBrowserEvent('modal-close');
    }

    private function generateSlug($value)
    {
        $this->slug = str_slug($value);
    }

    protected function rules(): array
    {
        return [
            'name' => ['bail', 'required', 'string', 'min:3', 'unique:search_indexes,name'],
            'slug' => ['bail', 'required', 'string', 'min:3', 'unique:search_indexes,slug'],
            'type' => ['bail', 'required', 'string', 'min:3'],
            'description' => ['bail', 'nullable', 'string', 'min:5'],
        ];
    }

    /**
     * @param $name
     * @throws ValidationException
     */
    public function updated($name): void
    {
        $this->validateOnly($name, $this->rules());
    }

    public function updatedName($value): void
    {
        $this->generateSlug($value);
    }

    public function render()
    {
        return view('search::admin.indexes.create')
            ->with('types', $this->getIndexTypes());
    }

    protected function getIndexTypes(): array
    {
        return config('search.index_types');
    }

    protected function clearFields(): void
    {
        $this->name = '';
        $this->type = '';
        $this->slug = '';
        $this->description = '';
    }
}