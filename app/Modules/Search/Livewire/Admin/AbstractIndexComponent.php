<?php

namespace App\Modules\Search\Livewire\Admin;

use App\Modules\Search\Services\SearchService;
use Livewire\Component;

abstract class AbstractIndexComponent extends Component
{
    protected function searchService(): SearchService
    {
        return resolve(SearchService::class);
    }


    protected function rules(): array
    {
        return [
        ];
    }

    protected function clear()
    {
        $this->clearValidation();

        $this->clearFields();
    }

    protected function clearFields()
    {
    }

}