<?php

namespace App\Modules\Search\Commands;

use App\Modules\Search\Commands\Traits\ElasticIndexConfiguration;

class ElasticIndexDropCommand extends \ScoutElastic\Console\ElasticIndexDropCommand
{
    use ElasticIndexConfiguration;

    protected $name = 'searchable:drop-index';

    protected $description = 'Удалить индекс';
}