<?php

namespace App\Modules\Search\Commands;

use App\Modules\Search\Commands\Traits\ElasticIndexConfiguration;
use App\Modules\Search\Models\Traits\Searchable;
use Exception;
use Illuminate\Database\Eloquent\Model;

class ElasticUpdateMappingCommand extends \ScoutElastic\Console\ElasticUpdateMappingCommand
{
    use ElasticIndexConfiguration;

    protected $name = 'searchable:update-mapping';

    protected $description = 'Обновить схему индекса.';

    /**
     * @return Searchable|Model
     * @throws Exception
     */
    protected function getModel()
    {
        /** @var Searchable $modelInstance */
        $modelInstance = parent::getModel();

        $configurator = $modelInstance->getIndexConfigurator();

        if ($indexName = $this->getIndexName()) {
            $configurator->setName($indexName);
        }

        return $modelInstance;
    }

}