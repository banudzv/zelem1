<?php

namespace App\Modules\Search\Commands;

use App\Modules\Search\Models\SearchIndex;
use App\Modules\Search\Models\Traits\Searchable;
use App\Modules\Search\Services\BulkIndexer;
use App\Modules\Search\Services\SearchService;
use App\Modules\System\Commands\ProcessableInterface;
use App\Modules\System\Commands\Traits\Processable;
use App\Modules\System\Jobs\SearchableModelSyncJob;
use Carbon\Carbon;
use Closure;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

class ElasticIndexUpdateCommand extends Command implements ProcessableInterface
{

    use Processable;

    protected $signature = 'searchable:model:sync {model} {index-name?} {process?} {--updated_at=}';

    protected $description = 'Обновление данных продуктов в индексе';

    protected $chunk = 500;

    /**
     * @var BulkIndexer
     */
    protected $service;

    /**
     * @var SearchService
     */
    protected $searchService;

    /**
     * @throws Throwable
     * @throws InvalidArgumentException
     */
    public function handle()
    {
        $this->init();

        $count = $this->queryWithFilter()->count();

        $this->startMessage();

        $this->startProgress($count);

        $currentIndex = $this->getIndex();

        try {
            $updatedModel = $this->getSearchableModel();

            $currentIndex->setUpdating();

            $this->queryWithFilter()
                ->chunk($this->chunk, $this->doUpdate($updatedModel));

            $currentIndex->setUpdated();

            $this->finishProgress();

            $this->line(PHP_EOL);

            $this->endMessage();

            return;
        } catch (Throwable $throwable) {
            $currentIndex->setError();

            $this->failProgress($throwable->getMessage());

            throw $throwable;
        }
    }

    protected function init()
    {
        $this->searchService = resolve(SearchService::class);
        $this->service = resolve(BulkIndexer::class);
    }

    /**
     * @return Builder
     */
    protected function queryWithFilter(): Builder
    {
        return $this->getQuery()
            ->where($this->filter());
    }

    protected function getQuery()
    {
        return $this->getModelClass()->query();
    }

    protected function getModelClass(): Model
    {
        $class = $this->argument('model');

        return new $class;
    }

    protected function filter(): Closure
    {
        return function (Builder $builder) {
            if ($updatedAt = $this->updateAt()) {
                $builder->where('updated_at', '>', $updatedAt);
            }
        };
    }

    protected function updateAt()
    {
        if ($updatedAt = $this->option('updated_at')) {
            return Carbon::parse($updatedAt);
        }

        return null;
    }

    protected function startMessage()
    {
        $this->line($this->description . PHP_EOL);
    }

    /**
     * @return SearchIndex
     * @throws Exception
     */
    protected function getIndex()
    {
        $indexName = $this->getIndexName();

        if ($index = $this->searchService->findBySlug($indexName)) {
            return $index;
        }

        throw new Exception(sprintf('Index with slug %s not found.', $indexName));
    }

    /**
     * @return string|null
     * @throws Exception
     */
    protected function getIndexName(): ?string
    {
        if ($indexName = $this->argument('index-name')) {
            return $indexName;
        }

        $searchableType = $this->getModel()->getSearchableType();
        if ($searchIndex = $this->searchService->getMainIndexByType($searchableType)) {
            return $searchIndex->getSlug();
        }

        throw new Exception('Main search index must be set!');
    }

    /**
     * @return Searchable
     */
    protected function getModel()
    {
        $class = $this->getModelClass();

        /** @var Searchable $model */
        $model = new $class;

        return $model;
    }

    /**
     * @return Searchable
     * @throws Exception
     */
    protected function getSearchableModel()
    {
        $model = $this->getModel();

        $configurator = $model->getIndexConfigurator();

        if ($indexName = $this->getIndexName()) {
            $configurator->setName($indexName);
        }

        return $model;
    }

    /**
     * @param Searchable $model
     * @return Closure
     */
    protected function doUpdate($model): Closure
    {
        return function (Collection $models) use ($model) {
            $models->load($model->searchableWith());

            $this->service->update($models, $model);

            $this->addProgress($models->count());
        };
    }

    protected function endMessage(): void
    {
        $this->line(
            sprintf('Операция: "%s" - окончена.', $this->description)
        );
    }

    public function getProcessType(): string
    {
        return SearchableModelSyncJob::type();
    }
}