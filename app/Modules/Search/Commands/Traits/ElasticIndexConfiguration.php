<?php

namespace App\Modules\Search\Commands\Traits;

use App\Modules\Search\Configurations\AbstractIndexConfigurator;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;

trait ElasticIndexConfiguration
{
    protected function getIndexConfigurator()
    {
        $configuratorClass = trim($this->argument('index-configurator'));

        $configuratorInstance = new $configuratorClass;

        if (!($configuratorInstance instanceof AbstractIndexConfigurator)) {
            throw new InvalidArgumentException(
                sprintf(
                    'The class %s must extend %s.',
                    $configuratorClass,
                    AbstractIndexConfigurator::class
                )
            );
        }

        if ($indexName = $this->getIndexName()) {
            $configuratorInstance->setName($indexName);
        }

        return $configuratorInstance;
    }

    protected function getIndexName()
    {
        return $this->argument('index-name');
    }

    protected function getArguments()
    {
        $arguments = parent::getArguments();

        $arguments[] = [
            'index-name',
            InputArgument::OPTIONAL,
            'The index name',
        ];

        return $arguments;
    }
}