<?php

namespace App\Modules\Search\Commands;

use App\Modules\Search\Commands\Traits\ElasticIndexConfiguration;

class ElasticIndexCreateCommand extends \ScoutElastic\Console\ElasticIndexCreateCommand
{
    use ElasticIndexConfiguration;

    protected $name = 'searchable:create-index';

    protected $description = 'Создать индекс.';
}