<?php

use App\Modules\Search\Controllers\Admin\SearchController;

Route::middleware(['auth:admin'])->name('admin.system.')->group(
    function () {
        Route::get('system/search', [SearchController::class, 'index'])
            ->name('search.index');

        Route::livewire('system/search/indexes', 'index.table')
            ->name('elastic.indexes');

        Route::livewire('system/search/indexes/create', 'index.create')
            ->name('elastic.indexes.create');

        Route::livewire('system/search/indexes/{searchIndex}/actions', 'index.actions')
            ->name('elastic.indexes.actions');
    }
);
