<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSearchIndexesTable extends Migration
{

    public function up()
    {
        Schema::create(
            'search_indexes',
            function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('type');
                $table->string('name');
                $table->string('slug');
                $table->string('description')->nullable();
                $table->boolean('active')->default(0);
                $table->boolean('is_main')->nullable();
                $table->string('state')->default('not_created');

            }
        );
    }


    public function down()
    {
        Schema::dropIfExists('search_indexes');
    }
}
