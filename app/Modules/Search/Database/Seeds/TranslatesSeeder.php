<?php

namespace App\Modules\Search\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'search';

    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'Поиск',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Список индексов',
                ],
                [
                    'name' => 'seo.count',
                    'ru' => 'Количество индексов :count',
                ],
                [
                    'name' => 'settings.attributes.category-limit',
                    'ru' => 'Лимит категорий',
                ],
                [
                    'name' => 'settings.attributes.product-limit',
                    'ru' => 'Лимит товаров',
                ],
                [
                    'name' => 'settings.attributes.min-cost',
                    'ru' => 'Минимальный бал для найденных данных',
                ],
                [
                    'name' => 'settings.live-search',
                    'ru' => 'Живой поиск',
                ],
                [
                    'name' => 'admin.indexes.create',
                    'ru' => 'Создать',
                ],
                [
                    'name' => 'admin.indexes.create-index',
                    'ru' => 'Создать индекс',
                ],
                [
                    'name' => 'admin.index_types.product',
                    'ru' => 'Продукт',
                ],
                [
                    'name' => 'admin.attributes.description',
                    'ru' => 'Описание',
                ],
                [
                    'name' => 'admin.attributes.status',
                    'ru' => 'Состояние',
                ],
                [
                    'name' => 'admin.attributes.actions',
                    'ru' => 'Действия',
                ],
                [
                    'name' => 'admin.modal.confirmation',
                    'ru' => 'Подтверждение действия',
                ],
                [
                    'name' => 'admin.index.main',
                    'ru' => 'Основной',
                ],
                [
                    'name' => 'admin.index.set-main',
                    'ru' => 'Сделать основным',
                ],
                [
                    'name' => 'admin.index.delete',
                    'ru' => 'Удалить',
                ],
                [
                    'name' => 'admin.states.not_created',
                    'ru' => 'Не создан',
                ],
                [
                    'name' => 'admin.index.delete-main',
                    'ru' => 'Нельзя удалить главный индекс.',
                ],
                [
                    'name' => 'admin.states.empty',
                    'ru' => 'Не заполнен',
                ],
                [
                    'name' => 'admin.states.updated',
                    'ru' => 'Обновлён',
                ],
                [
                    'name' => 'admin.states.updating',
                    'ru' => 'Обновляется',
                ],
                [
                    'name' => 'admin.states.filling',
                    'ru' => 'Заполняется',
                ],
                [
                    'name' => 'admin.actions.create',
                    'ru' => 'Создать',
                ],
                [
                    'name' => 'admin.actions.fill',
                    'ru' => 'Заполнить',
                ],
                [
                    'name' => 'admin.actions.clear',
                    'ru' => 'Очистить',
                ],
                [
                    'name' => 'admin.actions.refresh',
                    'ru' => 'Обновить',
                ],
                [
                    'name' => 'admin.actions.update_schema',
                    'ru' => 'Обновить схему',
                ],
                [
                    'name' => 'admin.actions.schema_updated',
                    'ru' => 'Схема обновлена',
                ],
            ],
            Translate::PLACE_SITE => [
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
