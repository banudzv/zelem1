<?php

namespace App\Modules\Search\Configurations;

use ScoutElastic\IndexConfigurator;

abstract class AbstractIndexConfigurator extends IndexConfigurator
{
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }
}