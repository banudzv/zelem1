<?php

namespace App\Modules\Search\Configurations;

use ScoutElastic\Migratable;

class ProductIndexConfiguratorOld extends AbstractIndexConfigurator
{
    use Migratable;

    protected $settings = [
        'analysis' => [
            'char_filter' => [
                'replace' => [
                    'type' => 'mapping',
                    'mappings' => [
                        '&=> and '
                    ],
                ],
                'rus_en_key' => [
                    'type' => 'mapping',
                    'mappings' => [
                        "a => ф",
                        "b => и",
                        "c => с",
                        "d => в",
                        "e => у",
                        "f => а",
                        "g => п",
                        "h => р",
                        "i => ш",
                        "j => о",
                        "k => л",
                        "l => д",
                        "m => ь",
                        "n => т",
                        "o => щ",
                        "p => з",
                        "r => к",
                        "s => ы",
                        "t => е",
                        "u => г",
                        "v => м",
                        "w => ц",
                        "x => ч",
                        "y => н",
                        "z => я",
                        "A => Ф",
                        "B => И",
                        "C => С",
                        "D => В",
                        "E => У",
                        "F => А",
                        "G => П",
                        "H => Р",
                        "I => Ш",
                        "J => О",
                        "K => Л",
                        "L => Д",
                        "M => Ь",
                        "N => Т",
                        "O => Щ",
                        "P => З",
                        "R => К",
                        "S => Ы",
                        "T => Е",
                        "U => Г",
                        "V => М",
                        "W => Ц",
                        "X => Ч",
                        "Y => Н",
                        "Z => Я",
                        "[ => х",
                        "] => ъ",
                        "; => ж",
                        "< => б",
                        "> => ю",
                        "Ё => Е",
                        "Й => И",
                        "ё => е",
                        "і => и",
                        "І => И",
                    ],
                ],
            ],
            'filter' => [
                'word_delimiter' => [
                    'type' => 'word_delimiter',
                    'split_on_numerics' => false,
                    'split_on_case_change' => true,
                    'generate_word_parts' => true,
                    'generate_number_parts' => true,
                    'catenate_all' => true,
                    'preserve_original' => true,
                    'catenate_numbers' => true,
                ],
                'trigrams' => [
                    'type' => 'edge_ngram',
                    'min_gram' => 2,
                    'max_gram' => 14,
                    'token_chars' => [
                        'letter',
                        'digit',
                        'whitespace'
                    ],
                ],
            ],
            'analyzer' => [
                'default' => [
                    'type' => 'custom',
                    'char_filter' => [
                        'html_strip',
                        'replace',
                        'rus_en_key'
                    ],
                    'tokenizer' => 'whitespace',
                    'filter' => [
                        'lowercase',
                        'word_delimiter',
                        'trigrams',
                    ],
                ],
            ],
        ],
    ];
}