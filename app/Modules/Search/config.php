<?php


use App\Modules\Products\Models\Product;
use App\Modules\Search\Models\SearchIndex;

return [
    'index_types' => [
        Product::TABLE_NAME => 'search::admin.index_types.product',
    ],
    'short_types' => [
        Product::TABLE_NAME => Product::class,
    ],
    'states' => [
        SearchIndex::STATE_NOT_CREATED => 'search::admin.states.not_created',
        SearchIndex::STATE_EMPTY => 'search::admin.states.empty',
        SearchIndex::STATE_UPDATED => 'search::admin.states.updated',
        SearchIndex::STATE_UPDATING => 'search::admin.states.updating',
        SearchIndex::STATE_FILLING => 'search::admin.states.filling',
        SearchIndex::STATE_ERROR => 'search::admin.states.error',
    ],
    'state_classes' => [
        SearchIndex::STATE_NOT_CREATED => 'default',
        SearchIndex::STATE_EMPTY => 'warning',
        SearchIndex::STATE_UPDATED => 'success',
        SearchIndex::STATE_FILLING => 'primary',
        SearchIndex::STATE_UPDATING => 'primary',
        SearchIndex::STATE_ERROR => 'danger',
    ]
];