<?php

namespace App\Modules\Search\Services;

use App\Modules\Search\Models\SearchIndex;
use App\Modules\Search\Repositories\SearchIndexRepository;
use DB;
use Exception;
use Log;
use Throwable;

class SearchService
{
    /**
     * @var SearchIndexRepository
     */
    protected $indexRepository;

    /**
     * @var ElasticIndexService
     */
    protected $elasticIndexService;

    public function __construct(
        SearchIndexRepository $indexRepository,
        ElasticIndexService $elasticIndexService
    ) {
        $this->indexRepository = $indexRepository;
        $this->elasticIndexService = $elasticIndexService;
    }

    public function getAll()
    {
        return $this->indexRepository->all();
    }

    public function createModel(array $attributes): SearchIndex
    {
        return SearchIndex::query()->create($attributes);
    }

    /**
     * @param  int  $searchIndexId
     *
     * @throws Exception
     */
    public function deleteIndexById(int $searchIndexId)
    {
        $currentIndex = $this->findById($searchIndexId);

        $this->checkMain($currentIndex);

        try {
            $this->elasticIndexService->drop($currentIndex);
        } catch (Throwable $throwable) {
            Log::error($throwable);
        }

        $currentIndex->delete();
    }

    public function findById(int $id)
    {
        return $this->indexRepository->findById($id);
    }

    /**
     * @param  int|SearchIndex  $searchIndex
     *
     * @throws Exception
     */
    public function setAsMainById($searchIndex): void
    {
        try {
            DB::beginTransaction();

            if ($searchIndex instanceof SearchIndex) {
                $currentIndex = $searchIndex;
            } else {
                $currentIndex = $this->findById($searchIndex);
            }

            $this->indexRepository->whereTypes($currentIndex->getType())
                ->update(['is_main' => null]);

            $currentIndex->setAsMain();

            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            Log::error($throwable);
        }
    }

    /**
     * @param  int  $searchIndexId
     *
     * @throws Throwable
     */
    public function createIndex(int $searchIndexId)
    {
        try {
            $currentIndex = $this->findById($searchIndexId);

            $this->elasticIndexService->create($currentIndex);

            $currentIndex->setCreated();
        } catch (Throwable $throwable) {
            Log::error($throwable);
            throw $throwable;
        }
    }

    /**
     * @param  int  $searchIndexId
     *
     * @throws Throwable
     */
    public function fill(int $searchIndexId)
    {
        $currentIndex = $this->findById($searchIndexId);

        try {
            $this->elasticIndexService->fill($currentIndex);
        } catch (Throwable $throwable) {
            Log::error($throwable);
            throw $throwable;
        }
    }

    /**
     * @param  int  $searchIndexId
     *
     * @throws Throwable
     */
    public function updateSchema(int $searchIndexId)
    {
        $currentIndex = $this->findById($searchIndexId);

        try {
            $this->elasticIndexService->updateSchema($currentIndex);
        } catch (Throwable $throwable) {
            Log::error($throwable);
            throw $throwable;
        }
    }

    public function findBySlug(string $indexName)
    {
        return $this->indexRepository->findBySlug($indexName);
    }

    public function getMainIndexByType(string $type)
    {
        return $this->indexRepository->getMainIndexByTypeCached($type);
    }

    public function cacheFlush(): void
    {
        $this->indexRepository->cacheFlush();
    }

    /**
     * @param  SearchIndex  $currentIndex
     *
     * @throws Exception
     */
    private function checkMain(SearchIndex $currentIndex): void
    {
        if ($currentIndex->isMain()) {
            throw new Exception(__('search::admin.index.delete-main'));
        }
    }
}