<?php

namespace App\Modules\Search\Services;

use App\Modules\Search\Commands\ElasticIndexCreateCommand;
use App\Modules\Search\Commands\ElasticIndexDropCommand;
use App\Modules\Search\Commands\ElasticIndexUpdateCommand;
use App\Modules\Search\Commands\ElasticUpdateMappingCommand;
use App\Modules\Search\Models\SearchIndex;
use Artisan;

class ElasticIndexService
{

    public function drop(SearchIndex $currentIndex)
    {
        Artisan::call(
            ElasticIndexDropCommand::class,
            [
                'index-configurator' => $currentIndex->getIndexConfiguratorClass(),
                'index-name' => $currentIndex->getSlug(),
            ]
        );
    }

    public function create(SearchIndex $currentIndex)
    {
        Artisan::call(
            ElasticIndexCreateCommand::class,
            [
                'index-configurator' => $currentIndex->getIndexConfiguratorClass(),
                'index-name' => $currentIndex->getSlug(),
            ]
        );

        Artisan::call(
            ElasticUpdateMappingCommand::class,
            [
                'model' => $currentIndex->getModelClass(),
                'index-name' => $currentIndex->getSlug(),
            ]
        );
    }

    public function updateSchema(SearchIndex $currentIndex)
    {
        Artisan::call(
            ElasticUpdateMappingCommand::class,
            [
                'model' => $currentIndex->getModelClass(),
                'index-name' => $currentIndex->getSlug(),
            ]
        );
    }

    public function fill(SearchIndex $currentIndex)
    {
        Artisan::queue(
            ElasticIndexUpdateCommand::class,
            [
                'model' => $currentIndex->getModelClass(),
                'index-name' => $currentIndex->getSlug(),
            ]
        )->onQueue('elastic');
    }
}