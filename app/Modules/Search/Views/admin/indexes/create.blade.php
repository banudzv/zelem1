<div>
    <div class="text-right clearfix">
        <button
            type="button"
            class="btn btn-flat bg-olive margin"
            data-toggle="modal"
            data-target="#create-index"
        >
            @lang('search::admin.indexes.create')
        </button>
    </div>

    <div
        {{ 'wire:ignore.self' }}
        class="modal fade"
        id="create-index"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">
                        @lang('search::admin.indexes.create-index')
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="ajax-form">
                        <div class="form-group @error('type') {{ 'has-error' }} @enderror">
                            <label for="type" class="control-label">
                                @lang('validation.attributes.type')
                            </label>
                            <select
                                class="form-control"
                                name="type"
                                {{ 'wire:model=type' }}
                            >
                                <option
                                    value=""
                                >
                                    {{ '---------------' }}
                                </option>

                                @foreach($types as $key => $name)
                                    <option
                                        selected="{{ $type === $type ? 'selected' : ''}}"
                                        value="{{ $key }}"
                                    >
                                        @lang($name)
                                    </option>
                                @endforeach
                            </select>
                            @error('type')
                            <span class="help-block error-help-block">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="form-group @error('name') {{ 'has-error' }} @enderror">
                            <label for="name" class="control-label">
                                @lang('validation.attributes.name')
                            </label>
                            <input
                                class="form-control"
                                type="text"
                                {{ 'wire:model.debounce.500ms=name' }}
                            >
                            @error('name')
                            <span class="help-block error-help-block">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="form-group @error('slug') {{ 'has-error' }} @enderror">
                            <label for="slug" class="control-label">
                                @lang('validation.attributes.slug')
                            </label>
                            <input
                                disabled
                                class="form-control"
                                type="text"
                                {{ 'wire:model.debounce.500ms=slug' }}
                            >
                            @error('slug')
                            <span class="help-block error-help-block">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="form-group @error('description') {{ 'has-error' }} @enderror">
                            <label for="description" class="control-label">
                                @lang('search::admin.attributes.description')
                            </label>
                            <textarea
                                class="form-control"
                                type="text"
                                rows="4"
                                {{ 'wire:model=description' }}
                            ></textarea>
                            @error('description')
                            <span class="help-block error-help-block">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <section class="pull-right">
                            <button
                                type="button"
                                class="btn btn-flat btn-m btn-success"
                                {{ 'wire:click.prevent=create' }}
                            >
                                @lang('buttons.save')
                            </button>
                        </section>
                        <div class="clearfix">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener("modal-close", function () {
            $('.modal').modal('hide')
        });
    </script>
@endpush