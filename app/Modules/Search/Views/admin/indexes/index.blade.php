@php
    use App\Modules\Search\Models\SearchIndex;
    use Illuminate\Database\Eloquent\Collection;
    /** @var SearchIndex[]|Collection $indexes */
@endphp

<div class="col-xs-12" {{ 'wire:poll.5s' }}>
    {!! '' !!}
    <div class="box">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                    <th>
                        @lang('validation.attributes.name')
                    </th>
                    <th>
                        @lang('validation.attributes.slug')
                    </th>
                    <th>
                        @lang('validation.attributes.type')
                    </th>
                    <th>
                        @lang('search::admin.attributes.status')
                    </th>
                    <th>
                        @lang('validation.attributes.updated_at')
                    </th>
                    <th>
                    </th>
                </tr>
                @foreach($indexes as $key => $index)
                    <tr>
                        <td class="col-lg-2">
                            {{ $index->getName() }}
                        </td>
                        <td class="col-lg-2">
                            <span class="label label-blue">
                                {{ $index->getSlug() }}
                            </span>
                        </td>
                        <td class="col-lg-2">
                            <span class="label label-default">
                            {{ $index->getType() }}
                            </span>
                        </td>
                        <td class="col-lg-2">
                            <span class="label label-{{ $index->getStateClass() }}">
                                {{ $index->getStateText() }}
                            </span>
                        </td>
                        <td class="col-lg-2">
                            {{ $index->updated_at }}
                        </td>
                        <td class="col-lg-2">
                            @livewire('index.actions', ['searchIndex' => $index], key(rand()))
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="text-center">
        {{--        {{ $indexes->appends(request()->except('page'))->links() }}--}}
    </div>
</div>
