<div class="pull-right">

    @if ($isMain)
        <button
            type="button"
            class="btn btn-xs btn-flat btn-success"
        >
            <i
                class="fa fa-check"
                title="@lang('search::admin.index.main')"
            >
            </i>
        </button>
    @else
        <button
            type="button"
            class="btn btn-xs btn-flat btn-warning"
            onclick="confirm('@lang('search::admin.index.set-main')') || event.stopImmediatePropagation()"
            {{ sprintf('wire:click=setMain(%d)', $key) }}
        >
            <i
                class="fa fa-warning"
                title="@lang('search::admin.index.set-main')"
            >
            </i>
        </button>
    @endif

    @if ($state === \App\Modules\Search\Models\SearchIndex::STATE_NOT_CREATED)
        <button
            type="button"
            class="btn btn-xs btn-flat btn-primary"
            onclick="confirm('@lang('search::admin.actions.create')?') || event.stopImmediatePropagation()"
            {{ sprintf('wire:click=create(%d)', $key) }}
        >
            <i
                class="fa fa-plus"
                title="@lang('search::admin.actions.create')"
            >
            </i>
        </button>
    @endif

    @if ($state !== \App\Modules\Search\Models\SearchIndex::STATE_NOT_CREATED)
        <button
            type="button"
            class="btn btn-xs btn-flat btn-info"
            onclick="confirm('@lang('search::admin.actions.update_schema')?') || event.stopImmediatePropagation()"
            {{ sprintf('wire:click=updateSchema(%d)', $key) }}
        >
            <i
                class="fa fa-wrench"
                title="@lang('search::admin.actions.update_schema')"
            >
            </i>
        </button>
    @endif

    @if ($state === \App\Modules\Search\Models\SearchIndex::STATE_EMPTY)
        <button
            type="button"
            class="btn btn-xs btn-flat btn-primary"
            onclick="confirm('@lang('search::admin.actions.fill')?') || event.stopImmediatePropagation()"
            {{ sprintf('wire:click=fillIndex(%d)', $key) }}
        >
            <i
                class="fa fa-download"
                title="@lang('search::admin.actions.fill')"
            >
            </i>
        </button>
    @endif

    @if ($state === \App\Modules\Search\Models\SearchIndex::STATE_FILLING)
        <button
            type="button"
            class="btn btn-xs btn-flat btn-default"
            disabled
        >
            <i
                class="fa fa-refresh"
                title="@lang('search::admin.actions.fill')"
            >
            </i>
        </button>
    @endif


    @if (
        $state === \App\Modules\Search\Models\SearchIndex::STATE_UPDATED
        || $state ===  \App\Modules\Search\Models\SearchIndex::STATE_ERROR
    )
        <button
            type="button"
            class="btn btn-xs btn-flat btn-primary"
            onclick="confirm('@lang('search::admin.actions.update')?') || event.stopImmediatePropagation()"
            {{ sprintf('wire:click=refresh(%d)', $key) }}
        >
            <i
                class="fa fa-refresh"
                title="@lang('search::admin.actions.update')"
            >
            </i>
        </button>
    @endif


    <button
        class="btn btn-xs btn-flat btn-danger"
        type="button"
        onclick="confirm('@lang('admin.messages.delete')') || event.stopImmediatePropagation()"
        {{ sprintf('wire:click=delete(%d)', $key) }}
    >
        <i
            class="fa fa-trash"
            title="@lang('search::admin.index.delete')"
        >
        </i>
    </button>

</div>
