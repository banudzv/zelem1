@extends('admin.layouts.main')

@section('content')
    @livewire('index.create')
    @livewire('index.table')
@endsection
