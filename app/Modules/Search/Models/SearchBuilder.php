<?php

namespace App\Modules\Search\Models;

use App\Modules\Search\Models\Traits\Searchable;
use App\Modules\Search\Resolvers\ElasticResponseResolver;
use Closure;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Arr;
use stdClass;

class SearchBuilder
{
    protected $aggregations = [];

    protected $query = [];

    protected $source = [];

    protected $orders;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var int
     */
    protected $limit;

    protected $with = [];

    /**
     * @var Searchable
     */
    protected $modelClass;

    /**
     * @var Model|Searchable
     */
    protected $model;

    /**
     * @var float
     */
    protected $minScore;

    /**
     * @var string
     */
    protected $collapse;

    /**
     * @var bool
     */
    private $explain;

    public function __construct(string $modelClass)
    {
        $this->modelClass = $modelClass;
    }

    public function select($fields): self
    {
        $this->source = array_merge(
            $this->source,
            Arr::wrap($fields)
        );

        return $this;
    }

    public function aggr(): self
    {
        return $this->aggregation(...func_get_args());
    }

    public function aggregation(string $name, string $field, $type = 'terms', int $size = 1000): self
    {
        $this->aggregations[$name][$type]['field'] = $field;

        if ($type === 'terms') {
            $this->aggregations[$name][$type]['size'] = $size;
        }

        return $this;
    }

    public function whereIn(string $field, array $values): self
    {
        $this->query['bool']['must'][] = [
            'terms' => [
                $field => $values,
            ],
        ];

        return $this;
    }

    public function where($field, $values = null): self
    {
        if ($field instanceof Closure) {
            $this->query['bool']['must'] = array_merge(
                $this->query['bool']['must'],
                $field()
            );

            return $this;
        }

        $this->query['bool']['must'][] = [
            'term' => [
                $field => $values,
            ],
        ];

        return $this;
    }

    public function orWhereRaw($raw): self
    {
        return $this->whereRaw($raw, 'should');
    }

    /**
     * @param          $raw
     * @param string $where
     *
     * @return $this
     */
    public function whereRaw($raw, $where = 'must'): self
    {
        if ($raw instanceof Closure) {
            $raw = $raw();
        }

        $this->query['bool'][$where] = array_merge(
            $this->query['bool']['must'],
            $raw
        );

        return $this;
    }

    public function orWhere(string $field, $values): self
    {
        $this->query['bool']['should'][] = [
            'term' => [
                $field => $values,
            ],
        ];

        return $this;
    }

    public function whereBetween(string $field, array $value): self
    {
        $this->query['bool']['must'][] = [
            'range' => [
                $field => [
                    'gte' => $value[0],
                    'lte' => $value[1],
                ],
            ],
        ];

        return $this;
    }

    public function orderBy(string $field, $direction = 'asc'): self
    {
        $this->orders[] = [
            $field => strtolower($direction) === 'asc' ? 'asc' : 'desc',
        ];

        return $this;
    }

    public function query(): array
    {
        $request['query'] = $this->query ?: ['match_all' => new stdClass()];

        if ($this->aggregations) {
            $request['aggregations'] = $this->aggregations;
        }

        if ($this->source) {
            $request['_source'] = $this->source;
        }

        if ($this->orders) {
            $request['sort'] = $this->orders;
        }

        if ($this->limit) {
            $request['size'] = $this->limit;
        }

        if ($this->offset) {
            $request['from'] = $this->offset;
        }

        if ($this->minScore) {
            $request['min_score'] = $this->minScore;
        }

        if ($this->collapse) {
            $request['collapse']['field'] = $this->collapse;
        }

        if ($this->explain) {
            $request['explain'] = true;
        }

        return $request;
    }

    public function filter(array $parameters, string $filterClass): self
    {
        (new $filterClass())
            ->setParameters($parameters)
            ->setBuilder($this)
            ->handle();

        return $this;
    }

    public function paginate($perPage = null, $pageName = 'page', $page = null): LengthAwarePaginator
    {
        $resolver = $this->resolvePaginate($perPage, $pageName, $page);

        $searchable = $this->getModel();

        $ids = $resolver->getKeys();
        $models = $this->resolveModels($searchable, $ids);

        return $this->doPaginate($models, $resolver->total(), $perPage, $page);
    }

    public function resolvePaginate($perPage = null, $pageName = 'page', $page = null): ElasticResponseResolver
    {
        $searchable = $this->getModel();

        $this->take($perPage ?: $searchable->getPerPage())
            ->setPage($page ?: Paginator::resolveCurrentPage($pageName));

        return $this->resolve();
    }

    /**
     * @return Searchable|Model
     */
    private function getModel(): Model
    {
        if (is_null($this->model)) {
            $this->model = new $this->modelClass();
        }

        return $this->model;
    }

    public function setPage(int $page): self
    {
        $limit = $this->limit ?: $this->getModel()->getPerPage();

        return $this->from(($page - 1) * $limit);
    }

    public function from(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    public function take(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function resolve(): ElasticResponseResolver
    {
        return self::resolveRaw($this->get());
    }

    public static function resolveRaw(array $rawResponse): ElasticResponseResolver
    {
        return new ElasticResponseResolver($rawResponse);
    }

    public function get(): array
    {
        $model = $this->getModel();

        return $model->searchableUsing()
            ->searchRaw($model, $this->query());
    }

    private function resolveModels(Model $searchable, array $ids): Collection
    {
        $models = $searchable->newQuery()
            ->find($ids)
            ->sortBy(sort_models_by_ids_order($ids));

        if ($this->with) {
            $models->load($this->with);
        }

        return $models;
    }

    /**
     * @param array|Collection $items
     * @param int $total
     * @param int $perPage
     * @param int $page
     * @return LengthAwarePaginator
     */
    private function doPaginate($items, int $total, int $perPage, int $page): LengthAwarePaginator
    {
        return new LengthAwarePaginator($items, $total, $perPage, $page);
    }

    public function paginateRaw($perPage = null, $pageName = 'page', $page = null): LengthAwarePaginator
    {
        $resolver = $this->resolvePaginate($perPage, $pageName, $page);

        return $this->doPaginate($resolver->items(), $resolver->total(), $perPage, $page);
    }

    public function setPerPage(int $perPage): self
    {
        return $this->take($perPage);
    }

    public function with($relations): self
    {
        $this->with = is_string($relations) ? func_get_args() : $relations;

        return $this;
    }

    public function minScore(float $score): self
    {
        $this->minScore = $score;

        return $this;
    }

    public function whereNotIn(string $field, array $values): self
    {
        $this->query['bool']['must_not'][] = [
            'terms' => [
                $field => $values,
            ],
        ];

        return $this;
    }

    public function orMultiMatch(array $fields, string $value, string $type = 'best_fields', array $options = []): self
    {
        return $this->multiMatch($fields, $value, $type, $options, 'should');
    }

    public function multiMatch(
        array $fields,
        string $value,
        string $type = 'best_fields',
        array $options = [],
        $where = 'must'
    ): self {
        $this->query['bool'][$where][] = [
            'multi_match' => array_merge(
                [
                    'query' => $value,
                    'fields' => $fields,
                    'type' => $type,
                ],
                $options
            )
        ];

        return $this;
    }

    public function orTerm(string $field, string $value, $boost = 1.0): self
    {
        return $this->term($field, $value, $boost, 'should');
    }

    public function term(string $field, string $value, $boost = 1.0, $where = 'must'): self
    {
        $this->query['bool'][$where][] = [
            'term' => [
                $field => [
                    'value' => $value,
                    'boost' => $boost,
                ],
            ],
        ];

        return $this;
    }

    public function orWildcard(string $field, string $value, $boost = 1.0, array $options = []): self
    {
        return $this->wildcard($field, $value, $boost, $options, 'should');
    }

    public function wildcard(string $field, string $value, $boost = 1.0, array $options = [], $where = 'must'): self
    {
        $params = [
            'value' => $value,
            'boost' => $boost,
        ];

        $this->query['bool'][$where][] = [
            'wildcard' => [
                $field => array_merge($params, $options),
            ],
        ];

        return $this;
    }

    public function orFuzzy(string $field, string $value, $fuzziness = 'AUTO'): self
    {
        return $this->fuzzy($field, $value, $fuzziness, 'should');
    }

    public function fuzzy(string $field, string $value, $fuzziness = 'AUTO', $where = 'must'): self
    {
        $this->query['bool'][$where][] = [
            'fuzzy' => [
                $field => [
                    'value' => $value,
                    'fuzziness' => $fuzziness,
                    'transpositions' => false
                ],
            ],
        ];

        return $this;
    }

    public function collapse(string $field): self
    {
        $this->collapse = $field;

        return $this;
    }

    public function orMatch(string $field, string $value, array $parameters = []): self
    {
        return $this->match($field, $value, $parameters, 'should');
    }

    public function match(string $field, string $value, array $options = [], $where = 'must'): self
    {
        $basicParams = [
            'query' => $value,
            'operator' => 'and',
            'zero_terms_query' => 'none',
            'auto_generate_synonyms_phrase_query' => false,
        ];

        if ($options) {
            $basicParams = array_merge($basicParams, $options);
        }

        $this->query['bool'][$where][] = [
            'match' => [
                $field => $basicParams,
            ],
        ];

        return $this;
    }

    public function orMatchPhrase(string $field, string $query, $boost = 1.0, $options = []): self
    {
        return $this->matchPhrase($field, $query, $boost, $options, 'should');
    }

    public function matchPhrase(string $field, string $query, $boost = 1.0, $options = [], $where = 'must'): self
    {
        $params = [
            'query' => $query,
            'boost' => $boost,
        ];

        $params = array_merge($params, $options);

        $this->query['bool'][$where][] = [
            'match_phrase' => [
                $field => $params,
            ],
        ];

        return $this;
    }

    public function orMatchPhrasePrefix(string $field, string $query, array $options = []): self
    {
        return $this->matchPhrasePrefix($field, $query, $options, 'should');
    }

    public function matchPhrasePrefix(string $field, string $query, array $options = [], $where = 'must'): self
    {
        $params = [
            'query' => $query,
        ];

        $params = array_merge($params, $options);

        $this->query['bool'][$where][] = [
            'match_phrase_prefix' => [
                $field => $params,
            ],
        ];

        return $this;
    }

    public function orSimpleQueryString(array $fields, string $value, $options = []): self
    {
        return $this->simpleQueryString($fields, $value, $options, 'should');
    }

    public function simpleQueryString(array $fields, string $value, $options = [], $where = 'must'): self
    {
        $params = [
            'query' => $value,
            'fields' => $fields,
        ];

        if (count($options)) {
            $params = array_merge($params, $options);
        }

        $this->query['bool'][$where][] = [
            'simple_query_string' => $params,
        ];

        return $this;
    }

    public function orQueryString(string $query, float $boost = 1.0, array $options = []): self
    {
        return $this->queryString($query, $boost, $options, 'should');
    }

    public function queryString(string $query, float $boost = 1.0, array $options = [], $where = 'must'): self
    {
        $params = [
            'query' => $query,
            'boost' => $boost
        ];

        $this->query['bool'][$where][] = array_merge($params, $options);

        return $this;
    }

    public function explain(): array
    {
        $this->explain = true;

        return $this->get();
    }

    public function dumpRawAndDie(): void
    {
        dd($this->get());
    }

    public function dumpExplainAndDie(): void
    {
        dd($this->explain());
    }

    public function dumpQueryAndDie(): void
    {
        dd($this->query());
    }

}