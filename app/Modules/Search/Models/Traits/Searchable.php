<?php

namespace App\Modules\Search\Models\Traits;

use App\Modules\Search\Configurations\AbstractIndexConfigurator;
use App\Modules\Search\Models\SearchBuilder;
use App\Modules\Search\Services\SearchService;
use Exception;
use ScoutElastic\Searchable as ElasticSearchable;

trait Searchable
{
    use ElasticSearchable {
        ElasticSearchable::search as searchByText;
    }

    protected static $indexConfiguratorModel;

    public static function searchBuilder()
    {
        return new SearchBuilder(__CLASS__);
    }

    public function getIndexConfiguratorClass(): string
    {
        return $this->indexConfigurator;
    }

    /**
     * @return AbstractIndexConfigurator
     * @throws Exception
     */
    public function getIndexConfigurator()
    {
        if (!static::$indexConfiguratorModel) {
            if (!isset($this->indexConfigurator) || empty($this->indexConfigurator)) {
                throw new Exception(
                    sprintf(
                        'An index configurator for the %s model is not specified.',
                        __CLASS__
                    )
                );
            }

            $indexConfiguratorClass = $this->indexConfigurator;
            /** @var AbstractIndexConfigurator $configurator */
            $configurator = new $indexConfiguratorClass;
            $searchIndex = resolve(SearchService::class)
                ->getMainIndexByType($this->getSearchableType());

            if ($searchIndex) {
                $configurator->setName($searchIndex->getSlug());
            }

            static::$indexConfiguratorModel = $configurator;
        }

        return static::$indexConfiguratorModel;
    }

    public function getSearchableType(): string
    {
        return $this->getTable();
    }

    public function searchableWith(): array
    {
        return [];
    }
}