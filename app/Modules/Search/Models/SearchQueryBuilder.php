<?php

namespace App\Modules\Search\Models;

class SearchQueryBuilder
{
    protected $query = [];

    protected $source = [];

    protected $orders;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var float
     */
    protected $minScore;

    /**
     * @var string
     */
    protected $collapse;

}