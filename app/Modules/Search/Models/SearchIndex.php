<?php

namespace App\Modules\Search\Models;

use App\Modules\Search\Models\Traits\Searchable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string type
 * @property string name
 * @property string description
 * @property string slug
 * @property string state
 * @property int active
 * @property int is_main
 * @property Carbon|null created_at
 * @property Carbon|null updated_at
 *
 * @method static Builder|self query()
 * @method static self create(array $attributes = [])
 * @method static self find(int|array $id)
 */
class SearchIndex extends Model
{
    public const TABLE_NAME = 'search_indexes';

    public const MAIN = 1;

    public const STATE_NOT_CREATED = 'not_created';

    public const STATE_EMPTY = 'empty';

    public const STATE_FILLING = 'filling';

    public const STATE_UPDATED = 'updated';

    public const STATE_UPDATING = 'updating';

    public const STATE_ERROR = 'error';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'type',
        'is_main',
        'name',
        'slug',
        'status',
        'state',
        'description',
    ];

    public function getName(): string
    {
        return $this->name;
    }

    public function getStateText(): string
    {
        return __(config('search.states')[$this->getState()]);
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getStateClass(): string
    {
        return config('search.state_classes')[$this->getState()];
    }

    public function isMain(): bool
    {
        return !!$this->is_main;
    }

    public function getIndexConfiguratorClass(): string
    {
        $modelClass = $this->getModelClass();
        /** @var Searchable $model */
        $model = new $modelClass;

        return $model->getIndexConfiguratorClass();
    }

    public function getModelClass(): string
    {
        return config('search.short_types')[$this->getType()];
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setAsMain(): void
    {
        $this->is_main = self::MAIN;

        $this->save();
    }

    public function setCreated(): void
    {
        $this->state = self::STATE_EMPTY;

        $this->save();
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setUpdated(): void
    {
        $this->state = self::STATE_UPDATED;

        $this->save();
    }

    public function setUpdating(): void
    {
        if ($this->isEmpty()) {
            $this->state = self::STATE_FILLING;
        } else {
            $this->state = self::STATE_UPDATING;
        }

        $this->save();
    }

    private function isEmpty()
    {
        return $this->getState() === self::STATE_EMPTY;
    }

    public function setError()
    {
        $this->state = self::STATE_ERROR;

        $this->save();
    }

}