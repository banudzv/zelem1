<?php

namespace App\Modules\Search\Models;

interface SearchableWithRelations
{
    /**
     * Массив подгружаемых связей во время импорта в индекс
     *
     * @return array
     */
    public function searchableWith(): array ;
}