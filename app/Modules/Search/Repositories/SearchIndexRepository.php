<?php

namespace App\Modules\Search\Repositories;

use App\Modules\Search\Models\SearchIndex;
use App\Traits\CachableTrait;
use Illuminate\Database\Eloquent\Builder;

/**
 * @see SearchIndexRepository::getMainIndexByType()
 * @method SearchIndex getMainIndexByTypeCached(string $type)
 */
class SearchIndexRepository
{
    use CachableTrait;

    public function all()
    {
        return $this->query()
            ->get()
            ->keyBy('id');
    }

    /**
     * @return Builder|SearchIndex
     */
    public function query(): Builder
    {
        return SearchIndex::query();
    }

    public function findById(int $id): SearchIndex
    {
        return $this->query()->find($id);
    }

    public function findBySlug(string $indexName): ?SearchIndex
    {
        /** @var SearchIndex $index */
        $index = $this->query()->where('slug', $indexName)->first();

        return $index;
    }

    public function getMainIndexByType(string $type)
    {
        return $this->whereTypes($type)
            ->where('is_main', true)
            ->first();
    }

    public function whereTypes($type): Builder
    {
        return $this->query()->where('type', $type);
    }
}