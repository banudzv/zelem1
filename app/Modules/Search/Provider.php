<?php

namespace App\Modules\Search;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Search\Commands\ElasticIndexCreateCommand;
use App\Modules\Search\Commands\ElasticIndexDropCommand;
use App\Modules\Search\Commands\ElasticIndexUpdateCommand;
use App\Modules\Search\Commands\ElasticUpdateMappingCommand;
use App\Modules\Search\Livewire\Admin\CreateIndex;
use App\Modules\Search\Livewire\Admin\IndexActions;
use App\Modules\Search\Livewire\Admin\IndexList;
use App\Modules\Search\Services\SearchService;
use CustomForm\Input;
use CustomMenu;
use CustomRoles;
use CustomSettings;
use Livewire;

class Provider extends BaseProvider
{

    /**
     * @throws WrongParametersException
     */
    public function loadForAdmin()
    {
        CustomMenu::get()->group()
            ->block('system')
            ->link(
                'search::general.menu',
                RouteObjectValue::make('admin.system.search.index')
            )
            ->additionalRoutesForActiveDetect(
                ...[
                       RouteObjectValue::make('admin.system.search.index'),
                   ]
            );

        $settings = CustomSettings::createAndGet('search', 'search::settings.live-search');
        $settings->add(
            Input::create('product-limit')
                ->setLabel('search::settings.attributes.product-limit')
                ->setDefaultValue(7)
                ->setType('number')
                ->required(),
            ['required', 'integer']
        );

        $settings->add(
            Input::create('category-limit')
                ->setLabel('search::settings.attributes.category-limit')
                ->setDefaultValue(3)
                ->setType('number')
                ->required(),
            ['required', 'integer']
        );

        $settings->add(
            Input::create('min-cost')
                ->setLabel('search::settings.attributes.min-cost')
                ->setDefaultValue(1.0)
                ->setType('number')
                ->required(),
            ['required', 'numeric']
        );

        CustomRoles::add('search', 'search::general.permission-name')
            ->enableAll()
            ->except(RoleRule::UPDATE);
    }

    protected function presets()
    {
        $this->app->singleton(SearchService::class, SearchService::class);

        $this->commands(
            [
                ElasticIndexCreateCommand::class,
                ElasticIndexDropCommand::class,
                ElasticUpdateMappingCommand::class,
                ElasticIndexUpdateCommand::class,
            ]
        );
    }

    protected function livewire()
    {
        Livewire::component('index.table', IndexList::class);
        Livewire::component('index.create', CreateIndex::class);
        Livewire::component('index.actions', IndexActions::class);
    }

}