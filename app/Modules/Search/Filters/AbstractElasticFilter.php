<?php

namespace App\Modules\Search\Filters;

use App\Modules\Search\Models\SearchBuilder;
use Illuminate\Support\Str;

abstract class AbstractElasticFilter
{
    /**
     * @var array
     */
    protected $parameters;

    /**
     * @var SearchBuilder
     */
    protected $builder;

    public function setParameters(array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function handle(): void
    {
        $this->before($this->parameters);

        foreach ($this->parameters as $key => $value) {
            $key = Str::camel($key);
            if (!empty($value) && method_exists($this, $key)) {
                $this->$key($value);
                unset($this->parameters[$key]);
            }
        }

        $this->after($this->parameters);
    }

    protected function before(array $parameters)
    {
    }

    protected function after(array $parameters)
    {
    }

    protected function getBuilder(): SearchBuilder
    {
        return $this->builder;
    }

    public function setBuilder(SearchBuilder $builder): self
    {
        $this->builder = $builder;

        return $this;
    }

}