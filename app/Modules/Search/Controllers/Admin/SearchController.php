<?php

namespace App\Modules\Search\Controllers\Admin;

use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Search\Services\SearchService;
use Seo;

class SearchController
{
    /**
     * @var SearchService
     */
    protected $searchService;

    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    public function index()
    {
        Seo::breadcrumbs()->add('search::seo.index', RouteObjectValue::make('admin.system.search.index'));

        Seo::meta()->setH1('search::seo.index');
        Seo::meta()->setCountOfEntity(
            trans(
                'search::seo.count',
                [
                    'count' => $this->searchService->getAll()->count()
                ]
            )
        );

        return view('search::admin.index');
    }
}