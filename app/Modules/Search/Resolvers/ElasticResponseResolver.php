<?php

namespace App\Modules\Search\Resolvers;

class ElasticResponseResolver
{
    /**
     * @var array
     */
    protected $response;

    public function __construct(array $response)
    {
        $this->response = $response;
    }

    public function getAggregationByName(string $name)
    {
        $aggr = $this->response['aggregations'][$name];

        if (isset($aggr['buckets'])) {
            return $this->collect($aggr['buckets']);
        }

        return $this->value($aggr);
    }

    protected function collect(array $buckets): array
    {
        $result = [];

        foreach ($buckets as ['key' => $key, 'doc_count' => $value]) {
            $result[$key] = $value;
        }

        return $result;
    }

    protected function value(array $aggr)
    {
        return $aggr['value'];
    }

    public function getResponse(): array
    {
        return $this->response;
    }

    public function total(): int
    {
        return $this->response['hits']['total']['value'];
    }

    public function getKeys(): array
    {
        return collect($this->items())
            ->pluck('_id', '_id')
            ->toArray();
    }

    public function items(): array
    {
        return $this->response['hits']['hits'];
    }

    public function dd(): void
    {
        dd($this->response);
    }
}