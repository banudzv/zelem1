<?php

namespace App\Modules\SlideshowSimple\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\SlideshowSimple\Images\SliderSmallImage;
use App\Modules\SlideshowSimple\Models\SlideshowSmall;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Image;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\TinyMce;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleForm
 *
 * @package App\Core\Modules\Administrators\Forms
 */
class SlideshowSmallForm implements FormInterface
{
    
    /**
     * @param Model|SlideshowSmall|null $slideshowSimple
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $slideshowSimple = null): Form
    {
        $slideshowSimple = $slideshowSimple ?? new SlideshowSmall;
        $form = Form::create();
        $image = Image::create(SliderSmallImage::getField(), $slideshowSimple->image);
        if (!$slideshowSimple->image || !$slideshowSimple->image->exists) {
            $image->required();
        }
        // Field set with languages tabs
        $form->fieldSetForLang(12)->add(
            Input::create('name', $slideshowSimple)->required(),
            Input::create('url', $slideshowSimple)
        );
        // Simple field set
        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->add(
            Toggle::create('active', $slideshowSimple)->required(),
            $image
        );
        return $form;
    }
    
}
