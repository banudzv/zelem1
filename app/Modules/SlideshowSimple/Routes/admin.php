<?php

use Illuminate\Support\Facades\Route;

// Only authenticated
Route::middleware(['auth:admin', 'permission:slideshow_simple'])->group(function () {
    // Admins list in admin panel
    Route::put(
        'slideshow-simple/sortable',
        ['uses' => 'SlideshowSimpleController@sortable', 'as' => 'admin.slideshow_simple.sortable']
    );
    Route::put('slideshow-simple/{slideshowSimple}/active', ['uses' => 'SlideshowSimpleController@active', 'as' => 'admin.slideshow_simple.active']);
    Route::get('slideshow-simple/{slideshowSimple}/destroy', ['uses' => 'SlideshowSimpleController@destroy', 'as' => 'admin.slideshow_simple.destroy']);
    Route::resource('slideshow-simple', 'SlideshowSimpleController')->except('show', 'destroy')->names('admin.slideshow_simple');
});
// Only authenticated
Route::middleware(['auth:admin', 'permission:slideshow_small'])->group(function () {
    // Admins list in admin panel
    Route::put(
        'slideshow-small/sortable',
        ['uses' => 'SlideshowSmallController@sortable', 'as' => 'admin.slideshow_small.sortable']
    );
    Route::put('slideshow-small/{slideshowSmall}/active', ['uses' => 'SlideshowSmallController@active', 'as' => 'admin.slideshow_small.active']);
    Route::get('slideshow-small/{slideshowSmall}/avatar/delete', ['uses' => 'SlideshowSmallController@deleteImage', 'as' => 'admin.slideshow_small.delete-avatar']);
    Route::get('slideshow-small/{slideshowSmall}/destroy', ['uses' => 'SlideshowSmallController@destroy', 'as' => 'admin.slideshow_small.destroy']);
    Route::resource('slideshow-small', 'SlideshowSmallController')->except('show', 'destroy')->names('admin.slideshow_small');
});
