<?php

namespace App\Modules\SlideshowSimple\Models;

use App\Core\Modules\Images\Models\Image;
use App\Core\Modules\Languages\Models\Language;
use App\Modules\SlideshowSimple\Images\SliderImage;
use App\Traits\ActiveScopeTrait;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Modules\SlideshowSimple\Models\SlideshowSimple
 *
 * @property int $id
 * @property int $position
 * @property bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Image[] $allImages
 * @property-read SlideshowSimpleTranslates $current
 * @property-read Collection|SlideshowSimpleTranslates[] $data
 * @method static Builder|SlideshowSimple active($active = true)
 * @method static Builder|SlideshowSimple newModelQuery()
 * @method static Builder|SlideshowSimple newQuery()
 * @method static Builder|SlideshowSimple query()
 * @method static Builder|SlideshowSimple whereActive($value)
 * @method static Builder|SlideshowSimple whereCreatedAt($value)
 * @method static Builder|SlideshowSimple whereId($value)
 * @method static Builder|SlideshowSimple wherePosition($value)
 * @method static Builder|SlideshowSimple whereUpdatedAt($value)
 * @method static Builder|SlideshowSimple whereUrl($value)
 * @mixin Eloquent
 * @property-read int|null $all_images_count
 * @property-read int|null $data_count
 * @property-read int|null $images_count
 */
class SlideshowSimple extends Model
{
    use ModelMain, ActiveScopeTrait;

    protected $table = 'slideshow_simple';

    protected $casts = ['active' => 'boolean'];

    protected $fillable = ['active'];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    public static function getList()
    {
        return self::with(['current'])
            ->oldest('position')
            ->get();
    }

    public static function getAllActive()
    {
        return self::query()
            ->active(true)
            ->oldest('position')
            ->with(['current', 'current.image', 'current.image.current'])
            ->get();
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function getPagesLinksByIdForImage(int $id)
    {
        $links = [];
        $item = SlideshowSimple::active()->find($id);
        if ($item) {
            $languages = config('languages', []);
            /** @var Language $language */
            foreach ($languages as $language) {
                $prefix = $language->default ? '' : '/' . $language->slug;
                $links[] = url($prefix . route('site.home', [], false), [], isSecure());
            }
        }
        return $links;
    }

    public function deleteImagesIfExist()
    {
        foreach ($this->data as $item) {
            $item->deleteImagesIfExist();
        }
    }

    public function getUrlAttribute(): ?string
    {
        return $this->current
            ? $this->current->url
            : null;
    }
}
