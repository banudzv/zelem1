<?php

namespace App\Modules\SlideshowSimple\Models;

use App\Core\Modules\Images\Models\Image;
use App\Core\Modules\Languages\Models\Language;
use App\Modules\SlideshowSimple\Images\SliderSmallImage;
use App\Traits\ActiveScopeTrait;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Modules\SlideshowSimple\Models\SlideshowSmall
 *
 * @property int $id
 * @property int $position
 * @property bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Image[] $allImages
 * @property-read SlideshowSmallTranslates $current
 * @property-read Collection|SlideshowSmallTranslates[] $data
 * @property-read Image $image
 * @property-read Collection|Image[] $images
 * @method static Builder|SlideshowSmall active($active = true)
 * @method static Builder|SlideshowSmall newModelQuery()
 * @method static Builder|SlideshowSmall newQuery()
 * @method static Builder|SlideshowSmall query()
 * @method static Builder|SlideshowSmall whereActive($value)
 * @method static Builder|SlideshowSmall whereCreatedAt($value)
 * @method static Builder|SlideshowSmall whereId($value)
 * @method static Builder|SlideshowSmall wherePosition($value)
 * @method static Builder|SlideshowSmall whereUpdatedAt($value)
 * @method static Builder|SlideshowSmall whereUrl($value)
 * @mixin Eloquent
 * @property-read int|null $all_images_count
 * @property-read int|null $data_count
 * @property-read int|null $images_count
 */
class SlideshowSmall extends Model
{
    use ModelMain, Imageable, ActiveScopeTrait;

    protected $table = 'slideshow_small';

    protected $casts = ['active' => 'boolean'];

    protected $fillable = ['active'];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    public static function getList()
    {
        return self::query()
            ->with(['current'])
            ->oldest('position')
            ->get();
    }

    public static function getAllActive($limit = 999999)
    {
        return self::query()
            ->with(['current', 'image', 'image.current'])
            ->active(true)
            ->oldest('position')
            ->limit($limit)
            ->get();
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function getPagesLinksByIdForImage(int $id)
    {
        $links = [];
        $item = SlideshowSmall::active()->find($id);
        if ($item) {
            $languages = config('languages', []);
            /** @var Language $language */
            foreach ($languages as $language) {
                $prefix = $language->default ? '' : '/' . $language->slug;
                $links[] = url($prefix . route('site.home', [], false), [], isSecure());
            }
        }
        return $links;
    }

    /**
     * Image config
     *
     * @return string|array
     */
    protected function imageClass()
    {
        return [
            SliderSmallImage::class,
        ];
    }

    public function getUrlAttribute(): ?string
    {
        return $this->current
            ? $this->current->url
            : null;
    }
}
