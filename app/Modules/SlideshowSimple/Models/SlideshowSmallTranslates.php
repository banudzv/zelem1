<?php

namespace App\Modules\SlideshowSimple\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates
 *
 * @property int $id
 * @property string $name
 * @property int $row_id
 * @property string|null $url
 * @property string $language
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\SlideshowSimple\Models\SlideshowSmall $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates whereRowId($value)
 * @mixin \Eloquent
 * @property string $text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSmallTranslates whereText($value)
 */
class SlideshowSmallTranslates extends Model
{
    use ModelTranslates;
    
    protected $table = 'slideshow_small_translates';
    
    public $timestamps = false;
    
    protected $fillable = ['name', 'text', 'url'];
    
    protected $hidden = ['id', 'row_id'];
}
