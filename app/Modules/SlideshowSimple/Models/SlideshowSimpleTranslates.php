<?php

namespace App\Modules\SlideshowSimple\Models;

use App\Core\Modules\Images\Models\Image;
use App\Modules\SlideshowSimple\Images\SliderImage;
use App\Traits\Imageable;
use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates
 *
 * @property int $id
 * @property string $name
 * @property int $row_id
 * @property string|null $url
 * @property string $language
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\SlideshowSimple\Models\SlideshowSimple $row
 * @property-read Image $image
 * @property-read Collection|Image[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates whereRowId($value)
 * @mixin \Eloquent
 * @property string $text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\SlideshowSimple\Models\SlideshowSimpleTranslates whereText($value)
 */
class SlideshowSimpleTranslates extends Model
{
    use ModelTranslates, Imageable;
    
    protected $table = 'slideshow_simple_translates';
    
    public $timestamps = false;
    
    protected $fillable = ['name', 'text', 'url'];
    
    protected $hidden = ['id', 'row_id'];



    /**
     * Image config
     *
     * @return string|array
     */
    protected function imageClass()
    {
        return [
            SliderImage::class,
        ];
    }

    /**
     * Upload image
     *
     * @param string|null
     * @throws \App\Exceptions\WrongParametersException
     */
    public function uploadImage(?string $type = null)
    {
        $arr = request()->file($this->language, []);

        if (array_get($arr, $this->imageFieldName($type), false)) {
            $this->uploadImageFromResource(array_get($arr, $this->imageFieldName($type), false), $type);
        }
    }
}
