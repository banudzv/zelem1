<?php

namespace App\Modules\SlideshowSimple\Controllers\Api;

use App\Core\ApiController;
use App\Modules\SlideshowSimple\Models\SlideshowSimple;
use App\Modules\SlideshowSimple\Resources\SlideFullResource;

/**
 * Class SlidesController
 *
 * @package App\Modules\SlideshowSimple\Controllers\Api
 */
class SlidesController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/slides/all",
     *     tags={"Slideshow"},
     *     summary="Returns all slides with all data",
     *     operationId="getSlideshowFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/SlideFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return SlideFullResource::collection(SlideshowSimple::with(['data', 'image'])->oldest('position')->get());
    }
}
