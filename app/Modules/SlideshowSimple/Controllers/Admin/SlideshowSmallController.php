<?php

namespace App\Modules\SlideshowSimple\Controllers\Admin;

use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\SlideshowSimple\Forms\SlideshowSmallForm;
use App\Modules\SlideshowSimple\Models\SlideshowSmall;
use App\Modules\SlideshowSimple\Requests\SliderSmallRequest;
use Seo;
use App\Core\AdminController;

/**
 * Class SlideshowSmallController
 *
 * @package App\Modules\SlideshowSimple\Controllers\Admin
 */
class SlideshowSmallController extends AdminController
{
    
    public function __construct()
    {
        Seo::breadcrumbs()->add('slideshow_simple::seo.slideshow_small.index', RouteObjectValue::make('admin.slideshow_small.index'));
    }
    
    /**
     * Register widgets with buttons
     */
    private function registerButtons()
    {
        // Create new slideshow button
        $this->addCreateButton('admin.slideshow_small.create');
    }
    
    /**
     * News sortable list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // Set page buttons on the top of the page
        $this->registerButtons();
        // Set h1
        Seo::meta()->setH1('slideshow_simple::seo.slideshow_small.index');
        // Get slideshowSmall
        $sliders = SlideshowSmall::getList();
        // Return view list
        return view('slideshow_simple::admin.small.index', ['sliders' => $sliders]);
    }
    
    /**
     * Create new element page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function create()
    {
        // Breadcrumb
        Seo::breadcrumbs()->add('slideshow_simple::seo.slideshow_small.create');
        // Set h1
        Seo::meta()->setH1('slideshow_simple::seo.slideshow_small.create');
        // Javascript validation
        $this->initValidation((new SliderSmallRequest())->rules());
        // Return form view
        return view('slideshow_simple::admin.small.create', [
            'form' => SlideshowSmallForm::make(),
        ]);
    }
    
    /**
     * Create page in database
     *
     * @param SliderSmallRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(SliderSmallRequest $request)
    {
        $slideshowSmall = (new SlideshowSmall());
        // Create new slideshowSmall
        if ($message = $slideshowSmall->createRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterStore(['id' => $slideshowSmall->id]);
    }
    
    /**
     * Update element page
     *
     * @param SlideshowSmall $slideshowSmall
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(SlideshowSmall $slideshowSmall)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($slideshowSmall->current->name ?? 'slideshow_simple::seo.slideshow_small.edit');
        // Set h1
        Seo::meta()->setH1('slideshow_simple::seo.slideshow_small.edit');
        // Javascript validation
        $this->initValidation((new SliderSmallRequest())->rules());
        // Return form view
        return view('slideshow_simple::admin.small.update', [
            'form' => SlideshowSmallForm::make($slideshowSmall),
        ]);
    }
    
    /**
     * Update page in database
     *
     * @param SliderSmallRequest $request
     * @param SlideshowSmall $slideshowSmall
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(SliderSmallRequest $request, SlideshowSmall $slideshowSmall)
    {
        // Update existed slideshow
        if ($message = $slideshowSmall->updateRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterUpdate();
    }
    
    /**
     * Totally delete page from database
     *
     * @param SlideshowSmall $slideshowSmall
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(SlideshowSmall $slideshowSmall)
    {
        // Delete news's image
        $slideshowSmall->deleteImagesIfExist();
        // Delete news
        $slideshowSmall->forceDelete();
        // Do something
        return $this->afterDestroy();
    }
    
    /**
     * Delete image
     *
     * @param SlideshowSmall $slideshowSmall
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function deleteImage(SlideshowSmall $slideshowSmall)
    {
        // Delete slideshowSmall's image
        $slideshowSmall->deleteImagesIfExist();
        // Do something
        return $this->afterDeletingImage();
    }
    
}
