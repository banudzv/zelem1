<?php

namespace App\Modules\SlideshowSimple\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\SlideshowSimple\Models\SlideshowSimple;
use App\Modules\SlideshowSimple\Models\SlideshowSmall;

class Slider implements AbstractWidget
{

    public function render()
    {
        $slides = SlideshowSimple::getAllActive();

        $small = SlideshowSmall::getAllActive(3);

        return view('slideshow_simple::site.widget', [
            'slides' => $slides,
            'small' => $small
        ]);
    }
    
}
