<?php

namespace App\Modules\SlideshowSimple\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\SlideshowSimple\Images\SliderImage;
use App\Modules\SlideshowSimple\Images\SliderSmallImage;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SliderSmallRequest
 *
 * @package App\Modules\Slideshow\Requests
 */
class SliderSmallRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $simpleRules = ['active' => ['required', 'boolean']];
        if (!$this->route() || !$this->route()->parameter('slideshow_small')) {
            $simpleRules[SliderSmallImage::getField()] = ['required', 'image', 'max:' . config('image.max-size')];
        }
        return $this->generateRules($simpleRules, [
            'name' => ['required', 'max:191'],
        ]);
    }
    
}
