@php
/** @var \App\Modules\SlideshowSimple\Models\SlideshowSimple[]|\Illuminate\Database\Eloquent\Collection $slides */
/** @var \App\Modules\SlideshowSimple\Models\SlideshowSmall[]|\Illuminate\Database\Eloquent\Collection $small */
@endphp
<div class="section section--gradient _def-mb-def">
    <div class="container container--white container--full container--border-bottom-radius ">
        <div class="action-bar">
            <div class="action-bar__wrapper">
                @if(browserizr()->isDesktop())
                    <div class="action-bar__side _lg-show"></div>
                @endif
                <div class="action-bar__wide">
                    <div>
                        @include('slideshow_simple::site.action-bar-slider.action-bar-slider', [
                            'slides' => $slides,
                        ])
                    </div>
                    <div class="grid grid--3 _def-show _ptb-sm">
                        @foreach($small as $slide)
                            <div class="gcell _pr-sm">
                            <a href="{{ $slide->url }}" class="action-bar-bottom">
                                <div class="action-bar-bottom__title">
                                    {{ $slide->current->name }}
                                </div>
                                <div class="action-bar-bottom__img">
                                    <img src="{{ $slide->image->link('small') }}" alt="turbine">
                                </div>
                                <div class="action-bar-bottom__arrow">
                                    {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right') !!}
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
