@php
/** @var \App\Modules\SlideshowSimple\Models\SlideshowSimple $slide */
$image = $slide->current->image;
@endphp

<div class="slick-slider-list__item">
    <div class="action-bar-slide" style="{{ $image ? 'background-image:url(' . $image->link('big') . ');' : ''}}">
        <div class="action-bar-slide__holder">
            <div class="action-bar-slide__slide">
                <div class="action-bar-slide__content _pb-def">
                    <div class="title title--size-h1 title--normal _color-white">
                        {{ $slide->current->name }}
                    </div>
                    <div class="text _color-stroke">
                        {!! $slide->current->text !!}
                    </div>
                </div>
                    @if($slide->url)
                        <a href="{{ $slide->url }}" class="button button--theme-default button--size-large button--width-md">
                            <span class="button__body">
                                {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right-thin', [
                                    'class' => 'button__icon button__icon--after button__icon--small',
                                    'width' => '5',
                                    'height' => '9'
                                ]) !!}
                                <div class="button__text">
                                    @lang('slideshow_simple::site.more')
                                </div>
                            </span>
                        </a>
                    @endif
                <div class="action-bar-slide__lines"></div>
                <div class="action-bar-slide__image"></div>
            </div>
        </div>
    </div>
</div>
