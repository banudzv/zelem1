@php
/** @var \App\Modules\SlideshowSimple\Models\SlideshowSmall[] $sliders */
$className = App\Modules\SlideshowSimple\Models\SlideshowSmall::class;
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        <div class="dd pageList" id="myNest" data-depth="1">
            <ol class="dd-list">
                @include('slideshow_simple::admin.small.items', ['slides' => $sliders])
            </ol>
        </div>
        <span id="parameters"
              data-url="{{ route('admin.slideshow_small.sortable', ['class' => $className]) }}"></span>
        <input type="hidden" id="myNestJson">
    </div>
@stop
