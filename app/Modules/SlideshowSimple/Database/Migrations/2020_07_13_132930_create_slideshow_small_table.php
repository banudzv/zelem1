<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideshowSmallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slideshow_small', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('position')->default(0);
            $table->boolean('active')->default(false);
            $table->string('url')->nullable();
            $table->timestamps();
        });

        Schema::create('slideshow_small_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);

            $table->foreign('language')->references('slug')->on('languages')
                ->index('slideshow_small_translates_language_languages_slug')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('slideshow_small')
                ->index('slideshow_small_translates_row_id_slideshow_small_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (app()->environment() !== 'production') {
            Schema::table('slideshow_small_translates', function (Blueprint $table) {
                $table->dropForeign('slideshow_small_translates_language_languages_slug');
                $table->dropForeign('slideshow_small_translates_row_id_slideshow_small_id');
            });
            Schema::dropIfExists('slideshow_small_translates');
            Schema::dropIfExists('slideshow_small');
        }
    }
}
