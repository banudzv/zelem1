<?php

namespace App\Modules\SlideshowSimple\Resources;

use App\Modules\SlideshowSimple\Models\SlideshowSimple;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SlidesFullResource
 *
 * @package App\Modules\Pages\Resources
 *
 * @OA\Schema(
 *   schema="SlideFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"active", "data"},
 *           @OA\Property(property="position", type="integer", description="Slide position"),
 *           @OA\Property(property="active", type="boolean", description="Slide activity"),
 *           @OA\Property(property="url", type="integer", description="Link for slide button"),
 *           @OA\Property(
 *              property="data",
 *              type="array",
 *              description="Multilanguage data",
 *              @OA\Items(
 *                  type="object",
 *                  allOf={
 *                      @OA\Schema(
 *                          required={"language", "name"},
 *                          @OA\Property(property="language", type="string", description="Language to store"),
 *                          @OA\Property(property="name", type="string", description="Article name"),
 *                      )
 *                  }
 *              )
 *           ),
 *       )
 *   }
 * )
 */
class SlideFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var SlideshowSimple $slide */
        $slide = $this->resource;
        $response = $slide->toArray();
        $response['image'] = $slide->image->link('original', false);
        return $response;
    }
}
