<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUniqueIndexSlugLanguageIntoBrandsTranslatesTable extends Migration
{

    public function up()
    {
        Schema::table(
            'brands_translates',
            function (Blueprint $table) {
                $table->unique(['slug', 'language']);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'brands_translates',
            function (Blueprint $table) {
                $table->dropUnique(['slug', 'language']);
            }
        );
    }
}
