<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRelevanceFieldIntoBrandsTable extends Migration
{

    public function up()
    {
        Schema::table(
            'brands',
            function (Blueprint $table) {
                $table->integer('relevance')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'brands',
            function (Blueprint $table) {
                $table->dropColumn('relevance');
            }
        );
    }
}
