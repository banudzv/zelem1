<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFilterConfigFieldsIntoBrandsTable extends Migration
{

    public function up()
    {
        Schema::table(
            'brands',
            function (Blueprint $table) {
                $table->boolean('show_product_features_in_filter')->default(true);
                $table->boolean('show_engine_features_in_filter')->default(true);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'brands',
            function (Blueprint $table) {
                $table->dropColumn(['show_product_features_in_filter', 'show_engine_features_in_filter']);
            }
        );
    }
}
