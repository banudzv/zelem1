<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSearchKeywordsIntoBrandsTranslatesTable extends Migration
{

    public function up()
    {
        Schema::table(
            'brands_translates',
            function (Blueprint $table) {
                $table->text('search_keywords')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'brands_translates',
            function (Blueprint $table) {
                $table->dropColumn('search_keywords');
            }
        );
    }
}
