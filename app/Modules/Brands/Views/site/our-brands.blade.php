@php
/** @var \App\Modules\Brands\Models\Brand[] $brands */
@endphp

@include('brands::site.brands-slider.brands-slider', [
    'mod_class' => 'reviews-slider--theme-light',
    'brands' => $brands,
])

