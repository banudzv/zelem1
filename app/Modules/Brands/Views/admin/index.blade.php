@php
/** @var \App\Modules\Brands\Models\Brand[] $brands */
/** @var string $filter*/
$className = \App\Modules\Brands\Models\Brand::class;

@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter !!}
        <div class="box">
            <div class="box-body table-responsive no-padding mailbox-messages">
                <table class="table table-hover">
                    <tr>
                        <th>
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle">
                                <i class="fa fa-square-o"></i>
                            </button>
                        </th>
                        <th>
                            @lang('validation.attributes.name')
                        </th>
                        <th>
                            @lang('validation.attributes.relevance')
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($brands AS $brand)
                        <tr data-id="{{$brand->id}}">
                            <td width="30"><input type="checkbox"></td>
                            <td>
                                {{ $brand->getName() }}
                            </td>
                            <td>
                                <span class="label label-default" >
                                    {{ $brand->getRelevance() }}
                                </span>
                            </td>
                            <td>
                                {!! Widget::active($brand, 'admin.brands.active') !!}
                            </td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.brands.edit', $brand->id) !!}
                                {!! \App\Components\Buttons::delete('admin.brands.destroy', $brand->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $brands->appends(request()->except('page'))->links() }}</div>
        <span id="parameters-name" data-name="{{ $className }}"></span>
    </div>
@stop
