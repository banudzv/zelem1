<?php

namespace App\Modules\Brands\Controllers\Api;

use App\Core\ApiController;
use App\Modules\Brands\Models\Brand;
use App\Modules\Brands\Resources\BrandFullResource;
use App\Modules\Brands\Resources\BrandResource;
use App\Modules\Brands\Resources\BrandSimpleResource;

/**
 * Class BrandsController
 *
 * @package App\Modules\Brands\Controllers\Api
 */
class BrandsController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/brands/all",
     *     tags={"Brands"},
     *     summary="Returns all brands with all data",
     *     operationId="getBrandsFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/BrandFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return BrandFullResource::collection(Brand::with(['data', 'image'])->get());
    }
    
    /**
     * @OA\Get(
     *     path="/api/brands",
     *     tags={"Brands"},
     *     summary="Returns list of brands",
     *     operationId="getBrands",
     *     @OA\Parameter(
     *         name="imageSize",
     *         in="query",
     *         description="Image size to return (original by default)",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/BrandSimple")
     *         )
     *     ),
     * )
     */
    public function index()
    {
        return BrandSimpleResource::collection(Brand::with(['current', 'image'])->whereActive(true)->latest('id')->get());
    }
    
    /**
     * @OA\Get(
     *     path="/api/brands/{id}",
     *     tags={"Brands"},
     *     summary="Returns one brand by id",
     *     description="Returns one brand by id",
     *     operationId="getBrand",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Brand id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="brandId"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="imageSize",
     *         in="query",
     *         description="Image size to return (original by default)",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/Brand"),
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     *
     * @param Brand $brand
     * @return BrandResource
     */
    public function show(Brand $brand)
    {
        abort_unless($brand->active, 404);
        return new BrandResource($brand);
    }
}
