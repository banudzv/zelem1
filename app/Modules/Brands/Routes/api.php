<?php

use Illuminate\Support\Facades\Route;

Route::get('brands', 'BrandsController@index');
Route::get('brands/all', 'BrandsController@all');
Route::get('brands/{brand}', ['uses' => 'BrandsController@show', 'as' => 'api.brands.show']);
