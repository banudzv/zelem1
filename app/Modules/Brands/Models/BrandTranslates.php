<?php

namespace App\Modules\Brands\Models;

use App\Core\Modules\Languages\Models\Language;
use App\Traits\ModelTranslates;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\Brands\Models\BrandTranslates
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $content
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $search_keywords
 * @property string|null $description
 * @property int $row_id
 * @property string $language
 * @property-read Language $lang
 * @property-read Brand $row
 * @method static Builder|BrandTranslates newModelQuery()
 * @method static Builder|BrandTranslates newQuery()
 * @method static Builder|BrandTranslates query()
 * @method static Builder|BrandTranslates whereContent($value)
 * @method static Builder|BrandTranslates whereDescription($value)
 * @method static Builder|BrandTranslates whereH1($value)
 * @method static Builder|BrandTranslates whereId($value)
 * @method static Builder|BrandTranslates whereKeywords($value)
 * @method static Builder|BrandTranslates whereLanguage($value)
 * @method static Builder|BrandTranslates whereName($value)
 * @method static Builder|BrandTranslates whereRowId($value)
 * @method static Builder|BrandTranslates whereSlug($value)
 * @method static Builder|BrandTranslates whereTitle($value)
 * @mixin Eloquent
 */
class BrandTranslates extends Model
{
    use ModelTranslates;

    public $timestamps = false;

    protected $table = 'brands_translates';

    protected $fillable = [
        'slug',
        'name',
        'content',
        'h1',
        'title',
        'keywords',
        'description',
        'row_id',
        'language',
        'search_keywords',
    ];

}
