<?php

namespace App\Modules\Brands\Models;

use App\Core\Modules\Images\Models\Image;
use App\Modules\Brands\Filters\BrandsFilter;
use App\Modules\Brands\Images\BrandImage;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Traits\ActiveScopeTrait;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use Eloquent;
use EloquentFilter\Filterable;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Modules\Brands\Models\Brand
 *
 * @property int $id
 * @property bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property bool $show_engine_features_in_filter
 * @property bool $show_product_features_in_filter
 * @property null|int $relevance
 * @property-read Collection|Image[] $allImages
 * @property-read BrandTranslates $current
 * @property-read Collection|BrandTranslates[] $data
 * @property-read string $link
 * @property-read string $link_in_admin_panel
 * @property-read Collection|ProductGroup[] $groups
 * @property-read Image $image
 * @property-read Collection|Image[] $images
 * @property-read Collection|Product[] $products
 * @method static Builder|Brand active($active = true)
 * @method static Builder|Brand filter($input = array(), $filter = null)
 * @method static Builder|Brand newModelQuery()
 * @method static Builder|Brand newQuery()
 * @method static Builder|Brand paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Brand query()
 * @method static Builder|Brand simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Brand whereActive($value)
 * @method static Builder|Brand whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|Brand whereCreatedAt($value)
 * @method static Builder|Brand whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|Brand whereId($value)
 * @method static Builder|Brand whereLike($column, $value, $boolean = 'and')
 * @method static Builder|Brand whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read int|null $all_images_count
 * @property-read int|null $data_count
 * @property-read int|null $groups_count
 * @property-read int|null $images_count
 *
 * @see \App\Modules\Products\Services\ProductSearchServiceInterface::getBrandStatsByRequest()
 * @property int|null $products_count
 *
 */
class Brand extends Model
{
    use ModelMain, Imageable, EloquentTentacle, ActiveScopeTrait, Filterable;

    const DEFAULT_LIMIT_IN_ADMIN_PANEL = 10;

    protected $casts = ['active' => 'boolean'];

    protected $fillable = [
        'active',
        'show_engine_features_in_filter',
        'show_product_features_in_filter',
        'relevance',
    ];

    /**
     * Returns list of brands to show in admin panel
     *
     * @return LengthAwarePaginator
     */
    public static function getList()
    {
        return Brand::with('current')
            ->filter(request()->only('name', 'active'))
            ->latest('id')
            ->paginate(config('db.brands.per-page', 10));
    }

    /**
     * Returns all active brands list
     *
     * @return Brand[]|Builder[]|Collection
     */
    public static function allActive()
    {
        return Brand::with('current', 'image', 'image.current')->active(true)->latest('id')->get();
    }

    public static function allActiveForFilter()
    {
        return Brand::with('current')
            ->active(true)
            ->latest('id')
            ->get();
    }

    /**
     * Register filter model
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(BrandsFilter::class);
    }

    /**
     * Link in brand page in admin panel
     *
     * @return string
     */
    public function getLinkInAdminPanelAttribute(): string
    {
        return route('admin.brands.edit', $this->id);
    }

    /**
     * Returns URl on brand landing page
     *
     * @return string
     */
    public function getLinkAttribute(): string
    {
        return route('site.brands.show', [$this->current->slug]);
    }

    public function groups()
    {
        return $this->hasMany(ProductGroup::class, 'brand_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'brand_id', 'id');
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function getPagesLinksByIdForImage(int $id)
    {
        $links = [];
        $item = Brand::active()->find($id);
        if ($item) {
            $links[] = url(route('site.brands.show', ['slug' => $item->current->slug], false), [], isSecure());
        }
        return $links;
    }

    public function getSlug()
    {
        return $this->slug ?? $this->current->slug;
    }

    public function getName()
    {
        return $this->name ?? $this->current->name;
    }

    public function isShowEngineFeaturesInFilter(): bool
    {
        return (bool)$this->show_engine_features_in_filter;
    }

    public function isShowProductFeaturesInFilter(): bool
    {
        return (bool)$this->show_product_features_in_filter;
    }

    public function getRelevance(): int
    {
        return $this->relevance ?? 0;
    }

    /**
     * Product image configurations class
     *
     * @return string
     */
    protected function imageClass()
    {
        return BrandImage::class;
    }

}
