<?php

namespace App\Modules\Brands\Resources;

use App\Modules\Brands\Models\Brand;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BrandSimpleResource
 *
 * @package App\Modules\Brands\Resources
 *
 * @OA\Schema(
 *   schema="BrandSimple",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "name", "link"},
 *           @OA\Property(property="id", type="integer", description="Brand id"),
 *           @OA\Property(property="name", type="string", description="Brand name"),
 *           @OA\Property(property="link", type="string", description="Brand link (for API request)"),
 *           @OA\Property(property="image", type="string", description="Image in chosen size (original by default)"),
 *       )
 *   }
 * )
 */
class BrandSimpleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Brand $brand */
        $brand = $this->resource;
        
        return [
            'id' => $brand->id,
            'name' => $brand->current->name,
            'link' => route('api.brands.show', $brand->id),
            'image' => $brand->image->link(request()->query('imageSize', 'original')),
        ];
    }
}
