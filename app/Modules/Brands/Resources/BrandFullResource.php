<?php

namespace App\Modules\Brands\Resources;

use App\Modules\Brands\Models\Brand;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BrandFullResource
 *
 * @package App\Modules\Brands\Resources
 *
 * @OA\Schema(
 *   schema="BrandFullInformation",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "active", "data"},
 *           @OA\Property(property="id", type="integer", description="Brand id"),
 *           @OA\Property(property="active", type="boolean", description="Brand activity"),
 *           @OA\Property(property="image", type="string", description="Link to original image"),
 *           @OA\Property(
 *              property="data",
 *              type="array",
 *              description="Multilanguage data",
 *              @OA\Items(
 *                  type="object",
 *                  allOf={
 *                      @OA\Schema(
 *                          required={"language", "name", "slug"},
 *                          @OA\Property(property="language", type="string", description="Language to store"),
 *                          @OA\Property(property="name", type="string", description="Brand name"),
 *                          @OA\Property(property="slug", type="string", description="Category slug"),
 *                          @OA\Property(property="content", type="string", description="Brand full content"),
 *                          @OA\Property(property="h1", type="string", description="Brand meta h1"),
 *                          @OA\Property(property="title", type="string", description="Brand meta title"),
 *                          @OA\Property(property="description", type="string", description="Brand meta description"),
 *                          @OA\Property(property="keywords", type="string", description="Brand meta keywords"),
 *                      )
 *                  }
 *              )
 *           ),
 *       )
 *   }
 * )
 */
class BrandFullResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Brand $brand */
        $brand = $this->resource;
        $response = $brand->toArray();
        
        unset($response['image']);
        $response['image'] = $brand->image->link('original', false);
        
        return $response;
    }
}
