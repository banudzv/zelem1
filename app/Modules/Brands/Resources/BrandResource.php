<?php

namespace App\Modules\Brands\Resources;

use App\Modules\Brands\Models\Brand;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BrandResource
 *
 * @package App\Modules\Brands\Resources
 *
 * @OA\Schema(
 *   schema="Brand",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "name", "link"},
 *           @OA\Property(property="id", type="integer", description="Brand id"),
 *           @OA\Property(property="name", type="string", description="Brand name"),
 *           @OA\Property(property="link", type="string", description="Brand link (for API request)"),
 *           @OA\Property(property="content", type="string", description="Brand full content"),
 *           @OA\Property(property="h1", type="string", description="Brand meta h1"),
 *           @OA\Property(property="title", type="string", description="Brand meta title"),
 *           @OA\Property(property="description", type="string", description="Brand meta description"),
 *           @OA\Property(property="keywords", type="string", description="Brand meta keywords"),
 *           @OA\Property(property="image", type="string", description="Image in chosen size (original by default)"),
 *       )
 *   }
 * )
 */
class BrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Brand $brand */
        $brand = $this->resource;
        
        return [
            'id' => $brand->id,
            'name' => $brand->current->name,
            'link' => route('api.brands.show', $brand->id),
            'content' => $brand->current->content,
            'h1' => $brand->current->h1,
            'title' => $brand->current->title,
            'keywords' => $brand->current->keywords,
            'description' => $brand->current->description,
    
            'image' => $brand->image->link(request()->query('imageSize', 'original')),
        ];
    }
}
