<?php

namespace App\Modules\Brands\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Brands\Models\Brand;
use App\Modules\Brands\Models\BrandTranslates;
use App\Rules\MultilangSlug;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;

    public function authorize(): bool
    {
        return true;
    }

    /**
     * @throws WrongParametersException
     */
    public function rules(): array
    {
        /** @var Brand $brand */
        $brand = $this->route('brand');
        
        return $this->generateRules([
            'active' => ['required', 'bool'],
        ], [
            'name' => ['required', 'max:191'],
            'slug' => ['required', new MultilangSlug(
                (new BrandTranslates())->getTable(),
                null,
                $brand->id ?? null
            )],
            'search_keywords' => ['nullable', 'string', 'max:2048'],
        ]);
    }
    
}
