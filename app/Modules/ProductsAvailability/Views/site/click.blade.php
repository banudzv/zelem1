<button class="button button--theme-main button--buy button--radius-bottom-left button--size-large button--width-full js-init" data-mfp="ajax" data-mfp-src="{{ $url }}">
    <span class="button__body">
        {!! SiteHelpers\SvgSpritemap::get('icon-megaphone', [
            'class' => 'button__icon button__icon--after'
        ]) !!}
        <span class="button__text button__text--double">{{ __('products-availability::site.button') }}</span>
    </span>
</button>
