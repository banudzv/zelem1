@php
/** @var \App\Modules\Import\Models\EngineNumbersImport[] $imports */
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['files' => true]) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop

@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <td>Дата старта</td>
                        <td>Дата финиша</td>
                        <td>Статус</td>
                        <td>Ошибок</td>
                        <td></td>
                    </tr>
                    @foreach($imports AS $import)
                        <tr>
                            <td>{{ $import->created_at->format('Y-m-d H:i:s') }}</td>
                            <td>{!! $import->finished_at ? $import->finished_at->format('Y-m-d H:i:s') : '&mdash;' !!}</td>
                            <td>@lang("engine::status.{$import->status}")</td>
                            <td>{{ $import->errors->count() }}</td>
                            <td>
                                {!! \App\Components\Buttons::view('admin.en-import.show', $import->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $imports->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
