@php
/** @var \App\Modules\Import\Models\EngineNumbersImport $import */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-md-12">
        <div class="callout callout-info">
            @if($import->finished_at)
                <h4>Импорт успешно завершен!</h4>
            @else
                <h4>Импорт находится в процессе работы!</h4>
            @endif
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Старт</span>
                <span class="info-box-number">{{ $import->created_at->format('Y.m.d H:i') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-info"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Финиш</span>
                <span class="info-box-number">{!! $import->finished_at ? $import->finished_at->format('Y.m.d H:i') : '&mdash;' !!}</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-warning"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Ошибок</span>
                <span class="info-box-number">{{ $import->errors->count() }}</span>
            </div>
        </div>
    </div>
    @if($import->errors->isNotEmpty())
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Описание ошибки</th>
                <th>Оригинальная ошибка</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($import->errors as $error)
                <tr>
                    <td>{{ $error->description }}</td>
                    <td>{{ $error->error }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @endif
@endsection
