<?php

namespace App\Modules\Import\Forms;

use App\Core\Interfaces\FormInterface;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use CustomForm\Text;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ImportForm
 *
 * @package App\Core\Modules\Import\Forms
 */
class ImportForm implements FormInterface
{
    /**
     * @param  Model|null $model
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $model = null): Form
    {
        $form = Form::create();
        // Field set with languages tabs
        $form->fieldSet()->add(
            Input::create('import')->setLabel('Файл импорта .xlsx')
                ->setType('file')->setOptions(['accept' => '.xlsx']),
            Select::create('categories')
                ->add([
                    'none' => 'Ничего не делать',
                    'just-update' => 'Только обновить существующие',
                    'only-new' => 'Только создать новые',
                    'all' => 'И обновить сущесвующие и создать новые категории',
                ])
                ->setLabel('Категории')->setDefaultValue('just-update')->required(),
            Select::create('products')
                ->add([
                    'none' => 'Ничего не делать',
                    'just-update' => 'Только обновить существующие',
                    'only-new' => 'Только создать новые',
                    'all' => 'И обновить сущесвующие и создать новые товары',
                ])
                ->setLabel('Товары')
                ->setDefaultValue('just-update')
                ->required(),
            Select::create('images')
                ->add([
                    'none' => 'Ничего не делать',
                    'rewrite' => 'Удалить старые и загрузить новые',
                    'add' => 'Не удалять старые и догрузить из прайса как новые',
                ])
                ->setHelp('Примечание: Загрузка изображений сильно увеличивает время обработки прайса')
                ->setLabel('Изображения')->setDefaultValue('none')
                ->required(),
            Text::create()->setDefaultValue(\Widget::show('import::courses'))
        );
        $form->buttons->doNotShowSaveAndAddButton();
        $form->buttons->doNotShowSaveAndCloseButton();
        $form->doNotShowTopButtons();

        return $form;
    }
}
