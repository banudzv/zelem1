<?php

namespace App\Modules\Import\Forms;

use App\Core\Interfaces\FormInterface;
use CustomForm\Builder\Form;
use CustomForm\Input;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EngineNumbersImportForm
 *
 * @package App\Core\Modules\Import\Forms
 */
class EngineNumbersImportForm implements FormInterface
{
    /**
     * @param  Model|null $model
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $model = null): Form
    {
        $form = Form::create();
        // Field set with languages tabs
        $form->fieldSet()->add(
            Input::create('import')
                ->setLabel('Файл импорта .xlsx')
                ->setType('file')
                ->setOptions(['accept' => '.xlsx'])
        );
        $form->buttons->doNotShowSaveAndAddButton();
        $form->buttons->doNotShowSaveAndCloseButton();
        $form->doNotShowTopButtons();
        
        return $form;
    }
}
