<?php

namespace App\Modules\Import\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Modules\Import\Models\EngineNumbersImport
 *
 * @package App\Modules\Import\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $finished_at
 * @property string $description
 * @property string $status
 * @property-read EngineNumbersImportDetail[]|Collection $errors
 * @method static Builder|Import newModelQuery()
 * @method static Builder|Import newQuery()
 * @method static Builder|Import query()
 * @method static Builder|Import whereCreatedAt($value)
 * @method static Builder|Import whereId($value)
 * @method static Builder|Import whereStatus($value)
 * @method static Builder|Import whereUpdatedAt($value)
 * @method static Builder|Import whereFinishedAt($value)
 * @method static Builder|Import whereDescription($value)
 * @mixin \Eloquent
 * @property-read int|null $details_count
 */
class EngineNumbersImport extends Model
{
    const STATUS_IN_QUEUE = 'in_queue';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_FINISHED = 'finished';

    /**
     * {@inheritDoc}
     */
    protected $table = 'car_engine_numbers_imports';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['description', 'finished_at', 'status'];
    /**
     * {@inheritDoc}
     */
    protected $dates = ['finished_at'];

    /**
     * Relation to the specific import details.
     *
     * @return HasMany
     */
    public function errors()
    {
        return $this->hasMany(EngineNumbersImportDetail::class, 'import_id', 'id');
    }
}
