<?php

namespace App\Modules\Import\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Modules\Engine\Models\EngineNumbersImportDetail
 *
 * @property int $id
 * @property int $import_id
 * @property string|null $description Error description.
 * @property string|null $error Original error description.
 * @property string|null $trace Error stack trace.
 * @property-read EngineNumbersImport $import
 * @method static Builder|EngineNumbersImportDetail newModelQuery()
 * @method static Builder|EngineNumbersImportDetail newQuery()
 * @method static Builder|EngineNumbersImportDetail query()
 * @method static Builder|EngineNumbersImportDetail whereImportId($value)
 * @method static Builder|EngineNumbersImportDetail whereDescription($value)
 * @method static Builder|EngineNumbersImportDetail whereError($value)
 * @method static Builder|EngineNumbersImportDetail whereTrace($value)
 * @method static Builder|EngineNumbersImportDetail whereId($value)
 * @mixin \Eloquent
 */
class EngineNumbersImportDetail extends Model
{
    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'car_engine_numbers_imports_details';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['import_id', 'description', 'error', 'trace'];

    /**
     * Relation to the main import row.
     *
     * @return BelongsTo
     */
    public function import()
    {
        return $this->belongsTo(EngineNumbersImport::class, 'import_id', 'id');
    }
}
