<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportEngineNumbersDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_engine_numbers_imports_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unsigned();
            $table->text('error')->nullable()->comment('Original error description.');
            $table->text('trace')->nullable()->comment('Error stack trace.');
            $table->text('description')->nullable()->comment('Error description.');
            $table->foreign('import_id')->on('car_engine_numbers_imports')->references('id')
                ->index('cenid_import_id_ceni_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_engine_numbers_imports_details', function (Blueprint $table) {
            $table->dropForeign('cenid_import_id_ceni_id');
        });
        Schema::drop('car_engine_numbers_imports_details');
    }
}
