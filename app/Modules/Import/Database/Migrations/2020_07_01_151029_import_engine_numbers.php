<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\Import\Models\EngineNumbersImport;

class ImportEngineNumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_engine_numbers_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('finished_at')->nullable();
            $table->string('status')->default(EngineNumbersImport::STATUS_IN_QUEUE);
            $table->text('description')->nullable()->comment('Error description.');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('car_engine_numbers_imports');
    }
}
