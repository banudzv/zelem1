<?php

use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:import'])->group(function () {
    Route::get('car/import', [
        'uses' => 'EngineNumbersController@index',
        'as' => 'admin.en-import.index',
    ]);
    Route::get('car/import/{import}', [
        'uses' => 'EngineNumbersController@show',
        'as' => 'admin.en-import.show',
    ]);
    Route::post('car/import', 'EngineNumbersController@store');

    Route::get('catalog/import/check-status', [
        'uses' => 'IndexController@status',
        'as' => 'admin.import.check-status',
    ]);
    Route::get('catalog/import', [
        'uses' => 'IndexController@index',
        'as' => 'admin.import.index',
    ]);
    Route::post('catalog/import', [
        'uses' => 'IndexController@store',
        'as' => 'admin.import.store',
    ]);
    Route::get('catalog/import/destroy', [
        'uses' => 'IndexController@destroy',
        'as' => 'admin.import.destroy',
    ]);
    Route::get('catalog/import/download', [
        'uses' => 'IndexController@download',
        'as' => 'admin.import.download'
    ]);
});
