<?php

use Illuminate\Support\Facades\Route;
use \App\Modules\Import\Middleware\Verify1cToken;

Route::middleware(Verify1cToken::class)->group(function () {
    Route::post('1c-import/{token}', [
        'as' => 'site.ones.import',
        'uses' => 'ErpController'
    ]);
});
