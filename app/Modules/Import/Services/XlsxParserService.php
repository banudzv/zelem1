<?php

namespace App\Modules\Import\Services;

/**
 * Class XlsxParserService
 * @package App\Modules\Import\Services
 */
class XlsxParserService
{
    /**
     * @var string Path to the .xlsx file.
     */
    protected $path;
    /**
     * @var string Path to the folder where unzipped file will live.
     */
    protected $tmp;
    /**
     * @var string[] Alphabet for column cells identification.
     */
    protected $alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
                           'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    /**
     * XlsxParserService constructor.
     *
     * @param string $file
     */
    public function __construct(string $file)
    {
        $this->path = storage_path('app/public/' . $file);
        $this->tmp = storage_path('app/tmp/' . date('Y_m_d_H_i_s') . '/');
    }

    /**
     * Receives raw data from the .xlsx file and returns it.
     *
     * @return array
     */
    public function start(): array
    {
        $this->unzip();
        $parsedDocument = $this->parse();
        $this->deleteTmpFolder();

        return $parsedDocument;
    }

    /**
     * Unzip *.xlsx file to the tmp folder.
     *
     * @return bool
     */
    final protected function unzip(): bool
    {
        $zip = new \ZipArchive();
        $res = $zip->open($this->path);
        if ($res === true) {
            $zip->extractTo($this->tmp);
            $zip->close();

            return true;
        }

        return false;
    }

    /**
     * Parses file with the shared strings.
     * Returns it back.
     *
     * @return array
     */
    private function parseSharedStrings(): array
    {
        $sharedStrings = [];
        $xml = $this->loadXml($this->tmp . 'xl/sharedStrings.xml');
        foreach ($xml->children() as $item) {
            $sharedStrings[] = (string)$item->t;
        }

        return $sharedStrings;
    }

    /**
     * The main method to parse the whole document
     *
     * @return array
     */
    private function parse(): array
    {
        $sharedStrings = $this->parseSharedStrings();
        $dir = $this->tmp . 'xl/worksheets';
        $handle = @opendir($dir);
        $parsedDocument = [];
        while ($file = @readdir($handle)) {
            $path = $dir . '/' . $file;
            if ($file === '.' || $file === '..' || is_dir($path)) {
                continue;
            }
            $parsedDocument[$this->parseWorksheetTitle($file)] = $this->parseSheet($path, $sharedStrings);
        }

        return $parsedDocument;
    }

    /**
     * Parses sheet title by sheet name.
     *
     * @param string $filename
     * @return string
     */
    private function parseWorksheetTitle(string $filename): string
    {
        $sheetId = (int)substr($filename, 5, 1);
        $xml = $this->loadXml($this->tmp . 'xl/workbook.xml');
        foreach ($xml->sheets->sheet as $sheet) {
            $attrs = $sheet->attributes();
            if ($sheetId === (int)$attrs['sheetId']) {
                return (string)$attrs['name'];
            }
        }

        return 'unknown';
    }

    /**
     * Returns data in array based on received xml
     *
     * @param string $path Path to the file to parse.
     * @param array $sharedStrings
     * @return array
     */
    private function parseSheet(string $path, array $sharedStrings): array
    {
        $xml = $this->loadXml($path);
        $data = [];
        foreach ($xml->sheetData->row as $row) {
            if (count($row) === 0) {
                break;
            }
            $rowHasValues = false;
            $datum = [];
            foreach ($row as $cell) {
                $valueIndex = isset($cell->v) ? (string)$cell->v : null;
                $cellIndex = (string)$cell->attributes()['r'];
                $cellLetter = preg_replace('/[0-9]*/', '', $cellIndex);
                if (isset($cell->attributes()['t']) && isset($sharedStrings[$valueIndex])) {
                    $datum[$cellLetter] = $sharedStrings[$valueIndex];
                    $rowHasValues = true;
                } elseif ($valueIndex) {
                    $datum[$cellLetter] = $valueIndex;
                    $rowHasValues = true;
                } else {
                    $datum[$cellLetter] = null;
                }
            }
            if ($rowHasValues === true) {
                $data[] = $datum;
            }
        }
        $parsed = [];
        if (count($data) > 0) {
            $columns = array_shift($data);
            foreach ($data as $datum) {
                $parsed[] = $this->parseRow($columns, $datum);
            }
        }

        return $parsed;
    }

    /**
     * Parses row.
     *
     * @param array $columns
     * @param array $row
     * @return array
     */
    private function parseRow(array $columns, array $row): array
    {
        $element = [];
        foreach ($columns as $index => $column) {
            if (!$column) {
                continue;
            }
            if (isset($element[$column])) {
                $element[$column] = is_array($element[$column]) ? $element[$column] : [$element[$column]];
                $element[$column][] = $row[$index] ?? null;
            } else {
                $element[$column] = $row[$index] ?? null;
            }
        }

        return $element;
    }

    /**
     * Recursively deletes folder with all files inside
     *
     * @param string|null $folder
     */
    private function deleteTmpFolder(?string $folder = null): void
    {
        $folder = $folder ?? $this->tmp;
        $folder = rtrim($folder, '/');
        if (!is_dir($folder)) {
            return;
        }
        foreach (scandir($folder) as $object) {
            if ($object === '.' || $object === '..') {
                continue;
            }
            $objectPath = $folder . '/' . $object;
            if (is_dir($objectPath) && !is_link($objectPath)) {
                $this->deleteTmpFolder($objectPath);
            } else {
                @unlink($objectPath);
            }
        }
        rmdir($folder);
    }

    private function loadXml(string $path): \SimpleXMLElement
    {
        return simplexml_load_file($path);
    }
}
