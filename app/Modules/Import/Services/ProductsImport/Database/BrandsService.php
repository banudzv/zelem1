<?php

namespace App\Modules\Import\Services\ProductsImport\Database;

use App\Modules\Brands\Models\Brand;
use App\Modules\Brands\Models\BrandTranslates;

/**
 * Class BrandsService
 * @package App\Modules\Import\Services\Database
 */
class BrandsService extends DatabaseServiceAbstraction
{
    public function make(array $data, array $products = []): array
    {
        list($brands, $brandsSlugs) = $this->getBrandsNamesAndSlugsArrayFromTheDatabase();
        foreach ($data as $datum) {
            if (isset($datum['Brand']) === false || isset($brands[$datum['Brand']])) {
                continue;
            }
            $brands[$datum['Brand']] = $this->createBrand($datum['Brand'], $brandsSlugs);
        }

        return $brands;
    }

    protected function getBrandsNamesAndSlugsArrayFromTheDatabase(): array
    {
        $brands = [];
        $brandsSlugs = [];
        BrandTranslates::all(['row_id', 'name', 'language', 'slug'])
            ->each(function (BrandTranslates $brand) use (&$brands, &$brandsSlugs) {
                $brands[$brand->name] = $brand->row_id;
                $brandsSlugs[$brand->language] = $brandsSlugs[$brand->language] ?? [];
                $brandsSlugs[$brand->language][] = $brand->slug;
            });

        return [$brands, $brandsSlugs];
    }

    protected function createBrand(string $name, array &$brandsSlugs): int
    {
        $brandId = Brand::create(['active' => true])->id;
        foreach ($this->languages as $lang => $language) {
            $brandsSlugs[$lang] = $brandsSlugs[$lang] ?? [];
            $slug = $this->createSlug($brandsSlugs[$lang], $name);
            $brandsSlugs[$lang][] = $slug;
            BrandTranslates::create([
                'row_id' => $brandId,
                'language' => $lang,
                'name' => $name,
                'slug' => $slug,
            ]);
        }

        return $brandId;
    }
}
