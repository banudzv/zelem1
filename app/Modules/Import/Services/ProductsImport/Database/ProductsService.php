<?php

namespace App\Modules\Import\Services\ProductsImport\Database;

use App\Modules\Engine\Models\Year;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductEngineNumber;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupCategory;
use App\Modules\Products\Models\ProductGroupFeatureValue;
use App\Modules\Products\Models\ProductGroupTranslates;
use App\Modules\Products\Models\ProductTranslates;

/**
 * Class ProductsService
 * @package App\Modules\Import\Services\Database
 */
class ProductsService extends DatabaseServiceAbstraction
{
    /**
     * @var array Categories list [number => categoryId]
     */
    protected $categories;
    /**
     * @var array Brands list [brandName => brandId]
     */
    protected $brands;
    /**
     * @var array Features list [featureName => featureId]
     */
    protected $features;
    /**
     * @var array Values list [featureId => [valueName => valueId]]
     */
    protected $values;
    /**
     * Little piece of information about the existed in DB groups.
     * Example:
     *      [groupId => [price_min, rice_max, available]]
     *
     * @var array
     */
    protected $groups;
    /**
     * @var array Categories tree in the format [id => parent_id]
     */
    protected $tree;
    /**
     * @var bool Should script create a new products
     */
    protected $create;
    /**
     * @var bool Should script update old products
     */
    protected $update;

    /**
     * ProductsService constructor.
     *
     * @param array $categories
     * @param array $brands
     * @param array $features
     * @param array $values
     * @param array $groups
     * @param array $catsTree
     * @param bool $create
     * @param bool $update
     */
    public function __construct(array $categories, array $brands, array $features, array $values, array $groups,
                                array $catsTree, bool $create, bool $update)
    {
        parent::__construct();

        $this->categories = $categories;
        $this->brands = $brands;
        $this->features = $features;
        $this->values = $values;
        $this->groups = $groups;
        $this->tree = $catsTree;
        $this->create = $create;
        $this->update = $update;
    }

    /**
     * Creates/updates products and their groups in the database.
     *
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function make(array $data)
    {
        $images = [];
        $slugs = $this->getExistedProductsSlugs();
        foreach ($data as $datum) {
            $featureId = $this->getFeatureId($datum['MainFeature'] ?? null);
            if (is_null($datum['id'])) {
                if (!$this->create || !$datum['Products']) {
                    continue;
                }
                $productImages = $this->createProductEcosystem($datum, $featureId, $slugs);
            } elseif($this->update) {
                $productImages = $this->updateProductEcosystem($datum, $featureId, $slugs);
            } else {
                $productImages = $this->justGetImages($datum);
            }
            if (isset($productImages) && is_array($productImages) && count($productImages) > 0) {
                $images += $productImages;
            }
        }

        return $images;
    }

    /**
     * Creates a new product: group + links products here.
     *
     * @param array $datum Product group full parsed data
     * @param int|null $featureId Main feature ID of the group.
     * @param array $slugs Products slugs
     * @return null|array With images
     * @throws \Exception
     */
    protected function createProductEcosystem(array $datum, ?int $featureId, array &$slugs): ?array
    {
        if (!$datum['Products']) {
            return null;
        }
        $categoryId = $this->getCategoryId($datum['category_id'], $datum['category_number']);
        $groupId = $this->createProductGroup($datum, $categoryId, $featureId);
        $productIds = $separateValues = $images = [];
        foreach ($datum['Products'] as $index => $product) {
            $valueId = $this->getValueId($featureId, $datum['MainFeatureValue'] ?? null);
            $productId = $this->createProduct($datum, $valueId, $product, $slugs, $index === 0);
            $productIds[] = $productId;
            $separateValues[$productId] = $valueId;
            $images[$productId] = $product['Images'];
        }
        $this->updateEngineNumbers($groupId, $datum['EngineNumbers']);
        $this->syncWithYears($groupId, $datum['Years'] ?? []);
//        $this->syncWithCategories($groupId, $categoryId);
        $this->syncWithFeatures($groupId, $productIds, $datum['Features'], $featureId, $separateValues);

        return $images;
    }

    /**
     * Updates the existed product group and creates/updates inner products.
     *
     * @param array $datum Product group full parsed data
     * @param int|null $featureId Main feature ID of the group.
     * @param array $slugs Products slugs
     * @return array With images
     * @throws \Exception
     */
    protected function updateProductEcosystem(array $datum, ?int $featureId, array &$slugs): array
    {
        $categoryId = $this->getCategoryId($datum['category_id'], $datum['category_number']);
        $groupId = $this->updateProductGroup($datum, $categoryId, $featureId);
        $productIds = $separateValues = $images = [];
        foreach ($datum['Products'] as $product) {
            $valueId = $this->getValueId($featureId, $datum['MainFeatureValue'] ?? null);
            if (is_null($product['id'])) {
                $productId = $this->createProduct($datum, $valueId, $product, $slugs);
            } else {
                $productId = $this->updateProduct($product, $valueId);
            }
            $productIds[] = $productId;
            $separateValues[$productId] = $valueId;
            $images[$productId] = $product['Images'];
        }
        $this->updateEngineNumbers($groupId, $datum['EngineNumbers']);
        $this->syncWithYears($groupId, $datum['Years'] ?? []);
//        $this->syncWithCategories($groupId, $categoryId);
        $this->syncWithFeatures($groupId, $productIds, $datum['Features'], $featureId, $separateValues);

        return $images;
    }

    /**
     * Just returns an array of images.
     *
     * @param array $datum
     * @return array
     */
    protected function justGetImages(array $datum): array
    {
        $images = [];
        foreach ($datum['Products'] as $product) {
            if (is_null($product['id']) || !$product['Images']) {
                continue;
            }
            $images[$product['id']] = $product['Images'];
        }

        return $images;
    }

    /**
     * Updates the relation of the products to the engine numbers list.
     *
     * @param int $groupId
     * @param array $engineNumbers
     * @throws \Exception
     */
    protected function updateEngineNumbers(int $groupId, array $engineNumbers): void
    {
        ProductEngineNumber::whereGroupId($groupId)->whereNotIn('engine_number', $engineNumbers)->delete();
        $existedEngineNumbers = ProductEngineNumber::whereGroupId($groupId)->pluck('engine_number')->toArray();
        foreach (array_diff($engineNumbers, $existedEngineNumbers) as $engineNumber) {
            if (!$engineNumber) {
                continue;
            }
            ProductEngineNumber::create(['engine_number' => $engineNumber, 'group_id' => $groupId]);
        }
    }

    /**
     * Syncs product group with the years list.
     *
     * @param int $groupId
     * @param array $years
     * @throws \Exception
     */
    protected function syncWithYears(int $groupId, array $years): void
    {
        Year::whereGroupId($groupId)->whereNotIn('year', $years)->delete();
        $existedYears = Year::whereGroupId($groupId)->pluck('year')->toArray();
        foreach (array_diff($years, $existedYears) as $year) {
            if (!$years) {
                continue;
            }
            Year::create(['year' => $year, 'group_id' => $groupId]);
        }
    }

    /**
     * Syncs product group to the categories list.
     *
     * @param int $groupId
     * @param int $categoryId
     * @throws \Exception
     */
    protected function syncWithCategories(int $groupId, int $categoryId): void
    {
        $categoryIds = array_unique($this->getInnerCategoriesTreeFor($categoryId));
        ProductGroupCategory::whereGroupId($groupId)->whereNotIn('category_id', $categoryIds)->delete();
        $linkedCategoriesIds = ProductGroupCategory::whereGroupId($groupId)->pluck('category_id')->toArray();
        foreach (array_diff($categoryIds, $linkedCategoriesIds) as $categoryId) {
            ProductGroupCategory::create(['category_id' => $categoryId, 'group_id' => $groupId]);
        }
    }

    /**
     * Returns all child categories IDs for a chosen one plus itself.
     *
     * @param int $categoryId
     * @return array
     */
    protected function getInnerCategoriesTreeFor(int $categoryId): array
    {
        $children = array_filter($this->tree, function ($parentId) use ($categoryId) {
            return $parentId === $categoryId;
        });
        $categoryIds = array_keys($children);
        $categoryIds[] = $categoryId;
        foreach ($children as $childCategoryId) {
            $categoryIds = array_merge($categoryIds, $this->getInnerCategoriesTreeFor($childCategoryId));
        }

        return $categoryIds;
    }

    /**
     * Links products with features.
     *
     * @param int $groupId
     * @param array $productIds
     * @param array $features
     * @param int|null $featureId
     * @param array $separateValues
     * @throws \Exception
     */
    protected function syncWithFeatures(int $groupId, array $productIds, array $features, ?int $featureId, array $separateValues): void
    {
        if ($featureId) {
            foreach ($separateValues as $productId => $valueId) {
                if (is_null($valueId)) {
                    continue;
                }

                $this->linkFeatureValueToProduct($groupId, $productId, $featureId, $valueId);
            }
        }
        foreach ($features as $featureName => $valueNames) {
            foreach ($valueNames as $valueName) {
                foreach ($productIds as $productId) {
                    if (!$featureId = $this->getFeatureId($featureName)) {
                        continue;
                    }
                    if (!$valueId = $this->getValueId($featureId, $valueName)) {
                        continue;
                    }
                    $this->linkFeatureValueToProduct($groupId, $productId, $featureId, $valueId);
                }
            }
        }
    }

    /**
     * Links product to value.
     *
     * @param int $groupId
     * @param int $productId
     * @param int $featureId
     * @param int $valueId
     * @throws \Exception
     */
    protected function linkFeatureValueToProduct(int $groupId, int $productId, int $featureId, int $valueId): void
    {
        ProductGroupFeatureValue::updateOrCreate([
            'group_id' => $groupId, 'feature_id' => $featureId,
            'value_id' => $valueId, 'product_id' => $productId,
        ]);
        ProductGroupFeatureValue::whereGroupId($groupId)
            ->whereFeatureId($featureId)->whereValueId($valueId)
            ->where('product_id', '!=', $productId)->delete();
    }

    /**
     * Creates product instance inside the group.
     *
     * @param int|null $valueId
     * @param array $datum
     * @return int
     */
    protected function updateProduct(array $datum, ?int $valueId): int
    {
        Product::whereId($datum['id'])->update([
            'active' => true,
            'price' => $datum['price'],
            'old_price' => $datum['old_price'],
            'available' => $datum['available'],
            'vendor_code' => $datum['vendor_code'],
            'value_id' => $valueId,
        ]);
        foreach ($this->languages as $lang => $language) {
            ProductTranslates::whereRowId($datum['id'])->whereLanguage($lang)->update($datum[$lang]);
        }

        return $datum['id'];
    }

    /**
     * Creates product instance and links it to the product group.
     *
     * @param array $group
     * @param int|null $valueId
     * @param array $datum
     * @param array $slugs
     * @param bool $isMain
     * @return int|null
     * @throws \Exception
     */
    protected function createProduct(array $group, ?int $valueId, array $datum, array &$slugs, bool $isMain = false): int
    {
        # Creates product
        $productId = Product::create([
            'active' => true,
            'price' => $datum['price'],
            'old_price' => $datum['old_price'],
            'available' => $datum['available'],
            'vendor_code' => $datum['vendor_code'],
            'group_id' => $group['id'],
            'value_id' => $valueId,
            'is_main' => $isMain,
        ])->id;
        foreach ($this->languages as $lang => $language) {
            $slugs[$lang] = $slugs[$lang] ?? [];
            $slug = $this->createSlug($slugs[$lang], $datum[$lang]['name'] ?: $group[$lang]['name']);
            $slugs[$lang][] = $slug;
            ProductTranslates::create([
                'row_id' => $productId,
                'language' => $lang,
                'name' => $datum[$lang]['name'],
                'short_name' => $datum[$lang]['short_name'],
                'slug' => $slug,
            ]);
        }

        return $productId;
    }

    /**
     * Updates product group in the database with the text data on each language of the site.
     *
     * @param array $datum
     * @param int|null $featureId
     * @param int|null $categoryId
     * @return int
     */
    protected function updateProductGroup(array $datum, ?int $categoryId, ?int $featureId): int
    {
        $group = $this->groups[$datum['id']];
        list($available, $minPrice, $maxPrice) = $this->getPricesAndAvailabilityFromProducts(
            $datum['Products'],
            $group['available'] ?? true,
            $group['price_min'] ?? 0,
            $group['price_max'] ?? 0
        );
        # Updates group
        ProductGroup::whereId($datum['id'])->update([
            'brand_id' => $this->getBrandId($datum['Brand'] ?? null),
            'category_id' => $categoryId,
            'available' => $available,
            'price_min' => $minPrice,
            'price_max' => $maxPrice,
            'active' => true,
            'feature_id' => $featureId,
        ]);
        # Updates existed text data in the group instance
        foreach ($this->languages as $lang => $language) {
            ProductGroupTranslates::whereRowId($datum['id'])->whereLanguage($lang)->update($datum[$lang]);
        }

        return $datum['id'];
    }

    /**
     * Creates product group in the database with the text data on each language of the site.
     *
     * @param array $datum
     * @param int|null $featureId
     * @param int|null $categoryId
     * @return int
     */
    protected function createProductGroup(array &$datum, ?int $categoryId, ?int $featureId): int
    {
        list($available, $minPrice, $maxPrice) = $this->getPricesAndAvailabilityFromProducts($datum['Products']);
        # Creates group
        $datum['id'] = $groupId = ProductGroup::create([
            'brand_id' => $this->getBrandId($datum['Brand'] ?? null),
            'category_id' => $categoryId,
            'active' => true,
            'position' => 500,
            'available' => $available,
            'price_min' => $minPrice,
            'price_max' => $maxPrice,
            'feature_id' => $featureId,
        ])->id;
        # Links text data to the group instance
        foreach ($this->languages as $lang => $language) {
            ProductGroupTranslates::create([
                'row_id' => $groupId,
                'language' => $lang,
                'name' => $datum[$lang]['name'],
                'text' => $datum[$lang]['text'],
            ]);
        }

        return $groupId;
    }

    /**
     * Returns all slugs separated by language for the products.
     *
     * @return array
     */
    protected function getExistedProductsSlugs(): array
    {
        return ProductTranslates::all(['language', 'slug'])
            ->each(function (ProductTranslates $tr) use (&$slugs) {
                $slugs[$tr->language] = $slugs[$tr->language] ?? [];
                $slugs[$tr->language][] = $tr->slug;
            })->toArray();
    }

    /**
     * Returns availability, min price and max price for the group of products.
     *
     * @param array $products
     * @param bool $available
     * @param int $minPrice
     * @param int $maxPrice
     * @return array
     */
    protected function getPricesAndAvailabilityFromProducts(array $products, bool $available = false, int $minPrice = 0,
                                                            int $maxPrice = 0): array
    {
        foreach ($products as $product) {
            if ($product['available']) {
                $available = true;
            }
            if ($product['price']) {
                if (($minPrice === 0 && $product['price'] > 0) || $minPrice > $product['price']) {
                    $minPrice = $product['price'];
                }
                if ($maxPrice < $product['price']) {
                    $maxPrice = $product['price'];
                }
            }
        }

        return [$available, $minPrice, $maxPrice];
    }

    /**
     * Returns feature ID by its name.
     *
     * @param string|null $featureName
     * @return int|null
     */
    protected function getFeatureId(?string $featureName): ?int
    {
        return $this->features[$featureName] ?? null;
    }

    /**
     * Returns value id by its name.
     *
     * @param int|null $featureId
     * @param string|null $valueName
     * @return int|null
     */
    protected function getValueId(?int $featureId, ?string $valueName): ?int
    {
        if ($featureId && isset($this->values[$featureId])) {
            return $this->values[$featureId][$valueName] ?? null;
        }

        return null;
    }

    /**
     * Returns brand ID by its name.
     *
     * @param string|null $brandName
     * @return int|null
     */
    protected function getBrandId(?string $brandName): ?int
    {
        return $this->brands[$brandName] ?? null;
    }

    /**
     * Returns category id from the list of existed categories by number.
     * If categoryId filled the method will return it back.
     *
     * @param int|null $categoryId
     * @param string|null $categoryNumber
     * @return int|null
     */
    protected function getCategoryId(?int $categoryId, ?string $categoryNumber): ?int
    {
        return $categoryId ?? $this->categories[$categoryNumber] ?? null;
    }
}
