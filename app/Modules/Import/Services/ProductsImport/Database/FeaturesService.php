<?php

namespace App\Modules\Import\Services\ProductsImport\Database;

use App\Modules\Features\Models\Feature;
use App\Modules\Features\Models\FeatureTranslates;

/**
 * Class FeaturesService
 * @package App\Modules\Import\Services\Database
 */
class FeaturesService extends DatabaseServiceAbstraction
{
    /**
     * @var bool Default value for `active` column for a new features.
     */
    protected $defaultActiveValue = true;
    /**
     * @var bool Default value for `in_filter` column for a new features.
     */
    protected $defaultInFilterValue = false;
    /**
     * @var bool Default value for `type` column for a new features.
     */
    protected $defaultTypeValue = Feature::TYPE_MULTIPLE;

    public function make(array $data): array
    {
        list($features, $featuresSlugs) = $this->getFeaturesNamesAndSlugsArrayFromTheDatabase();
        foreach ($data as $datum) {
            if (isset($datum['MainFeature']) && $datum['MainFeature'] && !isset($features[$datum['MainFeature']])) {
                $features[$datum['MainFeature']] = $this->createFeature($datum['MainFeature'], $featuresSlugs);
            }
            foreach ($datum['Features'] ?? [] as $featureName => $valuesNames) {
                if (!$featureName || isset($features[$featureName])) {
                    continue;
                }
                $features[$featureName] = $this->createFeature($featureName, $featuresSlugs);
            }
        }

        return $features;
    }

    protected function getFeaturesNamesAndSlugsArrayFromTheDatabase(): array
    {
        $features = [];
        $featuresSlugs = [];
        FeatureTranslates::all(['row_id', 'name', 'slug', 'language'])
            ->each(function (FeatureTranslates $translate) use (&$features, &$featuresSlugs) {
                $featuresSlugs[$translate->language] = $featuresSlugs[$translate->language] ?? [];
                $featuresSlugs[$translate->language][] = $translate->slug;
                $features[$translate->name] = $translate->row_id;
            });

        return [$features, $featuresSlugs];
    }

    protected function createFeature(string $name, array &$featuresSlugs): int
    {
        $featureId = Feature::create([
            'active' => $this->defaultActiveValue,
            'in_filter' => $this->defaultInFilterValue,
            'type' => $this->defaultTypeValue,
        ])->id;
        foreach ($this->languages as $lang => $language) {
            $featuresSlugs[$lang] = $featuresSlugs[$lang] ?? [];
            $slug = $this->createSlug($featuresSlugs[$lang], $name);
            $featuresSlugs[$lang][] = $slug;
            FeatureTranslates::updateOrCreate(
                ['row_id' => $featureId, 'language' => $lang],
                ['slug' => $slug, 'name' => $name]
            );
        }

        return $featureId;
    }
}
