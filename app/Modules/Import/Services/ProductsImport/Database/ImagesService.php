<?php

namespace App\Modules\Import\Services\ProductsImport\Database;

use App\Core\Modules\Images\Models\Image;
use App\Modules\Products\Models\Product;
use Exception;
use File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Log;
use Storage;

class ImagesService extends DatabaseServiceAbstraction
{
    /**
     * @var bool Should script upload photos from the file as a new?
     */
    protected $upload;
    /**
     * @var bool Should script delete the old product photos?
     */
    protected $delete;

    /**
     * ImagesService constructor.
     *
     * @param bool $delete
     * @param bool $upload
     */
    public function __construct(bool $delete, bool $upload)
    {
        parent::__construct();

        $this->delete = $delete;
        $this->upload = $upload;
    }

    /**
     * Stores images on the disk.
     * Also adds data to the `images` table.
     *
     * @param array $images
     * @return mixed|void
     */
    public function make(array $images)
    {
        if (
            (!$this->delete && !$this->upload)
            || (!($images = array_filter($images)) && !$this->delete)
        ) {
            return;
        }
        foreach (array_chunk(array_keys($images), 100) as $productsInList) {
            Product::with('images')->whereIn('id', $productsInList)->oldest()->get()
                ->each(
                    function (Product $product) use ($images) {
                        $maxPosition = $product->images->max('position');
                        if ($this->upload) {
                            foreach ($images[$product->id] as $image) {
                                $this->uploadImage($product, $image, ++$maxPosition);
                            }
                        }
                        if ($this->delete) {
                            $product->images->each(
                                function (Image $image) {
                                    $image->deleteImage();
                                    $image->delete();
                                }
                            );
                        }
                    }
                );
        }
    }

    /**
     * @param Product $product
     * @param string $image
     * @param int $position
     */
    private function uploadImage(Product $product, string $image, int $position = 0): void
    {
        try {
            /** @var Image $existedImage */
            $imageExists = false;
            $image = trim($image);
            if (filter_var($image, FILTER_VALIDATE_URL)) {
                // Upload by URL address.
                $parsed = parse_url($image);
                if ($parsed['host'] === 'drive.google.com') {
                    list($originalName, $link) = $this->getLinkToImageOnGoogleDrive($parsed, $image);
                } else {
                    list($originalName, $link, $context, $imageExists) = $this->getLinkToImage(
                        $product,
                        $parsed,
                        $image
                    );
                }
                // Load image to tmp folder
                if ((!$imageExists || $this->delete) && $source = @file_get_contents($link, false, $context ?? null)) {
                    $tempPath = "temp/$originalName";
                    File::makeDirectory(storage_path('app/public/temp'), 0777, true, true);
                    Storage::put($tempPath, $source);
                }
            } else {
                // $image is a path inside folder storage/app/public/temp
                $image = ltrim($image, '/');
                $parsed = explode('/', $image);
                $originalName = end($parsed);
                $tempPath = "temp/$image";
            }
            // Upload image to real path and remove it from the temp path.
            if (isset($tempPath) && file_exists(storage_path("app/public/" . $tempPath))) {
                $product->uploadImageFromResource(
                    new UploadedFile(
                        storage_path("app/public/" . $tempPath),
                        $originalName
                    ),
                    null,
                    $position
                );
            }
        } catch (Exception $exception) {
            Log::error($exception);
        }
    }

    private function getLinkToImageOnGoogleDrive(array $parsed, string $image): array
    {
        $originalName = '';
        foreach (explode('/', $parsed['path']) as $el) {
            if (strlen($el) > strlen($originalName)) {
                $originalName = $el;
            }
        }
        $page = file_get_contents(trim($image));
        preg_match('/<meta property="og:image" content="([^"]*)">/', $page, $matches);
        if (count($matches) > 1) {
            $link = $matches[1];
            $lastEqualSymbol = strripos($link, '=');
            $link = substr($link, 0, $lastEqualSymbol);
        } else {
            $link = "https://drive.google.com/u/0/uc?id=$originalName&export=download";
        }

        return [$originalName, $link];
    }

    private function getLinkToImage(Product $product, array $parsed, string $image): array
    {
        $originalName = basename($image);
        if ($parsed['host'] === Arr::get($_SERVER, 'HTTP_HOST')) {
            if (config('db.basic-auth.auth') && config('db.basic-auth.username') && config('db.basic-auth.password')) {
                $context = stream_context_create(
                    [
                        'http' => [
                            'header' => 'Authorization: Basic ' . base64_encode(
                                    config('db.basic-auth.username') . ':' . config('db.basic-auth.password')
                                )
                        ]
                    ]
                );
            }
            $copy = $product->images->where('name', $originalName)->count() > 0;
        }

        return [$originalName, $image, $context ?? null, $copy ?? false];
    }
}
