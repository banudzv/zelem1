<?php

namespace App\Modules\Import\Services\ProductsImport\Database;

use App\Modules\Features\Models\FeatureValue;
use App\Modules\Features\Models\FeatureValueTranslates;

/**
 * Class ValuesService
 * @package App\Modules\Import\Services\Database
 */
class ValuesService extends DatabaseServiceAbstraction
{
    /**
     * @var bool Default value for `active` column for a new features.
     */
    protected $defaultActiveValue = true;
    /**
     * @var bool Default value for `position` column for a new features.
     */
    protected $defaultPositionValue = false;
    /**
     * @var array Features list in [name => id, name => id,..] style.
     */
    protected $features;

    /**
     * ValuesService constructor.
     *
     * @param array $features
     */
    public function __construct(array $features)
    {
        parent::__construct();

        $this->features = $features;
    }

    public function make(array $data): array
    {
        list($values, $valuesSlugs) = $this->getFeaturesValuesNamesAndSlugsArrayFromTheDatabase();
        foreach ($data as $datum) {
            foreach ($datum['Features'] ?? [] as $featureName => $valuesNames) {
                if (!$featureName || isset($this->features[$featureName]) === false) {
                    continue;
                }
                $featureId = $this->features[$featureName];
                $featureValues = $values[$featureId] ?? [];
                foreach ($valuesNames as $valueName) {
                    if (!$valueName || isset($featureValues[$valueName])) {
                        continue;
                    }
                    $featureValues[$valueName] = $this->createFeatureValue($this->features[$featureName], $valueName, $valuesSlugs);
                }
                $values[$featureId] = $featureValues;
            }
            if (!isset($datum['MainFeature']) || !isset($features[$datum['MainFeature']]) || !isset($datum['Products']) || !is_array($datum['Products'])) {
                continue;
            }
            $featureId = $this->features[$datum['MainFeature']];
            $featureValues = $values[$featureId] ?? [];
            foreach ($datum['Products'] as $product) {
                if (!isset($featureValues[$product['MainFeatureValue']])) {
                    $featureValues[$product['MainFeatureValue']] = $this->createFeatureValue(
                        $featureId,
                        $product['MainFeatureValue'],
                        $valuesSlugs
                    );
                }
            }
            $values[$featureId] = $featureValues;
        }

        return $values;
    }

    protected function getFeaturesValuesNamesAndSlugsArrayFromTheDatabase(): array
    {
        $values = [];
        $valuesSlugs = [];
        FeatureValue::with('data')->get(['id', 'feature_id'])
            ->each(function (FeatureValue $value) use (&$values, &$valuesSlugs) {
                $values[$value->feature_id] = $values[$value->feature_id] ?? [];
                $value->data->each(function (FeatureValueTranslates $translate) use (&$values, &$valuesSlugs, $value) {
                    $valuesSlugs[$translate->language] = $valuesSlugs[$translate->language] ?? [];
                    $valuesSlugs[$translate->language][] = $translate->slug;
                    $values[$value->feature_id][$translate->name] = $value->id;
                });
            });

        return [$values, $valuesSlugs];
    }

    protected function createFeatureValue(int $featureId, string $name, array &$valuesSlugs): int
    {
        $valueId = FeatureValue::create([
            'feature_id' => $featureId,
            'active' => $this->defaultActiveValue,
            'position' => $this->defaultPositionValue,
        ])->id;
        foreach ($this->languages as $lang => $language) {
            $valuesSlugs[$lang] = $valuesSlugs[$lang] ?? [];
            $slug = $this->createSlug($valuesSlugs[$lang], $name);
            $valuesSlugs[$lang][] = $slug;
            FeatureValueTranslates::updateOrCreate(
                ['row_id' => $valueId, 'language' => $lang],
                ['slug' => $slug, 'name' => $name]
            );
        }

        return $valueId;
    }
}
