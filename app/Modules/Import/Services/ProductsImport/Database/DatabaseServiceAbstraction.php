<?php

namespace App\Modules\Import\Services\ProductsImport\Database;

use App\Core\Modules\Languages\Models\Language;
use Illuminate\Support\Str;

/**
 * Class DatabaseServiceAbstraction
 * @package App\Modules\Import\Services\Database
 */
abstract class DatabaseServiceAbstraction
{
    /**
     * @var Language[]
     */
    protected $languages;

    /**
     * DatabaseServiceAbstraction constructor.
     */
    public function __construct()
    {
        $this->languages = config('languages', []);
    }

    /**
     * Stores some data to the database.
     * Returns something if it should do this.
     *
     * @param array $data
     * @return mixed
     */
    abstract public function make(array $data);

    /**
     * Creates a unique slug for the element.
     * Uses existed elements list as a base.
     *
     * @param array $slugs
     * @param string $name
     * @param string $postfix
     * @return string
     * @throws \Exception
     */
    protected function createSlug(array $slugs, string $name, string $postfix = ''): string
    {
        $name = $name ?: time();
        $slug = Str::slug($name) . $postfix;
        if (in_array($slug, $slugs)) {
            return $this->createSlug($slugs, $name, random_int(1000, 9999));
        }

        return $slug;
    }
}
