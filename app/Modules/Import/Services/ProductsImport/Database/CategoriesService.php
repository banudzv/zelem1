<?php

namespace App\Modules\Import\Services\ProductsImport\Database;

use App\Modules\Categories\Models\Category;
use App\Modules\Categories\Models\CategoryTranslates;

/**
 * Class CategoriesService
 * @package App\Modules\Import\Services\Database
 */
class CategoriesService extends DatabaseServiceAbstraction
{
    /**
     * @var bool
     */
    protected $create;
    /**
     * @var bool
     */
    protected $update;

    /**
     * CategoriesService constructor.
     *
     * @param bool $create
     * @param bool $update
     */
    public function __construct(bool $create, bool $update)
    {
        parent::__construct();

        $this->create = $create;
        $this->update = $update;
    }

    /**
     * @param array $data
     * @param array $existedData
     * @return array
     */
    public function make(array $data, array &$existedData = []): array
    {
        $slugs = $this->getExistedSlugs();
        $categories = [];
        // Import data to the database
        foreach ($data as $index => $datum) {
            if (is_null($datum['id'])) {
                if ($this->create) {
                    $data[$index]['id'] = $this->createCategory($datum, $slugs);
                    $existedData['categories'][$datum['id']] = null;
                    $categories[$datum['number']] = $data[$index]['id'];
                }
            } elseif ($this->update) {
                $this->updateCategory($datum);
            }
        }
        // Update parent_id
        foreach ($data as $index => $datum) {
            if (is_null($datum['parent_id'])) {
                if (!is_null($datum['parent_number']) && isset($categories[$datum['parent_number']])) {
                    Category::whereId($datum['id'])->update(['parent_id' => $categories[$datum['parent_number']]]);
                    $existedData['categories'][$datum['id']] = $categories[$datum['parent_number']];
                }
            } else {
                Category::whereId($datum['id'])->update(['parent_id' => $datum['parent_id']]);
            }
        }

        return $categories;
    }

    protected function updateCategory(array $datum): int
    {
        // Update category name and parent if it exists
        foreach ($this->languages as $lang => $language) {
            CategoryTranslates::whereRowId($datum['id'])->whereLanguage($lang)->update([
                'name' => $datum[$lang]['name'],
            ]);
        }

        return $datum['id'];
    }

    protected function createCategory(array $datum, array &$slugs): int
    {
        // Create category
        $categoryId = Category::create([
            'active' => true, 'parent_id' => $datum['parent_id'] ?? null, 'position' => 500,
        ])->id;
        foreach ($this->languages as $lang => $language) {
            $slugs[$lang] = $slugs[$lang] ?? [];
            $slug = $this->createSlug($slugs[$lang], $datum[$lang]['name']);
            $slugs[$lang][] = $slug;
            CategoryTranslates::create(['row_id' => $categoryId, 'language' => $lang,
                'name' => $datum[$lang]['name'], 'slug' => $slug]);
        }

        return $categoryId;
    }

    protected function getExistedSlugs(): array
    {
        $slugs = [];
        CategoryTranslates::all(['language', 'slug'])
            ->each(function (CategoryTranslates $tr) use (&$slugs) {
                $slugs[$tr->language] = $slugs[$tr->language] ?? [];
                $slugs[$tr->language][] = $tr->slug;
            });

        return $slugs;
    }
}
