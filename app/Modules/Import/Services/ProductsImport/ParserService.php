<?php

namespace App\Modules\Import\Services\ProductsImport;

use App\Core\Modules\Languages\Models\Language;
use App\Modules\Currencies\Models\Currency as CurrencyModel;
use Illuminate\Support\Collection;

/**
 * Class ParserService
 * @package App\Modules\Import\Services
 */
class ParserService
{
    /**
     * @var string Categories sheet name.
     */
    protected $categoriesSheetName = 'Categories';
    /**
     * @var string Groups sheet name.
     */
    protected $groupsSheetName = 'Products';
    /**
     * @var string Products sheet name
     */
    protected $productsSheetName = 'Prices';
    /**
     * @var array Errors list.
     */
    protected $errors = ['categories' => [], 'groups' => [], 'products' => []];
    /**
     * @var array|Language[]
     */
    protected $languages;
    /**
     * @var CurrencyModel
     */
    protected $course;

    /**
     * ParserService constructor.
     */
    public function __construct()
    {
        $this->course = config('currency.admin');
        $this->languages = config('languages', []);
    }

    /**
     * Validates and prepares data for storing to the database.
     *
     * @param array $data
     * @param array $existedData
     * @param Collection $courses
     * @return array
     */
    public function start(array $data, Collection $courses, array $existedData): array
    {
        $categories = $this->categories($data, $existedData);
        $groups = $this->groups($data, $existedData);
        $this->products($data, $existedData, $groups, $courses);

        return [$categories, $groups];
    }

    /**
     * Collects raw data and transforms it to the useful for application.
     *
     * @param array $data
     * @param array $existedData
     * @return array
     */
    private function categories(array $data, array $existedData): array
    {
        # Check sheet existence
        if (!isset($data[$this->categoriesSheetName])) {
            return [];
        }
        $rows = $data[$this->categoriesSheetName];
        $categories = [];
        # Loop the data, transform and return only valid items
        foreach ($rows as $rowNumber => $row) {
            try {
                # Validate row
                $this->validateCategory($row, $existedData);
                # Transform data to the form we can use
                $categories[] = $this->transformCategory($row, $existedData);
            } catch (\ErrorException $exception) {
                # Here we can write the error somewhere
                $this->errors['categories'][] = $exception->getMessage();
            }
        }

        return $categories;
    }

    /**
     * @param array $category
     * @param array $existedData
     * @throws \ErrorException
     */
    private function validateCategory(array $category, array $existedData): void
    {
        if (!isset($category['CategoryId']) || !array_key_exists((int)$category['CategoryId'], $existedData['categories'])) {
            if (!isset($category['CategoryNumber'])) {
                throw new \ErrorException('[Category] Category number or ID is required.');
            }
        }
        if (!isset($category['Name'])) {
            foreach ($this->languages as $slug => $language) {
                if (!isset($category["Name_$slug"])) {
                    throw new \ErrorException('[Category] Category name(s) is required.');
                }
            }
        }
    }

    /**
     * Transforms raw data from .xlsx file to the useful for application.
     *
     * @param array $category
     * @param array $existedData
     * @return array
     */
    private function transformCategory(array $category, array $existedData): ?array
    {
        $id = isset($category['CategoryId']) ? (int)$category['CategoryId'] : null;
        $parentId = null;
        if (isset($category['CategoryParentId']) && array_key_exists((int)$category['CategoryParentId'], $existedData['categories'])) {
            $parentId = (int)$category['CategoryParentId'];
        }
        $datum = [
            'id' => $id,
            'parent_id' => $parentId,
            'number' => (!$id && isset($category['CategoryNumber'])) ? $category['CategoryNumber'] : null,
            'parent_number' => (!$parentId && isset($category['CategoryParentNumber'])) ? $category['CategoryParentNumber'] : null,
        ];
        foreach ($this->languages as $lang => $language) {
            $datum[$lang]['name'] = $category["Name_$lang"] ?? $category['Name'];
        }

        return $datum;
    }

    /**
     * Collects raw data and transforms it to the useful for application.
     *
     * @param array $data
     * @param array $existedData
     * @return array
     */
    private function groups(array $data, array $existedData): array
    {
        # Check sheet existence
        if (!isset($data[$this->groupsSheetName])) {
            return [];
        }
        $rows = $data[$this->groupsSheetName];
        $groups = [];
        # Loop the data, transform and return only valid items
        foreach ($rows as $rowNumber => $row) {
            try {
                # Validate row
                $this->validateGroup($row, $existedData);
                # Transform data to the form we can use
                $group = $this->transformGroup($row, $existedData);
                $groups[$group['id'] . '-' . $group['number']] = $group;
            } catch (\ErrorException $exception) {
                # Here we can write the error somewhere
                $this->errors['groups'][] = $exception->getMessage();
            }
        }

        return $groups;
    }

    /**
     * @param array $group
     * @param array $existedData
     * @throws \ErrorException
     */
    private function validateGroup(array $group, array $existedData): void
    {
        if (!isset($group['GroupId']) || !isset($existedData['groups'][$group['GroupId']])) {
            if (!isset($group['GroupNumber'])) {
                throw new \ErrorException('[Group] Group number or ID is required.');
            }
        }
        if (!isset($group['Name'])) {
            foreach ($this->languages as $slug => $language) {
                if (!isset($group["Name_$slug"])) {
                    throw new \ErrorException('[Group] Group name(s) is required.');
                }
            }
        }
    }

    /**
     * Transforms raw data from .xlsx file to the useful for application.
     *
     * @param array $group
     * @param array $existedData
     * @return array
     */
    private function transformGroup(array $group, array $existedData): array
    {
        $id = isset($group['GroupId']) ? (int)$group['GroupId'] : null;
        $categoryId = null;
        if (isset($group['CategoryId']) && array_key_exists((int)$group['CategoryId'], $existedData['categories'])) {
            $categoryId = (int)$group['CategoryId'];
        }
        $features = [];
        foreach ($group['Feature'] ?? [] as $index => $featureName) {
            if (isset($group['Value'][$index])) {
                $features[$featureName] = array_map(function ($element) {
                    return trim($element, " \t\n\r\0\x0B");
                }, explode('|', $group['Value'][$index]));
            }
        }
        $years = [];
        if (isset($group['Year'])) {
            foreach (explode('|', $group['Year']) as $yearPeriod) {
                list($fromYear, $toYear) = array_pad(explode('-', $yearPeriod), 2, null);
                $fromYear = (int)$fromYear;
                if (is_null($toYear)) {
                    $years[] = $fromYear;
                } else {
                    $years = array_merge($years, range($fromYear, trim($toYear) ?: date('Y')));
                }
            }
            $years = array_unique($years);
        }
        $datum = [
            'id' => $id,
            'number' => (!$id && isset($group['GroupNumber'])) ? $group['GroupNumber'] : null,
            'category_id' => $categoryId,
            'category_number' => (!$categoryId && isset($group['CategoryNumber'])) ? $group['CategoryNumber'] : null,
            'Brand' => $group['Brand'] ?? null,
            'MainFeature' => $group['MainFeature'] ?? null,
            'Features' => $features,
            'Years' => $years,
            'Products' => [],
            'EngineNumbers' => array_map(function ($element) {
                return trim($element, " \t\n\r\0\x0B");
            }, explode('|', $group['EngineNumbers'] ?? '')),
        ];
        foreach ($this->languages as $lang => $language) {
            $datum[$lang]['name'] = $group["Name_$lang"] ?? $group['Name'] ?? null;
            $datum[$lang]['text'] = $group["Description_$lang"] ?? $group['Description'] ?? null;
        }

        return $datum;
    }

    /**
     * Collects raw data and transforms it to the useful for application.
     *
     * @param array $data
     * @param array $existedData
     * @param array $groups
     * @param Collection $courses
     * @return array
     */
    private function products(array $data, array $existedData, array &$groups, Collection $courses): array
    {
        # Check sheet existence
        if (!isset($data[$this->productsSheetName])) {
            return [];
        }
        $rows = $data[$this->productsSheetName];
        $products = [];
        # Loop the data, transform and return only valid items
        foreach ($rows as $rowNumber => $row) {
            try {
                # Validate row
                $this->validateProduct($row, $existedData);
                # Transform data to the form we can use
                $product = $this->transformProduct($row, $existedData, $courses);
                if (isset($groups[$product['group_id'] . '-' . $product['group_number']])) {
                    $groups[$product['group_id'] . '-' . $product['group_number']]['Products'][] = $product;
                }
                $products[] = $product;
            } catch (\ErrorException $e) {
                # Here we can write the error somewhere
                $this->errors['products'][] = "{$e->getMessage()} :{$e->getLine()}";
            }
        }

        return $products;
    }

    /**
     * Validates data for the product.
     *
     * @param array $product
     * @param array $existedData
     * @throws \ErrorException
     */
    private function validateProduct(array $product, array $existedData): void
    {
        if (!isset($product['GroupId']) || !isset($existedData['groups'][(int)$product['GroupId']])) {
            if (!isset($product['GroupNumber'])) {
                throw new \ErrorException('[Product] Group number or ID is required.');
            }
        }
        if (isset($product['Price']) && (float)$product['Price'] < 0) {
            throw new \ErrorException('[Product] Price is required and should be greater or equal to zero.');
        }
    }

    /**
     * Transforms raw data from .xlsx file to the useful for application.
     *
     * @param array $product
     * @param array $existedData
     * @param Collection $courses
     * @return array
     */
    private function transformProduct(array $product, array $existedData, Collection $courses): array
    {
        $id = (isset($product['ProductId']) && in_array((int)$product['ProductId'], $existedData['products'])) ? (int)$product['ProductId'] : null;
        $groupId = null;
        if (isset($product['GroupId']) && isset($existedData['groups'][(int)$product['GroupId']])) {
            $groupId = (int)$product['GroupId'];
        }
        $datum = [
            'id' => $id,
            'group_id' => $groupId,
            'group_number' => (!$groupId && isset($product['GroupNumber'])) ? $product['GroupNumber'] : null,
            'price' => $this->calculate($courses, (float)str_replace('$', '', $product['Price']),
                $product['Currency'] ?? null),
            'old_price' => isset($product['OldPrice'])
                ? $this->calculate($courses, (float)str_replace('$', '', $product['OldPrice']),
                    $product['Currency'] ?? null)
                : 0,
            'available' => isset($product['Availability']) ? (int)$product['Availability'] : 0,
            'vendor_code' => $product['VendorCode'] ?? null,
            'MainFeatureValue' => isset($product['MainFeatureValue']) ? (string)$product['MainFeatureValue'] : null,
            'Images' => array_filter(array_map(function ($element) {
                return trim($element, " \t\n\r\0\x0B");
            }, explode('|', $product['Images'] ?? ''))),
        ];
        foreach (config('languages') as $lang => $language) {
            $datum[$lang]['name'] = $product["Name_$lang"] ?? $product['Name'] ?? '';
            $datum[$lang]['short_name'] = $product["ShortName_$lang"] ?? $product['ShortName'] ?? $product['Name'] ?? '';
        }

        return $datum;
    }

    /**
     * Calculates price using the real course.
     *
     * @param Collection $courses
     * @param float $price
     * @param string|null $currency
     * @return float
     */
    protected function calculate(Collection $courses, float $price, ?string $currency): float
    {
        if ($course = $courses->get($currency ?? $this->course->microdata)) {
            return $course * $price;
        }

        return $price;
    }
}
