<?php

namespace App\Modules\Import\Services\ProductsImport;

use App\Core\Modules\Languages\Models\Language;
use App\Modules\Categories\Models\Category;
use App\Modules\Import\Components\ImportSettings;
use App\Modules\Import\Services\ProductsImport\Database\BrandsService;
use App\Modules\Import\Services\ProductsImport\Database\CategoriesService;
use App\Modules\Import\Services\ProductsImport\Database\FeaturesService;
use App\Modules\Import\Services\ProductsImport\Database\ImagesService;
use App\Modules\Import\Services\ProductsImport\Database\ValuesService;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Import\Services\ProductsImport\Database\ProductsService as DBProductService;

/**
 * Class ImportService
 * @package App\Modules\Import\Services
 */
class ImportService
{
    /**
     * @var ParserService
     */
    protected $parserService;
    /**
     * @var ImportSettings
     */
    protected $settings;
    /**
     * @var Language[]
     */
    protected $languages;

    /**
     * ImportService constructor.
     *
     * @param ParserService $parser
     */
    public function __construct(ParserService $parser)
    {
        $this->parserService = $parser;
        $this->languages = config('languages', []);
    }

    /**
     * Sets the settings.
     *
     * @param ImportSettings $settings
     */
    public function setSettings(ImportSettings $settings): void
    {
        $this->settings = $settings;
    }

    /**
     * Import process.
     *
     * @param array $parsedData Parsed data from the .xlsx file.
     * @throws \Exception
     */
    public function start(array $parsedData)
    {
        $this->import(
            $this->parserService->start(
                $parsedData,
                $this->settings->courses,
                $existedData = $this->retrieveDataFromTheDatabase()
            ),
            $existedData
        );
    }

    /**
     * Works with the existed data in the database.
     * TODO Do not work with the models here. But this is not critical or urgent for now.
     *
     * @return array
     */
    protected function retrieveDataFromTheDatabase(): array
    {
        // Make some requests to the database and store data as arrays here
        // in memory. All the relations should be searched in this arrays.
        $existedData['categories'] = Category::pluck('parent_id', 'id')->toArray();
        $existedData['products'] = Product::pluck('id')->toArray();
        $existedData['groups'] = ProductGroup::all(['id', 'available', 'price_max', 'price_min'])
            ->keyBy('id')->toArray();

        return $existedData;
    }

    /**
     * Import body.
     *
     * @param array $data
     * @param array $existedData
     * @throws \Exception
     */
    protected function import(array $data, array $existedData)
    {
        list($categories, $groups) = $data;
        # Removes unnecessary data from the memory
        unset($data);
        # Creates/Updates brands and returns an array like [brandName => brandId]
        $brands = (new BrandsService())->make($groups);
        # Creates/Updates features and returns an array like [featureName => featureId]
        $features = (new FeaturesService())->make($groups);
        # Creates/Updates features and returns an array like [featureId => [valueName => valueId]]
        $values = (new ValuesService($features))->make($groups);
        # Creates/Updates categories and returns an array like [categoryNumber => newCategoryId]
        $categories = (new CategoriesService($this->settings->createCategories,
            $this->settings->updateCategories))->make($categories, $existedData);
        # Creates/Updates products and their groups
        # Also returns a list of images as an array like [productId => [image, image]]
        $images = (new DBProductService($categories, $brands, $features, $values,
            $existedData['groups'], $existedData['categories'],
            $this->settings->createProducts, $this->settings->updateProducts))->make($groups);
        # Removes unnecessary data from the memory
        unset($existedData, $groups, $categories, $brands, $features, $values);
        # Uploads images and links them to the products inside the database
        (new ImagesService($this->settings->deleteOldImages,
            $this->settings->uploadImages))->make($images);
        # Clear temp images folder
        \File::deleteDirectory(storage_path('app/public/temp'));
        \File::makeDirectory(storage_path('app/public/temp'), 0777, true, true);
    }
}
