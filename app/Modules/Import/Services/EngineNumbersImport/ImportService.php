<?php

namespace App\Modules\Import\Services\EngineNumbersImport;

use App\Core\Modules\Languages\Models\Language;
use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Engine\Models\Model;
use App\Modules\Engine\Models\Power;
use App\Modules\Engine\Models\PowerTranslates;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use Illuminate\Database\Eloquent\Builder;
use App\Modules\Import\Models\EngineNumbersImportDetail as ImportDetail;
use Illuminate\Support\Str;

/**
 * Class ImportService
 * @package App\Modules\Import\Services
 */
class ImportService
{
    /**
     * @var Language[]
     */
    protected $languages;
    /**
     * @var int
     */
    protected $importId;

    /**
     * ImportService constructor.
     */
    public function __construct()
    {
        $this->languages = config('languages', []);
    }

    /**
     * Sets import ID.
     *
     * @param int $importId
     */
    public function setImportId(int $importId)
    {
        $this->importId = $importId;
    }

    /**
     * Import process.
     *
     * @param array $parsedData Parsed data from the .xlsx file.
     * @throws \Exception
     */
    public function start(array $parsedData)
    {
        $vendors = $models = $volumes = $powers = [];
        foreach ($parsedData as $parsedDatum) {
            try {
                # Works with vendor
                if (!$parsedDatum['Vendor']) continue;
                $vendorId = $this->getVendorId($parsedDatum['Vendor'], $vendors);
                # Works with model
                if (!$parsedDatum['Model']) continue;
                if (Str::endsWith($parsedDatum['Model'], '.0')) {
                    $modelName = Str::replaceLast('.0', '', $parsedDatum['Model']);
                } else {
                    $modelName = $parsedDatum['Model'];
                }
                $modelId = $this->getModelId($vendorId, $modelName, $models);
                # Works with volume
                if (!$parsedDatum['Volume']) continue;
                $volumeId = $this->getVolumeId($vendorId, $modelId, $parsedDatum['Volume'], $volumes);
                # Works with power
                if (!$parsedDatum['Power']) continue;
                $powerId = $this->getPowerId($vendorId, $modelId, $volumeId, $parsedDatum['Power'], $powers);
                # Works with engine numbers
                if (!$parsedDatum['Number']) continue;
                foreach (array_filter(explode('|', $parsedDatum['Number']), 'trim') as $number) {
                    if ($number) {
                        $this->createEngineNumber($vendorId, $modelId, $volumeId, $powerId, $number);
                    }
                }
            } catch (\Exception $exception) {
                $this->error($exception, 'Engine number was not added.');
            }
        }
    }

    /**
     * @param \Exception $e
     * @param string|null $description
     */
    protected function error(\Exception $e, ?string $description = null): void
    {
        ImportDetail::create([
            'import_id' => $this->importId,
            'message' => "[{$e->getFile()}:{$e->getLine()}] {$e->getMessage()}",
            'trace' => $e->getTraceAsString(),
            'description' => $description ?? "Unknown error",
        ]);
    }

    /**
     * Creates car vendor in the database.
     *
     * @param string $vendorName
     * @param array $vendors
     * @return int
     */
    protected function getVendorId(string $vendorName, array &$vendors): int
    {
        if (isset($vendors[$vendorName])) {
            return $vendors[$vendorName];
        }
        $vendors[$vendorName] = $vendorId = Vendor::updateOrCreate(['name' => $vendorName])->id;

        return $vendorId;
    }

    /**
     * Creates car model in the database.
     *
     * @param int $vendorId
     * @param string $modelName
     * @param array $models
     * @return int
     */
    protected function getModelId(int $vendorId, string $modelName, array &$models): int
    {
        $key = $vendorId . '|' . $modelName;
        if (isset($models[$key])) {
            return $models[$key];
        }
        $models[$key] = $modelId = Model::updateOrCreate(['name' => $modelName, 'vendor_id' => $vendorId])->id;

        return $modelId;
    }

    /**
     * Creates engine volume in the database.
     *
     * @param int $vendorId
     * @param int $modelId
     * @param string $volumeName
     * @param array $volumes
     * @return int
     */
    protected function getVolumeId(int $vendorId, int $modelId, string $volumeName, array &$volumes): int
    {
        $key = $vendorId . '|' . $modelId . '|' . $volumeName;
        if (isset($volumes[$key])) {
            return $volumes[$key];
        }
        $volumeId = Volume::updateOrCreate(['name' => $volumeName, 'vendor_id' => $vendorId, 'model_id' => $modelId])
            ->id;
        $volumes[$key] = $volumeId;

        return $volumeId;
    }

    /**
     * Creates engine power in the database.
     *
     * @param int $vendorId
     * @param int $modelId
     * @param int $volumeId
     * @param string $powerName
     * @param array $powers
     * @return int
     */
    protected function getPowerId(int $vendorId, int $modelId, int $volumeId, string $powerName, array &$powers): int
    {
        $key = $vendorId . '|' . $modelId . '|' . $volumeId . '|' . $powerName;
        if (isset($powers[$key])) {
            return $powers[$key];
        }
        $power = Power::whereVendorId($vendorId)->whereModelId($modelId)->whereVolumeId($volumeId)
            ->with('data')
            ->whereHas('data', function (Builder $builder) use ($powerName) {
                $builder->where('name', $powerName);
            })->first();
        if ($power) {
            $powerId = $power->id;
        } else {
            $powerId = Power::create(['vendor_id' => $vendorId, 'model_id' => $modelId,
                'volume_id' => $volumeId])->id;
            foreach ($this->languages as $lang => $language) {
                PowerTranslates::create(['row_id' => $powerId, 'language' => $lang, 'name' => $powerName]);
            }
        }
        $powers[$key] = $powerId;

        return $powerId;
    }

    /**
     * Creates engine number in the database.
     *
     * @param int $vendorId
     * @param int $modelId
     * @param int $volumeId
     * @param int $powerId
     * @param string $number
     */
    protected function createEngineNumber(int $vendorId, int $modelId, int $volumeId, int $powerId, string $number): void
    {
        EngineNumber::updateOrCreate(['vendor_id' => $vendorId, 'model_id' => $modelId,
            'volume_id' => $volumeId, 'power_id' => $powerId, 'engine_number' => $number]);
    }
}
