<?php


namespace App\Modules\Import\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Class Verify1cToken
 *
 * @package App\Modules\Import\Middleware
 */
class Verify1cToken
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = config('db.ones.token');
        if ($request->route()->parameter('token') !== $token) {
            abort( 403);
        }

        return $next($request);

    }

}
