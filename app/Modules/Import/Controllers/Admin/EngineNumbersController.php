<?php

namespace App\Modules\Import\Controllers\Admin;

use App\Core\AjaxTrait;
use App\Modules\Import\Forms\EngineNumbersImportForm;
use App\Modules\Import\Jobs\EngineNumbersImport;
use App\Core\AdminController;
use App\Modules\Import\Models\EngineNumbersImport as EngineNumbersImportModel;
use Illuminate\Http\Request;

/**
 * Class EngineNumbersController
 *
 * @package App\Modules\Import\Controllers\Admin
 */
class EngineNumbersController extends AdminController
{
    use AjaxTrait;

    /**
     * @var int Limit of rows on the page
     */
    protected $limit = 15;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function index()
    {
        $imports = EngineNumbersImportModel::with(['errors'])->latest()->paginate($this->limit);
        $form = EngineNumbersImportForm::make();

        return view('import::admin.engine.index', compact('imports', 'form'));
    }

    /**
     * @param EngineNumbersImportModel $import
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(EngineNumbersImportModel $import)
    {
        return view('import::admin.engine.show', compact('import'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $file = $request->file('import');
        \Storage::put('import.' . $file->clientExtension(), $file->get());
        $import = EngineNumbersImportModel::create();
        EngineNumbersImport::dispatch('import.' . $file->clientExtension(), $import);

        return redirect()->back();
    }
}
