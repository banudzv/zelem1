<?php

namespace App\Modules\Import\Controllers\Admin;

use App\Components\Parsers\Entry;
use App\Core\AjaxTrait;
use App\Modules\Import\Jobs\ProductsImport;
use App\Core\AdminController;
use App\Modules\Import\Forms\ImportForm;
use App\Modules\Import\Requests\ImportRequest;
use App\Modules\Import\Models\Import as ImportModel;
use App\Core\Modules\Images\Models\Image;
use App\Modules\Categories\Models\Category;
use App\Modules\Features\Models\Feature;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\ProductsDictionary\Models\Dictionary;
use App\Modules\ProductsLabels\Models\Label;
use File;

/**
 * Class IndexController
 *
 * @package App\Modules\Import\Controllers\Admin
 */
class IndexController extends AdminController
{
    use AjaxTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function index()
    {
        $this->initValidation((new ImportRequest)->rules());

        return view('import::admin.index', [
            'form' => ImportForm::make(),
            'import' => ImportModel::getLast(),
        ]);
    }

    /**
     * @param ImportRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws \Throwable
     */
    public function store(ImportRequest $request)
    {
        $import = ImportModel::start($request->all());
        $file = $request->file('import');
        \Storage::put('import.' . $file->clientExtension(), $file->get());
        ProductsImport::dispatch('import.' . $file->clientExtension(), $import);

        return redirect()->back();
    }

    /**
     * Check import status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status()
    {
        $import = ImportModel::getLast();

        return $this->successJsonAnswer([
            'status' => $import ? $import->status : '',
        ]);
    }

    /**
     * Delete all catalog entity
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy()
    {
        try {
            abort_unless(isTest(), 403, 'No access');
            ProductGroup::query()->delete();
            Dictionary::query()->delete();
            Feature::query()->delete();
            Category::query()->delete();
            Label::query()->delete();
            Image::query()->whereImageableType('groups')->delete();
            Image::query()->whereImageableType('products')->delete();
            Image::query()->whereImageableType('brands')->delete();
            Image::query()->whereImageableType('categories')->delete();
            File::deleteDirectory(storage_path() . '/app/public/products');
            File::deleteDirectory(storage_path() . '/app/public/categories');
            File::deleteDirectory(storage_path() . '/app/public/brands');

            return $this->afterDestroy();
        } catch (\Exception $exception) {
            return $this->afterFail($exception->getMessage());
        }
    }

    public function download()
    {
        $file = database_path() . '/import/export-products-full.xlsx';
        if (is_file($file)) {
            return response()->download($file);
        }

        return $this->afterFail();
    }
}
