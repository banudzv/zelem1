<?php

namespace App\Modules\Import\Controllers\Site;

use App\Components\Parsers\Entry;
use App\Core\AjaxTrait;
use App\Modules\Import\Jobs\Import;
use App\Core\AdminController;
use App\Modules\Import\Models\Import as ImportModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class ErpController
 *
 * @package App\Modules\Import\Controllers\Admin
 */
class ErpController extends AdminController
{
    use AjaxTrait;
    
    /**
     * For 1C
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function __invoke(Request $request)
    {
        Log::info(json_encode($request->all()));
        Log::info($request->hasFile('file') ? 'Import file was uploaded!' : 'Import file was not uploaded!');
        if (!$request->hasFile('file')) {
            return $this->errorJsonAnswer();
        }
        $fileName = $request->file('file')->storeAs('ones/exchange', 'ones_exchange.xml');
        $import = ImportModel::start([
            'categories' => 'update-and-disable-old',
            'products' => 'update-and-disable-old',
        ]);
        Import::dispatch(url('storage/' . $fileName), $import, Entry::TYPE_ONE_S);
        return $this->successJsonAnswer();
    }
}
