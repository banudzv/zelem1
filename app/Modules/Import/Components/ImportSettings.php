<?php

namespace App\Modules\Import\Components;

use Illuminate\Support\Collection;

/**
 * Class ImportSettings
 *
 * @package App\Components\Parsers\PromUa
 */
class ImportSettings
{
    /**
     * Do nothing with images
     */
    const IMAGES_DO_NOT_UPLOAD = 'none';
    /**
     * Delete old images & upload new from the file
     */
    const IMAGES_REWRITE = 'rewrite';
    /**
     * Just upload new images from the file
     */
    const IMAGES_ADD = 'add';
    const PRODUCTS_DO_NOTHING = 'none';
    const PRODUCTS_JUST_UPDATE = 'just-update';
    const PRODUCTS_ONLY_NEW = 'only-new';
    const PRODUCTS_ALL = 'all';
    const CATEGORIES_DO_NOTHING = 'none';
    const CATEGORIES_JUST_UPDATE = 'just-update';
    const CATEGORIES_ONLY_NEW = 'only-new';
    const CATEGORIES_ALL = 'all';

    /**
     * @var bool
     */
    public $uploadImages = false;
    /**
     * @var bool
     */
    public $deleteOldImages = false;
    /**
     * @var bool
     */
    public $updateProducts = false;
    /**
     * @var bool
     */
    public $createProducts = false;
    /**
     * @var array
     */
    public $courses;
    /**
     * @var bool
     */
    public $updateCategories = false;
    /**
     * @var bool
     */
    public $createCategories = false;
    
    /**
     * ImportSettings constructor.
     *
     * @param array $settings
     */
    public function __construct(array $settings = [])
    {
        if (array_get($settings, 'categories') === self::CATEGORIES_JUST_UPDATE) {
            $this->updateCategories = true;
        }
        if (array_get($settings, 'categories') === self::CATEGORIES_ONLY_NEW) {
            $this->createCategories = true;
        }
        if (array_get($settings, 'categories') === self::CATEGORIES_ALL) {
            $this->updateCategories = true;
            $this->createCategories = true;
        }
        if (array_get($settings, 'products') === self::PRODUCTS_JUST_UPDATE) {
            $this->updateProducts = true;
        }
        if (array_get($settings, 'products') === self::PRODUCTS_ONLY_NEW) {
            $this->createProducts = true;
        }
        if (array_get($settings, 'products') === self::PRODUCTS_ALL) {
            $this->updateProducts = true;
            $this->createProducts = true;
        }
        if (array_get($settings, 'images') === self::IMAGES_ADD) {
            $this->uploadImages = true;
        } elseif(array_get($settings, 'images') === self::IMAGES_REWRITE) {
            $this->deleteOldImages = true;
            $this->uploadImages = true;
        }
        $this->courses = new Collection();
        foreach (array_get($settings, 'course', []) as $currency => $course) {
            $this->courses->put($currency, (float)$course);
        }
    }
}
