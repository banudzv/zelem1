<?php

namespace App\Modules\Import;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Import\Widgets\Courses;
use App\Modules\Import\Widgets\CoursesHistory;
use CustomForm\Input;
use CustomMenu, CustomRoles;
use CustomSettings;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Home
 */
class Provider extends BaseProvider
{

    /**
     * Set custom presets
     */
    protected function presets() {}

    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register left menu block
        CustomMenu::get()->group()->block('catalog')
            ->link('import::admin.menu', RouteObjectValue::make('admin.import.index'))
            ->setPosition(10);
        CustomMenu::get()->group()->block('engine')
            ->link('import::admin.menu', RouteObjectValue::make('admin.en-import.index'))
            ->additionalRoutesForActiveDetect(RouteObjectValue::make('admin.en-import.show'))
            ->setPosition(10);
        // Register role scopes
        CustomRoles::add('import', 'import::admin.permission-name')
            ->only(RoleRule::STORE, RoleRule::INDEX, RoleRule::DELETE);

        $settings = CustomSettings::createAndGet('ones', 'import::settings.group-name');
        $settings->add(
            Input::create('token')
                ->setLabel('import::settings.attributes.token')
                ->required()
        );

        \Widget::register(Courses::class, 'import::courses');
        \Widget::register(CoursesHistory::class, 'import::courses-history');
    }

    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
    }

}
