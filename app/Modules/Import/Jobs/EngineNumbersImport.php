<?php

namespace App\Modules\Import\Jobs;

use App\Modules\Import\Services\EngineNumbersImport\ImportService;
use App\Modules\Import\Services\XlsxParserService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Arr;
use App\Modules\Import\Models\EngineNumbersImport as EngineNumbersImportModel;

/**
 * Class EngineNumbersImport
 * @package App\Modules\Import\Jobs
 */
class EngineNumbersImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * @var string
     */
    protected $url;
    /**
     * @var EngineNumbersImportModel
     */
    protected $import;

    /**
     * Create a new job instance.
     *
     * @param string $url
     * @param EngineNumbersImportModel $import
     */
    public function __construct(string $url, EngineNumbersImportModel $import)
    {
        $this->url = $url;
        $this->import = $import;
    }
    
    /**
     * @throws \Throwable
     */
    public function handle()
    {
        try {
            $this->import->update(['status' => EngineNumbersImportModel::STATUS_IN_PROGRESS]);
            $dataFromXlsx = (new XlsxParserService($this->url))->start();
            $service = \App::make(ImportService::class);
            $service->setImportId($this->import->id);
            foreach ($dataFromXlsx as $datum) {
                if (count($datum) > 0) {
                    $service->start($datum);
                }
            }
            $this->import->update([
                'finished_at' => Carbon::now(),
                'status' => EngineNumbersImportModel::STATUS_FINISHED,
            ]);
        } catch (\Exception $exception) {
            $this->import->update([
                'finished_at' => Carbon::now(),
                'description' => $exception->getMessage(),
                'status' => EngineNumbersImportModel::STATUS_FINISHED,
            ]);
        }
    }
}
