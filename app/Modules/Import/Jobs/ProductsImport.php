<?php

namespace App\Modules\Import\Jobs;

use App\Modules\Import\Components\ImportSettings;
use App\Modules\Import\Services\ProductsImport\ImportService;
use App\Modules\Import\Services\XlsxParserService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Modules\Import\Models\Import as ImportModel;
use Illuminate\Support\Str;

/**
 * Class ProductsImport
 * @package App\Modules\Import\Jobs
 */
class ProductsImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * @var string
     */
    protected $url;
    /**
     * @var ImportModel
     */
    protected $import;
    /**
     * @var ImportSettings
     */
    protected $settings;
    
    /**
     * Create a new job instance.
     *
     * @param string $url
     * @param ImportModel $import
     */
    public function __construct(string $url, ImportModel $import)
    {
        $this->url = $url;
        $this->import = $import;
        $this->settings = new ImportSettings($import->data);
    }
    
    /**
     * @throws \Throwable
     */
    public function handle()
    {
        $this->import->update(['status' => ImportModel::STATUS_PROCESSING]);
        // Parse .xlsx file
        $dataFromXlsx = (new XlsxParserService($this->url))->start();
        $service = \App::make(ImportService::class);
        $service->setSettings($this->settings);
        $service->start($dataFromXlsx);
        $this->import->update(['status' => ImportModel::STATUS_DONE]);
    }
    
    /**
     * The job failed to process.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $this->import->update([
            'status' => ImportModel::STATUS_FAILED,
            'message' => Str::substr($exception->getMessage(), 0, 180),
        ]);
    }
    
    /**
     * @return ImportSettings
     */
    public function getSettings(): ImportSettings
    {
        return $this->settings;
    }
}
