<?php

namespace App\Modules\Products\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Products\Models\Product;
use Catalog;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Group\Row;
use CustomForm\Hidden;
use CustomForm\Input;
use CustomForm\Macro\InputForSlug;
use CustomForm\Macro\Slug;
use CustomForm\SimpleSelect;
use CustomForm\Text;
use CustomForm\TextArea;
use Exception;
use Illuminate\Database\Eloquent\Model;

class ProductForm implements FormInterface
{

    /**
     * @param Model|Product|null $product
     * @param int $index
     * @return Form
     * @throws WrongParametersException|Exception
     */
    public static function make(?Model $product = null, int $index = 0): Form
    {
        $priceLabel = 'validation.attributes.price';
        $oldPriceLabel = 'validation.attributes.old_price';
        if (Catalog::currenciesLoaded()) {
            $currencyClassName = Catalog::currency()->getClassName();
            $currency = config('currency.admin', new $currencyClassName);
            $priceLabel = __('validation.attributes.price') . ', ' . $currency->sign;
            $oldPriceLabel = __('validation.attributes.old_price') . ', ' . $currency->sign;
        }

        $product = $product ?? new Product();

        $form = Form::create();

        $form->fieldSet(12, FieldSet::COLOR_SUCCESS)->boxed(false)->add(
            Hidden::create('modification[id][' . $index . ']')->setValue($product->id ?? 0),
            Text::create()->setDefaultValue(
                view('products::admin.product.images', ['product' => $product, 'index' => $index])
            ),
            Row::create()->add(
                Input::create('modification[price][' . $index . ']')
                    ->setType('number')
                    ->setValue($product->price ?? 0)
                    ->addClasses('modification-price')
                    ->setLabel($priceLabel)
                    ->setOptions(['step' => 0.25, 'min' => 0])
                    ->addClassesToDiv('col-md-6')
                    ->required(),
                Input::create('modification[old_price][' . $index . ']')
                    ->setType('number')
                    ->setValue($product->old_price ?? 0)
                    ->setLabel($oldPriceLabel)
                    ->setOptions(['step' => 0.25, 'min' => 0])
                    ->addClassesToDiv('col-md-6')
            ),
            Row::create()->add(
                Input::create('modification[vendor_code][' . $index . ']')
                    ->setValue($product->vendor_code)
                    ->setLabel('products::general.attributes.vendor_code')
                    ->addClassesToDiv('col-md-6'),
                ((bool)config('db.products.show-availability', true) === true)
                    ? SimpleSelect::create('modification[available][' . $index . ']')
                    ->setValue($product->available)
                    ->setLabel('validation.attributes.available')
                    ->add(config('products.availability', []))
                    ->addClassesToDiv('col-md-6')
                    ->setDefaultValue(Product::AVAILABLE)
                    : Hidden::create('modification[available][' . $index . ']')
                    ->setValue(true)
                    ->setLabel(false)
            ),
            Row::create()->add(
                Input::create('modification[relevance][' . $index . ']')
                    ->setValue($product->relevance)
                    ->setType('number')
                    ->setDefaultValue(0)
                    ->setLabel('products::general.attributes.relevance')
                    ->addClassesToDiv('col-md-6')
            )
        /*,
        Row::create()->add(
            Text::create()->setDefaultValue(view('products::admin.wholesale.list', [
                'prices' => $product->wholesale,
                'product' => $product,
                'index' => $index,
            ]))
        )*/
        );
        $languages = config('languages', []);
        if (count($languages) > 1) {
            /* NEW */
            $form->fieldSetForLang(12)->boxed(false)->add(
                InputForSlug::create('modification][name][' . $index, $product)
                    ->addClassesToDiv('col-md-6')
                    ->setLabel('validation.attributes.name'),
                Slug::create('modification][slug][' . $index, $product)
                    ->addClassesToDiv('col-md-6')
                    ->setLabel('validation.attributes.slug'),
                TextArea::create('modification][search_keywords][' . $index, $product)
                    ->addClassesToDiv('col-md-12')
                    ->setLabel('products::admin.attributes.search_keywords')
            );
        } else {
            /* OLD */
            $form->fieldSetForLang(12)->boxed(false)->add(
                InputForSlug::create('modification][name][' . $index)
                    ->setValue($product->exists ? $product->current->name : null)
                    ->addClassesToDiv('col-md-6')
                    ->setLabel('validation.attributes.name'),
                Slug::create('modification][slug][' . $index)
                    ->setValue($product->exists ? $product->current->slug : null)
                    ->addClassesToDiv('col-md-6')
                    ->setLabel('validation.attributes.slug'),
                TextArea::create('modification][search_keywords][' . $index, $product)
                    ->addClassesToDiv('col-md-12')
                    ->setLabel('products::admin.attributes.search_keywords')
            );
        }

        $form->doNotShowTopButtons();
        $form->doNotShowBottomButtons();
        return $form;
    }

}
