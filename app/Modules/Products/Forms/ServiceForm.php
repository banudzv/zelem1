<?php

namespace App\Modules\Products\Forms;

use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Products\Models\Service;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Macro\InputForSlug;
use CustomForm\Macro\Slug;
use CustomForm\Macro\Toggle;
use CustomForm\TextArea;
use CustomForm\TinyMce;
use CustomForm\View;
use CustomForm\WysiHtml5;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceForm
 * @package App\Modules\Products\Forms
 */
class ServiceForm implements FormInterface
{
    /**
     * {@inheritDoc}
     * @throws WrongParametersException
     */
    public static function make(?Model $service = null, int $index = 0): Form
    {
        $service = $service ?? new Service();
        $form = new Form();
        $tabs = $form->tabs();
        $mainTab = $tabs->createTab('main');
        $mainTab->fieldSet(6, FieldSet::COLOR_SUCCESS)->add(
            Toggle::create('active')->required(),
            View::create('prices')->setContent(
                view('products::admin.services.prices', compact('service'))
            )
        );
        $mainTab->fieldSetForLang(6)->add(
            InputForSlug::create('name', $service)->required(),
            Slug::create('slug', $service)->required(),
            Input::create('url', $service),
            TinyMce::create('teaser', $service)
        );

        $seoTab = $tabs->createTab('seo');
        $seoTab->fieldSetForLang()->add(
            TinyMce::create('text', $service),
            Input::create('h1', $service),
            Input::create('title', $service),
            TextArea::create('description', $service),
            TextArea::create('keywords', $service)
        );

        return $form;
    }
}
