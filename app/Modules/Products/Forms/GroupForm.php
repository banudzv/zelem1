<?php

namespace App\Modules\Products\Forms;

use App\Components\Form\ObjectValues\ModelForSelect;
use App\Core\Interfaces\FormInterface;
use App\Exceptions\WrongParametersException;
use App\Modules\Brands\Models\Brand;
use App\Modules\Categories\Models\Category;
use App\Modules\Features\Models\Feature;
use App\Modules\ProductsLabels\Models\Label;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupFeatureValue;
use App\Modules\Products\Models\ProductGroupLabel;
use App\Modules\Sales\Models\Sales;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Macro\MultiSelect;
use CustomForm\Macro\Toggle;
use CustomForm\Select;
use CustomForm\Text;
use CustomForm\TinyMce;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Widget;

class GroupForm implements FormInterface
{

    /**
     * Make form
     *
     * @param Model|ProductGroup|null $group
     * @return Form
     * @throws WrongParametersException|Exception
     */
    public static function make(?Model $group = null): Form
    {
        $group = $group ?? new ProductGroup();

        $selectedMainCategory = null;
        $allSelectedLabels = [];
        if ($group->exists) {
            ProductGroupLabel::getRelationsForProduct($group->id)
                ->each(function (ProductGroupLabel $relation) use (&$allSelectedLabels) {
                    $allSelectedLabels[] = $relation->label_id;
                });
        }

        $form = Form::create();
        $tabs = $form->tabs(12);
        $mainInformationTab = $tabs->createTab('admin.tabs.general-information');

        $engineNumbers = $group->id
            ? $group->engineNumbers->pluck('engine_number', 'engine_number')->toArray()
            : [];

        $categories = Category::getDictionaryForSelects();
        $mainFieldSet = $mainInformationTab->fieldSet(4, FieldSet::COLOR_SUCCESS);
        $mainFieldSet->add(
            Toggle::create('active', $group)->required(),
            Select::create('category_id', $group)
                ->add($categories)
                ->setLabel('categories::general.attributes.main-category')
                ->required(),
//            MultiSelect::create('categories[]')
//                ->add($categories)
//                ->setLabel('categories::general.attributes.other-categories')
//                ->setValue($group ? $group->other_categories_ids : []),
            Input::create('position', $group)
                ->setType('number')
                ->setDefaultValue(ProductGroup::DEFAULT_POSITION)
                ->setLabel('products::general.attributes.position'),
            Select::create('brand_id', $group)
                ->model(
                    ModelForSelect::make(
                        Brand::with('current')->get()
                    )->setValueFieldName('current.name')
                )
                ->setPlaceholder('&mdash;'),
            Select::create('feature_id', $group)
                ->setLabel('products::admin.main-feature')
                ->setPlaceholder('&mdash;')
                ->model(
                    ModelForSelect::make(
                        Feature::with('current')->get()
                    )->setValueFieldName('current.name')
                ),
            Select::create('sale_id', $group)
                ->add(Sales::getListForSelect())
                ->setPlaceholder('&mdash;')
                ->setLabel('sales::admin.sales-for-product'),
            MultiSelect::create('labels[]')
                ->addClassesToDiv('col-md-12')
                ->model(ModelForSelect::make(
                    Label::all()
                )->setValueFieldName('current.name'))
                ->setLabel('labels::general.permission-name')
                ->setValue($allSelectedLabels),
            MultiSelect::create('engine_numbers[]')
                ->setClasses('js-data-ajax')
                ->setOptions(['data-href' => route('api.ajax.engine-numbers-by-name', ['groupId' => $group->id ?? 0])])
                ->addClassesToDiv('col-md-12')
                ->setLabel('Номера двигателя')
                ->add($engineNumbers)
                ->setValue($engineNumbers)
        );

        /*if (config('db.products_dictionary.site_status') && !config('db.products_dictionary.select_status')) {
            $dictionary = Dictionary::with(['current'])->whereActive(true)->get()->mapWithKeys(function (Dictionary $dictionary) {
                return [$dictionary->id => $dictionary->current->name];
            })->toArray();
            $selected = $group->dictionaryRelations->map(function (DictionaryRelation $relation) {
                return $relation->dictionary_id;
            })->toArray();
            $mainFieldSet->add(
                MultiSelect::create('dictionaries[]')
                    ->add($dictionary)
                    ->setValue($selected)
                    ->setLabel('products::general.dictionary-values')
                    ->setOptions(['id' => 'dictionary-select'])
            );
        }*/

        $mainInformationTab->fieldSetForLang(8)->add(
            Input::create('name', $group)->required(),
            TinyMce::create('text', $group)
                ->setOptions(['style' => 'height: 205px'])
                ->setLabel(__('products::general.attributes.text'))
        );
        $mainInformationTab->fieldSetForView('products::admin.groups.products', ['group' => $group], 12);

        if ($group->exists) {
            $tabs->createTab('products::admin.tabs.features')
                ->fieldSet()
                ->add(
                    Text::create()->setDefaultValue(Widget::show(
                        'features::select-in-product',
                        ProductGroupFeatureValue::getLinkedFeaturesAsArray($group),
                        $group->feature_id
                    ))
                );
        }

        return $form;
    }

}
