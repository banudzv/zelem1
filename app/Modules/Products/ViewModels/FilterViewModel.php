<?php

namespace App\Modules\Products\ViewModels;

use Illuminate\Support\Arr;

/**
 * Class FilterViewModel
 * @package App\Modules\Products\ViewModels
 */
class FilterViewModel
{
    /**
     * @var array Parameters from the query.
     */
    protected $query;
    /**
     * @var array Vendors list [id => name]
     */
    protected $vendors;
    /**
     * @var array Models list [id = name]
     */
    protected $models;
    /**
     * @var array Volumes list [id => name]
     */
    protected $volumes;
    /**
     * @var array Powers list [id => name]
     */
    protected $powers;
    /**
     * @var array [engine_number, engine_number,..]
     */
    protected $engineNumbers;
    /**
     * @var int Min price in the category.
     */
    protected $minPrice;
    /**
     * @var int Max price in the category.
     */
    protected $maxPrice;

    /**
     * FilterViewModel constructor.
     *
     * @param array $query Car parameters from the query string.
     */
    public function __construct(array $query = [])
    {
        $this->query = $query;
    }

    /**
     * Stores min price.
     *
     * @param int $price
     */
    public function setMinPrice(int $price): void
    {
        $this->minPrice = $price;
    }

    /**
     * Returns min price.
     *
     * @return int
     */
    public function getMinPrice(): int
    {
        return $this->minPrice ?? 0;
    }

    /**
     * Stores max price.
     *
     * @param int $price
     */
    public function setMaxPrice(int $price): void
    {
        $this->maxPrice = $price;
    }

    /**
     * Returns max price.
     *
     * @return int
     */
    public function getMaxPrice(): int
    {
        return $this->maxPrice ?? 0;
    }

    /**
     * Stores vendors.
     *
     * @param array $vendors
     */
    public function setVendors(array $vendors): void
    {
        $this->vendors = $vendors;
    }

    /**
     * Returns vendors.
     *
     * @return array
     */
    public function getVendors(): array
    {
        return $this->vendors ?? [];
    }

    /**
     * Stores models.
     *
     * @param array $models
     */
    public function setModels(array $models): void
    {
        $this->models = $models;
    }

    /**
     * Returns models.
     *
     * @return array
     */
    public function getModels(): array
    {
        return $this->models ?? [];
    }

    /**
     * Stores volumes.
     *
     * @param array $volumes
     */
    public function setVolumes(array $volumes): void
    {
        $this->volumes = $volumes;
    }

    /**
     * Returns volumes.
     *
     * @return array
     */
    public function getVolumes(): array
    {
        return $this->volumes ?? [];
    }

    /**
     * Stores powers.
     *
     * @param array $powers
     */
    public function setPowers(array $powers): void
    {
        $this->powers = $powers;
    }

    /**
     * Returns powers.
     *
     * @return array
     */
    public function getPowers(): array
    {
        return $this->powers ?? [];
    }

    /**
     * Stores engine numbers.
     *
     * @param array $engineNumbers
     */
    public function setEngineNumbers(array $engineNumbers): void
    {
        $this->engineNumbers = $engineNumbers;
    }

    /**
     * Returns engine numbers.
     *
     * @return array
     */
    public function getEngineNumbers(): array
    {
        return $this->engineNumbers ?? [];
    }

    /**
     * Returns chosen vendor ID from the query parameters.
     *
     * @return int|null
     */
    public function getChosenVendorId(): ?int
    {
        return Arr::get($this->query, 'vendor');
    }

    /**
     * Returns chosen model ID from the query parameters.
     *
     * @return int|null
     */
    public function getChosenModelId(): ?int
    {
        return Arr::get($this->query, 'model');
    }

    /**
     * Returns chosen volume ID from the query parameters.
     *
     * @return int|null
     */
    public function getChosenVolumeId(): ?int
    {
        return Arr::get($this->query, 'volume');
    }

    /**
     * Returns chosen power ID from the query parameters.
     *
     * @return int|null
     */
    public function getChosenPowerId(): ?int
    {
        return Arr::get($this->query, 'power');
    }

    /**
     * Returns chosen engine number from the query parameters.
     *
     * @return string|null
     */
    public function getChosenEngineNumber(): ?string
    {
        return Arr::get($this->query, 'number');
    }
}
