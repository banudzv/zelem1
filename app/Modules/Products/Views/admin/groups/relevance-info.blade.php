@php
    /** @var App\Modules\Products\Models\ProductGroup $group */
    $relevance = $group->relevant_product->getRelevance();
    $relevanceMap = $group->relevant_product->getRelevanceMap()
@endphp
<span class="label label-default"
      data-toggle="tooltip"
      data-html="true"
      data-trigger="click"
      data-title="Бренд: {{ $relevanceMap['brand'] }}<br> Категория: {{ $relevanceMap['category'] }}<br> Товар: {{ $relevanceMap['product'] }}">
    {{ $relevance }}
</span>
