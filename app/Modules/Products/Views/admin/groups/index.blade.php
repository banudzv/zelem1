@php
    /** @var \App\Modules\Products\Models\ProductGroup[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator $groups */
    /** @var string $filter */
$className = \App\Modules\Products\Models\ProductGroup::class;

@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter !!}
        <div class="box">
            <div class="box-body table-responsive no-padding mailbox-messages">
                <table class="table table-hover">
                    <tr>
                        <th>
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle">
                                <i class="fa fa-square-o"></i>
                            </button>
                        </th>
                        <th></th>
                        <th>{{ __('validation.attributes.name') }}</th>
                        <th>{{ __('validation.attributes.price') }}</th>
                        <th>{{ __('validation.attributes.brand_id') }}</th>
                        <th>{{ __('categories::general.attributes.main-category') }}</th>
                        <th>
                            {{ __('products::general.attributes.relevance') }}
                        </th>
                        <th>{{ __('products::general.attributes.position') }}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($groups AS $group)
                        <tr data-id="{{$group->id}}">
                            <td width="30"><input type="checkbox"></td>
                            <td><img style="max-width: 40px;" src="{{ $group->preview->link('40x40') }}"/></td>
                            <td style="vertical-align: middle">{{ $group->name }}</td>
                            <td>{!! $group->relevant_product ? $group->relevant_product->formatted_price_for_admin : '&mdash;' !!}</td>
                            <td>
                                @if($group->brand)
                                {!!
                                    Html::link($group->brand->link_in_admin_panel, $group->brand->current->name, ['target' => '_blank'])
                                !!}
                                @else&mdash;@endif
                            </td>
                            <td>
                                @if($group->category)
                                    {!! Html::link($group->category->link_in_admin_panel, $group->category->current->name, ['target' => '_blank']) !!}
                                @else
                                    &nbsp;
                                @endif
                            </td>
                            <td>
                                @include('products::admin.groups.relevance-info', ['product' => $group->relevant_product])
                            </td>
                            <td>
                                {!! Widget::show('products::groups::sortable',$group) !!}
                            </td>
                            <td>{!! Widget::active($group) !!}</td>
                            <td>
                                @if(CustomRoles::can('products', 'changeFeature'))
                                    {!!
                                        \App\Components\Buttons::custom(
                                            Html::tag('i', '', ['class' => ['fa', 'fa-exchange']]),
                                            'admin.groups.change-feature',
                                            $group->id,
                                            CustomRoles::can('products.edit'),
                                            'btn-foursquare'
                                        )
                                    !!}
                                @endif
                                {!! \App\Components\Buttons::edit('admin.groups.edit', $group->id) !!}
                                {!! \App\Components\Buttons::delete('admin.groups.destroy', $group->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $groups->appends(request()->except('page'))->links() }}</div>
        <span id="parameters-name" data-name="{{ $className }}"></span>

    </div>
@stop

@push('scripts')
    <script>
        $('[data-toggle="tooltip"]').tooltip();
    </script>
@endpush