@php
    /** @var \App\Modules\Products\Models\ProductGroup $group */
@endphp
<input style="width: 50px; display: inline-block;" type="text" class="form-control" value="{{$group->position}}"
       data-route="{{ $group->sorted_group_link }}"/>
<a href="#" style="display: inline-block;" class="setPosition">OK</a>