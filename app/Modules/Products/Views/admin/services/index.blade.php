@php
/** @var $services \App\Modules\Products\Models\Service[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th></th>
                        <th>{{ __('validation.attributes.name') }}</th>
                        <th></th>
                    </tr>
                    @foreach($services AS $service)
                        <tr>
                            <td>{{ $service->current->name }}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.services.edit', [$service->id]) !!}
                                {!! \App\Components\Buttons::delete('admin.services.destroy', $service->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $services->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
