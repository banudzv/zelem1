@php
/** @var $service \App\Modules\Products\Models\Service */
/** @var $errors \Illuminate\Support\ViewErrorBag */
$priceError = false;
foreach ($errors->getMessages() as $key => $particularErrors) {
    if (\Illuminate\Support\Str::startsWith($key, 'ServiceOption')) {
        $priceError = true;
        break;
    }
}
@endphp

<div class="callout callout-warning">
    <p>@lang('products::messages.services.warning')</p>
</div>

<table>
    <thead>
    <tr>
        @foreach(config('languages', []) as $lang => $_)
            <td>@lang('validation.attributes.name') ({{ $lang }})</td>
        @endforeach
        <td>@lang('products::general.cost')</td>
        <td></td>
    </tr>
    </thead>
    <tbody>
    @foreach($service->options as $option)
        <tr>
            <input type="hidden" name="ServiceOptionId[]" value="{{ $option->id }}">
            @foreach(config('languages', []) as $lang => $_)
                <td><input type="text" class="form-control" name="ServiceOptionName[{{ $lang }}][]" value="{{ $option->dataFor($lang)->name }}" /></td>
            @endforeach
            <td><input type="number" min="0" class="form-control" name="ServiceOptionPrice[]" value="{{ $option->price }}" /></td>
            <td><span class="btn delete-option"><i class="fa fa-minus"></i></span></td>
        </tr>
    @endforeach
    @foreach(old('ServiceOptionPrice', []) as $index => $price)
        <tr>
            <input type="hidden" name="ServiceOptionId[]">
            @foreach(config('languages', []) as $lang => $_)
                <td><input type="text" class="form-control" name="ServiceOptionName[{{ $lang }}][]" value="{{ old("ServiceOptionName.$lang.$index") }}" /></td>
            @endforeach
            <td><input type="number" min="0" class="form-control" name="ServiceOptionPrice[]" value="{{ $price }}" /></td>
            <td><span class="btn delete-option"><i class="fa fa-minus"></i></span></td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <input type="hidden" name="ServiceOptionId[]">
        @foreach(config('languages', []) as $lang => $_)
            <td><input type="text" class="form-control option-name" data-lang="{{ $lang }}" /></td>
        @endforeach
        <td><input type="number" min="0" class="form-control option-price" /></td>
        <td><span class="btn" id="add-option"><i class="fa fa-plus"></i></span></td>
    </tr>
    </tfoot>
</table>

@if($priceError === true)
    <div class="callout callout-danger" style="margin-bottom: 0; margin-top: 20px;">
        <p>@lang('products::messages.services.error')</p>
    </div>
@endif
