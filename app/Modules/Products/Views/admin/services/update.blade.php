@php
/** @var \CustomForm\Builder\Form $form */
/** @var \App\Modules\Products\Models\Service $service */
$form->buttons->showCloseButton(route('admin.services.index'));
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['method' => 'PUT', 'route' => ['admin.services.update', $service->id]]) !!}
        {!! $form->render() !!}
    {!! Form::close() !!}
@stop

@push('scripts')
    <script>
        (function () {
            $('body').on('click', '.delete-option', function () {
                $(this).closest('tr').remove();
            });
            $('body').find('#add-option').on('click', function () {
                const $tr = $(this).closest('tr').clone();
                $tr.find('.option-name').each(function () {
                    $(this).attr('name', 'ServiceOptionName[' + $(this).data('lang') + '][]');
                });
                $tr.find('.option-price').attr('name', 'ServiceOptionPrice[]');
                $tr.find('td:last').html('<span class="btn delete-option"><i class="fa fa-minus"></i></span>');
                $(this).closest('table').find('tbody').append($tr);
                $(this).closest('tr').find('input').val('');
            });
        })();
    </script>
@endpush
