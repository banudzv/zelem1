@php
    /** @var \App\Modules\Products\Models\Product $product */
    /** @var bool $collapse */
    /** @var int $index */
    $collapse = $collapse ?? false;
@endphp
<div class="box box-{{ $product->is_main ? 'success' : 'primary' }} box-solid {{ $collapse ? 'collapsed-box' : 'loaded' }} modification recently-created" data-index="{{ $index }}">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ $product->name . ' - ' . $product->formatted_price_for_admin }}
        </h3>
        <input class="form-control" id="modificationid{{ $index }}" name="modification[id][{{ $index }}]" type="hidden" value="{{$product->id}}">

        <div class="box-tools pull-right">
            <div style="display: inline-block;border-right:1px solid #ccc;padding-right: 9px;" class="form-group block-position">
                <span>@lang('products::general.attributes.position')</span>
                <input style="width: 50px; display: inline-block;" type="text" class="form-control" value="{{$product->position}}"
                       data-route="{{ $product->sorted_product_link }}"/>
                <a href="#" style="display: inline-block; border: #0a0a0a" class="setPosition">OK</a>
            </div>

            <div style="@if(($product->exists && $product->value)) display: inline-block;padding-left: 7px; @else display: none; @endif" class="form-group block-feature">
                <label for="modification[value_id][{{ $index }}]" class="control-label">{{($product->exists && $product->value) ? $product->value->feature->current->name : trans('products::admin.attributes.modification-value')}}</label>
                <select style="color: black;" class="feature-value-select" id="modificationvalue_id{{ $index }}" name="modification[value_id][{{ $index }}]">
                    @if(($product->exists && $product->value))

                        @foreach($product->value->feature->values as $featureValue)
                            <option {{$product->value_id == $featureValue->id ? 'selected' : ''}} value="{{$featureValue->id}}">{{$featureValue->current->name}}</option>
                        @endforeach
                    @endif

                </select>
            </div>
            <button type="button" class="btn btn-box-tool" data-locotrade="clone" data-url="{{ route('admin.products.clone', $product->id) }}">
                <i class="fa fa-clone"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-locotrade="delete" data-url="{{ route('admin.products.destroy', $product->id) }}">
                <i class="fa fa-trash"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-locotrade="main" data-url="{{ route('admin.products.main', $product->id) }}">
                <i class="fa fa-toggle-off"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-locotrade="collapse" data-url="{{ route('admin.products.edit', [$product->id, 'index' => $index]) }}">
                <i class="fa fa-{{ $collapse ? 'plus' : 'minus' }}"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        @if(!$collapse)
            {!! \App\Modules\Products\Forms\ProductForm::make($product, $index)->render() !!}
        @endif
    </div>
</div>
