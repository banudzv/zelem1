@php
/** @var \App\Modules\Products\Models\Product $product */
@endphp
<div class="grid grid--3 _flex-nowrap _justify-center _justify-between">
    <div class="gcell gcell--8 gcell--sm-6 gcell--md-4 gcell--xl-5 _flex-noshrink">
        @if($product->is_custom_goods)
            {!! Widget::show('custom-goods::button', $product->id) !!}
        @else
            {!! Widget::show('orders::cart::add-button', $product, $product->id, $product->is_available, true) !!}
        @endif
    </div>
    <div class="gcell _pl-sm _xl-pl-none gcell--4 gcell--sm-3 gcell--md-4 _nml-xs">
        <div class="action-bar__wrapper">
            <div class="action-bar__control">
                <div class="action-bar-control action-bar-control--compare" data-comparelist-toggle={{$product->id}}>
                    {!! SiteHelpers\SvgSpritemap::get('icon-compare', ['class' => 'action-bar-control__icon']) !!}
                </div>
            </div>
            <div class="action-bar__control action-bar__control--wishlist">
                <div class="action-bar-control action-bar-control--wishlist" data-wishlist-toggle={{$product->id}}>
                    {!! SiteHelpers\SvgSpritemap::get('icon-wishlist', ['class' => 'action-bar-control__icon']) !!}
                </div>
            </div>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="gcell _pl-sm _sm-show">
        <div class="action-bar__wrapper">
            <div class="action-bar__control action-bar__control--buy-one-click">
                <div class="action-bar-control js-init" data-mfp="ajax" data-trigger-event="oneClickBuy" data-mfp-src="{{route('fast-orders-popup', $product -> id)}}">
                    {!! SiteHelpers\SvgSpritemap::get('icon-phone', ['class' => 'action-bar-control__icon']) !!}
                    <div class="action-bar-control__text">
                        Купить в <br> 1 клик
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
