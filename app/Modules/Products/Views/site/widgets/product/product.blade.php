@php
/** @var \Illuminate\Support\Collection $tabs */
@endphp
<div class="js-init _pt-md _ms-pt-none" data-product-tabs>
    <div class="tabs-nav">
       <div class="tabs-nav__scroll-container">
           @foreach($tabs as $index => $tab)
               @if($tab['widget'])
                   <div class="tabs-nav__button tabs-nav__button--normal{{ $index === 'main' ? ' is-active' : '' }}"
                        data-wstabs-ns="product"
                        data-wstabs-button="{{ $index }}">
                    <span>{{ $tab['name'] }}
                        @if(isset($tab['count']) && (int)$tab['count'] > 0)
                            <span class=" _color-dark-mint text--semi-bold">{{ $tab['count'] }}</span>
                        @endif
                    </span>
                   </div>
               @endif
           @endforeach
       </div>
    </div>
    <div class="tabs-blocks _mb-xxl">
        @foreach($tabs as $index => $tab)
            @if($tab['widget'])
                <div class="tabs-blocks__block{{ $index === 'main' ? ' is-active' : ' product-aside--show' }}"
                        data-wstabs-ns="product"
                        data-wstabs-block="{{ $index }}">
                    {!! $tab['widget'] !!}
                </div>
            @endif
        @endforeach
        @include('products::site.widgets.product.product-aside.product-aside', ['product' => $product])
    </div>
</div>
