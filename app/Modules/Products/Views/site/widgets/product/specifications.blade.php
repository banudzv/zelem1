@php
/** @var array $features */
/** @var \App\Modules\Products\Models\Product $product */
/** @var $years string */
/** @var $engineNumbers \App\Modules\Engine\Models\EngineNumber[]|\Illuminate\Support\Collection */

$cars = implode(', ', array_unique($engineNumbers->map(function (\App\Modules\Engine\Models\EngineNumber $engineNumber) {
    return $engineNumber->vendor->name . ' ' . $engineNumber->model->name;
})->unique()->sort()->toArray()));
$volumes = implode(', ', array_unique($engineNumbers->unique('volume_id')->map(function (\App\Modules\Engine\Models\EngineNumber $engineNumber) {
    return $engineNumber->volume->name;
})->toArray()));
$powers = implode(', ', array_unique($engineNumbers->unique('power_id')->map(function (\App\Modules\Engine\Models\EngineNumber $engineNumber) {
    return $engineNumber->power->current->name;
})->toArray()));
$numbers = $engineNumbers->unique('engine_number')->implode('engine_number', ', ');
@endphp
<div class="title title--size-h2 title--product-title _color-dark-blue title--normal">@lang('products::site.main-specifications') <span class="_color-dark-mint">{{ $product->name }}</span></div>
@foreach($features as $featureName => $values)
    <div class=" _pt-md _plr-def _nmlr-def _ms-mlr-none">
        <div class="grid grid--1 _ms-nml-md">
            <div class="gcell gcell--5 gcell--md-4 _flex">
                <strong class="text text--width-200 text--size-16 text--semi-bold _color-dark-blue _def-flex-noshrink _pr-xs">{{ $featureName }}</strong>
                <div class="dots"></div>
            </div>
            <div class="gcell gcell--7 gcell--md-8 _pl-xs">
                <span class="text text--size-16 _color-blue-smoke">{!! $values !!}</span>
            </div>
        </div>
    </div>
@endforeach
@if($cars)
<div class=" _pt-md _plr-def _nmlr-def _ms-mlr-none">
    <div class="grid grid--1 _ms-nml-md">
        <div class="gcell gcell--5 gcell--md-4 _flex">
            <strong class="text text--width-200 text--size-16 text--semi-bold _color-dark-blue _def-flex-noshrink _pr-xs">
                @lang('products::site.engine.car')
            </strong>
            <div class="dots"></div>
        </div>
        <div class="gcell gcell--7 gcell--md-8 _pl-xs">
            <span class="text text--size-16 _color-blue-smoke">{{ $cars }}</span>
        </div>
    </div>
</div>
@endif
@if($volumes)
<div class=" _pt-md _plr-def _nmlr-def _ms-mlr-none">
    <div class="grid grid--1 _ms-nml-md">
        <div class="gcell gcell--5 gcell--md-4 _flex">
            <strong class="text text--width-200 text--size-16 text--semi-bold _color-dark-blue _def-flex-noshrink _pr-xs">
                @lang('products::site.engine.volume')
            </strong>
            <div class="dots"></div>
        </div>
        <div class="gcell gcell--7 gcell--md-8 _pl-xs">
            <span class="text text--size-16 _color-blue-smoke">{{ $volumes }}</span>
        </div>
    </div>
</div>
@endif
@if($powers)
<div class=" _pt-md _plr-def _nmlr-def _ms-mlr-none">
    <div class="grid grid--1 _ms-nml-md">
        <div class="gcell gcell--5 gcell--md-4 _flex">
            <strong class="text text--width-200 text--size-16 text--semi-bold _color-dark-blue _def-flex-noshrink _pr-xs">
                @lang('products::site.engine.power')
            </strong>
            <div class="dots"></div>
        </div>
        <div class="gcell gcell--7 gcell--md-8 _pl-xs">
            <span class="text text--size-16 _color-blue-smoke">{{ $powers }}</span>
        </div>
    </div>
</div>
@endif
@if($numbers)
<div class=" _pt-md _plr-def _nmlr-def _ms-mlr-none">
    <div class="grid grid--1 _ms-nml-md">
        <div class="gcell gcell--5 gcell--md-4 _flex">
            <strong class="text text--width-200 text--size-16 text--semi-bold _color-dark-blue _def-flex-noshrink _pr-xs">
                @lang('products::site.engine.number')
            </strong>
            <div class="dots"></div>
        </div>
        <div class="gcell gcell--7 gcell--md-8 _pl-xs">
            <span class="text text--size-16 _color-blue-smoke">{{ $numbers }}</span>
        </div>
    </div>
</div>
@endif
@if($years)
<div class=" _pt-md _plr-def _nmlr-def _ms-mlr-none">
    <div class="grid grid--1 _ms-nml-md">
        <div class="gcell gcell--5 gcell--md-4 _flex">
            <strong class="text text--width-200 text--size-16 text--semi-bold _color-dark-blue _def-flex-noshrink _pr-xs">
                @lang('products::site.engine.year')
            </strong>
            <div class="dots"></div>
        </div>
        <div class="gcell gcell--7 gcell--md-8 _pl-xs">
            <span class="text text--size-16 _color-blue-smoke">{{ $years }}</span>
        </div>
    </div>
</div>
@endif
