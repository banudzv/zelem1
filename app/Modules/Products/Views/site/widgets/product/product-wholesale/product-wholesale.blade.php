<table class="wholesale-price _ptb-sm">
    <tr>
        <th class="wholesale-price__title">@lang('products::general.amount')</th>
        <th class="wholesale-price__title">@lang('products::general.cost')</th>
    </tr>
    @foreach($wholesale as $item)
        <tr>
            <td class="wholesale-price__label">{{$item->quantity}}</td>
            <td class="wholesale-price__current">{!! $item->formatted_price !!}</td>
        </tr>
    @endforeach
</table>
