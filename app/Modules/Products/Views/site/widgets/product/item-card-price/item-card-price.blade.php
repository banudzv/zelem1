<div class="product-price">
    <div class="product-price__current {{isset($old_value) ? 'is-active' : null}}">{{ $value }}</div>
    @if (isset($old_value))
        <div class="product-price__old">{{ $old_value }}</div>
    @endif
</div>
