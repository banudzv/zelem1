@php
/** @var \App\Modules\Products\Models\Product $product */
/** @var $years string */
/** @var $engineNumbers \App\Modules\Engine\Models\EngineNumber[] */
/** @var $services \App\Modules\Products\Models\Service[]|\Illuminate\Support\Collection */
@endphp

@push('scripts')
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ee0f8b6c9a8541c"></script>
@endpush

<div class="grid _md-nmlr-def _items-start">
    <div class="gcell gcell--12 gcell--md-7 _md-plr-def">
        <div class="_posr">
            @include('products::site.widgets.product.product-facade.product-facade-slider', [
                'images' => $product->gallery(),
                'product' => $product,
            ])
        </div>
    </div>
    <div class="gcell gcell--12 gcell--md-5 _md-pr-def">
        <div class="grid _justify-between _items-center _mb-md">
            <div class="gcell _pr-sm">
                <div class="grid">
                    <div class="gcell _pr-def">
                        {{-- @if($product->group->products->count() > 1)
                            <div class="packages grid _nm-xs _pb-def">
                                @foreach($product->group->products as $modification)
                                    <div class="gcell _p-xs">
                                        <div class="packages__item">
                                            <a href="{{ $modification->site_link }}" class="packages__link packages__link--big{{ $modification->id === $product->id ? ' is-active' : null }}">
                                                @if($modification->value_id)
                                                    {{ $modification->value->current->name }}
                                                @else
                                                    {{ $modification->name }}
                                                @endif
                                            </a>
                                            @if ($modification->id !== $product->id)
                                                <div class="packages__dropdown">
                                                    @if((bool)config('db.products.show-availability', true) === true)
                                                        @include('products::site.widgets.product.item-availability.item-availability', [
                                                            'available' => $modification->available,
                                                            'text' => __(config('products.availability.' . $modification->available)),
                                                        ])
                                                    @endif
                                                    <div class="product-price">
                                                        <div
                                                            class="product-price__old">{{ $modification->formatted_old_price ?? '' }}</div>
                                                        <div
                                                            class="product-price__current">{{ $modification->formatted_price }}</div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif --}}
                        <div class="product-inform">
                            @if((bool)config('db.products.show-availability', true) === true)
                                @include('products::site.widgets.product.item-availability.item-availability', [
                                    'available' => $product->available,
                                    'text' => __(config('products.availability.' . $product->available)),
                                ])
                            @endif
                        </div>
                    </div>
                    <div class="gcell _flex-noshrink _md-show">
                        @if($product->group && $product->group->relationExists('comments'))
                            <div class="grid _items-center">
                                <div class="gcell">
                                    @include('site._widgets.stars-block.stars-block', [
                                        'count' => $product->group->mark,
                                    ])
                                </div>
                                <div class="gcell _pl-sm">
                                    <a class="link link--underline text--semi-bold" data-wstabs-ns="product"
                                       data-wstabs-button="review">
                                        {{-- @if ($product->group->comments->count())
                                            {!! trans_choice('products::site.products-review', $product->group->comments->count()) !!}
                                        @else
                                            @lang('products::site.products-review-write')
                                        @endif --}}
                                        {{$product->group->comments->count()}}
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="gcell _pt-none _xs-pt-lg _ms-pt-none _md-pt-lg">
                @if($product->wholesale->isNotEmpty())
                    @include('products::site.widgets.product.product-wholesale.product-wholesale', [
                        'wholesale' => $product->wholesale,
                    ])
                @endif
            </div>
            <div class="gcell _md-show">
                @if($product->vendor_code)
                    @include('site._widgets.elements.vendor-code.vendor-code', [
                        'code' => $product->vendor_code,
                    ])
                @endif
            </div>
        </div>
        {!!  Widget::show('products_dictionary::show-in-product', $product)  !!}
        <div class="product-sticky">
            @if($product->old_price && $product->group->sale)
                <div class="grid grid--1">
                    <div class="gcell">
                        <div class="promo promo--red">
                            <div class="promo__title">
                                {{ __('sales::site.sales') }}
                            </div>
                            <div class="promo__text">
                                <a href="{{ $product->group->sale->link }}">
                                    {!! $product->group->sale->teaser !!}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="grid">
                <div class="gcell _pl-def">
                    {!! Widget::show('products::price', $product) !!}
                </div>
            </div>
            {!! Widget::show('products::controls', $product, true) !!}
            {{-- <div class="grid  _items-center _justify-center _def-justify-start _mb-def _nml-sm">
                <div class="gcell">

                </div>
                <div class="gcell _pl-sm">
                    @if($product->is_available)
                        {!! Widget::show('fast-orders::button', $product->id) !!}
                    @endif
                </div>
            </div> --}}
        </div>
        @if(config('db.products.sizes_text_'.\Lang::getLocale()))
            <div class="_pb-md _text-right">
                <span class="conditions-item__link link link--underline link--dark js-init" data-mfp="ajax"
                      data-mfp-src="{{ route('site.product.info-popup', 'sizes') }}">
                    @lang('products::site.sizes')
                </span>
            </div>
        @endif
        {!! Widget::show('products-services::product-page', $services) !!}
    </div>
</div>
<div class="grid _pt-lg _md-pt-xxl _def-pl-xxl">
    <div class="gcell gcell--12 gcell--md-7 _pr-def _md-plr-def _md-pr-xxl ">
        @include('products::site.widgets.product.specifications', [
            'product' => $product,
            'features' => $product->features_list,
            'engineNumbers' => $engineNumbers,
            'years' => $years,
        ])
        <div class="_pt-xl _pl-xxs">
            <a class="link link--underline link--icon-small text--semi-bold link--dark _mt-xl js-init"
               data-scroll-window='{"target": "up"}' data-wstabs-ns="product" data-wstabs-button="features">
                @lang('products::site.all-features')
                <div class="icon">
                    {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right') !!}
                </div>
            </a>
        </div>

        {{-- <div class="text--size-15 _pt-def _ms-show">
            {!! Widget::show('products::description', $product, true) !!}
        </div> --}}
    </div>
    <div class="gcell gcell--12 gcell--md-5 _pt-lg _pt-none">
        <hr class="separator _color-gray1 _mtb-md _ms-hide">
        <div class="_flex _justify-between _items-center">
            <div class="title title--size-h2 title--product-title _color-dark-blue title--normal _mb-none">
                {{ __('products::site.products-users-review') }}
                <span class="_color-dark-mint">{{$product->group->comments->count()}}</span></div>
            <button class="button button--size-normal button--theme-empty js-init" data-scroll-window='{"target": "up"}'
                    data-wstabs-ns="product" data-wstabs-button="review">
                <div class="button__text">{{ __('products::site.products-review-write') }}</div>
            </button>
        </div>
        <div class="_mt-def _md-mt-xl">
            @foreach($product->group->getComments(config('db.comments.groups-per-page-site', 5)) as $comment)
                <div class="review">
                    <div class="review__head">
                        <div class="grid _items-center">
                            <div class="gcell">
                                <div class="review__user">{{ $comment->name }} /&nbsp;</div>
                            </div>
                            <div class="gcell">
                                <time class="review__time"
                                      datetime="{{ $comment->published_at->format('Y-m-d') }}">{{ $comment->publish_date }}</time>
                            </div>
                            @if($comment->mark)
                                <div class="gcell _pl-md">
                                    @include('site._widgets.stars-block.stars-block', [
                                        'count' => $comment->mark,
                                    ])
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="review__body">
                        <div class="review__content">{!! $comment->comment !!}</div>
                    </div>
                    @if($comment->answered_at)
                        <div class="review">
                            <div class="review__head">
                                <div class="review__user">{{ config('db.comments.admin-name', 'Administrator') }}</div>
                                <time class="review__time"
                                      datetime="{{ $comment->answered_at->format('Y-m-d') }}">{{ $comment->answer_date }}</time>
                            </div>
                            <div class="review__body">
                                <div class="review__content">{!! $comment->answer !!}</div>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
        <a class="link link--underline link--icon-small text--semi-bold link--dark _mt-xl js-init"
           data-scroll-window='{"target": "up"}' data-wstabs-ns="product" data-wstabs-button="review">
            {{ __('reviews::site.all-reviews') }}
            <div class="icon">
                {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right') !!}
            </div>
        </a>
        <hr class="separator _color-gray1 _mtb-md _ms-hide">
    </div>
</div>
