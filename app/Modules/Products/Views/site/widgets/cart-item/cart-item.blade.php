@php
/** @var \App\Modules\Products\Models\Product $product */
/** @var int $quantity */
/** @var \Illuminate\Support\Collection|\App\Modules\Products\Models\ServicePrice[]|null $services */
$productLink = sprintf("href=\"%s\"", $product->site_link);
$productId = $product->id;
$productName = $product->name;
$image = $product->preview;

$productPrice = $product->formatted_price;
$itemPrice = calc($product->price * $quantity);
foreach ($services ?? [] as $service) {
    $itemPrice += $service->price * $quantity;
}
@endphp

<div class="cart-item js-init"
     data-product="{{ r2d2()->attrJsonEncode($product->ecomm_data, JSON_UNESCAPED_UNICODE) }}"
     data-product-id="{{ $productId }}" {!! isset($dictionaryId) ? 'data-dictionary-id="'. $dictionaryId .'"' : null !!}>
    <form onsubmit="event.preventDefault()">
        {!! Form::hidden('old_dictionary_id', $dictionaryId ?? '') !!}
        <div class="grid _flex-nowrap">
            <div class="gcell gcell--auto _flex-noshrink">
                <div class="cart-item__options">
                    <div class="dropdown dropdown--to-right js-init" data-toggle>
                        <div class="dropdown__head" data-toggle-trigger>
                            <div class="dropdown__head-svg">{!! SiteHelpers\SvgSpritemap::get('icon-close', []) !!}</div>
                        </div>
                        <div class="dropdown__body" data-toggle-content>
{{--                            <div class="cart-item__sure-delete" data-sure-show>--}}
{{--                                <div class="cart-item__sure-delete-text">--}}
{{--                                   <span> @lang('products::site.messages.delete-product-from-the-cart-warning')</span>--}}
{{--                                </div>--}}
{{--                                <div class="cart-item__yes" data-cart-action="delete" data-product-id="{{ $productId }}">--}}
{{--                                    @lang('products::global.yes')--}}
{{--                                </div>--}}
{{--                                <div class="cart-item__no" data-sure-close>--}}
{{--                                    @lang('products::global.no')--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="grid" style="height: 100%;">
                                <div class="gcell gcell--12 gcell--ms-auto _ms-pl-def _flex-grow">
                                    <div class="grid _justify-center _items-center _nmlr-sm _md-nmlr-lg _text-center" style="height: 100%;">
                                        {!! Widget::show('wishlist::move', $productId) !!}
                                        <div class="gcell gcell--4 _plr-sm _md-plr-lg _ptb-sm">
                                            <a data-cart-action="delete" data-trigger-event="removeFromCartAll" data-product-id="{{ $productId }}" class="cart-item__action cart-item__action--delete">
                                                {!! SiteHelpers\SvgSpritemap::get('icon-close', []) !!}
                                                <div class="cart-item__action-text">@lang('products::site.cart.delete')</div>
                                            </a>
                                        </div>
                                        <div class="gcell gcell--4 _plr-sm _md-plr-lg _ptb-sm">
                                            <a class="cart-item__action cart-item__action--cancel" data-toggle-close>
                                                {!! SiteHelpers\SvgSpritemap::get('icon-back', []) !!}
                                                <div class="cart-item__action-text">@lang('products::site.cart.cancel')</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gcell gcell--auto _md-plr-def _flex-noshrink">
                <a {!! $productLink !!} class="cart-item__image" data-trigger-event="productClick">
                    {!! Widget::show('image', $image, 'small') !!}
                </a>
            </div>
            <div class="gcell gcell--auto _flex-grow _pt-sm">
                <div class="grid">
                    <div class="gcell gcell--12">
                        <a {!! $productLink !!} class="cart-item__name" data-trigger-event="productClick">{{ $productName }}</a>
                    </div>
                    @if($dictionaryId)
                        {!! Widget::show('products_dictionary::show-in-cart', $product, $dictionaryId) !!}
                    @endif
                    <div class="gcell gcell--12">
                        <div class="grid _justify-between _sm-flex-nowrap _pt-xs">
                            <div class="gcell _flex-noshrink _pr-lg _ms-show _flex _flex-column _items-start">
                                <div class="cart-item__unit-price">
                                    @lang('products::site.product-price'): <span>{{ $productPrice }}</span>
                                </div>
                                @foreach($services ?? [] as $service)
                                    <div class="cart-item__optional">
                                        @lang('products::site.add-service'):
                                        <span class="text--dark-blue">
                                            {{ $service->current->name }} {{ format_only($service->price) }}
                                        </span>
                                    </div>
                                @endforeach
                            </div>
                            <div class="gcell _flex-noshrink">
                                <div class="cart-item__unit-quantity">
                                    <div class="input-quantity js-init" data-quantity>
                                        <div class="grid _justify-between _items-center _flex-nowrap">
                                            <div class="gcell gcell--auto _flex-grow _flex-order-2">
                                                <input class="input-quantity__field" type="tel" name="quantity" value="{{ $quantity }}" data-quantity="input" data-product-quantity>
                                            </div>
                                            <div class="gcell gcell--auto _flex-noshrink _flex-order-3">
                                                <div class="input-quantity__increase" data-quantity="increase" onclick="fbq('trackCustom', 'AddToCart', {{ $product->FBPixelData }})" data-trigger-event="addToCart"></div>
                                            </div>
                                            <div class="gcell gcell--auto _flex-noshrink _flex-order-1">
                                                <div class="input-quantity__decrease" data-quantity="decrease" data-trigger-event="removeFromCart"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gcell _flex-noshrink _pl-xs">
                                <div class="cart-item__unit-price-total">{{ format_only($itemPrice) }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
