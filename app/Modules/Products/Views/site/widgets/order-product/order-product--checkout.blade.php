@php
/** @var \App\Modules\Products\Models\Product $product */
/** @var string $formattedPrice */
/** @var string $formattedAmount */
/** @var int $quantity */
/** @var int|null $dictionaryId */
/** @var array $services */

$productLink = $product ? sprintf("href=\"%s\"", $product->site_link) : null;

/** @var \App\Core\Modules\Images\Models\Image|null $image */
$image = $product ? $product->preview : null;

$dictionary = Widget::show('products_dictionary::display-text', $dictionaryId ?? null);
@endphp
<div class="gcell gcell--12">
    <div class="order-product order-product--checkout"
         data-product="{{ r2d2()->attrJsonEncode($product->ecomm_data, JSON_UNESCAPED_UNICODE) }}"
    >
        <div class="grid _justify-center _sm-justify-between _sm-flex-nowrap _nm-xxs _sm-nm-sm">
            <div class="gcell gcell--auto order-product__cell _flex-noshrink _p-xxs _sm-p-sm">
                <a class="order-product__preview" {!! $productLink !!}>
                    {!! Widget::show('image', $image, 'small', ['class' => 'order-product__image']) !!}
                </a>
            </div>
            <div class="gcell gcell--auto _flex-grow _p-xxs _sm-p-sm _mr-auto">
                <a class="order-product__link" {!! $productLink !!}>
                    <div class="order-product__name">{{ $product ? ($product->name ?? '&mdash;') : trans('products::site.deleted') }}</div>
                </a>
                @foreach($services ?? [] as $service)
                    <div class="cart-item__optional">
                        @lang('products::site.add-service'):
                        <span class="text--dark-blue">
                            {{ $service['service'] }} {{ $service['option'] }} {{ format_only($service['price']) }}
                        </span>
                    </div>
                @endforeach
                {{-- @if($dictionary)
                    <div class="order-product__label _mt-xs">
                        {!! $dictionary !!}
                    </div>
                @endif --}}
            </div>
            <div class="gcell gcell--auto order-product__cell _flex-noshrink _ptb-xxs _sm-ptb-sm">
                <div class="order-product__qty">{{ $quantity }} &times; <strong>{{ $formattedPrice }}</strong></div>
            </div>
        </div>
    </div>
</div>
