<hr class="separator _color-gray2 _mtb-md">
@component('products::site.widgets.filter-accordion.accordion-block', [
    'ns' => 'filter',
    'id' => $block->alias,
    'header' => __($block->name),
    'open' => $block->isOpen(),
])
    <input type="checkbox" hidden name="" value="" class="fake-input js-filter-item">
    @foreach($block->getElements() as $element)
        @if($element->showInFilter())
            <div class="_mb-sm _color-black">
                @component('products::site.widgets.accordion.checker.checker', [
                      'attributes' => [
                          'type' => 'checkbox',
                          'checked' => $element->selected,
                          'value' => $element->alias,
                      ],
                      'disabled' => $element->disabled(),
                      'checked' => $element->selected,
                      'name' => $block->alias,
                  ])
                    {{ $element->name }} <span class="checker__counter">{{ $element->filteredCountToShow() }}</span>
                @endcomponent
            </div>
        @endif
    @endforeach
@endcomponent
