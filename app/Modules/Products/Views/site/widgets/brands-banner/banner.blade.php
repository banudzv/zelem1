@foreach($banners as $banner)
    <a {{ $banner->current->url ? 'href='. $banner->current->url  : null }}  class="brands-banner _mb-none _ms-mb-lg _def-mb-xxl">
            <img src="{{ $banner->image->link() }}" alt="">
        <div class="brands-banner__content _flex _flex-column _items-center _md-show _p-md _def-p-lg _lg-p-xxl">
            <div class="brands-banner__text _flex-grow _flex _pb-sm">
                <div class="_mtb-auto">{!! $banner->current->content !!}</div>
            </div>
        </div>
    </a>
@endforeach
