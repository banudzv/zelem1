@php
/** @var string $query */
/** @var string $perPage */
/** @var string $orderBy */
@endphp

<div class="action-bar__wide action-bar__wide-mobile _mr-sm _ms-mlr-sm">
    @include('products::site.widgets.action-bar-search.action-bar-search-mobile', [
        'query' => $query,
        'perPage' => $perPage,
        'orderBy' => $orderBy,
    ])
</div>
