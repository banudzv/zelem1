@php
/** @var \App\Modules\Products\Models\Product $product */
@endphp
<div class="item-card-controls">
    <div class="item-card-controls__control">
        {!! Widget::show('compare::button', $product->id) !!}
    </div>
    <div class="item-card-controls__control">
        {!! Widget::show('wishlist::product-button', $product->id) !!}
    </div>
</div>
