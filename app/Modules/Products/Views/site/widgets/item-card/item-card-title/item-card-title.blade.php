@php
    /** @var \App\Modules\Products\Models\Product $product */
    /** @var \App\Modules\Products\Models\ProductGroup $group */
    /** @var \App\Modules\Brands\Models\Brand|null $brand */
    $showGroupName = $showGroupName ?? false;
@endphp
<div class="item-card-title">
    <a href="{{ $product->site_link }}" class="item-card-title__text text text--dark-blue" data-trigger-event="productClick">
        {{ $showGroupName ? $group->name : $product->name }}
    </a>
    @if($product->is_available)
        <span class="_flex _items-center _pt-xs _pb-sm">
                {!! SiteHelpers\SvgSpritemap::get('icon-available', ['width' => 18, 'height' => 18]) !!}
                <span class="text text--dark-mint _ml-sm text--nowrap">
                    {{ trans('products::general.available') }}
                </span>
            </span>
    @elseif($product->is_custom_goods)
        <span class="text text--color-main _pt-xs _pb-sm">
            {{ trans('products::general.custom-goods') }}
        </span>
    @else
        <span class="_flex _items-center _pt-xs _pb-sm">
            {!! SiteHelpers\SvgSpritemap::get('icon-not-available', ['width' => 18, 'height' => 18, 'fill' => '#eb5757']) !!}
            <span class="text _ml-sm _color-red">
                {{ trans('products::general.not-available') }}
            </span>
        </span>
    @endif
    <div class="_mt-xs _mb-sm">
        @if($group->comments)
            <div class="grid _items-center">
                <div class="gcell">
                    @include('site._widgets.stars-block.stars-block', [
                        'mod' => 'normal',
                        'count' => $group->mark
                    ])
                </div>
                <div class="gcell _pl-xs">
                    <a href="{{ $product->site_link }}#review" class="text text--dark-blue text--semi-bold">
                        {{$group->comments->count()}}
                    </a>
                </div>
            </div>
        @endif
    </div>
</div>
