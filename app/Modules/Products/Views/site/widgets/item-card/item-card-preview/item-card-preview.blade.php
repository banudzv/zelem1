@php
    /** @var \App\Core\Modules\Images\Models\Image $image */
    /** @var string $link */
    /** @var string $meta */
    if (config('db.products.count-of-products-in-the-row') === \App\Modules\Products\Models\Product::THREE_IN_THE_ROW) {
        $size = 'small';
    } else {
        $size = 'xs';
    }
    $meta = isset($meta) ? $meta : null;
@endphp

<a href="{{ $link }}" class="item-card-preview lazyload-blur" data-trigger-event="productClick">
    {!! $image->imageTag($size, [
        'class' => 'item-card-preview__image lazyload-blur__image',
        'alt' => $meta,
        'title' => $meta
    ]) !!}
</a>
