@php
/** @var \App\Modules\Products\Models\ProductGroup[] $groups */
if (isset($full_width) && $full_width) {
    $classes = 'grid--2 grid--xs-3 grid--md-4 grid--xl-5';
} elseif (config('db.products.count-of-products-in-the-row') === \App\Modules\Products\Models\Product::THREE_IN_THE_ROW) {
    $classes = 'grid--2 grid--xs-3 grid--md-3 grid--def-3 grid--xl-3';
} else {
    $classes = 'grid--2 grid--xs-3 grid--md-4 grid--def-3 grid--lg-4';
}
@endphp
<div class="item-list">
    <div class="grid {{ $classes }}">
        @foreach($groups as $group)
            <div class="gcell _mb-sm">
                {!! Widget::show('products::group-card', $group) !!}
            </div>
        @endforeach
    </div>
</div>
