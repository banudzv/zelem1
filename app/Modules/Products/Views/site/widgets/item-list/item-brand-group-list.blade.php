@php
/** @var \App\Modules\Products\Models\ProductGroup[] $groups */
@endphp
<div class="item-list">
    <div class="grid grid--2 grid--xs-3 grid--md-4 grid--def-3 grid--lg-4">
        @foreach($groups as $group)
            <div class="gcell _mb-sm">
                {!! Widget::show('products::group-card', $group) !!}
            </div>
        @endforeach
    </div>
</div>
