@php
/** @var \App\Modules\Products\Models\Product[]|\Illuminate\Database\Eloquent\Collection $products */
/** @var string $text */
/** @var string $link */
$canCreateSlider = $products->isNotEmpty();
$_config = !$canCreateSlider ? [] : [
    'type' => $preset ?? 'SlickItem',
    'user-type-options' => [ ]
];
@endphp
<div class="section _mb-xl _lg-mb-xxl">
    <div class="container container--slider container--white">
        <div class="grid grid--auto _justify-center _items-center _mt-lg _def-mt-xxl _pb-def">
            <div class="gcell gcell--12 _mb-def _mr-def">
                <div class="title title--size-h2 title--product-title _color-dark-blue title--normal">{{ $text }}</div>
            </div>
            {{-- @if($link ?? false)
                <div class="gcell _mb-def _self-end">
                    @include('site._widgets.elements.goto.goto', [
                        'href' => $link,
                        'to' => 'next',
                        'text' => trans('products::site.see-all'),
                    ])
                </div>
            @endif --}}
        </div>
        <div {!! Html::attributes([
            'class' => [
                'item-slider',
                $mod_class ?? null,
                $canCreateSlider ? 'js-init' : null
            ]
        ]) !!} data-slick-slider='{!! json_encode($_config) !!}'>
            <div class="item-slider__list slick-slider-list" data-slick-slider>
                @foreach($products as $product)
                    {!! Widget::show('products::card', $product, false) !!}
                @endforeach
            </div>
            {{-- @if($canCreateSlider) --}}
                <div class="item-slider__arrows">
                    <div class="slick-slider-arrow slick-slider-arrow--prev" data-slick-arrow-prev>
                        {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-left-thin') !!}
                    </div>
                    <div class="slick-slider-arrow slick-slider-arrow--next" data-slick-arrow-next>
                        {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-right-thin') !!}
                    </div>
                </div>
            {{-- @endif --}}
            {{-- @if($canCreateSlider)
                <div {!! Html::attributes([
                    'class' => [
                        'item-slider__dots',
                        'slick-slider-dots',
                        $add_dot_classes ?? null
                    ]
                ]) !!} data-slick-dots></div>
            @endif --}}
        </div>
    </div>
</div>
