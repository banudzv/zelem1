@php
/** @var \App\Modules\Products\Models\ProductGroup[]|\Illuminate\Database\Eloquent\Collection $groups */
/** @var string $text */
/** @var string $link */
$canCreateSlider = $groups->isNotEmpty();
$_config = !$canCreateSlider ? [] : [
    'type' => $preset ?? 'SlickItem',
    'user-type-options' => [ ]
];
@endphp
<div class="section">
    <div class="container container--slider container--white">
        <div class="grid grid--auto _justify-center _items-center _pt-lg _def-pt-xxl _pb-def">
            <div class="gcell gcell--8 _mb-def _pl-xs">
                <div class="title title--size-h2 _color-dark-blue title--normal">{{ $text }}</div>
            </div>
            @if($link ?? false)
                <div class="gcell gcell--4 _mb-def _flex _justify-end">
                    @include('site._widgets.elements.goto.goto', [
                        'href' => $link,
                        'to' => 'next',
                        'text' => trans('products::site.see-all'),
                        'mod_class' => '_nmt-xs'
                    ])
                </div>
            @endif
        </div>
        <div {!! Html::attributes([
            'class' => [
                'item-slider',
                $mod_class ?? null,
                $canCreateSlider ? 'js-init' : null
            ]
        ]) !!} data-slick-slider='{!! json_encode($_config) !!}'>
            <div class="item-slider__list slick-slider-list" data-slick-slider>
                @foreach($groups as $group)
                    {!! Widget::show('products::group-card', $group, false) !!}
                @endforeach
            </div>
            <div class="item-slider__arrows">
                <div class="slick-slider-arrow slick-slider-arrow--prev" data-slick-arrow-prev>
                    {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-left-thin') !!}
                </div>
                <div class="slick-slider-arrow slick-slider-arrow--next" data-slick-arrow-next>
                    {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-right-thin') !!}
                </div>
            </div>
            @if($canCreateSlider)
                <div {!! Html::attributes([
                    'class' => [
                        'item-slider__dots',
                        'slick-slider-dots',
                        $add_dot_classes ?? null
                    ]
                ]) !!} data-slick-dots></div>
            @endif
        </div>
    </div>
</div>
