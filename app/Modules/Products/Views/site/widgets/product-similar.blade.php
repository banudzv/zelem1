@if(!empty($products) && $products->count())
    <div class="section _mb-lg">
        <div class="container container--slider container--white">
            <div class="grid grid--auto _justify-center _items-center _mt-lg _def-mt-xxl _pb-def">
                <div class="gcell gcell--12 _mb-def _mr-def">
                    <div
                        class="title title--size-h2 title--product-title _color-dark-blue title--normal">{{  __('products::site.similar') }}</div>
                </div>
                {{-- <div class="gcell _mb-def _self-end _md-show">
                    @include('products::site.widgets.product.arrows.items')
                </div> --}}
            </div>
            @include('products::site.widgets.item-list.item-slider', [
                'preset' => 'SlickItem',
                'add_dot_classes' => '_md-hide',
                'groups' => $products
            ])
        </div>
    </div>
@endif
