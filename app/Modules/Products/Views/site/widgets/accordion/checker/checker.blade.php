<label class="checker checkbox--simple {{ $class ?? null }}">
    <input class="checker__input js-filter-item" name="{{$name}}" {!! Html::attributes($attributes ?? []) !!}>
    <span class="checker__text" {{ isset($disabled) && $disabled === true ? 'disabled' : null }} >{{ $slot }}</span>
</label>


