@php
    /** @var string $categoryLink */
    /** @var \App\Modules\Products\Models\Product[] $products */
    /** @var \App\Modules\Categories\Models\Category $category */
    $productsIds = implode(',', $products->pluck('id')->toArray());
@endphp
<div class="compare-wrap">
    <div class="compare-wrap__title">
        @lang('products::site.comparing')
        <span>
            {{ $category ? $category->current->name : '' }}
            (<span data-compare-count>{{ $products->count() }}</span> @lang('products::site.items.count'))
        </span>
    </div>
    <div class="compare-table js-init is-loading" data-comparelist-table>
        <div class="grid compare-table__head">
            <div class="gcell gcell--4 gcell--def-1-of-5">
                <div data-row="1" class="compare-table__row--add _p-sm _ms-p-md">
                    <div class="compare-table__cell grid _justify-center">
                        <a href="{{ $categoryLink }}" class="compare-table__add">
                            <span>@lang('products::site.add-product')</span>
                        </a>
                        <a class="compare-table__clear"
                           data-products-ids="{{ $productsIds }}"
                           data-delete-url="{{ route('site.compare.remove') }}"
                        >
                            <span>@lang('products::site.clear-compare-list')</span>
                            {!! SiteHelpers\SvgSpritemap::get('icon-close') !!}
                        </a>
                    </div>
                </div>
            </div>
            <div class="gcell gcell--8 gcell--def-4-of-5">
                <div class="js-init _posr" data-slider-update data-slick-slider="{{ json_encode(['type' => 'CompareSliderHead']) }}">
                    <div class="compare-slider__arrows">
                        <div class="slick-slider-arrow slick-slider-arrow--prev" data-slick-arrow-prev>
                            {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-left-thin') !!}
                        </div>
                        <div class="slick-slider-arrow slick-slider-arrow--next" data-slick-arrow-next>
                            {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-right-thin') !!}
                        </div>
                    </div>
                    <div data-slick-slider id="compare-slider-head">
                        @foreach($products as $product)
                            <div data-row="1" class="compare-card compare-table__cell {{ $loop -> last ? 'compare-table__cell--last' : null }}">
                                <div class="compare-card__head" data-comparelist-card data-product-id="{{ $product->id }}">
                                    <div class="compare-card__preview">
                                        <a href="{{ $product->site_link }}" class="compare-card__image">
                                            {!! $product->preview->imageTag('small', ['width' => 60]) !!}
                                        </a>
                                        <a href="{{ $product->site_link }}" class="compare-card__title"
                                           title="{{$product->name}}">{{$product->name}}</a>
                                        <div class="compare-card__options">
                                            <div class="compare-card__options-delete" data-slick-update
                                                 data-comparelist-toggle="{{ $product->id }}">
                                                {!! SiteHelpers\SvgSpritemap::get('icon-close') !!}
                                            </div>
                                        </div>
                                    </div>
                                    @if($product->group && $product->group->relationExists('comments'))
                                        <div class="grid _self-stretch _items-center _mb-sm _mt-auto">
                                            <div class="gcell gcell--9 gcell--xs-auto _pl-xs _ms-pl-none">
                                                @include('site._widgets.stars-block.stars-block', [
                                                    'count' => $product->group->mark,
                                                    'mod' => 'compare-card'
                                                ])
                                            </div>
                                            <div class="gcell gcell--3 gcell--xs-auto _pl-xs">
                                                <a class="link link--underline text--semi-bold {{ $product->group->comments->count() == 0 ? 'link--cursor-def' : null }}" data-wstabs-ns="product"
                                                   data-wstabs-button="review">
                                                    {{-- @if ($product->group->comments->count())
                                                        {!! trans_choice('products::site.products-review', $product->group->comments->count()) !!}
                                                    @else
                                                        @lang('products::site.products-review-write')
                                                    @endif --}}
                                                    {{$product->group->comments->count()}}
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="grid _items-center _justify-between">
                                        <div class="gcell">
                                            <div class="compare-card__price">{{ $product->formatted_price }}</div>
                                        </div>
                                        @if($product->is_available)
                                            <div class="gcell _ml-auto _pl-def">
                                                <button type="button"
                                                        class="button button--compare"
                                                        data-cart-action="add"
                                                        data-product-id="{{ $product->id }}">
                                                        <span class="button__body">
                                                            {!! SiteHelpers\SvgSpritemap::get('icon-shopping', [
                                                                'class' => 'button__icon button__icon--before'
                                                            ]) !!}
                                                        </span>
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="grid">
            <div class="gcell gcell--4 gcell--def-1-of-5">
                <div data-row="2" class="compare-table__row compare-table__row--char">
                    <div class="compare-table__cell compare-table__cell--first compare-table__cell--char">@lang('products::site.brand')</div>
                </div>
                @php($i=3)
                @foreach($features as $key => $value)
                    <div data-row="{{ $i }}" class="compare-table__row compare-table__row--char">
                        <div class="compare-table__cell compare-table__cell--first compare-table__cell--char {{ $loop -> last ? 'compare-table__cell--last-row' : null }}">{{ $value }}</div>
                    </div>
                    @php($i++)
                @endforeach
            </div>
            <div class="gcell gcell--8 gcell--def-4-of-5" data-row-count="{{ $i - 1 }}">
                <div class="js-init" data-slider-update data-slick-slider="{{ json_encode(['type' => 'CompareSliderBody']) }}">
                    <div data-slick-slider id="compare-slider-body">
                        @foreach($products as $product)
                            <div class="compare-table__cell compare-card {{ $loop -> last ? 'compare-table__cell--last' : null }}" data-product-id="{{ $product->id }}" data-comparelist-card>
                                <div data-row="2" class="compare-table__cell compare-table__cell--char">
                                    <div class="compare-card__body">
                                        {!! $product->brand ? $product->brand->current->name : '&mdash;' !!}
                                    </div>
                                </div>
                                @php($n = 3)
                                    @foreach($features as $key => $value)
                                        <div data-row="{{ $n }}" class="compare-table__cell compare-table__cell--char {{ $loop -> last ? 'compare-table__cell--last-row' : null }}">
                                            <div class="compare-card" data-comparelist-card
                                                 data-product-id="{{ $product->id }}">
                                                @if(isset($values->get($product->id)[$value]))
                                                    <div class="compare-card__body">
                                                        @if(!is_array($values->get($product->id)[$value]))
                                                            {{ $values->get($product->id)[$value] }}
                                                        @else
                                                            @foreach($values->get($product->id)[$value] as $item)
                                                                {{ $item }}{{ $loop->last ? '' : ', ' }}
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        @php($n++)
                                    @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

