@component('products::site.widgets.accordion.accordion', [
    'options' => [
        'type' => 'multiple',
    ],
])
    @forelse(ProductsFilter::getBlocks() as $index => $block)

        @if($block->showInFilter())
            @include('products::site.widgets.filter-block.block', ['block' => $block])
        @endif

        @if($index === 'brand' || $index === 'category')
            {!! $priceRange !!}
        @endif

        @isset($query)
            <input type="hidden" name="query" value="{{ $query }}">
        @endif
    @empty
        {!! $priceRange !!}
    @endforelse
@endcomponent


<hr class="separator _color-gray2 _mtb-md">

{!! $carFilter !!}