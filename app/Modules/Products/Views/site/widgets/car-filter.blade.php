@php
    /** @var $vendors array */
    /** @var $chosenVendorId int|null */
    /** @var $models array */
    /** @var $chosenModelId int|null */
    /** @var $volumes array */
    /** @var $chosenVolumeId int|null */
    /** @var $powers array */
    /** @var $chosenPowerId int|null */
    /** @var $engineNumbers array */
    /** @var $chosenEngineNumber string */
    $params = ProductsFilter::getParameters();
    $parameters = Route::current()->parameters();
    unset($parameters['pageQuery']);
    $link = route(Route::currentRouteName(), $parameters);
@endphp

<div class="_mb-sm _color-black select-wrap select-wrap-mark visible">
    <div class="select-wrap__name">@lang('engine::site.filter.vendor')</div>
    <select name="car[vendor]" data-id="vendorId" data-find="mark" class="select js-filter-item js-select-filter js-engine-filter" data-url="{{ route('api.ajax.models') }}">
        <option value="">@lang('engine::site.filter.all')</option>
        @foreach($vendors as $id => $name)
            <option value="{{ $id }}" {{ $chosenVendorId === $id ? 'selected' : '' }}>{{ $name }}</option>
        @endforeach
    </select>
    <hr class="separator _color-gray2 _mtb-md">
</div>

<div class="_mb-sm _color-black select-wrap {{ (($chosenModelId && count($models)) || $chosenVendorId) ? 'visible' : '' }}">
    <div class="select-wrap__name">@lang('engine::site.filter.model')</div>
    <select name="car[model]" data-find="models" data-id="modelId" class="select js-filter-item js-select-filter js-engine-filter" data-url="{{ route('api.ajax.volumes') }}">
        <option value="">@lang('engine::site.filter.all')</option>
        @foreach($models as $id => $name)
            <option value="{{ $id }}" {{ $chosenModelId === $id ? 'selected' : '' }}>{{ $name }}</option>
        @endforeach
    </select>
    <hr class="separator _color-gray2 _mtb-md">
</div>

<div class="_mb-sm _color-black select-wrap {{ (($chosenVolumeId && count($volumes)) || ($chosenModelId && count($models))) ? 'visible' : '' }}">
    <div class="select-wrap__name">@lang('engine::site.filter.volume')</div>
    <select name="car[volume]" data-find="volumes" data-id="volumeId" class="select js-filter-item js-select-filter js-engine-filter" data-url="{{ route('api.ajax.powers') }}">
        <option value="">@lang('engine::site.filter.all')</option>
        @foreach($volumes as $id => $name)
            <option value="{{ $id }}" {{ $chosenVolumeId === $id ? 'selected' : '' }}>{{ $name }}</option>
        @endforeach
    </select>
    <hr class="separator _color-gray2 _mtb-md">
</div>

<div class="_mb-sm _color-black select-wrap {{ (($chosenPowerId && count($powers)) || ($chosenVolumeId && count($volumes))) ? 'visible' : '' }}">
    <div class="select-wrap__name">@lang('engine::site.filter.power')</div>
    <select name="car[power]" data-find="powers" data-id="powerId" class="select js-filter-item js-select-filter js-engine-filter" data-url="{{ route('api.ajax.engine-numbers') }}">
        <option value="">@lang('engine::site.filter.all')</option>
        @foreach($powers as $id => $name)
            <option value="{{ $id }}" {{ $chosenPowerId === $id ? 'selected' : '' }}>{{ $name }}</option>
        @endforeach
    </select>
    <hr class="separator _color-gray2 _mtb-md">
</div>

<div class="_mb-sm _color-black select-wrap {{ (($chosenEngineNumber && count($engineNumbers)) || ($chosenPowerId && count($powers))) ? 'visible' : '' }}">
    <div class="select-wrap__name">@lang('engine::site.filter.engine-number')</div>
    <select name="car[number]" data-find="engineNumbers" class="select js-filter-item js-engine-filter">
        <option value="">@lang('engine::site.filter.all')</option>
        @foreach($engineNumbers as $engineNumber)
            <option value="{{ $engineNumber }}" {{ $chosenEngineNumber === $engineNumber ? 'selected' : '' }}>{{ $engineNumber }}</option>
        @endforeach
    </select>
    <hr class="separator _color-gray2 _mtb-md">
</div>
