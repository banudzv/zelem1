@php
    /** @var array $object */
@endphp

<script>
    if(typeof dataLayer === "object") {
        dataLayer.push({!! $object !!});
    }
</script>