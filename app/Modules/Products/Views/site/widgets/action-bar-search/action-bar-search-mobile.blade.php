@php
/** @var string $query */
/** @var string $perPage */
/** @var string $orderBy */
@endphp

<div class="action-bar-search-wrap action-bar-search-wrap--mobile js-init" data-livesearch data-short-query="Введите минимум 3 символа">
    {!! Form::open(['route' => 'site.search-products', 'method' => 'get', 'class' => 'action-bar-search action-bar-search--mobile', 'data-search-form' => true]) !!}
        <input{!! HTML::attributes([
            'type' => 'hidden',
            'name' => 'order',
            'disabled' => !$orderBy,
            'value' => $orderBy,
        ]) !!}>
        <input{!! HTML::attributes([
            'type' => 'hidden',
            'name' => 'per-page',
            'disabled' => !$perPage,
            'value' => $perPage,
        ]) !!}>
        <div class="action-bar-search__group action-bar-search__group--mobile">
            <input type="search" required class="action-bar-search__input action-bar-search__input--mobile"
                    name="query"
                    data-search-input
                    autocomplete="off"
                    placeholder="@lang('products::global.search')"
                    value="{{ $query }}">
            <button type="submit" class="action-bar-search__submit action-bar-search__submit--mobile" title="Поиск товаров">
                {!! SiteHelpers\SvgSpritemap::get('icon-search') !!}
            </button>
        </div>
        <div class="action-bar-search__suggestions action-bar-search__suggestions--mobile" data-search-suggestions></div>
    {!! Form::close() !!}
</div>
