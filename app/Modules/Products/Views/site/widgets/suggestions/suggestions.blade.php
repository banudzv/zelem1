@php
    use Illuminate\Pagination\LengthAwarePaginator;

    /** @var int $limit */
    /** @var int $total */
    /** @var string $query */
    /** @var App\Modules\Products\Models\Product[]|LengthAwarePaginator $products */
    /** @var App\Modules\Categories\Models\Category[] $categories */

    $total = $products->total();
@endphp

<div class="suggestions">
    <div class="suggestions__body">
        @if($total > 0)
            <div class="suggestions__section">
                <div class="suggestions__list">
                    @foreach($products as $product)
                        <a class="suggestions__item" href="{{ $product->site_link }}">
                            <div class="grid _flex-nowrap">
                                <div class="gcell gcell--auto _flex-noshrink _pr-def">
                                    <div class="suggestions__item-aside">
                                        <div class="suggestions__item-image">
                                            {!! Widget::show('image', $product->preview, 'small') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="gcell gcell--auto _flex-grow">
                                    <div class="suggestions__item-main">
                                        <div class="suggestions__item-title">{{ $product->name }}</div>
                                        <div class="suggestions__item-entry">{{ $product->teaser }}</div>
                                        <div class="suggestions__item-price">{{ $product->formatted_price }}</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach

                    @foreach($categories as $category)
                        <a class="suggestions__item" href="{{ $category->link(['query' => $query]) }}">
                            <div class="grid _flex-nowrap _items-center">
                                <div class="gcell gcell--auto _flex-noshrink _pr-def">
                                    <div class="suggestions__item-aside">
                                        <div class="suggestions__item-image suggestions__item-image--category">
                                            {!! \SiteHelpers\SvgSpritemap::get('icon-search') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="gcell gcell--auto _flex-grow">
                                    <div class="suggestions__item-main">
                                        <div class="suggestions__item-title _m-none">{{ $category->getName() }}</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        @else
            <div class="suggestions__section">
                <div class="suggestions__title _text-center">
                    @lang('products::site.search.no-found')
                </div>
            </div>
        @endif
    </div>
    @if($total > $limit)
        <div class="suggestions__footer">
            <a href="{{ route('site.search-products') . "?query=$query" }}" class="suggestions__all-results-link">
                @lang('products::site.search.all-results') ({{ $total }})
            </a>
        </div>
    @endif
</div>
