@php
/** @var \App\Modules\Products\Models\Product $product */
/** @var \Illuminate\Database\Eloquent\Collection|\App\Modules\Products\Models\Product[] $related */

$hideH1 = true;
@endphp

@extends('site._layouts.main',[
    'wide' => true
])

@if($product->microdata)
    @push('microdata')
        <script type="application/ld+json">/*<![CDATA[*/{!! $product->microdata !!}/*]]>*/</script>
        <script>
            fbq('track', 'ViewContent', {!! $product->FBPixelData !!});
        </script>
    @endpush
@endif

@section('layout-body')
    <div class="section">
        <div class="container container--small container--white">
            <div class="grid grid--1 _items-center">
                <div class="gcell _flex-grow _pr-sm">
                    <a href="@foreach (Seo::breadcrumbs()->elements() as $breadcrumb)@if($loop -> remaining == 1){{ $breadcrumb -> getUrl() }}@endif @endforeach"
                       class="title title--link title--back _def-hide title--size-h1 title--size-h1-border"> {!! \SiteHelpers\SvgSpritemap::get('icon-arrow-left-thin', ['class' => 'title__icon']) !!} <span>@foreach (Seo::breadcrumbs()->elements() as $breadcrumb)@if($loop -> remaining == 1){{ $breadcrumb -> getTitle() }}@endif @endforeach</span></a>
                    <h1 class="title title--size-h1 title--product-title title--normal _color-dark-blue">{!! Seo::site()->getH1() !!}</h1>
                </div>
                <div class="gcell _flex-grow _pt-sm _flex _justify-between _items-center _md-hide">
                    @if($product->group && $product->group->relationExists('comments'))
                        <div class="grid _items-center _ml-xs _md-ml-xs">
                            <div class="gcell">
                                @include('site._widgets.stars-block.stars-block', [
                                    'count' => $product->group->mark,
                                ])
                            </div>
                            <div class="gcell _pl-sm">
                                <a class="link link--underline text--semi-bold" data-wstabs-ns="product" data-wstabs-button="review">
                                    {{-- @if ($product->group->comments->count())
                                        {!! trans_choice('products::site.products-review', $product->group->comments->count()) !!}
                                    @else
                                        @lang('products::site.products-review-write')
                                    @endif --}}
                                    {{$product->group->comments->count()}}
                                </a>
                            </div>
                        </div>
                    @endif
                    @if($product->vendor_code)
                    @include('site._widgets.elements.vendor-code.vendor-code', [
                        'code' => $product->vendor_code,
                    ])
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="section js-init" data-product="{{ r2d2()->attrJsonEncode($product->ecomm_data, JSON_UNESCAPED_UNICODE) }}">
        <div class="container container--small container--white">
            @include('products::site.widgets.product.product', [
                'tabs' => $tabs,
            ])
        </div>
    </div>
    {!! Widget::show('products::related-products', $product) !!}
    {!! Widget::show('products::similar-products', $product) !!}
    {!! Widget::show('viewed::products', $product->id) !!}
    <div class="section _mtb-def">
        <div class="container container--white _pb-xl">
            <div class="_pt-def _pb-sm _def-pb-md _ms-text-center">
                <div class="title title--size-h2 title--product-title _color-dark-blue title--normal">@lang('brands::site.our-brands')</div>
            </div>
            {!! Widget::show('brands::our-brands') !!}
        </div>
    </div>
@endsection
