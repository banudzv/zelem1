@php
/** @var int $categoryId */
/** @var App\Modules\Products\Models\ProductGroup[]|Illuminate\Pagination\LengthAwarePaginator $groups */
/** @var App\Modules\Products\Widgets\Site\ProductFilter $filter */
@endphp

@extends('site._layouts.catalog')

@section('layout-body')
    <div class="container container--white container--small">
        <div class="_ptb-sm _def-pt-none _def-pb-def">
            {!! Widget::show('categories::kids-slider', $categoryId) !!}
        </div>
    </div>

    <div class="section _mb-lg">
        <div class="container container--small container--white">
            <div class="grid grid--1" id="filter">
                <div class="gcell gcell--def-3 gcell--lg-1-of-5">
                    <div class="box _pt-none">
                        <div class="_def-show">
                            {!! Widget::show('categories::in-filter') !!}
                            {!! Widget::show('categories::kids', $categoryId) !!}
                        </div>
                        <div class="mobile-filter">
                            <div class="mobile-filter__overlay js-filter-close"></div>
                            <div class="mobile-filter__body">
                                {!! Form::open(['class' => ['js-filter-form'], 'id' => '50']) !!}
                                {!! $filter !!}
                                <a class="show-filter show-filter--mobile">
                                    показать
                                </a>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <a class="show-filter">
                            <span class="show-filter__text">
                                показать
                            </span>
                        </a>
                    </div>
                </div>
                <div class="gcell gcell--def-9 gcell--lg-4-of-5">
                    <div class="grid _justify-between _pb-def _nmb-md">
                        <div class="gcell gcell--6 gcell--sm-auto _pb-md _pr-sm _sm-pr-none">
                            <div class="_def-hide">
                                <div class="grid _justify-center">
                                    <div class="gcell gcell--12 gcell--sm-auto">
                                        <button type="button"
                                                class="button button--theme-main button--size-normal button--width-full js-filter-show">
                                            <span class="button__body">
                                                {!! SiteHelpers\SvgSpritemap::get('icon-filter', [
                                                    'class' => 'button__icon button__icon--before'
                                                ]) !!}
                                                <span class="button__text">@lang('products::site.filter-btn')</span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gcell gcell--6 gcell--sm-auto _pb-md gcell--auto gcell--def-12">
                            {!! Widget::show('products::sort-bar', true) !!}
                        </div>
                        {!! Widget::show('products::chosen-parameters-in-filter') !!}
                    </div>
                    <div class="_def-ml-xxs">
                        {!! Widget::show('products::groups-list', $groups) !!}

                        @if($groups instanceof \Illuminate\Pagination\LengthAwarePaginator)
                            <hr class="separator _color-gray3 _mtb-xl">
                            {!! $groups->links('pagination.site') !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
