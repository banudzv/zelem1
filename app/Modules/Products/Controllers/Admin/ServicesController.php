<?php

namespace App\Modules\Products\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Products\Forms\ServiceForm;
use App\Modules\Products\Models\Service;
use App\Modules\Products\Models\ServicePrice;
use App\Modules\Products\Requests\ServiceJSRequest;
use App\Modules\Products\Requests\ServiceRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class ServicesController
 * @package App\Modules\Products\Controllers\Admin
 */
class ServicesController extends AdminController
{
    /**
     * @var int Count of services on the page.
     */
    protected $limit = 15;

    /**
     * ServicesController constructor.
     */
    public function __construct()
    {
        \Seo::breadcrumbs()->add(
            'products::seo.services.index',
            RouteObjectValue::make('admin.services.index')
        );
    }

    /**
     * Services page.
     *
     * @return Application|Factory|View
     * @throws \Exception
     */
    public function index()
    {
        $this->addCreateButton('admin.services.create');
        \Seo::meta()->setH1('products::seo.services.index');
        $services = Service::query()->latest('id')->paginate($this->limit);

        return view('products::admin.services.index', compact('services'));
    }

    /**
     * Creating service page with form.
     *
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function create()
    {
        $this->jsValidation(new ServiceJSRequest());
        \Seo::breadcrumbs()->add('products::seo.services.create');
        \Seo::meta()->setH1('products::seo.services.create');
        $form = ServiceForm::make();

        return view('products::admin.services.create', compact('form'));
    }

    /**
     * Creates service and syncs it with the options.
     *
     * @param ServiceRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     * @throws \Exception
     */
    public function store(ServiceRequest $request)
    {
        $service = (new Service);
        if ($error = $service->createRow($request)) {
            return $this->afterFail($error);
        }
        $this->syncWithOptions($request, $service->id);

        return $this->afterStore(['id' => $service->id]);
    }

    /**
     * Edit service page.
     *
     * @param Service $service
     * @return Application|Factory|View
     * @throws WrongParametersException
     */
    public function edit(Service $service)
    {
        $this->jsValidation(new ServiceJSRequest());
        \Seo::breadcrumbs()->add($service->current->name ?? 'products::seo.services.edit');
        \Seo::meta()->setH1('products::seo.services.edit');
        $form = ServiceForm::make($service);

        return view('products::admin.services.update', compact('form', 'service'));
    }

    /**
     * Updates service in the database.
     *
     * @param Request $request
     * @param Service $service
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException
     * @throws \Exception
     */
    public function update(Request $request, Service $service)
    {
        if ($error = $service->updateRow($request)) {
            return $this->afterFail($error);
        }
        $this->syncWithOptions($request, $service->id);

        return $this->afterUpdate(['id' => $service->id]);
    }

    /**
     * Deletes service and its options.
     *
     * @param Service $service
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(Service $service)
    {
        return $service->deleteRow() ? $this->afterDestroy() : $this->afterFail();
    }

    /**
     * Syncs service and options.
     *
     * @param Request $request
     * @param int $serviceId
     * @throws \Exception
     */
    protected function syncWithOptions(Request $request, int $serviceId): void
    {
        $ids = array_filter($request->input('ServiceOptionId', []), function ($number) {
            return is_null($number) === false;
        });
        ServicePrice::whereNotIn('id', $ids)->whereServiceId($serviceId)->delete();
        foreach ($request->input('ServiceOptionId', []) as $index => $id) {
            $data = [
                'service_id' => $serviceId,
                'price' => $request->input("ServiceOptionPrice.$index"),
                'position' => $index,
            ];
            foreach (config('languages', []) as $lang => $_) {
                $data[$lang]['name'] = $request->input("ServiceOptionName.$lang.$index");
            }
            if (isset($id)) {
                ServicePrice::find($id)->updateRow($data);
            } else {
                (new ServicePrice)->createRow($data);
            }
        }
    }
}
