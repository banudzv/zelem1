<?php

namespace App\Modules\Products\Controllers\Admin;

use App\Core\AdminController;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Features\Models\Feature;
use App\Modules\Features\Models\FeatureValue;
use App\Modules\Products\Filters\ProductGroupAdminFilter;
use App\Modules\Products\Forms\GroupForm;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupFeatureValue;
use App\Modules\Products\Requests\FeatureUpdateForFrontendRequest;
use App\Modules\Products\Requests\FeatureUpdateRequest;
use App\Modules\Products\Requests\GroupFrontendRequest;
use App\Modules\Products\Requests\GroupRequest;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Seo;

/**
 * Class ProductsController
 *
 * @package App\Modules\Products\Controllers\Admin
 */
class GroupsController extends AdminController
{
    /**
     * @param Request $request
     * @return Application|Factory|View
     * @throws WrongParametersException
     * @throws Exception
     */
    public function index(Request $request)
    {
        $this->addBaseBreadcrumbs();
        $this->addCreateButton('admin.groups.create');
        $this->addActiveButton('admin.groups.active-multi');
        $this->addDeleteButton('admin.groups.delete');

        Seo::meta()->setCountOfEntity(trans('products::seo.groups.count', ['count' => ProductGroup::count(['id'])]));
        Seo::meta()->setH1('products::seo.groups.index');

        $groups = ProductGroup::query()
            ->with(
                [
                    'current',
                    'products.current',
                    'products.group.brand',
                    'products.group.category',
                    'category.current',
                    'brand.current',
                    'products.images',
                ]
            )
            ->filter($request->all())
            ->latest('id')
            ->paginate(config('db.products.per-page', ProductGroup::LIMIT_PER_PAGE_BY_DEFAULT_ADMIN_PANEL));

        return view(
            'products::admin.groups.index',
            [
                'groups' => $groups,
                'filter' => ProductGroupAdminFilter::showFilter(),
            ]
        );
    }

    private function addBaseBreadcrumbs()
    {
        Seo::breadcrumbs()->add(
            'products::seo.groups.index',
            RouteObjectValue::make('admin.groups.index')
        );
    }

    /**
     * Create product page
     *
     * @return Factory|View
     * @throws WrongParametersException
     */
    public function create()
    {
        $this->addBaseBreadcrumbs();
        Seo::breadcrumbs()->add('products::seo.groups.create');
        Seo::meta()->setH1('products::seo.groups.create');
        $this->jsValidation(new GroupFrontendRequest);
        return view(
            'products::admin.groups.create',
            [
                'form' => GroupForm::make(),
            ]
        );
    }

    /**
     * Store new product
     *
     * @param GroupRequest $request
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException|Exception
     */
    public function store(GroupRequest $request)
    {
        $group = ProductGroup::store($request);
        return $this->afterStore([$group->id]);
    }

    /**
     * Update category page
     *
     * @param ProductGroup $group
     * @return Factory|View
     * @throws WrongParametersException
     */
    public function edit(ProductGroup $group)
    {
        $this->addBaseBreadcrumbs();
        Seo::breadcrumbs()->add($group->current->name ?? 'products::seo.groups.edit');
        Seo::meta()->setH1('products::seo.groups.edit');
        $this->jsValidation(new GroupFrontendRequest);
        return view(
            'products::admin.groups.update',
            [
                'form' => GroupForm::make($group),
                'group' => $group,
            ]
        );
    }

    /**
     * Update information for current category
     *
     * @param GroupRequest $request
     * @param ProductGroup $group
     * @return RedirectResponse|Redirector
     * @throws WrongParametersException|Exception
     */
    public function update(GroupRequest $request, ProductGroup $group)
    {
        $group->edit($request);
        return $this->afterUpdate();
    }

    /**
     * Delete all data for category including images
     *
     * @param ProductGroup $group
     * @return RedirectResponse
     * @throws WrongParametersException
     */
    public function destroy(ProductGroup $group)
    {
        $group->products->each(
            function (Product $product) {
                $product->deleteRow();
            }
        );
        $group->deleteRow();
        return $this->afterDestroy();
    }

    /**
     * @return JsonResponse
     */
    public function delete()
    {
        $modelName = request()->input('className');
        $ids = request()->input('ids');
        abort_unless($modelName, 404);
        ProductGroup::whereIn('id', $ids)->get()->each(
            function (ProductGroup $group) {
                $group->products->each(
                    function (Product $product) {
                        $product->deleteRow();
                    }
                );
                $group->deleteRow();
            }
        );
        return response()->json();
    }

    /**
     * Action to change active status of a model
     *
     * @param  $id
     * @return JsonResponse
     */
    public function active($id)
    {
        $group = ProductGroup::findOrFail($id);
        $group->toggleActiveStatus();
        return response()->json(['success' => true]);
    }

    /**
     * @param ProductGroup $group
     * @return Factory|View
     */
    public function changeFeature(ProductGroup $group)
    {
        $this->addBaseBreadcrumbs();
        Seo::breadcrumbs()->add('products::seo.groups.change-feature');
        Seo::meta()->setH1('products::seo.groups.change-feature');

        $features = [];
        $featuresObjects = Feature::with('current')->where('id', '!=', $group->feature_id)->get();
        $featuresObjects->each(
            function (Feature $feature) use (&$features) {
                $features[$feature->id] = $feature->current->name;
            }
        );

        $values = [];
        if ($featuresObjects->isNotEmpty() && $featuresObjects->first()->values) {
            $featuresObjects->first()->values->each(
                function (FeatureValue $value) use (&$values) {
                    $values[$value->id] = $value->current->name;
                }
            );
        }

        $this->jsValidation(new FeatureUpdateForFrontendRequest);

        return view(
            'products::admin.feature.update',
            [
                'group' => $group,
                'features' => $features,
                'values' => $values,
            ]
        );
    }

    /**
     * @param FeatureUpdateRequest $request
     * @param ProductGroup $group
     * @return RedirectResponse
     * @throws WrongParametersException
     * @throws Exception
     */
    public function changeFeatureConfirmation(FeatureUpdateRequest $request, ProductGroup $group)
    {
        $featureId = $request->input('feature_id');
        $group->update(['feature_id' => $featureId]);
        ProductGroupFeatureValue::whereGroupId($group->id)->where('feature_id', $featureId)->delete();
        $group->products->each(
            function (Product $product) use ($request, $featureId, $group) {
                $valueId = $request->input('value.' . $product->id);
                ProductGroupFeatureValue::create(
                    [
                        'product_id' => $product->id,
                        'group_id' => $group->id,
                        'feature_id' => $featureId,
                        'value_id' => $valueId,
                    ]
                );
                $product->update(
                    [
                        'value_id' => $valueId,
                    ]
                );
            }
        );
        return $this->customRedirect(
            'admin.groups.edit',
            [$group->id],
            trans('products::admin.feature-changed-successfully')
        );
    }
}
