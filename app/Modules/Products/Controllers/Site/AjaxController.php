<?php

namespace App\Modules\Products\Controllers\Site;

use App\Core\AjaxTrait;
use App\Core\SiteController;
use App\Modules\Products\Requests\ProductSearchRequest;
use App\Modules\Products\Services\ProductSearchServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Lang;
use Throwable;

class AjaxController extends SiteController
{
    use AjaxTrait;

    /**
     * @var ProductSearchServiceInterface
     */
    protected $productSearchService;

    public function __construct(ProductSearchServiceInterface $productSearchService)
    {
        $this->productSearchService = $productSearchService;
    }

    /**
     * Live search
     *
     * @param Request $request
     * @return string
     * @throws Throwable
     */
    public function search(Request $request)
    {
        $limit = config('db.search.product-limit', 8);

        $searchRequest = ProductSearchRequest::byRequest($request)
            ->setPerPage($limit)
            ->setHideModification(config('db.products.hide-products-mods-in-search', false));

        $query = $searchRequest->getSearchString();

        if (!$query && Str::length($query) >= $limit) {
            return $this->errorJsonAnswer();
        }

        $searchResponse = $this->productSearchService->searchByText($searchRequest);
        $products = $searchResponse->getProducts();
        $products->loadMissing(['current', 'images']);

        $categories = $searchResponse->getAggregateCategories();
        $categories->loadMissing(['current']);

        return $this->successJsonAnswer(
            [
                'html' => view(
                    'products::site.widgets.suggestions.suggestions',
                    [
                        'products' => $products,
                        'categories' => $categories,
                        'limit' => $limit,
                        'query' => $query,
                    ]
                )->render(),
            ]
        );
    }

    public function infoPopup(string $info)
    {
        $type = $info == 'sizes' ? 'products' : 'basic';
        if ($info == 'products') {
            return view(
                'site._widgets.popup.wysiwyg',
                [
                    'title' => config("db.products.sizes_title_" . Lang::getLocale()),
                    'text' => config("db.products.sizes_text_" . Lang::getLocale()),
                ]
            );
        }
        return view(
            'site._widgets.popup.wysiwyg',
            [
                'title' => config("db.{$type}.{$info}_title"),
                'text' => config("db.{$type}.{$info}_text"),
            ]
        );
    }

}
