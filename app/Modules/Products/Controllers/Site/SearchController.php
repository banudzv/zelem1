<?php

namespace App\Modules\Products\Controllers\Site;

use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\SiteController;
use App\Modules\Products\Requests\ProductSearchRequest;
use App\Modules\Products\Services\ProductSearchServiceInterface;
use Catalog;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchController extends SiteController
{
    const SITE_SEARCH_PRODUCTS = 'site.search-products';

    /**
     * @var ProductSearchServiceInterface
     */
    protected $productSearchService;

    public function __construct(ProductSearchServiceInterface $productSearchService)
    {
        $this->productSearchService = $productSearchService;
    }

    public function index(Request $request)
    {
        /** @var SystemPage $page */
        $page = SystemPage::getByCurrent('slug', 'search');

        $this->setMeta($page);

        $searchRequest = ProductSearchRequest::byRequest($request)
            ->setHideModification(config('db.products.hide-products-mods-in-search', false));

        $products = new Collection();

        if ($query = $searchRequest->getSearchString()) {
            $searchResponse = $this->productSearchService->searchByText($searchRequest);
            $products = $searchResponse->getProducts();
            $products->load(
                [
                    'current',
                    'images',
                    'group.comments',
                    'labels',
                    'brand.current',
                    'category.current'
                ]
            );
        }
        Catalog::ecommerce()->setPage('searchresults', 'productImpressions');
        Catalog::ecommerce()->addProducts($products->getCollection(), 'Search Results');

        return view(
            'products::site.search',
            [
                'products' => $products,
                'query' => $query,
                'page' => $page,
                'total' => $products instanceof LengthAwarePaginator ? $products->total() : $products->count(),
            ]
        );
    }

    protected function setMeta(SystemPage $page): void
    {
        $this->setOtherLanguagesLinks($page);
        $this->meta($page->current, $page->current->content);
        $this->breadcrumb($page->current->name, self::SITE_SEARCH_PRODUCTS);
        $this->canonical(route(self::SITE_SEARCH_PRODUCTS));
    }

}
