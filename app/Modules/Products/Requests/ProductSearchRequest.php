<?php

namespace App\Modules\Products\Requests;

use Illuminate\Http\Request;

class ProductSearchRequest
{
    const PER_PAGE = 10;

    /**
     * @var Request
     */
    protected $request;

    protected $page = 1;

    protected $perPage;

    protected $order;

    protected $query;

    protected $ignored = [];

    protected $hideModifications = false;

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->init();
    }

    private function init()
    {
        $request = $this->getRequest();

        $this->query = $request->input('query');
        $this->page = resolve('paginateroute')->currentPage() ?? 1;
        $this->perPage = $request->input('per-page', config('db.products.site-per-page', self::PER_PAGE));
        $this->order = $request->input('order');
    }

    public function getForFilter(): array
    {
        return $this->getRequest()->only(['query', 'order']);
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public static function byRequest(Request $request)
    {
        return new self($request);
    }

    public function getSearchString(): ?string
    {
        return $this->query;
    }

    public function getOrder(): ?string
    {
        return $this->order;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    public function setPerPage(int $perPage)
    {
        $this->perPage = $perPage;

        return $this;
    }

    public function getIgnored(): array
    {
        return $this->ignored;
    }

    public function setIgnored(array $ignored = [])
    {
        $this->ignored = $ignored;

        return $this;
    }

    public function isHideModifications(): bool
    {
        return $this->hideModifications;
    }

    public function setHideModification(bool $hideModification): self
    {
        $this->hideModifications = $hideModification;

        return $this;
    }
}