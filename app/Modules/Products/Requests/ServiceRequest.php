<?php

namespace App\Modules\Products\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ServiceRequest
 * @package App\Modules\Products\Requests
 */
class ServiceRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;
    
    /**
     * {@inheritDoc}
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function rules(): array
    {
        return $this->generateRules([
            'active' => ['required', 'boolean'],
            'ServiceOptionPrice' => ['array'],
            'ServiceOptionPrice.*' => ['required', 'integer', 'min:0'],
            'ServiceOptionName.*' => ['array'],
            'ServiceOptionName.*.*' => ['required', 'string', 'max:191'],
        ], [
            'name' => ['required', 'max:191'],
            'slug' => ['required', 'max:191'],
            'teaser' => ['nullable'],
            'h1' => ['nullable', 'max:191'],
            'title' => ['nullable', 'max:191'],
            'description' => ['nullable'],
            'keywords' => ['nullable'],
            'text' => ['nullable'],
            'url' => ['nullable', 'max:191']
        ]);
    }
}
