<?php

namespace App\Modules\Products\Requests;

class FilteringRequest
{

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @var int
     */
    private $key;

    /**
     * @var int
     */
    private $perPage;

    /**
     * @var int
     */
    private $page;

    public static function create(): self
    {
        return new self();
    }

    public function getPerPage(): ?int
    {
        return $this->perPage;
    }

    public function setPerPage(int $perPage): self
    {
        $this->perPage = $perPage;

        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return int|string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param int|string $key
     * @return $this
     */
    public function setKey($key): self
    {
        $this->key = $key;

        return $this;
    }

}