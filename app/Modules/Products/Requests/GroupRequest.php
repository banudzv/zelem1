<?php

namespace App\Modules\Products\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\Brands\Rules\IsBrand;
use App\Modules\Categories\Rules\CategoryExists;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

class GroupRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return $this->generateRules(
            [
                'category_id' => ['required', new CategoryExists],
                'brand_id' => ['nullable', new IsBrand],

                'position' => ['required', 'integer'],
            ],
            [
                'name' => ['required', 'max:191'],
                'search_keywords' => ['nullable', 'string', 'max:2048'],
            ]
        );
    }

}
