<?php


namespace App\Modules\Products\Widgets;


use App\Components\Widget\AbstractWidget;

class GroupSortable implements AbstractWidget
{
    public $group;

    public function __construct($group)
    {
        $this->group = $group;
    }

    public function render()
    {
        return view('products::admin.groups.widgets.sortable', [
            'group' => $this->group,
        ]);
    }
}