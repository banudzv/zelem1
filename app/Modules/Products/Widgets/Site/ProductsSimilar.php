<?php

namespace App\Modules\Products\Widgets\Site;

use App\Components\Widget\AbstractWidget;
use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use Catalog;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ProductsSimilar
 *
 * @package App\Modules\Products\Widgets\Site
 */
class ProductsSimilar implements AbstractWidget
{

    /**
     * @var Product
     */
    protected $product;

    /**
     * ProductsSimilar constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $category = $this->product->group->category;
        $categoryIds = [];
        foreach ($category->otherCategories as $cat) {
            $ids = Category::getAllChildrenIds($cat->id);
            $ids[] = $cat->id;
            $categoryIds += $ids;
        }
        $categoryIds[] = $category->id;

        $featuresIds = $category->features->pluck('id')->toArray();
        $valueIds = $this->product->featureValues->whereIn('feature_id', $featuresIds)->pluck('value_id')->toArray();
        $products = ProductGroup::whereHas('featureValues', function (Builder $builder) use ($featuresIds, $valueIds) {
                $builder->whereIn('feature_id', $featuresIds)->whereIn('value_id', $valueIds);
            })
            ->whereHas('category', function (Builder $builder) use ($categoryIds){
                $builder->whereIn('id', $categoryIds);
            })
            ->where('id', '!=', $this->product->group->id)
            ->limit(15)
            ->get();

        $products->loadMissing(
            [
                'products.current',
                'current',
                'labels',
                'comments',
                'products.images',
                'brand.current',
                'category.current'
            ]
        );
        Catalog::ecommerce()->addProducts($products, trans('products::site.similar'));

        return view('products::site.widgets.product-similar', [
            'products' => $products,
            'product' => $this->product,
        ]);
    }

}
