<?php

namespace App\Modules\Products\Widgets\Site;

use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Requests\FilteringRequest;
use Widget;

class ProductsListCategoryPage extends AbstractProductsListPage
{

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var array|int[]
     */
    protected $categoryIds;

    public function __construct(Category $category)
    {
        $this->category = $category;

        $this->categoryIds = Category::getCategoriesIds($this->category->id);
    }

    public function render()
    {
        $filterStateResolver = $this->getFilterStateResolver()
            ->setConfig($this->getProductFilterConfig())
            ->setRequestForFilteringGroups($this->getRequestForFilteringGroups())
            ->setRequestForBasicFilter($this->getRequestForBasicFilter())
            ->addRequestsForRecalculation($this->getRequestsForRecalculation())
            ->resolve();

        $groups = $filterStateResolver->filteredGroups();

        if ($groups->isEmpty()) {
            return null;
        }

        $filter = Widget::show('products::filter', $this->getProductFilterConfig(), $filterStateResolver);

        return view(
            'products::site.products-list',
            [
                'groups' => $groups,
                'categoryId' => $this->category->id,
                'filter' => $filter,
            ]
        );
    }

    protected function getRequestForFilteringGroups()
    {
        $perPage = $this->getRequest()->query('per-page')
            ?: config('db.products.site-per-page', ProductGroup::LIMIT_PER_PAGE_BY_DEFAULT);
        $page = resolve('paginateroute')->currentPage();

        $parameters = $this->getBasicQueryParameters();
        $parameters['filter'] = $this->getParamsWithoutExcepted();

        $parameters = array_merge($parameters, $this->getRequest()->only(['price', 'order', 'car']));
        $parameters['order'] = $parameters['order'] ?? 'default';

        return FilteringRequest::create()
            ->setParameters($parameters)
            ->setPerPage($perPage)
            ->setPage($page);
    }

    protected function getParamsWithoutExcepted(): array
    {
        return $this->getRequest()
            ->except(
                array_merge([
                        'query',
                        'price',
                        'order',
                        'per-page',
                        'page',
                        'car',
                        'categories'
                    ],
                    config('products.excluded_url_params', [])
                )
            );
    }

    protected function getRequestForBasicFilter(): FilteringRequest
    {
        return FilteringRequest::create()
            ->setParameters($this->getBasicQueryParameters());
    }

    protected function getRequestsForRecalculation(): array
    {
        $parameters = $this->getBasicQueryParameters();

        $parameters['filter'] = $this->getParamsWithoutExcepted();

        $parameters = array_merge($parameters, $this->getRequest()->only(['price']));

        if (count($parameters['filter']) === 0 && empty($parameters['price'])) {
            return [];
        }

        $filters = $this->recalculateParameters($parameters);

        return $this->createFilteringRequests($filters);
    }

    protected function getFilterConfigParameters(): array
    {
        return array_merge(
            parent::getFilterConfigParameters(),
            [
                ProductFilter::ALIAS_KEY_FEATURE => $this->category->isShowProductFeaturesInFilter(),
                ProductFilter::ALIAS_KEY_CAR => $this->category->isShowEngineFeaturesInFilter(),
                ProductFilter::ALIAS_KEY_BRAND => true,
            ]
        );
    }

    protected function getBasicQueryParameters(): array
    {
        $parameters['category_ids'] = $this->categoryIds;

        if ($query = $this->getRequest()->input('query')) {
            $parameters['query'] = $query;
        }

        return $parameters;
    }

}
