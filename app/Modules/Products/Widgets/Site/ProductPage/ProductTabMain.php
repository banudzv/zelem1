<?php

namespace App\Modules\Products\Widgets\Site\ProductPage;

use App\Components\Widget\AbstractWidget;
use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\Service;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ProductTabMain
 *
 * @package App\Modules\Products\Widgets\Site\ProductPage
 */
class ProductTabMain implements AbstractWidget
{

    /**
     * @var Product
     */
    protected $product;

    /**
     * ProductCard constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        if (!$this->product) {
            return null;
        }
        $this->product->loadMissing(['group.allEngineNumbers.vendor',
            'group.allEngineNumbers.model', 'group.allEngineNumbers.power.current',
            'group.allEngineNumbers.volume']);
        if ($services = $this->getServices($this->product->group->category)) {
            $services->loadMissing(['current', 'options.current']);
            $services = $services->filter(function (Service $service) {
                return $service->options->isNotEmpty();
            });
        }

        return view('products::site.widgets.product.product-facade.product-facade', [
            'product' => $this->product,
            'engineNumbers' => $this->product->group->allEngineNumbers,
            'years' => $this->product->group->years->implode('year', ', '),
            'services' => $services,
        ]);
    }

    /**
     * Returns the list of services to show to the user.
     *
     * @param Category|null $category
     * @return Collection|null
     */
    protected function getServices(?Category $category): ?Collection
    {
        if (!$category) {
            return null;
        }
        if ($services = $category->services) {
            return $services;
        }

        return $this->getServices($category->parent);
    }
}
