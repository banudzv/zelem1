<?php

namespace App\Modules\Products\Widgets\Site;

use App\Components\Widget\BasicWidget;
use App\Modules\Brands\Models\Brand;
use App\Modules\Categories\Models\Category;
use App\Modules\Features\Models\FeatureValue;
use App\Modules\Products\Dto\ProductFilterConfig;
use App\Modules\Products\Services\FilterResponseResolver;
use App\Modules\Products\Services\ProductFilterStateResolverInterface;
use Catalog;
use Illuminate\Database\Eloquent\Collection;
use ProductsFilter;
use Widget;

class ProductFilter extends BasicWidget
{
    public const ALIAS_KEY_BRAND = 'brand';

    public const ALIAS_KEY_CATEGORY = 'category';

    public const ALIAS_KEY_FEATURE = 'feature';

    public const ALIAS_KEY_PRICE = 'price';

    public const ALIAS_KEY_CAR = 'car';

    /**
     * @var ProductFilterStateResolverInterface
     */
    protected $productResolver;

    /**
     * @var ProductFilterConfig
     */
    protected $config;

    public function __construct(ProductFilterConfig $config, ProductFilterStateResolverInterface $productResolver)
    {
        $this->config = $config;
        $this->productResolver = $productResolver;
    }

    public function render()
    {
        $basic = $this->productResolver->basicStats();
        $this->generateBlocks($basic);

        ProductsFilter::generateIds();

        $this->setMainBlocks();

        $this->fillCounters($basic);

        $this->fillFilteredCounters($this->productResolver->recalculatedStats());

        $priceWidget = $this->buildPriceWidget($basic);
        $carFilter = $this->buildCarFilter();

        return view(
            'products::site.widgets.filter-accordion.accordion',
            [
                'priceRange' => $priceWidget,
                'carFilter' => $carFilter,
                'query' => $this->getQueryParameter(),
            ]
        );
    }

    private function generateBlocks(FilterResponseResolver $basic)
    {
        if ($this->config->isShow(self::ALIAS_KEY_BRAND)) {
            $this->generateBrandBlock($basic->getBrands());
        }

        if ($this->config->isShow(self::ALIAS_KEY_CATEGORY)) {
            $this->generateCategoryBlock($basic->getCategories());
        }

        if ($this->config->isShow(self::ALIAS_KEY_FEATURE)) {
            $this->generateFeatureBlocks($basic->getFeatureValues());
        }
    }

    /**
     * @param  Collection|Brand[]  $brands
     */
    private function generateBrandBlock(Collection $brands)
    {
        if (ProductsFilter::hasBlock(self::ALIAS_KEY_BRAND)) {
            return;
        }

        $block = ProductsFilter::addBlock(self::ALIAS_KEY_BRAND, 'brands::site.brand', self::ALIAS_KEY_BRAND, true);

        foreach ($brands as $brand) {
            $block->addElement($brand->getKey(), $brand->getName(), $brand->getSlug());
        }
    }

    /**
     * @param  Collection|Category[]  $categories
     */
    private function generateCategoryBlock(Collection $categories)
    {
        if (ProductsFilter::hasBlock(self::ALIAS_KEY_CATEGORY)) {
            return;
        }

        $block = ProductsFilter::addBlock(
            self::ALIAS_KEY_CATEGORY,
            'categories::site.category',
            self::ALIAS_KEY_CATEGORY,
            true
        );

        foreach ($categories as $category) {
            $block->addElement($category->getKey(), $category->getName(), $category->getSlug());
        }
    }

    /**
     * @param  Collection|FeatureValue[]  $featureValues
     */
    private function generateFeatureBlocks(Collection $featureValues)
    {
        foreach ($featureValues as $featureValue) {
            $feature = $featureValue->feature;

            $block = ProductsFilter::getBlockById($feature->getKey())
                ?: ProductsFilter::addBlock(
                    $feature->getKey(),
                    $feature->getName(),
                    $feature->getSlug(),
                    true
                );

            if ($feature->isMultiple()) {
                $block->setAsMultiple();
            }

            $block->addElement($featureValue->getKey(), $featureValue->getName(), $featureValue->getSlug());
        }
    }

    private function setMainBlocks()
    {
        //STUB
    }

    private function fillCounters(FilterResponseResolver $basic)
    {
        $this->fillBrandsCounters($basic->getBrandStats());
        $this->fillCategoriesCounters($basic->getCategoryStats());
        $this->fillFeatureValuesCounters($basic->getFeatureValuesStats());
    }

    private function fillBrandsCounters(array $brandStats)
    {
        foreach ($brandStats as $brandId => $counter) {
            $this->setCounter(self::ALIAS_KEY_BRAND, $brandId, $counter);
        }
    }

    protected function setCounter($blockId, $elementId, int $count)
    {
        ProductsFilter::setCounters([$blockId => [$elementId => $count]]);
    }

    private function fillCategoriesCounters(array $categoryStats)
    {
        foreach ($categoryStats as $brandId => $counter) {
            $this->setCounter(self::ALIAS_KEY_CATEGORY, $brandId, $counter);
        }
    }

    private function fillFeatureValuesCounters(array $featureStats)
    {
        foreach ($featureStats as $featureId => $featureValueStats) {
            foreach ($featureValueStats as $featureValueId => $counter) {
                $this->setCounter($featureId, $featureValueId, $counter);
            }
        }
    }

    private function fillFilteredCounters(array $filteredStats)
    {
        if (empty($filteredStats)) {
            return;
        }

        foreach (ProductsFilter::getBlocks() as $block) {
            foreach ($block->elements as $element) {
                if (array_key_exists($block->alias, $filteredStats)) {
                    $filteredBlockStats = $filteredStats[$block->alias];
                } else {
                    $filteredBlockStats = $filteredStats[0];
                }

                $this->setFilteredCounter($block->id, $element->id, $filteredBlockStats[$element->id] ?? 0);
            }
        }
    }

    protected function setFilteredCounter($blockId, int $elementId, int $count)
    {
        ProductsFilter::setFilteredCounters([$blockId => [$elementId => $count]]);
    }

    private function buildPriceWidget(FilterResponseResolver $basic): ?string
    {
        if ($this->config->isShow(self::ALIAS_KEY_PRICE)) {
            $minPrice = $basic->getMinPrice();
            $maxPrice = $basic->getMaxPrice();

            if (Catalog::currenciesLoaded()) {
                $minPrice = Catalog::currency()->calculate($minPrice);
                $maxPrice = Catalog::currency()->calculate($maxPrice);
            }

            /** @see ProductFilterPrice */
            return Widget::show('products::filter-price', $minPrice, $maxPrice);
        }

        return null;
    }

    private function buildCarFilter(): ?string
    {
        if ($this->config->isShow(self::ALIAS_KEY_CAR)) {
            /** @see CarFilter */
            return Widget::show('products::car-filter', $this->config);
        }

        return null;
    }

    private function getQueryParameter(): ?string
    {
        return $this->getRequest()->input('query');
    }

}