<?php

namespace App\Modules\Products\Widgets\Site;

use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ServicePrice;
use Illuminate\Support\Collection;

/**
 * Class ProductInCheckout
 *
 * @package App\Modules\Products\Widgets\Site
 */
class ProductInCheckout implements AbstractWidget
{
    /**
     * @var int
     */
    protected $productId;
    /**
     * @var int
     */
    protected $quantity;
    /**
     * @var int|null
     */
    protected $dictionaryId;
    /**
     * @var Collection|ServicePrice[]|null
     */
    protected $services;

    /**
     * ProductInCheckout constructor.
     *
     * @param int $productId
     * @param int $quantity
     * @param int|null $dictionaryId
     * @param Collection|ServicePrice[]|null $services
     */
    public function __construct(int $productId, int $quantity, ?int $dictionaryId = null, ?Collection $services = null)
    {
        $this->productId = $productId;
        $this->quantity = $quantity;
        $this->dictionaryId = $dictionaryId;
        $this->services = $services;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        if (!$product = Product::getOne($this->productId)) {
            return null;
        }

        return view('products::site.widgets.order-product.order-product--checkout', [
            'product' => $product,
            'formattedPrice' => $product->formatted_price,
            'formattedAmount' => $this->getAmount($product),
            'quantity' => $this->quantity,
            'dictionaryId' => $this->dictionaryId,
            'services' => $this->services ? $this->services->map(function (ServicePrice $option) {
                return [
                    'option' => $option->current->name,
                    'service' => $option->service->current->name,
                    'price' => $option->price,
                ];
             })->toArray() : [],
        ]);
    }

    /**
     * @param Product $product
     * @return string
     */
    protected function getAmount(Product $product): string
    {
        $amount = calc($product->price * $this->quantity);
        foreach ($this->services ?? [] as $service) {
            $amount += $service->price * $this->quantity;
        }

        return format_only($amount);
    }
}
