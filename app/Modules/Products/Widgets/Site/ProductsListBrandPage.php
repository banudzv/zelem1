<?php

namespace App\Modules\Products\Widgets\Site;

use App\Modules\Brands\Models\Brand;
use App\Modules\BrandsBanner\Models\Banner;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Requests\FilteringRequest;
use Widget;

class ProductsListBrandPage extends AbstractProductsListPage
{

    /**
     * @var Brand
     */
    protected $brand;

    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    public function render()
    {
        $filterStateResolver = $this->getFilterStateResolver()
            ->setConfig($this->getProductFilterConfig())
            ->setRequestForFilteringGroups($this->getRequestForFilteringGroups())
            ->setRequestForBasicFilter($this->getRequestForBasicFilter())
            ->addRequestsForRecalculation($this->getRequestsForRecalculation())
            ->resolve();

        $groups = $filterStateResolver->filteredGroups();

        if ($groups->isEmpty()) {
            return null;
        }

        $filter = Widget::show('products::filter', $this->getProductFilterConfig(), $filterStateResolver);

        $brandBanners = Banner::whereBrandId($this->brand->id)->active()->get();
        return view(
            'products::site.brand-products-list',
            [
                'groups' => $groups,
                'filter' => $filter,
                'brandBanners' => $brandBanners
            ]
        );
    }

    protected function getRequestForFilteringGroups()
    {
        $perPage = $this->getRequest()->query('per-page')
            ?: config('db.products.site-per-page', ProductGroup::LIMIT_PER_PAGE_BY_DEFAULT);
        $page = resolve('paginateroute')->currentPage();

        $parameters['brand_ids'] = [$this->brand->id];
        $parameters['filter'] = $this->getRequest()
            ->except(
                $this->exceptParameters()
            );

        $parameters = array_merge($parameters, $this->getRequest()->only(['price', 'order', 'car']));
        $parameters['order'] = $parameters['order'] ?? 'default';

        return FilteringRequest::create()
            ->setParameters($parameters)
            ->setPerPage($perPage)
            ->setPage($page);
    }

    protected function getRequestForBasicFilter()
    {
        $parameters['brand_ids'] = [$this->brand->id];

        return FilteringRequest::create()
            ->setParameters($parameters);
    }

    protected function getRequestsForRecalculation()
    {
        $parameters['brand_ids'] = [$this->brand->id];

        $parameters['filter'] = $this->getRequest()
            ->except(
                $this->exceptParameters()
            );

        $parameters = array_merge($parameters, $this->getRequest()->only(['price']));

        if ($query = $this->getRequest()->input('query')) {
            $parameters['query'] = $query;
        }

        if (count($parameters['filter']) === 0 && empty($parameters['price'])) {
            return [];
        }

        $filters = $this->recalculateParameters($parameters);

        return $this->createFilteringRequests($filters);
    }

    protected function getFilterConfigParameters(): array
    {
        return array_merge(
            parent::getFilterConfigParameters(),
            [
                ProductFilter::ALIAS_KEY_CAR => $this->brand->isShowEngineFeaturesInFilter(),
                ProductFilter::ALIAS_KEY_FEATURE => $this->brand->isShowProductFeaturesInFilter(),
                ProductFilter::ALIAS_KEY_CATEGORY => true,
            ]
        );
    }

    /**
     * @return string[]
     */
    protected function exceptParameters(): array
    {
        return [
            'query',
            'price',
            'order',
            'per-page',
            'page',
            'car',
            'brand'
        ];
    }
}
