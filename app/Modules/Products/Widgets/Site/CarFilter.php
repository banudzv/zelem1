<?php

namespace App\Modules\Products\Widgets\Site;

use App\Components\Widget\AbstractWidget;
use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Engine\Models\Model as EngineModel;
use App\Modules\Engine\Models\PowerTranslates;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use Cache;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CarFilter implements AbstractWidget
{

    public function render()
    {
        $query = $this->getRequest()->query('car', []);

        return view(
            'products::site.widgets.car-filter',
            [
                'vendors' => $this->getVendors(),
                'chosenVendorId' => (int)Arr::get($query, 'vendor'),
                'models' => $this->getModels(Arr::get($query, 'vendor')),
                'chosenModelId' => (int)Arr::get($query, 'model'),
                'volumes' => $this->getVolumes(Arr::get($query, 'model')),
                'chosenVolumeId' => (int)Arr::get($query, 'volume'),
                'powers' => $this->getPowers(Arr::get($query, 'volume')),
                'chosenPowerId' => (int)Arr::get($query, 'power'),
                'engineNumbers' => $this->getEngineNumbers(Arr::get($query, 'power')),
                'chosenEngineNumber' => Arr::get($query, 'number'),
            ]
        );
    }

    protected function getRequest(): Request
    {
        return request();
    }

    protected function getVendors(): array
    {
        return Cache::remember(
            'car_vendor_list',
            3600,
            function () {
                return Vendor::pluck('name', 'id')->toArray();
            }
        );
    }

    protected function getModels(?int $vendorId): array
    {
        return $vendorId
            ? EngineModel::whereVendorId($vendorId)->oldest('name')->pluck('name', 'id')->toArray()
            : [];
    }

    protected function getVolumes(?int $modelId): array
    {
        return $modelId
            ? Volume::whereModelId($modelId)->oldest('name')->pluck('name', 'id')->toArray()
            : [];
    }

    protected function getPowers(?int $volumeId): array
    {
        return $volumeId
            ? PowerTranslates::whereHas(
                'row',
                function (Builder $builder) use ($volumeId) {
                    $builder->where('volume_id', $volumeId);
                }
            )
                ->whereLangIsCurrent()
                ->oldest('name')
                ->pluck('name', 'row_id')
                ->toArray()
            : [];
    }

    protected function getEngineNumbers(?int $powerId): array
    {
        return $powerId
            ? EngineNumber::wherePowerId($powerId)
                ->oldest('engine_number')
                ->pluck('engine_number', 'engine_number')
                ->toArray()
            : [];
    }

}