<?php

namespace App\Modules\Products\Widgets\Site;

use App\Components\Widget\BasicWidget;
use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Engine\Models\Model;
use App\Modules\Engine\Models\PowerTranslates;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use Catalog;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use ProductsFilter;

/**
 * Class ProductFilterChosen
 * @package App\Modules\Products\Widgets\Site
 */
class ProductFilterChosen extends BasicWidget
{
    public function render()
    {
        $elements = new Collection();
        $this->addChosenFeatureValues($elements);
        $price = $this->getChosenPrice();
        $carChosenElements = $this->getCarChosenElements();
        if ($elements->isEmpty() && !$price && !$this->getSearchQuery() && !$carChosenElements) {
            return null;
        }

        return view('products::site.widgets.filter-param.param', compact('elements', 'carChosenElements', 'price'));
    }

    /**
     * Returns an array with chosen car engine elements in filter data.
     *
     * @return array
     */
    protected function getCarChosenElements(): array
    {
        $carQuery = $this->getRequest()->query('car', []);
        $carChosenElements = [];
        if ($vendorId = Arr::get($carQuery, 'vendor')) {
            if ($vendor = Vendor::whereId($vendorId)->select('name')->first()) {
                $carChosenElements[] = [
                    'name' => $vendor->name,
                    'link' => $this->getLinkWithoutEngineParams('vendor', 'model', 'volume', 'power', 'number'),
                ];

                if ($modelId = Arr::get($carQuery, 'model')) {
                    if ($model = Model::whereId($modelId)->select('name')->first()) {
                        $carChosenElements[] = [
                            'name' => $model->name,
                            'link' => $this->getLinkWithoutEngineParams('model', 'volume', 'power', 'number'),
                        ];

                        if ($volumeId = Arr::get($carQuery, 'volume')) {
                            if ($volume = Volume::whereId($volumeId)->select('name')->first()) {
                                $carChosenElements[] = [
                                    'name' => $volume->name,
                                    'link' => $this->getLinkWithoutEngineParams('volume', 'power', 'number'),
                                ];

                                if ($powerId = Arr::get($carQuery, 'power')) {
                                    if ($power = PowerTranslates::whereRowId($powerId)->select('name')->first()) {
                                        $carChosenElements[] = [
                                            'name' => $power->name,
                                            'link' => $this->getLinkWithoutEngineParams('power', 'number'),
                                        ];

                                        if ($number = Arr::get($carQuery, 'number')) {
                                            $carChosenElements[] = [
                                                'name' => $number,
                                                'link' => $this->getLinkWithoutEngineParams('number'),
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $carChosenElements;
    }

    /**
     * Builds new url without unnecessary query elements.
     *
     * @param array<string> $params
     * @return string
     */
    protected function getLinkWithoutEngineParams(...$params): string
    {
        $query = $this->getRequest()->query();
        foreach ($params as $param) {
            unset($query['car'][$param]);
        }

        return $this->getRequest()->url() . '?' . http_build_query($query);
    }

    protected function addChosenFeatureValues(Collection $chosen): void
    {
        foreach (ProductsFilter::getBlocks() as $block) {
            foreach ($block->getElements() as $element) {
                if ($element->selected) {
                    $chosen->push($element);
                }
            }
        }
    }

    private function getChosenPrice(): ?array
    {
        list ($minPrice, $maxPrice) = array_pad(explode('-', request()->query('price')), 2, null);

        if (!$minPrice && !$maxPrice) {
            return null;
        }

        if (!$minPrice) {
            if (Catalog::currenciesLoaded()) {
                $maxPrice = Catalog::currency()->formatWithoutCalculation($maxPrice);
            }

            $text = "до $maxPrice";
        } elseif (!$maxPrice) {
            if (Catalog::currenciesLoaded()) {
                $minPrice = Catalog::currency()->formatWithoutCalculation($minPrice);
            }

            $text = "от $minPrice";
        } else {
            if (Catalog::currenciesLoaded()) {
                $minPrice = Catalog::currency()->formatWithoutCalculation((float)$minPrice);
                $maxPrice = Catalog::currency()->formatWithoutCalculation((float)$maxPrice);
            }

            $text = "от $minPrice до $maxPrice";
        }

        $parameters = ProductsFilter::getParametersAsArray(ProductsFilter::getParameters());

        unset($parameters['price']);

        return [
            'name' => $text,
            'link' => ProductsFilter::generateUrl($parameters),
        ];
    }

    protected function getSearchQuery(): ?string
    {
        return $this->getRequest()->input('query');
    }

}
