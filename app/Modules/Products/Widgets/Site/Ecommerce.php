<?php

namespace App\Modules\Products\Widgets\Site;


use App\Components\Widget\AbstractWidget;
use Catalog;

class Ecommerce implements AbstractWidget
{

    public function render()
    {
        $object = Catalog::ecommerce()->getObject();
        if (!$object) {
            return null;
        }

        return view(
            'products::site.widgets.ecommerce',
            [
                'object' => $object
            ]
        );
    }
}