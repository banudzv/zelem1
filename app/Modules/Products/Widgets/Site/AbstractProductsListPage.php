<?php

namespace App\Modules\Products\Widgets\Site;

use App\Components\Widget\BasicWidget;
use App\Modules\Products\Dto\ProductFilterConfig;
use App\Modules\Products\Requests\FilteringRequest;
use App\Modules\Products\Services\ProductFilterStateResolverInterface;

abstract class AbstractProductsListPage extends BasicWidget
{

    protected function createFilteringRequests(array $filters): array
    {
        $result = [];

        foreach ($filters as $key => $filter) {
            $result[] = FilteringRequest::create()
                ->setParameters($filter)
                ->setKey($key);
        }

        return $result;
    }

    protected function getFilterStateResolver(): ProductFilterStateResolverInterface
    {
        return resolve(ProductFilterStateResolverInterface::class);
    }

    protected function recalculateParameters(array $parameters): array
    {
        $filters = $this->initFilters($parameters);

        foreach ($parameters['filter'] as $key => $values) {
            $filter = $parameters;

            unset($filter['filter'][$key]);

            $filters[$key] = $filter;
        }
        return $filters;
    }

    protected function initFilters(array $parameters): array
    {
        $filters[0] = $parameters;

        if ($this->configParametersShow(ProductFilter::ALIAS_KEY_BRAND)) {
            $filters[ProductFilter::ALIAS_KEY_BRAND] = $parameters;
        }

        if ($this->configParametersShow(ProductFilter::ALIAS_KEY_CATEGORY)) {
            $filters[ProductFilter::ALIAS_KEY_CATEGORY] = $parameters;
        }

        return $filters;
    }

    protected function configParametersShow($key): bool
    {
        return $this->getFilterConfigParameters()[$key] ?? false;
    }

    protected function getFilterConfigParameters(): array
    {
        return [
            ProductFilter::ALIAS_KEY_PRICE => config('db.products.show-filter-by-price', true),
        ];
    }

    protected function getProductFilterConfig(): ProductFilterConfig
    {
        return new ProductFilterConfig($this->getFilterConfigParameters());
    }

    abstract protected function getRequestsForRecalculation();

    abstract protected function getRequestForBasicFilter();

    abstract protected function getRequestForFilteringGroups();

}