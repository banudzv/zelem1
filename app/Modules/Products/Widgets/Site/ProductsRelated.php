<?php

namespace App\Modules\Products\Widgets\Site;

use App\Components\Widget\AbstractWidget;
use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use Catalog;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ProductsRelated
 *
 * @package App\Modules\Products\Widgets\Site
 */
class ProductsRelated implements AbstractWidget
{

    /**
     * @var Product
     */
    protected $product;

    /**
     * ProductsRelated constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $category = $this->product->group->category;
        $categoryIds = [];
        foreach ($category->relCategories as $cat){
            $ids = Category::getAllChildrenIds($cat->id);
            $ids[] = $cat->id;
            $categoryIds += $ids;
        }

        $featuresIds = $category->features->pluck('id')->toArray();
        $ids = $this->product->group->featureValues->pluck('feature_id')->toArray();
        $findIds = array_intersect($ids, $featuresIds);
        $ids = $this->product->group->featureValues()->whereIn('feature_id', $findIds)->pluck('value_id')->toArray();
        $products = ProductGroup::whereHas('featureValues', function (Builder $builder) use ($findIds, $ids) {
            $builder->whereIn('feature_id', $findIds)
                ->whereIn('value_id', $ids);
        })->whereHas('category', function (Builder $builder) use ($categoryIds){
            $builder->whereIn('id', $categoryIds);
        })->where('id', '!=', $this->product->group->id)->get()->each(function(ProductGroup $group){
            return $group->products;
        })->take(15);

        $products->loadMissing(
            [
                'products.current',
                'current',
                'labels',
                'comments',
                'products.images',
                'brand.current',
                'category.current'
            ]
        );
        Catalog::ecommerce()->addProducts($products, trans('products::site.related'));

        return view('products::site.widgets.product-related', [
            'products' => $products,
            'product' => $this->product,
        ]);
    }

}
