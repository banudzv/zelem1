<?php

namespace App\Modules\Products\Widgets\Site;

use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\Product;
use Illuminate\Support\Collection;

/**
 * Class ProductCartItem
 *
 * @package App\Modules\Products\Widgets\Site
 */
class ProductCartItem implements AbstractWidget
{
    
    /**
     * @var int
     */
    protected $productId;
    
    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var int|null
     */
    protected $dictionaryId;
    /**
     * @var Collection|null
     */
    protected $services;

    /**
     * ProductCartItem constructor.
     *
     * @param int $productId
     * @param int $quantity
     * @param int|null $dictionaryId
     * @param Collection|null $services
     */
    public function __construct(int $productId, int $quantity, $dictionaryId = null, ?Collection $services = null)
    {
        $this->productId = $productId;
        $this->quantity = $quantity;
        $this->dictionaryId = $dictionaryId;
        $this->services = $services;
    }
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        if (!$product = Product::getOne($this->productId)) {
            return null;
        }

        return view('products::site.widgets.cart-item.cart-item', [
            'product' => $product,
            'quantity' => $this->quantity,
            'dictionaryId' => $this->dictionaryId,
            'services' => $this->services ?? [],
        ]);
    }
}
