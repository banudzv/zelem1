<?php


namespace App\Modules\Products\Widgets;


use App\Components\Widget\AbstractWidget;
use App\Modules\Products\Models\Product;
use Form;

class ProductLiveSearch implements AbstractWidget
{

    /**
     * @var array
     */
    protected $ignored;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int|null
     */
    protected $chosenProductId;

    /**
     * ProductLiveSearchSelect constructor.
     *
     * @param array $ignored
     * @param string $name
     * @param int|null $chosenProductId
     */
    public function __construct(array $ignored = [], string $name = 'product_id', ?int $chosenProductId = null)
    {
        $this->ignored = $ignored;
        $this->name = $name;
        $this->chosenProductId = $chosenProductId;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     * @return \Illuminate\Support\HtmlString
     * @throws \Throwable
     */
    public function render()
    {
        $options = [];
        if ($this->chosenProductId) {
            $product = Product::find($this->chosenProductId);
            if ($product && $product->exists) {
                $options[$product->id] = view('products::admin.product.widgets.live-search-select-markup', [
                    'product' => $product,
                ])->render();
            }
        }
        return Form::select($this->name, $options, $this->chosenProductId, [
            'class' => ['form-control', 'js-data-ajax'],
            'data-href' => route('admin.products.live-search'),
            'data-ignored' => json_encode($this->ignored),
        ]);
    }
}