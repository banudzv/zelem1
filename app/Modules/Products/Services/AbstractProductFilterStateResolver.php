<?php

namespace App\Modules\Products\Services;

use App\Modules\Products\Dto\ProductFilterConfig;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Requests\FilteringRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

abstract class AbstractProductFilterStateResolver implements ProductFilterStateResolverInterface
{

    /**
     * @var FilteringRequest
     */
    protected $filterRequest;

    /**
     * @var FilteringRequest
     */
    protected $basicRequest;

    /**
     * @var null|FilteringRequest[]|Collection
     */
    protected $recalculationRequests;

    /**
     * @var null|LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|ProductGroup[]
     */
    protected $filteredGroups;

    /**
     * @var null|FilterResponseResolver
     */
    protected $basicStats;

    /**
     * @var null|array
     */
    protected $recalculatedStats;

    /**
     * @var ProductFilterConfig
     */
    protected $config;

    public function setRequestForFilteringGroups(FilteringRequest $request): ProductFilterStateResolverInterface
    {
        $this->filterRequest = $request;

        return $this;
    }

    public function basicStats(): FilterResponseResolver
    {
        if (is_null($this->basicStats)) {
            $this->basicStats = $this->fetchBasicStats();
        }

        return $this->basicStats;
    }

    public function filteredGroups()
    {
        if (is_null($this->filteredGroups)) {
            $this->filteredGroups = $this->fetchFilteredGroups();
        }

        return $this->filteredGroups;
    }

    public function recalculatedStats()
    {
        if (is_null($this->recalculatedStats)) {
            $this->recalculatedStats = $this->fetchRecalculatedStats();
        }

        return $this->recalculatedStats;
    }

    abstract protected function fetchFilteredGroups();

    abstract protected function fetchBasicStats();

    abstract protected function fetchRecalculatedStats();

    public function setRequestForBasicFilter(FilteringRequest $request): ProductFilterStateResolverInterface
    {
        $this->basicRequest = $request;

        return $this;
    }

    public function addRequestsForRecalculation(array $requests): ProductFilterStateResolverInterface
    {
        $this->recalculationRequests = collect($requests);

        return $this;
    }

    public function setConfig(ProductFilterConfig $config): ProductFilterStateResolverInterface
    {
        $this->config = $config;

        return $this;
    }

    public function isShow(string $blockName): bool
    {
        return $this->config->isShow($blockName);
    }
}