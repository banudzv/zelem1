<?php

namespace App\Modules\Products\Services;

use App\Modules\Products\Filters\ElasticSluggableFilter;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Requests\FilteringRequest;
use App\Modules\Products\Widgets\Site\ProductFilter;
use App\Modules\Search\Models\SearchBuilder;
use App\Modules\Search\Resolvers\ElasticResponseResolver;
use ElasticClient;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class ElasticFilterStateResolver extends AbstractProductFilterStateResolver
{

    public function resolve(): ProductFilterStateResolverInterface
    {
        $queries = $this->makeQueries();

        $this->resolveByResult(
            $this->fetchByQueries($queries)
        );

        return $this;
    }

    private function makeQueries(): Collection
    {
        $queries = Collection::make();

        if ($this->basicRequest) {
            $queries->put(
                'basicRequest',
                $this->basicStatsQuery($this->basicRequest)->query()
            );
        }

        if ($this->filterRequest) {
            $queries->put(
                'filterRequest',
                $this->filteredGroupQuery($this->filterRequest)
                    ->setPerPage($this->filterRequest->getPerPage())
                    ->setPage($this->filterRequest->getPage())
                    ->query()
            );
        }

        if ($this->recalculationRequests->isNotEmpty()) {
            $queries = $queries->merge($this->makeRecalculateQueries());
        }

        return $queries;
    }

    private function filteredGroupQuery(FilteringRequest $request): SearchBuilder
    {
        return Product::searchBuilder()
            ->select(['group_id'])
            ->where('active', true)
            ->orderBy('available', 'desc')
            ->filter($request->getParameters(), ElasticSluggableFilter::class);
    }

    private function makeRecalculateQueries(): Collection
    {
        $queries = Collection::make();

        foreach ($this->recalculationRequests as $request) {
            $queries->put(
                $request->getKey(),
                $this->recalculatedStatsQuery($request)->query()
            );
        }

        return $queries;
    }

    public function recalculatedStatsQuery(FilteringRequest $request): SearchBuilder
    {
        return Product::searchBuilder()
            ->select([false])
            ->aggr('brands', 'brand_id')
            ->aggr('categories', 'category_ids')
            ->aggr('featureValues', 'feature_value_ids')
            ->filter($request->getParameters(), ElasticSluggableFilter::class);
    }

    private function resolveByResult(Collection $responses): void
    {
        if ($this->basicRequest) {
            $elasticResponseResolver = SearchBuilder::resolveRaw($responses->shift());
            $this->basicStats = $this->resolveBasicStats($elasticResponseResolver);
        }

        if ($this->filterRequest) {
            $this->filteredGroups = $this->resolveFilteredGroups($responses);
        }

        if ($this->recalculationRequests && $this->recalculationRequests->isNotEmpty()) {
            $this->recalculatedStats = $this->resolveRecalculatedResponse($responses);
        }
    }

    private function resolveFilteredGroups(Collection $responses): LengthAwarePaginator
    {
        $resolver = SearchBuilder::resolveRaw($responses->shift());

        $modelIds = Arr::pluck($resolver->items(), '_source.group_id');
        $groups = ProductGroup::find($modelIds)
            ->sortBy(sort_models_by_ids_order($modelIds));

        return new LengthAwarePaginator(
            $groups,
            $resolver->total(),
            $this->filterRequest->getPerPage(),
            $this->filterRequest->getPage()
        );
    }

    private function resolveRecalculatedResponse(Collection $responses): array
    {
        $recalculated = [];

        foreach ($this->recalculationRequests as $request) {
            $elasticResponseResolver = new ElasticResponseResolver($responses->shift());
            $recalculated[$request->getKey()] = $this->chooseAggregation($request, $elasticResponseResolver);
        }

        return $recalculated;
    }

    private function chooseAggregation(
        FilteringRequest $request,
        ElasticResponseResolver $elasticResponseResolver
    ): array {
        if ($request->getKey() === ProductFilter::ALIAS_KEY_BRAND) {
            return $elasticResponseResolver->getAggregationByName('brands');
        }

        if ($request->getKey() === ProductFilter::ALIAS_KEY_CATEGORY) {
            return $elasticResponseResolver->getAggregationByName('categories');
        }

        return $elasticResponseResolver->getAggregationByName('featureValues');
    }

    private function fetchByQueries(Collection $queries): Collection
    {
        if ($queries->isEmpty()) {
            return Collection::make();
        }

        $multipleSearchRequests = $this->makeMultipleSearchRequests($queries->all());
        $result = ElasticClient::msearch($multipleSearchRequests);

        return Collection::make($result['responses']);
    }

    private function makeMultipleSearchRequests(array $queries): array
    {
        $body = [];

        foreach ($queries as $query) {
            $body[] = $this->getCurrentIndexPayload();
            $body[] = $query;
        }

        return ['body' => $body];
    }

    private function getCurrentIndexPayload(): array
    {
        /** @var Collection $payload */
        $payload = Product::searchByText('*')->buildPayload();
        $indexConfig = $payload->first();

        unset($indexConfig['body']);

        return $indexConfig;
    }

    /**
     * @throws Exception
     */
    protected function fetchBasicStats(): FilterResponseResolver
    {
        if (is_null($this->basicRequest)) {
            throw new Exception('Basic request not set!');
        }

        $resolver = $this->basicStatsQuery($this->basicRequest)->resolve();

        return $this->resolveBasicStats($resolver);
    }

    private function basicStatsQuery(FilteringRequest $request): SearchBuilder
    {
        $searchBuilder = Product::searchBuilder()
            ->select([false])
            ->filter($request->getParameters(), ElasticSluggableFilter::class);

        if ($this->isShow(ProductFilter::ALIAS_KEY_FEATURE)) {
            $searchBuilder->aggr('featureValues', 'feature_value_ids');
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_BRAND)) {
            $searchBuilder->aggr('brands', 'brand_id');
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_CATEGORY)) {
            $searchBuilder->aggr('categories', 'category_ids');
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_PRICE)) {
            $searchBuilder
                ->aggr('minPrice', 'price', 'min')
                ->aggr('maxPrice', 'price', 'max');
        }

        return $searchBuilder;
    }

    private function resolveBasicStats(ElasticResponseResolver $resolver): FilterResponseResolver
    {
        $response = new FilterResponse();

        if ($minPrice = $resolver->getAggregationByName('minPrice')) {
            $response->setMinPrice($minPrice);
        }

        if ($maxPrice = $resolver->getAggregationByName('maxPrice')) {
            $response->setMaxPrice($maxPrice);
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_BRAND)) {
            $response->setBrandStats($resolver->getAggregationByName('brands'));
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_CATEGORY)) {
            $response->setCategoryStats($resolver->getAggregationByName('categories'));
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_FEATURE)) {
            $response->setFeatureValueStats($resolver->getAggregationByName('featureValues'));
        }

        return FilterResponseResolver::bySearchResult($response);
    }

    /**
     * @throws Exception
     */
    protected function fetchFilteredGroups()
    {
        if (is_null($this->filterRequest)) {
            throw new Exception('Filter request not set!');
        }

        return $this->filteredGroupQuery($this->filterRequest)
            ->paginateRaw($this->filterRequest->getPerPage());
    }

    /**
     * @throws Exception
     */
    protected function fetchRecalculatedStats()
    {
        if (is_null($this->recalculationRequests)) {
            throw new Exception('Recalculation requests not set!');
        }

        $collection = $this->fetchByQueries(
            $this->makeRecalculateQueries()
        );
        return $collection->toArray();
    }

}