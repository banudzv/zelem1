<?php

namespace App\Modules\Products\Services;

use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Requests\FilteringRequest;
use App\Modules\Products\Widgets\Site\ProductFilter;
use Exception;

class EloquentFilteringResolver extends AbstractProductFilterStateResolver
{
    /**
     * @throws Exception
     */
    protected function fetchBasicStats(): FilterResponseResolver
    {
        $request = $this->basicRequest;

        if (is_null($request)) {
            throw new Exception('Basic request not set!');
        }

        $response = new FilterResponse();

        if ($this->isShow(ProductFilter::ALIAS_KEY_PRICE)) {
            [$minPrice, $maxPrice] = $this->getProductSearchService()->getPriceStatsByRequest($request);

            if ($minPrice) {
                $response->setMinPrice($minPrice);
            }

            if ($maxPrice) {
                $response->setMaxPrice($maxPrice);
            }
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_BRAND)) {
            $response->setBrandStats(
                $this->getProductSearchService()->getBrandStatsByRequest($request)
            );
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_CATEGORY)) {
            $response->setCategoryStats(
                $this->getProductSearchService()->getCategoryStatsByRequest($request)
            );
        }

        if ($this->isShow(ProductFilter::ALIAS_KEY_FEATURE)) {
            $response->setFeatureValueStats(
                $this->getProductSearchService()->getFeatureValueStatsByRequest($request)
            );
        }

        $response->setMainFeatures(
            $this->getProductSearchService()->getMainFeatureStatsByRequest($request)
        );

        return FilterResponseResolver::bySearchResult($response);
    }

    protected function getProductSearchService(): EloquentProductSearchService
    {
        return resolve(EloquentProductSearchService::class);
    }

    /**
     * @throws Exception
     */
    protected function fetchFilteredGroups()
    {
        $filteringRequest = $this->filterRequest;

        if (!$filteringRequest) {
            throw new Exception('Filter request not set!');
        }

        return ProductGroup::getFilteredList($filteringRequest->getParameters(), $filteringRequest->getPerPage());
    }

    /**
     * @throws Exception
     */
    public function resolve(): ProductFilterStateResolverInterface
    {
        if ($this->basicRequest) {
            $this->basicStats = $this->fetchBasicStats();
        }

        if ($this->filterRequest) {
            $this->filteredGroups = $this->fetchFilteredGroups();
        }

        if ($this->recalculationRequests) {
            $this->recalculatedStats = $this->fetchRecalculatedStats();
        }

        return $this;
    }

    /**
     * @throws Exception
     */
    protected function fetchRecalculatedStats(): array
    {
        if (is_null($this->recalculationRequests)) {
            throw new Exception('Recalculation request not set!');
        }

        $result = [];

        foreach ($this->recalculationRequests as $request) {
            $result[$request->getKey()] = $this->getFilteredStats($request);
        }

        return $result;
    }

    private function getFilteredStats(FilteringRequest $request): iterable
    {
        if ($request->getKey() === ProductFilter::ALIAS_KEY_BRAND) {
            return $this->getProductSearchService()
                ->getBrandStatsByRequest($request);
        }

        if ($request->getKey() === ProductFilter::ALIAS_KEY_CATEGORY) {
            return $this->getProductSearchService()
                ->getCategoryStatsByRequest($request);
        }

        return $this->getProductSearchService()
            ->getFeatureValueStatsByRequest($request);
    }
}