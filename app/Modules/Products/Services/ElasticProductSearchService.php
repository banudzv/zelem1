<?php

namespace App\Modules\Products\Services;

use App\Modules\Categories\Models\Category;
use App\Modules\Products\Filters\ElasticProductFilter;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Requests\ProductSearchRequest;
use App\Modules\Products\Responses\ProductSearchResponses;
use App\Modules\Search\Resolvers\ElasticResponseResolver;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class ElasticProductSearchService implements ProductSearchServiceInterface
{

    public function searchByText(ProductSearchRequest $request): ProductSearchResponses
    {
        $resolver = $this->buildAndSearch($request);

        $products = $this->resolveProducts($resolver, $request);

        $categories = $this->resolveCategories($resolver);

        return new ProductSearchResponses($products, $categories);
    }

    private function buildAndSearch(ProductSearchRequest $request): ElasticResponseResolver
    {
        $builder = Product::searchBuilder()
            ->aggr('categories', 'category_ids')
            ->select(['id'])
            ->where('active', true)
            ->filter($request->getForFilter(), ElasticProductFilter::class)
            ->orderBy('relevance', 'desc')
            ->orderBy('_score', 'desc')
        ;

        if ($request->getIgnored()) {
            $builder->whereNotIn('id', $request->getIgnored());
        }

        if ($request->isHideModifications()) {
            $builder->collapse('group_id');
        }

        $builder->minScore(
            $this->calculateMinScore()
        );
//        $builder->setPerPage(20)
//            ->setPage(1);
//        dd($builder->explain()['hits']['hits'][1]);

        return $builder->resolvePaginate($request->getPerPage(), 'page', $request->getPage());
    }

    private function calculateMinScore(): float
    {
        return (float)config('db.search.min-cost');
    }

    private function resolveProducts(ElasticResponseResolver $resolver, ProductSearchRequest $request): Paginator
    {
        $ids = $resolver->getKeys();

        $products = Product::query()
            ->find($ids)
            ->sortBy(sort_models_by_ids_order($ids));

        return new Paginator(
            $products,
            $resolver->total(),
            $request->getPerPage(),
            $request->getPage()
        );
    }

    private function resolveCategories(ElasticResponseResolver $resolver): Collection
    {
        $categoryStats = $resolver->getAggregationByName('categories');

        $categoryIds = array_keys($categoryStats);

        $categories = Category::query()
            ->whereIn('id', $categoryIds)
            ->limit(config('db.search.category-limit', 5))
            ->orderBy('position')
            ->get()
            ->sortBy(sort_models_by_ids_order($categoryIds));

        foreach ($categories as $category) {
            $category->products_count = $categoryStats[$category->getKey()];
        }

        return $categories;
    }

}