<?php

namespace App\Modules\Products\Services;

use App\Modules\Products\Requests\ProductSearchRequest;
use App\Modules\Products\Responses\ProductSearchResponses;

interface ProductSearchServiceInterface
{
    /**
     * @param ProductSearchRequest $request
     * @return ProductSearchResponses
     */
    public function searchByText(ProductSearchRequest $request): ProductSearchResponses;

}