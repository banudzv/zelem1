<?php

namespace App\Modules\Products\Services;

use App\Modules\Products\Filters\EloquentProductSluggableFilter;
use App\Modules\Products\Filters\SearchGroupsSluggableFilter;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupFeatureValue;
use App\Modules\Products\Requests\FilteringRequest;
use App\Modules\Products\Requests\ProductSearchRequest;
use App\Modules\Products\Responses\ProductSearchResponses;
use App\Traits\WhereSlugsTrait;
use Closure;
use DB;
use Illuminate\Database\Eloquent\Builder;

class EloquentProductSearchService implements ProductSearchServiceInterface
{

    use WhereSlugsTrait;

    /**
     * @param ProductSearchRequest $request
     * @return ProductSearchResponses
     */
    public function searchByText(ProductSearchRequest $request): ProductSearchResponses
    {
        $builder = Product::query()
            ->filter($request->getRequest()->only(['query', 'order']))
            ->active()
            ->whereNotIn('id', $request->getIgnored());

        if ($request->isHideModifications()) {
            $builder->groupBy('products.group_id');
        }

        $products = $builder->paginate($request->getPerPage(), ['*'], 'page', $request->getPage());

        return new ProductSearchResponses(
            $products
        );
    }

    public function getMainFeatureStatsByRequest(FilteringRequest $filteringRequest): array
    {
        return ProductGroup::query()
            ->select(['feature_id'])
            ->filter($filteringRequest->getParameters(), EloquentProductSluggableFilter::class)
            ->active()
            ->groupBy('feature_id')
            ->get()
            ->pluck('products_count', 'brand_id')
            ->filter()
            ->toArray();
    }

    public function getBrandStatsByRequest(FilteringRequest $filteringRequest): array
    {
        return ProductGroup::query()
            ->select(['brand_id', DB::raw('COUNT(brand_id) as products_count')])
            ->filter($filteringRequest->getParameters(), EloquentProductSluggableFilter::class)
            ->active()
            ->groupBy('brand_id')
            ->get()
            ->pluck('products_count', 'brand_id')
            ->filter()
            ->toArray();
    }

    public function getFeatureValueStatsByRequest(FilteringRequest $filteringRequest): array
    {
        $builder = ProductGroupFeatureValue::query()
            ->select(['value_id', DB::raw('COUNT(DISTINCT group_id) as products_count')])
            ->filter($filteringRequest->getParameters(), SearchGroupsSluggableFilter::class);

        if ($filteringRequest->getKey()) {
            $builder->whereHas('featureTranslates', $this->whereSlugs([$filteringRequest->getKey()]));
        }

        return $builder
            ->whereHas('feature', $this->whereActiveInFilter())
            ->whereHas('value', $this->whereActive())
            ->groupBy('value_id')
            ->get()
            ->pluck('products_count', 'value_id')
            ->filter()
            ->toArray();
    }

    /**
     * @return Closure
     */
    private function whereActiveInFilter(): Closure
    {
        return function (Builder $builder) {
            $builder->where('active', true)
                ->where('in_filter', true);
        };
    }

    private function whereActive(): Closure
    {
        return function (Builder $builder) {
            $builder->where('active', true);
        };
    }

    public function getPriceStatsByRequest(FilteringRequest $filteringRequest): array
    {
        $product = $this->fetchPriceStatsByCategories($filteringRequest->getParameters());

        return [$product->minPrice, $product->maxPrice];
    }

    private function fetchPriceStatsByCategories(array $parameters): Product
    {
        return Product::query()
            ->select(DB::raw('max(price) as maxPrice'), DB::raw('min(price) as minPrice'))
            ->filter($parameters)
            ->active()
            ->first();
    }

    public function getCategoryStatsByRequest(FilteringRequest $filteringRequest)
    {
        return ProductGroup::query()
            ->select(['category_id', DB::raw('COUNT(category_id) as products_count')])
            ->filter($filteringRequest->getParameters(), EloquentProductSluggableFilter::class)
            ->active()
            ->groupBy('category_id')
            ->get()
            ->pluck('products_count', 'category_id')
            ->filter()
            ->toArray();
    }
}