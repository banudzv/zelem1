<?php

namespace App\Modules\Products\Services;

use Exception;
use Illuminate\Foundation\Application;

class FilteringResolvingFactory
{
    /**
     * @param Application $application
     * @return ElasticFilterStateResolver|EloquentFilteringResolver|mixed
     * @throws Exception
     */
    public function create(Application $application)
    {
        switch (config('products.search.driver')) {
            case 'db' :
                return $application->make(EloquentFilteringResolver::class);
            case 'elastic' :
                return $application->make(ElasticFilterStateResolver::class);
        }

        throw new Exception('The products search driver must be configured!');
    }
}