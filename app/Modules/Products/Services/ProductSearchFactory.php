<?php

namespace App\Modules\Products\Services;

use Exception;
use Illuminate\Foundation\Application;

class ProductSearchFactory
{
    /**
     * @param Application $application
     * @return ProductSearchServiceInterface
     * @throws Exception
     */
    public function create(Application $application): ProductSearchServiceInterface
    {
        switch (config('products.search.driver')) {
            case 'db' :
                return $application->make(EloquentProductSearchService::class);
            case 'elastic' :
                return $application->make(ElasticProductSearchService::class);
        }

        throw new Exception('The products search driver must be configured!');
    }
}