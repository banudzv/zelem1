<?php

namespace App\Modules\Products\Services;

use App\Modules\Brands\Models\Brand;
use App\Modules\Categories\Models\Category;
use App\Modules\Features\Models\FeatureValue;
use Cache;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class FilterResponseResolver
{

    /**
     * @var FilterResponse
     */
    private $response;

    /**
     * @var null|Collection|Brand[]
     */
    private $brands;

    /**
     * @var null|Collection|Brand[]
     */
    private $categories;

    /**
     * @var null|Collection|FeatureValue[]
     */
    private $featureValues;

    private $featureValuesStats = [];

    public static function bySearchResult(FilterResponse $response)
    {
        $self = new self();

        $self->response = $response;

        return $self;
    }

    public function getMinPrice(): ?float
    {
        return $this->response->getMinPrice();
    }

    public function getMaxPrice(): ?float
    {
        return $this->response->getMaxPrice();
    }

    /**
     * @return Collection|Brand[]
     */
    public function getBrands(): Collection
    {
        if (is_null($this->brands)) {
            $brandStats = $this->response->getBrandStats();
            $this->brands = Brand::query()
                ->whereIn('id', array_keys($brandStats))
                ->active()
                ->get();

            $this->brands->load('current');
        }

        return $this->brands;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        if (is_null($this->categories)) {
            $categoryStats = $this->response->getCategoryStats();

            $this->categories = Category::query()
                ->whereIn('id', array_keys($categoryStats))
//                ->whereTopLevel()
                ->active()
                ->get();

            $this->categories->load(['current']);
        }

        return $this->categories;
    }

    /**
     * @return Collection|FeatureValue[]
     */
    public function getFeatureValues(): Collection
    {
        if (is_null($this->featureValues)) {
            $featureValueStats = $this->response->getFeatureValueStats();

            $this->featureValues = $this->fetchFeatureValues($featureValueStats);

            foreach ($this->featureValues as $featureValue) {
                $this->featureValuesStats[$featureValue->feature_id][$featureValue->id] = $featureValueStats[$featureValue->id];
            }
        }

        return $this->featureValues;
    }

    protected function fetchFeatureValues(array $featureValueStats): Collection
    {
        return Cache::remember(
            'feature_values_' . implode(',', $featureValueStats),
            3600,
            function () use ($featureValueStats) {
                $featureValues = FeatureValue::query()
                    ->whereIn('id', array_keys($featureValueStats))
                    ->whereHas(
                        'feature',
                        function (Builder $builder) {
                            $builder->where('in_filter', true)
                                ->where('active', true);
                        }
                    )
                    ->active()
                    ->get();

                return $featureValues->load('current', 'feature.current');
            }
        );
    }

    public function getBrandStats(): array
    {
        return $this->response->getBrandStats();
    }

    public function getFeatureValuesStats(): array
    {
        return $this->featureValuesStats;
    }

    public function getCategoryStats(): array
    {
        return $this->response->getCategoryStats();
    }

    public function getMainFeatures(): array
    {
        return $this->response->getMainFeatures();
    }
}