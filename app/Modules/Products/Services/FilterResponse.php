<?php

namespace App\Modules\Products\Services;

class FilterResponse
{

    private $minPrice = 0;

    private $maxPrice = 0;

    private $brandStats = [];

    private $categoryStats = [];

    private $featureValueStats = [];

    private $mainFeatures = [];

    public function getBrandStats(): array
    {
        return $this->brandStats;
    }

    public function setBrandStats(iterable $brandStats)
    {
        $this->brandStats = $brandStats;

        return $this;
    }

    public function getFeatureValueStats(): array
    {
        return $this->featureValueStats;
    }

    public function setFeatureValueStats(iterable $featureValueStats)
    {
        $this->featureValueStats = $featureValueStats;

        return $this;
    }

    public function getMainFeatures(): array
    {
        return $this->mainFeatures;
    }

    public function setMainFeatures(array $mainFeatures): self
    {
        $this->mainFeatures = $mainFeatures;

        return $this;
    }

    public function getMinPrice(): float
    {
        return $this->minPrice;
    }

    public function setMinPrice(float $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    public function getMaxPrice(): float
    {
        return $this->maxPrice;
    }

    public function setMaxPrice(float $maxPrice): self
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    public function getCategoryStats(): array
    {
        return $this->categoryStats;
    }

    public function setCategoryStats(array $categoryStats): FilterResponse
    {
        $this->categoryStats = $categoryStats;

        return $this;
    }
}