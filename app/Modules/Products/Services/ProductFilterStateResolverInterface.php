<?php

namespace App\Modules\Products\Services;

use App\Modules\Products\Dto\ProductFilterConfig;
use App\Modules\Products\Requests\FilteringRequest;

interface ProductFilterStateResolverInterface
{
    public function setRequestForFilteringGroups(FilteringRequest $request): self;

    public function setRequestForBasicFilter(FilteringRequest $request): self;

    public function addRequestsForRecalculation(array $requests): self;

    public function resolve(): self;

    public function filteredGroups();

    public function basicStats(): FilterResponseResolver;

    public function recalculatedStats();

    public function setConfig(ProductFilterConfig $config): self;

    public function isShow(string $blockName): bool;
}