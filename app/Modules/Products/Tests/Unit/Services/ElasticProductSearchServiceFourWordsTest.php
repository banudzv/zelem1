<?php

namespace App\Modules\Products\Tests\Unit\Services;

class ElasticProductSearchServiceFourWordsTest extends AbstractElasticProductSearchServiceTest
{
    /**
     * @param $query
     * @param $total
     * @param $expected
     *
     * @dataProvider getConcreteResultFromElasticDataProvider
     */
    public function test_it_get_concrete_result_from_elastic_for_four_words_in_query($query, $total, $expected)
    {
        $this->searchTest($query, $total, $expected);
    }

    public function getConcreteResultFromElasticDataProvider()
    {
        return [
            'Марка, модель авто, Категория, Бренд 1' => [
                'Alfa-Romeo 156 Геометрия Jrone',
                1,
                [
                    'Геометрия турбины Jrone 3000-016-036',
                ],
            ],
            'Марка, модель авто, Категория, Бренд 2' => [
                'Alfa-Romeo 156 Геометрия Powertec',
                1,
                [
                    'Геометрия турбины Powertec  ZM.GT17',
                ],
            ],
            'Марка, модель авто, Категория, Бренд 3' => [
                'Alfa-Romeo 156 Геометрия TurboParts Литва',
                1,
                [
                    'Геометрия турбины TurboParts Литва GT17-90',
                ],
            ],
            'Марка, модель авто, Категория, Бренд в другой раскладке 3' => [
                'Alfa-Romeo 156 Геометрия Зщцукеус',
                1,
                [
                    'Геометрия турбины Powertec  ZM.GT17',
                ],
            ],
            'Марка, модель авто, Категория, Бренд в другой раскладке 4' => [
                'Геометрия Alfa-Romeo 156 Зщцукеус',
                1,
                [
                    'Геометрия турбины Powertec  ZM.GT17',
                ],
            ],
            'Номер турбины, авто Audi a4, Категория 1' => [
                '53039700133 Audi a4 Картридж',
                3,
                [
                    'Картридж турбин TurboParts Литва BV43-214',
                    'Картридж турбины Powertec ZM.BV43-21',
                    'Картридж турбины Jrone 1000-030-197T',
                ]
            ],
            'Номер турбины, авто Audi a4, Категория 2' => [
                '53039700133 Audi a4 Геометрия',
                4,
                [
                    'Геометрия турбины Jrone 3000-016-056',
                    'Геометрия турбины TurboParts Литва BV43-90',
                    'Геометрия турбины TurboParts Литва BV43-91',
                    'Геометрия турбины Powertec  ZM.BV43-9',
                ]
            ],
            'Номер турбины, марка авто Audi, Бренд, Категория 1' => [
                '53039700133 Audi a4 Геометрия Jrone',
                1,
                [
                    'Геометрия турбины Jrone 3000-016-056',
                ]
            ],
            'Номер турбины, марка авто Audi, Бренд, Категория 2' => [
                '53039700133 Audi a4 Геометрия TurboParts Литва',
                2,
                [
                    'Геометрия турбины TurboParts Литва BV43-90',
                    'Геометрия турбины TurboParts Литва BV43-91',
                ]
            ],
            'Полный текст названия товара 1' => [
                'Картридж турбины Powertec ZM.TD04L',
                1,
                [
                    'Картридж турбины Powertec ZM.TD04L',
                ]
            ],
            'Полный текст названия товара 2' => [
                'Картридж турбины Powertec ZM.TD04L',
                1,
                [
                    'Картридж турбины Powertec ZM.TD04L',
                ]
            ],
        ];
    }
}