<?php

namespace App\Modules\Products\Tests\Unit\Services;

class ElasticProductSearchServiceOneWordTest extends AbstractElasticProductSearchServiceTest
{

    /**
     * @param $query
     * @param $total
     * @param $expected
     *
     * @dataProvider getConcreteResultFromElasticDataProvider
     */
    public function test_it_get_concrete_result_from_elastic_for_one_word_in_query($query, $total, $expected)
    {
        $this->searchTest($query, $total, $expected);
    }

    public function getConcreteResultFromElasticDataProvider()
    {
        return [
            'Номер турбины цифрами 1' => ['53039700133', 15, [],],
            'Номер турбины цифрами с тире 2' => [
                '5303-988-0073',
                3,
                [
                    'Картридж турбин TurboParts Литва K03-211',
                    'Картридж турбин THM Польша chra98',
                    'Картридж турбины Powertec ZM.K03-211',
                ],
            ],
            'Номер турбины цифрами с тире 3' => [
                '3000-016-064',
                1,
                [
                    'Геометрия турбины Jrone 3000-016-064',
                ]
            ],
            'Номер турбины цифрами с тире 4' => [
                '1000-030-191',
                4,
                [
                    'Картридж турбин TurboParts Литва BV43-210',
                    'Картридж турбин THM Польша chra495',
                    'Картридж турбины Powertec ZM.BV43',
                    'Картридж турбины Jrone 1000-030-191',
                ]
            ],
            'Номер турбины цифрами с тире 5' => [
                '1000-010-100',
                2,
                [
                    'Картридж турбины Powertec ZM.GT1238S',
                    'Картридж турбины Jrone 1000-010-100',
                ]
            ],
            'Номер турбины цифрами с тире и точкой 1' => [
                'ZM.TD04-1',
                1,
                [
                    'Геометрия турбины Powertec  ZM.TD04-1',
                ]
            ],
            'Номер турбины без точек и тире 1' => [
                'ZMTD041',
                1,
                [
                    'Геометрия турбины Powertec  ZM.TD04-1',
                ]
            ],
            'Номер турбины с точкой 1' => [
                'ZM.TD04',
                3,
                [
                    'Картридж турбины Powertec ZM.TD04L',
                    'Геометрия турбины Powertec  ZM.TD04',
                    'Геометрия турбины Powertec  ZM.TD04-1',
                ]
            ],
            'Номер турбины без точек 1' => [
                'ZMTD04',
                3,
                [
                    'Картридж турбины Powertec ZM.TD04L',
                    'Геометрия турбины Powertec  ZM.TD04',
                    'Геометрия турбины Powertec  ZM.TD04-1',
                ]
            ],
            'Номер ОЕМ' => [
                '058145703L',
                2,
                [
                    'Картридж турбины TurboParts Литва K03-216',
                    'Картридж турбины Powertec ZM.K03',
                ],
            ],
            'MSG - все буквы в верхнем регистре' => ['MSG', 25, [],],
            'msg - все буквы в нижнем регистре' => ['msg', 25, [],],
            'Jrone' => ['Jrone', 1071, [],],
            'Jrone - все буквы в нижнем регистре' => ['jrone', 1071, []],
            'Jrone - все буквы в верхнем регистре' => ['JRONE', 1071, []],
            'Audi' => ['Audi', 202, []],
            'Audi - в нижнем регистре' => ['audi', 202, []],
            'Артикул' => [
                'BV45-211',
                2,
                [
                    'Картридж турбин TurboParts Литва BV45-211',
                    'Картридж турбины Jrone 1000-030-284T',
                ]
            ],
            'Артикул с аналогом' => [
                '1000-030-197T',
                3,
                [
                    'Картридж турбин TurboParts Литва BV43-214',
                    'Картридж турбины Powertec ZM.BV43-21',
                    'Картридж турбины Jrone 1000-030-197T',
                ]
            ],
            'Номер двигателя' => [
                'AJQ',
                7,
                [
                    'Картридж турбин TurboParts Литва K03-211',
                    'Картридж турбин TurboParts Литва K03-213',
                    'Картридж турбин THM Польша chra98',
                    'Картридж турбины Powertec ZM.K03-213',
                    'Картридж турбины Powertec ZM.K03-211',
                    'Картридж турбины Jrone 1000-030-003',
                    'Картридж турбины Jrone 1000-030-004',
                ]
            ],
            'Бренд 1' => ['Jrone', 1071, []],
            'Бренд 1 в другой раскладке' => ['Окщту', 1071, []],
            'Бренд 2' => ['Powertec', 1170, []],
            'Бренд 2 в другой раскладке' => ['Зщцукеус', 1170, []],
        ];
    }

}