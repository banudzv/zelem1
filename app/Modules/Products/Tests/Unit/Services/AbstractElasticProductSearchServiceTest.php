<?php

namespace App\Modules\Products\Tests\Unit\Services;

use App\Modules\Products\Models\Product;
use App\Modules\Products\Requests\ProductSearchRequest;
use App\Modules\Products\Services\ElasticProductSearchService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

abstract class AbstractElasticProductSearchServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var ElasticProductSearchService|mixed
     */
    protected $service;


    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(ElasticProductSearchService::class);
    }


    protected function getRequest(): Request
    {
        return request();
    }

    protected function searchTest($query, $total, $expected)
    {
        $searchRequest = ProductSearchRequest::byRequest(
            $this->getRequest()->merge(['query' => $query, 'per_page' => 20])
        );

        $response = $this
            ->service
            ->searchByText($searchRequest);

        $products = $response->getProducts();
//        dd($this->mapProductsToArrayOfNames($products));

        $this->assertEquals($total, $products->total());

        if ($expected) {
            $this->assertEquals(
                $expected,
                $this->mapProductsToArrayOfNames($products)
            );
        }
    }

    /**
     * @param Collection|LengthAwarePaginator $products
     * @return array
     */
    protected function mapProductsToArrayOfNames($products)
    {
        return $products->map(
            function (Product $product) {
                return $product->getName();
            }
        )
            ->toArray();
    }

}