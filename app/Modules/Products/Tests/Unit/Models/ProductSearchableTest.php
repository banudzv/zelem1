<?php

namespace App\Modules\Products\Tests\Unit\Models;

use App\Modules\Products\Models\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProductSearchableTest extends TestCase
{
    use DatabaseTransactions;

    public function test_it_has_category_in_searchable_array()
    {
        self::markTestSkipped();
        $product = Product::find(303);

        $array = $product->toSearchableArray();

        $this->assertCount(1, $product->group->otherCategories);

        $this->assertCount(1, $array['category_slugs']);
    }
}