<?php

namespace App\Modules\Products\Components;

use App\Components\Catalog\Interfaces\EcommerceInterface;
use App\Modules\Orders\Components\Cart\Cart;
use App\Modules\Orders\Models\Order;
use Catalog;
use Illuminate\Database\Eloquent\Collection;

use function DI\string;

/**
 * Class Ecommerce
 *
 * @package App\Modules\Products\Facades
 * @method static getFacadeRoot()
 */
class Ecommerce implements EcommerceInterface
{
    const PAGE_TYPE = 'ecomm_pagetype';
    const DEFAULT_PAGE_TYPE = 'other';
    const PROD_ID = 'ecomm_prodid';
    const TOTAL_VALUE = 'ecomm_totalvalue';
    const FORM_DATA_LAYERS = [
        'contact' => [
            'event' => 'gaTriggerEvent',
            'gaEventCategory' => 'feedback',
            'gaEventAction' => 'send'
        ],
        'fast-order' => [
            'event' => 'gaTriggerEvent',
            'gaEventCategory' => '1_click_purchase',
            'gaEventAction' => 'send'
        ],
        'login' => [
            'event' => 'gaTriggerEvent',
            'gaEventCategory' => 'Cabinet',
            'gaEventAction' => 'send'
        ]
    ];

    private $object = [];
    private $pageType;
    private $event;
    private $actionField;
    private $products;
    private $product;
    private $order;
    private $cart;

    /**
     * @return mixed
     */
    public function getObject()
    {
        $this->prepareData();
        return count($this->object) ? json_encode($this->object, JSON_UNESCAPED_UNICODE) : null;
    }

    public function setPage(string $type, ?string $event = null, ?array $actionField = null)
    {
        $this->pageType = $type;
        $this->event = $event;
        $this->actionField = $actionField;
    }

    public function addProducts(Collection $products, string $list = 'catalog')
    {
        if (!$this->products) {
            $this->products = new Collection();
        }
        if (!$this->products->has($list)) {
            $this->products->put($list, new Collection());
        }

        $this->products->put($list, $this->products->get($list)->concat($products));
    }

    public function setProduct(Ecommercable $product)
    {
        $this->product = $product;
    }

    protected function prepareData()
    {
        $this->setField(static::PAGE_TYPE, $this->pageType ?? static::DEFAULT_PAGE_TYPE);
        if($this->event) {
            $this->setField('event', $this->event);
        }
        if ($this->product) {
            $this->setCommonFields($this->product);
        } elseif ($this->products) {
            $products = $this->products->flatten();
            $products->each(function (Ecommercable $product) {
                $this->setCommonFields($product);
            });
        }

        $this->checkCommonFields();

        if (($method = $this->event) && method_exists($this, $this->event)) {
            $this->$method();
        }
    }

    protected function productImpressions(): void
    {
        if (!$this->products) {
            return;
        }
        $impressions = [];
        $this->products->each(function (Collection $item, $key) use (&$impressions) {
            $item->each(function (Ecommercable $product, $index) use (&$impressions, $key) {
                $impressions[] = [
                    'id' => $product->ecommId(),
                    'name' => $product->ecommName(),
                    'price' => $product->ecommPrice(),
                    'brand' => $product->ecommBrand(),
                    'category' => $product->ecommCategory(),
                    'list' => $key,
                    'position' => $index
                ];
            });
        });

        $this->addToEcommerce([
            'currencyCode' => Catalog::currency()->microdataName(),
            'impressions' => $impressions
        ]);
    }

    public function productDetail()
    {
        $this->productImpressions();
        if ($this->product) {
            $this->addToEcommerce([
                'currencyCode' => Catalog::currency()->microdataName(),
                'detail' => [
                    'products' => [
                        [
                            'id' => $this->product->ecommId(),
                            'name' => $this->product->ecommName(),
                            'price' => $this->product->ecommPrice(),
                            'brand' => $this->product->ecommBrand(),
                            'category' => $this->product->ecommCategory()
                        ]
                    ]
                ]
            ]);
        }
    }

    protected function transaction()
    {
        $products = [];
        if ($this->order) {
            $collection = $this->order->items;
            $total = $this->order->total_amount_as_number;

            $collection->loadMissing(
                'product.current',
                'product.brand.current',
                'product.category.current'
            );
            $productsIds = $collection->map(function ($item, $index) use (&$products) {
                $products[] = [
                    'id' => $item->product->ecommId(),
                    'name' => $item->product->ecommName(),
                    'price' => $item->product->ecommPrice(),
                    'brand' => $item->product->ecommBrand(),
                    'category' => $item->product->ecommCategory(),
                    'position' => $index,
                    'quantity' => $item->quantity
                ];
                return (string) $item->product->id;
            })->toArray();
        }

        if (count($products)) {
            $this->setField(static::PROD_ID, $productsIds);
            $this->setField(static::TOTAL_VALUE, (int) number_format($total, config('db.currencies.number-symbols', 2), '.', ''));

            $ecommerce = [
                'purchase' => [
                    'products' => $products,
                    'actionField' => $this->getActionField()
                ]
            ];

            $this->addToEcommerce($ecommerce);
        }
    }

    protected function getActionField()
    {
        switch ($this->event) {
            case 'transaction':
                return $this->order ? $this->order->transaction_action_field : $this->actionField;
                break;
            default:
                return $this->actionField;
                break;
        }
    }

    protected function checkout()
    {
        $products = [];
        if ($this->cart) {
            $collection = $this->cart->getModel()->items;
            list($total) = $this->cart->getTotalAmountOfTheCart($this->cart->getServices());
        } elseif ($this->order) {
            $collection = $this->order->items;
            $total = $this->order->total_amount_as_number;
        }

        if ($collection) {
            $collection->loadMissing(
                'product.current',
                'product.brand.current',
                'product.category.current'
            );
            $productsIds = $collection->map(function ($item, $index) use (&$products) {
                $products[] = [
                    'id' => $item->product->ecommId(),
                    'name' => $item->product->ecommName(),
                    'price' => $item->product->ecommPrice(),
                    'brand' => $item->product->ecommBrand(),
                    'category' => $item->product->ecommCategory(),
                    'position' => $index,
                    'quantity' => $item->quantity
                ];
                return (string) $item->product->id;
            })->toArray();
        }

        if (count($products)) {
            $this->setField(static::PROD_ID, $productsIds);
            $this->setField(static::TOTAL_VALUE, (int) number_format($total, config('db.currencies.number-symbols', 2), '.', ''));

            $ecommerce = [
                'checkout' => [
                    'products' => $products,
                    'actionField' => $this->getActionField()
                ]
            ];

            $this->addToEcommerce($ecommerce);
        }
    }

    protected function addToEcommerce(array $data)
    {
        if (!isset($this->object['ecommerce'])) {
            $this->object['ecommerce'] = [];
        }
        $this->object['ecommerce'] = array_merge($this->object['ecommerce'], $data);
    }

    public function setCommonFields(Ecommercable $product)
    {
        $this->pushField(static::PROD_ID, $product->ecommId());
        $this->pushField(static::TOTAL_VALUE, $product->ecommPrice());
    }

    protected function checkCommonFields()
    {
        foreach ([static::PROD_ID, static::TOTAL_VALUE] as $field) {
            if (!isset($this->object[$field])) {
                $this->setField($field, '');
            }
        }
    }

    public function pushField(string $key, $value)
    {
        if (!isset($this->object[$key])) {
            $this->setField($key, []);
        }

        $this->object[$key][] = $value;
    }

    public function setField(string $key, $value)
    {
        $this->object[$key] = $value;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    public function setCart(Cart $cart)
    {
        $this->cart = $cart;
    }
    
    public function getFormDataLayer(string $key)
    {
        return data_get(static::FORM_DATA_LAYERS, $key);
    }
}
