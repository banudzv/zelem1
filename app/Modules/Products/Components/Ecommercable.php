<?php


namespace App\Modules\Products\Components;


interface Ecommercable
{
    public function ecommId(): string;

    public function ecommName(): string;

    public function ecommPrice(): float;

    public function ecommBrand(): ?string;

    public function ecommCategory(): string;
}