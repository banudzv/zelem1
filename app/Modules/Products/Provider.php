<?php

namespace App\Modules\Products;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\Modules\Languages\Models\Language;
use App\Core\ObjectValues\RouteObjectValue;
use App\Exceptions\WrongParametersException;
use App\Modules\Products\Components\Ecommerce;
use App\Modules\Products\Models\Product;
use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductTranslates;
use App\Modules\Products\Services\FilteringResolvingFactory;
use App\Modules\Products\Services\ProductFilterStateResolverInterface;
use App\Modules\Products\Services\ProductSearchFactory;
use App\Modules\Products\Services\ProductSearchServiceInterface;
use App\Modules\Products\Widgets\GroupSortable;
use App\Modules\Products\Widgets\ProductAmount;
use App\Modules\Products\Widgets\ProductAmountOld;
use App\Modules\Products\Widgets\ProductGroupLiveSearchSelect;
use App\Modules\Products\Widgets\ProductInvoice;
use App\Modules\Products\Widgets\ProductLiveSearch;
use App\Modules\Products\Widgets\ProductOrderMail;
use App\Modules\Products\Widgets\Site\CarFilter;
use App\Modules\Products\Widgets\Site\CompareProductCard;
use App\Modules\Products\Widgets\Site\CompareProductsList;
use App\Modules\Products\Widgets\Site\CompareProductsListAll;
use App\Modules\Products\Widgets\Site\Ecommerce as EcommerceWidget;
use App\Modules\Products\Widgets\Site\MicroDataSearchBar;
use App\Modules\Products\Widgets\Site\MobileButton;
use App\Modules\Products\Widgets\Site\ProductBrandGroupList;
use App\Modules\Products\Widgets\Site\ProductCard;
use App\Modules\Products\Widgets\Site\ProductCartItem;
use App\Modules\Products\Widgets\Site\ProductControls;
use App\Modules\Products\Widgets\Site\ProductDescription;
use App\Modules\Products\Widgets\Site\ProductFilter;
use App\Modules\Products\Widgets\Site\ProductFilterChosen;
use App\Modules\Products\Widgets\Site\ProductFilterPrice;
use App\Modules\Products\Widgets\Site\ProductGroupCard;
use App\Modules\Products\Widgets\Site\ProductGroupDescription;
use App\Modules\Products\Widgets\Site\ProductGroupList;
use App\Modules\Products\Widgets\Site\ProductGroupSlider;
use App\Modules\Products\Widgets\Site\ProductImage;
use App\Modules\Products\Widgets\Site\ProductInCheckout;
use App\Modules\Products\Widgets\Site\ProductOrder;
use App\Modules\Products\Widgets\Site\ProductPage\ProductTabDescription;
use App\Modules\Products\Widgets\Site\ProductPage\ProductTabDocuments;
use App\Modules\Products\Widgets\Site\ProductPage\ProductTabFeatures;
use App\Modules\Products\Widgets\Site\ProductPage\ProductTabMain;
use App\Modules\Products\Widgets\Site\ProductPage\ProductTabReviews;
use App\Modules\Products\Widgets\Site\ProductPrice;
use App\Modules\Products\Widgets\Site\ProductPrintOrder;
use App\Modules\Products\Widgets\Site\ProductsList;
use App\Modules\Products\Widgets\Site\ProductsListBrandPage;
use App\Modules\Products\Widgets\Site\ProductsListCategoryPage;
use App\Modules\Products\Widgets\Site\ProductsRelated;
use App\Modules\Products\Widgets\Site\ProductsSimilar;
use App\Modules\Products\Widgets\Site\ProductsSlider;
use App\Modules\Products\Widgets\Site\ProductsViewedPage;
use App\Modules\Products\Widgets\Site\ProductsWishlistPage;
use App\Modules\Products\Widgets\Site\SearchBar;
use App\Modules\Products\Widgets\Site\SearchBarMobile;
use App\Modules\Products\Widgets\Site\SortProductsBar;
use Catalog;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\Select;
use CustomForm\TinyMce;
use CustomMenu;
use CustomRoles;
use CustomSettings;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Application;
use Illuminate\Validation\Rule;
use Widget;

class Provider extends BaseProvider
{

    public function initSitemapXml(): array
    {
        $languages = config('languages', []);
        $default_language = null;
        /** @var Language $language */
        foreach ($languages as $language) {
            if ($language->default) {
                $default_language = $language->slug;
            }
        }

        $items = [];
        ProductTranslates::whereHas(
            'row',
            function (Builder $builder) {
                $builder->where('active', true);
            }
        )->get()->each(
            function (ProductTranslates $page) use (&$items, &$default_language) {
                $prefix = ($default_language === $page->language) ? '' : '/' . $page->language;
                $items[] = [
                    'url' => url($prefix . route('site.product', ['slug' => $page->slug], false), [], isSecure()),
                ];
            }
        );

        return $items;
    }

    protected function presets()
    {
        $this->app->singleton(
            ProductSearchServiceInterface::class,
            function (Application $application) {
                return $application->make(ProductSearchFactory::class)
                    ->create($application);
            }
        );

        $this->app->bind(
            ProductFilterStateResolverInterface::class,
            function (Application $application) {
                return $application->make(FilteringResolvingFactory::class)
                    ->create($application);
            }
        );

        Catalog::loadEcommerce(new Ecommerce());
    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register role scopes
        CustomRoles::add('products', 'products::general.permission-name')
            ->except(RoleRule::VIEW)
            ->addCustomPolicy('groups', RoleRule::UPDATE)
            ->addCustomPolicy('deleteImage', RoleRule::UPDATE)
            ->addCustomPolicy('setAsMain', RoleRule::UPDATE)
            ->addCustomPolicy('linkFeatureValueToGroup', RoleRule::UPDATE)
            ->addCustomPolicy('changeFeature', RoleRule::UPDATE)
            ->addCustomPolicy('changeFeatureConfirmation', RoleRule::UPDATE)
            ->addCustomPolicy('cloneProduct', RoleRule::STORE)
            ->addCustomPolicy('linkFeatureValue', RoleRule::UPDATE);
        CustomRoles::linkGroup('products', 'groups');

        $group = CustomMenu::get()->group();
        $group
            ->block('catalog')
            ->link('products::menu.groups', RouteObjectValue::make('admin.groups.index'))
            ->setPosition(1)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.groups.create'),
                RouteObjectValue::make('admin.groups.change-feature'),
                RouteObjectValue::make('admin.groups.edit')
            )
            ->setPermissionScope('products.index');
        $group
            ->block('catalog', 'products::menu.block', 'fa fa-cubes')
            ->setPosition(2)
            ->link('products::menu.products', RouteObjectValue::make('admin.products.index'))
            ->setPosition(2)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.products.create'),
                RouteObjectValue::make('admin.products.edit')
            );
        $group
            ->block('catalog')
            ->link('products::menu.services', RouteObjectValue::make('admin.services.index'))
            ->setPosition(1)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.services.create'),
                RouteObjectValue::make('admin.services.edit')
            )
            ->setPermissionScope('products.index');

        // Register module configurable settings
        $settings = CustomSettings::createAndGet('microdata', 'products::settings.microdata');
        $settings->add(
            Toggle::create('search')
                ->setLabel('products::settings.attributes.microdata-search')
                ->setDefaultValue(true)
                ->required(),
            ['required', 'boolean']
        );
        $settings->add(
            Toggle::create('product')
                ->setLabel('products::settings.attributes.microdata-product')
                ->setDefaultValue(true)
                ->required(),
            ['required', 'boolean']
        );

        $settings = CustomSettings::createAndGet('products', 'products::settings.group-name');
        $settings->add(
            Toggle::create('hide-products-mods-in-search')
                ->setLabel('products::settings.attributes.hide-products-mods-in-search')
                ->setDefaultValue(false)
                ->required(),
            ['required', 'boolean']
        );
        $settings->add(
            Toggle::create('show-filter-by-price')
                ->setLabel('products::settings.attributes.show-filter-by-price')
                ->setDefaultValue(true)
                ->required(),
            ['required', 'boolean']
        );
        $settings->add(
            Toggle::create('show-availability')
                ->setLabel('products::settings.attributes.show-availability')
                ->setDefaultValue(true)
                ->required(),
            ['required', 'boolean']
        );
        $settings->add(
            Toggle::create('filter-counters')
                ->setLabel('products::settings.attributes.filter-counters')
                ->setDefaultValue(true)
                ->required(),
            ['required', 'boolean']
        );
        $settings->add(
            Input::create('per-page')
                ->setType('number')
                ->setLabel('products::settings.attributes.per-page')
                ->setDefaultValue(ProductGroup::LIMIT_PER_PAGE_BY_DEFAULT_ADMIN_PANEL)
                ->required(),
            ['required', 'integer', 'min:1', 'max:100']
        );
        $settings->add(
            Input::create('site-per-page')
                ->setType('number')
                ->setLabel('products::settings.attributes.site-per-page')
                ->setDefaultValue(Product::LIMIT_PER_PAGE_BY_DEFAULT)
                ->required(),
            ['required', 'integer', 'min:1', 'max:100']
        );
        $settings->add(
            Select::create('count-of-products-in-the-row')
                ->add(
                    [
                        Product::THREE_IN_THE_ROW => 'products::settings.attributes.three-in-the-row',
                        Product::FOUR_IN_THE_ROW => 'products::settings.attributes.four-in-the-row',
                    ]
                )
                ->setLabel('products::settings.attributes.count-of-products-in-the-row')
                ->setDefaultValue(Product::FOUR_IN_THE_ROW)
                ->required(),
            ['required', Rule::in([Product::FOUR_IN_THE_ROW, Product::THREE_IN_THE_ROW])]
        );
        $settings->add(
            Toggle::create('show-categories-if-has')
                ->setLabel('products::settings.attributes.show-categories-if-has')
                ->setDefaultValue(false)
                ->required(),
            ['required', 'boolean']
        );
        $settings->add(
            Toggle::create('show-brand-in-item-card')
                ->setLabel('products::settings.attributes.show-brand-in-item-card')
                ->setDefaultValue(true)
                ->required(),
            ['required', 'boolean']
        );
        foreach (config('languages') as $language) {
            $settings->add(
                Input::create('sizes_title_' . $language->slug)
                    ->setLabel('products::settings.attributes.multi-lang-sizes_title')
                    ->setAdditionalLabel(['language' => $language->name]),
                [],
                5001
            );
            $settings->add(
                TinyMce::create('sizes_text_' . $language->slug)
                    ->setLabel('products::settings.attributes.multi-lang-sizes_text')
                    ->setAdditionalLabel(['language' => $language->name]),
                [],
                5002
            );
        }

        Widget::register(ProductInvoice::class, 'products::in-invoice');
        Widget::register(ProductGroupLiveSearchSelect::class, 'products::groups::live-search');
        Widget::register(ProductLiveSearch::class, 'products::live-search');
        Widget::register(GroupSortable::class, 'products::groups::sortable');
    }

    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
        Widget::register(ProductsList::class, 'products::list');
        Widget::register(ProductGroupList::class, 'products::groups-list');
        Widget::register(ProductBrandGroupList::class, 'products::brand-groups-list');
        Widget::register(ProductCard::class, 'products::card');
        Widget::register(ProductGroupCard::class, 'products::group-card');
        Widget::register(ProductControls::class, 'products::controls');
        Widget::register(ProductPrice::class, 'products::price');
        Widget::register(ProductsListCategoryPage::class, 'products::category-page');
        Widget::register(ProductsListBrandPage::class, 'products::brand-page');
        Widget::register(ProductsViewedPage::class, 'products::viewed-page');
        Widget::register(ProductFilter::class, 'products::filter');
        Widget::register(CarFilter::class, 'products::car-filter');
        if ((bool)config('db.products.show-filter-by-price', true) === true) {
            Widget::register(ProductFilterPrice::class, 'products::filter-price');
        }
        Widget::register(ProductTabMain::class, 'products::tab-main');
        Widget::register(ProductTabDescription::class, 'products::tab-description');
        Widget::register(ProductTabFeatures::class, 'products::tab-features');
        Widget::register(ProductTabReviews::class, 'products::tab-reviews');
        Widget::register(ProductTabDocuments::class, 'products::tab-documents');
        Widget::register(ProductsWishlistPage::class, 'products::wishlist-page');
        Widget::register(SearchBar::class, 'products::search-bar');
        Widget::register(SearchBarMobile::class, 'products::search-bar-mobile');
        Widget::register(SortProductsBar::class, 'products::sort-bar');
        Widget::register(ProductFilterChosen::class, 'products::chosen-parameters-in-filter');
        Widget::register(ProductAmount::class, 'products::amount');
        Widget::register(ProductAmountOld::class, 'products::amount-old');
        Widget::register(ProductCartItem::class, 'products::cart-item');
        Widget::register(ProductInCheckout::class, 'products::checkout');
        Widget::register(ProductImage::class, 'products::image');
        Widget::register(ProductOrder::class, 'products::in-order');
        Widget::register(ProductPrintOrder::class, 'products::print-order');
        Widget::register(MobileButton::class, 'mobile-top-search-button');
        Widget::register(CompareProductsListAll::class, 'products::compare-page-all');
        Widget::register(CompareProductsList::class, 'products::compare-page');
        Widget::register(CompareProductCard::class, 'products::compare-card');
        Widget::register(ProductOrderMail::class, 'products::order-mail-item');
        Widget::register(ProductsSlider::class, 'products::slider');
        Widget::register(ProductGroupSlider::class, 'products::groups-slider');
        Widget::register(ProductDescription::class, 'products::description');
        Widget::register(ProductGroupDescription::class, 'products::group-description');
        Widget::register(ProductsSimilar::class, 'products::similar-products');
        Widget::register(ProductsRelated::class, 'products::related-products');
        Widget::register(MicroDataSearchBar::class, 'products::microdata-search');
        Widget::register(EcommerceWidget::class, 'ecommerce');
        Widget::register(ProductComments::class, 'comments');

        view()->share('searchBarMicrodataWidgetName', 'products::microdata-search');
    }
}
