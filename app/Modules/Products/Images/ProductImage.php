<?php

namespace App\Modules\Products\Images;

use App\Components\Image\ImagesGroup;
use App\Core\Abstractions\ImageContainer;

/**
 * Class ArticlesImage
 *
 * @package App\Modules\Articles\Images
 */
class ProductImage extends ImageContainer
{
    
    /**
     * Field name in the form
     *
     * @return string
     */
    public static function getField(): string
    {
        return 'image';
    }
    
    /**
     * Folder name
     *
     * @return string
     */
    public static function getType(): string
    {
        return 'products';
    }
    
    /**
     * Configurations
     *
     * @return ImagesGroup
     * @throws \App\Exceptions\WrongParametersException
     */
    public function configurations(): ImagesGroup
    {
        $image = new ImagesGroup($this->getType());
        $image
            ->addTo('xs')
            ->setWidth(250)
            ->setHeight(250)
            ->setCrop(false);
        $image
            ->addTo('small')
            ->setWidth(350)
            ->setHeight(350)
            ->setCrop(false);
        $image
            ->addTo('medium')
            ->setWatermark(true)
            ->setWidth(680)
            ->setHeight(500);
        $image
            ->addTo('big')
            ->setWatermark(true)
            ->setWidth(1900)
            ->setHeight(1900);
        return $image;
    }
    
}
