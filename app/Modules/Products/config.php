<?php

use App\Modules\Products\Models\Product;

return [
    'search' => [
        /*
         * possible options: db, elastic
         */
        'driver' => env('PRODUCT_SEARCH_DRIVER', 'db'),
    ],
    'availability' => [
        Product::NOT_AVAILABLE => 'products::general.not-available',
        Product::CUSTOM_GOODS => 'products::general.custom-goods',
        Product::AVAILABLE => 'products::general.available',
    ],

    'available-order-fields' => [
        '' => 'products::site.order.default',
        'price-asc' => 'products::site.order.price-asc',
        'price-desc' => 'products::site.order.price-desc',
        'name-asc' => 'products::site.order.name-asc',
        'name-desc' => 'products::site.order.name-desc',
    ],
    'excluded_url_params' => [
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_term',
        'utm_content',
        'gclid',
        'fbclid',
        'yclid',
        'ref',
        '_openstat',
    ]
];
