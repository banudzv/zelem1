<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRelevanceFieldIntoProductsTable extends Migration
{
    public function up()
    {
        Schema::table(
            'products',
            function (Blueprint $table) {
                $table->integer('relevance')->default(0);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'products',
            function (Blueprint $table) {
                $table->dropColumn('relevance');
            }
        );
    }
}
