<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalesField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_groups', function (Blueprint $table) {
            $table->integer('sale_id')->unsigned()->nullable();

            $table->foreign('sale_id')->on('sales')->references('id')
                ->index('products_groups_sale_id_sales_id')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (app()->environment() !== 'production') {
            Schema::table('products_groups', function (Blueprint $table) {
                $table->dropForeign('products_groups_sale_id_sales_id');
            });
        }
    }
}
