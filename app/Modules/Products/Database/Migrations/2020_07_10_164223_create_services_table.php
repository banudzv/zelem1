<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('active')->default(false);
        });

        Schema::create('services_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('teaser')->nullable();
            $table->text('text')->nullable();
            $table->string('h1')->nullable();
            $table->string('title')->nullable();
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);

            $table->foreign('language')->references('slug')->on('languages')
                ->index('services_translates_language_languages_slug')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('services')
                ->index('services_translates_row_id_services_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('service_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->unsigned();
            $table->decimal('price', 12, 2)->default(0);
            $table->integer('position')->default(0);

            $table->foreign('service_id')->references('id')->on('services')
                ->index('service_prices_service_id_services_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('service_prices_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);

            $table->foreign('language')->references('slug')->on('languages')
                ->index('spt_language_languages_slug')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('service_prices')
                ->index('spt_row_id_services_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_prices_translates', function (Blueprint $table) {
            $table->dropForeign('spt_language_languages_slug');
            $table->dropForeign('spt_row_id_services_id');
        });
        Schema::table('service_prices', function (Blueprint $table) {
            $table->dropForeign('service_prices_service_id_services_id');
        });
        Schema::table('services_translates', function (Blueprint $table) {
            $table->dropForeign('services_translates_language_languages_slug');
            $table->dropForeign('services_translates_row_id_services_id');
        });
        Schema::dropIfExists('service_prices');
        Schema::dropIfExists('services_translates');
        Schema::dropIfExists('services');
    }
}
