<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryServicesSyncTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->on('categories')->references('id')
                ->index('category_services_category_id_categories_id')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->integer('service_id')->unsigned();
            $table->foreign('service_id')->on('services')->references('id')
                ->index('category_services_service_id_services_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_groups', function (Blueprint $table) {
            $table->dropForeign('category_services_category_id_categories_id');
            $table->dropForeign('category_services_service_id_services_id');
        });
        Schema::drop('products_groups');
    }
}
