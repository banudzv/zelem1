<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductsGroupsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_groups_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->foreign('group_id')->on('products_groups')->references('id')
                ->index('products_groups_categories_group_id_products_groups_id')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('category_id')->on('categories')->references('id')
                ->index('products_groups_categories_category_id_categories_id')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_groups_categories', function (Blueprint $table) {
            $table->dropForeign('products_groups_categories_group_id_products_groups_id');
            $table->dropForeign('products_groups_categories_category_id_categories_id');
        });
        Schema::dropIfExists('products_groups_categories');
    }
}
