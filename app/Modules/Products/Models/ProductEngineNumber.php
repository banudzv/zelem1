<?php

namespace App\Modules\Products\Models;

use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Engine\Models\Power;
use App\Modules\Engine\Models\Vendor;
use App\Modules\Engine\Models\Volume;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * Class EngineNumber
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property int $group_id
 * @property string $engine_number
 * @property-read ProductGroup $group
 * @property-read EngineNumber $number
 * @property-read Collection|Vendor[] $vendors
 * @property-read Collection|Model[] $models
 * @property-read Collection|Volume[] $volumes
 * @property-read Collection|Power[] $powers
 * @method static Builder|EngineNumber newModelQuery()
 * @method static Builder|EngineNumber newQuery()
 * @method static Builder|EngineNumber query()
 * @method static Builder|EngineNumber whereCreatedAt($value)
 * @method static Builder|EngineNumber whereId($value)
 * @method static Builder|EngineNumber whereUpdatedAt($value)
 * @method static Builder|EngineNumber whereGroupId($value)
 * @method static Builder|EngineNumber whereEngineNumber($value)
 * @mixin \Eloquent
 */
class ProductEngineNumber extends Model
{
    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'product_engine_numbers';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['group_id', 'engine_number'];

    /**
     * Relation to the product group.
     *
     * @return BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(ProductGroup::class, 'group_id', 'id');
    }

    /**
     * Relation to the engine number details.
     *
     * @return BelongsTo
     */
    public function number()
    {
        return $this->belongsTo(EngineNumber::class, 'engine_number', 'engine_number');
    }

    /**
     * Relation to the vendors.
     * TODO Check this relation correctness
     *
     * @return HasManyThrough
     */
    public function vendors()
    {
        return $this->hasManyThrough(
            Vendor::class, ProductEngineNumber::class,
            'engine_number', 'id', 'engine_number', 'vendor_id'
        );
    }

    /**
     * Relation to the models.
     * TODO Check this relation correctness
     *
     * @return HasManyThrough
     */
    public function models()
    {
        return $this->hasManyThrough(
            Model::class, ProductEngineNumber::class,
            'engine_number', 'id', 'engine_number', 'model_id'
        );
    }

    /**
     * Relation to the volumes.
     * TODO Check this relation correctness
     *
     * @return HasManyThrough
     */
    public function volumes()
    {
        return $this->hasManyThrough(
            Volume::class, ProductEngineNumber::class,
            'engine_number', 'id', 'engine_number', 'volume_id'
        );
    }

    /**
     * Relation to the powers.
     * TODO Check this relation correctness
     *
     * @return HasManyThrough
     */
    public function powers()
    {
        return $this->hasManyThrough(
            Power::class, ProductEngineNumber::class,
            'engine_number', 'id', 'engine_number', 'power_id'
        );
    }
}
