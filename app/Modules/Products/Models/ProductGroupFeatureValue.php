<?php

namespace App\Modules\Products\Models;

use App\Modules\Features\Models\Feature;
use App\Modules\Features\Models\FeatureTranslates;
use App\Modules\Features\Models\FeatureValue;
use App\Modules\Features\Models\FeatureValueTranslates;
use App\Modules\Products\Filters\SearchGroupsFilter;
use Eloquent;
use EloquentFilter\Filterable;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductGroupFeatureValue
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property int $group_id
 * @property int $product_id
 * @property int $feature_id
 * @property int $value_id
 * @property-read Feature $feature
 *
 * @see ProductGroupFeatureValue::featureTranslates()
 * @property-read Collection|FeatureTranslates[] $featureTranslates
 *
 * @property-read ProductGroup $group
 * @property-read Product $product
 * @property-read Collection|Product[] $products
 * @property-read int|null $products_count
 * @property-read Collection|ProductGroupFeatureValue[] $same
 * @property-read int|null $same_count
 * @property-read FeatureValue $value
 *
 * @see ProductGroupFeatureValue::valueTranslates()
 * @property-read Collection|FeatureValueTranslates[] $valueTranslates
 *
 * @see ProductFilter::generateFeatureCounters()
 * @property int count
 *
 * @method static Builder|self filter($input = array(), $filter = null)
 * @method static Builder|self newModelQuery()
 * @method static Builder|self newQuery()
 * @method static Builder|self paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|self query()
 * @method static Builder|self simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|self whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|self whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|self whereFeatureId($value)
 * @method static Builder|self whereGroupId($value)
 * @method static Builder|self whereId($value)
 * @method static Builder|self whereLike($column, $value, $boolean = 'and')
 * @method static Builder|self whereProductId($value)
 * @method static Builder|self whereValueId($value)
 * @mixin Eloquent
 */
class ProductGroupFeatureValue extends Model
{
    use Filterable;

    public $timestamps = false;

    protected $table = 'products_groups_features_values';

    protected $fillable = ['group_id', 'feature_id', 'value_id', 'product_id'];

    protected $hidden = ['id', 'group_id', 'product_id'];

    /**
     * @param ProductGroup $group
     * @return array
     */
    public static function getLinkedFeaturesAsArray(ProductGroup $group): array
    {
        $featuresIds = [];
        if ($group && $group->exists) {
            ProductGroupFeatureValue::whereGroupId($group->id)
                ->get()
                ->each(
                    function (ProductGroupFeatureValue $relation) use (&$featuresIds) {
                        $ids = $featuresIds[$relation->feature_id] ?? [];
                        $ids[] = $relation->value_id;
                        $featuresIds[$relation->feature_id] = $ids;
                    }
                );
        }
        return $featuresIds;
    }

    /**
     * @param ProductGroup[] $groups
     * @return array
     */
    public static function getLinkedFeaturesAsArrayForGroups($groups): array
    {
        $featuresIds = [];
        if ($groups && count($groups) > 0) {
            $groupsIds = [];
            foreach ($groups as $group) {
                $groupsIds[] = $group->id;
            }
            self::whereIn('group_id', $groupsIds)
                ->get()
                ->each(
                    function (ProductGroupFeatureValue $relation) use (&$featuresIds) {
                        $features = $featuresIds[$relation->group_id] ?? [];
                        $ids = $features[$relation->feature_id] ?? [];
                        $ids[] = $relation->value_id;
                        $featuresIds[$relation->group_id][$relation->feature_id] = $ids;
                    }
                );
        }
        return $featuresIds;
    }

    /**
     * @param ProductGroup $group
     * @param Product $product
     * @param FeatureValue $valueToRemove
     * @param FeatureValue $valueToAdd
     * @throws Exception
     */
    public static function change(
        ProductGroup $group,
        Product $product,
        ?FeatureValue $valueToRemove,
        ?FeatureValue $valueToAdd
    ): void {
        if ($valueToRemove && $valueToAdd && $valueToAdd->id === $valueToRemove->id) {
            return;
        }
        if ($valueToRemove) {
            ProductGroupFeatureValue::unlink($group, $product, $valueToRemove->feature_id, $valueToRemove->id);
        }
        if ($valueToAdd) {
            ProductGroupFeatureValue::link($group, $product, $valueToAdd->feature_id, $valueToAdd->id);
        }
    }

    /**
     * Unlink product, value and feature
     *
     * @param int $featureId
     * @param ProductGroup $group
     * @param Product $product
     * @param int|null $valueId
     * @return bool
     * @throws Exception
     */
    public static function unlink(ProductGroup $group, Product $product, int $featureId, ?int $valueId = null): bool
    {
        $query = ProductGroupFeatureValue::whereGroupId($group->id)
            ->whereProductId($product->id)
            ->whereFeatureId($featureId);
        if ($valueId) {
            $query->whereValueId($valueId);
        }
        return $query->delete();
    }

    /**
     * @param ProductGroup $group
     * @param Product $product
     * @param int $featureId
     * @param int $valueId
     * @return ProductGroupFeatureValue|Model
     */
    public static function link(ProductGroup $group, Product $product, int $featureId, int $valueId): Model
    {
        return ProductGroupFeatureValue::linkByIds($group->id, $product->id, $featureId, $valueId);
    }

    /**
     * @param int $groupId
     * @param int $productId
     * @param int $featureId
     * @param int $valueId
     * @return Model
     */
    public static function linkByIds(int $groupId, int $productId, int $featureId, int $valueId): Model
    {
        return self::updateOrCreate(
            [
                'group_id' => $groupId,
                'feature_id' => $featureId,
                'value_id' => $valueId,
                'product_id' => $productId,
            ]
        );
    }

    /**
     * @param ProductGroup $group
     * @param Product $product
     * @param int $featureId
     * @param int $valueId
     */
    public static function changeSingleValue(ProductGroup $group, Product $product, int $featureId, int $valueId): void
    {
        $old = self::whereGroupId($group->id)
            ->whereProductId($product->id)
            ->whereFeatureId($featureId)
            ->first();
        if ($old) {
            $old->update(
                [
                    'value_id' => $valueId,
                ]
            );
        } else {
            self::create(
                [
                    'group_id' => $group->id,
                    'product_id' => $product->id,
                    'feature_id' => $featureId,
                    'value_id' => $valueId,
                ]
            );
        }
    }

    public function group()
    {
        return $this->hasOne(ProductGroup::class, 'id', 'group_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function feature()
    {
        return $this->hasOne(Feature::class, 'id', 'feature_id');
    }

    public function featureTranslates()
    {
        return $this->hasMany(FeatureTranslates::class, 'row_id', 'feature_id');
    }

    public function value()
    {
        return $this->hasOne(FeatureValue::class, 'id', 'value_id');
    }

    public function valueTranslates()
    {
        return $this->hasMany(FeatureValueTranslates::class, 'row_id', 'value_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'group_id', 'group_id');
    }

    public function same()
    {
        return $this->hasMany(self::class, 'group_id', 'group_id');
    }

    /**
     * Register filter model
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(SearchGroupsFilter::class);
    }

    public function getAllFeatureValueSlugs(): array
    {
        $result = [];
        $valueTranslates = $this->valueTranslates->pluck('slug', 'language');

        foreach (config('languages') as $slug => $languages) {

            $valueSlug = $valueTranslates->get($slug);

            $result[$valueSlug] = $valueSlug;
            $cleanSlug = str_clear($valueSlug);
            $result[$cleanSlug] = $cleanSlug;
        }

        return array_values($result);
    }

    public function getAllFeatureValueNames(): array
    {
        $result = [];
        $valueTranslates = $this->valueTranslates->pluck('name', 'language');

        foreach (config('languages') as $slug => $languages) {

            $valueSlug = $valueTranslates->get($slug);

            $result[$valueSlug] = $valueSlug;

            $cleanSlug = str_clear($valueSlug);
            $result[$cleanSlug] = $cleanSlug;
        }

        return array_values($result);
    }

}
