<?php

namespace App\Modules\Products\Models;

use App\Core\Modules\Images\Models\Image;
use App\Exceptions\WrongParametersException;
use App\Modules\Brands\Models\Brand;
use App\Modules\Brands\Models\BrandTranslates;
use App\Modules\Categories\Models\Category;
use App\Modules\Comments\Models\Comment;
use App\Modules\Engine\Models\EngineNumber;
use App\Modules\Engine\Models\Year;
use App\Modules\Features\Models\Feature;
use App\Modules\Features\Models\FeatureTranslates;
use App\Modules\Features\Models\FeatureValueTranslates;
use App\Modules\Products\Components\Ecommercable;
use App\Modules\Products\Filters\EloquentProductSluggableFilter;
use App\Modules\ProductsLabels\Models\Label;
use App\Modules\Products\Filters\ProductGroupAdminFilter;
use App\Modules\Products\Images\GroupImage;
use App\Modules\ProductsDictionary\Models\Dictionary;
use App\Modules\ProductsDictionary\Models\DictionaryRelation;
use App\Modules\Sales\Models\Sales;
use App\Traits\ActiveScopeTrait;
use App\Traits\CheckRelation;
use App\Traits\Commentable;
use CustomForm\Element;
use CustomForm\SimpleElement;
use Eloquent;
use Exception;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use ProductsFilter;
use Widget, Catalog, Html;

/**
 * Class ProductGroup
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property bool $active
 * @property int $position
 * @property int|null $brand_id
 * @property int|null $category_id
 * @property int|null $feature_id
 * @property float $price_min
 * @property float $price_max
 * @property bool $available
 *
 * @see ProductGroup::brandTranslates()
 * @property-read BrandTranslates[]|EloquentCollection brandTranslates
 *
 * @see ProductGroup::engineNumbers()
 * @property-read EloquentCollection|ProductEngineNumber[] $engineNumbers
 *
 * @see ProductGroup::allEngineNumbers()
 * @property-read EloquentCollection|EngineNumber[] $allEngineNumbers
 *
 * @property-read Collection|Comment[] comments
 *
 * @property-read EloquentCollection|Year[] $years
 * @property-read EloquentCollection|FeatureTranslates[] $featuresCurrent
 * @property-read EloquentCollection|FeatureValueTranslates[] $valuesCurrent
 * @property-read EloquentCollection|Image[] $allImages
 * @property-read int|null $all_images_count
 * @property-read Brand|null $brand
 * @property-read BrandTranslates|null $brandCurrent
 * @property-read Category|null $category
 * @property-read ProductGroupTranslates $current
 * @property-read EloquentCollection|ProductGroupTranslates[] $data
 * @property-read int|null $data_count
 * @property-read EloquentCollection|Dictionary[] $dictionaries
 * @property-read int|null $dictionaries_count
 * @property-read EloquentCollection|DictionaryRelation[] $dictionaryRelations
 * @property-read int|null $dictionary_relations_count
 * @property-read Feature $feature
 * @property-read FeatureTranslates $featureCurrent
 * @property-read EloquentCollection|ProductGroupFeatureValue[] $featureValues
 * @property-read int|null $feature_values_count
 * @property-read EloquentCollection|ProductGroupFeatureValue[] $filters
 * @property-read int|null $filters_count
 * @property-read mixed $filtered_products
 * @property-read mixed $ignored_for_live_search
 * @property-read mixed $main_features
 * @property-read mixed $mark
 * @property-read mixed $name
 * @property-read mixed $other_categories_ids
 * @property-read mixed $preview
 * @property-read mixed $print_categories
 * @property-read Product|null $relevant_product
 * @property-read string $sorted_group_link
 * @property-read mixed $sorted_products
 * @property-read Image $image
 * @property-read EloquentCollection|Image[] $images
 * @property-read int|null $images_count
 * @property-read EloquentCollection|Label[] $labels
 * @property-read int|null $labels_count
 * @property-read EloquentCollection|ProductGroupLabel[] $labelsRelations
 * @property-read int|null $labels_relations_count
 * @property-read Product $mainProduct
 * @property-read EloquentCollection|Category[] $otherCategories
 * @property-read int|null $other_categories_count
 * @property-read EloquentCollection|ProductGroupCategory[] $otherCategoriesRelations
 * @property-read int|null $other_categories_relations_count
 * @property-read EloquentCollection|Product[] $products
 * @property-read int|null $products_count
 * @property-read EloquentCollection|ProductGroupFeatureValue[] $same
 * @property-read int|null $same_count
 * @method static Builder|ProductGroup active($active = true)
 * @method static Builder|ProductGroup filter($input = array(), $filter = null)
 * @method static Builder|ProductGroup newModelQuery()
 * @method static Builder|ProductGroup newQuery()
 * @method static Builder|ProductGroup paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|ProductGroup query()
 * @method static Builder|ProductGroup simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|ProductGroup whereActive($value)
 * @method static Builder|ProductGroup whereAvailable($value)
 * @method static Builder|ProductGroup whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|ProductGroup whereBrandId($value)
 * @method static Builder|ProductGroup whereCategoryId($value)
 * @method static Builder|ProductGroup whereCreatedAt($value)
 * @method static Builder|ProductGroup whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|ProductGroup whereFeatureId($value)
 * @method static Builder|ProductGroup whereId($value)
 * @method static Builder|ProductGroup whereLike($column, $value, $boolean = 'and')
 * @method static Builder|ProductGroup wherePosition($value)
 * @method static Builder|ProductGroup wherePriceMax($value)
 * @method static Builder|ProductGroup wherePriceMin($value)
 * @method static Builder|ProductGroup whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int|null $sale_id
 * @property-read EloquentCollection|Sales $sale
 */
class ProductGroup extends Model implements Ecommercable
{
    use Filterable;
    use Imageable;
    use ModelMain;
    use Commentable;
    use EloquentTentacle;
    use CheckRelation;
    use ActiveScopeTrait;

    public const LIMIT_PER_PAGE_BY_DEFAULT_ADMIN_PANEL = 20;

    public const LIMIT_PER_PAGE_BY_DEFAULT = 16;

    public const LIMIT_SLIDER_WIDGET = 15;

    public const DEFAULT_POSITION = 500;

    public const TABLE_NAME = 'products_groups';

    protected $table = self::TABLE_NAME;

    protected $casts = [
        'active' => 'boolean',
        'available' => 'boolean',
        'price_min' => 'double',
        'price_max' => 'double'
    ];

    protected $fillable = [
        'brand_id',
        'category_id',
        'position',
        'active',
        'available',
        'price_min',
        'price_max',
        'feature_id',
        'sale_id'
    ];

    public function isActive()
    {
        return $this->active;
    }

    protected function imageClass()
    {
        return GroupImage::class;
    }

    public function modelFilter()
    {
        return $this->provideFilter(ProductGroupAdminFilter::class);
    }

    public function filters()
    {
        return $this->hasMany(ProductGroupFeatureValue::class, 'group_id', 'id');
    }

    public function dictionaryRelations()
    {
        return $this->hasMany(DictionaryRelation::class, 'group_id', 'id');
    }

    public function dictionaries()
    {
        return $this->hasManyThrough(
            Dictionary::class,
            DictionaryRelation::class,
            'group_id',
            'id',
            'id',
            'dictionary_id'
        );
    }

    public function featuresCurrent()
    {
        return $this->hasManyThrough(
            FeatureTranslates::class,
            ProductGroupFeatureValue::class,
            'group_id',
            'row_id',
            'id',
            'feature_id'
        )
            ->distinct()
            ->where('language', $this->getCurrentLanguageSlug());
    }

    public function valuesCurrent()
    {
        return $this->hasManyThrough(
            FeatureValueTranslates::class,
            ProductGroupFeatureValue::class,
            'group_id',
            'row_id',
            'id',
            'value_id'
        )
            ->distinct()
            ->where('language', $this->getCurrentLanguageSlug());
    }

    /**
     * Relation to the engine numbers.
     *
     * @return HasMany
     */
    public function engineNumbers()
    {
        return $this->hasMany(ProductEngineNumber::class, 'group_id', 'id');
    }

    /**
     * Relation to the engine numbers.
     *
     * @return HasManyThrough
     */
    public function allEngineNumbers()
    {
        return $this->hasManyThrough(
            EngineNumber::class,
            ProductEngineNumber::class,
            'group_id',
            'engine_number',
            'id',
            'engine_number'
        );
    }

    /**
     * Relation to the years of production.
     *
     * @return HasMany
     */
    public function years()
    {
        return $this->hasMany(Year::class, 'group_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function sale()
    {
        return $this->hasOne(Sales::class, 'id', 'sale_id');
    }

    public function getPrintCategoriesAttribute(): string
    {
        $categories = [];
        $this->otherCategories->each(
            function (Category $category) use (&$categories) {
                $categories[] = Html::link(
                    $category->link_in_admin_panel,
                    $category->current->name,
                    ['target' => '_blank']
                );
            }
        );
        return count($categories) > 0 ? implode(', ', $categories) : '&nbsp;';
    }

    public function getMainFeaturesAttribute(): string
    {
        $features = [];
        foreach ($this->featureValues as $productFeatureValue) {
            if ($productFeatureValue->feature_id !== $this->feature_id && $productFeatureValue->value && $productFeatureValue->feature->main) {
                $features[$productFeatureValue->feature->current->name] = $features[$productFeatureValue->feature->current->name] ?? [];
                $features[$productFeatureValue->feature->current->name][$productFeatureValue->value_id] = $productFeatureValue->value->current->name;
            }
        }
        $descriptions = [];
        foreach ($features as $featureName => $values) {
            $descriptions[] = '<b>' . $featureName . ':</b> ' . implode(', ', $values);
        }
        return implode('<br />', $descriptions);
    }

    /**
     * @return Product|null
     */
    public function getRelevantProductAttribute()
    {
        $products = $this->filtered_products->where('available', '!=', Product::NOT_AVAILABLE);
        if ($products->isEmpty()) {
            $products = $this->filtered_products;
        }
        if ($products->isEmpty()) {
            $products = $this->products;
        }
        switch (request()->query('order')) {
            case 'price-asc':
                return $products->sortBy('price')->first();
            case 'price-desc':
                return $products->sortByDesc('price')->first();
        }
        return $products->first();
    }

    public function getFilteredProductsAttribute()
    {
        list ($priceMin, $priceMax) = array_pad(explode('-', request()->query('price')), 2, null);
        $priceMin = (float)$priceMin;
        $priceMax = (float)$priceMax;
        if (Catalog::currenciesLoaded()) {
            $priceMin = Catalog::currency()->calculateBack($priceMin);
            $priceMax = Catalog::currency()->calculateBack($priceMax);
        }
        $filter = ProductsFilter::getFilterParametersIdsFromQuery()->toArray();
        $currentFeatureFilter = array_get($filter, $this->feature_id, []);
        unset($currentFeatureFilter['brand']);
        return $this->products->filter(
            function (Product $product) use ($priceMin, $priceMax, $currentFeatureFilter) {
                if (count($currentFeatureFilter) > 0 && in_array($product->value_id, $currentFeatureFilter) === false) {
                    return false;
                }
                if ($priceMin > 0 && $product->price < $priceMin) {
                    return false;
                }
                if ($priceMax > 0 && $product->price > $priceMax) {
                    return false;
                }
                return true;
            }
        );
    }

    /**
     * @param EloquentCollection|LengthAwarePaginator|ProductGroup[] $groups
     */
    public static function loadMissingForLists($groups): void
    {
        $groups->loadMissing(
            'products',
            'current',
            'images',
            'comments',
            'images.current',
            'labels',
            'labels.current',
            'products.current',
            'products.images',
            'products.images.current',
            'products.value',
            'products.value.current',
            'products.group.category.current',
            'brand.current',
            'category.current'
        );

        /*if (config('db.products.show-brand-in-item-card', true)) {
            $groups->loadMissing(
                'brand',
                'brand.current'
            );
        }*/

        if (config('db.products.show-main-features')) {
            $groups->loadMissing(
                'featureValues.value',
                'featureValues.feature',
                'featureValues.value.current',
                'featureValues.feature.current'
            );
        }
    }

    /**
     * @return Collection|Image[]
     */
    public function gallery()
    {
        return $this->products->first()->images;
    }

    public function getPreviewAttribute()
    {
        foreach ($this->gallery() as $image) {
            if ($image->isImageExists()) {
                return $image;
            }
        }
        return new Image();
    }

    /**
     * @param array $filter
     * @param int|null $limit
     * @return LengthAwarePaginator|EloquentCollection|ProductGroup[]
     */
    public static function getFilteredList(array $filter = [], ?int $limit = null)
    {
        return ProductGroup::query()
            ->filter($filter, EloquentProductSluggableFilter::class)
            ->where(self::TABLE_NAME . '.active', true)
            ->paginate($limit ?: config('db.products.site-per-page', ProductGroup::LIMIT_PER_PAGE_BY_DEFAULT));
    }

    /**
     * @param int|null $groupId
     * @return string|null
     */
    public static function getElementForList(?int $groupId = null): ?string
    {
        if (!$groupId) {
            return null;
        }
        $group = ProductGroup::find($groupId);
        if (!$group) {
            return null;
        }
        return (string)Html::link(
            route('admin.groups.edit', $group->id),
            $group->current->name,
            ['target' => '_blank']
        );
    }

    public function getMarkAttribute(): int
    {
        $mark = 0;
        if ($this->comments->isNotEmpty()) {
            $count = 0;
            $total = 0;
            $this->comments->each(
                function (Model $comment) use (&$total, &$count) {
                    if ($comment->mark) {
                        $count++;
                        $total += $comment->mark;
                    }
                }
            );
            if ($count > 0) {
                $mark = round($total / $count);
            }
        }
        return $mark;
    }

    /**
     * @param int|null $commentableId
     * @return Element|null
     * @throws WrongParametersException
     */
    public static function formElementForComments(?int $commentableId = null): ?Element
    {
        return SimpleElement::create()
            ->setLabel('products::admin.choose-product')
            ->setDefaultValue(Widget::show('products::groups::live-search', [], 'commentable_id', $commentableId))
            ->required();
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function brandTranslates(): HasMany
    {
        return $this->hasMany(BrandTranslates::class, 'row_id', 'brand_id');
    }

    public function brandCurrent()
    {
        return $this
            ->hasOne(BrandTranslates::class, 'row_id', 'brand_id')
            ->where('language', $this->getCurrentLanguageSlug())
            ->withDefault();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function feature()
    {
        return $this->hasOne(Feature::class, 'id', 'feature_id');
    }

    public function featureCurrent()
    {
        return $this
            ->hasOne(FeatureTranslates::class, 'row_id', 'feature_id')
            ->where('language', $this->getCurrentLanguageSlug())
            ->withDefault();
    }

    public function labels()
    {
        $relation = $this->hasManyThrough(
            Label::class,
            ProductGroupLabel::class,
            'group_id',
            'id',
            'id',
            'label_id'
        );
        if (config('app.place') === 'site') {
            $relation->where('active', true);
        }
        return $relation;
    }

    public function labelsRelations()
    {
        return $this->hasMany(ProductGroupLabel::class, 'group_id', 'id');
    }

    public function featureValues()
    {
        return $this->hasMany(ProductGroupFeatureValue::class, 'group_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'group_id', 'id')->orderBy('position');
    }

    public function getSortedProductsAttribute()
    {
        return $this->products->sort(
            function (Product $prevProduct, Product $product) {
                if ($prevProduct->value_id && $product->value_id) {
                    return $prevProduct->value->position <=> $product->value->position;
                }
                return $prevProduct->position <=> $product->position;
            }
        );
    }

    public function same()
    {
        return $this->hasMany(ProductGroupFeatureValue::class, 'group_id', 'id');
    }

    public function otherCategories(): HasManyThrough
    {
        return $this->hasManyThrough(
            Category::class,
            ProductGroupCategory::class,
            'group_id',
            'id',
            'id',
            'category_id'
        )->with('current');
    }

    public function otherCategoriesForSync(): BelongsToMany
    {
        return $this->belongsToMany(Category::class,
        ProductGroupCategory::TABLE_NAME,
        'group_id',
        'id',
        'id',
        'category_id'
        );
    }

    public function getOtherCategoriesIdsAttribute(): array
    {
        $ids = [];
        ProductGroupCategory::whereGroupId($this->id)->get()->each(
            function (ProductGroupCategory $relation) use (&$ids) {
                $ids[] = $relation->category_id;
            }
        );
        return $ids;
    }

    public function otherCategoriesRelations()
    {
        return $this->hasMany(
            ProductGroupCategory::class,
            'group_id',
            'id'
        );
    }

    public function getNameAttribute(): ?string
    {
        return $this->current->name;
    }

    public function imageMeta(int $number = 1): ?string
    {
        return trans_choice(
            'products::site.image.meta',
            $number,
            [
                'name' => $this->name,
                'count' => $number
            ]
        );
    }

    /**
     * @return HasOne
     */
    public function mainProduct()
    {
        return $this->hasOne(Product::class, 'group_id', 'id')
            ->latest('is_main')
            ->latest('available')
            ->latest('id');
    }

    /**
     * @param Request $request
     * @return ProductGroup
     * @throws Exception
     */
    public static function store(Request $request): ProductGroup
    {
        $group = new ProductGroup();
        $group->createRow($request);
        $group->syncWithProducts($request);
        $group->updatePrices();
        $group->syncOtherCategories($request->input('categories', []), $request->input('category_id'));
        $group->syncLabels($request->input('labels', []));
        $group->syncAdditionalFeatureValues($request->input('dictionaries', []));
        $group->syncEngineNumbers($request->input('engine_numbers', []));

        return $group;
    }

    /**
     * Creates relations between product group and engine numbers.
     *
     * @param array $engineNumbers
     * @throws Exception
     */
    public function syncEngineNumbers(array $engineNumbers): void
    {
        ProductEngineNumber::whereGroupId($this->id)->whereNotIn('engine_number', $engineNumbers)->delete();
        $existedEngineNumbers = ProductEngineNumber::whereGroupId($this->id)->pluck('engine_number')->toArray();
        foreach (array_diff($engineNumbers, $existedEngineNumbers) as $engineNumber) {
            if (!$engineNumber) {
                continue;
            }
            ProductEngineNumber::create(['engine_number' => $engineNumber, 'group_id' => $this->id]);
        }
    }

    /**
     * @param Request $request
     * @throws Exception
     */
    public function edit(Request $request): void
    {
        $this->updateRow($request);
        $this->syncWithProducts($request);
        $this->updatePrices();
        $this->syncLabels($request->input('labels', []));
        $this->syncOtherCategories($request->input('categories', []), $request->input('category_id'));
        $this->syncAdditionalFeatureValues($request->input('dictionaries', []));
        $this->syncEngineNumbers($request->input('engine_numbers', []));
    }

    /**
     * @param array $additionalFeatureValuesIds
     * @throws Exception
     */
    private function syncAdditionalFeatureValues(array $additionalFeatureValuesIds = []): void
    {
        DictionaryRelation::whereGroupId($this->id)->whereNotIn('dictionary_id', $additionalFeatureValuesIds)->delete();
        foreach ($additionalFeatureValuesIds as $additionalFeatureValueId) {
            DictionaryRelation::updateOrCreate(
                [
                    'dictionary_id' => $additionalFeatureValueId,
                    'group_id' => $this->id,
                ]
            );
        }
    }

    public function updatePrices(): void
    {
        $products = $this->fresh()->products;
        $availableProducts = $products->where('available', true);
        if ($availableProducts->isNotEmpty()) {
            $this->price_min = $availableProducts->min('price');
            $this->price_max = $availableProducts->max('price');
            $this->available = true;
        } else {
            $this->price_min = $products->min('price') ?: 0;
            $this->price_max = $products->max('price') ?: 0;
            $this->available = false;
        }
        $this->save();
    }

    public function syncWithProducts(Request $request): void
    {
        $hasProducts = $this->products->isNotEmpty();

        $modifications = new Collection();
        foreach ($request->input('modification', []) as $field => $values) {
            foreach ($values as $index => $value) {
                $modification = $modifications->get($index, []);
                $modification[$field] = $value;
                $modifications->put($index, $modification);
            }
        }
        foreach (config('languages', []) as $lang => $language) {
            foreach ($request->input($lang . '.modification', []) as $field => $values) {
                foreach ($values as $index => $value) {
                    $modification = $modifications->get($index, []);
                    $modification[$lang][$field] = $value;
                    $modifications->put($index, $modification);
                }
            }
        }
        $modifications->each(
            function ($modification, $index) use ($hasProducts, $request) {
                if (count($modification) === 1) {
                    return;
                }
                if ($hasProducts === false) {
                    $modification['is_main'] = $index === 0;
                }
                $modification['images'] = $request->file("modification.$index.images", []);
                Product::createOrUpdateFromArray($modification, $this);
            }
        );
    }

    /**
     * @param array $labelsIds
     * @throws Exception
     */
    public function syncLabels(array $labelsIds = [])
    {
        ProductGroupLabel::whereGroupId($this->id)->whereNotIn('label_id', $labelsIds)->delete();
        foreach ($labelsIds as $labelId) {
            ProductGroupLabel::updateOrCreate(
                [
                    'label_id' => $labelId,
                    'group_id' => $this->id,
                ]
            );
        }
    }

    /**
     * @param array $categoryIds
     * @param int|null $categoryId
     * @throws Exception
     */
    public function syncOtherCategories(array $categoryIds = [], ?int $categoryId = null)
    {
        $this->otherCategoriesForSync()->sync($categoryIds);
//        if (in_array($categoryId, $categoryIds) === false) {
//            $categoryIds[] = $categoryId;
//        }
//        $realCategoryIds = [];
//        if ($categoryIds) {
//            Category::with('parent')->whereIn('id', $categoryIds)->get()->each(
//                function (Category $category) use (&$realCategoryIds) {
//                    $this->iterativeWithParents($realCategoryIds, $category);
//                }
//            );
//        }
//        ProductGroupCategory::whereGroupId($this->id)->whereNotIn('category_id', $realCategoryIds)->delete();
//        foreach ($realCategoryIds as $categoryId) {
//            ProductGroupCategory::updateOrCreate(
//                [
//                    'category_id' => $categoryId,
//                    'group_id' => $this->id,
//                ]
//            );
//        }
    }

    /**
     * @param array $dictionary
     * @param Category $category
     */
    private function iterativeWithParents(array &$dictionary, Category $category): void
    {
        $dictionary[] = $category->id;
        if ($category->parent_id && $category->parent) {
            $this->iterativeWithParents($dictionary, $category->parent);
        }
    }

    public function getIgnoredForLiveSearchAttribute(): array
    {
        $ignored = [$this->id];
        $this->related->each(
            function (ProductGroup $group) use (&$ignored) {
                $ignored[] = $group->id;
            }
        );
        return $ignored;
    }

    /**
     * @param int|null $limit
     * @param array $ignored
     * @return LengthAwarePaginator|EloquentCollection
     */
    public static function search(?int $limit = null, array $ignored = []): LengthAwarePaginator
    {
        $forFilter = request()->only('query', 'order');
        $forFilter['order'] = $forFilter['order'] ?? 'default';
        $groupsQuery = ProductGroup::whereNotIn('id', $ignored)->filter($forFilter);
        $limit = (int)$limit ?: request()->query('per-page');
        $groups = $groupsQuery->paginate(
            $limit ?: config('db.products.per-page', static::LIMIT_PER_PAGE_BY_DEFAULT_ADMIN_PANEL)
        );
        return $groups;
    }

    public function toggleActiveStatus(): void
    {
        $this->active = !$this->active;
        $this->save();

        $this->products->each(
            function (Product $product) {
                $product->active = $this->active;
                $product->save();
            }
        );
    }


    /**
     * @param int $id
     * @return array|null
     */
    public function getPagesLinksByIdForImage(int $id)
    {
        $links = [];
        $item = ProductGroup::active()->find($id);
        if ($item && $item->products) {
            foreach ($item->products as $product) {
                $links[] = url(route('site.product', ['slug' => $product->current->slug], false), [], isSecure());
            }
        }
        return $links;
    }

    /**
     * @return string
     */
    public function getSortedGroupLinkAttribute(): string
    {
        return route('admin.groups.set-position-value', $this->id);
    }

    public function getComments($limit = null)
    {
        if ($limit) {
            return $this->comments()->limit($limit)->get();
        }
        return $this->comments;
    }

    public function ecommId(): string
    {
        return $this->relevant_product->id;
    }

    public function ecommName(): string
    {
        return $this->relevant_product->name;
    }

    public function ecommPrice(): float
    {
        return $this->relevant_product->number_price_for_site;
    }

    public function ecommBrand(): ?string
    {
        return $this->brand ? $this->brand->current->name : null;
    }

    public function ecommCategory(): string
    {
        return $this->category->current->name;
    }

    public function getEcommDataAttribute()
    {
        return [
            'id' => $this->ecommId(),
            'name' => $this->ecommName(),
            'price' => $this->ecommPrice(),
            'brand' => $this->ecommBrand(),
            'category' => $this->ecommCategory(),
            'link' => $this->relevant_product->site_link
        ];
    }
}
