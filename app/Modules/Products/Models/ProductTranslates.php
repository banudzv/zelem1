<?php

namespace App\Modules\Products\Models;

use App\Core\Modules\Languages\Models\Language;
use App\Exceptions\WrongParametersException;
use App\Modules\Features\Models\FeatureValue;
use App\Rules\MultilangSlug;
use App\Traits\ModelTranslates;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Validator;

/**
 * Class ProductTranslates
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $short_name
 * @property string|null $search_keywords
 * @property int $row_id
 * @property string $language
 * @property-read Language $lang
 * @property-read Product $row
 * @method static Builder|ProductTranslates newModelQuery()
 * @method static Builder|ProductTranslates newQuery()
 * @method static Builder|ProductTranslates query()
 * @method static Builder|ProductTranslates whereId($value)
 * @method static Builder|ProductTranslates whereLanguage($value)
 * @method static Builder|ProductTranslates whereName($value)
 * @method static Builder|ProductTranslates whereRowId($value)
 * @method static Builder|ProductTranslates whereShortName($value)
 * @method static Builder|ProductTranslates whereSlug($value)
 * @mixin Eloquent
 */
class ProductTranslates extends Model
{
    use ModelTranslates;

    public $timestamps = false;

    protected $table = 'products_translates';

    protected $fillable = [
        'name',
        'slug',
        'short_name',
        'row_id',
        'language',
        'search_keywords',
    ];

    /**
     * @param string $lang
     * @param array $data
     * @param int $productId
     * @param ProductGroup $group
     * @param FeatureValue $value
     * @return ProductTranslates|null
     * @throws WrongParametersException
     */
    public static function createOrUpdateFromArray(
        string $lang,
        array $data,
        ?int $productId,
        ProductGroup $group,
        FeatureValue $value = null
    ): ?ProductTranslates {
        if (!isset($data['slug']) || !$data['slug']) {
            if (isset($data['name']) && $data['name']) {
                $data['slug'] = Str::slug($data['name']);
            } else {
                $data['slug'] = Str::slug($group->brand->dataFor($lang)->name ?? Str::random(8));
            }
        }
        if (Validator::make(
            $data,
            ['slug' => new MultilangSlug((new ProductTranslates)->getTable(), $lang, $productId)]
        )->fails()) {
            $data['slug'] .= '-' . Str::random(8);
        }
        return ProductTranslates::updateOrCreate(
            [
                'row_id' => $productId,
                'language' => $lang,
            ],
            $data
        );
    }
}
