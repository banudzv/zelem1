<?php

namespace App\Modules\Products\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\Products\Models\ProductGroupTranslates
 *
 * @property int $id
 * @property int $row_id
 * @property string $language
 * @property string $name
 * @property string|null $text
 * @property string|null $text_related
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\Products\Models\ProductGroup $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates whereRowId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates whereTextRelated($value)
 * @mixin \Eloquent
 * @property string|null $documents
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductGroupTranslates whereDocuments($value)
 */
class ProductGroupTranslates extends Model
{
    use ModelTranslates;

    public $timestamps = false;

    protected $table = 'products_groups_translates';

    protected $fillable = ['name', 'text', 'row_id', 'language'];
}
