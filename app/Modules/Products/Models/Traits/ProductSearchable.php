<?php

namespace App\Modules\Products\Models\Traits;

use App\Modules\Products\Models\ProductGroup;
use App\Modules\Products\Models\ProductGroupFeatureValue;
use App\Modules\Search\Configurations\ProductIndexConfigurator;
use App\Modules\Search\Models\Traits\Searchable;
use Closure;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

/**
 * @property-read Collection|ProductGroup $group
 * @property-read Collection data
 */
trait ProductSearchable
{
    use Searchable;

    protected $indexConfigurator = ProductIndexConfigurator::class;

    protected $searchRules = [
    ];

    protected $mapping
        = [
            'dynamic' => false,
            'properties' => [
                'id' => ['type' => 'long'],
                'group_id' => ['type' => 'integer'],

                'position' => ['type' => 'integer'],
                'relevance' => ['type' => 'integer'],
                'active' => ['type' => 'boolean'],
                'available' => ['type' => 'short'],

                'brand_slugs' => ['type' => 'keyword'],
                'brand_id' => ['type' => 'long'],
                'brand_names' => [
                    'type' => 'text',
                    'fielddata' => true,
                    'fields' => [
                        'keyword' => [
                            'type' => 'keyword',
                            'ignore_above' => 256,
                        ],
                    ],
                ],
                'brand_search_keywords' => [
                    'type' => 'text',
                    'analyzer' => 'whitespace',
                ],

                'category_slugs' => ['type' => 'keyword'],
                'category_ids' => ['type' => 'long'],
                'category_names' => [
                    'type' => 'text',
                    'fielddata' => true,
                    'analyzer' => 'rebuilt_russian',
                    'fields' => [
                        'keyword' => [
                            'type' => 'keyword',
                            'ignore_above' => 256,
                        ],
                    ],
                ],
                'category_search_keywords' => ['type' => 'text'],

                'feature_value_slugs' => ['type' => 'keyword'],
                'feature_value_ids' => ['type' => 'long'],
                'feature_value_names' => [
                    'type' => 'text',
                    'analyzer' => 'whitespace',
                    'fielddata' => true,
                    'fields' => [
                        'keyword' => [
                            'type' => 'keyword',
                            'ignore_above' => 256,
                        ],
                    ],
                ],

                'vendor_code' => ['type' => 'keyword'],

                'name' => [
                    'type' => 'text',
                    'fielddata' => true,
                    'analyzer' => 'rebuilt_russian',
                    'fields' => [
                        'keyword' => [
                            'type' => 'keyword',
                            'ignore_above' => 256,
                        ],
                    ],
                ],
                'name_ua' => ['type' => 'keyword'],
                'name_ru' => ['type' => 'keyword'],
                'search_keywords' => ['type' => 'text'],

//            'text' => ['type' => 'text'],

                'price' => ['type' => 'float'],
                'old_price' => ['type' => 'float'],

                'cars' => [
                    'type' => 'text',
                    'analyzer' => 'whitespace',
                    'fielddata' => true,
                    'fields' => [
                        'keyword' => [
                            'type' => 'keyword',
                            'ignore_above' => 256,
                        ],
                    ],
                ],

                'car_vendor_ids' => ['type' => 'integer'],
                'car_vendor_names' => ['type' => 'text'],

                'car_model_ids' => ['type' => 'integer'],
                'car_model_names' => ['type' => 'text'],

                'car_volume_ids' => ['type' => 'integer'],
                'car_volume_names' => ['type' => 'text'],

                'car_power_ids' => ['type' => 'integer'],
                'car_power_names' => ['type' => 'text'],

                'car_engine_numbers' => ['type' => 'keyword'],
                'car_engine_names' => [
                    'type' => 'text',
                    'analyzer' => 'whitespace',
                    'fielddata' => true,
                    'fields' => [
                        'keyword' => [
                            'type' => 'keyword',
                            'ignore_above' => 256,
                        ],
                    ],
                ],
            ],
        ];

    public function searchableWith()
    {
        return [
            'data',
            'group.brand.data',
            'group.allEngineNumbers.model.vendor',
            'group.allEngineNumbers.vendor',
            'group.allEngineNumbers.volume',
            'group.allEngineNumbers.power.data',
            'group.category.data',
            'group.otherCategories.data',
            'group.featureValues.featureTranslates',
            'group.featureValues.valueTranslates',
        ];
    }

    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'group_id' => $this->group_id,

            'position' => $this->getPosition(),
            'relevance' => $this->getRelevance(),

            'active' => $this->isActive(),
            'available' => $this->available,

            'brand_slugs' => $this->getBrandSlugs(),
            'brand_id' => $this->getBrandId(),
            'brand_names' => $this->getBrandNames(),
            'brand_search_keywords' => $this->getBrandSearchKeywords(),

            'category_slugs' => $this->getCategorySlugs(),
            'category_ids' => $this->getCategoryIds(),
            'category_names' => $this->getCategoryNames(),
            'category_search_keywords' => $this->getCategorySearchKeywords(),

            'feature_value_slugs' => $this->getFeatureValueSlugs(),
            'feature_value_ids' => $this->getFeatureValueIds(),
            'feature_value_names' => $this->getFeatureValueNames(),

            'vendor_code' => $this->getVendorCodes(),
            'name' => $this->getNames(),
            'name_ua' => $this->getNameForLang('ua'),
            'name_ru' => $this->getNameForLang('ru'),
            'search_keywords' => $this->getProductSearchKeywords(),

            'price' => $this->getPrice(),
            'old_price' => $this->getOldPrice(),

            'cars' => $this->getCars(),

            'car_vendor_ids' => $this->getCarVendorIds(),
            'car_vendor_names' => $this->getCarVendorNames(),

            'car_model_ids' => $this->getModelIds(),
            'car_model_names' => $this->getModelNames(),

            'car_volume_ids' => $this->getVolumeIds(),
            'car_volume_names' => $this->getVolumeNames(),

            'car_power_ids' => $this->getPowerIds(),
            'car_power_names' => $this->getPowerNames(),

            'car_engine_numbers' => $this->getEngineNumbers(),
            'car_engine_names' => $this->getEngineNumbers(),
        ];
    }

    public function getPosition(): int
    {
        return $this->group->position;
    }

    public function getRelevance(): int
    {
        return array_sum($this->getRelevanceMap());
    }

    public function getRelevanceMap(): array
    {
        return [
            'brand' => $this->group->brand
                ? $this->group->brand->getRelevance()
                : 0,
            'category' => $this->group->category
                ? $this->group->category->getRelevance()
                : 0,
            'product' => $this->relevance ?? 0,
        ];
    }

    protected function getBrandSlugs(): array
    {
        if ($this->group->brand) {
            return $this->group->brand->data
                ->pluck('slug', 'slug')
                ->values()
                ->toArray();
        }

        return [];
    }

    protected function getBrandId(): ?int
    {
        return $this->group->brand_id;
    }

    protected function getBrandNames(): array
    {
        if ($this->group->brand) {
            return $this->group
                ->brand
                ->data
                ->pluck('name', 'name')
                ->values()
                ->map($this->textClear())
                ->toArray();
        }

        return [];
    }

    protected function textClear(): Closure
    {
        return function (string $string) {
            return text_clear($string);
        };
    }

    public function getBrandSearchKeywords(): array
    {
        if ($this->group->brand) {
            return $this->group
                ->brand
                ->data
                ->pluck('search_keywords')
                ->filter()
                ->unique()
                ->values()
                ->toArray();
        }

        return [];
    }

    protected function getCategorySlugs(): array
    {
        $categorySlugs = $this->group
            ->otherCategories
            ->pluck('data.*.slug')
            ->flatten()
            ->toArray();

        if ($this->group->category_id) {
            $categorySlugs = array_merge(
                $categorySlugs,
                $this->group
                    ->category
                    ->data
                    ->pluck('slug')
                    ->flatten()
                    ->toArray()
            );
        }

        return array_values(
            array_unique($categorySlugs)
        );
    }

    protected function getCategoryIds(): array
    {
        $categories = $this->group
            ->otherCategories
            ->pluck('id')
            ->toArray();

        if ($this->group->category_id) {
            $categories[] = $this->group->category_id;
        }

        return array_unique($categories);
    }

    protected function getCategoryNames(): array
    {
        $categoryNames = $this->group
            ->otherCategories
            ->pluck('data.*.name')
            ->flatten()
            ->toArray();

        if ($this->group->category_id) {
            $categoryNames = array_merge(
                $categoryNames,
                $this->group
                    ->category
                    ->data
                    ->pluck('name')
                    ->flatten()
                    ->toArray()
            );
        }

        $categoryNames = array_unique($categoryNames);
        $categoryNames = array_values($categoryNames);
        return array_map($this->textClear(), $categoryNames);
    }

    public function getCategorySearchKeywords(): array
    {
        $keywords = $this->group
            ->otherCategories
            ->pluck('data.*.search_keywords')
            ->flatten()
            ->toArray();

        if ($this->group->category_id) {
            $keywords = array_merge(
                $keywords,
                $this->group
                    ->category
                    ->data
                    ->pluck('search_keywords')
                    ->flatten()
                    ->toArray()
            );
        }

        $keywords = array_filter($keywords);
        $keywords = array_unique($keywords);
        return array_values($keywords);
    }

    protected function getFeatureValueSlugs(): array
    {
        return $this->group->featureValues->map(
            function (ProductGroupFeatureValue $featureValue) {
                return $featureValue->getAllFeatureValueSlugs();
            }
        )
            ->flatten()
            ->toArray();
    }

    protected function getFeatureValueIds(): array
    {
        return $this->group
            ->featureValues
            ->pluck('value_id')
            ->unique()
            ->toArray();
    }

    protected function getFeatureValueNames(): array
    {
        return $this->group->featureValues->map(
            function (ProductGroupFeatureValue $featureValue) {
                return $featureValue->getAllFeatureValueNames();
            }
        )
            ->flatten()
            ->map($this->textClear())
            ->toArray();
    }

    protected function getVendorCodes(): array
    {
        $result = [];

        if (empty($this->vendor_code)) {
            return $result;
        }

        $vendorCode = $this->vendor_code;
        $result[$vendorCode] = $vendorCode;

        $sluggedVendorCode = Str::slug($vendorCode);
        $result[$sluggedVendorCode] = $sluggedVendorCode;

        $clearVendorCode = str_clear($vendorCode);
        $result[$clearVendorCode] = $clearVendorCode;

        $clearTextVendorCode = text_clear($vendorCode);
        $result[$clearTextVendorCode] = $clearTextVendorCode;

        return array_unique(
            array_values($result)
        );
    }

    protected function getNames(): array
    {
        return $this->data
            ->pluck('name', 'name')
            ->values()
            ->map($this->textClear())
            ->toArray();
    }

    protected function getNameForLang($lang)
    {
        $first = $this->data->where('language', $lang)->first();
        return $first ? text_clear($first->name) : null;
    }

    public function getProductSearchKeywords(): array
    {
        return $this->data
            ->pluck('search_keywords')
            ->unique()
            ->values()
            ->toArray();
    }

    public function getCars(): array
    {
        $result = [];

        foreach ($this->group->allEngineNumbers as $number) {
            $fullName = text_clear($number->model->getFullName());
            $result[$fullName] = $fullName;
        }

        return array_values(
            array_unique($result)
        );
    }

    public function getCarVendorIds(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('vendor_id', 'vendor_id')
            ->values()
            ->toArray();
    }

    public function getCarVendorNames(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('vendor.name')
            ->unique()
            ->filter()
            ->values()
            ->toArray();
    }

    public function getModelIds(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('model_id', 'model_id')
            ->values()
            ->toArray();
    }

    public function getModelNames(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('model.name')
            ->unique()
            ->filter()
            ->values()
            ->toArray();
    }

    public function getVolumeIds(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('volume_id', 'volume_id')
            ->values()
            ->toArray();
    }

    public function getVolumeNames(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('volume.name')
            ->unique()
            ->filter()
            ->values()
            ->toArray();
    }

    public function getPowerIds(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('power_id', 'power_id')
            ->values()
            ->toArray();
    }

    public function getPowerNames(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('power.data.*.name')
            ->flatten()
            ->unique()
            ->filter()
            ->values()
            ->toArray();
    }

    public function getEngineNumbers(): array
    {
        return $this->group
            ->allEngineNumbers
            ->pluck('engine_number', 'engine_number')
            ->values()
            ->map($this->textClear())
            ->toArray();
    }

    public function searchableAs()
    {
        return self::TABLE_NAME;
    }

}