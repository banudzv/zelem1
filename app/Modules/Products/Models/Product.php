<?php

namespace App\Modules\Products\Models;

use App\Core\Modules\Images\Models\Image;
use App\Exceptions\WrongParametersException;
use App\Modules\Brands\Models\Brand;
use App\Modules\Categories\Models\Category;
use App\Modules\Comments\Models\Comment;
use App\Modules\Currencies\Models\Currency;
use App\Modules\Features\Models\FeatureValue;
use App\Modules\Features\Models\FeatureValueTranslates;
use App\Modules\Import\Models\ProductsGroupsRemote;
use App\Modules\Products\Components\Ecommercable;
use App\Modules\Products\Filters\ProductsAdminFilter;
use App\Modules\Products\Filters\SearchFilter;
use App\Modules\Products\Images\ProductImage;
use App\Modules\Products\Models\Traits\ProductSearchable;
use App\Modules\ProductsAvailability\Events\ChangeProductStatusEvent;
use App\Modules\ProductsLabels\Models\Label;
use App\Traits\ActiveScopeTrait;
use App\Traits\CheckRelation;
use App\Traits\FormattedDateAdmin;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use Carbon\Carbon;
use Catalog;
use Eloquent;
use EloquentFilter\Filterable;
use Event;
use Exception;
use Greabock\Tentacles\EloquentTentacle;
use Html;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Http\UploadedFile;
use Lang;
use Seo;
use Widget;

/**
 * Class Product
 *
 * @package App\Modules\Products\Models
 * @property int $id
 * @property bool $active
 * @property int $available
 * @property float $price
 * @property float|null $old_price
 * @property string|null $vendor_code
 * @property int|null $relevance
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property bool $is_main
 * @property int $group_id
 * @property int|null $value_id
 * @property-read Collection|Image[] $allImages
 * @property-read int|null $all_images_count
 * @property-read Collection|Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read ProductTranslates $current
 * @property-read Collection|ProductTranslates[] $data
 * @property-read int|null $data_count
 * @property-read array $feature_values_as_array
 * @property-read Collection|ProductGroupFeatureValue[] $feature_values
 * @property-read array $features_list
 * @property-read array $features_list_export
 * @property-read null|string $formatted_date
 * @property-read string $formatted_old_price
 * @property-read string $formatted_old_price_for_admin
 * @property-read string $formatted_price
 * @property-read string $formatted_price_for_admin
 *
 * @see Product::getIsAvailableAttribute()
 * @property-read mixed $is_available
 *
 * @property-read mixed $is_custom_goods
 * @property-read string $main_features
 * @property-read string|null $microdata
 * @property-read mixed $name
 * @property-read int $old_price_for_site
 * @property-read mixed $preview
 * @property-read float $price_for_site
 * @property-read string $price_with_code
 * @property-read null|string $published_date
 * @property-read string $site_link
 * @property-read string $sorted_product_link
 * @property-read Collection $tabs
 *
 * @see Product::brand()
 * @property-read Brand $brand
 *
 * @property-read FeatureValueTranslates $valueCurrent
 * @property-read ProductGroup $group
 * @property-read Image $image
 * @property-read Collection|Image[] $images
 * @property-read int|null $images_count
 * @property-read Collection|Label[] $labels
 * @property-read int|null $labels_count
 * @property-read Collection|ProductWholesale[] $wholesale
 * @property-read int|null $wholesale_count
 *
 * @see \App\Modules\Products\Services\EloquentProductSearchService::getPriceStatsByRequest()
 * @property null|float minPrice
 * @property null|float maxPrice
 *
 * @method static Builder|Product active($active = true)
 * @method static Builder|Product filter($input = array(), $filter = null)
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Product query()
 * @method static Builder|Product simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Product whereActive($value)
 * @method static Builder|Product whereAvailable($value)
 * @method static Builder|Product whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|Product whereGroupId($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereIsMain($value)
 * @method static Builder|Product whereLike($column, $value, $boolean = 'and')
 * @method static Builder|Product whereOldPrice($value)
 * @method static Builder|Product wherePosition($value)
 * @method static Builder|Product wherePrice($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @method static Builder|Product whereValueId($value)
 * @method static Builder|Product whereVendorCode($value)
 * @mixin Eloquent
 */
class Product extends Model implements Ecommercable
{
    use ModelMain, Imageable, ActiveScopeTrait, EloquentTentacle, CheckRelation, Filterable, FormattedDateAdmin;

    use ProductSearchable;

    const LIMIT_PER_PAGE_BY_DEFAULT = 10;
    const LIMIT_PER_PAGE_BY_DEFAULT_ADMIN_PANEL = 10;
    const LIMIT_SLIDER_WIDGET = 15;

    const DEFAULT_POSITION = 500;

    const NOT_AVAILABLE = 0;
    const AVAILABLE = 1;
    const CUSTOM_GOODS = 2;

    const SEO_TEMPLATE_ALIAS = 'products';

    const THREE_IN_THE_ROW = 'three-in-the-row';
    const FOUR_IN_THE_ROW = 'four-in-the-row';

    const TABLE_NAME = 'products';

    public static $specs_feature_export;

    protected $table = self::TABLE_NAME;

    protected $casts = [
        'price' => 'float',
        'old_price' => 'float',
        'is_main' => 'boolean',
        'active' => 'boolean'
    ];

    protected $fillable = [
        'active',
        'price',
        'available',
        'old_price',
        'position',
        'vendor_code',
        'relevance',
        'group_id',
        'is_main',
        'value_id'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function value()
    {
        return $this->hasOne(FeatureValue::class, 'id', 'value_id');
    }

    public function valueCurrent()
    {
        return $this->hasOne(FeatureValueTranslates::class, 'row_id', 'value_id')
            ->where('language', config('app.place') === 'site' ? Lang::getLocale() : config('app.default-language', app()->getLocale()))
            ->withDefault();
    }

    public function gallery()
    {
        return $this->images;
    }

    public function getIsAvailableAttribute(): bool
    {
        return $this->available === static::AVAILABLE || (bool)config('db.products.show-availability', true) === false;
    }

    public function getIsCustomGoodsAttribute(): bool
    {
        return $this->available === static::CUSTOM_GOODS;
    }

    public function brand()
    {
        return $this->hasOneThrough(Brand::class, ProductGroup::class, 'id', 'id', 'group_id', 'brand_id');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable', 'groups', null, 'group_id')
            ->where('active', true)
            ->where('published_at', '<=', Carbon::now());
    }

    public function getPreviewAttribute()
    {
        foreach ($this->images as $image) {
            if ($image->isImageExists()) {
                return $image;
            }
        }

        return new Image();
    }

    public function getNameAttribute(): string
    {
        if (!$this->exists) {
            return '';
        }
        if ($this->current->name) {
            return $this->current->name;
        }
        $parts = [];
        $parts[] = $this->group->name;
        if ($this->value_id) {
            $parts[] = $this->value->current->name;
        }

        return implode(' ', $parts);
    }

    public function getNameForLanguage(string $lang)
    {
        if (!$this->exists) {
            return '';
        }
        if ($this->dataFor($lang) and $this->dataFor($lang)->name) {
            return $this->dataFor($lang)->name;
        }
        $parts = [];
        $parts[] = $this->group->dataFor($lang)->name;
        if ($this->value_id) {
            $parts[] = $this->value->dataFor($lang)->name;
        }

        return implode(' ', $parts);
    }

    /**
     * TODO
     * @param array $modification
     * @param ProductGroup $group
     * @return Product
     * @throws WrongParametersException|Exception
     */
    public static function createOrUpdateFromArray(array $modification, ProductGroup $group): Product
    {
        $price = array_get($modification, 'price');
        $oldPrice = array_get($modification, 'old_price');
        if ($price > $oldPrice && $oldPrice > 0) {
            $modification['price'] = $oldPrice;
            $modification['old_price'] = $price;
        } elseif ($price === $oldPrice && $oldPrice > 0) {
            $modification['old_price'] = ($oldPrice - 1) ?: 0;
        }
        $modification['brand_id'] = $group->brand_id;
        $modification['group_id'] = $group->id;
        $product = Product::findOrNew(array_get($modification, 'id', 0));
        $originalAvailable = $product->available;
        $lastValue = $product->value;
        /*if ($product->group && $product->group->feature && $product->group->feature->current && $product->value) {
            foreach (config('languages', []) as $lang => $language) {
                foreach ($modification as $key => $value) {
                    if ($lang === $key) {
                        $productFeatureName = $product->group->feature->dataFor($lang)->name;
                        $productValueName = $product->value->dataFor($lang)->name;
                        $modificationName = $modification[$lang]['name'] ?? $product->group->dataFor($lang)->name;
                        if($productFeatureName && $productValueName && $modificationName) {
                            if(!stristr($modificationName,$productFeatureName) && (!stristr($modificationName,$productValueName))){
                                $modification[$lang]['name'] = $modificationName . ' ' . $productFeatureName . ' ' . $productValueName;
                            }
                        }
                    }
                }
            }
        }*/
        $product->fill($modification);
        $product->active = $group->active;

        if ($product->id && isset($originalAvailable) && $originalAvailable !== (int)$product->available && (int)$product->available) {
            Event::dispatch(new ChangeProductStatusEvent($product->id));
        }

        $product->save();
        $needsFeatureRebase = $product->wasRecentlyCreated || $product->wasChanged('value_id');
        $product = $product->fresh();
        // Translations
        foreach (config('languages') as $lang => $language) {
            ProductTranslates::createOrUpdateFromArray($lang, array_get($modification, $lang, []), $product->id, $group, $product->value);
        }
        // Main feature
        if ($needsFeatureRebase) {
            ProductGroupFeatureValue::change($group, $product, $lastValue, $product->value);
        }
        // Upload images
        foreach (array_get($modification, 'images', []) as $image) {
            /** @var UploadedFile $image */
            $product->uploadImageFromResource($image);
        }
        // Wholesales
        $quantities = explode(';', array_get($modification, 'wholesaleQuantities'));
        $prices = explode(';', array_get($modification, 'wholesalePrices'));
        $product->addWholesales($quantities, $prices);
        return $product;
    }

    /**
     * TODO
     * @param array $quantities
     * @param array $prices
     * @throws Exception
     */
    public function addWholesales(array $quantities, array $prices): void
    {
        ProductWholesale::whereProductId($this->id)->delete();
        foreach ($quantities as $key => $value) {
            if (array_get($quantities, $key) && array_get($prices, $key)) {
                ProductWholesale::whereProductId($this->id)
                    ->firstOrCreate([
                        'product_id' => $this->id,
                        'quantity' => array_get($quantities, $key),
                        'price' => array_get($prices, $key),
                    ]);
            }
        }
    }

    /**
     * TODO
     * @param Collection|LengthAwarePaginator|Product[]|Product $products
     */
    public static function loadMissingForLists($products): void
    {
        $products->loadMissing(
            'group', 'current', 'images', 'images.current', 'group.comments',
            'labels', 'labels.current', 'group.current', 'group.images', 'group.images.current',
            'value', 'value.current', 'brand.current', 'category.current'
        );

        /*if (config('db.products.show-brand-in-item-card', true)) {
            $products->loadMissing(
                'brand',
                'brand.current'
            );
        }*/

        if (config('db.products.show-main-features')) {
            $products->loadMissing(
                'group.featureValues',
                'group.featureValues.value',
                'group.featureValues.feature',
                'group.featureValues.value.current',
                'group.featureValues.feature.current'
            );
        }
    }

    /**
     * TODO
     * @param array|null $productIds
     * @param int|null $ignoredId
     * @param int $limit
     * @return Collection
     */
    public static function getByIdsListWithIgnoredOne(?array $productIds = null, int $ignoredId = null, int $limit = 10): Collection
    {
        $productIds = $productIds ?? [];

        $query = Product::query()
            ->whereIn('id', $productIds);

        if ($ignoredId) {
            $query->where('id', '!=', $ignoredId);
        }

        return $query
            ->where('active', true)
            ->latest('available')
            ->limit($limit ?: 10)
            ->get();
    }

    /**
     * Relation to the group.
     *
     * @return BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(ProductGroup::class, 'group_id', 'id');
    }

    /**
     * Relation to the labels list.
     *
     * @return HasManyThrough
     */
    public function labels()
    {
        $relation = $this->hasManyThrough(Label::class, ProductGroupLabel::class,
            'group_id', 'id', 'group_id', 'label_id');
        if (config('app.place') === 'site') {
            $relation->where('active', true);
        }

        return $relation;
    }

    /**
     * Product image configurations class
     *
     * @return string
     */
    protected function imageClass()
    {
        return ProductImage::class;
    }

    /**
     * Register filter model
     *
     * @return string
     */
    public function modelFilter()
    {
        if (config('app.place') === 'site') {
            return $this->provideFilter(SearchFilter::class);
        }

        return $this->provideFilter(ProductsAdminFilter::class);
    }

    /**
     * TODO
     * Information for admin
     *
     * @return string
     */
    public function getFormattedPriceForAdminAttribute(): string
    {
        return format_for_admin($this->price);
    }

    /**
     * TODO
     * Information for client
     *
     * @return string
     */
    public function getFormattedPriceAttribute(): string
    {
        if (Catalog::currenciesLoaded()) {
            return Catalog::currency()->format($this->price);
        }
        return $this->price_for_site;
    }

    /**
     * TODO
     * @return string
     */
    public function getPriceWithCodeAttribute(): string
    {
        if (Catalog::currenciesLoaded()) {
            $numberSymbols = config('db.currencies.number-symbols', 2);
            return number_format(Catalog::currency()->calculate($this->price), $numberSymbols, '.', '')
                . ' ' . config('currency.site.microdata');
        }
        return $this->price_for_site . ' UAH';
    }

    public function getNumberPriceForSiteAttribute()
    {
        return (float) number_format($this->price_for_site, config('db.currencies.number-symbols', 2), '.', '');
    }

    public function getOldPriceWithCodeAttribute(): ?string
    {
        if (!$this->old_price) {
            return null;
        }
        if (Catalog::currenciesLoaded()) {
            $numberSymbols = config('db.currencies.number-symbols', 2);
            return number_format(Catalog::currency()->calculate($this->old_price), $numberSymbols, '.', '')
                . ' ' . config('currency.site.microdata');
        }
        return $this->old_price_for_site . ' UAH';
    }

    /**
     * TODO
     * Price for client
     *
     * @return float
     */
    public function getPriceForSiteAttribute(): float
    {
        if (Catalog::currenciesLoaded()) {
            return Catalog::currency()->calculate($this->price);
        }
        return $this->price;
    }

    /**
     * TODO
     * Information for admin
     *
     * @return string
     */
    public function getFormattedOldPriceForAdminAttribute(): ?string
    {
        if (!$this->old_price) {
            return null;
        }
        if (Catalog::currenciesLoaded()) {
            return Catalog::currency()->formatForAdmin($this->old_price);
        }
        return $this->old_price;
    }

    /**
     * TODO
     * Information for client
     *
     * @return string
     */
    public function getFormattedOldPriceAttribute(): ?string
    {
        if (!$this->old_price_for_site) {
            return null;
        }
        if (Catalog::currenciesLoaded()) {
            return Catalog::currency()->format($this->old_price);
        }
        return $this->old_price_for_site;
    }

    /**
     * TODO
     * Price for client
     *
     * @return int
     */
    public function getOldPriceForSiteAttribute(): float
    {
        if (!$this->old_price) {
            return 0;
        }
        if (Catalog::currenciesLoaded()) {
            return Catalog::currency()->calculate($this->old_price);
        }
        return $this->old_price;
    }

    /**
     * Product url on the site
     *
     * @return string
     */
    public function getSiteLinkAttribute(): string
    {
        return route('site.product', ['slug' => $this->current->slug]);
    }

    /**
     * TODO
     * Searching on the site
     *
     * @param int|null $limit
     * @param array $ignored
     * @param bool $hide_mods
     * @return LengthAwarePaginator|Collection|Product[]
     */
    public static function search(?int $limit = null, array $ignored = [], $hide_mods = false)
    {
        if (!$ignored) {
            $ignored = [];
        }
        $forFilter = request()->only('query', 'order');
        $forFilter['order'] = $forFilter['order'] ?? 'default';
        $productsQuery = Product::active()
            ->whereNotIn('id', $ignored)
            ->filter($forFilter);
        if ($hide_mods) {
            $productsQuery->groupBy('products.group_id');
        }
        $limit = (int)$limit ?: request()->query('per-page');
        $products = $productsQuery->paginate(
            $limit ?: config('db.products.site-per-page', static::LIMIT_PER_PAGE_BY_DEFAULT)
        );

        return $products;
    }

    /**
     * @return HasOneThrough
     */
    public function category(): HasOneThrough
    {
        return $this->hasOneThrough(
            Category::class,
            ProductGroup::class,
            'id',
            'id',
            'group_id',
            'category_id'
        );
    }

    public function categories()
    {
        $category = $this->category;
        $categories = new Collection();
        if (!$category) {
            return $categories;
        }
        $categories->push($category);
        while ((int)$category->parent_id > 0) {
            $category = $category->getParent();
            $categories->push($category);
        }

        return $categories->reverse();
    }
    /**
     * @param int|null $limit
     * @return LengthAwarePaginator
     */
    public static function getList(?int $limit = null): LengthAwarePaginator
    {
        return Product::with('current', 'brand.current', 'category.current', 'images')
            ->filter(request()->all())
            ->latest('id')
            ->paginate($limit ?: config(
                'db.products.per-page',
                Product::LIMIT_PER_PAGE_BY_DEFAULT_ADMIN_PANEL
            ));
    }

    /**
     * @param int $categoryId
     * @return float
     */
    public static function getMinPriceInCategory(int $categoryId): float
    {
        return (float)Product::whereHas('group.otherCategories', function (Builder $builder) use ($categoryId) {
            return $builder->where('category_id', $categoryId);
        })->active(true)->min('price');
    }

    public static function getMinPriceInCategories(array $categories): float
    {
        return (float)Product::whereHas('group', function (Builder $builder) use ($categories) {
            return $builder->whereIn('category_id', $categories);
        })->active(true)->min('price');
    }

    /**
     * @param int $categoryId
     * @return float
     */
    public static function getMaxPriceInCategory(int $categoryId): float
    {
        return (float)Product::whereHas('group.otherCategories', function (Builder $builder) use ($categoryId) {
            return $builder->where('category_id', $categoryId);
        })->active(true)->max('price');
    }

    public static function getMaxPriceInCategories(array $categories): float
    {
        return (float)Product::whereHas('group', function (Builder $builder) use ($categories) {
            return $builder->whereIn('category_id', $categories);
        })->active(true)->max('price');
    }

    /**
     * @return Collection|ProductGroupFeatureValue[]
     */
    public function getFeatureValuesAttribute()
    {
        $collection = new Collection();
        $group = $this->group;
        $added = [];
        $group->featureValues->each(function (ProductGroupFeatureValue $featureValue) use ($collection, $group, &$added) {
            if ($group->feature_id === $featureValue->feature_id && $this->value_id !== $featureValue->value_id) {
                return;
            }
            if (in_array($featureValue->value_id, $added)) {
                return;
            }
            $added[] = $featureValue->value_id;
            $collection->push($featureValue);
        });
        return $collection;
    }

    /**
     * @return array
     */
    public function getFeaturesListAttribute(): array
    {
        $group = $this->group;
        $values = [];
        if ($this->brand && $this->brand->active) {
            $values[trans('products::site.brand')] = [
                Html::link($this->brand->link, $this->brand->current->name),
            ];
        }
        if ($group->feature_id && $this->value_id) {
            $values[$group->feature->current->name] = [$this->value->current->name];
        }
        $featureValues = $group->featureValues;
        $featureValues->load('value', 'value.current', 'feature', 'feature.current');
        $featureValues->each(function (ProductGroupFeatureValue $featureValue) use (&$values, $group) {
            if (
                $group->feature_id && $group->feature_id === $featureValue->feature_id &&
                $this->value_id && $this->value_id !== $featureValue->value_id
            ) {
                return;
            }
            if (isset($values[$featureValue->feature->current->name])) {
                $values[$featureValue->feature->current->name][] = $featureValue->value->current->name;
            } else {
                $values[$featureValue->feature->current->name] = [$featureValue->value->current->name];
            }
        });
        foreach ($values as $featureName => $featureValues) {
            $featureValues = array_unique($featureValues);
            $values[$featureName] = implode(', ', $featureValues);
        }
        return $values;
    }

    /**
     * @return array
     */
    public function getFeaturesListExportAttribute(): array
    {
        $group = $this->group;
        $values = [];
        if ($group->feature_id && $this->value_id) {
            $values[$group->feature->current->name] = [$this->value->current->name];
        }
        $featureValues = $group->featureValues;
        $featureValues->load('value', 'value.current', 'feature', 'feature.current');
        $featureValues->each(function (ProductGroupFeatureValue $featureValue) use (&$values, $group) {
            if (
                $group->feature_id && $group->feature_id === $featureValue->feature_id &&
                $this->value_id && $this->value_id !== $featureValue->value_id
            ) {
                return;
            }
            if (isset($values[$featureValue->feature->current->name])) {
                $values[$featureValue->feature->current->name][] = $featureValue->value->current->name;
            } else {
                $values[$featureValue->feature->current->name] = [$featureValue->value->current->name];
            }
        });
        foreach ($values as $featureName => $featureValues) {
            $featureValues = array_unique($featureValues);
            $values[$featureName] = implode('|', $featureValues);
        }
        return $values;
    }

    /**
     * @return array
     */
    public function getFeatureValuesAsArrayAttribute(): array
    {
        $dictionary = [];
        $this->feature_values->each(function (ProductGroupFeatureValue $featureValue) use (&$dictionary) {
            $dictionary[$featureValue->feature_id] = array_get($dictionary, $featureValue->feature_id, []);
            $dictionary[$featureValue->feature_id][] = $featureValue->value_id;
        });
        return $dictionary;
    }

    /**
     * @return Collection
     */
    public function getTabsAttribute(): Collection
    {
        $tabs = new Collection();
        $tabs->put('main', [
            'name' => __('products::site.all-about-product'),
            'widget' => Widget::show('products::tab-main', $this),
        ]);
        $tabs->put('description', [
            'name' => __('products::site.description'),
            'widget' => Widget::show('products::tab-description', $this),
        ]);
        $tabs->put('features', [
            'name' => __('products::site.features'),
            'widget' => Widget::show('products::tab-features', $this),
        ]);
        $tabs->put('review', [
            'name' => __('products::site.reviews'),
            'widget' => Widget::show('products::tab-reviews', $this),
            'count' => $this->group->comments->count(),
        ]);
        $tabs->put('documents', [
            'name' => __('products::site.documents'),
            'widget' => Widget::show('products::tab-documents', $this),
        ]);
        return $tabs;
    }

    public static function getOne(int $productId)
    {
        return Product::whereId($productId)
            ->active(true)
            ->first();
    }

    public static function getMany(array $productsIds)
    {
        if (!$productsIds) {
            return new Collection();
        }
        return Product::with('current', 'images', 'images.current')
            ->whereIn('id', $productsIds)
            ->active(true)
            ->get();
    }

    /**
     * @return string|null
     */
    public function getMicrodataAttribute(): ?string
    {
        if (!config('db.microdata.product', true)) {
            return null;
        }
        $data = [
            '@context' => 'http://schema.org/',
            '@type' => 'Product',
            'name' => $this->name,
            'image' => $this->preview ? $this->preview->link('small') : '',
        ];
        if (Seo::site()->getDescription()) {
            $data['description'] = Seo::site()->getDescription();
        }
        if ($this->group->brand) {
            $data['brand'] = $this->group->brand->current->name;
        }
        if ($this->vendor_code) {
            $data['sku'] = $this->vendor_code;
        }
        $data['offers'] = [
            '@type' => 'Offer',
            'priceCurrency' => Catalog::currency()->microdataName() ?: 'UAH',
            'price' => number_format($this->price_for_site, config('db.currencies.number-symbols', 2), '.', ''),
            'url' => $this->site_link,
            'availability' => 'http://schema.org/' . (($this->is_available || $this->is_custom_goods) ? 'InStock' : 'OutOfStock'),
            'itemCondition' => 'http://schema.org/NewCondition',
        ];
        if ($this->group->comments && $this->group->comments->isNotEmpty() && $this->group->mark > 0) {
            $data['aggregateRating'] = [
                '@type' => 'AggregateRating',
                'ratingValue' => $this->group->mark,
                'ratingCount' => $this->group->comments->count(),
            ];
        }
        return json_encode($data);
    }

    public function getFBPixelDataAttribute()
    {
        $data = [
            'content_type' => 'product',
            'content_ids' => [(string) $this->id],
            'content_name' => $this->name,
            'content_category' => $this->group->category->current->name,
            'value' => (int) number_format($this->price_for_site, config('db.currencies.number-symbols', 2), '.', ''),
            'currency' => Catalog::currency()->microdataName() ?: 'UAH'
        ];

        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function getMainFeaturesAttribute(): string
    {
        $features = [];
        foreach ($this->feature_values as $productFeatureValue) {
            if ($productFeatureValue->feature_id !== $this->group->feature_id && $productFeatureValue->value && $productFeatureValue->feature->main) {
                $features[$productFeatureValue->feature->current->name] = $features[$productFeatureValue->feature->current->name] ?? [];
                $features[$productFeatureValue->feature->current->name][$productFeatureValue->value_id] = $productFeatureValue->value->current->name;
            }
        }
        $descriptions = [];
        foreach ($features as $featureName => $values) {
            $descriptions[] = '<b>' . $featureName . ':</b> ' . implode(', ', $values);
        }
        return implode('<br />', $descriptions);
    }

    /**
     * @return HasMany
     */
    public function wholesale()
    {
        return $this->hasMany(ProductWholesale::class, 'product_id', 'id')->orderBy('quantity');
    }

    /**
     * @param int $quantity
     * @return float
     */
    public function getPriceForCart(int $quantity): float
    {
        if ($quantity < 2) {
            return $this->price;
        }

        $wholesale = $this->wholesale
            ->where('quantity', '<=', $quantity)
            ->sort(function (ProductWholesale $prev, ProductWholesale $current) {
                return $prev->quantity < $current->quantity || (
                        $prev->id < $current->id && $current->quantity === $prev->quantity
                    );
            })
            ->first();

        if ($wholesale) {
            return $quantity * $wholesale->price;
        }

        return $quantity * $this->price;
    }

    /**
     * Set current product as main product in group
     */
    public function setAsMain(): void
    {
        if ($this->group) {
            $this->group->products->each(function (Product $product) {
                if ($product->is_main) {
                    $product->update([
                        'is_main' => false,
                    ]);
                }
            });
        }
        $this->update([
            'is_main' => true,
        ]);
    }

    public static function getCategoriesIdsByProductsIds(array $ids): array
    {
        $categoryIds = [];
        ProductGroup::query()
            ->select('category_id')
            ->whereHas(
                'products',
                function (Builder $query) use ($ids) {
                    $query->whereIn('id', $ids);
                }
            )
            ->get()
            ->each(
                function (ProductGroup $group) use (&$categoryIds) {
                    $categoryIds[] = $group->category_id;
                }
            )
            ->toArray();

        return $categoryIds;
    }

    public static function getArrayForExportProm($category_ids = null): array
    {
        $groups = ProductGroup::with(
            'products', 'products.current',
            'otherCategories',
            'brand'
        );
        if (!empty($category_ids)){
            $groups = $groups->whereIn('category_id', $category_ids);
        }
        $groups = $groups->oldest('id')->get()->reverse();
        $export = [];
        foreach ($groups as $group) {
            $arrayGroups = [];
            $productFirst = $group->products->first();

            $wholePriceArray = [];
            $wholeQuantityArray = [];
            if ($productFirst->wholesale->count()) {
                $wholesale = 'u';
                foreach ($productFirst->wholesale as $whole) {
                    $wholePriceArray[] = $whole->price;
                    $wholeQuantityArray[] = $whole->quantity;
                }
            } else {
                $wholesale = 'r';
            }
            $images = '';
            $imagesArray = [];
            if ($productFirst->gallery()->count()) {
                foreach ($productFirst->gallery() as $image) {
                    if ($image->isImageExists('big')) {
                        $imagesArray[] = $image->link('big');
                    }
                }
                $images = implode(', ', $imagesArray);
            }
            $labels = [];
            $labelsString = '';
            if ($productFirst->labels->count()) {
                foreach ($productFirst->labels as $label) {
                    $labels[] = $label->current->name;
                }
                $labelsString = implode(', ', $labels);
            }

            $arrayGroups['Код_товара'] = $productFirst->vendor_code;
            $arrayGroups['Название_позиции'] = isset($group->current) ? $group->current->name : '';
            $arrayGroups['Описание'] = isset($group->current) ? $group->current->text : '';
            $arrayGroups['Тип_товара'] = $wholesale;
            $arrayGroups['Цена'] = $productFirst->price;
            $arrayGroups['Валюта'] = Currency::getDefaultInAdminPanel()->microdata;
            $arrayGroups['Оптовая_цена'] = sizeof($wholePriceArray) ? implode(';', $wholePriceArray) : '';
            $arrayGroups['Минимальный_заказ_опт'] = sizeof($wholeQuantityArray) ? implode(';', $wholeQuantityArray) : '';
            $arrayGroups['Ссылка_изображения'] = $images;
            $arrayGroups['Наличие'] = $productFirst->available ? '+' : '-';
            $arrayGroups['Производитель'] = isset($group->brand->current) ? $group->brand->current->name : '';
            $arrayGroups['Номер_группы'] = $group->category_id;
            $arrayGroups['Идентификатор_подраздела'] = $group->category_id;
            $arrayGroups['Название_группы'] = isset($group->category->current->name) ? $group->category->current->name : '';
            $arrayGroups['Идентификатор_товара'] = $group->id;
            $arrayGroups['Уникальный_идентификатор'] = $group->id;
            $arrayGroups['Продукт_на_сайте'] = $productFirst->site_link;
            $arrayGroups['Метки'] = $labelsString;
            $arrayGroups['ID_группы_разновидностей'] = $group->id;

            $export[] = $arrayGroups;

            if ($group->products->count()) {
                foreach ($group->products as $product) {
                    $array = [];
                    $wholePriceArray = [];
                    $wholeQuantityArray = [];
                    if ($product->wholesale->count()) {
                        $wholesale = 'u';
                        foreach ($product->wholesale as $whole) {
                            $wholePriceArray[] = $whole->price;
                            $wholeQuantityArray[] = $whole->quantity;
                        }
                    } else {
                        $wholesale = 'r';
                    }
                    $images = '';
                    $imagesArray = [];
                    if ($product->gallery()->count()) {
                        foreach ($product->gallery() as $image) {
                            if ($image->isImageExists('big')) {
                                $imagesArray[] = $image->link('big');
                            }
                        }
                        $images = implode(', ', $imagesArray);
                    }
                    $labels = [];
                    $labelsString = '';
                    if ($product->labels->count()) {
                        foreach ($product->labels as $label) {
                            $labels[] = $label->current->name;
                        }
                        $labelsString = implode(', ', $labels);
                    }
                    $specs = [];
                    if (!empty($product->features_list_export)) {
                        foreach ($product->features_list_export as $feature_name => $feature) {
                            $specs[]['Название_Характеристики'] = $feature_name;
                            $specs[]['Измерение_Характеристики'] = '';
                            $specs[]['Значение_Характеристики'] = $feature;
                        }
                        if (static::$specs_feature_export < count($product->features_list_export)) {
                            static::$specs_feature_export = count($product->features_list_export);
                        }
                    }
                    $array['Код_товара'] = $product->vendor_code;
                    $array['Название_позиции'] = isset($product->current) ? $product->current->name : $group->current->name;
                    $array['Описание'] = isset($group->current) ? $group->current->text : '';
                    $array['Тип_товара'] = $wholesale;
                    $array['Цена'] = $product->price;
                    $array['Валюта'] = Currency::getDefaultInAdminPanel()->microdata;
                    $array['Оптовая_цена'] = sizeof($wholePriceArray) ? implode(';', $wholePriceArray) : '';
                    $array['Минимальный_заказ_опт'] = sizeof($wholeQuantityArray) ? implode(';', $wholeQuantityArray) : '';
                    $array['Ссылка_изображения'] = $images;
                    $array['Наличие'] = $product->available ? '+' : '-';
                    $array['Производитель'] = isset($product->brand->current) ? $product->brand->current->name : '';
                    $array['Номер_группы'] = $group->category_id;
                    $array['Идентификатор_подраздела'] = $group->category_id;
                    $array['Название_группы'] = isset($group->category->current) ? $group->category->current->name : '';
                    $array['Идентификатор_товара'] = $product->id;
                    $array['Уникальный_идентификатор'] = $product->id . '-' . $group->id;
                    $array['Продукт_на_сайте'] = $product->site_link;
                    $array['Метки'] = $labelsString;
                    $array['ID_группы_разновидностей'] = $group->id;
                    if (count($specs)) {
                        $array[] = $specs;
                    }
                    $export[] = $array;

                    ProductsGroupsRemote::updateOrCreate([
                        'group_id' => $group->id,
                        'product_id' => $product->id,
                        'system' => 'locotrade ',
                    ], [
                        'remote_id' => $product->id,
                    ]);
                }
            }
        }

        return $export;
    }

    public static function getArrayForExportPromOld(): array
    {
        $products = Product::with('current', 'group', 'group.otherCategories', 'group.brand')
            ->oldest('id')
            ->get()->reverse();
        $export = [];
        foreach ($products as $product) {
            $array = [];
            $wholePriceArray = [];
            $wholeQuantityArray = [];
            if ($product->wholesale->count()) {
                $wholesale = 'u';
                foreach ($product->wholesale as $whole) {
                    $wholePriceArray[] = $whole->price;
                    $wholeQuantityArray[] = $whole->quantity;
                }
            } else {
                $wholesale = 'r';
            }
            $images = '';
            $imagesArray = [];
            if ($product->gallery()->count()) {
                foreach ($product->gallery() as $image) {
                    if ($image->isImageExists('big')) {
                        $imagesArray[] = $image->link('big');
                    }
                }
                $images = implode(', ', $imagesArray);
            }
            $labels = [];
            $labelsString = '';
            if ($product->labels->count()) {
                foreach ($product->labels as $label) {
                    $labels[] = $label->current->name;
                }
                $labelsString = implode(', ', $labels);
            }
            $specs = [];
            if (!empty($product->features_list_export)) {
                foreach ($product->features_list_export as $feature_name => $feature) {
                    $specs[]['Название_Характеристики'] = $feature_name;
                    $specs[]['Измерение_Характеристики'] = '';
                    $specs[]['Значение_Характеристики'] = $feature;
                }
                if (static::$specs_feature_export < count($product->features_list_export)) {
                    static::$specs_feature_export = count($product->features_list_export);
                }
            }
            $array['Код_товара'] = $product->vendor_code;
            $array['Название_позиции'] = $product->current->name ? $product->current->name : $product->group->current->name;
            $array['Описание'] = $product->group ? $product->group->current->text : null;
            $array['Тип_товара'] = $wholesale;
            $array['Цена'] = $product->price;
            $array['Валюта'] = Currency::getDefaultInAdminPanel()->microdata;
            $array['Оптовая_цена'] = sizeof($wholePriceArray) ? implode(';', $wholePriceArray) : '';
            $array['Минимальный_заказ_опт'] = sizeof($wholeQuantityArray) ? implode(';', $wholeQuantityArray) : '';
            $array['Ссылка_изображения'] = $images;
            $array['Наличие'] = $product->available ? '+' : '-';
            $array['Производитель'] = $product->brand ? $product->brand->current->name : '';
            $array['Номер_группы'] = $product->group ? $product->group->category_id : '';
            $array['Идентификатор_подраздела'] = $product->group ? $product->group->category_id : '';
            $array['Название_группы'] = $product->group && $product->group->category ? $product->group->category->current->name : '';
            $array['Идентификатор_товара'] = $product->id;
            $array['Уникальный_идентификатор'] = $product->id;
            $array['Продукт_на_сайте'] = $product->site_link;
            $array['Метки'] = $labelsString;
            $array['ID_группы_разновидностей'] = $product->group->id;
            if (count($specs)) {
                $array[] = $specs;
            }
            $export[] = $array;

            ProductsGroupsRemote::updateOrCreate([
                'group_id' => $product->group->id,
                'product_id' => $product->id,
                'system' => 'export',
            ], [
                'remote_id' => $product->id,
            ]);
        }
        return $export;
    }

    /**
     * @return string
     */
    public function getSortedProductLinkAttribute(): string
    {
        return route('admin.product.set-position-value', $this->id);
    }

    public function isActive(): bool
    {
        return $this->group->isActive()
            && $this->active;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getOldPrice()
    {
        return $this->old_price ?? null;
    }

    public function ecommId(): string
    {
        return $this->id;
    }

    public function ecommName(): string
    {
        return $this->name;
    }

    public function ecommPrice(): float
    {
        return $this->number_price_for_site;
    }

    public function ecommBrand(): ?string
    {
        return $this->brand ? $this->brand->current->name : null;
    }

    public function ecommCategory(): string
    {
        return $this->category->current->name;
    }

    public function getEcommDataAttribute()
    {
        return [
            'id' => $this->ecommId(),
            'name' => $this->ecommName(),
            'price' => $this->ecommPrice(),
            'brand' => $this->ecommBrand(),
            'category' => $this->ecommCategory(),
            'link' => $this->site_link
        ];
    }


    public function getName()
    {
        return $this->name ?? $this->current->name;
    }

}
