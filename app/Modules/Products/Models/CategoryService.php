<?php

namespace App\Modules\Products\Models;

use App\Modules\Categories\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Service
 * @package App\Modules\Products\CategoryService
 * @property int $id
 * @property int $category_id
 * @property int $service_id
 * @property-read Category $category
 * @property-read Service $service
 * @method static Builder|Service query()
 * @method static Builder|Service whereId($value)
 * @method static Builder|Service whereCreatedAt($value)
 * @method static Builder|Service whereUpdatedAt($value)
 * @method static Builder|Service whereCategoryId($value)
 * @method static Builder|Service whereServiceId($value)
 * @mixin \Eloquent
 */
class CategoryService extends Model
{
    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'category_services';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['category_id', 'service_id'];

    /**
     * Relation to the category.
     *
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * Relation to the service.
     *
     * @return BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }
}
