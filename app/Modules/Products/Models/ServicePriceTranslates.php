<?php

namespace App\Modules\Products\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceTranslates
 * @package App\Modules\Products\Models
 * @property int $id
 * @property int $row_id
 * @property string $language
 * @property string $name
 * @method static Builder|ServiceTranslates query()
 * @method static Builder|ServiceTranslates whereId($value)
 * @method static Builder|ServiceTranslates whereRowId($value)
 * @method static Builder|ServiceTranslates whereLanguage($value)
 * @method static Builder|ServiceTranslates whereName($value)
 */
class ServicePriceTranslates extends Model
{
    use ModelTranslates;

    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'service_prices_translates';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['row_id', 'language', 'name'];
}
