<?php

namespace App\Modules\Products\Models;

use App\Traits\ModelMain;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Service
 * @package App\Modules\Products\Models
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property bool $active
 *
 * @property-read ServiceTranslates $current
 * @property-read Collection|ServicePrice[] $options
 * @method static Builder|Service query()
 * @method static Builder|Service whereId($value)
 * @method static Builder|Service whereCreatedAt($value)
 * @method static Builder|Service whereUpdatedAt($value)
 * @method static Builder|Service whereActive($value)
 * @mixin \Eloquent
 */
class Service extends Model
{
    use ModelMain;

    /**
     * {@inheritDoc}
     */
    protected $table = 'services';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['active'];
    /**
     * {@inheritDoc}
     */
    protected $casts = ['active' => 'boolean'];

    /**
     * Relation to the prices.
     *
     * @return HasMany
     */
    public function options()
    {
        return $this->hasMany(ServicePrice::class, 'service_id', 'id');
    }
}
