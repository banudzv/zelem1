<?php

namespace App\Modules\Products\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ServiceTranslates
 * @package App\Modules\Products\Models
 * @property int $id
 * @property int $row_id
 * @property string $language
 * @property string $name
 * @property string $slug
 * @property string $teaser
 * @property string $text
 * @property string $h1
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $url
 * @property-read Collection|ServicePrice[] $options
 * @method static Builder|ServiceTranslates query()
 * @method static Builder|ServiceTranslates whereId($value)
 * @method static Builder|ServiceTranslates whereRowId($value)
 * @method static Builder|ServiceTranslates whereLanguage($value)
 * @method static Builder|ServiceTranslates whereName($value)
 * @method static Builder|ServiceTranslates whereSlug($value)
 * @method static Builder|ServiceTranslates whereTeaser($value)
 * @method static Builder|ServiceTranslates whereText($value)
 * @method static Builder|ServiceTranslates whereH1($value)
 * @method static Builder|ServiceTranslates whereTitle($value)
 * @method static Builder|ServiceTranslates whereKeywords($value)
 * @method static Builder|ServiceTranslates whereDescription($value)
 */
class ServiceTranslates extends Model
{
    use ModelTranslates;

    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'services_translates';
    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'row_id',
        'language',
        'name',
        'slug',
        'teaser',
        'text',
        'h1',
        'title',
        'keywords',
        'description',
        'url'
    ];

    /**
     * Relation to prices.
     *
     * @return HasMany
     */
    public function options()
    {
        return $this->hasMany(ServicePrice::class, 'service_id', 'row_id');
    }
}
