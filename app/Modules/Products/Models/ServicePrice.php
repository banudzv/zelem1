<?php

namespace App\Modules\Products\Models;

use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class ServicePrice
 * @package App\Modules\Products\Models
 * @property int $id
 * @property int $service_id
 * @property int $price
 * @property int $position
 * @property-read Service $service
 * @method static Builder|ServicePrice query()
 * @method static Builder|ServicePrice whereId($value)
 * @method static Builder|ServicePrice whereServiceId($value)
 * @method static Builder|ServicePrice wherePrice($value)
 * @method static Builder|ServicePrice wherePosition($value)
 * @mixin \Eloquent
 */
class ServicePrice extends Model
{
    use ModelMain;

    /**
     * {@inheritDoc}
     */
    public $timestamps = false;
    /**
     * {@inheritDoc}
     */
    protected $table = 'service_prices';
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['service_id', 'price', 'position'];

    /**
     * Relation to the service.
     *
     * @return BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }
}
