<?php

namespace App\Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;
use Catalog;


/**
 * App\Modules\Products\Models\ProductWholesale
 *
 * @property int $id
 * @property int $product_id
 * @property int $quantity
 * @property float $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Products\Models\ProductWholesale whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read string $formatted_price
 * @property-read float $price_for_site
 */
class ProductWholesale extends Model
{
    protected $table = 'products_wholesale';

    protected $fillable = ['product_id', 'quantity', 'price'];

    /**
     * @return string
     */
    public function getFormattedPriceAttribute(): string
    {
        if (Catalog::currenciesLoaded()) {
            return Catalog::currency()->format($this->price);
        }
        return $this->price_for_site;
    }

    /**
     * @return float
     */
    public function getPriceForSiteAttribute(): float
    {
        if (Catalog::currenciesLoaded()) {
            return Catalog::currency()->calculate($this->price);
        }
        return $this->price;
    }
}
