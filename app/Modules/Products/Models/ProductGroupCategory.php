<?php

namespace App\Modules\Products\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Modules\Products\Models\ProductGroupCategory
 *
 * @property int $id
 * @property int $category_id
 * @property int $group_id
 * @method static Builder|ProductGroupCategory newModelQuery()
 * @method static Builder|ProductGroupCategory newQuery()
 * @method static Builder|ProductGroupCategory query()
 * @method static Builder|ProductGroupCategory whereCategoryId($value)
 * @method static Builder|ProductGroupCategory whereGroupId($value)
 * @method static Builder|ProductGroupCategory whereId($value)
 * @mixin Eloquent
 */
class ProductGroupCategory extends Model
{
    public const TABLE_NAME = 'products_groups_categories';

    public $timestamps = false;

    protected $table = self::TABLE_NAME;

    protected $fillable = ['group_id', 'category_id'];
}
