<?php

namespace App\Modules\Products\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use App\Modules\Products\Models\ProductGroupLabel;

/**
 * App\Modules\Products\Models\ProductGroupLabel
 *
 * @property int $id
 * @property int $group_id
 * @property int $label_id
 * @method static Builder|ProductGroupLabel newModelQuery()
 * @method static Builder|ProductGroupLabel newQuery()
 * @method static Builder|ProductGroupLabel query()
 * @method static Builder|ProductGroupLabel whereGroupId($value)
 * @method static Builder|ProductGroupLabel whereId($value)
 * @method static Builder|ProductGroupLabel whereLabelId($value)
 * @mixin \Eloquent
 */
class ProductGroupLabel extends Model
{
    public $timestamps = false;

    protected $table = 'products_groups_labels';

    protected $fillable = ['group_id', 'label_id'];

    /**
     * @param int $groupId
     * @return Collection|ProductGroupLabel[]
     */
    public static function getRelationsForProduct(int $groupId): Collection
    {
        return ProductGroupLabel::whereGroupId($groupId)->latest('id')->get();
    }
}
