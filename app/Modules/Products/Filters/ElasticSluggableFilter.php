<?php

namespace App\Modules\Products\Filters;

class ElasticSluggableFilter extends ElasticProductFilter
{

    protected function filter(array $filter): void
    {
        if (isset($filter['brand'])) {
            $brandSlugs = explode(',', $filter['brand']);

            if (count($brandSlugs)) {
                $this->getBuilder()->whereIn('brand_slugs', $brandSlugs);
            }

            unset($filter['brand']);
        }

        if (isset($filter['category'])) {
            $categorySlugs = explode(',', $filter['category']);

            if ($categorySlugs) {
                $this->getBuilder()->whereIn('category_slugs', $categorySlugs);
            }

            unset($filter['category']);
        }

        foreach ($filter as $feature => $valueSlugs) {
            $valueSlugs = explode(',', $valueSlugs);
            if (count($valueSlugs)) {
                $this->getBuilder()->whereIn('feature_value_slugs', $valueSlugs);
            }
        }
    }

}