<?php

namespace App\Modules\Products\Filters;

use App\Modules\Categories\Models\CategoryTranslates;
use App\Traits\WhereSlugsTrait;

class EloquentProductSluggableFilter extends Filter
{
    use WhereSlugsTrait;

    public function filter(array $filters)
    {
        if (isset($filters['brand'])) {
            $brands = explode(',', $filters['brand']);
            if (count($brands)) {
                $this->whereHas('brandTranslates', $this->whereSlugs($brands));
            }

            unset($filters['brand']);
        }

        if (isset($filters['category'])) {
            $categories = explode(',', $filters['category']);
            if (count($categories)) {
                $subQuery = CategoryTranslates::query()
                    ->select('row_id')
                    ->where($this->whereSlugs($categories))
                    ->getQuery();

                $this->whereIn('category_id', $subQuery);
            }

            unset($filters['category']);
        }

        foreach ($filters as $key => $value) {
            $values = explode(',', $value);
            if (count($values)) {
                $this->whereHas('same.valueTranslates', $this->whereSlugs($values));
            }
        }
    }

    public function categoryIds(array $categoryIds): void
    {
        $this->whereIn('category_id', $categoryIds);
    }

    public function brandIds(array $brands)
    {
        $this->whereIn('brand_id', $brands);
    }
}