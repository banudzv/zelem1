<?php

namespace App\Modules\Products\Filters;

use App\Modules\Products\Filters\TextSearchBuilders\TextSearchBuilderConfigurator;
use App\Modules\Search\Filters\AbstractElasticFilter;
use Catalog;
use Lang;

class ElasticProductFilter extends AbstractElasticFilter
{

    protected function price(string $price): void
    {
        [$priceMin, $priceMax] = explode('-', $price);

        $priceMin = (float)$priceMin;
        $priceMax = (float)$priceMax;

        if (Catalog::currenciesLoaded()) {
            $priceMin = Catalog::currency()->calculateBack($priceMin);
            $priceMax = Catalog::currency()->calculateBack($priceMax);
        }

        $this->getBuilder()->whereBetween('price', [$priceMin, $priceMax]);
    }

    protected function categoryIds(array $categoryIds): void
    {
        $this->getBuilder()->whereIn('category_ids', $categoryIds);
    }

    protected function brandIds(array $brandIds): void
    {
        $this->getBuilder()->whereIn('brand_id', $brandIds);
    }

    protected function order(string $order): void
    {
        $orders = $this->getAllowedOrders();

        if (isset($orders[$order])) {
            $this->getBuilder()->orderBy(...$orders[$order]);
            return;
        }

        $this->getBuilder()->orderBy('position', 'desc');
    }

    protected function getAllowedOrders(): array
    {
        $lang = Lang::getLocale();

        return [
            'price-asc' => ['price', 'asc'],
            'price-desc' => ['price', 'desc'],
            'name-asc' => ['name_' . $lang, 'asc'],
            'name-desc' => ['name_' . $lang, 'desc'],
        ];
    }

    protected function car(array $car): void
    {
        if (isset($car['vendor'])) {
            $this->getBuilder()->whereIn('car_vendor_ids', [(int)$car['vendor']]);
        }

        if (isset($car['model'])) {
            $this->getBuilder()->where('car_model_ids', $car['model']);
        }

        if (isset($car['volume'])) {
            $this->getBuilder()->where('car_volume_ids', $car['volume']);
        }

        if (isset($car['power'])) {
            $this->getBuilder()->where('car_power_ids', $car['power']);
        }

        if (isset($car['number'])) {
            $this->getBuilder()->where('car_engine_numbers', $car['number']);
        }
    }

    protected function filter(array $filter): void
    {
        if (isset($filter['brand']) && count($filter['brand'])) {
            $this->getBuilder()->whereIn('brand_id', $filter['brand']);
            unset($filter['brand']);
        }

        foreach ($filter as $featureId => $valueIds) {
            $this->getBuilder()->whereIn('feature_value_ids', $valueIds);
        }
    }

    protected function query(string $query): void
    {
        $builder = $this->getBuilder();

        (new TextSearchBuilderConfigurator($builder, $query))->handle();

        $builder->minScore((float)config('db.search.min-cost'));
    }

}