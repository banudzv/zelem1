<?php

namespace App\Modules\Products\Filters;

use App\Components\Form\ObjectValues\ModelForSelect;
use App\Exceptions\WrongParametersException;
use App\Modules\Brands\Models\Brand;
use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\ProductGroup;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

class ProductGroupAdminFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @return string
     * @throws WrongParametersException
     */
    public static function showFilter()
    {
        $form = Form::create();
        $form->fieldSetForFilter()->add(
            Input::create('name')->setValue(request('name'))
                ->addClassesToDiv('col-md-3'),
            Input::create('vendor')->setValue(request('vendor'))
                ->setLabel('products::general.attributes.vendor_code')
                ->addClassesToDiv('col-md-3'),
            Select::create('category')
                ->add(Category::getDictionaryForSelects())
                ->addClassesToDiv('col-md-2')
                ->setPlaceholder(__('global.all'))
                ->setValue(request('category')),
            Select::create('brand')
                ->addClassesToDiv('col-md-2')
                ->model(
                    ModelForSelect::make(
                        Brand::with('current')->get()
                    )->setValueFieldName('current.name')
                )
                ->setPlaceholder(__('global.all'))
                ->setLabel('validation.attributes.brand_id')
                ->setValue(request()->query('brand')),
            Select::create('active')
                ->add(
                    [
                        '0' => __('global.unpublished'),
                        '1' => __('global.published'),
                    ]
                )
                ->setValue(request('active'))
                ->addClassesToDiv('col-md-2')
                ->setPlaceholder(__('global.all'))
        );
        return $form->renderAsFilter();
    }

    public function query(string $query)
    {
        return $this->where(
            function (Builder $db) use ($query) {
                $db->whereHas(
                    'products.current',
                    function (Builder $db) use ($query) {
                        return $db
                            ->where('name', 'like', "%$query%")
                            ->orWhere('vendor_code', 'like', "%$query%");
                    }
                )->orWhereHas(
                    'current',
                    function (Builder $db) use ($query) {
                        return $db->where('name', 'like', "%$query%");
                    }
                );
            }
        );
    }

    public function name(string $name)
    {
        return $this->where(
            function (Builder $query) use ($name) {
                $query->whereHas(
                    'current',
                    function (Builder $query) use ($name) {
                        return $query->where('name', 'like', "%$name%");
                    }
                )->orWhereHas(
                    'products.current',
                    function (Builder $query) use ($name) {
                        return $query->where('name', 'like', "%$name%");
                    }
                );
            }
        );
    }

    public function category(string $categoryId)
    {
        $categories = Category::getAllChildrenIds((int)$categoryId);
        $categories[] = (int)$categoryId;

        return $this->whereIn('category_id', $categories);
    }

    public function vendor(string $vendor)
    {
        return $this->whereHas(
            'products',
            function (Builder $query) use ($vendor) {
                return $query->where('vendor_code', 'like', "%$vendor%");
            }
        );
    }

    public function brand(string $brandId)
    {
        return $this->where('brand_id', $brandId);
    }

    public function active(string $active)
    {
        return $this->where(
            function (Builder $query) use ($active) {
                /** @var ProductGroup $query */
                return $query->whereActive((bool)$active);
            }
        );
    }
}
