<?php

namespace App\Modules\Products\Filters;

use App\Components\Form\ObjectValues\ModelForSelect;
use App\Exceptions\WrongParametersException;
use App\Modules\Brands\Models\Brand;
use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\Product;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use CustomForm\Text;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Widget;

class ProductsAdminFilter extends ModelFilter
{
    /**
     * Generate form view
     *
     * @return string
     * @throws WrongParametersException
     */
    public static function showFilter()
    {
        $form = Form::create();
        $form->fieldSetForFilter()->add(
            Input::create('name')->setValue(request('name'))
                ->addClassesToDiv('col-md-3'),
            Input::create('vendorCode')->setValue(request('vendorCode'))
                ->setLabel('products::general.attributes.vendor_code')
                ->addClassesToDiv('col-md-3'),
            Select::create('category')
                ->add(Category::getDictionaryForSelects())
                ->addClassesToDiv('col-md-2')
                ->setPlaceholder(__('global.all'))
                ->setValue(request('category')),
            Select::create('brand')
                ->addClassesToDiv('col-md-2')
                ->model(
                    ModelForSelect::make(
                        Brand::with('current')->get()
                    )->setValueFieldName('current.name')
                )
                ->setPlaceholder(__('global.all'))
                ->setLabel('validation.attributes.brand_id')
                ->setValue(request()->query('brand')),
            Select::create('active')
                ->add(
                    [
                        '0' => __('global.unpublished'),
                        '1' => __('global.published'),
                    ]
                )
                ->setValue(request('active'))
                ->addClassesToDiv('col-md-2')
                ->setPlaceholder(__('global.all')),
            Text::create()->setDefaultValue(
                Widget::show('categories::select-for-filter')
            ),
            Text::create()->setDefaultValue(
                Widget::show('brands::select-for-filter')
            )
        );
        return $form->renderAsFilter();
    }

    public function query(string $query)
    {
        $query = Str::lower($query);
        return $this->where(
            function (Builder $db) use ($query) {
                $db->whereHas(
                    'current',
                    function (Builder $db) use ($query) {
                        return $db
                            ->whereRaw('LOWER(vendor_code) LIKE ?', ["%$query%"])
                            ->orWhereRaw('LOWER(name) LIKE ?', ["%$query%"]);
                    }
                )->orWhereHas(
                    'group.current',
                    function (Builder $db) use ($query) {
                        return $db->orWhereRaw('LOWER(name) LIKE ?', ["%$query%"]);
                    }
                );
            }
        );
    }

    public function name(string $name)
    {
        $name = Str::lower($name);
        return $this->related(
            'current',
            function (Builder $query) use ($name) {
                return $query->whereRaw('LOWER(name) LIKE ?', ["%$name%"]);
            }
        );
    }

    public function category(string $categoryId)
    {
        return $this
            ->orWhereHas(
                'group.category',
                function (Builder $builder) use ($categoryId) {
                    $builder->where('category_id', $categoryId);
                }
            )
            ->orWhereHas(
                'group.otherCategories',
                function (Builder $builder) use ($categoryId) {
                    $builder->where('category_id', $categoryId);
                }
            );
    }

    public function vendorCode(string $vendorCode)
    {
        return $this->where('products.vendor_code', '=', $vendorCode);
    }

    public function brand(string $brandId)
    {
        return $this->where('brand_id', '=', $brandId);
    }

    public function active(string $active)
    {
        return $this->where(
            function (Builder $query) use ($active) {
                /** @var Product $query */
                return $query->whereActive((bool)$active);
            }
        );
    }
}
