<?php

namespace App\Modules\Products\Filters\TextSearchBuilders;

use App\Modules\Search\Models\SearchBuilder;

abstract class AbstractWordSearchBuilder implements WordSearchBuilder
{
    /**
     * @var SearchBuilder
     */
    protected $builder;

    /**
     * @var string
     */
    protected $term;

    public function __construct(SearchBuilder $builder, string $query)
    {
        $this->builder = $builder;
        $this->term = $query;
    }

    abstract public function build(): void;

    protected function getBuilder(): SearchBuilder
    {
        return $this->builder;
    }

    protected function getTerm(): string
    {
        return $this->term;
    }

    protected function explodeTerm(string $term): array
    {
        return explode(' ', $term);
    }

    protected function cleanTerm(string $word): string
    {
        return str_replace(['-', '.',], '', $word);
    }

}