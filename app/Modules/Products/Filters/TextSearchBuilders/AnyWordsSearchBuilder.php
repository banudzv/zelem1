<?php

namespace App\Modules\Products\Filters\TextSearchBuilders;

class AnyWordsSearchBuilder extends AbstractWordSearchBuilder
{

    public function build(): void
    {
        $builder = $this->getBuilder();
        $term = $this->getTerm();

        $cleanTerm = $this->cleanTerm($term);
        $clearFullString = str_clear($term);

        $builder
            ->orTerm('name.keyword', $term, 10)
            ->orTerm('name.keyword', $cleanTerm, 9)
            ->orWildcard('name.keyword', "*$cleanTerm*", 8)
            ->orTerm('car_engine_numbers', $clearFullString, 6)//
        ;
    }
}