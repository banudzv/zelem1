<?php

namespace App\Modules\Products\Filters\TextSearchBuilders;

use App\Modules\Search\Models\SearchBuilder;
use Illuminate\Support\Collection;

class TextSearchBuilderConfigurator
{
    /**
     * @var WordSearchBuilder
     */
    private $builder;

    /**
     * @var string
     */
    private $query;

    public function __construct(SearchBuilder $builder, string $query)
    {
        $this->builder = $builder;
        $this->query = text_clear($query);
    }

    public function handle()
    {
        $this->getSearchBuilderForCurrentNumberOfWords()->build();
        $this->getDefaultWordsBuilder()->build();
    }

    protected function getSearchBuilderForCurrentNumberOfWords(): WordSearchBuilder
    {
        $query = $this->query;
        $searchBuilder = $this->builder;

        $words = $this->convertQueryAsCollection($query);

        switch ($words->count()) {
            case 1:
                return new OneWordSearchBuilder($searchBuilder, $query);
            case 2:
                return new TwoWordsSearchBuilder($searchBuilder, $query);
            case 3:
                return new ThreeWordsSearchBuilder($searchBuilder, $query);
        }

        return new FourWordsSearchBuilder($searchBuilder, $query);
    }

    protected function convertQueryAsCollection(string $query): Collection
    {
        return Collection::make(
            explode(' ', $query)
        );
    }

    protected function getDefaultWordsBuilder(): AnyWordsSearchBuilder
    {
        return new AnyWordsSearchBuilder($this->builder, $this->query);
    }

    protected function getBuilder(): SearchBuilder
    {
        return $this->builder;
    }
}