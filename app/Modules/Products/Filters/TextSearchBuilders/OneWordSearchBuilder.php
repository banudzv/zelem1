<?php

namespace App\Modules\Products\Filters\TextSearchBuilders;

class OneWordSearchBuilder extends AbstractWordSearchBuilder
{

    public function build(): void
    {
        $builder = $this->getBuilder();
        $term = $this->getTerm();

        $clearFullString = str_clear($term);

        $builder
            ->orWildcard('brand_names', "*$term*", 7)
            ->orTerm('feature_value_names.keyword', "$clearFullString", 6)
            ->orTerm('car_engine_names.keyword', $term, 3)//
            ->orTerm('vendor_code', $clearFullString, 10)
            ->orWildcard('vendor_code', "*$term?", 9)
            ->orWildcard('name.keyword', "*$term?", 6)
            ->orTerm('car_vendor_names', $term, 6)
            ->orWildcard('car_vendor_names', "*$term*", 7)
            ->orTerm('car_volume_names', $term, 6)
            ->orWildcard('car_volume_names', "*$term*", 5)
            ->orTerm('car_model_names', $term, 6)
            ->orWildcard('car_model_names', "*$term*", 5)
            ->orTerm('car_power_names', $term, 6)
            ->orWildcard('car_power_names', "*$term*", 5)
            ->orTerm('category_names', $term, 6)
            ->orTerm('category_search_keywords', $term, 6)
            ->orSimpleQueryString(
                [
                    'cars^0.5',
                    'feature_value_names.keyword',
                    'car_power_names^0.5',
                    'search_keywords^6',
                    'category_names^6',
                    'category_search_keywords^8',
                    'name^0.2',
                ],
                $term,
                [
                    'default_operator' => 'and',
                    'minimum_should_match' => '100%',
                    'analyze_wildcard' => true,
                ]
            )//
        ;
    }
}