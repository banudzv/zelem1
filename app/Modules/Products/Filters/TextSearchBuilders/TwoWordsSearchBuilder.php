<?php

namespace App\Modules\Products\Filters\TextSearchBuilders;

class TwoWordsSearchBuilder extends AbstractWordSearchBuilder
{

    public function build(): void
    {
        $builder = $this->getBuilder();
        $term = $this->getTerm();

        $builder
            ->orSimpleQueryString(
                [
                    'vendor_code^1.2',
                    'car_vendor_names',
                    'car_model_names',
                    'feature_value_slugs^0.9',
                    'category_names^2.5',
                    'category_search_keywords^2.5',
                    'brand_names^2.5',
                    'brand_search_keywords^2.5',
                    'car_engine_names^2',
                ],
                $term,
                [
                    'default_operator' => 'and',
                    'minimum_should_match' => '100%',
                    'analyze_wildcard' => true,
                ]
            )
//            ->orMultiMatch(
//                [
//                    'brand_names^0.3',
//                    'category_names^0.6',
//                ],
//                $term,
//                'cross_fields',
//                [
//                    'operator' => 'and',
//                    'minimum_should_match' => '100%',
//                    'tie_breaker' => 0.5,
//                ]
//            )
//            ->orMultiMatch(
//                [
//                    'car_vendor_names^0.3',
//                    'car_model_names^0.3',
//                ],
//                $term,
//                'cross_fields',
//                [
//                    'operator' => 'and',
//                    'minimum_should_match' => '100%',
//                    'tie_breaker' => 0.01,
//                ]
//            )
//            ->orWildcard('name.keyword', "*$term*", 7)
            ->orTerm('cars.keyword', $term, 5)//
        ;

        foreach ($this->explodeTerm($term) as $word) {
            $builder
                ->orWildcard('brand_names.keyword', "*$word*", 0.5)
                ->orWildcard('brand_search_keywords', "*$word*", 5.5)
                ->orWildcard('category_names.keyword', "*$word*", 0.3)
                ->orTerm('car_engine_names.keyword', $word)//
            ;
        }
    }

}