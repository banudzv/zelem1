<?php

namespace App\Modules\Products\Filters\TextSearchBuilders;

use App\Modules\Search\Models\SearchBuilder;

interface WordSearchBuilder
{
    public function __construct(SearchBuilder $builder, string $query);

    public function build(): void;
}