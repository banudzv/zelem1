<?php

namespace App\Modules\Products\Filters\TextSearchBuilders;

class FourWordsSearchBuilder extends AbstractWordSearchBuilder
{

    public function build(): void
    {
        $builder = $this->getBuilder();
        $term = $this->getTerm();

        $builder
            ->orSimpleQueryString(
                [
                    'vendor_code^0.7',
                    'cars^3',
                    'car_vendor_names^3',
                    'car_model_names^3',
                    'feature_value_names.keyword^1.5',
                    'category_names^2.5',
                    'category_search_keywords^2.5',
                    'brand_names^2.5',
                    'brand_search_keywords^2.5',
                ],
                $term,
                [
                    'default_operator' => 'and',
                    'minimum_should_match' => '95%',
                    'analyze_wildcard' => true,
                ]
            )
            ->orMultiMatch(
                [
                    'brand_names^0.3',
                    'category_names^0.3',
                    'car_engine_names^0.3',
                ],
                $term,
                'cross_fields',
                [
                    'operator' => 'and',
                    'minimum_should_match' => '100%',
                    'tie_breaker' => 0.5,
                ]
            )
            ->orMultiMatch(
                [
                    'car_vendor_names^0.3',
                    'car_model_names^0.3',
                    'category_names^0.1',
                    'brand_names^0.3',
                ],
                $term,
                'cross_fields',
                [
                    'operator' => 'and',
                    'minimum_should_match' => '100%',
                    'tie_breaker' => 0.01,
                    'boost' => 0.6,
                ]
            )
            ->orWildcard('name.keyword', "*$term*", 7)
            ->orTerm('cars.keyword', $term, 5)//
        ;

        foreach ($this->explodeTerm($term) as $word) {
//
            $builder
                ->orWildcard('brand_names.keyword', "*$word*", 0.5)
                ->orTerm('car_engine_names.keyword', $word)//
            ;
        }
    }
}