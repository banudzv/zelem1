<?php

namespace App\Modules\Products\Filters\TextSearchBuilders;

class ThreeWordsSearchBuilder extends AbstractWordSearchBuilder
{

    public function build(): void
    {
        $builder = $this->getBuilder();
        $term = $this->getTerm();

        $builder
            ->orSimpleQueryString(
                [
                    'name^0.1',
                    'vendor_code',
                    'cars^3',
                    'car_vendor_names^3',
                    'car_model_names^3',
                    'feature_value_names^2',
                    'category_names^2.5',
                    'category_search_keywords^2.5',
                    'brand_names^2.5',
                    'brand_search_keywords^2.5',
                ],
                $term,
                [
                    'default_operator' => 'and',
                    'minimum_should_match' => '100%',
                    'analyze_wildcard' => true,
                ]
            )
            ->orMultiMatch(
                [
                    'brand_names',
                    'category_names',
                    'car_engine_names',
                ],
                $term,
                'cross_fields',
                [
                    'operator' => 'and',
                    'minimum_should_match' => '100%',
                    'tie_breaker' => 1,
                ]
            )
            ->orMultiMatch(
                [
                    'cars',
                    'category_names',
                    'brand_names',
                ],
                $term,
                'cross_fields',
                [
                    'operator' => 'and',
                    'minimum_should_match' => '100%',
                    'tie_breaker' => 1,
                ]
            )
//            ->orWildcard('name.keyword', "*$term*", 5)
            ->orTerm('cars.keyword', $term, 5)//
        ;

        foreach ($this->explodeTerm($term) as $word) {
            $builder
                ->orWildcard('brand_names.keyword', "*$word*", 1)
                ->orTerm('brand_names', $word, 1)
                ->orTerm('category_names', $word, 1)
                ->orTerm('car_engine_names.keyword', $word)//
            ;
        }
    }
}