<?php

namespace App\Modules\Products\Filters;

use App\Modules\Features\Models\FeatureValueTranslates;
use App\Modules\Products\Models\ProductGroup;
use App\Traits\WhereSlugsTrait;
use Illuminate\Database\Eloquent\Builder;

class SearchGroupsSluggableFilter extends SearchGroupsFilter
{
    use WhereSlugsTrait;

    public function filter(array $filters)
    {
        foreach ($filters as $parameter => $values) {
            if ($parameter === 'brand') {
                $brands = explode(',', $values);
                if (count($brands)) {
                    $this->whereHas('group.brandTranslates', $this->whereSlugs($brands));
                }
            } else {
                $values = explode(',', $values);
                if (count($values)) {
                    $subQuery = FeatureValueTranslates::query()
                        ->select('row_id')
                        ->where($this->whereSlugs($values))
                        ->getQuery();

                    $this->whereHas(
                        'same',
                        function ($builder) use ($subQuery) {
                            $builder->whereIn('value_id', $subQuery);
                        }
                    );
                }
            }
        }
        return $this;
    }

    public function categoryIds(array $categories)
    {
        $this->whereIn(
            'group_id',
            ProductGroup::query()
                ->select('id')
                ->whereIn('category_id', $categories)
                ->active()
        );
    }

    public function brandIds(array $brands)
    {
        $this->whereHas(
            'group',
            function (Builder $builder) use ($brands) {
                $builder->whereIn('brand_id', $brands);
            }
        );
    }

}