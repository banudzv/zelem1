<?php

namespace App\Modules\Products\Responses;

use App\Modules\Categories\Models\Category;
use App\Modules\Products\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductSearchResponses
{
    /**
     * @var LengthAwarePaginator|Product[]|Collection
     */
    protected $products;

    /**
     * @var Category[]|Collection
     */
    protected $categories;

    /**
     * ProductSearchResponses constructor.
     * @param LengthAwarePaginator|Product[]|Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator $products
     * @param Collection|null $categories
     */
    public function __construct(LengthAwarePaginator $products, Collection $categories = null)
    {
        $this->products = $products;
        $this->categories = $categories;

        if (is_null($categories)) {
            $this->categories = Collection::make();
        }
    }

    /**
     * @return LengthAwarePaginator|Product[]|Collection
     */
    public function getProducts(): LengthAwarePaginator
    {
        return $this->products;
    }

    /**
     * @return Collection|Category[]
     */
    public function getAggregateCategories(): Collection
    {
        return $this->categories;
    }

}