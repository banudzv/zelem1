<?php

namespace App\Modules\Products\Dto;

class ProductFilterConfig
{
    private $attributes;

    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    public function isShow(string $attribute): bool
    {
        return $this->attributes[$attribute] ?? false;
    }
}