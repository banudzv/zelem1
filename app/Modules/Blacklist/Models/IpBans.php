<?php

namespace App\Modules\Blacklist\Models;

use App\Modules\Blacklist\Filters\IpBansFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

/**
 * App\Modules\Blacklist\Models\IpBans
 *
 * @property int $id
 * @property string $ip
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static EloquentBuilder|IpBans filter($input = array(), $filter = null)
 * @method static EloquentBuilder|IpBans newModelQuery()
 * @method static EloquentBuilder|IpBans newQuery()
 * @method static EloquentBuilder|IpBans paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static EloquentBuilder|IpBans query()
 * @method static EloquentBuilder|IpBans simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static EloquentBuilder|IpBans whereActive($value)
 * @method static EloquentBuilder|IpBans whereBeginsWith($column, $value, $boolean = 'and')
 * @method static EloquentBuilder|IpBans whereCreatedAt($value)
 * @method static EloquentBuilder|IpBans whereEndsWith($column, $value, $boolean = 'and')
 * @method static EloquentBuilder|IpBans whereId($value)
 * @method static EloquentBuilder|IpBans whereLike($column, $value, $boolean = 'and')
 * @method static EloquentBuilder|IpBans whereLinkFrom($value)
 * @method static EloquentBuilder|IpBans whereLinkTo($value)
 * @method static EloquentBuilder|IpBans whereType($value)
 * @method static EloquentBuilder|IpBans whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Blacklist\Models\IpBans whereIp($value)
 */
class IpBans extends Model
{
    use Filterable;

    protected $casts = ['active' => 'boolean'];

    protected $fillable = ['ip', 'active'];


    /**
     * Register filter model
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(IpBansFilter::class);
    }

    /**
     * @return mixed
     */
    public static function getList()
    {
        return IpBans::filter(request()->only('ip', 'active'))
            ->latest('id')
            ->paginate(config('db.blacklist.ip-bans.per-page', 10));
    }


    public static function getBanned ($email){
        return IpBans::where('ip','=',$email)
            ->where('active', '=', true)
            ->first();
    }
}
