<?php

namespace App\Modules\Blacklist\Models;

use App\Modules\Blacklist\Filters\EmailBansFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

/**
 * App\Modules\SeoRedirects\Models\SeoRedirect
 *
 * @property int $id
 * @property string $email
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static EloquentBuilder|EmailBans filter($input = array(), $filter = null)
 * @method static EloquentBuilder|EmailBans newModelQuery()
 * @method static EloquentBuilder|EmailBans newQuery()
 * @method static EloquentBuilder|EmailBans paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static EloquentBuilder|EmailBans query()
 * @method static EloquentBuilder|EmailBans simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static EloquentBuilder|EmailBans whereActive($value)
 * @method static EloquentBuilder|EmailBans whereBeginsWith($column, $value, $boolean = 'and')
 * @method static EloquentBuilder|EmailBans whereCreatedAt($value)
 * @method static EloquentBuilder|EmailBans whereEndsWith($column, $value, $boolean = 'and')
 * @method static EloquentBuilder|EmailBans whereId($value)
 * @method static EloquentBuilder|EmailBans whereLike($column, $value, $boolean = 'and')
 * @method static EloquentBuilder|EmailBans whereLinkFrom($value)
 * @method static EloquentBuilder|EmailBans whereLinkTo($value)
 * @method static EloquentBuilder|EmailBans whereType($value)
 * @method static EloquentBuilder|EmailBans whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Blacklist\Models\EmailBans whereEmail($value)
 */
class EmailBans extends Model
{
    use Filterable;

    protected $casts = ['active' => 'boolean'];

    protected $fillable = ['email', 'active'];


    /**
     * Register filter model
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(EmailBansFilter::class);
    }

    /**
     * @return mixed
     */
    public static function getList()
    {
        return EmailBans::filter(request()->only('email', 'active'))
            ->latest('id')
            ->paginate(config('db.blacklist.email-bans.per-page', 10));
    }

    public static function getBanned ($email){
        return EmailBans::where('email','=',$email)
            ->where('active', '=', true)
            ->first();
    }

}
