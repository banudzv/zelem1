<?php

namespace App\Modules\Blacklist\Controllers\Admin;

use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Blacklist\Filters\EmailBansFilter;
use App\Modules\Blacklist\Forms\AdminEmailBansForm;
use App\Modules\Blacklist\Models\EmailBans;
use App\Modules\Blacklist\Requests\AdminEmailBansRequest;
use Seo;
use App\Core\AdminController;

/**
 * Class EmailBansController
 *
 * @package App\Modules\SeoRedirects\Controllers\Admin
 */
class EmailBansController extends AdminController
{

    public function __construct()
    {
        Seo::breadcrumbs()->add('blacklist::email-bans.seo.index', RouteObjectValue::make('admin.email_bans.index'));
    }

    /**
     * Register widgets with buttons
     *
     * @throws \Exception
     */
    private function registerButtons()
    {
        $this->addCreateButton('admin.email_bans.create');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function index()
    {
        // Set page buttons on the top of the page
        $this->registerButtons();

        Seo::meta()->setH1('blacklist::email-bans.seo.index');

        $emailBans = EmailBans::getList();
        // Return view list
        return view('blacklist::admin.email-bans.index', [
            'emailBans' => $emailBans,
            'filter' => EmailBansFilter::showFilter(),
        ]);
    }

    /**
     * Create new element page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function create()
    {
        // Breadcrumb
        Seo::breadcrumbs()->add('blacklist::email-bans.seo.create');
        // Set h1
        Seo::meta()->setH1('blacklist::email-bans.seo.create');
        // Javascript validation
        $this->initValidation((new AdminEmailBansRequest())->rules());
        // Return form view
        return view('blacklist::admin.email-bans.create', [
            'form' => AdminEmailBansForm::make(),
        ]);
    }

    /**
     * @param AdminEmailBansRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(AdminEmailBansRequest $request)
    {
        $emailBan = new EmailBans();
        $emailBan->fill($request->all());
        $emailBan->save();
        return $this->afterStore(['id' => $emailBan->id]);
    }

    /**
     * Update element page
     *
     * @param  EmailBans $emailBan
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(EmailBans $emailBan)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($emailBan->name ?? 'blacklist::email-bans.seo.edit');
        // Set h1
        Seo::meta()->setH1('blacklist::email-bans.seo.edit');
        // Javascript validation
        $this->initValidation((new AdminEmailBansRequest())->rules());
        // Return form view
        return view('blacklist::admin.email-bans.update', [
            'form' => AdminEmailBansForm::make($emailBan),
        ]);
    }

    /**
     * Update page in database
     *
     * @param AdminEmailBansRequest $request
     * @param EmailBans $emailBan
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(AdminEmailBansRequest $request, EmailBans $emailBan)
    {
        // Fill new data
        $emailBan->fill($request->all());
        // Create new page
        $emailBan->save();
        // Do something
        return $this->afterUpdate(['id' => $emailBan->id]);
    }

    /**
     * Totally delete page from database
     *
     * @param  EmailBans $emailBan
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(EmailBans $emailBan)
    {
        // Delete seoRedirects
        $emailBan->forceDelete();
        // Do something
        return $this->afterDestroy();
    }
}
