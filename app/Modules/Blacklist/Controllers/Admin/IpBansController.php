<?php

namespace App\Modules\Blacklist\Controllers\Admin;

use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Blacklist\Filters\IpBansFilter;
use App\Modules\Blacklist\Forms\AdminIpBansForm;
use App\Modules\Blacklist\Models\IpBans;
use App\Modules\Blacklist\Requests\AdminIpBansRequest;
use Seo;
use App\Core\AdminController;

/**
 * Class IpBansController
 *
 * @package App\Modules\SeoRedirects\Controllers\Admin
 */
class IpBansController extends AdminController
{

    public function __construct()
    {
        Seo::breadcrumbs()->add('blacklist::ip-bans.seo.index', RouteObjectValue::make('admin.ip_bans.index'));
    }

    /**
     * Register widgets with buttons
     *
     * @throws \Exception
     */
    private function registerButtons()
    {
        $this->addCreateButton('admin.ip_bans.create');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function index()
    {
        // Set page buttons on the top of the page
        $this->registerButtons();

        Seo::meta()->setH1('blacklist::ip-bans.seo.index');

        $ipBans = IpBans::getList();
        // Return view list
        return view('blacklist::admin.ip-bans.index', [
            'ipBans' => $ipBans,
            'filter' => IpBansFilter::showFilter(),
        ]);
    }

    /**
     * Create new element page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function create()
    {
        // Breadcrumb
        Seo::breadcrumbs()->add('blacklist::ip-bans.seo.create');
        // Set h1
        Seo::meta()->setH1('blacklist::ip-bans.seo.create');
        // Javascript validation
        $this->initValidation((new AdminIpBansRequest())->rules());
        // Return form view
        return view('blacklist::admin.ip-bans.create', [
            'form' => AdminIpBansForm::make(),
        ]);
    }

    /**
     * @param AdminIpBansRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(AdminIpBansRequest $request)
    {
        $ipBan = new IpBans();
        $ipBan->fill($request->all());
        $ipBan->save();
        return $this->afterStore(['id' => $ipBan->id]);
    }

    /**
     * Update element page
     *
     * @param  IpBans $ipBan
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(IpBans $ipBan)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($ipBan->name ?? 'blacklist::ip-bans.seo.edit');
        // Set h1
        Seo::meta()->setH1('blacklist::ip-bans.seo.edit');
        // Javascript validation
        $this->initValidation((new AdminIpBansRequest())->rules());
        // Return form view
        return view('blacklist::admin.ip-bans.update', [
            'form' => AdminIpBansForm::make($ipBan),
        ]);
    }

    /**
     * Update page in database
     *
     * @param AdminIpBansRequest $request
     * @param IpBans $ipBan
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(AdminIpBansRequest $request, IpBans $ipBan)
    {
        // Fill new data
        $ipBan->fill($request->all());
        // Create new page
        $ipBan->save();
        // Do something
        return $this->afterUpdate(['id' => $ipBan->id]);
    }

    /**
     * Totally delete page from database
     *
     * @param  IpBans $ipBan
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(IpBans $ipBan)
    {
        // Delete seoRedirects
        $ipBan->forceDelete();
        // Do something
        return $this->afterDestroy();
    }
}
