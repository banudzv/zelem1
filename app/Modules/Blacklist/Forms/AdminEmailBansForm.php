<?php

namespace App\Modules\Blacklist\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\Blacklist\Models\EmailBans;
use App\Modules\SeoRedirects\Models\SeoRedirect;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use Illuminate\Database\Eloquent\Model;

/**
* Class AdminEmailBansForm
*
* @package App\Core\Modules\SeoRedirects\Forms
*/
class AdminEmailBansForm implements FormInterface
{

    /**
     * @param  Model|SeoRedirect|null $model
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $model = null): Form
    {
        $model = $model ?? new EmailBans();
                $form = Form::create();
        $form->fieldSet()->add(
            Toggle::create('active', $model)->setDefaultValue(true)->required(),
            Input::create('email', $model)->setLabel(__('blacklist::email-bans.general.email'))->required()
        );
        return $form;
    }
}
