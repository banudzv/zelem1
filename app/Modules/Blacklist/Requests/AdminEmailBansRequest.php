<?php

namespace App\Modules\Blacklist\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\Blacklist\Models\EmailBans;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AdminSeoRedirectsRequest
 *
 * @package App\Modules\SeoRedirects\Requests
 */
class AdminEmailBansRequest extends FormRequest implements RequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $emailBan = request('email_ban') ?? new EmailBans();
        return [
            'active' => ['required', 'boolean'],
            'email' => [
                'required',
                'email',
                'min:1',
                Rule::unique('email_bans','email' )->ignore($emailBan->exists() ? $emailBan->id : null)
            ],
        ];
    }

}
