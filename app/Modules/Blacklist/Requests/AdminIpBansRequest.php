<?php

namespace App\Modules\Blacklist\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\Blacklist\Models\IpBans;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AdminSeoRedirectsRequest
 *
 * @package App\Modules\SeoRedirects\Requests
 */
class AdminIpBansRequest extends FormRequest implements RequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $ipBan = request('ip_ban') ?? new IpBans();

        return [
            'active' => ['required', 'boolean'],
            'ip' => [
                'required',
                'regex:/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/',
                Rule::unique('ip_bans','ip' )->ignore($ipBan->exists ? $ipBan->id : null)
            ],
        ];
    }

}
