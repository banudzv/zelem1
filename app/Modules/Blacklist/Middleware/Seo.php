<?php

namespace App\Modules\Blacklist\Middleware;

use App\Modules\SeoRedirects\Models\SeoRedirect;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Request;

class Seo
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $redirect = $this->redirects();
        if ($redirect) {
            return redirect($redirect->link_to, $redirect->type);
        }
        $url = request()->fullUrl();
        $urlParts = explode('?', $url);
        if (preg_match('/\/page\/1$/', $urlParts[0])) {
            $url = Str::replaceLast('/page/1', '', $urlParts[0]);
            if (count($urlParts) > 1) {
                $url .= '?' . $urlParts[1];
            }
            return redirect($url, 302);
        }
        return $next($request);
    }

    /**
     * @return SeoRedirect|Builder|Model|null|object
     */
    private function redirects()
    {
        $result = SeoRedirect::getWhereUrl(Request::getRequestUri());
        if ($result && $result->exists) {
            return $result;
        }
        return null;
    }
}
