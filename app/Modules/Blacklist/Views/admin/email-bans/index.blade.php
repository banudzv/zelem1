@php
/** @var \App\Modules\Blacklist\Models\EmailBans[] $emailBans */
/** @var string $filter */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter !!}
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>{{ __('blacklist::general.email') }}</th>
                        <th>{{ __('validation.attributes.created_at') }}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($emailBans AS $emailBan)
                        <tr>
                            <td>{{ $emailBan->email }}</td>
                            <td>{!! $emailBan->created_at ?? '&mdash;' !!}</td>
                            <td>{!! Widget::active($emailBan, 'admin.email_bans.active') !!}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.email_bans.edit', $emailBan->id) !!}
                                {!! \App\Components\Buttons::delete('admin.email_bans.destroy', $emailBan->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $emailBans->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
