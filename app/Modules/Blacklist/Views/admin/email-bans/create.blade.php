@php
    /** @var \CustomForm\Builder\Form $form */
    $form->buttons->showCloseButton(route('admin.email_bans.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.email_bans.store']) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop
