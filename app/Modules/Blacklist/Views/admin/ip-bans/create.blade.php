@php
    /** @var \CustomForm\Builder\Form $form */
    $form->buttons->showCloseButton(route('admin.ip_bans.index'))
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['route' => 'admin.ip_bans.store']) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop
