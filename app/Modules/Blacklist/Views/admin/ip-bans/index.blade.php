@php
/** @var \App\Modules\Blacklist\Models\IpBans[] $ipBans */
/** @var string $filter */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! $filter !!}
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>{{ __('blacklist::general.ip') }}</th>
                        <th>{{ __('validation.attributes.created_at') }}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($ipBans  AS $ipBan)
                        <tr>
                            <td>{{ $ipBan->ip }}</td>
                            <td>{!! $ipBan->created_at ?? '&mdash;' !!}</td>
                            <td>{!! Widget::active($ipBan, 'admin.ip_bans.active') !!}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.ip_bans.edit', $ipBan->id) !!}
                                {!! \App\Components\Buttons::delete('admin.ip_bans.destroy', $ipBan->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $ipBans->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
