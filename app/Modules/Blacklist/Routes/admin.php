<?php
use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:Blacklist'])->group(function () {
    // Admins list in admin panel
    Route::put('email-bans/{emailBan}/active', ['uses' => 'EmailBansController@active', 'as' => 'admin.email_bans.active']);
    Route::get('email-bans/{emailBan}/destroy', [
        'uses' => 'EmailBansController@destroy',
        'as' => 'admin.email_bans.destroy',
    ]);
    Route::resource('email-bans', 'EmailBansController')
        ->except('show', 'destroy')
        ->names('admin.email_bans')
        ->parameters(['email-bans' => 'email-ban']);

    Route::put('ip-bans/{ipBan}/active', ['uses' => 'IpBansController@active', 'as' => 'admin.ip_bans.active']);
    Route::get('ip-bans/{ipBan}/destroy', [
        'uses' => 'IpBansController@destroy',
        'as' => 'admin.ip_bans.destroy',
    ]);
    Route::resource('ip-bans', 'IpBansController')
        ->except('show', 'destroy')
        ->names('admin.ip_bans')
        ->parameters(['ip-bans' => 'ip-ban']);
});

// Routes for unauthenticated administrators
