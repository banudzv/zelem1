<?php

namespace App\Modules\Blacklist\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'blacklist';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.permission-name',
                    'ru' => 'Черный список',
                ],
                [
                    'name' => 'general.main-menu-block',
                    'ru' => 'Черный список',
                ],
                [
                    'name' => 'general.settings-name',
                    'ru' => 'Черный список',
                ],
                [
                    'name' => 'email-bans.general.menu',
                    'ru' => 'Email бан лист',
                ],
                [
                    'name' => 'ip-bans.general.menu',
                    'ru' => 'IP бан лист',
                ],

                [
                    'name' => 'email-bans.seo.index',
                    'ru' => 'Email баны',
                ],
                [
                    'name' => 'ip-bans.seo.index',
                    'ru' => 'Ip баны',
                ],
                [
                    'name' => 'email-bans.seo.edit',
                    'ru' => 'Редактирование Email бана',
                ],
                [
                    'name' => 'ip-bans.seo.edit',
                    'ru' => 'Редактирование Ip бана',
                ],
                [
                    'name' => 'email-bans.seo.create',
                    'ru' => 'Создание Email бана',
                ],
                [
                    'name' => 'email-bans.seo.create',
                    'ru' => 'Создание Ip бана',
                ],


                [
                    'name' => 'settings.group-name',
                    'ru' => 'Баны для формы',
                ],
                [
                    'name' => 'settings.attributes.per-page',
                    'ru' => 'Количество записей на странице в админ панели',
                ],
                [
                    'name' => 'general.email',
                    'ru' => 'Email',
                ],
                [
                    'name' => 'general.ip',
                    'ru' => 'Ip',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.general.email-is-in-blacklist',
                    'ru' => 'Ваша почта в черном списке. Если она оказалась в нем по ошибке обратитесь к администратору',
                ],
                [
                    'name' => 'site.general.ip-is-in-blacklist',
                    'ru' => 'Ваш ip в черном списке. Если он оказался в нем по ошибке обратитесь к администратору',
                ],

            ],
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
