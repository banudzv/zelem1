<?php

namespace App\Modules\Blacklist\Filters;

use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SeoRedirectsFilter
 *
 * @package App\Core\Modules\SeoRedirects\Filters
 */
class EmailBansFilter extends ModelFilter
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    static function showFilter()
    {
        $form = Form::create();
        $form->fieldSetForFilter()->add(
            Input::create('email')->setValue(request('email'))->setLabel(__('blacklist::general.email'))
                ->addClassesToDiv('col-md-3'),
            Select::create('active')
                ->add([
                    0 => __('global.unpublished'),
                    1 => __('global.published')
                ])
                ->setValue(request('active'))
                ->addClassesToDiv('col-md-2')
                ->setPlaceholder(__('global.all'))
        );
        return $form->renderAsFilter();
    }

    /**
     * @param string $email
     * @return EmailBansFilter
     */
    public function email(string $email)
    {
        return $this->where(function (Builder $query) use ($email) {
            return $query->whereRaw('LOWER(email) LIKE ?', ["%$email%"]);
        });
    }


    /**
     * @param string $active
     * @return EmailBansFilter
     */
    public function active(string $active)
    {
        return $this->where(function (Builder $query) use ($active) {
            return $query->whereActive((bool)$active);
        });
    }
}
