<?php

namespace App\Modules\Blacklist\Filters;

use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SeoRedirectsFilter
 *
 * @package App\Core\Modules\SeoRedirects\Filters
 */
class IpBansFilter extends ModelFilter
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    static function showFilter()
    {
        $form = Form::create();
        $form->fieldSetForFilter()->add(
            Input::create('ip')->setValue(request('ip'))->setLabel(__('blacklist::general.ip'))
                ->addClassesToDiv('col-md-3'),
            Select::create('active')
                ->add([
                    0 => __('global.unpublished'),
                    1 => __('global.published')
                ])
                ->setValue(request('active'))
                ->addClassesToDiv('col-md-2')
                ->setPlaceholder(__('global.all'))
        );
        return $form->renderAsFilter();
    }

    /**
     * @param string $ip
     * @return IpBansFilter
     */
    public function ip(string $ip)
    {
        return $this->where(function (Builder $query) use ($ip) {
            return $query->whereRaw('LOWER(ip) LIKE ?', ["%$ip%"]);
        });
    }


    /**
     * @param string $active
     * @return IpBansFilter
     */
    public function active(string $active)
    {
        return $this->where(function (Builder $query) use ($active) {
            return $query->whereActive((bool)$active);
        });
    }
}
