<?php

namespace App\Modules\Blacklist;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use CustomForm\Input;
use CustomSettings, CustomRoles, CustomMenu;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Index
 */
class Provider extends BaseProvider
{

    /**
     * Set custom presets
     */
    protected function presets()
    {
    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        // Register module configurable settings
        $settings = CustomSettings::createAndGet('blacklist', 'blacklist::settings.group-name');
        $settings->add(
            Input::create('per-page')->setLabel('blacklist::settings.attributes.per-page'),
            ['required', 'integer', 'min:1']
        );
        // Register left menu block
        $block = CustomMenu::get()->group()->block('blacklist', 'blacklist::general.main-menu-block','fa fa-ban');
        $block ->link('blacklist::email-bans.general.menu', RouteObjectValue::make('admin.email_bans.index'))
                ->setPosition(7)
                ->additionalRoutesForActiveDetect(
                    RouteObjectValue::make('admin.email_bans.edit'),
                    RouteObjectValue::make('admin.email_bans.create')
                );
        $block ->link('blacklist::ip-bans.general.menu', RouteObjectValue::make('admin.ip_bans.index'))
            ->setPosition(7)
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.ip_bans.edit'),
                RouteObjectValue::make('admin.ip_bans.create')
            );
        // Register role scopes
        CustomRoles::add('blacklist', 'blacklist::general.permission-name')
            ->except(RoleRule::VIEW);
    }


    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
//        $this->app->make(Kernel::class)->pushMiddleware(Seo::class);
    }

}
