<?php

namespace App\Modules\Comments\Widgets\Site;

use App\Components\Widget\AbstractWidget;
use App\Modules\Comments\Models\Comment;
use App\Modules\Comments\Requests\SiteCommentRequest;

/**
 * Class ArticleReviewForm
 *
 * @package App\Modules\Comments\Widgets\Site
 */
class ArticleReviewForm implements AbstractWidget
{
    
    /**
     * @var int|null
     */
    protected $commentableId;
    
    /**
     * @var string
     */
    protected $commentableType;
    
    /**
     * ProductAmount constructor.
     *
     * @param string $commentableType
     * @param int|null $articleId
     */
    public function __construct(string $commentableType, ?int $articleId = null)
    {
        $this->commentableType = $commentableType;
        $this->commentableId = $articleId;
    }
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        if (!$this->commentableId || !$this->commentableType) {
            return null;
        }
        $formId = uniqid('form-article-review');
        $validation = \JsValidator::make(
            (new SiteCommentRequest())->rules(),
            [],
            [],
            "#$formId"
        );
        return view('comments::site.article-review-form', [
            'articleId' => $this->commentableId,
            'type' => $this->commentableType,
            'formId' => $formId,
            'validation' => $validation,
        ]);
    }
    
}
