<?php


namespace App\Modules\Comments\Listeners;


use App\Core\Interfaces\ListenerInterface;
use App\Core\Modules\Notifications\Models\Notification;
use App\Modules\Comments\Models\Comment;

class NewCommentNotification implements ListenerInterface
{
    const NOTIFICATION_TYPE = 'comments';

    const NOTIFICATION_ICON = 'fa fa-comment';

    public static function listens(): string
    {
        return 'comments';
    }

    /**
     * @param Comment $comment
     */
    public function handle(Comment $comment)
    {
        Notification::send(
            static::NOTIFICATION_TYPE,
            "comments::general.notification.{$comment->commentable_type}",
            'admin.comments.edit',
            ['type' => $comment->commentable_type,'comment' => $comment->id]
        );
    }
}