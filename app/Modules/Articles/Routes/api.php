<?php

use Illuminate\Support\Facades\Route;

Route::get('articles', 'ArticlesController@index');
Route::get('articles/all', 'ArticlesController@all');
Route::get('articles/{article}', ['uses' => 'ArticlesController@show', 'as' => 'api.articles.show']);
