<?php

use Illuminate\Support\Facades\Route;

// Only authenticated
Route::middleware(['auth:admin', 'permission:articles'])->group(function () {
    // Admins list in admin panel
    Route::post('articles/live-search', [
        'uses' => 'LiveSearchController',
        'as' => 'admin.articles.live-search',
    ]);
    Route::put('articles/{article}/active', ['uses' => 'ArticlesController@active', 'as' => 'admin.articles.active']);
    Route::get(
        'articles/{article}/destroy',
        ['uses' => 'ArticlesController@destroy', 'as' => 'admin.articles.destroy']
    );
    Route::resource('articles', 'ArticlesController')->except('show', 'destroy')->names('admin.articles');

    // Categories
    Route::put('articles/categories/{category}/active', [
        'uses' => 'ArticlesCategoriesController@active',
        'as' => 'admin.articles-categories.active',
    ]);
    Route::put('articles/categories/sortable', [
        'uses' => 'ArticlesCategoriesController@sortable',
        'as' => 'admin.articles-categories.sortable',
    ]);
    Route::get(
        'articles/categories/{category}/destroy',
        ['uses' => 'ArticlesCategoriesController@destroy', 'as' => 'admin.articles-categories.destroy']
    );
    Route::resource('articles/categories', 'ArticlesCategoriesController')
        ->except('show', 'destroy')
        ->names('admin.articles-categories');
});
