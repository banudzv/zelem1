<?php

use Illuminate\Support\Facades\Route;

// Frontend routes
Route::paginate('articles', ['as' => 'site.articles', 'uses' => 'ArticlesController@index']);
Route::paginate('articles/category/{slug}', ['as' => 'site.articles-category', 'uses' => 'ArticlesController@category']);
Route::get('articles/{slug}', ['as' => 'site.articles-inner', 'uses' => 'ArticlesController@show']);
