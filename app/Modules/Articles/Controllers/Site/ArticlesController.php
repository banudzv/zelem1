<?php

namespace App\Modules\Articles\Controllers\Site;

use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\SiteController;
use App\Modules\Articles\Models\Article;
use App\Modules\Articles\Models\ArticleCategory;

/**
 * Class ArticlesController
 *
 * @package App\Modules\Articles\Controllers\Site
 */
class ArticlesController extends SiteController
{
    /**
     * @var SystemPage
     */
    static $page;
    
    /**
     * ArticlesController constructor.
     */
    public function __construct()
    {
        /** @var SystemPage $page */
        static::$page = SystemPage::getByCurrent('slug', 'articles');
        abort_unless(static::$page && static::$page->exists, 404);
    }
    
    /**
     * Articles list page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $articles = Article::getArticlesForClientSide();
        $this->pageNumber();
        $this->canonical(route('site.articles'));
        $this->meta(static::$page->current, static::$page->current->content);
        $this->breadcrumb(static::$page->current->name, 'site.articles');
        $this->setOtherLanguagesLinks(static::$page);
        if(\Config::get('db.articles.view')){
            return view('articles::site.categories', [
                'categories' => ArticleCategory::getCategoriesListForMenu(null),
                'page' => static::$page,
            ]);
        }
        return view('articles::site.index', [
            'articles' => $articles,
            'menu' => \Widget::show('articles::categories-menu'),
            'page' => static::$page,
        ]);
    }
    
    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category(string $slug)
    {
        /** @var ArticleCategory $category */
        $category = ArticleCategory::getByCurrent('slug', $slug);
        abort_unless($category && $category->exists && $category->active, 404);
        $this->pageNumber();
        $this->canonical(route('site.articles-category', ['slug' => $slug]));
        $this->meta($category->current, $category->current->seo_text);
        $this->breadcrumb(static::$page->current->name, 'site.articles');
        $this->addParentBreadcrumbs($category);
        $this->setOtherLanguagesLinks($category);
        $articles = Article::getArticlesForClientSide($category->id);
        if($articles->count() == 0 and $category->kids()->count() == 0){
            return view('articles::site.no-articles', [
                'menu' => \Widget::show('articles::categories-menu', $category),
                'page' => static::$page,
            ]);
        }
        if(\Config::get('db.articles.view') and $category->kids()->count() > 0){
            return view('articles::site.categories', [
                'categories' => ArticleCategory::getCategoriesListForMenu($category),
                'page' => static::$page,
            ]);
        }
        return view('articles::site.index', [
            'page' => $category,
            'menu' => \Widget::show('articles::categories-menu', $category),
            'articles' => $articles,
        ]);
    }
    
    /**
     * Show articles page
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $slug)
    {
        /** @var Article $article */
        $article = Article::getByCurrent('slug', $slug);
        abort_unless($article && $article->exists && $article->active, 404);
        $this->meta($article->current);
        $this->metaTemplate(Article::SEO_TEMPLATE_ALIAS, [
            'name' => $article->current->name,
        ]);
        $this->breadcrumb(static::$page->current->name, 'site.articles');
        $this->addParentBreadcrumbs($article->category);
        $this->breadcrumb($article->current->name);
        $this->setOtherLanguagesLinks($article);
        $this->canonical(route('site.articles-inner', [$article->current->slug]));
        return view('articles::site.show', [
            'article' => $article,
        ]);
    }

    /**
     * @param ArticleCategory $category
     */
    private function addParentBreadcrumbs(?ArticleCategory $category): void
    {
        if (!$category) {
            return;
        }
        if ($category->parent_id) {
            $this->addParentBreadcrumbs(ArticleCategory::find($category->parent_id));
        }
        if ($category->active) {
            $this->breadcrumb($category->current->name, 'site.articles-category', [
                'slug' => $category->current->slug,
                'page' => null,
            ]);
        }
    }
}
