<?php

namespace App\Modules\Articles\Controllers\Admin;

use App\Core\AjaxTrait;
use App\Modules\Articles\Models\Article;

/**
 * Class LiveSearchController
 *
 * @package App\Modules\Articles\Controllers\Admin
 */
class LiveSearchController
{
    use AjaxTrait;
    
    public function __invoke()
    {
        $items = [];
        Article::search(50, (array)request()->input('ignored', []))->each(function (Article $article) use (&$items) {
            $view = view('articles::admin.live-search-select-markup', [
                'article' => $article,
            ])->render();
            $items[] = [
                'id' => $article->id,
                'markup' => $view,
                'selection' => $view,
                'name' => $article->current->name,
            ];
        });
        return $this->successJsonAnswer([
            'items' => $items,
        ]);
    }
}
