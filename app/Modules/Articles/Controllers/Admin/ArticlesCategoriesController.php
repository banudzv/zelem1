<?php

namespace App\Modules\Articles\Controllers\Admin;

use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Articles\Forms\ArticleForm;
use App\Modules\Articles\Forms\CategoryForm;
use App\Modules\Articles\Models\Article;
use App\Modules\Articles\Models\ArticleCategory;
use App\Modules\Articles\Requests\ArticleRequest;
use App\Modules\Articles\Requests\CategoryRequest;
use Seo;
use App\Core\AdminController;

/**
 * Class ArticlesCategoriesController
 *
 * @package App\Modules\Articles\Controllers\Admin
 */
class ArticlesCategoriesController extends AdminController
{
    
    public function __construct()
    {
        Seo::breadcrumbs()->add(
            'articles::seo.categories-index',
            RouteObjectValue::make('admin.articles-categories.index')
        );
    }
    
    /**
     * Register widgets with buttons
     */
    private function registerButtons()
    {
        // Create new article button
        $this->addCreateButton('admin.articles-categories.create');
    }
    
    /**
     * Articles sortable list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->registerButtons();
        Seo::meta()->setH1('articles::seo.categories-index');
        Seo::meta()->setCountOfEntity(trans('articles::seo.categories.count',['count' => ArticleCategory::count(['id'])]));
        $categories = ArticleCategory::tree();
        if ($categories->isNotEmpty()) {
            return view('articles::admin.categories.index', ['categories' => $categories]);
        }
        return view('articles::admin.categories.no-pages');
    }
    
    /**
     * Create new element page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function create()
    {
        // Breadcrumb
        Seo::breadcrumbs()->add('articles::seo.categories-create');
        // Set h1
        Seo::meta()->setH1('articles::seo.categories-create');
        // Javascript validation
        $this->initValidation((new CategoryRequest())->rules());
        // Return form view
        return view(
            'articles::admin.categories.create', [
                'form' => CategoryForm::make(),
            ]
        );
    }
    
    /**
     * Create page in database
     *
     * @param  CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(CategoryRequest $request)
    {
        $article = new ArticleCategory;
        // Create new article
        if ($message = $article->createRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterStore(['id' => $article->id]);
    }

    /**
     * Update element page
     *
     * @param ArticleCategory $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     * @throws \Throwable
     */
    public function edit(ArticleCategory $category)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($category->current->name ?? 'articles::seo.categories-edit');
        // Set h1
        Seo::meta()->setH1('articles::seo.categories-edit');
        // Javascript validation
        $this->initValidation((new CategoryRequest)->rules());
        // Return form view
        return view('articles::admin.categories.update', [
            'form' => CategoryForm::make($category),
        ]);
    }
    
    /**
     * Update page in database
     *
     * @param  CategoryRequest $request
     * @param  ArticleCategory $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(CategoryRequest $request, ArticleCategory $category)
    {
        // Update existed article
        if ($message = $category->updateRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterUpdate();
    }
    
    /**
     * Totally delete page from database
     *
     * @param  ArticleCategory $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(ArticleCategory $category)
    {
        ArticleCategory::whereParentId($category->id)->update([
            'parent_id' => $category->parent_id,
        ]);
        $category->forceDelete();
        return $this->afterDestroy();
    }
    
}
