<?php

namespace App\Modules\Articles\Controllers\Api;

use App\Core\ApiController;
use App\Modules\Articles\Models\Article;
use App\Modules\Articles\Resources\ArticleCollection;
use App\Modules\Articles\Resources\ArticleFullResource;
use App\Modules\Articles\Resources\ArticleResource;

/**
 * Class ArticlesController
 *
 * @package App\Modules\Articles\Controllers\Api
 */
class ArticlesController extends ApiController
{
    const LIMIT = 10;
    
    /**
     * @OA\Get(
     *     path="/api/articles/all",
     *     tags={"Articles"},
     *     summary="Returns all articles with all data",
     *     operationId="getArticlesFullInformation",
     *     deprecated=false,
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *            type="array",
     *            @OA\Items(ref="#/components/schemas/ArticleFullInformation")
     *         )
     *     ),
     * )
     */
    public function all()
    {
        return ArticleFullResource::collection(Article::with(['data', 'image'])->get());
    }
    
    /**
     * @OA\Get(
     *     path="/api/articles",
     *     tags={"Articles"},
     *     summary="Returns list of articles with block for pagination",
     *     description="`page` attribute could be used in query",
     *     operationId="getArticles",
     *     deprecated=false,
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page needed for pagination",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="imageSize",
     *         in="query",
     *         description="Image size to return (original by default)",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/ArticlesList")
     *     ),
     * )
     */
    public function index()
    {
        return new ArticleCollection(Article::with(['current', 'image'])->whereActive(true)->latest('id')->paginate(static::LIMIT));
    }
    
    /**
     * @OA\Get(
     *     path="/api/articles/{id}",
     *     tags={"Articles"},
     *     summary="Returns one article by id",
     *     description="Returns one article by id",
     *     operationId="getArticle",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Article id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="articleId"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="imageSize",
     *         in="query",
     *         description="Image size to return (original by default)",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/Article"),
     *     ),
     *     @OA\Parameter(ref="#/components/parameters/Auth"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     *
     * @param Article $article
     * @return ArticleResource
     */
    public function show(Article $article)
    {
        abort_unless($article->active, 404);
        return new ArticleResource($article);
    }
}
