<?php

namespace App\Modules\Articles\Rules;

use App\Modules\Articles\Models\ArticleCategory;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class CategoryExists
 *
 * @package App\Modules\Articles\Rules
 */
class CategoryExists implements Rule
{
    
    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ArticleCategory::whereId($value)->exists();
    }
    
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('categories::general.category-does-not-exist');
    }
    
}
