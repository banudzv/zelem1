@php
/** @var \App\Modules\Articles\Models\ArticleCategory[] $categories */
$className = \App\Modules\Articles\Models\ArticleCategory::class;
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        <div class="dd pageList" id="myNest" data-depth="10">
            <ol class="dd-list">
                @include('articles::admin.categories.items', ['categories' => $categories, 'parentId' => 0])
            </ol>
        </div>
        <span id="parameters" data-url="{{ route('admin.articles-categories.sortable', ['class' => $className]) }}"></span>
        <input type="hidden" id="myNestJson">
    </div>
@stop
