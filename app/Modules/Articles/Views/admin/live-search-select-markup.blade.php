@php
/** @var \App\Modules\Articles\Models\Article $article */
@endphp
<div style="vertical-align: top; text-align: left;">
    @if($article->image && $article->image->isImageExists())
        <span style="display: inline-block; width: 30px;">
            {!! Widget::show('image', $article->image, '50x50', ['style' => 'max-width: 30px; max-height: 20px;']) !!}
        </span>
    @endif
    <span style="display: inline-block; width: calc(100% - 50px); margin-left: 10px;">{{ $article->current->name }}</span>
</div>
