@php
/** @var \App\Modules\Articles\Models\Article[]|\Illuminate\Pagination\LengthAwarePaginator $articles */
/** @var \App\Core\Modules\SystemPages\Models\SystemPage $page */
/** @var string|null $menu */
@endphp

@extends('site._layouts.main')

@section('layout-body')
    <div class="section _mb-def">
        <div class="container">
            <div class="grid _flex-nowrap _items-start">
                {!! $menu !!}
                <div class="gcell _flex-grow">
                    <div class="box">
                        @include('articles::site.widgets.articles-list', [
                            'articles' => $articles,
                            'grid_mod_classes' => 'grid--2 grid--def-' . ($menu ? 3 : 4),
                        ])
                        {!! $articles->links('pagination.site') !!}
                        <hr class="separator _color-white _mtb-xl">
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Widget::show('products::new') !!}
@endsection
