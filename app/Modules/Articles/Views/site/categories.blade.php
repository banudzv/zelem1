@php
/** @var \App\Modules\Categories\Models\Category[] $categories */
/** @var \App\Core\Modules\SystemPages\Models\SystemPage $page */
@endphp

@extends('site._layouts.main')

@section('layout-body')
    <div class="section _mb-lg">
        <div class="container _mb-xl _def-mb-xxl">
            <div class="_nmlr-xxs">
                <div class="grid grid--2 grid--ms-3 grid--def-4 grid--lg-5">
                    @foreach($categories as $category)
                        <div class="gcell">
                            <div class="catalog-group">
                                <div class="catalog-group__image">
                                    <a href="{{ $category->url }}" title="{{ $category->current->name }}">
                                        {!! $category->imageTag('small', ['width' => 260, 'height' => 260], false, site_media('static/images/placeholders/no-category.png')) !!}
                                    </a>
                                </div>
                                <div class="catalog-group__title">
                                    <div class="title title--size-h3">
                                        <a href="{{ $category->url }}">
                                            {{ $category->current->name }}
                                        </a>
                                    </div>
                                </div>
                                <div class="catalog-group__inner">
                                    @foreach($category->activeChildren as $index => $childCategory)
                                        <div class="catalog-group__item">
                                            <a href="{{ $childCategory->url }}"
                                               class="catalog-group__link"
                                               title="{{ $childCategory->current->name }}">
                                                {{ $childCategory->current->name }}
                                            </a>
                                        </div>
                                        @if($index === 5)
                                            <div class="catalog-group__item">
                                                <a href="{{ $category->url }}" class="catalog-group__link catalog-group__link--all">
                                                    @lang('categories::site.show-all')
                                                </a>
                                            </div>
                                            @break
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
