@php
/** @var \App\Modules\Articles\Models\ArticleCategory[]|\Illuminate\Support\Collection $categories */
/** @var int|null $currentCategoryId */
@endphp

<div class="gcell gcell--3 _flex-noshrink _def-show _def-pr-def">
    <div class="sidebar">
        <div class="sidebar__body">
            @foreach($categories as $category)
                <a href="{{ $category->url }}" class="sidebar-item {{ $category->id === $currentCategoryId ? 'is-active' : null }} ">
                    <div class="grid _hspace-def">
                        <div class="gcell gcell--auto _flex-grow">
                            <div class="sidebar-item__title">{{ $category->current->name }}</div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</div>
