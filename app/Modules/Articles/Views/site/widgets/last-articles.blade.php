@php
/** @var \App\Modules\Articles\Models\Article[] $articles */
@endphp
<div class="section">
    <div class="container _mtb-lg _def-mtb-xxl">
        <div class="grid grid--auto _justify-between _items-center _pb-def">
            <div class="gcell _mb-def _mr-def">
                <div class="title title--size-h2 _color-dark-blue title--normal">@lang('articles::site.last')</div>
            </div>
            <div class="gcell _mb-def _self-end">
                <a href="{{route('site.articles')}}" class="link link--underline link--icon-small text--semi-bold link--dark">
                    {{trans('articles::site.all-articles')}}
                    <div class="icon">
                        {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right') !!}
                    </div>
                </a>
            </div>
        </div>

        @include('articles::site.widgets.articles-list', [
            'articles' => $articles,
            'grid_mod_classes' => 'grid--2 grid--def-4',
        ])
    </div>
</div>
