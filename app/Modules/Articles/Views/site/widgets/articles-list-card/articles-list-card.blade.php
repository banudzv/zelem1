@php
    /** @var \App\Modules\Articles\Models\Article $article */
    $image = $article->imageTag('345x255', [
        'class' => 'lazyload-blur__image',
    ], false, site_media('static/images/placeholders/no-news.png'));
@endphp
<div class="news-card">
    {!! Html::link($article->link, $image, ['class' => 'news-card__image lazyload-blur'], null, false) !!}
    <div class="news-card__title">
        <a href="{{ $article->link }}">{{ $article->current->name }}</a>
    </div>
    <div class="news-card__desc _def-show">
        {{ $article->teaser }}
    </div>
    {!! Widget::show('articles::json-ld', $article) !!}
</div>
