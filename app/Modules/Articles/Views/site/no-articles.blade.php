@php
    /** @var \App\Modules\Articles\Models\Article[]|\Illuminate\Pagination\LengthAwarePaginator $articles */
    /** @var \App\Core\Modules\SystemPages\Models\SystemPage $page */
    /** @var string|null $menu */
@endphp

@extends('site._layouts.main')

@section('layout-body')
    <div class="section _mb-def">
        <div class="container">
            <div class="grid _flex-nowrap _items-start">
                {!! $menu !!}
                <div class="gcell _flex-grow box--full-height">
                    <div class="box box--full-height">
                        <div class="_text-center">
                            <div class="wysiwyg">
                                <p>@lang('messages.no-content')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Widget::show('products::new') !!}
@endsection
