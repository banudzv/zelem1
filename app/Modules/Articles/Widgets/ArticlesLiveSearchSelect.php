<?php

namespace App\Modules\Articles\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Articles\Models\Article;
use Form;

/**
 * Class ArticlesLiveSearchSelect
 *
 * @package App\Modules\Products\Widgets
 */
class ArticlesLiveSearchSelect implements AbstractWidget
{
    
    /**
     * @var array
     */
    protected $ignored;
    
    /**
     * @var string
     */
    protected $name;
    
    /**
     * @var int|null
     */
    protected $chosenArticleId;
    
    /**
     * @var array
     */
    protected $attributes;
    
    /**
     * ProductLiveSearchSelect constructor.
     *
     * @param array $ignored
     * @param string $name
     * @param int|null $chosenArticleId
     * @param array $attributes
     */
    public function __construct(array $ignored = [], string $name = 'article_id', ?int $chosenArticleId = null, array $attributes = [])
    {
        $this->ignored = $ignored;
        $this->name = $name;
        $this->chosenArticleId = $chosenArticleId;
        $this->attributes = $attributes;
    }
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $options = [];
        if ($this->chosenArticleId) {
            $article = Article::find($this->chosenArticleId);
            if ($article && $article->exists) {
                $options[$article->id] = view('articles::admin.live-search-select-markup', [
                    'article' => $article,
                ])->render();
            }
        }
        return Form::select($this->name, $options, $this->chosenArticleId, $this->attributes + [
            'class' => ['form-control', 'js-data-ajax'],
            'data-href' => route('admin.articles.live-search'),
            'data-ignored' => json_encode($this->ignored),
        ]);
    }
    
}
