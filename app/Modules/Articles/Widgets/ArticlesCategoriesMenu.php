<?php

namespace App\Modules\Articles\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Articles\Models\ArticleCategory;

/**
 * Class ArticlesCategoriesMenu
 *
 * @package App\Modules\Pages\Widgets
 */
class ArticlesCategoriesMenu implements AbstractWidget
{
    /**
     * Current category
     *
     * @var ArticleCategory|null
     */
    private $category;

    /**
     * ArticlesCategoriesMenu constructor.
     *
     * @param ArticleCategory|null $category
     */
    public function __construct(?ArticleCategory $category = null)
    {
        $this->category = $category;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function render()
    {
        $categories = ArticleCategory::getCategoriesListForMenu($this->category);
        if ($categories->isEmpty() or \Config::get('db.articles.view')) {
            return null;
        }
        return view('articles::site.widgets.menu', [
            'categories' => $categories,
            'currentCategoryId' => $this->category ? $this->category->id : null,
        ]);
    }

}
