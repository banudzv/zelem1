<?php

namespace App\Modules\Articles\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'articles';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'admin.choose-article',
                    'ru' => 'Выберите статью',
                ],
                [
                    'name' => 'general.menu',
                    'ru' => 'Статьи',
                ],
                [
                    'name' => 'general.attributes.main-category',
                    'ru' => 'Категория',
                ],
                [
                    'name' => 'general.categories-menu',
                    'ru' => 'Категории статей',
                ],
                [
                    'name' => 'general.labels.show-short-content',
                    'ru' => 'Выводить краткое описание на внутренней странице?',
                ],
                [
                    'name' => 'general.labels.show-image',
                    'ru' => 'Выводить изображение на внутренней странице?',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Список статей',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование статьи',
                ],
                [
                    'name' => 'seo.create',
                    'ru' => 'Добавление новой статьи',
                ],
                [
                    'name' => 'seo.categories-index',
                    'ru' => 'Список категорий статей',
                ],
                [
                    'name' => 'seo.categories-edit',
                    'ru' => 'Редактирование категории',
                ],
                [
                    'name' => 'seo.categories-create',
                    'ru' => 'Добавление новой категории',
                ],
                [
                    'name' => 'seo.template-variables.name',
                    'ru' => 'Название статьи',
                ],
                [
                    'name' => 'seo.template-variables.name',
                    'ru' => 'Название статьи',
                ],
                [
                    'name' => 'settings.group-name',
                    'ru' => 'Статьи',
                ],
                [
                    'name' => 'settings.attributes.per-page',
                    'ru' => 'Количество статей на странице в админ панели',
                ],
                [
                    'name' => 'settings.attributes.per-page-client-side',
                    'ru' => 'Количество статей на странице на сайте',
                ],
                [
                    'name' => 'settings.attributes.view',
                    'ru' => 'Отображать категории плиткой',
                ],
                [
                    'name' => 'seo.categories.count',
                    'ru' => 'Количество категорий статей - :count',
                ],
                [
                    'name' => 'seo.articles.count',
                    'ru' => 'Количество статей - :count',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.last',
                    'ru' => 'Статьи',
                    'ua' => 'Статті',
                ],
                [
                    'name' => 'site.all-articles',
                    'ru' => 'Все статьи',
                    'ua' => 'Всі статті',
                ],
                [
                    'name' => 'site.sitemap-articles',
                    'ru' => 'Статьи',
                    'ua' => 'Статті',
                ],
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
