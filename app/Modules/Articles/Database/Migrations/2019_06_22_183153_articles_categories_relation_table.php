<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticlesCategoriesRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles_categories_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('article_id')->unsigned();

            $table->foreign('article_id')->references('id')->on('articles')
                ->index('acr_article_id_articles_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('articles_categories')
                ->index('acr_category_id_articles_categories_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles_categories_relations', function (Blueprint $table) {
            $table->dropForeign('acr_article_id_articles_id');
            $table->dropForeign('acr_category_id_articles_categories_id');
        });
        Schema::dropIfExists('articles_categories_relations');
    }
}
