<?php

namespace App\Modules\Articles\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ArticleCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     *
     * @OA\Schema(
     *   schema="ArticlesList",
     *   @OA\Property(
     *      property="data",
     *      description="Articles list",
     *      type="array",
     *      @OA\Items(ref="#/components/schemas/ArticleSimple")
     *   ),
     *   @OA\Property(
     *      property="links",
     *      ref="#/components/schemas/PaginationLinks",
     *   ),
     *   @OA\Property(
     *      property="meta",
     *      ref="#/components/schemas/PaginationMeta",
     *   ),
     * )
     */
    public function toArray($request)
    {
        return ArticleSimpleResource::collection($this->collection);
    }
}
