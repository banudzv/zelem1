<?php

namespace App\Modules\Articles\Resources;

use App\Modules\Articles\Models\Article;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ArticleSimpleResource
 *
 * @package App\Modules\Articles\Resources
 *
 * @OA\Schema(
 *   schema="ArticleSimple",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "views", "name", "link"},
 *           @OA\Property(property="id", type="integer", description="Article id"),
 *           @OA\Property(property="views", type="integer", description="Article views counter"),
 *           @OA\Property(property="name", type="string", description="Article name"),
 *           @OA\Property(property="link", type="string", description="Article link (for API request)"),
 *           @OA\Property(property="short_content", type="string", description="Short article content (preview)"),
 *           @OA\Property(property="image", type="string", description="Image in chosen size (original by default)"),
 *       )
 *   }
 * )
 */
class ArticleSimpleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Article $article */
        $article = $this->resource;
        
        return [
            'id' => $article->id,
            'views' => $article->views,
            'name' => $article->current->name,
            'link' => route('api.articles.show', $article->id),
            'short_content' => $article->current->short_content,
            'image' => $article->image->link(request()->query('imageSize', 'original')),
        ];
    }
}
