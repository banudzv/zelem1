<?php

namespace App\Modules\Articles\Resources;

use App\Modules\Articles\Models\Article;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ArticleResource
 *
 * @package App\Modules\Articles\Resources
 *
 * @OA\Schema(
 *   schema="Article",
 *   type="object",
 *   allOf={
 *       @OA\Schema(
 *           required={"id", "views", "name", "link"},
 *           @OA\Property(property="id", type="integer", description="Article id"),
 *           @OA\Property(property="views", type="integer", description="Article views counter"),
 *           @OA\Property(property="name", type="string", description="Article name"),
 *           @OA\Property(property="link", type="string", description="Article link (for API request)"),
 *           @OA\Property(property="short_content", type="string", description="Short article content (preview)"),
 *           @OA\Property(property="content", type="string", description="Article full content"),
 *           @OA\Property(property="h1", type="string", description="Article meta h1"),
 *           @OA\Property(property="title", type="string", description="Article meta title"),
 *           @OA\Property(property="description", type="string", description="Article meta description"),
 *           @OA\Property(property="keywords", type="string", description="Article meta keywords"),
 *           @OA\Property(property="image", type="string", description="Image in chosen size (original by default)"),
 *       )
 *   }
 * )
 */
class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Article $article */
        $article = $this->resource;
        
        return [
            'id' => $article->id,
            'views' => $article->views,
            'name' => $article->current->name,
            'link' => route('api.articles.show', $article->id),
            'short_content' => $article->show_short_content ? $article->current->short_content : null,
            'content' => $article->current->content,
            'h1' => $article->current->h1,
            'title' => $article->current->title,
            'keywords' => $article->current->keywords,
            'description' => $article->current->description,
    
            'image' => $article->image->link(request()->query('imageSize', 'original')),
        ];
    }
}
