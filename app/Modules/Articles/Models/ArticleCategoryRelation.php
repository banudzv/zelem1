<?php

namespace App\Modules\Articles\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\Articles\Models\ArticleCategoryRelation
 *
 * @property int $id
 * @property int $category_id
 * @property int $article_id
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\Articles\Models\ArticleCategoryRelation $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryRelation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryRelation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryRelation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryRelation whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryRelation whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryRelation whereId($value)
 * @mixin \Eloquent
 */
class ArticleCategoryRelation extends Model
{
    use ModelTranslates;
    
    protected $table = 'articles_categories_relations';
    
    public $timestamps = false;

    protected $fillable = ['category_id', 'article_id'];
}
