<?php

namespace App\Modules\Articles\Models;

use App\Modules\Articles\Images\ArticleCategoryImage;
use App\Traits\ActiveScopeTrait;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use \Illuminate\Database\Eloquent\Collection as EloquentCollection;

/**
 * App\Modules\Articles\Models\ArticleCategory
 *
 * @property int $id
 * @property int $active
 * @property int|null $parent_id
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Core\Modules\Images\Models\Image $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Articles\Models\Article[] $articles
 * @property-read \App\Modules\Articles\Models\ArticleCategoryTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Articles\Models\ArticleCategoryTranslates[] $data
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory active($active = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Articles\Models\ArticleCategory[] $activeChildren
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Modules\Images\Models\Image[] $allImages
 * @property-read mixed $url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Modules\Images\Models\Image[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Articles\Models\ArticleCategory[] $kids
 * @property-read int|null $active_children_count
 * @property-read int|null $all_images_count
 * @property-read int|null $articles_count
 * @property-read int|null $data_count
 * @property-read int|null $images_count
 * @property-read int|null $kids_count
 */
class ArticleCategory extends Model
{
    use ModelMain, Imageable, ActiveScopeTrait;

    protected $table = 'articles_categories';

    protected $fillable = ['active', 'parent_id', 'position'];


    /**
     * Image config
     *
     * @return string|array
     */
    protected function imageClass()
    {
        return ArticleCategoryImage::class;
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'category_id', 'id');
    }

    public function getUrlAttribute()
    {
        return route('site.articles-category', ['slug' => $this->current->slug, 'page' => null]);
    }

    public static function getCategoriesListForMenu(?ArticleCategory $category)
    {
        if ($category) {
            $kids = ArticleCategory::active()
                ->where('parent_id', $category->id)
                ->oldest('position')
                ->latest('id')
                ->get();
            if ($kids->isNotEmpty()) {
                return $kids;
            } elseif($category->parent_id) {
                $kids = ArticleCategory::active()
                    ->where('parent_id', $category->parent_id)
                    ->oldest('position')
                    ->latest('id')
                    ->get();
                if ($kids->isNotEmpty()) {
                    return $kids;
                }
            }
        }
        return ArticleCategory::active()
            ->where(function (Builder $builder) {
                $builder
                    ->whereNull('parent_id')
                    ->orWhere('parent_id', 0);
            })
            ->oldest('position')
            ->latest('id')
            ->get();
    }

    public function getIdsTreeFor(int $categoryId)
    {
        $categories = [$categoryId];
        ArticleCategory::select('parent_id')
            ->whereId($categoryId)
            ->whereNotNull('parent_id')
            ->where('parent_id', '!=', 0)
            ->get()
            ->each(function (ArticleCategory $category) use (&$categories) {
                $categories = array_merge($categories, $this->getIdsTreeFor($category->parent_id));
            });
        return $categories;
    }

    public function kids()
    {
        return $this->hasMany(ArticleCategory::class, 'parent_id', 'id');
    }

    /**
     * Checks if parent can be linked as parent for chosen category
     *
     * @param int $categoryId
     * @param int|null $parentId
     * @return bool
     */
    public static function isAvailableToChoose(int $categoryId, ?int $parentId = null): bool
    {
        if ($parentId === null) {
            return true;
        }
        if ($categoryId === $parentId) {
            return false;
        }
        return in_array($parentId, ArticleCategory::getAllChildrenIds($categoryId)) === false;
    }

    /**
     * Get children ids list for category by id
     *
     * @param int|null $categoryId
     * @return array
     */
    public static function getAllChildrenIds(?int $categoryId = null): array
    {
        return ArticleCategory::getChildrenIdsFromCollection(ArticleCategory::tree(), $categoryId);
    }

    /**
     * Get children ids list for category from existing list by category id
     *
     * @param Collection|ArticleCategory[][] $categories
     * @param int|null $categoryId
     * @return array
     */
    public static function getChildrenIdsFromCollection(Collection $categories, ?int $categoryId = null): array
    {
        $childrenIds = [];
        foreach ($categories->get((int)$categoryId, []) as $category) {
            $childrenIds[] = $category->id;
            $childrenIds = array_merge($childrenIds, ArticleCategory::getChildrenIdsFromCollection($categories, $category->id));
        }
        return $childrenIds;
    }

    /**
     * Get categories list related to this group
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeChildren()
    {
        return $this->hasMany(ArticleCategory::class, 'parent_id', 'id')
            ->where('active', true)
            ->with(['current', 'activeChildren'])
            ->oldest('position')
            ->latest('id');
    }

    /**
     * @return array
     */
    public static function getDictionaryForSelects(): array
    {
        $categories = new EloquentCollection();
        ArticleCategory::with('current')
            ->oldest('position')
            ->latest('id')
            ->get()
            ->each(function (ArticleCategory $category) use ($categories) {
                $currentCollection = $categories->get((int)$category->parent_id, new EloquentCollection());
                $currentCollection->push($category);
                $categories->put((int)$category->parent_id, $currentCollection);
            });
        return static::makeDictionaryFrom($categories, 0);
    }

    /**
     * @param EloquentCollection $categories
     * @param int $parentId
     * @param array $dictionary
     * @param int $level
     * @return array
     */
    protected static function makeDictionaryFrom(EloquentCollection $categories, int $parentId, array &$dictionary = [], int $level = 0): array
    {
        $categories->get($parentId, new EloquentCollection)->each(function (ArticleCategory $category) use (&$dictionary, $level, $categories) {
            $spaces = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level);
            $dictionary[$category->id] = $spaces . $category->current->name;
            static::makeDictionaryFrom($categories, $category->id, $dictionary, ++$level);
        });
        return $dictionary;
    }

    /**
     * Builds categories full tree
     *
     * @param bool $onlyActive
     * @return Collection|ArticleCategory[][]
     */
    public static function tree(bool $onlyActive = false): Collection
    {
        $categories = new Collection();
        $query = ArticleCategory::with('current')
            ->oldest('position')
            ->latest('id');
        if ($onlyActive === true) {
            $query->active(true);
        }
        $query
            ->get()
            ->each(function (ArticleCategory $category) use ($categories) {
                $elements = $categories->get((int)$category->parent_id, []);
                $elements[] = $category;
                $categories->put((int)$category->parent_id, $elements);
            });
        return $categories;
    }
}