<?php

namespace App\Modules\Articles\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\Articles\Models\ArticleCategoryTranslates
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $row_id
 * @property string $language
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $seo_text
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\Articles\Models\ArticleCategory $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereRowId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereSeoText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\ArticleCategoryTranslates whereTitle($value)
 * @mixin \Eloquent
 */
class ArticleCategoryTranslates extends Model
{
    use ModelTranslates;
    
    protected $table = 'articles_categories_translates';
    
    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'h1', 'title', 'keywords', 'description', 'seo_text'];
    
    protected $hidden = ['id', 'row_id'];
}
