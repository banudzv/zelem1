<?php

namespace App\Modules\Articles\Models;

use App\Modules\Articles\Images\ArticlesImage;
use App\Traits\ActiveScopeTrait;
use App\Traits\Commentable;
use App\Traits\ModelMain;
use App\Traits\Imageable;
use CustomForm\Element;
use CustomForm\SimpleElement;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Articles\Filters\ArticlesFilter;
use EloquentFilter\Filterable;
use Illuminate\Support\Str;
use Widget;

/**
 * App\Modules\Articles\Models\Article
 *
 * @property int $id
 * @property bool $active
 * @property int $show_short_content
 * @property int $show_image
 * @property int $views
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $category_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Modules\Images\Models\Image[] $allImages
 * @property-read \App\Modules\Articles\Models\ArticleCategory|null $category
 * @property-read \App\Modules\Articles\Models\ArticleTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Articles\Models\ArticleTranslates[] $data
 * @property-read string $link
 * @property-read string $teaser
 * @property-read \App\Core\Modules\Images\Models\Image $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Modules\Images\Models\Image[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article active($active = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereShowImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereShowShortContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Articles\Models\Article whereViews($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Articles\Models\ArticleCategory[] $categories
 * @property-read int|null $all_images_count
 * @property-read int|null $categories_count
 * @property-read int|null $data_count
 * @property-read int|null $images_count
 */
class Article extends Model
{
    use ModelMain, Imageable, Filterable, EloquentTentacle, ActiveScopeTrait, Commentable;
    
    const TEASER_WORDS_LIMIT = 30;
    const SEO_TEMPLATE_ALIAS = 'articles';
    
    protected $casts = ['active' => 'boolean'];
    
    protected $fillable = ['active', 'show_image', 'show_short_content', 'category_id'];
    
    protected $hidden = ['created_at', 'updated_at'];
    
    /**
     * Register filter model
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(ArticlesFilter::class);
    }
    
    /**
     * @param int|null $limit
     * @param array $ignored
     * @return LengthAwarePaginator|Collection
     */
    public static function search(?int $limit = 50, array $ignored = []): LengthAwarePaginator
    {
        return Article::filter(request()->only('query', 'category'))
            ->whereNotIn('id', $ignored)
            ->paginate($limit);
    }
    
    /**
     * @param int|null $commentableId
     * @return Element|null
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function formElementForComments(?int $commentableId = null): ?Element
    {
        return SimpleElement::create()
            ->setLabel('articles::admin.choose-article')
            ->setDefaultValue(Widget::show('articles::live-search', [], 'commentable_id', $commentableId))
            ->required();
    }
    
    /**
     * @param int|null $articleId
     * @return string|null
     */
    public static function getElementForList(?int $articleId = null): ?string
    {
        if (!$articleId) {
            return null;
        }
        $article = Article::find($articleId);
        if (!$article) {
            return null;
        }
        return (string)\Html::link(
            route('admin.articles.edit', $article->id),
            $article->current->name,
            ['target' => '_blank']
        );
    }

    public function updateCategories()
    {
        ArticleCategoryRelation::whereArticleId($this->id)->delete();
        if ($this->category_id) {
            \DB::transaction(function () {
                foreach ((new ArticleCategory)->getIdsTreeFor($this->category_id) as $categoryId) {
                    ArticleCategoryRelation::create([
                        'article_id' => $this->id,
                        'category_id' => $categoryId,
                    ]);
                }
            });
        }
    }
    
    /**
     * Image class name
     *
     * @return string|array
     */
    protected function imageClass()
    {
        return ArticlesImage::class;
    }

    public function category()
    {
        return $this->belongsTo(ArticleCategory::class, 'category_id', 'id');
    }

    public function categories()
    {
        return $this->hasManyThrough(
            ArticleCategory::class,
            ArticleCategoryRelation::class,
            'article_id',
            'id',
            'id',
            'category_id'
        );
    }
    
    /**
     * Scope a query to only include popular users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('active', '=', true);
    }
    
    /**
     * Get list of news
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList()
    {
        return Article::with(['current'])
            ->filter(request()->only('name', 'active', 'category'))
            ->latest('id')
            ->paginate(config('db.articles.per-page', 10));
    }
    
    public static function last(?int $ignoreId = null, int $limit = 4)
    {
        $articles = Article::with('current', 'image', 'image.current')
            ->active(true)
            ->limit($limit)
            ->latest('id');
        if ($ignoreId) {
            $articles->where('id', '!=', $ignoreId);
        }
        return $articles->get();
    }
    
    /**
     * Link on the show news inner page
     *
     * @return string
     */
    public function getLinkAttribute(): string
    {
        return route('site.articles-inner', [
            'slug' => $this->current->slug,
        ]);
    }
    
    /**
     * Short description
     *
     * @return string
     */
    public function getTeaserAttribute()
    {
        $teaser = strip_tags($this->current->short_content);
        $teaser = trim($teaser);
        $teaser = Str::words($teaser, Article::TEASER_WORDS_LIMIT);
        return $teaser;
    }
    
    public static function getArticlesForClientSide(?int $categoryId = null)
    {
        $query =  Article::with(['current', 'image', 'image.current'])->published();
        if ($categoryId) {
            $query->whereHas('categories', function (Builder $builder) use ($categoryId) {
                $builder->where((new ArticleCategory)->getTable() . '.id', $categoryId);
            });
        }
        return $query->latest('id')->paginate(config('db.articles.per-page-client-side', 10));
    }


    /**
     * @param int $id
     * @return array|null
     */
    public function getPagesLinksByIdForImage(int $id)
    {
        $links = [];
        $item = Article::active()->find($id);
        if($item){
            $links[] = url(route('site.articles-inner', ['slug' => $item->current->slug], false), [], isSecure());
        }
        return $links;
    }
}
