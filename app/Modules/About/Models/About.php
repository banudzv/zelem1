<?php

namespace App\Modules\About\Models;

use App\Core\Modules\Images\Models\Image;
use App\Modules\About\Images\AboutImage;
use App\Modules\About\Images\AboutRightImage;
use App\Traits\ActiveScopeTrait;
use App\Traits\Imageable;
use App\Traits\ModelMain;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\About\Models\About
 *
 * @property int $id
 * @property bool $active
 * @property int $show_short_content
 * @property int $show_image
 * @property int $views
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $category_id
 * @property-read \App\Modules\About\Models\AboutTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\About\Models\AboutTranslates[] $data
 * @property-read \App\Core\Modules\Images\Models\Image $image
 * @property-read \App\Core\Modules\Images\Models\Image $imageRight
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About active($active = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\About whereCreatedAt($value)
 * @mixin \Eloquent
 */
class About extends Model
{
    use ModelMain, EloquentTentacle, Imageable, ActiveScopeTrait;

    const SEO_TEMPLATE_ALIAS = 'about';

    protected $table = 'about';
    protected $fillable = ['created_at', 'updated_at'];

    protected $hidden = ['created_at', 'updated_at'];


    /**
     * Set images upload config
     *
     * @return string|array
     */
    protected function imageClass()
    {
        return [
            AboutImage::class,
            AboutRightImage::class
        ];
    }

    public function imageRight()
    {
        return $this->hasOne(Image::class, 'imageable_id', 'id')
            ->where('imageable_type', '=', AboutRightImage::getType())
            ->oldest('position')
            ->latest('id')
            ->withDefault();

    }

    public function getShortContent()
    {
        return $this->current->short_content ?? '';
    }
}
