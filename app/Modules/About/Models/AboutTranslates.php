<?php

namespace App\Modules\About\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Modules\About\Models\AboutTranslates
 *
 * @property int $id
 * @property string|null $short_content
 * @property string|null $content_block1
 * @property string|null $content_block2
 * @property string|null $content_block3
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property int $row_id
 * @property string $language
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\About\Models\About $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereRowId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereShortContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\AboutTranslates whereTitle($value)
 * @mixin \Eloquent
 */
class AboutTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'about_translates';

    public $timestamps = false;

    protected $fillable = [
        'short_content',
        'content_block1',
        'content_block2',
        'content_block3',
        'h1',
        'title',
        'keywords',
        'description',
    ];

    protected $hidden = ['id', 'row_id'];
}
