<?php

namespace App\Modules\About\Models;

use App\Modules\Currencies\Models\Currency;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Modules\About\Models\About
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $title
 * @property string $videos
 * @property int $subscribers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\About\Models\Youtube whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Youtube extends Model
{

    protected $table = 'youtube_info';

    protected $fillable = ['title', 'subscribers', 'videos'];
}
