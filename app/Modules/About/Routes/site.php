<?php

use Illuminate\Support\Facades\Route;


Route::get('about', ['as' => 'site.about', 'uses' => 'IndexController@index']);
