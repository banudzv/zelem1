<?php

use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:about'])->group(function () {
    Route::post('about/update', ['uses' => 'IndexController@update','as' => 'admin.about.update']);
    Route::get('about', ['uses' => 'IndexController@index','as' => 'admin.about.index']);

});

// Routes for unauthenticated administrators


