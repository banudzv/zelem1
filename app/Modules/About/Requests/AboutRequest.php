<?php

namespace App\Modules\About\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\About\Images\AboutImage;
use App\Modules\About\Models\About;
use App\Modules\About\Models\AboutTranslates;
use App\Rules\MultilangSlug;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ArticleRequest
 *
 * @package App\Modules\Articles\Requests
 */
class AboutRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws \App\Exceptions\WrongParametersException
     */
    public function rules(): array
    {
        /**
         *
         *
         * @var About $news
         */
        $news = $this->route('news');

        return $this->generateRules(
            [
            ], [
            ]
        );
    }

}
