<?php

namespace App\Modules\About\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Modules\About\Models\About;
use App\Modules\Advantage\Models\Advantage;

class HomePage implements AbstractWidget
{

    public function render()
    {
        $page = SystemPage::getByCurrent('slug', 'about');
        $about = About::first();
        if (!$page || !$about || !$page->exists || !$about->exists) {
            return null;
        }

        return view('about::site.widget.home', [
            'page' => $page,
            'about' => $about
        ]);
    }

}
