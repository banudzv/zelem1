<?php

namespace App\Modules\About\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'about';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'О компании',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование страницы',
                ],
                [
                    'name' => 'admin.attribute.content-block1',
                    'ru' => 'Контент блока с лева',
                ],
                [
                    'name' => 'admin.attribute.content-block2',
                    'ru' => 'Контент блока справа',
                ],
                [
                    'name' => 'admin.attribute.content-block3',
                    'ru' => 'Контент блока снизу',
                ],
                [
                    'name' => 'admin.attribute.content-block',
                    'ru' => 'Контент блока сверху',
                ],
                [
                    'name' => 'admin.attribute.image-left',
                    'ru' => 'Изибражение слева',
                ],
                [
                    'name' => 'admin.attribute.image-right',
                    'ru' => 'Изибражение справа',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.advantage.counter-1',
                    'ru' => '536',
                ],
                [
                    'name' => 'site.advantage.text-1',
                    'ru' => '536 турбин ремонтирующихся ежемесячно',
                ],
                [
                    'name' => 'site.advantage.counter-2',
                    'ru' => '6487',
                ],
                [
                    'name' => 'site.advantage.text-2',
                    'ru' => '6 487 специалистов по всей Украине',
                ],
                [
                    'name' => 'site.advantage.text-3',
                    'ru' => 'Гарантія та фіксована ціна на ремонт',
                ],
                [
                    'name' => 'site.advantage.text-4',
                    'ru' => 'PowerTeck — деталі європейської якості',
                ],
                [
                    'name' => 'site.about.headline',
                    'ru' => 'О компании <span class="_color-dark-mint">Zalem</span>',
                ],
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
