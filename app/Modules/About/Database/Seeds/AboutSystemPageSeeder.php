<?php

namespace App\Modules\About\Database\Seeds;

use App\Core\Modules\Languages\Models\Language;
use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\Modules\SystemPages\Models\SystemPageTranslates;
use App\Modules\About\Models\About;
use App\Modules\About\Models\AboutTranslates;
use Illuminate\Database\Seeder;

class AboutSystemPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!$check = SystemPage::getByCurrent('slug', 'about')) {
            // Create main page
            $systemPage = new SystemPage();
            $systemPage->save();
            Language::all()->each(function (Language $language) use ($systemPage) {
                $translate = new SystemPageTranslates();
                $translate->name = 'О компании';
                $translate->slug = 'about';
                $translate->language = $language->slug;
                $translate->row_id = $systemPage->id;
                $translate->save();
            });
        }

        if(!$check = About::first()){
            $about = new About();
            $about->save();
            Language::all()->each(function (Language $language) use ($about) {
                $translate = new AboutTranslates();
                $translate->language = $language->slug;
                $translate->row_id = $about->id;
                $translate->save();
            });
        }
    }
}
