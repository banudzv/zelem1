<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
        Schema::create('about_translates', function (Blueprint $table) {
            $table->increments('id');
            $table->text('short_content')->nullable();

            $table->text('content_block1')->nullable();
            $table->text('content_block2')->nullable();
            $table->text('content_block3')->nullable();

            $table->string('h1')->nullable();
            $table->string('title')->nullable();
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->integer('row_id')->unsigned();
            $table->string('language', 3);

            $table->foreign('language')->references('slug')->on('languages')
                ->index('about_translates_language_languages_slug')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('row_id')->references('id')->on('about')
                ->index('about_translates_row_id_about_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('about_translates', function (Blueprint $table) {
            $table->dropForeign('about_translates_language_languages_slug');
            $table->dropForeign('about_translates_row_id_about_id');
        });
        Schema::dropIfExists('about_translates');
        Schema::dropIfExists('about');
    }
}
