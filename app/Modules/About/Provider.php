<?php

namespace App\Modules\About;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\About\Widgets\HomePage;
use CustomMenu, CustomRoles, Widget;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\About
 */
class Provider extends BaseProvider
{

    /**
     * Set custom presets
     */
    protected function presets()
    {
    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {

        // Menu element
        CustomMenu::get()->group()
            ->link('about::general.menu', RouteObjectValue::make('admin.about.index'), '	glyphicon glyphicon-question-sign')
            ->setPosition(12);
        // Register role scopes
        CustomRoles::add('about', 'about::general.permission-name')->except(RoleRule::VIEW);
    }


    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
        Widget::register(HomePage::class, 'about::home-page');
    }


}
