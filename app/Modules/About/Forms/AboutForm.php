<?php

namespace App\Modules\About\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\About\Images\AboutImage;
use App\Modules\About\Images\AboutRightImage;
use App\Modules\About\Models\About;
use Carbon\Carbon;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Image;
use CustomForm\Input;
use CustomForm\Macro\DateTimePicker;
use CustomForm\Macro\InputForSlug;
use CustomForm\Macro\Slug;
use CustomForm\Macro\Toggle;
use CustomForm\TextArea;
use CustomForm\TinyMce;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AboutForm
 *
 * @package App\Core\Modules\Administrators\Forms
 */
class AboutForm implements FormInterface
{

    /**
     * @param  Model|About|null $about
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $about = null): Form
    {
        $about = $about ?? new About;
        $form = Form::create();
        // Field set with languages tabs
        $form->fieldSetForLang(7)->add(
            //TinyMce::create('short_content', $about)->setLabel('about::admin.attribute.content-block'),
            TinyMce::create('content_block1', $about)->setLabel('about::admin.attribute.content-block1'),
            TinyMce::create('content_block2', $about)->setLabel('about::admin.attribute.content-block2'),
            TinyMce::create('content_block3', $about)->setLabel('about::admin.attribute.content-block3'),
            Input::create('h1', $about),
            Input::create('title', $about),
            TextArea::create('keywords', $about),
            TextArea::create('description', $about)
        );
        // Simple field set
        $form->fieldSet(5, FieldSet::COLOR_SUCCESS)->add(
            Image::create(AboutImage::getField(), $about->image)->setLabel('about::admin.attribute.image-left'),
            Image::create(AboutRightImage::getField(), $about->imageRight)->setLabel('about::admin.attribute.image-right')
        );
        return $form;
    }

}
