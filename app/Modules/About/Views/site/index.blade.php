@php

@endphp

@extends('site._layouts.main')

@section('layout-body')
    <div class="section about-page">
        <div class="container container--white container--small">
{{--            <div class="_mb-xl wysiwyg">--}}
{{--                {!! $about->getShortContent() !!}--}}
{{--            </div>--}}
            <div class="grid grid--1 grid--def-2 _pb-xl _flex-column-reverse _def-flex-row">
                <div class="gcell _pt-sm _def-pr-lg">
                    <div class="wysiwyg">
                        {!! $about->current->content_block1 !!}
                    </div>
                    <div
                        class="_flex _justify-center _def-justify-start _items-center _pt-xl _ms-nmr-md _flex-column _items-stretch _ms-flex-row">
                        <div
                            class="about-company__info about-company__info--about-page _m-none _mr-none _mtb-md _def-mtb-none _ms-mr-md">
                            <div class="about-company__counter">
                                @lang('about::site.advantage.counter-1')
                            </div>
                            <div class="text _text-center _color-blue-smoke">
                                @lang('about::site.advantage.text-1')
                            </div>
                        </div>
                        <div
                            class="about-company__info about-company__info--about-page _m-none _mr-none _mtb-md _def-mtb-none _ms-mr-md">
                            <div class="about-company__counter">
                                @lang('about::site.advantage.counter-2')
                            </div>
                            <div class="text _text-center _color-blue-smoke">
                                @lang('about::site.advantage.text-2')
                            </div>
                        </div>
                    </div>
                </div>
                @if($about->image->name)
                    <div class="gcell _pb-lg">
                        <img src="{{ $about->image->link() }}"
                             class="about-page__image _mlr-auto" alt="">
                    </div>
                @endif
            </div>
            <div class="grid grid--1 grid--def-2 _ptb-xxl _flex-column _def-flex-row">
                @if($about->imageRight->name)
                    <div class="gcell _pb-lg _pr-xl">
                        <img src="{{ $about->imageRight->link() }}"
                             class="about-page__image _ml-auto _def-ml-none _mr-auto"
                             alt="">
                    </div>
                @endif
                <div class="gcell _pt-lg {{ $about->imageRight->name ? '_def-nml-def' : null }}">
                    <div class="wysiwyg">
                        {!! $about->current->content_block2 !!}
                    </div>

                    <div
                        class="_flex _pt-xxl _justify-center _def-justify-start _items-center _flex-column _ms-nmr-md _items-stretch _ms-flex-row">
                        <div class="about-company__info about-company__info--about-page _m-none _mr-none _mtb-md _def-mtb-none _ms-mr-md">
                            <div class="about-company__icon">
                                {!! \SiteHelpers\SvgSpritemap::get( 'about-info-warranty' , [
                                        'fill' => '#47B39C',
                                        'class' => ''
                                ]) !!}
                            </div>
                            <div class="text _text-center _color-blue-smoke">
                                @lang('about::site.advantage.text-3')
                            </div>
                        </div>
                        <div
                            class="about-company__info about-company__info--about-page _m-none _mr-none _mtb-md _def-mtb-none _ms-mr-md">
                            <div class="about-company__icon">
                                {!! \SiteHelpers\SvgSpritemap::get( 'icon-europe-flag' , [
                                        'fill' => '#47B39C',
                                        'class' => ''
                                ]) !!}
                            </div>
                            <div class="text _text-center _color-blue-smoke">
                                @lang('about::site.advantage.text-4')
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="_pb-xxl wysiwyg">
                {!! $about->current->content_block3 !!}
            </div>
            {!! Widget::show('advantage::show') !!}
        </div>
    </div>
@endsection
