<div class="section">
    <div class="container _plr-xl _mt-def _md-mt-xl _def-pl-xxl">
        <div class="about-company">
            <div class="grid grid--1 grid--md-2 _def-pl-xl _justify-between">
                <div class="gcell">
                    <div class="title title--size-h3 title--normal _color-dark-blue">
                        {!! trans('about::site.about.headline') !!}
                    </div>

                    <div class="about-company__text _color-blue-smoke">
                        <div class="wysiwyg">
                            {!! $about->current->content_block1 !!}
                        </div>
                    </div>
                    <div class="grid grid--1 grid--xs-2 grid--xl-auto _pt-lg _nmt-sm _nml-sm">
                        <div class="gcell _pl-sm _pt-sm">
                            <div class="about-company__info">
                                <div class="about-company__counter">
                                    {{ __('about::site.advantage.counter-1') }}
                                </div>
                                <div class="text _text-center _color-blue-smoke">
                                    {{ __('about::site.advantage.text-1') }}
                                </div>
                            </div>
                        </div>
                        <div class="gcell _pl-sm _pt-sm">
                            <div class="about-company__info">
                                <div class="about-company__counter">
                                    {{ __('about::site.advantage.counter-2') }}
                                </div>
                                <div class="text _text-center _color-blue-smoke">
                                    {{ __('about::site.advantage.text-2') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($about->image->name)
                    <div class="gcell _plr-lg _md-pr-none _text-center _mt-xl _def-mt-none _def-text-right _def_nmr-md">
                        <img class="about-company__image" src="{{ $about->image->link() }}" alt="about-company">
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
