@php
    /** @var \CustomForm\Builder\Form $form */
@endphp

@extends('admin.layouts.main')

@section('content-no-row')
    {!! Form::open(['method' => 'POST', 'files' => true, 'url' => route('admin.about.update', Route::current()->parameters)]) !!}
    {!! $form->render() !!}
    {!! Form::close() !!}
@stop
