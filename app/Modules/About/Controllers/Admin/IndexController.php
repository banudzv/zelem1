<?php

namespace App\Modules\About\Controllers\Admin;

use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\About\Forms\AboutForm;
use App\Modules\About\Images\AboutRightImage;
use App\Modules\About\Models\About;
use App\Modules\About\Requests\AboutRequest;
use Seo;
use App\Core\AdminController;

/**
 * Class IndexController
 *
 * @package App\Modules\About\Controllers\Admin
 */
class IndexController extends AdminController
{

    public function __construct()
    {
        Seo::breadcrumbs()->add('about::seo.index', RouteObjectValue::make('admin.about.index'));
    }
    /**
     * Update element page
     *
     * @param  About $about
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function index()
    {
        // Breadcrumb
        Seo::breadcrumbs()->add('about::seo.edit');
        // Set h1
        Seo::meta()->setH1('about::seo.edit');
        // Javascript validation
        $this->initValidation((new AboutRequest)->rules());
        // Return form view
        $about = About::first();
        return view(
            'about::admin.update', [
                'form' => AboutForm::make($about),
            ]
        );
    }

    /**
     * @param AboutRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\WrongParametersException|\Exception
     */
    public function update(AboutRequest $request)
    {
        $about = About::first();
        // Update existed news
        if ($message = $about->updateRow($request)) {
            return $this->afterFail($message);
        }
        $about->uploadImage(AboutRightImage::getType());
        // Do something
        return $this->afterUpdate();
    }

    /**
     * Totally delete page from database
     *
     * @param  About $about
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(About $about)
    {
        // Delete news's image
        $about->deleteAllImages();
        // Delete news
        $about->forceDelete();
        // Do something
        return $this->afterDestroy();
    }

    /**
     * Delete image
     *
     * @param  About $about
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function deleteImage(About $about)
    {
        // Delete news's image
        $about->deleteImagesIfExist();
        // Do something
        return $this->afterDeletingImage();
    }

}
