<?php

namespace App\Modules\About\Controllers\Site;

use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\SiteController;
use App\Modules\About\Models\About;
use Seo;

/**
 * Class IndexController
 *
 * @package App\Modules\About\Controllers\Site
 */
class IndexController extends SiteController
{
    /**
     * @var SystemPage
     */
    static $page;

    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        /** @var SystemPage $page */
        static::$page = SystemPage::getByCurrent('slug', 'about');
        abort_unless(static::$page && static::$page->exists, 404);
    }

    /**
     * About list page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $about = About::first();
        $this->canonical(route('site.about'));
        $this->meta(static::$page->current, static::$page->current->content);
        Seo::site()->setH1(trans('about::site.about.headline'));
        $this->breadcrumb(static::$page->current->name, 'site.articles');
        $this->setOtherLanguagesLinks(static::$page);
        return view('about::site.index', [
            'page' => static::$page,
            'about' => $about
        ]);
    }
}
