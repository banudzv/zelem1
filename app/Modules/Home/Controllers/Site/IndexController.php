<?php

namespace App\Modules\Home\Controllers\Site;

use Catalog;
use App\Core\SiteController;
use App\Modules\Home\Provider;
use App\Modules\About\Models\Youtube;
use App\Modules\Products\Models\ProductGroupLabel;

/**
 * Class IndexController
 * @package App\Modules\Home\Controllers\Site
 */
class IndexController extends SiteController
{
    public function home()
    {
        if (Provider::$page) {
            $this->meta(Provider::$page->current, Provider::$page->current->content);
        }
        $this->canonical(route('site.home'));
        $youtube = Youtube::latest()->first();
        Catalog::ecommerce()->setPage('home', 'productImpressions');

        return view('home::site.home', [
            'page' => Provider::$page,
            'youtube' => $youtube,
        ]);
    }
}
