<?php

namespace App\Modules\Home;

use App\Core\BaseProvider;
use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\ObjectValues\RouteObjectValue;
use CustomForm\Input;
use CustomForm\Macro\Toggle;
use CustomForm\TextArea;
use CustomForm\TinyMce;
use CustomForm\WysiHtml5;
use Seo, CustomSettings;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Home
 */
class Provider extends BaseProvider
{
    /**
     * @var SystemPage
     */
    static $page;

    /**
    * Set custom presets
    */
    protected function presets()
    {
    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {

        $settings = CustomSettings::createAndGet('basic', 'home::basic.settings-name', -10000);
        foreach(config('languages') as $language){
            $settings->add(
                WysiHtml5::create('slogan_'.$language->slug)
                    ->setLabel('settings::general.multi-lang-slogan')
                    ->setAdditionalLabel(['language' => $language->name])
                    ->setHelp('settings::general.slogan-info'),
                [], 1
            );
        }
        foreach(config('languages') as $language){
            $settings->add(
                TextArea::create('schedule_'.$language->slug)
                    ->setLabel('home::basic.attributes.multi-lang-schedule')
                    ->setAdditionalLabel(['language' => $language->name]),
                [], 2
            );
        }
        foreach(config('languages') as $language){
            $settings->add(
                TextArea::create('company_'.$language->slug)
                    ->setLabel('home::basic.attributes.multi-lang-company')
                    ->setAdditionalLabel(['language' => $language->name]),
                [], 3
            );
        }
        foreach(config('languages') as $language) {
            $settings->add(
                Input::create('copyright_'.$language->slug)
                    ->setLabel('home::basic.attributes.multi-lang-copyright')
                    ->setAdditionalLabel(['language' => $language->name]),
                [], 4
            );
        }
        foreach(config('languages') as $language) {
            $settings->add(
                Input::create('agreement_link_'.$language->slug)
                    ->setLabel('home::basic.attributes.multi-lang-agreement_link')
                    ->setAdditionalLabel(['language' => $language->name]),
                [], 5
            );
        }
        $settings->add(
            Input::create('site_email')
                ->setLabel('home::basic.attributes.site-email'),
            [], 6
        );
        $settings->add(
            Input::create('hot_line')
                ->setLabel('home::basic.attributes.hot-line'),
            [], 7
        );
        $settings->add(
            Input::create('phone_number_1')
                ->setLabel('home::basic.attributes.phone-number'),
            [], 8
        );
        $settings->add(
            Input::create('phone_number_2')
                ->setLabel('home::basic.attributes.phone-number'),
            [], 9
        );
        $settings->add(
            Input::create('phone_number_3')
                ->setLabel('home::basic.attributes.phone-number'),
            [], 10
        );
        $settings->add(
            Toggle::create('mobile_show_phone_hide_slogan')
                ->setLabel('home::basic.attributes.toggle-phone-slogan-on-mobile')->setDefaultValue(false),
            [], 11
        );
        $settings->add(
            Toggle::create('agreement_show')
                ->setLabel('home::basic.attributes.agreement_show'),
            [], 12
        );
        $settings->add(
            Toggle::create('cookie_show')
                ->setLabel('home::basic.attributes.cookie_show'),
            [], 13
        );
        $settings->add(
            Input::create('cache_life_time')
                ->setType('number')
                ->required()
                ->setLabel('home::basic.attributes.cache_life_time'),
            ['required', 'integer', 'min:900'],14
        );
        $settings->add(
            Input::create('youtube_chanel')
                ->required()
                ->setLabel('home::basic.attributes.youtube_chanel'),
            ['required'], 15
        );
        $settings->add(
            Input::create('youtube_api')
                ->required()
                ->setLabel('home::basic.attributes.youtube_api'),
            ['required'], 16
        );
        foreach(config('languages') as $language) {
            $settings->add(
                Input::create('address_city_'.$language->slug)
                    ->setLabel('home::basic.attributes.multi-lang-address-city')
                    ->setAdditionalLabel(['language' => $language->name]),
                [], 17
            );
        }
        foreach(config('languages') as $language) {
            $settings->add(
                Input::create('address_street_'.$language->slug)
                    ->setLabel('home::basic.attributes.multi-lang-address-street')
                    ->setAdditionalLabel(['language' => $language->name]),
                [], 17
            );
        }
    }

    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
        if (\Schema::hasTable((new SystemPage)->getTable())) {
            static::$page = SystemPage::getByCurrent('slug', 'index');
            if (static::$page) {
                $this->app->booted(function () {
                    Seo::breadcrumbs()->add(static::$page->current->name, RouteObjectValue::make('site.home'));
                });
            }
        }
 }

}
