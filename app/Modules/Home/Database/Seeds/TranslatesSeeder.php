<?php

namespace App\Modules\Home\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'home';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'Базовые настройки',
                ],
                [
                    'name' => 'basic.settings-name',
                    'ru' => 'Базовые настройки',
                ],
                [
                    'name' => 'basic.attributes.site-email',
                    'ru' => 'E-Mail  сайта (в футере)',
                ],
                [
                    'name' => 'basic.attributes.hot-line',
                    'ru' => 'Горячая линия',
                ],
                [
                    'name' => 'basic.attributes.phone-number',
                    'ru' => 'Номер телефона',
                ],
                [
                    'name' => 'basic.attributes.toggle-phone-slogan-on-mobile',
                    'ru' => 'Показывать номера телефонов вместо слогана на мобильных устройствах?',
                ],
//                [
//                    'name' => 'basic.attributes.schedule',
//                    'ru' => 'График работы',
//                ],
                [
                    'name' => 'basic.attributes.multi-lang-schedule',
                    'ru' => 'График работы - :language',
                ],
//                [
//                    'name' => 'basic.attributes.company',
//                    'ru' => 'Название компании в подвале сайта',
//                ],
                [
                    'name' => 'basic.attributes.multi-lang-company',
                    'ru' => 'Название компании в подвале сайта - :language',
                ],
                [
                    'name' => 'basic.attributes.multi-lang-copyright',
                    'ru' => 'Copyright - :language',
                ],
//                [
//                    'name' => 'basic.attributes.copyright',
//                    'ru' => 'Copyright',
//                ],
                [
                    'name' => 'basic.attributes.multi-lang-address-city',
                    'ru' => 'Город - :language',
                ],
                [
                    'name' => 'basic.attributes.multi-lang-address-street',
                    'ru' => 'Адрес - :language',
                ],
                [
                    'name' => 'basic.attributes.multi-lang-agreement_link',
                    'ru' => 'Ссылка на страницу "Условия обработки данных" - :language',
                ],
                [
                    'name' => 'basic.attributes.agreement_show',
                    'ru' => 'Отображать согласие на обработку личных данных',
                ],
                [
                    'name' => 'basic.attributes.cookie_show',
                    'ru' => 'Отображать соглашение cookies',
                ],
                [
                    'name' => 'basic.attributes.cache_life_time',
                    'ru' => 'Время хранения кэшированных изображений (в секундах)',
                ],
                [
                    'name' => 'basic.attributes.youtube_chanel',
                    'ru' => 'Ссылка на канал Youtube',
                ],
                [
                    'name' => 'basic.attributes.youtube_api',
                    'ru' => 'API ключ к Youtube',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'general.catalog',
                    'ru' => 'Каталог',
                ],
            ],
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
