@php
/** @var \App\Core\Modules\SystemPages\Models\SystemPage|null $page */
$hideH1 = true;
@endphp

@extends('site._layouts.main')

@push('expandedCatalog', false)

@section('layout-body')
    <div class="section">
        <div class="container container--white">
            <div class="_def-hide _text-center _pt-md grid _justify-center">
                <a href="{{ route('site.categories') }}" class="gcell gcell--12 gcell--xs-8 gcell--sm-6 gcell--md-4 button button--size-normal _ptb-sm button--to-catalog button--theme-main button--width-lg">
                    <span class="button__body">
                        {!! \SiteHelpers\SvgSpritemap::get('catalogue', ['class' => 'button__icon button__icon--before button__icon--to-catalog']) !!}
                        <span class="button__text">@lang('home::general.catalog')</span>
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="section _md-mt-def _def-show _md-nmb-xs">
        <div class="container container--small container--border-top-radius container--white _pt-xl">
            {!! Widget::show('brands::our-brands') !!}
        </div>
    </div>
    <hr>
    {!! Widget::show('com::products') !!}
    <hr>
     {!! Widget::show('products-labels::all-labels') !!}
    {!! Widget::show('about::home-page') !!}
    {!! Widget::show('viewed::products') !!}
    {!! Widget::show('reviews') !!}
    <div class="section _md-mt-def _def-hide">
        <div class="container container--white">
            {!! Widget::show('brands::our-brands') !!}
        </div>
    </div>
    @if($youtube)
    <div class="section _def-show">
        <div class="container _mtb-lg _def-mt-xxl _def-mb-xl">
            <div class="youtube-block">
                <div class="youtube-block__header">
                    <div class="youtube-block__title">
                        {!! SiteHelpers\SvgSpritemap::get('icon-youtube-outline', [
                            'class' => 'youtube-block__icon',
                            'width' => 24,
                            'height' => 24
                        ]) !!}
                        <span>
                            Новые видео на канале <a target="_blank" href="{{ config('db.basic.youtube_chanel') }}">{{ $youtube->title }}</a>
                        </span>
                    </div>
                    <a target="_blank" href="{{ config('db.basic.youtube_chanel') }}?sub_confirmation=1#dialog" class="youtube-subscribe">
                        <span class="youtube-subscribe__title">
                        {!! SiteHelpers\SvgSpritemap::get('icon-youtube', [
                            'class' => 'youtube-subscribe__icon',
                            'width' => 16,
                            'height' => 16
                        ]) !!}
                            <span>YouTube</span>
                        </span>
                        <span class="youtube-subscribe__count">
                            {{ $youtube->subscribers }}
                        </span>
                    </a>
                </div>
                <div class="youtube-block__content">
                    @foreach(json_decode($youtube->videos) as $video)
                        <div class="youtube-card js-init" data-video="{{ $video->video_id }}">
                            <div class="youtube-card__cover">
                                <div class="youtube-card__thumbnail" data-thumbnail>
                                    <div class=" youtube-card__image">
                                        <img class="js-lazyload" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='120' height='25'%3E%3C/svg%3E" data-zzload-source-img="https://img.youtube.com/vi/{{ $video->video_id }}/0.jpg" alt="">
                                    </div>
                                    <button data-replace-with-video class="youtube-card__button">
                                        {!! SiteHelpers\SvgSpritemap::get('icon-play', [
                                            'class' => '',
                                            'width' => 28,
                                            'height' => 28
                                        ]) !!}
                                    </button>
                                    <time class="youtube-card__duration">{{ $video->duration }}</time>
                                </div>
                                <div class="youtube-card__title">
                                    {{ $video->title }}
                                </div>
                                <time class="youtube-card__date">
                                    {{ Carbon\Carbon::createFromTimestamp($video->date)->format('d F Y') }}
                                </time>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
    {!! Widget::show('seo-block') !!}
    {!! Widget::show('advantage::show') !!}
@endsection
