<?php

namespace App\Modules\Sales\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Sales\Models\Sales as SalesModel;

class JsonLd implements AbstractWidget
{
    /**
     * @var SalesModel|null
     */
    protected $salesItem;

    /**
     * JsonLd constructor.
     * @param SalesModel|null $salesItem
     */
    public function __construct(?SalesModel $salesItem = null)
    {
        $this->salesItem = $salesItem;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        if (!$this->salesItem) {
            return null;
        }
        $logo = null;
        $pathToLogo = 'app/public/' . config('app.logo.path') . '/' . config('app.logo.filename');
        $pathToLogo = preg_replace('/\/{2,}/', '/', $pathToLogo);
        $pathToLogo = storage_path($pathToLogo);
        if (is_file($pathToLogo)) {
            $logo = url(config('app.logo.urlPath') . '/' . config('app.logo.filename')) . '?v=' . filemtime($pathToLogo);
        }
        return view('sales::site.widgets.json-ld', [
            'sales' => $this->salesItem,
            'logo' => $logo
        ]);
    }

}
