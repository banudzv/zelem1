<?php

namespace App\Modules\Sales\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Sales\Models\Sales as SalesModel;

class SameSales implements AbstractWidget
{
    
    /**
     * @var null|int
     */
    protected $salesId;
    
    /**
     * SameSales constructor.
     *
     * @param int|null $salesId
     */
    public function __construct(?int $salesId = null)
    {
        $this->salesId = $salesId;
    }
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $sales = SalesModel::sameSales($this->salesId, 4);
        if (!$sales || !$sales->count()) {
            return null;
        }
        return view('sales::site.widgets.same-sales', [
            'sales' => $sales,
        ]);
    }
    
}
