<?php

namespace App\Modules\Sales\Widgets;

use App\Components\Widget\AbstractWidget;
use App\Modules\Sales\Models\Sales as SalesModel;

class Sales implements AbstractWidget
{
    
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function render()
    {
        $sales = SalesModel::salesForWidget();
        if (!$sales || !$sales->count()) {
            return null;
        }
        return view('sales::site.widget', [
            'sales' => $sales,
        ]);
    }
    
}
