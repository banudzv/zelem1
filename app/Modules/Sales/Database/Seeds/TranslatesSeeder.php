<?php

namespace App\Modules\Sales\Database\Seeds;

use App\Core\Modules\Translates\Models\Translate;
use Illuminate\Database\Seeder;

class TranslatesSeeder extends Seeder
{
    const MODULE = 'sales';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translates = [
            Translate::PLACE_ADMIN => [
                [
                    'name' => 'general.menu',
                    'ru' => 'Акции',
                ],
                [
                    'name' => 'general.stat-widget-title',
                    'ru' => 'Акции',
                ],
                [
                    'name' => 'seo.index',
                    'ru' => 'Список акций',
                ],
                [
                    'name' => 'seo.edit',
                    'ru' => 'Редактирование акции',
                ],
                [
                    'name' => 'seo.create',
                    'ru' => 'Добавление акции',
                ],
                [
                    'name' => 'seo.template-variables.name',
                    'ru' => 'Название акции',
                ],
                [
                    'name' => 'settings.group-name',
                    'ru' => 'Акции',
                ],
                [
                    'name' => 'settings.attributes.per-page',
                    'ru' => 'Количество акций на странице в админ панели',
                ],
                [
                    'name' => 'settings.attributes.per-page-client-side',
                    'ru' => 'Количество акций на странице на сайт',
                ],
                [
                    'name' => 'settings.attributes.addthis-key',
                    'ru' => 'Ключ для виджета "Поделится в соц. сетях" <a href="http://www.addthis.com/" target="_blank">www.addthis.com</a>',
                ],
                [
                    'name' => 'seo.count',
                    'ru' => 'Количество акций - :count',
                ],
                [
                    'name' => 'admin.sales-for-product',
                    'ru' => 'Акции',
                ],
            ],
            Translate::PLACE_SITE => [
                [
                    'name' => 'site.sales',
                    'ru' => 'Акции',
                ],
                [
                    'name' => 'site.sitemap-sales',
                    'ru' => 'Акции',
                ],
                [
                    'name' => 'site.all-sales',
                    'ru' => 'Все акции',
                ],
                [
                    'name' => 'site.same-sales',
                    'ru' => 'Последние акции',
                ],
                [
                    'name' => 'site.no-sales',
                    'ru' => 'Обубликованных акций нету',
                ],
            ]
        ];

        Translate::setTranslates($translates, static::MODULE);
    }
}
