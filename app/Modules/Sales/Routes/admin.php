<?php

use Illuminate\Support\Facades\Route;

// Routes for only authenticated administrators
Route::middleware(['auth:admin', 'permission:sales'])->group(function () {
    // Admins list in admin panel
    Route::put('sales/{sale}/active', ['uses' => 'SalesController@active', 'as' => 'admin.sales.active']);
    Route::get(
        'sales/{sale}/avatar/delete',
        ['uses' => 'SalesController@deleteImage', 'as' => 'admin.sales.delete-avatar']
    );
    Route::get('sales/{sale}/destroy', ['uses' => 'SalesController@destroy', 'as' => 'admin.sales.destroy']);
    Route::resource('sales', 'SalesController')->except('show', 'destroy')->names('admin.sales');

});

// Routes for unauthenticated administrators


