<?php

use Illuminate\Support\Facades\Route;

Route::paginate('sales', ['as' => 'site.sales', 'uses' => 'SalesController@index']);
Route::get('sales/{slug}', ['as' => 'site.sales-inner', 'uses' => 'SalesController@show']);