<?php

namespace App\Modules\Sales\Requests;

use App\Core\Interfaces\RequestInterface;
use App\Modules\Sales\Images\SalesImage;
use App\Modules\Sales\Models\Sales;
use App\Modules\Sales\Models\SalesTranslates;
use App\Rules\MultilangSlug;
use App\Traits\ValidationRulesTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ArticleRequest
 *
 * @package App\Modules\Articles\Requests
 */
class SalesRequest extends FormRequest implements RequestInterface
{
    use ValidationRulesTrait;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws \App\Exceptions\WrongParametersException
     */
    public function rules(): array
    {
        /**
         *
         *
         * @var Sales $sales
         */
        $sales = $this->route('sale');
        
        return $this->generateRules(
            [
                'active' => ['required', 'boolean'],
                SalesImage::getField() => ['sometimes', 'image', 'max:' . config('image.max-size')],
            ], [
                'name' => ['required', 'max:191'],
                'slug' => [
                    'required',
                    new MultilangSlug(
                        (new SalesTranslates())->getTable(),
                        null,
                        $sales ? $sales->id : null
                    ),
                ],
            ]
        );
    }
    
}
