<?php

namespace App\Modules\Sales\Models;

use App\Rules\MultilangSlug;
use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;


/**
 * App\Modules\Sales\Models\SalesTranslates
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $short_content
 * @property string|null $content
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property int $row_id
 * @property string $language
 * @property-read \App\Core\Modules\Languages\Models\Language $lang
 * @property-read \App\Modules\Sales\Models\Sales $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereRowId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereShortContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\SalesTranslates whereTitle($value)
 * @mixin \Eloquent
 */
class SalesTranslates extends Model
{
    use ModelTranslates;
    
    protected $table = 'sales_translates';
    
    public $timestamps = false;
    
    protected $fillable = ['slug', 'name', 'short_content', 'content', 'h1', 'title', 'keywords', 'description'];
    
    protected $hidden = ['row_id'];
    
}
