<?php

namespace App\Modules\Sales\Models;

use App\Modules\Sales\Images\SalesImage;
use App\Traits\ActiveScopeTrait;
use App\Traits\FormattedDateAdmin;
use App\Traits\ModelMain;
use App\Traits\Imageable;
use Carbon\Carbon;
use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Sales\Filters\SalesFilter;
use EloquentFilter\Filterable;
use Illuminate\Support\Str;

/**
 * App\Modules\Sales\Models\Sales
 *
 * @property int $id
 * @property bool $active
 * @property int $views
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Modules\Images\Models\Image[] $allImages
 * @property-read \App\Modules\Sales\Models\SalesTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Sales\Models\SalesTranslates[] $data
 * @property-read bool $is_in_feature
 * @property-read string $link
 * @property-read string $teaser
 * @property-read \App\Core\Modules\Images\Models\Image $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Modules\Images\Models\Image[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales active($active = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\Sales\Models\Sales whereViews($value)
 * @mixin \Eloquent
 * @property-read int|null $all_images_count
 * @property-read int|null $data_count
 * @property-read int|null $images_count
 */
class Sales extends Model
{
    use ModelMain, Imageable, Filterable, EloquentTentacle, ActiveScopeTrait, FormattedDateAdmin;

    protected $table = 'sales';

    const WIDGET_LIMIT = 4;

    const TEASER_WORDS_LIMIT = 30;

    const SEO_TEMPLATE_ALIAS = 'sales';

    protected $casts = ['active' => 'boolean'];

    protected $fillable = ['active'];
    
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Register filter model
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(SalesFilter::class);
    }

    /**
     * Set images upload config
     *
     * @return string|array
     */
    protected function imageClass()
    {
        return SalesImage::class;
    }

    /**
     * Get list of sales
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList()
    {
        return Sales::with(['current'])
            ->filter(request()->only('name', 'active'))
            ->latest('id')
            ->paginate(config('db.sales.per-page', 10));
    }

    /**
     * Paginated list of users
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getSalesForClientSide()
    {
        return Sales::with(['current', 'image', 'image.current'])
            ->active(true)
            ->latest('id')
            ->paginate(config('db.sales.per-page-client-side', 10));
    }

    /**
     * Sales list for widget
     *
     * @return Sales[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public static function salesForWidget()
    {
        return Sales::with('current', 'image', 'image.current')
            ->active(true)
            ->limit(Sales::WIDGET_LIMIT)
            ->latest('id')
            ->get();
    }

    /**
     * Sales list for widget `same sales`
     *
     * @param int|null $exceptSalesId
     * @param int $limit
     * @return Sales[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public static function sameSales(?int $exceptSalesId = null, int $limit = 4)
    {
        $sales = Sales::with('current', 'image', 'image.current')
            ->active(true)
            ->limit($limit)
            ->latest('id');
        if ($exceptSalesId) {
           $sales->where('id', '!=', $exceptSalesId);
        }
        return $sales->get();
    }

    /**
     * Link on the show sales inner page
     *
     * @return string
     */
    public function getLinkAttribute(): string
    {
        return route('site.sales-inner', [
            'slug' => $this->current->slug,
        ]);
    }

    /**
     * Short description
     *
     * @return string
     */
    public function getTeaserAttribute()
    {
        $teaser = strip_tags($this->current->short_content);
        $teaser = trim($teaser);
        $teaser = Str::words($teaser, Sales::TEASER_WORDS_LIMIT);
        return $teaser;
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function getPagesLinksByIdForImage(int $id)
    {
        $links = [];
        $item = Sales::active()->find($id);
        if($item){
            $links[] = url(route('site.sales-inner', ['slug' => $item->current->slug], false), [], isSecure());
        }
        return $links;
    }

    public static function getListForSelect()
    {
        $sales = Sales::with('current')
            ->active()
            ->orderBy('id')
            ->get();
        $arr = [];
        foreach ($sales as $sale) {
            $arr[$sale->id] = $sale->current->name;
        }
        return $arr;
    }

}
