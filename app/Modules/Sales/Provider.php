<?php

namespace App\Modules\Sales;

use App\Core\BaseProvider;
use App\Core\Modules\Administrators\Models\RoleRule;
use App\Core\Modules\Languages\Models\Language;
use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Sales\Models\SalesTranslates;
use App\Modules\Sales\Widgets\JsonLd;
use App\Modules\Sales\Widgets\Sales;
use App\Modules\Sales\Widgets\SameSales;
use Carbon\Carbon;
use CustomForm\Input;
use Illuminate\Database\Eloquent\Builder;
use App\Modules\Sales\Models\Sales as SalesModel;
use Widget, CustomMenu, CustomSettings, CustomRoles;

/**
 * Class Provider
 * Module configuration class
 *
 * @package App\Modules\Sales
 */
class Provider extends BaseProvider
{

    /**
     * Set custom presets
     */
    protected function presets()
    {
    }

    /**
     * Register widgets and menu for admin panel
     *
     * @throws \App\Exceptions\WrongParametersException
     */
    protected function afterBootForAdminPanel()
    {
        $settings = CustomSettings::createAndGet('sales', 'sales::settings.group-name');
        $settings->add(
            Input::create('per-page')
                ->setLabel('sales::settings.attributes.per-page')
                ->required(),
            ['required', 'integer', 'min:1']
        );
        $settings->add(
            Input::create('per-page-client-side')
                ->setLabel('sales::settings.attributes.per-page-client-side')
                ->required(),
            ['required', 'integer', 'min:1']
        );
        // Register left menu block
        CustomMenu::get()->group()
            ->block('content', 'sales::general.menu-block', 'fa fa-files-o')
            ->link('sales::general.menu', RouteObjectValue::make('admin.sales.index'))
            ->additionalRoutesForActiveDetect(
                RouteObjectValue::make('admin.sales.edit'),
                RouteObjectValue::make('admin.sales.create')
            );
        // Register role scopes
        CustomRoles::add('sales', 'sales::general.menu')->except(RoleRule::VIEW);
    }


    /**
     * Register module widgets and menu elements here for client side of the site
     */
    protected function afterBoot()
    {
        Widget::register(Sales::class, 'sales');
        Widget::register(SameSales::class, 'same-sales');
        Widget::register(JsonLd::class, 'sales::json-ld');
    }



    public function initSitemap()
    {
        $items = [];
        SalesModel::with('current')->active()->latest('id')->get()->each(function (SalesModel $sale) use (&$items) {
            $items[] = [
                'name' => $sale->current->name,
                'url' => $sale->link,
            ];
        });
        return [[
            'name' => __('sales::site.sitemap-sales'),
            'url' => route('site.sales'),
            'items' => $items,
        ]];
    }


    public function initSitemapXml()
    {
        $languages = config('languages', []);
        $default_language = null;
        /** @var Language $language */
        foreach($languages as $language){
            $prefix = $language->default ? '' : '/'.$language->slug;
            $items[] = [
                'url' => url($prefix . route('site.sales', [] , false), [], isSecure()),
            ];
            if($language->default){
                $default_language = $language->slug;
            }
        }

        SalesTranslates::whereHas('row', function (Builder $builder){
            $builder->where('active', true);
        })->get()->each(function (SalesTranslates $page) use (&$items, &$default_language) {
            $prefix = ($default_language === $page->language) ? '' : '/' . $page->language;
            $items[] = [
                'url' => url($prefix . route('site.sales-inner', ['slug' => $page->slug], false), [], isSecure())
            ];
        });
        return $items;
    }


}
