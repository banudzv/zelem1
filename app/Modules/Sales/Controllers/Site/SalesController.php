<?php

namespace App\Modules\Sales\Controllers\Site;

use App\Core\Modules\SystemPages\Models\SystemPage;
use App\Core\SiteController;
use App\Modules\Sales\Models\Sales;

/**
 * Class SalesController
 *
 * @package App\Modules\Sales\Controllers\Site
 */
class SalesController extends SiteController
{
    
    /**
     * @var SystemPage
     */
    static $page;
    
    /**
     * SalesController constructor.
     */
    public function __construct()
    {
        /** @var SystemPage $page */
        static::$page = SystemPage::getByCurrent('slug', 'sales');
        abort_unless(static::$page && static::$page->exists, 404);
    }
    
    /**
     * Sales list page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $sales = Sales::getSalesForClientSide();
        $this->pageNumber();
        $this->meta(static::$page->current, static::$page->current->content);
        $this->canonical(route('site.sales'));
        $this->breadcrumb(static::$page->current->name, 'site.sales');
        $this->setOtherLanguagesLinks(static::$page);
        return view('sales::site.index', [
            'sales' => $sales,
            'page' => static::$page,
        ]);
    }
    
    /**
     * Show sales page
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $slug)
    {
        /** @var Sales $sales */
        $sales = Sales::getByCurrent('slug', $slug);
        abort_unless($sales && $sales->exists && $sales->active, 404);
        $this->meta($sales->current);
        $this->metaTemplate(Sales::SEO_TEMPLATE_ALIAS, [
            'name' => $sales->current->name,
            'published_at' => $sales->formatted_published_at,
        ]);
        $this->breadcrumb(static::$page->current->name, 'site.sales');
        $this->breadcrumb($sales->current->name, 'site.sales-inner', [$sales->current->slug]);
        $this->setOtherLanguagesLinks($sales);
        $this->canonical(route('site.sales-inner', [$sales->current->slug]));
        return view('sales::site.show', [
            'sales' => $sales,
        ]);
    }
    
}
