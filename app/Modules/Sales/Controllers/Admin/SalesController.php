<?php

namespace App\Modules\Sales\Controllers\Admin;

use App\Core\ObjectValues\RouteObjectValue;
use App\Modules\Sales\Forms\SalesForm;
use App\Modules\Sales\Models\Sales;
use App\Modules\Sales\Requests\SalesRequest;
use Seo;
use App\Core\AdminController;

/**
 * Class SalesController
 *
 * @package App\Modules\Sales\Controllers\Admin
 */
class SalesController extends AdminController
{
    
    public function __construct()
    {
        Seo::breadcrumbs()->add('sales::seo.index', RouteObjectValue::make('admin.sales.index'));
    }
    
    /**
     * Register widgets with buttons
     */
    private function registerButtons()
    {
        // Create new sales button
        $this->addCreateButton('admin.sales.create');
    }
    
    /**
     * Sales sortable list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // Set page buttons on the top of the page
        $this->registerButtons();
        // Set h1
        Seo::meta()->setH1('sales::seo.index');
        Seo::meta()->setCountOfEntity(trans('sales::seo.count',['count' => Sales::count(['id'])]));
        // Get sales
        $sales = Sales::getList();
        // Return view list
        return view('sales::admin.index', ['sales' => $sales]);
    }
    
    /**
     * Create new element page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function create()
    {
        // Breadcrumb
        Seo::breadcrumbs()->add('sales::seo.create');
        // Set h1
        Seo::meta()->setH1('sales::seo.create');
        // Javascript validation
        $this->initValidation((new SalesRequest())->rules());
        // Return form view
        return view(
            'sales::admin.create', [
                'form' => SalesForm::make(),
            ]
        );
    }
    
    /**
     * Create page in database
     *
     * @param  SalesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function store(SalesRequest $request)
    {
        $sales = (new Sales);
        // Create new sales
        if ($message = $sales->createRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterStore(['id' => $sales->id]);
    }
    
    /**
     * Update element page
     *
     * @param Sales $sale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\WrongParametersException
     */
    public function edit(Sales $sale)
    {
        // Breadcrumb
        Seo::breadcrumbs()->add($sale->current->name ?? 'sales::seo.edit');
        // Set h1
        Seo::meta()->setH1('sales::seo.edit');
        // Javascript validation
        $this->initValidation((new SalesRequest)->rules());
        // Return form view
        return view(
            'sales::admin.update', [
                'form' => SalesForm::make($sale),
            ]
        );
    }
    
    /**
     * Update page in database
     *
     * @param  SalesRequest $request
     * @param  Sales $sale
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function update(SalesRequest $request, Sales $sale)
    {
        // Update existed sales
        if ($message = $sale->updateRow($request)) {
            return $this->afterFail($message);
        }
        // Do something
        return $this->afterUpdate();
    }
    
    /**
     * Totally delete page from database
     *
     * @param  Sales $sale
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function destroy(Sales $sale)
    {
        // Delete sales's image
        $sale->deleteAllImages();
        // Delete sales
        $sale->forceDelete();
        // Do something
        return $this->afterDestroy();
    }
    
    /**
     * Delete image
     *
     * @param  Sales $sale
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\WrongParametersException
     */
    public function deleteImage(Sales $sale)
    {
        // Delete sales's image
        $sale->deleteImagesIfExist();
        // Do something
        return $this->afterDeletingImage();
    }
    
}
