@php
    /** @var \App\Modules\Sales\Models\Sales[] $sales */
@endphp

@extends('admin.layouts.main')

@section('content')
    <div class="col-xs-12">
        {!! \App\Modules\Sales\Filters\SalesFilter::showFilter() !!}
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th></th>
                        <th>{{ __('validation.attributes.name') }}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($sales AS $item)
                        <tr>
                            <td width="50px"><img style="max-width: 40px;" src="{{ $item->image->link('40x40') }}"/></td>
                            <td>{{ Html::link($item->link, $item->current->name, ['target' => '_blank']) }}</td>
                            <td>{!! Widget::active($item) !!}</td>
                            <td>
                                {!! \App\Components\Buttons::edit('admin.sales.edit', $item->id) !!}
                                {!! \App\Components\Buttons::delete('admin.sales.destroy', $item->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="text-center">{{ $sales->appends(request()->except('page'))->links() }}</div>
    </div>
@stop
