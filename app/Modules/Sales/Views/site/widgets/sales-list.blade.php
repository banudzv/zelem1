@php
/** @var \App\Modules\Sales\Models\Sales[] $sales */
@endphp
<div class="grid{{ (isset($grid_mod_classes) ? ' ' . $grid_mod_classes : '') }} _nml-def">
    @foreach($sales as $element)
        <div class="gcell _pl-def _mb-xl">
            @include('sales::site.widgets.sales-list-card.sales-list-card', ['sales' => $element])
        </div>
    @endforeach
</div>
