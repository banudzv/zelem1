@php
/** @var \App\Modules\Sales\Models\Sales[] $sales */
@endphp

<div class="container container--inner">
    <div class="box">
        <div class="title title--size-h1 _mb-lg _text-center _sm-text-left">@lang('sales::site.same-sales')</div>
        @include('sales::site.widgets.sales-list', [
            'sales' => $sales,
            'grid_mod_classes' => 'grid--1 grid--sm-2 grid--def-4',
        ])
    </div>
</div>
