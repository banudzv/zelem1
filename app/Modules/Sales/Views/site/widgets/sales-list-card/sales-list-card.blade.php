@php
/** @var App\Modules\Sales\Models\Sales $sales */
@endphp

<div class="sales-card">
	<a href="{{ $sales->link }}" class="sales-card__image lazyload-blur">
		{!! $sales->imageTag('345x255', ['class' => 'lazyload-blur__image', ]) !!}
	</a>
	<div class="sales-card__title">
		<a href="{{ $sales->link }}">{{ $sales->current->name }}</a>
	</div>
	<div class="sales-card__desc _def-show">
		{{ $sales->teaser }}
	</div>
	{!! Widget::show('sales::json-ld', $sales) !!}
</div>
