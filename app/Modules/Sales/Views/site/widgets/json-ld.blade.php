@php
/** @var \App\Modules\Sales\Models\Sales $sales */
/** @var string|null $logo */
@endphp
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "SalesArticle",
    "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "{{ $sales->link }}"
    },
    "headline": "{{ $sales->current->name }}",
    @if($sales->image && $sales->image->isImageExists())
    "image": [
        "{{ $sales->image->link() }}"
    ],
    @endif
    "datePublished": "{{ $sales->published_at }}",
    "dateModified": "{{ $sales->updated_at ?: $sales->published_at }}",
    "author": {
        "@type": "Organization",
        "name": "{{ env('APP_NAME') }}"
    },
    "publisher": {
        "@type": "Organization",
        "name": "{{ env('APP_NAME') }}",
        @if(isset($logo) && $logo)
        "logo": {
            "@type": "ImageObject",
            "url": "{{ $logo }}}"
        }
        @endif
    },
    "description": "{{ $sales->teaser }}"
}
</script>
