@php
/** @var \App\Modules\Sales\Models\Sales[]|\Illuminate\Pagination\LengthAwarePaginator $sales */
/** @var \App\Core\Modules\SystemPages\Models\SystemPage $page */
@endphp

@extends('site._layouts.main')

@section('layout-body')
    <div class="section _mb-lg">
        <div class="container container--inner _mb-xl _def-mb-xxl">
            <div class="box">
                @if($sales->count() > 0)
                    @include('sales::site.widgets.sales-list', [
                        'sales' => $sales,
                        'grid_mod_classes' => 'grid--2 grid--def-4',
                    ])
                    {!! $sales->links('pagination.site') !!}
                @else
                    {{ trans('sales::site.no-sales') }}
                @endif
                <hr class="separator _color-white _mtb-xl">
            </div>
        </div>
    </div>

    {!! Widget::show('products::new') !!}
@endsection
