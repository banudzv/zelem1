@php
    /** @var \App\Modules\Sales\Models\Sales[] $sales */
@endphp
<div class="section _def-show">
    <div class="container _mtb-lg _def-mtb-xxl">
        <div class="grid grid--auto _justify-between _items-center _pb-def">
            <div class="gcell _mb-def _mr-def">
                <div class="title title--size-h2 _color-dark-blue title--normal">@lang('sales::site.sales')</div>
            </div>
            <div class="gcell _mb-def _self-end">
                <a href="{{route('site.sales')}}" class="link link--underline link--icon-small text--semi-bold link--dark">
                    {{trans('sales::site.all-sales')}}
                    <div class="icon">
                        {!! SiteHelpers\SvgSpritemap::get('icon-arrow-right') !!}
                    </div>
                </a>
            </div>
        </div>

        @include('sales::site.widgets.sales-list', [
			'sales' => $sales,
			'grid_mod_classes' => 'grid--2 grid--def-4',
		])
    </div>
</div>
