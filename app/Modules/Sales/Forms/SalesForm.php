<?php

namespace App\Modules\Sales\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\Sales\Images\SalesImage;
use App\Modules\Sales\Models\Sales;
use CustomForm\Builder\FieldSet;
use CustomForm\Builder\Form;
use CustomForm\Image;
use CustomForm\Input;
use CustomForm\Macro\InputForSlug;
use CustomForm\Macro\Slug;
use CustomForm\Macro\Toggle;
use CustomForm\TextArea;
use CustomForm\TinyMce;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleForm
 *
 * @package App\Core\Modules\Administrators\Forms
 */
class SalesForm implements FormInterface
{
    
    /**
     * @param  Model|Sales|null $sales
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $sales = null): Form
    {
        $sales = $sales ?? new Sales;
        $form = Form::create();
        // Field set with languages tabs
        $form->fieldSetForLang(7)->add(
            InputForSlug::create('name', $sales)->required(),
            Slug::create('slug', $sales)->required(),
            TextArea::create('short_content', $sales),
            TinyMce::create('content', $sales),
            Input::create('h1', $sales),
            Input::create('title', $sales),
            TextArea::create('keywords', $sales),
            TextArea::create('description', $sales)
        );
        // Simple field set
        $form->fieldSet(5, FieldSet::COLOR_SUCCESS)->add(
            Toggle::create('active', $sales)->required(),
            Image::create(SalesImage::getField(), $sales->image)
        );
        return $form;
    }
    
}
