<?php

namespace App\Modules\Subscribe\Forms;

use App\Core\Interfaces\FormInterface;
use App\Modules\Subscribe\Models\Subscriber;
use CustomForm\Builder\Form;
use CustomForm\Input;
use CustomForm\Select;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleForm
 *
 * @package App\Core\Modules\Administrators\Forms
 */
class AdminSubscriberExportForm implements FormInterface
{

    /**
     * @param  Model|Subscriber|null $subscriber
     * @return Form
     * @throws \App\Exceptions\WrongParametersException
     */
    public static function make(?Model $subscriber = null): Form
    {
        $form = Form::create();
        // Field set without languages tabs
        $form->fieldSet()->add(
            Select::create('extension')
                ->add(
                    [
                        'xls' => 'xls',
                        'xlsx' => 'xlsx',
                        'csv' => 'csv',
                    ]
                )
                ->setValue(request('extension'))
                ->addClassesToDiv('col-md-4'),
            Input::create('quote')->setValue(request('extension') ?: '"')->addClassesToDiv('col-md-4'),
            Input::create('coma')->setValue(request('extension') ?: ',')->addClassesToDiv('col-md-4')

        );
        $form->buttons->doNotShowCloseButton();
        $form->buttons->doNotShowSaveAndAddButton();
        $form->buttons->doNotShowSaveAndCloseButton();
        $form->doNotShowBottomButtons();
        return $form;
    }

    public function attributes()
    {
        return [
            'name' => __('validation.attributes.first_name'),
        ];
    }

}
